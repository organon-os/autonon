set -e
echo "starting to create virtual environment and install packages"
python -m venv orgenv


ACTIVATION_FILE="orgenv/bin/activate"
if ! [ -f "$ACTIVATION_FILE" ]; then
    ACTIVATION_FILE="orgenv/Scripts/activate"
fi
source "$ACTIVATION_FILE"

python -m pip install --upgrade pip

python -m pip install -r requirements.txt
echo "finished to create virtual environment and install packages"

echo "compiling cython sources"
python cython_setup.py build_ext --inplace
echo "cython sources are compiled"


echo "Generating development environment finished"