call "orgenv/Scripts/activate.bat" || goto :ERROR
coverage run --source=organon --omit="organon/tests/*" organon/tests/all_tests.py || goto :ERROR
coverage xml -o reports/coverage.xml || goto :ERROR
exit 0

:ERROR
exit /b %errorlevel%