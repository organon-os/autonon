"""Includes RFSelectionOutput class"""
from organon.ml.feature_selection.domain.objects.base_selection_output import BaseSelectionOutput


class RFSelectionOutput(BaseSelectionOutput):
    """Output of random forest selection"""
