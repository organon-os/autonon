"""Includes LassoSelectionOutput class"""
from organon.ml.feature_selection.domain.objects.base_selection_output import BaseSelectionOutput


class LassoSelectionOutput(BaseSelectionOutput):
    """Output of Lasso selection"""
