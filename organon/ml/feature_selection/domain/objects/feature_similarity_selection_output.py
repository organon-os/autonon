"""Includes FeatureSimilaritySelectionOutput class."""
from organon.ml.feature_selection.domain.objects.base_selection_output import BaseSelectionOutput


class FeatureSimilaritySelectionOutput(BaseSelectionOutput):
    """Output of Feature Similarity selection"""
