"""Includes CFSSelectionOutput class"""
from organon.ml.feature_selection.domain.objects.base_selection_output import BaseSelectionOutput


class CFSSelectionOutput(BaseSelectionOutput):
    """Output of CFS selection"""
