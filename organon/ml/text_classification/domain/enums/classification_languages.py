"""Includes ClassificationLanguages enum class"""
from enum import Enum, auto


class ClassificationLanguages(Enum):
    """Supported Languages"""
    ENGLISH = auto()
    TURKISH = auto()
