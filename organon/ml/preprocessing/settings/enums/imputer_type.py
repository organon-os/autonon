"""
This module includes enum class ImputerType.
"""
from enum import Enum


class ImputerType(Enum):
    """
    TargetType
    """
    SIMPLE = 1
    ITERATIVE = 2
