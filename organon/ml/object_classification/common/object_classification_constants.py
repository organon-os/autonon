"""Includes ObjectClassificationConstants"""


class ObjectClassificationConstants:
    """Object classification constants"""
    PREDICTON_COL_NAME = "pred"
    PROBA_COL_NAME = "prob"
    LABELS_COL_NAME = "label"
    FILENAME_COL_NAME = "filename"
    XCEPTION_MIN_IMG_SIZE = 71
    RES_NET_50_V2_MIN_IMG_SIZE = 32
    INCEPTION_RES_NET_V2_MIN_IMG_SIZE = 75
