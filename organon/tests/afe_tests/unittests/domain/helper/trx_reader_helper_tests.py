"""Includes tests for trx_reader_helper module."""
import unittest
from datetime import datetime
from unittest.mock import patch

from organon.afe.core.businessobjects.afe_static_objects import AfeStaticObjects

from organon.afe.dataaccess.services.transaction_file_record_reader import TransactionFileRecordReader
from organon.afe.domain.enums.afe_date_column_type import AfeDateColumnType
from organon.afe.domain.enums.date_resolution import DateResolution
from organon.afe.domain.modelling.helper import trx_reader_helper
from organon.afe.domain.settings.afe_algorithm_settings import AfeAlgorithmSettings
from organon.afe.domain.settings.afe_data_settings import AfeDataSettings
from organon.afe.domain.settings.afe_date_column import AfeDateColumn
from organon.afe.domain.settings.afe_reading_settings import AfeDataReadingSettings
from organon.afe.domain.settings.feature_generation_settings import FeatureGenerationSettings
from organon.afe.domain.settings.temporal_grid import TemporalGrid
from organon.afe.domain.settings.trx_descriptor import TrxDescriptor


def get_feature_generation_settings(date_col_name="datecol_1", no_date=False):
    """Generate mock feature generation settings for tests"""
    feature_generation_settings = FeatureGenerationSettings()
    if not no_date:
        feature_generation_settings.date_column = AfeDateColumn(column_name=date_col_name,
                                                                date_column_type=AfeDateColumnType.DateTime)
        feature_generation_settings.horizon_list = [11, 21, 1, 31]
        feature_generation_settings.date_offset = 0
        feature_generation_settings.temporal_grids = [TemporalGrid(offset=1, stride=31, length=4)]

    else:
        feature_generation_settings.temporal_grids = [TemporalGrid(offset=1, stride=1, length=1)]
        feature_generation_settings.horizon_list = [1]
    feature_generation_settings.date_resolution = DateResolution.Day
    feature_generation_settings.dimension_columns = ["dim1", "dim2"]
    feature_generation_settings.quantity_columns = ["qty1"]
    return feature_generation_settings


def get_data_source_settings():
    """Generate mock afe data settings."""
    data_source_settings = AfeDataSettings()
    trx_descriptor = TrxDescriptor()
    trx_descriptor.feature_gen_setting = get_feature_generation_settings()
    trx_descriptor.reading_settings = AfeDataReadingSettings(number_of_rows_per_step=1000)
    data_source_settings.trx_descriptor = trx_descriptor
    data_source_settings.max_number_of_transaction_samples = 10000
    return data_source_settings


def get_algorithm_settings():
    """Generate mock algorithm settings."""
    alg_settings = AfeAlgorithmSettings()
    alg_settings.dimension_compression_ratio = 0.8
    alg_settings.dimension_max_cardinality = 200
    return alg_settings


class TrxReaderHelperTestCase(unittest.TestCase):
    """Test class for trx_reader_helper module."""

    @classmethod
    def setUpClass(cls) -> None:
        AfeStaticObjects.set_defaults()

    def setUp(self) -> None:
        self.trx_reader_patcher = patch(trx_reader_helper.__name__ + "." + TransactionFileRecordReader.__name__)
        self.mock_trx_reader_class = self.trx_reader_patcher.start()
        self.algorithm_settings = get_algorithm_settings()
        self.data_source_settings = get_data_source_settings()

    def tearDown(self) -> None:
        self.trx_reader_patcher.stop()

    def get_nth_arg(self, arg_index: int = None, kwargs_key=None):
        """Get argument value for given index (or keyword if given as keyword argument) on reader initailization"""
        if arg_index is not None:
            return self.mock_trx_reader_class.call_args.args[arg_index]
        return self.mock_trx_reader_class.call_args.kwargs[kwargs_key]

    def test_date_columns(self):
        """tests if reader is generated with correct date_columns"""
        self.data_source_settings.trx_descriptor.feature_gen_setting = get_feature_generation_settings(no_date=True)
        trx_reader_helper.get_trx_file_record_reader_for_modelling(["dim1"], ["qty1"], self.data_source_settings,
                                                                   self.algorithm_settings,
                                                                   {}, ["1", "2", "3"])
        date_cols = self.get_nth_arg(1)
        self.assertEqual(0, len(date_cols))  # no date column should not be in the date_columns dictionary

    def test_min_max_dates_per_date_column(self):
        """tests if reader is generated with correct min_max_dates_per_date_column"""
        settings_with_max_obs_date = get_feature_generation_settings(date_col_name="datecol_2")
        settings_with_max_obs_date.max_observation_date = datetime(2020, 1, 1)
        self.data_source_settings.trx_descriptor.feature_gen_setting = settings_with_max_obs_date
        trx_reader_helper.get_trx_file_record_reader_for_modelling(["dim1"], ["qty1"], self.data_source_settings,
                                                                   self.algorithm_settings,
                                                                   {}, ["1", "2", "3"])
        min_max_dates_per_date_col = self.get_nth_arg(kwargs_key="min_max_dates_per_date_column")
        self.assertEqual(1, len(min_max_dates_per_date_col))
        self.assertIn("datecol_2", min_max_dates_per_date_col)
        self.assertEqual((None, datetime(2020, 1, 1)), min_max_dates_per_date_col["datecol_2"])


if __name__ == '__main__':
    unittest.main()
