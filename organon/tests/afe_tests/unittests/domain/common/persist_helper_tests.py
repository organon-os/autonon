"""Includes tests for PersistHelper"""
import unittest
from typing import List
from unittest.mock import MagicMock

from organon.afe.core.businessobjects.afe_static_objects import AfeStaticObjects
from organon.afe.domain.common.persist_helper import PersistHelper
from organon.afe.domain.enums.afe_date_column_type import AfeDateColumnType
from organon.afe.domain.enums.afe_operator import AfeOperator
from organon.afe.domain.enums.date_resolution import DateResolution
from organon.afe.domain.modelling.businessobjects.afe_column import AfeColumn
from organon.afe.domain.modelling.businessobjects.afe_feature import AfeFeature
from organon.afe.domain.modelling.businessobjects.transaction_file_stats import TransactionFileStats
from organon.afe.domain.settings.afe_date_column import AfeDateColumn
from organon.fl.core.businessobjects.histogram_16 import Histogram16
from organon.fl.core.exceptionhandling.known_exception import KnownException


def get_mock_histogram(rev_index_dict):
    """returns a mock histogram16 instance"""
    hist = MagicMock(wraps=Histogram16())
    hist.reverse_index = rev_index_dict
    return hist


def get_histogram_side_effect(stats_dict, dimension_name):
    """side effect for get_histogram which returns a mock histogram16 instance"""
    mock_histogram = get_mock_histogram(stats_dict[dimension_name])
    return mock_histogram


def get_mock_stats(stats_dict):
    """returns mock TransactionFileStats"""
    stats = MagicMock(wraps=TransactionFileStats())
    stats.get_histogram.side_effect = lambda dim_col: get_histogram_side_effect(stats_dict, dim_col)
    return stats


def get_feature(feature_name: str, dimension_name: str, group_id: int, _set: List[int], date_col_name: str = "dc",
                quantity_name: str = "qty", operator=AfeOperator.Sum,
                in_out: bool = True, date_resolution: DateResolution = DateResolution.Day,
                offset: int = 0, time_window: int = 1,
                source="some source", feature_extended_name: str = None,
                dimension_set: str = "ds"):
    """generates column sql mock object"""
    # pylint: disable=too-many-arguments
    col = AfeColumn()
    col.dimension_name = dimension_name
    if date_col_name is not None:
        col.date_column = AfeDateColumn(column_name=date_col_name, date_column_type=AfeDateColumnType.DateTime)
    col.quantity_name = quantity_name
    col.operator = operator
    col.group_id = group_id
    col.set = _set
    col.in_out = in_out
    col.date_resolution = date_resolution
    col.offset = offset
    col.time_window = time_window
    col.source = source
    if feature_extended_name is None:
        feature_extended_name = feature_name + "-ext"
    col.build_column_name_with_map = MagicMock(name="build_column_name_with_map", return_value=feature_extended_name)
    col.get_dimension_set = MagicMock(name="get_dimension_set", return_value=dimension_set)
    feature = AfeFeature(afe_column=col)
    feature.feature_name = feature_name
    return feature


class PersistHelperTestCase(unittest.TestCase):
    """Test class for PersistHelper"""

    def setUp(self) -> None:
        self.stats_dict = {
            "dim_col_1": {1: "dimval1", 2: "dimval2"},
            "dim_col_2": {1: "dimval1", 2: "dimval2"},
            "dimension_col_3": {1: "dimval1", 2: "dimval2"}
        }
        self.mock_stats = get_mock_stats(self.stats_dict)

    def test_get_lookup_data_frame_empty_collection(self):
        """tests if exception is raised when column_sql_collection is empty in get_lookup_data_frame"""
        with self.assertRaisesRegex(KnownException, "Output features are empty."):
            PersistHelper.get_lookup_data_frame([], TransactionFileStats())

    def test_get_lookup_data_frame_dim_set(self):
        """tests if dimension set strings are written correctly"""
        stats = self.mock_stats
        collection = [
            get_feature("ft1", "dim_col_1", 1, [1, 2]),
            get_feature("feature_2", "dim_col_1", 2, [1, 2]),
            get_feature("feature_3", "dim_col_1", 0, [1, 2], quantity_name=AfeStaticObjects.empty_quantity_column,
                        operator=AfeOperator.CountDistinct),
            get_feature("feature_4", "dim_col_1", 0, [1, 2], quantity_name=AfeStaticObjects.empty_quantity_column,
                        operator=AfeOperator.Mode),
        ]

        collection[0].afe_column.get_dimension_set = MagicMock(return_value="dim_set_str_1")
        collection[1].afe_column.get_dimension_set = MagicMock(return_value="dim_set_str_2")
        collection[2].afe_column.get_dimension_set = MagicMock(return_value="dim_set_str_3")
        collection[3].afe_column.get_dimension_set = MagicMock(return_value="dim_set_str_4")

        data_frame = PersistHelper.get_lookup_data_frame(collection, stats)
        self.assertEqual(4, len(data_frame))
        self.assertEqual("dim_set_str_1", data_frame.iloc[0]["DIMENSION_SET"])
        self.assertEqual("dim_set_str_2", data_frame.iloc[1]["DIMENSION_SET"])  # tests compression
        self.assertEqual("", data_frame.iloc[2]["DIMENSION_SET"])
        self.assertEqual("", data_frame.iloc[3]["DIMENSION_SET"])

    def test_get_lookup_data_frame_feature_extended_name(self):
        """tests if feature extended names are written correctly"""
        stats = self.mock_stats
        collection = [
            get_feature("ft1", "dim_col_1", 1, [1, 2]),
            get_feature("feature_2", "dim_col_1", 2, [1, 2])
        ]

        collection[0].afe_column.build_column_name_with_map = MagicMock(return_value="extended_1")
        collection[1].afe_column.build_column_name_with_map = MagicMock(return_value="abcde" * 30)

        data_frame = PersistHelper.get_lookup_data_frame(collection, stats)
        self.assertEqual(2, len(data_frame))
        row1 = data_frame.iloc[0]
        row2 = data_frame.iloc[1]
        self.assertEqual("extended_1", row1["FEATURE_EXTENDED_NAME"])
        self.assertEqual("abcde" * 19 + "ab...", row2["FEATURE_EXTENDED_NAME"])  # tests compression

    def test_get_lookup_data_frame_no_date_col(self):
        """Tests if no date columns are written as None in df"""
        stats = self.mock_stats
        collection = [
            get_feature("ft1", "dim_col_1", 1, [1, 2]),
            get_feature("feature_2", "dim_col_1", 2, [1, 2], date_col_name=None)
        ]

        data_frame = PersistHelper.get_lookup_data_frame(collection, stats)
        self.assertEqual(2, len(data_frame))
        self.assertIsNotNone(data_frame.iloc[0]["DATE_COLUMN_NAME"])
        self.assertIsNone(data_frame.iloc[1]["DATE_COLUMN_NAME"])

    def test_get_lookup_table_feature_ext_name_compression(self):
        """tests if feature extended names get compressed if longer than 100 characters"""
        stats = self.mock_stats
        collection = [
            get_feature("ft1", "dim_col_1", 1, [1, 2], feature_extended_name="a" * 150)
        ]
        data_frame = PersistHelper.get_lookup_data_frame(collection, stats)
        self.assertEqual("a" * 97 + "...", data_frame.iloc[0]["FEATURE_EXTENDED_NAME"])

    def test_print_report(self):
        """tests print_report"""
        name_to_column = {
            "feature1": get_feature("feature1", "dim_col_1", 1, [1, 2],
                                    feature_extended_name="feature_1_ext").afe_column,
            "feature2": get_feature("feature2", "dim_col_1", 2, [1, 2],
                                    feature_extended_name="feature_2_ext").afe_column,
            "feature3": get_feature("feature3", "dim_col_1", 1, [1],
                                    feature_extended_name="feature_3_ext").afe_column,
            "feature4": get_feature("feature4", "dim_col_1", 1, [1], date_col_name=None,
                                    feature_extended_name="feature_4_ext").afe_column,
        }

        for name, col in name_to_column.items():
            col.build_column_name_with_map = MagicMock(name="build_column_name_with_map", return_value=name + "-ext")

        str_in_file = ""

        def append_to_str(to_append):
            nonlocal str_in_file
            str_in_file += to_append

        mock_file = MagicMock(name="file")
        mock_file.write.side_effect = append_to_str
        mock_stats = MagicMock(spec=TransactionFileStats)

        histogram_mock = MagicMock()
        mock_stats.get_histogram.return_value = histogram_mock
        selected_cols = ["feature1", "feature2"]
        PersistHelper.print_report(selected_cols, mock_file, "dim_col_1", "qty_col_1", name_to_column,
                                   mock_stats)
        self.assertIn("dim_col_1", str_in_file)
        self.assertIn("qty_col_1", str_in_file)
        self.assertIn("feature1-ext", str_in_file)
        self.assertIn("feature2-ext", str_in_file)
        self.assertNotIn("feature_3", str_in_file)
        self.assertNotIn("feature_4", str_in_file)
        self.assertIn("******************", str_in_file)  # seperator should be written
        mock_file.flush.assert_called_once()
        for name, col in name_to_column.items():
            if name in selected_cols:
                col.build_column_name_with_map.assert_called_with(histogram_mock.reverse_index)


if __name__ == '__main__':
    unittest.main()
