"""Includes afe date helper tests."""
import unittest
from datetime import datetime
from unittest.mock import patch

from organon.afe.domain.common import afe_date_helper
from organon.afe.domain.enums.afe_date_column_type import AfeDateColumnType
from organon.afe.domain.enums.date_resolution import DateResolution


class AfeDateHelperTestCase(unittest.TestCase):
    """Test class for afe date helper"""

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()

    def test_get_date_subtraction_func_year(self):
        """test get_date_subtraction_func for year """
        timestamp = int(datetime.strptime('29-01-2018', '%d-%m-%Y').timestamp())
        new_time_stamp = timestamp - (60 * 60 * 24 * 365 * 31 * 1000)
        func = afe_date_helper.get_date_subtraction_func(DateResolution.Year, 31)
        value = func(timestamp)
        self.assertEqual(new_time_stamp, value)

    def test_get_date_subtraction_func_month(self):
        """test get_date_subtraction_func for month """

        timestamp = int(datetime.strptime('29-01-2018', '%d-%m-%Y').timestamp())
        new_time_stamp = timestamp - (60 * 60 * 24 * 30 * 31 * 1000)
        func = afe_date_helper.get_date_subtraction_func(DateResolution.Month, 31)
        value = func(timestamp)
        self.assertEqual(new_time_stamp, value)

    def test_get_date_subtraction_func_day(self):
        """test get_date_subtraction_func for day """

        timestamp = int(datetime.strptime('29-01-2018', '%d-%m-%Y').timestamp())
        new_time_stamp = timestamp - (60 * 60 * 24 * 31 * 1000)
        func = afe_date_helper.get_date_subtraction_func(DateResolution.Day, 31)
        value = func(timestamp)
        self.assertEqual(new_time_stamp, value)

    def test_get_date_subtraction_func_hour(self):
        """test get_date_subtraction_func for hour """

        timestamp = int(datetime.strptime('29-01-2018', '%d-%m-%Y').timestamp())
        new_time_stamp = timestamp - (60 * 60 * 31 * 1000)
        func = afe_date_helper.get_date_subtraction_func(DateResolution.Hour, 31)
        value = func(timestamp)
        self.assertEqual(new_time_stamp, value)

    def test_get_date_subtraction_func_minute(self):
        """test get_date_subtraction_func for minute """

        timestamp = int(datetime.strptime('29-01-2018', '%d-%m-%Y').timestamp())
        new_time_stamp = timestamp - (60 * 31 * 1000)
        func = afe_date_helper.get_date_subtraction_func(DateResolution.Minute, 31)
        value = func(timestamp)
        self.assertEqual(new_time_stamp, value)

    def test_get_date_subtraction_func_second(self):
        """test get_date_subtraction_func for second """

        timestamp = int(datetime.strptime('29-01-2018', '%d-%m-%Y').timestamp())
        new_time_stamp = timestamp - (31 * 1000)
        func = afe_date_helper.get_date_subtraction_func(DateResolution.Second, 31)
        value = func(timestamp)
        self.assertEqual(new_time_stamp, value)

    def test_get_date_subtraction_func_not_implemented(self):
        """test get_date_subtraction_func for not implemented date resolution """

        with self.assertRaises(NotImplementedError):
            afe_date_helper.get_date_subtraction_func("No Date", 31)

    def test_get_str_to_date_converter(self):
        """test get_str_to_date_converter for yyyyMm and YyyyMmDd """

        self.assertEqual(afe_date_helper.get_str_to_date_converter(AfeDateColumnType.YyyyMm)("201801"),
                         datetime(2018, 1, 1))
        self.assertEqual(
            afe_date_helper.get_str_to_date_converter(AfeDateColumnType.YyyyMmDd)("20180129"), datetime(2018, 1, 29))

    @patch("organon.afe.domain.common.afe_date_helper.get_date_from_string")
    def test_get_str_to_date_converter_use_date_helper(self, mock_date_helper):
        """test get_str_to_date_converter for DateTime and CustomFormat """

        mock_date_helper.return_value = datetime(2018, 1, 29, 15, 15, 15)
        afe_date_helper.get_str_to_date_converter(AfeDateColumnType.DateTime)
        self.assertEqual(
            afe_date_helper.get_str_to_date_converter(AfeDateColumnType.DateTime)(""),
            datetime(2018, 1, 29, 15, 15, 15))
        custom_format = "%d/%m/%Y %H-%M-%S"
        self.assertEqual(
            afe_date_helper.get_str_to_date_converter(AfeDateColumnType.DateTime, custom_format)("")
            , datetime(2018, 1, 29, 15, 15, 15))
        mock_date_helper.assert_called()

    def test_get_str_to_date_converter_not_implemented(self):
        """test get_str_to_date_converter for not implemented date column type """

        with self.assertRaises(NotImplementedError):
            afe_date_helper.get_str_to_date_converter("YYYYMMDD")("201801")

    @patch("organon.afe.domain.common.afe_date_helper.format_date")
    def test_get_date_to_str_converter(self, mock_date_helper):
        """test get_date_to_str_converter function"""

        mock_date_helper.return_value = "String date"
        custom_format = "%d/%m/%Y %H-%M-%S"
        afe_date_helper.get_date_to_str_converter(AfeDateColumnType.DateTime)("")
        afe_date_helper.get_date_to_str_converter(AfeDateColumnType.YyyyMmDd)("")
        afe_date_helper.get_date_to_str_converter(AfeDateColumnType.YyyyMm)("")
        afe_date_helper.get_date_to_str_converter(AfeDateColumnType.CustomFormat, custom_format)("")
        mock_date_helper.assert_called()
        self.assertEqual(4, mock_date_helper.call_count)

    def test_get_date_to_str_converter_not_implemented(self):
        """test get_date_to_str_converter function for not implemented date column type"""

        with self.assertRaises(NotImplementedError):
            afe_date_helper.get_date_to_str_converter("YYYYMMDD")("201801")
