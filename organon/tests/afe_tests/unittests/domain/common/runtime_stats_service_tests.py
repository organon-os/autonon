"""Includes runtime stats service tests."""
import time
import unittest
from unittest.mock import patch

from organon.afe.domain.common import runtime_stats_service
from organon.afe.domain.common.runtime_stats_service import RuntimeStatsService
from organon.fl.core.exceptionhandling.known_exception import KnownException


class IncrementalSideEffect:
    """Helper class to generate a side effect which changes with call count"""

    def __init__(self):
        self.counter = 0

    def get_mem_usage_side_effect(self, *args):
        """Side effect method t0 generate mock data for increasing memory usage"""
        # pylint: disable=unused-argument
        self.counter += 1
        return self.counter * 1024 * 1024 * 1024


_RUNTIME_STATS_SERVICE_MODULE_NAME = runtime_stats_service.__name__


class RuntimeStatsServiceTestCase(unittest.TestCase):
    """Test class for RuntimeStatsService"""

    mock_watch = None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.mock_watch = patch(f'{_RUNTIME_STATS_SERVICE_MODULE_NAME}.StopWatch')
        watch = cls.mock_watch.start()
        watch.return_value.get_elapsed_seconds.return_value = 20
        watch.return_value.get_cpu_times.return_value = (20.0, 25.0)

    @classmethod
    def tearDownClass(cls) -> None:
        cls.mock_watch.stop()

    def setUp(self) -> None:
        self.service = RuntimeStatsService()
        # process_info_helper should be mocked seperately for every method. that's why its not in setUpClass
        self.mock_pih = patch(f'{_RUNTIME_STATS_SERVICE_MODULE_NAME}.process_info_helper')
        lst = self.mock_pih.start()
        mem_usage_side_effect = IncrementalSideEffect()
        lst.get_memory_usage.side_effect = mem_usage_side_effect.get_mem_usage_side_effect
        lst.get_cpu_percentage_since_last_call.return_value = 15

    def tearDown(self) -> None:
        self.service.stop_recording_runtime_stats()  # test processes won't stop if this is not called
        self.mock_pih.stop()

    def test_service_is_running(self):
        """tests recording runtime stats"""
        service = self.service
        service.start_recording_runtime_stats(interval_in_seconds=0.2)
        self.assertTrue(service.is_running)

        service.stop_recording_runtime_stats()
        self.assertFalse(service.is_running)

    def test_stats_during_recording(self):
        """tests statistics values before stopping recording"""
        self.service.start_recording_runtime_stats(interval_in_seconds=0.2)

        time.sleep(1.1)
        self.assertEqual(15, list(self.service.runtime_statistics.cpu_usage.values())[0])
        self.assertEqual(1024, list(self.service.runtime_statistics.memory_usage.values())[0])
        self.assertEqual(2048, list(self.service.runtime_statistics.memory_usage.values())[1])
        self.assertEqual(3072, list(self.service.runtime_statistics.memory_usage.values())[2])
        self.assertIsNone(self.service.runtime_statistics.execution_time)
        self.assertIsNone(self.service.runtime_statistics.user_cpu_time)
        self.assertIsNone(self.service.runtime_statistics.system_cpu_time)

    def test_stats_after_recording(self):
        """tests statistics values after stopping recording"""
        self.service.start_recording_runtime_stats(interval_in_seconds=0.2)
        time.sleep(1.1)
        self.service.stop_recording_runtime_stats()
        self.assertEqual(15, list(self.service.runtime_statistics.cpu_usage.values())[0])
        self.assertEqual(1024, list(self.service.runtime_statistics.memory_usage.values())[0])
        self.assertEqual(2048, list(self.service.runtime_statistics.memory_usage.values())[1])
        self.assertEqual(3072, list(self.service.runtime_statistics.memory_usage.values())[2])
        self.assertEqual(20, self.service.runtime_statistics.execution_time)
        self.assertEqual(20.0, self.service.runtime_statistics.user_cpu_time)
        self.assertEqual(25.0, self.service.runtime_statistics.system_cpu_time)

    def test_thread_created_after_start(self):
        """tests if thread starts after starting recording"""
        service = self.service
        service.start_recording_runtime_stats()
        self.assertTrue(service.is_thread_alive)

    def test_thread_stopped_after_stop(self):
        """tests if thread stops after stopping recording"""
        service = self.service
        service.start_recording_runtime_stats()
        service.stop_recording_runtime_stats()
        self.assertFalse(service.is_thread_alive)

    def test_restart_before_stop_error(self):
        """tests if exception is raised if runtime stats service was restarted without being stopped first"""
        service = self.service
        service.start_recording_runtime_stats()
        with self.assertRaisesRegex(KnownException, ".*already running.*"):
            service.start_recording_runtime_stats()


if __name__ == '__main__':
    unittest.main()
