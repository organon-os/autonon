"""Includes tests for TransactionFileStats"""
import unittest
from unittest.mock import patch, MagicMock

from organon.afe.domain.modelling.businessobjects import transaction_file_stats
from organon.afe.domain.modelling.businessobjects.transaction_file_stats import TransactionFileStats


class TransactionFileStatsTestCase(unittest.TestCase):
    """Test class for TransactionFileStats"""

    def setUp(self) -> None:
        self.histogram_patcher = patch(transaction_file_stats.__name__ + ".Histogram16")
        self.mock_hist = self.histogram_patcher.start()

    def tearDown(self) -> None:
        self.histogram_patcher.stop()

    def test_add_get_histogram(self):
        """tests if histogram for added dimension can be retrieved with get_histogram"""
        first_hist = MagicMock()
        self.mock_hist.return_value = first_hist
        stats = TransactionFileStats()
        dim_col = "dimcol"
        stats.add(dim_col)
        hist = stats.get_histogram(dim_col)
        self.assertEqual(first_hist, hist)

    def test_add_same_dimension_twice(self):
        """tests if old histogram is replaced by new histogram if add is called twice for same dimension"""
        first_hist = MagicMock()
        self.mock_hist.return_value = first_hist
        stats = TransactionFileStats()
        dim_col = "dimcol"
        stats.add(dim_col)
        hist = stats.get_histogram(dim_col)
        second_mock = MagicMock()
        self.mock_hist.return_value = second_mock
        stats.add(dim_col)
        hist2 = stats.get_histogram(dim_col)
        self.assertEqual(hist, hist2)  # old histogram should not be replaced

    def test_get_histogram_exception(self):
        """tests if exception is raised when get_histogram is called for a dimension which had not been added"""
        stats = TransactionFileStats()
        stats.add("dimcol")
        with self.assertRaises(KeyError):
            stats.get_histogram("dummy_col")

    def test_build_indices(self):
        """tests build_indices"""
        stats = TransactionFileStats()
        dims = ["dim_1", "dim2", "dim_3"]
        magic_mocks = []
        for dim in dims:
            hist_mock = MagicMock()
            self.mock_hist.return_value = hist_mock
            stats.add(dim)
            magic_mocks.append(hist_mock)
        stats.build_indices(0.4, 200)
        for mock in magic_mocks:
            mock.build_indices_with_params.assert_called_with(0.4, 200)

    def test_increment(self):
        """tests increment"""
        stats = TransactionFileStats()
        dims = ["dim_1", "dim2", "dim_3"]
        magic_mocks = []
        for dim in dims:
            hist_mock = MagicMock()
            self.mock_hist.return_value = hist_mock
            stats.add(dim)
            magic_mocks.append(hist_mock)
        stats.increment(dims[1], "colval", 200)
        magic_mocks[1].add.assert_called_with("colval", 200)

    def test_get_index(self):
        """tests get_index"""
        stats = TransactionFileStats()
        mock_hist = MagicMock()
        self.mock_hist.return_value = mock_hist
        stats.add("dimcol")
        mock_hist2 = MagicMock()
        self.mock_hist.return_value = mock_hist2
        stats.add("dimcol2")
        stats.get_index("dimcol2", "colval")
        mock_hist.get_index_value.assert_not_called()
        mock_hist2.get_index_value.assert_called_with("colval")

    def test_increment_exception(self):
        """tests if get_index raises exception if column had not been added"""
        stats = TransactionFileStats()
        stats.add("dimcol")
        with self.assertRaises(KeyError):
            stats.increment("wrong dim", "asdfasd", 200)

    def test_get_index_exception(self):
        """tests if get_index raises exception if column had not been added"""
        stats = TransactionFileStats()
        stats.add("dimcol")
        with self.assertRaises(KeyError):
            stats.get_index("wrong dim", "asdfasd")

    def test_build_indices_when_empty(self):
        """tests if no exception occurs when build_indices is called before adding any dimension"""
        # pylint: disable=no-self-use
        stats = TransactionFileStats()
        stats.build_indices(0.5, 200)


if __name__ == '__main__':
    unittest.main()
