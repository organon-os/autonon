"""Includes tests for TransactionFileRecordCollection"""
import unittest
from datetime import datetime
from random import Random

import numpy as np

from organon.afe.domain.modelling.businessobjects.transaction_file_record_collection import \
    TransactionFileRecordCollection
from organon.fl.core.helpers import date_helper


def get_col_map(values: list):
    """Generates column index map"""
    return {col: i for i, col in enumerate(values)}


class TransactionFileRecordCollectionTestCase(unittest.TestCase):
    """Test class for TransactionFileRecordCollection"""

    def setUp(self) -> None:
        date_cols = ["date_col_1", "date_col_2"]
        dim_cols = ["dim1", "dim2"]
        qty_cols = ["qty1", "qty2"]
        self.collection = TransactionFileRecordCollection(get_col_map(date_cols),
                                                          get_col_map(dim_cols), get_col_map(qty_cols), 1000)

    def test_actual_record_count(self):
        """tests actual_record_count property"""
        self.assertEqual(self.collection.actual_record_count, 0)  # test if zero when no records are added
        date_val = date_helper.get_date_as_integer(datetime(2010, 1, 1))
        count = 30
        for _ in range(count):
            self.collection.append("123123", [date_val, date_val], [1, 2], [1, 2])
        self.assertEqual(self.collection.actual_record_count, count)

    def test_append(self):
        """tests append """
        date_val = date_helper.get_date_as_integer(datetime(2010, 1, 1))
        entity_id = "123123"
        dates = [date_val, date_val]
        d_vals = [1, 2]
        q_vals = [3.0, 4.0]
        self.collection.append(entity_id, [date_val, date_val], q_vals, d_vals)
        self.assertIn(entity_id, self.collection.entity_index_map)
        self.assertEqual(len(self.collection.entity_index_map[entity_id]), 1)
        self.assertEqual(list(self.collection.dates[0]), dates)
        self.assertEqual(list(self.collection.d_arrays[0]), d_vals)
        self.assertEqual(list(self.collection.q_arrays[0]), q_vals)

        # test append date with same entity
        date_val2 = date_helper.get_date_as_integer(datetime(2005, 1, 1))
        dates2 = [date_val2, date_val2]
        d_vals2 = [4, 9]
        q_vals2 = [15.0, 20.0]
        self.collection.append(entity_id, dates2, q_vals2, d_vals2)
        self.assertEqual(len(self.collection.entity_index_map[entity_id]), 2)
        self.assertEqual(list(self.collection.dates[1]), dates2)
        self.assertEqual(list(self.collection.d_arrays[1]), d_vals2)
        self.assertEqual(list(self.collection.q_arrays[1]), q_vals2)

    def test_append_no_date(self):
        """tests append when there are no date columns"""
        dim_cols = ["dim1", "dim2"]
        qty_cols = ["qty1", "qty2"]
        collection = TransactionFileRecordCollection({}, get_col_map(dim_cols), get_col_map(qty_cols), 1000)
        collection.append("12345", [], [1, 2], [2, 3])
        self.assertEqual(list(collection.dates[0]), [])

    def test_sort(self):
        """tests sorting by date column"""
        entity_ids = [str(i) for i in range(10)]
        min_val = date_helper.get_date_as_integer(datetime(2010, 1, 1))
        max_val = date_helper.get_date_as_integer(datetime(2020, 1, 1))
        random = Random()
        for i in range(30):
            for entity_id in entity_ids:
                date1 = random.randint(min_val, max_val)
                date2 = random.randint(min_val, max_val)
                self.collection.append(entity_id, [date1, date2], [15.1, 20.4], [2, 4])
        self.collection.convert_entity_index_lists_to_array()
        date_col, date_col_index = list(self.collection.date_col_map.items())[0]
        self.collection.sort(date_col)
        # for all entities, check if entity index map indices are sorted according to corresponding dates
        for entity_id in entity_ids:
            indices = self.collection.entity_index_map[entity_id]
            for i in range(len(indices) - 1):
                l_i = indices[i]
                u_i = indices[i + 1]
                self.assertTrue(self.collection.dates[l_i][date_col_index] >=
                                self.collection.dates[u_i][date_col_index])

    def test_convert_entity_index_lists_to_array(self):
        """tests convert_entity_index_lists_to_array"""
        for i in range(5):
            entity_id = str(i)
            self.collection.append(entity_id, [341241, 1234123], [1, 2], [2, 4])
        self.collection.convert_entity_index_lists_to_array()
        for arr in self.collection.entity_index_map.values():
            self.assertIsInstance(arr, np.ndarray)


if __name__ == '__main__':
    unittest.main()
