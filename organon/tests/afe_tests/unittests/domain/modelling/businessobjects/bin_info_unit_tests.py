"""This module includes tests for BinInfo class."""
import math
import unittest

from organon.afe.domain.modelling.businessobjects.bin_info import BinInfo


class Bin:
    """Class for Bin object"""

    def __init__(self, count, _sum, sum_of_squares):
        self.count = count
        self.sum = _sum
        self.sum_of_squares = sum_of_squares


class BinInfoTestCase(unittest.TestCase):
    """Test class for BinInfo"""

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()

    def setUp(self) -> None:
        self.bin_info_no_param = BinInfo()
        # for (1,2,3,4,5,6,7,8,9,10)
        bin_info = Bin(10, 55, 385)
        self.bin_info_one_param = BinInfo(bin_info)
        self.bin_info_three_param = BinInfo(10, 55, 385)

    def test_average_no_params(self):
        """ Case 1 test for average for Bin Info without params"""

        average = self.bin_info_no_param.average
        self.assertTrue(math.isnan(average))

    def test_average_one_params(self):
        """ Case 2 test for average Bin Info with one params """

        expected_average = 0.18181818181818182
        average = self.bin_info_one_param.average
        self.assertEqual(expected_average, average,
                         f'Average is incorrect: expected value {expected_average} actual value {average}')

    def test_average_three_params(self):
        """ Case 3 test for average Bin Info with three params """

        expected_average = 0.18181818181818182
        average = self.bin_info_one_param.average
        self.assertEqual(expected_average, average,
                         f'Average is incorrect: expected value {expected_average} actual value {average}')

    def test_variance_no_params(self):
        """Case 1 test for variance Bin Info without params"""

        variance = self.bin_info_no_param.variance
        self.assertTrue(math.isnan(variance))

    def test_variance_one_params(self):
        """ Case 2 test for variance Bin Info with one params"""

        expected_variance = 38.46694214876033
        variance = self.bin_info_one_param.variance
        self.assertEqual(expected_variance, variance,
                         f'Average is incorrect: expected value {expected_variance} actual value {variance}')

    def test_variance_three_params(self):
        """ Case 3 test for variance Bin Info with three params"""

        expected_variance = 38.46694214876033
        variance = self.bin_info_one_param.variance
        self.assertEqual(expected_variance, variance,
                         f'Average is incorrect: expected value {expected_variance} actual value {variance}')

    def test_std_dev_no_params(self):
        """ Case 1 test for std dev Bin Info without params"""

        std_dev = self.bin_info_no_param.std_dev
        self.assertTrue(math.isnan(std_dev))

    def test_std_dev_one_params(self):
        """Case 2 test for std dev Bin Info with one params"""

        expected_std_dev = 6.202172373351158
        std_dev = self.bin_info_one_param.std_dev
        self.assertEqual(expected_std_dev, std_dev,
                         f'Std dev is incorrect: expected value {expected_std_dev} actual value {std_dev}')

    def test_std_dev_three_params(self):
        """ Case 3 test for std dev Bin Info with three params"""

        expected_std_dev = 6.202172373351158
        std_dev = self.bin_info_one_param.std_dev
        self.assertEqual(expected_std_dev, std_dev,
                         f'Std dev is incorrect: expected value {expected_std_dev} actual value {std_dev}')

    def test_get_residual_sum_of_squares_no_params(self):
        """Case 1 test for  residual sum of squares Bin Info without params"""

        residual_sum_of_squares = self.bin_info_no_param.get_residual_sum_of_squares(5)
        self.assertTrue(math.isnan(residual_sum_of_squares))

    def test_get_residual_sum_of_squares_one_params(self):
        """ Case 2 test for  residual sum of squares Bin Info with one params"""

        expected_residual_sum_of_squares = 384.6727272727273
        residual_sum_of_squares = self.bin_info_one_param.get_residual_sum_of_squares(0.2)
        self.assertEqual(expected_residual_sum_of_squares, residual_sum_of_squares,
                         f'Residual sum of squares: expected value {expected_residual_sum_of_squares} '
                         f'actual value {residual_sum_of_squares}')

    def test_get_residual_sum_of_squares_params_and_without_pred(self):
        """Case 3 test for  residual sum of squares  Bin Info with three params"""

        expected_residual_sum_of_squares = 384.6694214876033
        residual_sum_of_squares = self.bin_info_one_param.get_residual_sum_of_squares()
        self.assertEqual(expected_residual_sum_of_squares, residual_sum_of_squares,
                         f'Residual sum of squares is incorrect: expected value {expected_residual_sum_of_squares} '
                         f'actual value {residual_sum_of_squares}')

    def test_overwrite(self):
        """Test for overwrite """

        bin_info = Bin(5, 15, 55)
        self.bin_info_no_param.overwrite(bin_info)
        expected_count = 5
        expected_sum = 15
        expected_sum_of_squares = 55
        count = self.bin_info_no_param.count
        _sum = self.bin_info_no_param.sum
        sum_of_squares = self.bin_info_no_param.sum_of_squares
        self.assertEqual(expected_count, count,
                         f'Count is incorrect: expected value {expected_count} '
                         f'actual value {count}')
        self.assertEqual(expected_sum, _sum,
                         f'Sum is incorrect: expected value {expected_sum} '
                         f'actual value {_sum}')
        self.assertEqual(expected_sum_of_squares, sum_of_squares,
                         f'Sum of squares is incorrect: expected value {expected_sum_of_squares} '
                         f'actual value {sum_of_squares}')

    def test_add_with_one_param(self):
        """Test for add with one parameter"""

        bin_info = Bin(10, 55, 385)
        self.bin_info_one_param.add(bin_info)
        expected_count = 20
        expected_sum = 110
        expected_sum_of_squares = 770
        count = self.bin_info_one_param.count
        _sum = self.bin_info_one_param.sum
        sum_of_squares = self.bin_info_one_param.sum_of_squares
        self.assertEqual(expected_count, count,
                         f'Count is incorrect: expected value {expected_count} '
                         f'actual value {count}')
        self.assertEqual(expected_sum, _sum,
                         f'Sum is incorrect: expected value {expected_sum} '
                         f'actual value {_sum}')
        self.assertEqual(expected_sum_of_squares, sum_of_squares,
                         f'Sum of squares is incorrect: expected value {expected_sum_of_squares} '
                         f'actual value {sum_of_squares}')

    def test_add_with_two_param(self):
        """Test for add with two parameters"""

        self.bin_info_one_param.add(10, 20)
        expected_count = 20
        expected_sum = 255
        expected_sum_of_squares = 4385
        count = self.bin_info_one_param.count
        _sum = self.bin_info_one_param.sum
        sum_of_squares = self.bin_info_one_param.sum_of_squares
        self.assertEqual(expected_count, count,
                         f'Count is incorrect: expected value {expected_count} '
                         f'actual value {count}')
        self.assertEqual(expected_sum, _sum,
                         f'Sum is incorrect: expected value {expected_sum} '
                         f'actual value {_sum}')
        self.assertEqual(expected_sum_of_squares, sum_of_squares,
                         f'Sum of squares is incorrect: expected value {expected_sum_of_squares} '
                         f'actual value {sum_of_squares}')

    def test_subtract_with_one_param(self):
        """Test for subtract with one parameter"""

        bin_info = Bin(5, 15, 55)
        self.bin_info_one_param.subtract(bin_info)
        expected_count = 5
        expected_sum = 40
        expected_sum_of_squares = 330
        count = self.bin_info_one_param.count
        _sum = self.bin_info_one_param.sum
        sum_of_squares = self.bin_info_one_param.sum_of_squares
        self.assertEqual(expected_count, count,
                         f'Count is incorrect: expected value {expected_count} '
                         f'actual value {count}')
        self.assertEqual(expected_sum, _sum,
                         f'Sum is incorrect: expected value {expected_sum} '
                         f'actual value {_sum}')
        self.assertEqual(expected_sum_of_squares, sum_of_squares,
                         f'Sum of squares is incorrect: expected value {expected_sum_of_squares} '
                         f'actual value {sum_of_squares}')

    def test_subtract_with_two_param(self):
        """Test for subtract with two parameters"""

        self.bin_info_one_param.subtract(5, 15)
        expected_count = 5
        expected_sum = -20
        expected_sum_of_squares = -740
        count = self.bin_info_one_param.count
        _sum = self.bin_info_one_param.sum
        sum_of_squares = self.bin_info_one_param.sum_of_squares
        self.assertEqual(expected_count, count,
                         f'Count is incorrect: expected value {expected_count} '
                         f'actual value {count}')
        self.assertEqual(expected_sum, _sum,
                         f'Sum is incorrect: expected value {expected_sum} '
                         f'actual value {_sum}')
        self.assertEqual(expected_sum_of_squares, sum_of_squares,
                         f'Sum of squares is incorrect: expected value {expected_sum_of_squares} '
                         f'actual value {sum_of_squares}')

    def test_subtract_with_three_param(self):
        """Test for subtract with three parameters"""

        self.bin_info_one_param.subtract(5, 15, 55)
        expected_count = 5
        expected_sum = -20
        expected_sum_of_squares = 330
        count = self.bin_info_one_param.count
        _sum = self.bin_info_one_param.sum
        sum_of_squares = self.bin_info_one_param.sum_of_squares
        self.assertEqual(expected_count, count,
                         f'Count is incorrect: expected value {expected_count} '
                         f'actual value {count}')
        self.assertEqual(expected_sum, _sum,
                         f'Sum is incorrect: expected value {expected_sum} '
                         f'actual value {_sum}')
        self.assertEqual(expected_sum_of_squares, sum_of_squares,
                         f'Sum of squares is incorrect: expected value {expected_sum_of_squares} '
                         f'actual value {sum_of_squares}')

    def test_get_metric(self):
        """Test for get metric """

        expected_metric = 302.5
        metric = self.bin_info_one_param.get_metric()
        self.assertEqual(expected_metric, metric,
                         f'Metric is incorrect: expected value {expected_metric} '
                         f'actual value {metric}')

    def test_distance_between(self):
        """Test for distance"""

        expected_distance = 0.007703468942236718
        bin_info = Bin(5, 15, 55)
        left = BinInfo(bin_info)
        distance = self.bin_info_one_param.distance_between(left, self.bin_info_one_param)
        self.assertEqual(expected_distance, distance,
                         f'Distance is incorrect: expected value {expected_distance} '
                         f'actual value {distance}')
