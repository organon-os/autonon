"""Includes unittests for DataFrameBuilder."""
import unittest
from datetime import datetime, timedelta
from typing import List
from unittest.mock import MagicMock, patch, PropertyMock

import numpy as np

from organon.afe.core.businessobjects.afe_static_objects import AfeStaticObjects
from organon.afe.domain.enums.afe_date_column_type import AfeDateColumnType
from organon.afe.domain.enums.afe_operator import AfeOperator
from organon.afe.domain.enums.afe_target_column_type import AfeTargetColumnType
from organon.afe.domain.enums.binary_target_class import BinaryTargetClass
from organon.afe.domain.enums.date_resolution import DateResolution
from organon.afe.domain.modelling.businessobjects import data_frame_builder
from organon.afe.domain.modelling.businessobjects.afe_column import AfeColumn
from organon.afe.domain.modelling.businessobjects.afe_feature import AfeFeature
from organon.afe.domain.modelling.businessobjects.data_frame_builder import DataFrameBuilder
from organon.afe.domain.modelling.businessobjects.target_file_record import TargetFileRecord
from organon.afe.domain.modelling.businessobjects.target_file_record_collection import TargetFileRecordCollection
from organon.afe.domain.modelling.businessobjects.transaction_file_record_collection import \
    TransactionFileRecordCollection
from organon.afe.domain.modelling.businessobjects.transaction_file_stats import TransactionFileStats
from organon.afe.domain.settings.afe_date_column import AfeDateColumn
from organon.afe.domain.settings.afe_target_column import AfeTargetColumn
from organon.afe.domain.settings.binary_target import BinaryTarget
from organon.afe.domain.settings.feature_generation_settings import FeatureGenerationSettings
from organon.afe.domain.settings.target_descriptor import TargetDescriptor
from organon.afe.domain.settings.temporal_grid import TemporalGrid
from organon.fl.core.businessobjects.histogram_16 import Histogram16
from organon.fl.core.exceptionhandling.known_exception import KnownException
from organon.fl.core.helpers import date_helper


def get_target_descriptor() -> TargetDescriptor:
    """return a mock TargetDescriptor instance"""
    target_descriptor = TargetDescriptor()
    target_descriptor.date_column = AfeDateColumn(column_name="target_date_col",
                                                  date_column_type=AfeDateColumnType.DateTime)
    target_descriptor.entity_column_name = "target_entity_col"
    target_descriptor.target_column = AfeTargetColumn(column_name="target_column",
                                                      target_column_type=AfeTargetColumnType.Binary,
                                                      binary_target_info=BinaryTarget(positive_category="P",
                                                                                      negative_category="N"))
    return target_descriptor


def get_target_collection():
    """Generate mock target collection for tests"""
    max_count = 10
    collection = TargetFileRecordCollection(max_count)
    event_date = datetime(2022, 1, 1)

    for i in range(max_count):
        record = TargetFileRecord()
        record.entity_id = str(i)
        record.event_date = date_helper.get_date_as_integer(event_date + timedelta(days=i))
        record.target_binary = BinaryTargetClass.POSITIVE if i % 2 == 0 else BinaryTargetClass.NEGATIVE
        record.target_scalar = 0.1 * i
        if i % 3 == 0:
            multi_class_value = "A"
        elif i % 3 == 1:
            multi_class_value = "B"
        else:
            multi_class_value = "C"
        record.target_multi_class = multi_class_value
        collection.append(record)
    return collection


def get_trx_collection():
    """Generate mock trx collection for tests"""
    max_count = 100
    date_cols = {col: i for i, col in enumerate(["datecol_1", "datecol_2"])}
    dim_cols = {col: i for i, col in enumerate([AfeStaticObjects.empty_dimension_column, "dim1", "dim2"])}
    qty_cols = {col: i for i, col in enumerate([AfeStaticObjects.empty_quantity_column, "qty1", "qty2"])}

    collection = TransactionFileRecordCollection(date_cols, dim_cols, qty_cols, max_count)
    event_date = datetime(2021, 12, 1)
    for i in range(max_count):
        date_1 = date_helper.get_date_as_integer(event_date + timedelta(days=i))
        date_2 = date_helper.get_date_as_integer(event_date + timedelta(days=i + 1))

        collection.append(str(i % 20), [date_1, date_2], [1.0, i, i + 1], [0, i, i + 1])

    trx_stats_mock = MagicMock(spec=TransactionFileStats)
    mock_histogram = MagicMock(spec=Histogram16)
    mock_histogram.original_to_compressed = {1: 1, 2: 2, 3: 3}
    mock_histogram.index = {"a": 1, "b": 2, "c": 3, "d": 4, "e": 5}
    mock_histogram.original_to_compressed = {1: 1, 2: 2, 3: 3, 4: -1, 5: -1}
    mock_histogram.compressed_reverse_index = {-1: ["d", "e"], 1: ["a"], 2: ["b"], 3: ["c"]}
    trx_stats_mock.get_histogram.return_value = mock_histogram
    collection.transaction_file_stats = trx_stats_mock
    collection.convert_entity_index_lists_to_array()
    collection.sort("datecol_1")
    return collection


def get_feature_generation_settings(no_date=False):
    """Generate mock feature generation settings for tests"""
    feature_generation_settings = FeatureGenerationSettings()

    if not no_date:
        feature_generation_settings.date_column = AfeDateColumn(column_name="datecol_1",
                                                                date_column_type=AfeDateColumnType.DateTime)
        feature_generation_settings.included_operators = [AfeOperator.Sum, AfeOperator.Min, AfeOperator.Max,
                                                          AfeOperator.TimeSinceFirst,
                                                          AfeOperator.TimeSinceLast, AfeOperator.Ratio,
                                                          AfeOperator.CountDistinct,
                                                          AfeOperator.Mode]
        feature_generation_settings.horizon_list = [11, 21, 1, 31]
        feature_generation_settings.date_offset = 0
        feature_generation_settings.temporal_grids = [TemporalGrid(offset=1, stride=31, length=4)]
    else:
        feature_generation_settings.temporal_grids = [TemporalGrid(offset=1, stride=1, length=1)]
        feature_generation_settings.horizon_list = [1]
        feature_generation_settings.included_operators = [AfeOperator.Sum, AfeOperator.Min, AfeOperator.Max,
                                                          AfeOperator.Ratio,
                                                          AfeOperator.CountDistinct,
                                                          AfeOperator.Mode]
    feature_generation_settings.date_resolution = DateResolution.Day
    feature_generation_settings.dimension_columns = ["dim1", "dim2"]
    feature_generation_settings.quantity_columns = ["qty1"]
    return feature_generation_settings


def get_afe_feature(feature_name="ft_1", dimension_name: str = "dim1", quantity_name: str = "qty1",
                    date_col_name: str = "datecol_1", operator=AfeOperator.Sum,
                    time_window: int = 1, _set: List[int] = None, in_out=True
                    ):
    """generates AfeFeature mock object"""
    # pylint: disable=too-many-arguments
    col = AfeColumn()
    col.dimension_name = dimension_name

    col.quantity_name = quantity_name
    col.operator = operator
    col.group_id = 1
    if _set is None:
        _set = [1]
    col.set = _set
    col.in_out = in_out

    if date_col_name is not None:
        col.date_column = AfeDateColumn(column_name=date_col_name, date_column_type=AfeDateColumnType.DateTime)
    col.date_resolution = DateResolution.Day
    col.offset = 0

    col.time_window = time_window
    col.source = "some source"
    feature = AfeFeature(col)
    feature.feature_name = feature_name
    return feature


class MockAfeColumn(AfeColumn):
    """Mock class for AfeColumn"""  # did not use magicmock to be able to use self in mocked methods

    def __init__(self):
        super().__init__()
        self.__column_name = None

    @property
    def column_name(self) -> str:
        return self.__column_name

    def set_column_name(self):
        date_col_name = self.date_column.column_name if self.date_column is not None else ""
        self.__column_name = f"{self.dimension_name}_{self.quantity_name}_{self.group_id}_" \
                             f"{self.operator.name}_{date_col_name}_{self.time_window}"

    def get_group_name(self) -> str:
        return f"{self.dimension_name}_{self.quantity_name}_{self.operator.name}" \
               f"_{self.time_window}"


class DataFrameBuilderTestCase(unittest.TestCase):
    """Unittest class or DataFrameBuilder"""

    @classmethod
    def setUpClass(cls) -> None:
        AfeStaticObjects.set_defaults()
        mock_afe_column = patch(data_frame_builder.__name__ + "." + AfeColumn.__name__).start()
        mock_afe_column.side_effect = MockAfeColumn  # calls MockAfeColumn when AfeColumn() is called in df_builder

    @classmethod
    def tearDownClass(cls) -> None:
        patch.stopall()

    def setUp(self) -> None:
        self.trx_collection = get_trx_collection()
        self.target_collection = get_target_collection()
        self.target_descriptor = get_target_descriptor()
        self.feature_generation_settings = [get_feature_generation_settings()]
        self.df_builder = self.get_df_builder()
        self.mock_df_builder_helper_patcher = patch(data_frame_builder.__name__ + ".data_frame_builder_helper")
        self.mock_df_builder_helper = self.mock_df_builder_helper_patcher.start()

    def tearDown(self) -> None:
        self.mock_df_builder_helper_patcher.stop()

    def get_df_builder(self):
        """Generates dataframe builder"""
        all_dimensions, all_horizons = {}, {}
        for order, feature_generation_settings in enumerate(self.feature_generation_settings):
            all_dimensions[order] = set(feature_generation_settings.dimension_columns)
            all_horizons[order] = set(feature_generation_settings.horizon_list)

        return DataFrameBuilder(self.target_descriptor, self.target_collection, self.trx_collection,
                                dimensions_per_date_col=all_dimensions,
                                horizons_per_date_col=all_horizons)

    def test_init_with_max_num_of_columns_missing_exception(self):
        """Check if exception is raised for missing arguments on DataFrameBuilder initializatiion"""
        with self.assertRaisesRegex(KnownException,
                                    ".*dimensions_per_date_col.*"):
            DataFrameBuilder(self.target_descriptor, self.target_collection, self.trx_collection)

    def test_generated_afe_columns(self):
        """Tests if expected afe columns are generated"""
        self.df_builder.execute(8, "dim1", "qty1", self.feature_generation_settings[0])
        frame = self.df_builder.frame
        column_names = frame.get_column_names()
        count = 0

        for group_id in self.trx_collection.transaction_file_stats.get_histogram("dim1").compressed_reverse_index:
            for operator in [AfeOperator.Min, AfeOperator.Max, AfeOperator.Sum]:
                for horizon in self.feature_generation_settings[0].horizon_list:
                    self.assertIn(f"dim1_qty1_{group_id}_{operator.name}_datecol_1_{horizon}", column_names)
                    count += 1

            self.assertIn(f"dim1_qty1_{group_id}_TimeSinceFirst_datecol_1_31", column_names)
            self.assertIn(f"dim1_qty1_{group_id}_TimeSinceLast_datecol_1_31", column_names)
            count += 2

            for horizon in self.feature_generation_settings[0].horizon_list:
                self.assertIn(f"dim1_qty1_{group_id}_Ratio_datecol_1_{horizon}", column_names)
                count += 1

        self.assertEqual(count, len(column_names))

    def test_noq_operators(self):
        """test if columns for count distinct and mode operators are included when qty column is NoQuantity"""
        self.df_builder.execute(8, "dim1", AfeStaticObjects.empty_quantity_column,
                                self.feature_generation_settings[0])
        column_names = self.df_builder.frame.get_column_names()
        for group_id in self.trx_collection.transaction_file_stats.get_histogram("dim1").compressed_reverse_index:
            for operator in [AfeOperator.CountDistinct, AfeOperator.Mode]:
                for horizon in self.feature_generation_settings[0].horizon_list:
                    self.assertIn(f"dim1_{AfeStaticObjects.empty_quantity_column}_"
                                  f"{group_id}_{operator.name}_datecol_1_{horizon}", column_names)

    def test_columns_no_date(self):
        """Check if columns for some operators are not generated if date column is None"""
        self.df_builder.execute(8, "dim1", "qty1", get_feature_generation_settings(True))
        afe_columns = self.df_builder.name_to_column.values()
        cols_to_test = [AfeOperator.TimeSinceFirst, AfeOperator.TimeSinceLast]
        for col in afe_columns:
            self.assertNotIn(col.operator, cols_to_test)

    def builder_helper_execute_arg_test(self, arg_index: int, val, for_afe_columns=False):
        """helper method for testing args values of data_frame_builder_helper.execute call"""
        if for_afe_columns:
            features = {
                "ft1": get_afe_feature("ft1", "dim1", "qty1", "datecol_1", AfeOperator.Density, 1),
            }
            self.df_builder.execute_for_afe_columns(8, features)
        else:
            self.df_builder.execute(8, "dim1", "qty1", self.feature_generation_settings[0])
        args = self.mock_df_builder_helper.execute.call_args.args
        self.assertEqual(val, args[arg_index])

    def test_builder_helper_args_date_column_index(self):
        """checks date column index value on data_Frame_builder_helper call"""
        self.builder_helper_execute_arg_test(2, 0)

    def test_builder_helper_args_date_column_index_no_date(self):
        """date column index should be -1 if date column is None"""
        self.df_builder.execute(8, "dim1", "qty1", get_feature_generation_settings(True))
        args = self.mock_df_builder_helper.execute.call_args.args
        self.assertEqual(args[2], -1)

    def test_builder_helper_args_dim_col_index(self):
        """checks dimension column index value on data_Frame_builder_helper call"""
        self.builder_helper_execute_arg_test(3, 1)

    def test_builder_helper_args_qty_col_index(self):
        """checks quantity column index value on data_Frame_builder_helper call"""
        self.builder_helper_execute_arg_test(4, 1)

    def test_builder_helper_args_horizons(self):
        """checks horizons value on data_Frame_builder_helper call"""
        self.builder_helper_execute_arg_test(6, [1, 11, 21, 31])  # should be sorted

    def test_builder_helper_args_nearest_exponent(self):
        """checks nearest_exponent value on data_Frame_builder_helper call"""
        feature_gen_settings = self.feature_generation_settings[0]
        feature_gen_settings.horizon_list = [1, 1000]
        self.df_builder.execute(8, "dim1", "qty1", feature_gen_settings)
        args = self.mock_df_builder_helper.execute.call_args.args
        self.assertEqual(args[19], 10000)

        feature_gen_settings.horizon_list = [1, 21, 31, 41]
        trx_stats_mock: MagicMock = self.trx_collection.transaction_file_stats
        trx_stats_mock.get_histogram("dim1").original_to_compressed = {i: i for i in range(110)}
        self.get_df_builder().execute(8, "dim1", "qty1", feature_gen_settings)
        args = self.mock_df_builder_helper.execute.call_args.args
        self.assertEqual(args[19], 1000)

    def test_builder_helper_args_hash_column_map(self):
        """checks hash_column_map value on data_Frame_builder_helper call"""
        self.feature_generation_settings[0].included_operators = [AfeOperator.Sum]
        self.feature_generation_settings[0].horizon_list = [5]
        mock_histogram = self.trx_collection.transaction_file_stats.get_histogram("dim1")
        mock_histogram.original_to_compressed = {1: 1}
        mock_histogram.index = {"a": 1}
        mock_histogram.original_to_compressed = {1: 1}
        mock_histogram.compressed_reverse_index = {1: ["a"]}

        self.get_df_builder().execute(8, "dim1", "qty1", self.feature_generation_settings[0])
        args = self.mock_df_builder_helper.execute.call_args.args
        hash_val = next(iter(args[18].keys()))
        # 01->group_id, 03->AfeOperator.Sum.value, 05->horizon
        # all in two digits like 01 since nearest_exponent is 100
        self.assertEqual(10305, hash_val)

    def test_splits(self):
        """Check if splits are created properly"""
        expected_split = np.array([[0, 5], [1, 6], [2, 7], [3, 8], [4, 9]])
        self.df_builder.execute(5, "dim1", "qty1", self.feature_generation_settings[0])
        args = self.mock_df_builder_helper.execute.call_args.args
        splits = args[5]
        np.testing.assert_equal(expected_split, splits)

    def test_splits_filled_with_dummy_val(self):
        """Check if splits are filled with -1 when there are no corresponding record for given indices"""
        expected_split = np.array([[0, 8], [1, 9], [2, -1], [3, -1], [4, -1], [5, -1], [6, -1], [7, -1]])
        self.df_builder.execute(8, "dim1", "qty1", self.feature_generation_settings[0])
        args = self.mock_df_builder_helper.execute.call_args.args
        splits = args[5]
        np.testing.assert_equal(expected_split, splits)

    @patch.object(TargetFileRecordCollection, "sampled", new_callable=PropertyMock)
    @patch.object(TargetFileRecordCollection, "sampled_count", new_callable=PropertyMock)
    @patch.object(TargetFileRecordCollection, "indices", new_callable=PropertyMock)
    def test_splits_sampled(self, mock_indices, mock_sampled_count, mock_sampled):
        """Check if splits are created properly when target is sampled"""
        expected_split = np.array([[1, 8], [3, -1], [4, -1], [5, -1], [7, -1]])
        mock_sampled.return_value = True
        mock_sampled_count.return_value = 6
        mock_indices.return_value = [1, 3, 4, 5, 7, 8]

        self.df_builder.execute(5, "dim1", "qty1", self.feature_generation_settings[0])
        args = self.mock_df_builder_helper.execute.call_args.args
        splits = args[5]
        np.testing.assert_equal(expected_split, splits)

    def test_index_to_id_and_date(self):
        """checks values in index_to_id and index_to_date of DataFrameBuilder"""
        self.df_builder.execute(8, "dim1", "qty1", self.feature_generation_settings[0])
        for i in range(10):
            self.assertEqual(str(i), self.df_builder.index_to_id[i])
            self.assertEqual(date_helper.get_date_as_integer(datetime(2022, 1, 1) + timedelta(days=i)),
                             self.df_builder.index_to_date[i])

    @patch.object(TargetFileRecordCollection, "sampled", new_callable=PropertyMock)
    @patch.object(TargetFileRecordCollection, "sampled_count", new_callable=PropertyMock)
    @patch.object(TargetFileRecordCollection, "indices", new_callable=PropertyMock)
    def test_index_to_id_and_date_sampled(self, mock_indices, mock_sampled_count, mock_sampled):
        """checks values in index_to_id and index_to_date of DataFrameBuilder when target is sampled"""
        mock_sampled.return_value = True
        mock_sampled_count.return_value = 6
        indices = [1, 3, 4, 5, 7, 8]
        mock_indices.return_value = indices

        self.df_builder.execute(8, "dim1", "qty1", self.feature_generation_settings[0])

        for i, index in enumerate(indices):
            self.assertEqual(str(index), self.df_builder.index_to_id[i])
            self.assertEqual(date_helper.get_date_as_integer(datetime(2022, 1, 1) + timedelta(days=index)),
                             self.df_builder.index_to_date[i])

    def test_new_entity_index_map(self):
        """Check if data_frame_builder_helper is called with correct new_entity_index_map value"""
        expected_map = {}
        for i in range(10):
            expected_map[i] = [i + (j - 1) * 20 for j in range(5, 0, -1)]
            # trx_collection i%20 şeklinde oluşturulduğu için
        self.df_builder.execute(8, "dim1", "qty1", self.feature_generation_settings[0])
        args = self.mock_df_builder_helper.execute.call_args.args
        new_map = args[13]
        for key, value in expected_map.items():
            self.assertListEqual(new_map[key].tolist(), value)
        self.assertEqual(len(expected_map), len(new_map))

    def test_target_array_for_binary_target(self):
        """checks values in target_array of df builder"""
        self.df_builder.execute(8, "dim1", "qty1", self.feature_generation_settings[0])
        self.assertEqual(10, len(self.df_builder.target_array))
        for i in range(10):
            if i % 2 == 0:
                self.assertEqual("P", self.df_builder.target_array[i])
            else:
                self.assertEqual("N", self.df_builder.target_array[i])

    @patch.object(TargetFileRecordCollection, "sampled", new_callable=PropertyMock)
    @patch.object(TargetFileRecordCollection, "sampled_count", new_callable=PropertyMock)
    @patch.object(TargetFileRecordCollection, "indices", new_callable=PropertyMock)
    def test_target_array_for_binary_target_sampled(self, mock_indices, mock_sampled_count, mock_sampled):
        """checks values in target_array of df builder when target is sampled"""
        mock_sampled.return_value = True
        mock_sampled_count.return_value = 6
        indices = [1, 3, 4, 5, 7, 8]
        mock_indices.return_value = indices
        self.df_builder.execute(8, "dim1", "qty1", self.feature_generation_settings[0])
        self.assertEqual(6, len(self.df_builder.target_array))
        for i, index in enumerate(indices):
            if index % 2 == 0:
                self.assertEqual("P", self.df_builder.target_array[i])
            else:
                self.assertEqual("N", self.df_builder.target_array[i])

    def test_target_array_for_scalar_target(self):
        """checks values in target_array of df builder when target is scalar"""
        self.target_descriptor.target_column.target_column_type = AfeTargetColumnType.Scalar
        self.df_builder.execute(8, "dim1", "qty1", self.feature_generation_settings[0])
        self.assertEqual(10, len(self.df_builder.target_array))
        for i in range(10):
            self.assertEqual(i * 0.1, self.df_builder.target_array[i])

    @patch.object(TargetFileRecordCollection, "sampled", new_callable=PropertyMock)
    @patch.object(TargetFileRecordCollection, "sampled_count", new_callable=PropertyMock)
    @patch.object(TargetFileRecordCollection, "indices", new_callable=PropertyMock)
    def test_target_array_for_scalar_target_sampled(self, mock_indices, mock_sampled_count, mock_sampled):
        """checks values in target_array of df builder when target is scalar and sampled"""
        self.target_descriptor.target_column.target_column_type = AfeTargetColumnType.Scalar
        mock_sampled.return_value = True
        mock_sampled_count.return_value = 6
        indices = [1, 3, 4, 5, 7, 8]
        mock_indices.return_value = indices
        self.df_builder.execute(8, "dim1", "qty1", self.feature_generation_settings[0])
        self.assertEqual(6, len(self.df_builder.target_array))
        for i, index in enumerate(indices):
            self.assertEqual(index * 0.1, self.df_builder.target_array[i])

    def test_target_array_for_multiclass_target(self):
        """checks values in target_array of df builder when target is multiclass"""
        self.target_descriptor.target_column.target_column_type = AfeTargetColumnType.MultiClass
        self.df_builder.execute(8, "dim1", "qty1", self.feature_generation_settings[0])
        self.assertEqual(10, len(self.df_builder.target_array))
        for index in range(10):
            if index % 3 == 0:
                self.assertEqual("A", self.df_builder.target_array[index])
            elif index % 3 == 1:
                self.assertEqual("B", self.df_builder.target_array[index])
            else:
                self.assertEqual("C", self.df_builder.target_array[index])

    @patch.object(TargetFileRecordCollection, "sampled", new_callable=PropertyMock)
    @patch.object(TargetFileRecordCollection, "sampled_count", new_callable=PropertyMock)
    @patch.object(TargetFileRecordCollection, "indices", new_callable=PropertyMock)
    def test_target_array_for_multiclass_target_sampled(self, mock_indices, mock_sampled_count, mock_sampled):
        """checks values in target_array of df builder when target is multiclass and sampled"""
        self.target_descriptor.target_column.target_column_type = AfeTargetColumnType.MultiClass
        mock_sampled.return_value = True
        mock_sampled_count.return_value = 6
        indices = [1, 3, 4, 5, 7, 8]
        mock_indices.return_value = indices
        self.df_builder.execute(8, "dim1", "qty1", self.feature_generation_settings[0])
        self.assertEqual(6, len(self.df_builder.target_array))
        for i, index in enumerate(indices):
            if index % 3 == 0:
                self.assertEqual("A", self.df_builder.target_array[i])
            elif index % 3 == 1:
                self.assertEqual("B", self.df_builder.target_array[i])
            else:
                self.assertEqual("C", self.df_builder.target_array[i])

    def test_exec_for_afe_columns_column_names(self):
        """Tests frame column names after execute_for_afe_columns call."""
        features = {
            "ft1": get_afe_feature("ft1", "dim1", "qty1", "datecol_1", AfeOperator.Max),
            "ft2": get_afe_feature("ft1", "dim1", "qty1", "datecol_2", AfeOperator.Sum, 1),
            "ft3": get_afe_feature("ft1", "dim2", AfeStaticObjects.empty_quantity_column,
                                   "datecol_1", AfeOperator.CountDistinct),
            "ft4": get_afe_feature("ft1", "dim2", "qty2", "datecol_2", AfeOperator.Ratio),
        }
        self.df_builder.execute_for_afe_columns(8, features)
        columns = self.df_builder.frame.get_column_names()
        self.assertIn("ft1", columns)
        self.assertIn("ft2", columns)
        self.assertIn("ft3", columns)
        self.assertIn("ft4", columns)
        self.assertEqual(4, len(columns))

    def test_exec_for_afe_columns_included_operators(self):
        """only the operator of afe column should be in included_operators on data_frame_builder_helper call"""
        features = {
            "ft1": get_afe_feature("ft1", "dim1", "qty1", "datecol_1", AfeOperator.Max),
        }
        self.df_builder.execute_for_afe_columns(8, features)
        args = self.mock_df_builder_helper.execute.call_args.args
        self.assertEqual(args[15], [AfeOperator.Max.value])  # included_operators
        self.assertEqual(args[16], [AfeOperator.Max.value])  # included_operators_set_one
        self.assertEqual(args[17], [])  # included_operators_set_two

    def test_exec_for_afe_columns_included_operators_for_time_since(self):
        """only the operator of afe column should be in included_operators on data_frame_builder_helper call"""
        features = {
            "ft1": get_afe_feature("ft1", "dim1", "qty1", "datecol_1", AfeOperator.TimeSinceFirst),
        }
        self.df_builder.execute_for_afe_columns(8, features)
        args = self.mock_df_builder_helper.execute.call_args.args
        self.assertEqual(args[15], [AfeOperator.TimeSinceFirst.value])  # included_operators
        self.assertEqual(args[16], [])  # included_operators_set_one
        self.assertEqual(args[17], [AfeOperator.TimeSinceFirst.value])  # included_operators_set_two

    def test_exec_for_afe_columns_trx_collection_sort_called(self):
        """tests if trx_collection.sort is called for every distinct date column"""
        self.trx_collection.sort = MagicMock()
        features = {
            "ft1": get_afe_feature("ft1", "dim1", "qty1", "datecol_1", AfeOperator.Density, 1),
            "ft2": get_afe_feature("ft2", "dim1", "qty1", "datecol_2", AfeOperator.Sum, 1),
            "ft3": get_afe_feature("ft3", "dim1", "qty1", None, AfeOperator.Min, 1)
        }
        self.df_builder.execute_for_afe_columns(8, features)
        date_col_args = [call_args.args[0] for call_args in self.trx_collection.sort.call_args_list]
        self.assertCountEqual(["datecol_2", "datecol_1"], date_col_args)

    def test_exec_for_afe_columns_new_entity_index_map(self):
        """Check if data_frame_builder_helper is called with correct new_entity_index_map value"""
        expected_map = {}
        for i in range(10):
            expected_map[i] = [i + (j - 1) * 20 for j in range(5, 0, -1)]
            # trx_collection i%20 şeklinde oluşturulduğu için
        features = {
            "ft1": get_afe_feature("ft1", "dim1", "qty1", "datecol_1", AfeOperator.Density, 1),
        }
        self.df_builder.execute_for_afe_columns(8, features)
        args = self.mock_df_builder_helper.execute.call_args.args
        new_map = args[13]
        for key, value in expected_map.items():
            self.assertListEqual(new_map[key].tolist(), value)
        self.assertEqual(len(expected_map), len(new_map))

    def test_exec_for_afe_column_builder_helper_args_date_column_index(self):
        """checks date column index value on data_Frame_builder_helper call"""
        self.builder_helper_execute_arg_test(2, 0, True)

    def test_exec_for_afe_column_builder_helper_args_date_column_index_no_date(self):
        """date column index should be -1 if date column is None"""
        features = {
            "ft1": get_afe_feature("ft1", "dim1", "qty1", None, AfeOperator.Density, 1),
        }
        self.df_builder.execute_for_afe_columns(8, features)
        args = self.mock_df_builder_helper.execute.call_args.args
        self.assertEqual(args[2], -1)


if __name__ == '__main__':
    unittest.main()
