"""
This module includes test class for MultiTargetEntityContainer.
"""
import unittest
from unittest.mock import patch

from organon.afe.domain.enums.binary_target_class import BinaryTargetClass
from organon.afe.domain.enums.date_resolution import DateResolution
from organon.afe.domain.modelling.businessobjects import multi_target_entity_container
from organon.afe.domain.modelling.businessobjects.multi_target_entity_container import MultiTargetEntityContainer
from organon.afe.domain.modelling.businessobjects.target_file_record import TargetFileRecord
from organon.afe.domain.modelling.businessobjects.target_file_record_collection import TargetFileRecordCollection
from organon.afe.domain.settings.record_source import RecordSource


def _get_date_subtract_func_side_effect(resolution, delta_val):
    # pylint: disable=unused-argument
    return lambda date: date - delta_val


def _get_target_file_record(entity_id: str, event_date: int):
    record = TargetFileRecord()
    record.entity_id = entity_id
    record.event_date = event_date
    record.target_binary = BinaryTargetClass.POSITIVE
    return record


class MultiTargetEntityContainerTestCase(unittest.TestCase):
    """Test class for MultiTargetEntityContainer"""

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()

    def setUp(self) -> None:
        self.target_file_record_collection_patcher = patch(multi_target_entity_container.__name__ + "."
                                                           + TargetFileRecordCollection.__name__)
        self.mock_target_file_record_collection_class = self.target_file_record_collection_patcher.start()

        self.collection_1 = self.mock_target_file_record_collection_class(1000)
        self.collection_1.get_sampled_unique_entities.return_value = ['abc', 'def', 'ghi', 'klm', 'nop']
        self.mock_target_file_record_collection_class_2 = self.target_file_record_collection_patcher.start()

        self.collection_2 = self.mock_target_file_record_collection_class_2(300)
        self.collection_2.get_sampled_unique_entities.return_value = ['abc', 'def', 'hyk', 'org', 'only']

        self.record_source = RecordSource(source="file1")
        self.record_source_2 = RecordSource(source="file2")

        self.multi_target_entity_container_service = MultiTargetEntityContainer()
        afe_date_helper_patcher = patch(multi_target_entity_container.__name__ + ".afe_date_helper")
        self.mock_afe_date_helper = afe_date_helper_patcher.start()
        self.addCleanup(afe_date_helper_patcher.stop)

    def tearDown(self) -> None:
        self.target_file_record_collection_patcher.stop()

    def test_get_records_per_file_for_empty_container(self):
        """Tests records_per_file property when container is empty"""
        records_per_file_dict = self.multi_target_entity_container_service.records_per_file
        self.assertEqual(len(records_per_file_dict), 0)

    def test_get_records_per_file(self):
        """Tests records_per_file property when container is not empty"""
        self.multi_target_entity_container_service.add(self.record_source, self.collection_1)
        records_per_file_dict = self.multi_target_entity_container_service.records_per_file
        self.assertEqual(len(records_per_file_dict), 1)
        self.multi_target_entity_container_service.add(self.record_source_2, self.collection_2)
        records_per_file_dict_2 = self.multi_target_entity_container_service.records_per_file
        self.assertEqual(len(records_per_file_dict_2), 2)

    def test_get_unique_entity_list_empty(self):
        """Tests unique_entity_list property when unique_entity_list is empty"""
        unique_entity_list = self.multi_target_entity_container_service.unique_entity_list
        self.assertEqual(len(unique_entity_list), 0)

    def test_get_unique_entity_list(self):
        """Tests unique_entity_list property when unique_entity_list is not empty"""
        self.multi_target_entity_container_service.add(self.record_source, self.collection_2)
        self.multi_target_entity_container_service.add(self.record_source_2, self.collection_1)
        self.multi_target_entity_container_service.unify_entity_list(100)
        unique_entity_list = self.multi_target_entity_container_service.unique_entity_list
        self.assertEqual(len(unique_entity_list), 8)

    def test_add(self):
        """Test for add"""
        records_per_file_dict = self.multi_target_entity_container_service.records_per_file
        self.assertEqual(len(records_per_file_dict), 0)
        self.multi_target_entity_container_service.add(self.record_source, self.collection_1)
        self.multi_target_entity_container_service.add(self.record_source_2, self.collection_2)
        records_per_file_dict_2 = self.multi_target_entity_container_service.records_per_file
        self.assertEqual(len(records_per_file_dict_2), 2)

    def test_remove(self):
        """Test for remove"""
        self.multi_target_entity_container_service.add(self.record_source, self.collection_1)
        self.multi_target_entity_container_service.add(self.record_source_2, self.collection_2)
        records_per_file_dict_1 = self.multi_target_entity_container_service.records_per_file
        self.assertEqual(len(records_per_file_dict_1), 2)
        self.multi_target_entity_container_service.remove(self.record_source)
        records_per_file_dict_2 = self.multi_target_entity_container_service.records_per_file
        self.assertEqual(len(records_per_file_dict_2), 1)
        self.multi_target_entity_container_service.remove(self.record_source_2)
        records_per_file_dict_3 = self.multi_target_entity_container_service.records_per_file
        self.assertEqual(len(records_per_file_dict_3), 0)

    def test_unify_entity_list(self):
        """Test for unify entity list"""
        self.multi_target_entity_container_service.add(self.record_source, self.collection_2)
        self.multi_target_entity_container_service.add(self.record_source_2, self.collection_1)
        self.multi_target_entity_container_service.unify_entity_list(100)
        unique_entity_list = self.multi_target_entity_container_service.unique_entity_list
        self.assertEqual(len(unique_entity_list), 8)

    def test_get_date_interval_per_entity(self):
        """Test for get date interval per entity"""
        target_record_collection_1 = TargetFileRecordCollection(100)
        target_record_collection_1.append(_get_target_file_record("1", 0))
        target_record_collection_1.append(_get_target_file_record("1", 5))
        target_record_collection_1.append(_get_target_file_record("1", 2))
        target_record_collection_1.append(_get_target_file_record("2", 3))
        target_record_collection_1.append(_get_target_file_record("2", 3))
        target_record_collection_1.append(_get_target_file_record("2", 1))

        self.multi_target_entity_container_service.add(self.record_source, target_record_collection_1)
        self.mock_afe_date_helper.get_date_subtraction_func.side_effect = _get_date_subtract_func_side_effect
        date_interval_per_entity = self.multi_target_entity_container_service.get_date_interval_per_entity(
            DateResolution.Day, 31, 2)
        self.assertEqual(2, len(date_interval_per_entity))

        self.assertEqual(-33, date_interval_per_entity["1"].lower_bound)
        self.assertEqual(3, date_interval_per_entity["1"].upper_bound)
        self.assertEqual(-32, date_interval_per_entity["2"].lower_bound)
        self.assertEqual(1, date_interval_per_entity["2"].upper_bound)
