"""Includes tests for TargetFileRecordCollection."""
import unittest
from datetime import datetime
from unittest.mock import MagicMock

from organon.afe.domain.enums.binary_target_class import BinaryTargetClass
from organon.afe.domain.modelling.businessobjects.target_file_record import TargetFileRecord
from organon.afe.domain.modelling.businessobjects.target_file_record_collection import TargetFileRecordCollection
from organon.fl.core.helpers import date_helper


class TargetFileRecordCollectionTestCase(unittest.TestCase):
    """Test class for TargetFileRecordCollection"""

    def setUp(self) -> None:
        self.collection = TargetFileRecordCollection(1000)

    def test_actual_record_count_empty(self):
        """tests actual_record_count property when collection is empty"""
        self.assertEqual(0, self.collection.actual_record_count)

    def test_actual_record_count(self):
        """tests actual_record_count property after appends"""
        for i in range(10):
            record = TargetFileRecord()
            record.entity_id = i
            record.event_date = date_helper.get_date_as_integer(datetime(2010, 1, 1))
            record.target_binary = BinaryTargetClass.POSITIVE
            self.collection.append(record)
        self.assertEqual(10, self.collection.actual_record_count)

    def test_append(self):
        """tests append """
        record = TargetFileRecord()
        record.entity_id = "1"
        record.event_date = date_helper.get_date_as_integer(datetime(2010, 1, 1))
        record.target_binary = BinaryTargetClass.POSITIVE

        record2 = TargetFileRecord()
        record2.entity_id = "2"
        record2.event_date = date_helper.get_date_as_integer(datetime(2011, 1, 1))
        record2.target_binary = BinaryTargetClass.NEGATIVE

        self.collection.append(record)
        self.collection.append(record2)

        self.assertEqual(record.entity_id, self.collection.entities[0])
        self.assertEqual(record2.entity_id, self.collection.entities[1])
        self.assertEqual(record.event_date, self.collection.event_dates[0])
        self.assertEqual(record2.event_date, self.collection.event_dates[1])
        self.assertEqual(record.target_binary.value, self.collection.target_binary_values[0])
        self.assertEqual(record2.target_binary.value, self.collection.target_binary_values[1])

    def test_sampling(self):
        """tests get_sampled_unique_entities method"""
        self.__add_records_for_sampling_test()
        random_mock = MagicMock()
        random_mock.random.side_effect = [(i / 20) for i in range(20)]
        entities = self.collection.get_sampled_unique_entities(random_mock, 10)
        self.assertCountEqual([str(i) for i in range(10)], entities)

    def test_indices_after_sampling(self):
        """tests indices property value after calling get_sampled_unique_entities method"""
        self.__add_records_for_sampling_test()
        random_mock = MagicMock()
        random_mock.random.side_effect = [(i / 20) for i in range(20)]
        self.collection.get_sampled_unique_entities(random_mock, 10)
        self.assertCountEqual(list(range(10)), self.collection.indices)

    def test_sampled_property(self):
        """tests sampled property value after sampling"""
        self.assertFalse(self.collection.sampled)
        self.__add_records_for_sampling_test()
        random_mock = MagicMock()
        random_mock.random.side_effect = [(i / 20) for i in range(20)]
        self.collection.get_sampled_unique_entities(random_mock, 10)
        self.assertTrue(self.collection.sampled)

    def test_sampling_max_entities_higher_than_record_count(self):
        """tests get_sampled_unique_entities method when max_entities is higher than actual_record_count"""
        self.__add_records_for_sampling_test()
        random_mock = MagicMock()
        random_mock.random.side_effect = [(i / 20) for i in range(20)]
        entities = self.collection.get_sampled_unique_entities(random_mock, 30)
        self.assertCountEqual([str(i) for i in range(20)], entities)

    def test_sampled_property_max_entities_higher_than_record_count(self):
        """tests sampled property value after sampling with max_entities>actual_record_count"""
        self.__add_records_for_sampling_test()
        random_mock = MagicMock()
        random_mock.random.side_effect = [(i / 20) for i in range(20)]
        self.collection.get_sampled_unique_entities(random_mock, 30)
        self.assertFalse(self.collection.sampled)

    def test_indices_property_max_entities_higher_than_record_count(self):
        """tests indices property value after sampling with max_entities>actual_record_count"""
        self.__add_records_for_sampling_test()
        random_mock = MagicMock()
        random_mock.random.side_effect = [(i / 20) for i in range(20)]
        self.collection.get_sampled_unique_entities(random_mock, 30)
        self.assertIsNone(self.collection.indices)

    def __add_records_for_sampling_test(self):
        for i in range(20):
            record = TargetFileRecord()
            record.entity_id = str(i)
            record.event_date = date_helper.get_date_as_integer(datetime(2010, 1, 1))
            record.target_binary = BinaryTargetClass.POSITIVE
            self.collection.append(record)


if __name__ == '__main__':
    unittest.main()
