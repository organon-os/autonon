"""
This module includes test class for AfeColumn.
"""
import unittest

from organon.afe.domain.enums.afe_date_column_encoding_type import AfeDateColumnEncodingType
from organon.afe.domain.enums.afe_date_column_type import AfeDateColumnType
from organon.afe.domain.enums.afe_operator import AfeOperator
from organon.afe.domain.modelling.businessobjects.afe_column import AfeColumn
from organon.afe.domain.settings.afe_date_column import AfeDateColumn


def get_afe_column():
    """Return Afe Column"""
    afe_column = AfeColumn()
    afe_column.dimension_name = "DIM"
    date_column = AfeDateColumn()
    date_column.column_name = "SEGMENTATION_DATE"
    date_column.date_column_type = AfeDateColumnType.DateTime
    date_column.date_column_encoding_type = AfeDateColumnEncodingType.DateTime
    afe_column.date_column = date_column
    afe_column.quantity_name = "QTY"
    operator = AfeOperator(3)
    afe_column.operator = operator
    afe_column.group_id = 3
    afe_column.set = [1]
    afe_column.in_out = True
    afe_column.date_resolution = "Day"
    afe_column.offset = 1
    afe_column.time_window = 31
    return afe_column


def get_dimension_set_map():
    """Return dimension set map"""
    dimension_set_map = {}
    dimension_set_map[0] = "BS"
    dimension_set_map[1] = "BS1"
    dimension_set_map[2] = "BS2"
    dimension_set_map[3] = None
    return dimension_set_map


class AfeColumnUnitTests(unittest.TestCase):
    """
    Unit test class for AfeColumn.
    """

    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()

    def setUp(self) -> None:
        self.afe_column = get_afe_column()
        self.dimension_set_map = get_dimension_set_map()

    def test_set_and_get_column_name_with_date_column_and_without_trend_operator(self):
        """Check column name with date column and without trend operator"""

        self.afe_column.set_column_name()
        expected_column_name = "DIM_QTY_3_Sum_SEGMENTATION_DATE_31"
        column_name = self.afe_column.column_name
        self.assertEqual(expected_column_name, column_name,
                         f'Column name incorrect: expected value {expected_column_name} actual value {column_name}')

    def test_set_and_get_column_name_without_date_column_and_without_trend_operator(self):
        """Check column name with date column and without trend operator"""

        self.afe_column.date_column = None
        self.afe_column.set_column_name()
        expected_column_name = "DIM_QTY_3_Sum__31"
        column_name = self.afe_column.column_name
        self.assertEqual(expected_column_name, column_name,
                         f'Column name incorrect: expected value {expected_column_name} actual value {column_name}')

    def test_get_group_name(self):
        """Check column name with date column and without trend operator"""
        expected_group_name = "DIM_QTY_Sum_31"
        group_name = self.afe_column.get_group_name()
        self.assertEqual(expected_group_name, group_name,
                         f'Group name incorrect: expected value {expected_group_name} actual value {group_name}')

    def test_get_dimension_set_with_empty_map(self):
        """Test dimension set value with empty map"""

        expected_dimension_set = ""
        dimension_set = self.afe_column.get_dimension_set({})
        self.assertEqual(expected_dimension_set, dimension_set,
                         f'Dimension set incorrect: expected value {expected_dimension_set} ,'
                         f' actual value {dimension_set}')

    def test_get_dimension_set_with_set_1(self):
        """Test dimension set value with set 1 value"""

        expected_dimension_set = "BS1"
        dimension_set = self.afe_column.get_dimension_set(self.dimension_set_map)
        self.assertEqual(expected_dimension_set, dimension_set,
                         f'Dimension set incorrect: expected value {expected_dimension_set} ,'
                         f' actual value {dimension_set}')

    def test_get_dimension_set_with_set_1_and_2(self):
        """Test dimension set value with set 1 and 2 value"""

        self.afe_column.set = [1, 2]
        expected_dimension_set = "BS1&BS2"
        dimension_set = self.afe_column.get_dimension_set(self.dimension_set_map)
        self.assertEqual(expected_dimension_set, dimension_set,
                         f'Dimension set incorrect: expected value {expected_dimension_set}, '
                         f'actual value {dimension_set}')

    def test_get_dimension_set_with_in_out(self):
        """Test dimension set value for in_out parameter value False"""

        self.afe_column.in_out = False
        expected_dimension_set = "~BS1"
        dimension_set = self.afe_column.get_dimension_set(self.dimension_set_map)
        self.assertEqual(expected_dimension_set, dimension_set,
                         f'Dimension set incorrect: expected value {expected_dimension_set}, '
                         f'actual value {dimension_set}')

    def test_get_dimension_set_with_none_value(self):
        """Test dimension set value with set 1 and 2 value"""

        self.afe_column.set = [3]
        expected_dimension_set = "None"
        dimension_set = self.afe_column.get_dimension_set(self.dimension_set_map)
        self.assertEqual(expected_dimension_set, dimension_set,
                         f'Dimension set incorrect: expected value {expected_dimension_set}, '
                         f'actual value {dimension_set}')

    def test_build_column_name_for_list_value_with_time_window(self):
        """Test build column name for list value with time window"""
        expected_column_name = "DIM_QTY_1_3_Sum_31"
        column_name = self.afe_column.build_column_name_for_list_value([1, 3], self.afe_column.dimension_name,
                                                                       self.afe_column.quantity_name,
                                                                       self.afe_column.operator,
                                                                       self.afe_column.time_window)
        self.assertEqual(expected_column_name, column_name,
                         f'Column name incorrect: expected value {expected_column_name}, actual value {column_name}')

    def test_build_column_name_for_list_value_without_time_window(self):
        """Test build column name for list value with time window"""
        expected_column_name = "DIM_QTY_1_3_Sum"
        column_name = self.afe_column.build_column_name_for_list_value([1, 3], self.afe_column.dimension_name,
                                                                       self.afe_column.quantity_name,
                                                                       self.afe_column.operator, None)
        self.assertEqual(expected_column_name, column_name,
                         f'Column name incorrect: expected value {expected_column_name}, actual value {column_name}')

    def test_build_column_name_with_map_with_dim_and_without_trend_operator(self):
        """Test build column name with map for with dim and without trend operator"""

        expected_column_name = "DIM__BS1__QTY__Sum__31"
        column_name = self.afe_column.build_column_name_with_map(self.dimension_set_map)
        self.assertEqual(expected_column_name, column_name,
                         f'Column name incorrect: expected value {expected_column_name}, actual value {column_name}')

    def test_build_column_name_with_map_without_dim_and_without_trend_operator(self):
        """Test build column name with map for without dim and without trend operator"""

        expected_column_name = "DIM__QTY__Sum__31"
        column_name = self.afe_column.build_column_name_with_map({})
        self.assertEqual(expected_column_name, column_name,
                         f'Column name incorrect: expected value {expected_column_name}, actual value {column_name}')
