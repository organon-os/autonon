"""Includes tests for afe_settings_type_validators"""
import unittest
from datetime import datetime
from typing import Dict, List, Tuple
from unittest.mock import patch

import numpy as np

from organon.afe.domain.enums.afe_learning_type import AfeLearningType
from organon.afe.domain.modelling.supervised.afe_supervised_algorithm_settings import AfeSupervisedAlgorithmSettings
from organon.afe.domain.settings import afe_settings_type_validators
from organon.afe.domain.settings.afe_modelling_settings import AfeModellingSettings
from organon.afe.domain.settings.afe_scoring_settings import AfeScoringSettings
from organon.afe.domain.settings.afe_settings_type_validators import attribute_types_validate, \
    validate_types_scoring_settings, validate_types_modelling_settings
from organon.afe.domain.settings.record_source import RecordSource
from organon.fl.core.exceptionhandling.known_exception import KnownException


class _SomeClass:
    """Mock class"""

    def __init__(self):
        self.attr_1 = None
        self.attr_2 = None


class _ClassWithAttrDict:
    """Mock class with ATTR_DICT"""
    ATTR_DICT = {"int_attr": int, "str_attr": str}

    def __init__(self):
        self.int_attr = None
        self.str_attr = None


class AfeSettingsTypeValidatorsTestCase(unittest.TestCase):
    """Unittest class for afe_settings_type_validators module"""

    def test_attribute_types_validate_dict(self):
        """tests type validation for dictionary attribute"""
        some_obj = _SomeClass()
        some_obj.attr_1 = {"attr_1": 1, "attr_2": 2}
        try:
            attribute_types_validate(some_obj, {"attr_1": Dict[str, int]})
        except KnownException:
            self.fail()

    def test_attribute_types_validate_list(self):
        """tests type validation for list attribute"""
        some_obj = _SomeClass()
        some_obj.attr_1 = [1, 2, 3]
        try:
            attribute_types_validate(some_obj, {"attr_1": List[int]})
        except KnownException:
            self.fail()

    def test_attribute_types_validate_dict_in_dict(self):
        """tests type validation for dictionary in dictionary"""
        some_obj = _SomeClass()
        some_obj.attr_1 = {"some_key": {"attr_1": 1, "attr_2": 2}}
        try:
            attribute_types_validate(some_obj, {"attr_1": Dict[str, Dict[str, int]]}, obj_str="obj")
        except KnownException:
            self.fail()

    def test_attribute_types_validate_list_in_list(self):
        """tests type validation for list in list"""
        some_obj = _SomeClass()
        some_obj.attr_1 = [[1, 2, 3], [4, 5, 6]]
        try:
            attribute_types_validate(some_obj, {"attr_1": List[List[int]]}, obj_str="obj")
        except KnownException:
            self.fail()

    def test_attribute_types_validate_list_in_dict(self):
        """tests type validation for dictionary attribute"""
        some_obj = _SomeClass()
        some_obj.attr_1 = {"some_key": [1, 2, 3]}
        try:
            attribute_types_validate(some_obj, {"attr_1": Dict[str, List[int]]}, obj_str="obj")
        except KnownException:
            self.fail()

    def test_attribute_types_validate_dict_cast(self):
        """tests if keys or values are cast to expected types on type validation for dictionary attribute"""
        some_obj = _SomeClass()
        some_obj.attr_1 = {2: 1, "attr_2": "3", 3: "5"}
        attribute_types_validate(some_obj, {"attr_1": Dict[str, int]})
        self.assertEqual(3, len(some_obj.attr_1))
        self.assertIn("2", some_obj.attr_1)
        self.assertIn("attr_2", some_obj.attr_1)
        self.assertIn("3", some_obj.attr_1)
        self.assertEqual(3, some_obj.attr_1["attr_2"])
        self.assertEqual(5, some_obj.attr_1["3"])

    def test_attribute_types_validate_list_cast(self):
        """tests if elements are cast to expected types on type validation for list attribute"""
        some_obj = _SomeClass()
        some_obj.attr_1 = ["1", "2", "3"]
        attribute_types_validate(some_obj, {"attr_1": List[int]})
        self.assertEqual(3, len(some_obj.attr_1))
        self.assertListEqual([1, 2, 3], some_obj.attr_1)

    def test_attribute_types_validate_dict_key_error(self):
        """tests if exception is raised when key in dictionary is not of expected type and
        cannot be casted to expected type on type validation for dictionary attribute"""

        obj = _SomeClass()
        obj.attr_1 = {1: "b", "c": "d"}
        with self.assertRaisesRegex(KnownException, r"Type of \'obj\.attr_1\.keys\[1\]\' is wrong\. "
                                                    r"Expected: \<class \'numpy\.int16\'\> "
                                                    r"Got: \<class \'str\'\>\(Value: c\)"):
            attribute_types_validate(obj, {"attr_1": Dict[np.int16, str]}, obj_str="obj")

    def test_attribute_types_validate_dict_value_error(self):
        """tests if exception is raised when value in dictionary is not of expected type and
                cannot be casted to expected type on type validation for dictionary attribute"""

        obj = _SomeClass()
        obj.attr_1 = {"attr_1": 2, "c": "b"}
        with self.assertRaisesRegex(KnownException, r"Type of \'obj\.attr_1\[c\]\' is wrong\. "
                                                    r"Expected: \<class \'int\'\> "
                                                    r"Got: \<class \'str\'\>\(Value: b\)"):
            attribute_types_validate(obj, {"attr_1": Dict[str, int]}, obj_str="obj")

    def test_attribute_types_validate_dict_key_unknown_type(self):
        """tests if exception is raised when key in dictionary is not of expected type and cannot be casted to expected
        type on type validation for dictionary attribute"""
        obj = _SomeClass()
        obj.attr_1 = {"a": 2, "c": "b"}
        with self.assertRaisesRegex(KnownException, r"Type of \'obj\.attr_1\.keys\[0\]\' is wrong\. "
                                                    r"Expected: \<class.*_SomeClass\'\> "
                                                    r"Got: \<class \'str\'\>\(Value: a\)"):
            attribute_types_validate(obj, {"attr_1": Dict[_SomeClass, int]}, obj_str="obj")

    def test_attribute_types_validate_list_elem_error(self):
        """tests if exception is raised when value in list is not of expected type and
                cannot be casted to expected type on type validation for list attribute"""
        some_obj = _SomeClass()
        some_obj.attr_1 = [1, 2, "a"]
        with self.assertRaisesRegex(KnownException, r"Type of \'obj\.attr_1\[2\]\' is wrong\. "
                                                    r"Expected: .*int.*"
                                                    r"Got:.*str.*\(Value: a\)"):
            attribute_types_validate(some_obj, {"attr_1": List[int]}, obj_str="obj")

    def test_attribute_types_validate_dict_value_unknown_type(self):
        """tests if exception is when value in dictionary is not of expected type and cannot be casted to expected
        type on type validation for dictionary attribute"""
        obj = _SomeClass()
        obj.attr_1 = {1: "a"}
        with self.assertRaisesRegex(KnownException, r"Type of \'obj\.attr_1\[1\]\' is wrong\. "
                                                    r"Expected: \<class.*_SomeClass\'\> "
                                                    r"Got: \<class \'str\'\>\(Value: a\)"):
            attribute_types_validate(obj, {"attr_1": Dict[int, _SomeClass]}, obj_str="obj")

    def test_attribute_types_validate_cast_datetime(self):
        """tests if datetime strings are converted to datetime objects"""
        some_obj = _SomeClass()
        some_obj.attr_1 = ["2019-12-29 13:13:23"]
        attribute_types_validate(some_obj, {"attr_1": List[datetime]})
        self.assertEqual(datetime(2019, 12, 29, 13, 13, 23), some_obj.attr_1[0])

    def test_attribute_types_validate_cast_datetime_error(self):
        """tests if exception raised when datetime strings are invalid"""
        some_obj = _SomeClass()
        some_obj.attr_1 = ["2019/12/29 13:13:23"]
        with self.assertRaisesRegex(KnownException, r"Invalid date format or value in obj\.attr_1\[0\]"):
            attribute_types_validate(some_obj, {"attr_1": List[datetime]}, obj_str="obj")

    def test_attribute_types_validate_cast_enum(self):
        """tests if enum strings are converted to enum objects"""
        some_obj = _SomeClass()
        some_obj.attr_1 = ["Supervised"]
        attribute_types_validate(some_obj, {"attr_1": List[AfeLearningType]})
        self.assertEqual(AfeLearningType.Supervised, some_obj.attr_1[0])

    def test_attribute_types_validate_cast_enum_error(self):
        """tests if exception raised when enum strings are invalid"""
        some_obj = _SomeClass()
        some_obj.attr_1 = ["asdfasd"]
        with self.assertRaisesRegex(KnownException, r"Invalid value for enum .* in obj\.attr_1\[0\]"):
            attribute_types_validate(some_obj, {"attr_1": List[AfeLearningType]}, obj_str="obj")

    def test_attribute_types_validate_cast_np_float_type(self):
        """tests casting to np float type on attribute_types_validate"""
        some_obj = _SomeClass()
        some_obj.attr_1 = [1.3]
        attribute_types_validate(some_obj, {"attr_1": List[np.float32]})
        self.assertEqual(np.float32(1.3), some_obj.attr_1[0])

    def test_attribute_types_validate_cast_np_int_type(self):
        """tests casting to np int type on attribute_types_validate"""
        some_obj = _SomeClass()
        some_obj.attr_1 = [4]
        attribute_types_validate(some_obj, {"attr_1": List[np.int16]})
        self.assertEqual(np.int16(4), some_obj.attr_1[0])

    def test_attribute_types_validate_none_attr(self):
        """tests if None value stays None on attribute_types_validate"""
        some_obj = _SomeClass()
        some_obj.attr_1 = None
        some_obj.attr_2 = "a"
        attribute_types_validate(some_obj, {"attr_1": List[np.int16], "attr_2": str})
        self.assertIsNone(some_obj.attr_1)

    def test_attribute_types_validate_wrong_type(self):
        """tests attribute_types_validate when expected type is not a built-in class
        but value is not of expected class"""
        some_obj = _SomeClass()
        some_obj.attr_1 = 1
        some_obj.attr_2 = "a"
        with self.assertRaisesRegex(KnownException, r"Type of \'obj\.attr_1\' is wrong.*_SomeClass.*int"):
            attribute_types_validate(some_obj, {"attr_1": _SomeClass, "attr_2": str}, obj_str="obj")

    def test_attribute_types_validate_invalid_expected_type(self):
        """tests attribute_types_validate when there is expected_type class cannot be handled"""
        some_obj = _SomeClass()
        some_obj.attr_1 = _SomeClass()
        some_obj.attr_2 = "a"
        with self.assertRaisesRegex(NotImplementedError, r"No type validation functions for given class.*_SomeClass"):
            attribute_types_validate(some_obj, {"attr_1": _SomeClass, "attr_2": str}, obj_str="obj")

    def test_attribute_types_validate_modelling_settings(self):
        """tests attribute_types_validate when an attribute is expected to be instance of AfeModellingSettings"""
        some_obj = _SomeClass()
        settings = AfeModellingSettings()
        some_obj.attr_1 = settings
        some_obj.attr_2 = "a"
        with patch(afe_settings_type_validators.__name__ + ".validate_types_modelling_settings") as mock:
            attribute_types_validate(some_obj, {"attr_1": AfeModellingSettings, "attr_2": str}, obj_str="obj")
            mock.assert_called_with(some_obj.attr_1, exception_list=False)
        self.assertEqual("a", some_obj.attr_2)

    def test_attribute_types_validate_record_source(self):
        """tests attribute_types_validate when an attribute is expected to be instance of RecordSource"""
        some_obj = _SomeClass()
        source = RecordSource(source="some file")
        some_obj.attr_1 = source
        some_obj.attr_2 = "a"
        with patch(afe_settings_type_validators.__name__ + ".validate_types_record_source") as mock:
            attribute_types_validate(some_obj, {"attr_1": RecordSource, "attr_2": str}, obj_str="obj")
            mock.assert_called_with(some_obj.attr_1, "obj.attr_1", False)
        self.assertEqual("a", some_obj.attr_2)

    def test_attribute_types_validate_obj_with_attr_dict(self):
        """tests attribute_types_validate when expected_type of an attribute has ATTR_DICT"""

        some_obj = _SomeClass()
        obj = _ClassWithAttrDict()
        obj.int_attr = "a"
        obj.str_attr = "a"
        some_obj.attr_1 = obj
        some_obj.attr_2 = "a"
        with patch(afe_settings_type_validators.__name__ + ".validate_types_for_obj_with_attr_dict") as mock:
            attribute_types_validate(some_obj, {"attr_1": _ClassWithAttrDict, "attr_2": str}, obj_str="obj")
            mock.assert_called_with(some_obj.attr_1, "obj.attr_1", False)
        self.assertEqual("a", some_obj.attr_2)

    def test_attribute_types_validate_invalid_typing_type(self):
        """tests if NotImplementedError is raised when an expected_type is an invalid typing type"""
        some_obj = _SomeClass()
        some_obj.attr_1 = "a"
        with self.assertRaises(NotImplementedError):
            attribute_types_validate(some_obj, {"attr_1": Tuple})

    def test_validate_types_scoring_settings(self):
        """tests validate_types_scoring_settings"""
        scoring_settings = AfeScoringSettings()
        scoring_settings.model_output = _SomeClass()
        with self.assertRaisesRegex(KnownException,
                                    r"scoring_settings\.model_output is not an instance of BaseAfeModelOutput"):
            validate_types_scoring_settings(scoring_settings)

    def test_validate_types_modelling_settings(self):
        """tests validate_types_modelling_settings"""
        settings = AfeModellingSettings()
        settings.algorithm_settings = AfeSupervisedAlgorithmSettings()
        with patch(afe_settings_type_validators.__name__ + ".validate_types_for_obj_with_attr_dict") as mock:
            mock.side_effect = lambda obj, *args, **kwargs: ["modelling settings error"] if obj == settings \
                else ["alg settings error"]
            exc_list = validate_types_modelling_settings(settings)
            self.assertEqual(2, len(exc_list))
            self.assertIn("modelling settings error", exc_list)
            self.assertIn("alg settings error", exc_list)


if __name__ == '__main__':
    unittest.main()
