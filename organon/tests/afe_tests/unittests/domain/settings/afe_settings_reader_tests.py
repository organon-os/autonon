"""
This module includes SettingsReaderTests class.
"""
import unittest
from datetime import datetime
from typing import List

from organon.afe.domain.enums.afe_learning_type import AfeLearningType
from organon.afe.domain.enums.date_resolution import DateResolution
from organon.afe.domain.modelling.supervised.afe_supervised_algorithm_settings import AfeSupervisedAlgorithmSettings
from organon.afe.domain.modelling.unsupervised.afe_unsupervised_algorithm_settings import \
    AfeUnsupervisedAlgorithmSettings
from organon.afe.domain.settings.afe_modelling_settings import AfeModellingSettings
from organon.afe.domain.settings.afe_output_settings import AfeOutputSettings
from organon.afe.domain.settings.afe_process_settings import AfeProcessSettings
from organon.afe.domain.settings.afe_reading_settings import AfeDataReadingSettings
from organon.afe.domain.settings.afe_settings_reader import AfeSettingsReader
from organon.afe.domain.settings.binary_target import BinaryTarget
from organon.afe.domain.settings.feature_generation_settings import FeatureGenerationSettings
from organon.afe.domain.settings.model_settings import ModelSettings
from organon.afe.domain.settings.record_source import RecordSource
from organon.afe.domain.settings.target_descriptor import TargetDescriptor
from organon.afe.domain.settings.temporal_grid import TemporalGrid
from organon.afe.domain.settings.trx_descriptor import TrxDescriptor
from organon.tests.afe_tests.test_helper import MOD_SET_FILE_PATH
from organon.tests.afe_tests.unittests.domain.settings.reader_test_helper import ReaderTestHelper


class AfeSettingsReaderTests(unittest.TestCase):
    """
    Tests class for afe_settings_reader class.
    """

    def setUp(self) -> None:
        self.settings_filepath = MOD_SET_FILE_PATH
        self.helper = ReaderTestHelper(test_case=self, allow_enums_as_string=True)

    def test_get_feature_generation_setting(self):
        """tests get_feature_generation_settings method"""
        args, date_column_args = get_feature_gen_dict()
        settings = AfeSettingsReader().get_feature_generation_setting(**args)
        self.assertIsInstance(settings, FeatureGenerationSettings)
        self.helper.assert_attribute_types(settings, FeatureGenerationSettings.ATTR_DICT)

        self.helper.assert_attribute_values(settings, args, ignored_attrs=list(date_column_args.keys()) +
                                                                          ["temporal_grids"])
        self.assertEqual(settings.date_column.column_name, date_column_args["date_column_name"])
        self.assertEqual(settings.date_column.date_column_type, date_column_args["date_column_type"])
        self.assertEqual(settings.date_column.custom_format, date_column_args["date_column_format"])
        self.assertEqual(len(settings.temporal_grids), 2)
        actual_temp_grids = args["temporal_grids"]
        # pylint: disable=unsubscriptable-object
        self.helper.assert_attribute_values(settings.temporal_grids[0], actual_temp_grids[0])
        self.assertEqual(settings.temporal_grids[1], actual_temp_grids[1])

        # test if invalid temporal_grid raises exception
        new_args2 = args.copy()
        new_args2["temporal_grids"] = [{"ofset": 1, "length": 1, "stride": 3}]
        with self.assertRaisesRegex(ValueError, ".*ofset.*"):
            AfeSettingsReader().get_feature_generation_setting(**new_args2)

        # Test if enum is also accepted as argument
        new_args3 = args.copy()
        new_args3["date_resolution"] = DateResolution.Day
        setting = AfeSettingsReader().get_feature_generation_setting(**new_args3)
        self.assertEqual(setting.date_resolution, DateResolution.Day)

    def test_get_trx_descriptor(self):
        """tests get_trx_descriptor"""
        args = get_trx_descriptor_dict()
        settings = AfeSettingsReader().get_trx_descriptor(**args)
        self.assertIsInstance(settings, TrxDescriptor)
        self.helper.assert_attribute_types(settings, TrxDescriptor.ATTR_DICT)

        self.assertEqual(settings.modelling_raw_input_source.source, args["source"])
        self.assertEqual(settings.entity_column_name, args["entity_column_name"])
        self.assertIsInstance(settings.feature_gen_setting, FeatureGenerationSettings)
        # pylint: disable=unsubscriptable-object
        self.assertEqual(settings.reading_settings.number_of_rows_per_step, 100)

    def test_get_trx_descriptor_multiple_feature_gen_settings(self):
        """tests get_trx_descriptor with multiple feature generation settings"""
        args = get_trx_descriptor_dict()
        feature_gen_setting = args["feature_gen_setting"]
        args["feature_gen_setting"] = [feature_gen_setting, feature_gen_setting]
        with self.assertRaisesRegex(ValueError, "Execution with multiple feature generation settings is available "
                                                "only in premium version"):
            AfeSettingsReader().get_trx_descriptor(**args)

    def test_get_trx_descriptor_feature_gen_setting_in_list(self):
        """tests get_trx_descriptor where feature_gen_Settings is given in a list"""
        args = get_trx_descriptor_dict()
        feature_gen_setting = args["feature_gen_setting"]
        args["feature_gen_setting"] = [feature_gen_setting]
        settings = AfeSettingsReader().get_trx_descriptor(**args)
        self.assertIsInstance(settings, TrxDescriptor)
        self.assertIsInstance(settings.feature_gen_setting, FeatureGenerationSettings)

    def test_get_target_descriptor(self):
        """tests get_target_descriptor method"""
        args = get_target_descriptor_dict()
        desc = AfeSettingsReader().get_target_descriptor(**args)
        self.assertIsInstance(desc, TargetDescriptor)
        self.helper.assert_attribute_types(desc, TargetDescriptor.ATTR_DICT)
        self.helper.assert_not_none(desc, ["entity_column_name", "date_column", "target_column",
                                           "default_measurement_date"])

    def test_get_target_descriptor_no_target_column(self):
        """tests get_target_descriptor method when target_column_name is not given"""
        args = get_target_descriptor_dict()
        args["target_column_name"] = None
        desc = AfeSettingsReader().get_target_descriptor(**args)
        self.assertIsInstance(desc, TargetDescriptor)
        self.assertIsNone(desc.target_column)

    def test_get_supervised_algorithm_settings(self):
        """tests get_supervised_algorithm_settings method"""
        all_args, args, _ = get_supervised_alg_settings_dict()
        settings = AfeSettingsReader().get_supervised_algorithm_settings(**all_args)
        self.assertIsInstance(settings, AfeSupervisedAlgorithmSettings)
        self.helper.assert_attribute_types(settings, AfeSupervisedAlgorithmSettings.ATTR_DICT)

        self.helper.assert_not_none(settings, list(args.keys()) + ["model_settings", "final_model_settings"])
        self.assertIsInstance(settings.model_settings, ModelSettings)
        self.assertIsInstance(settings.final_model_settings, ModelSettings)

    def test_get_unsupervised_algorithm_settings(self):
        """tests get_supervised_algorithm_settings method"""
        args = get_unsupervised_alg_settings_dict()

        settings = AfeSettingsReader().get_unsupervised_algorithm_settings(**args)
        self.assertIsInstance(settings, AfeUnsupervisedAlgorithmSettings)
        self.helper.assert_attribute_types(settings, AfeUnsupervisedAlgorithmSettings.ATTR_DICT)
        self.helper.assert_not_none(settings, list(args.keys()))

    def test_get_output_settings(self):
        """tests get_output_settings method"""
        args = {
            "output_folder": "folder",
            "output_prefix": "ewrqw",
            "feature_name_prefix": "wefrqwf",
            "enable_feature_lookup_output_to_csv": True,
            "enable_write_output": False,
        }
        settings = AfeSettingsReader().get_output_settings(**args)
        self.assertIsInstance(settings, AfeOutputSettings)
        self.helper.assert_attribute_types(settings, AfeOutputSettings.ATTR_DICT)
        self.helper.assert_not_none(settings, list(args.keys()))

    def test_get_settings(self):
        """tests get_settings method"""
        args = get_settings_args()
        settings = AfeSettingsReader().get_settings(**args)
        self.assertIsInstance(settings, AfeModellingSettings)
        self.helper.assert_attribute_types(settings, AfeModellingSettings.ATTR_DICT)
        self.helper.assert_not_none(settings, list(AfeModellingSettings.ATTR_DICT.keys()))
        self.helper.assert_is_instance(settings.data_source_settings.target_record_source_list,
                                       List[RecordSource])

    def test_get_settings_no_output_settings(self):
        """tests get_settings method with no output settings"""
        args = get_settings_args()
        args["output_settings"] = None
        settings = AfeSettingsReader().get_settings(**args)
        self.assertIsNotNone(settings.output_settings)
        self.assertIsInstance(settings.output_settings, AfeOutputSettings)

    def test_assign_default_values_binary_target_info_when_none(self):
        """tests if binary_target_info is set when it is none"""
        args = get_settings_args()
        settings = AfeSettingsReader().get_settings(**args)
        target_column = settings.data_source_settings.target_descriptor.target_column
        target_column.binary_target_info = None
        AfeSettingsReader().assign_default_values(settings)
        self.assertIsNotNone(target_column.binary_target_info)
        self.assertEqual("P", target_column.binary_target_info.positive_category)
        self.assertEqual("N", target_column.binary_target_info.negative_category)
        self.assertEqual("I", target_column.binary_target_info.indeterminate_category)
        self.assertEqual("X", target_column.binary_target_info.exclusion_category)

    def test_assign_default_values_binary_target_info_when_only_one_category_given(self):
        """tests if binary_target_info is set when only positive_category is given"""
        args = get_settings_args()
        settings = AfeSettingsReader().get_settings(**args)
        target_column = settings.data_source_settings.target_descriptor.target_column
        target_column.binary_target_info = BinaryTarget(positive_category="P")
        AfeSettingsReader().assign_default_values(settings)
        self.assertIsNotNone(target_column.binary_target_info)
        self.assertEqual("P", target_column.binary_target_info.positive_category)
        self.assertIsNone(target_column.binary_target_info.negative_category)
        self.assertIsNone(target_column.binary_target_info.indeterminate_category)
        self.assertIsNone(target_column.binary_target_info.exclusion_category)

    def test_assign_default_values_algorithm_settings_none_supervised(self):
        """tests if algorithm_settings is set when it is None and learning_type is supervised"""
        args = get_settings_args()
        settings = AfeSettingsReader().get_settings(**args)
        settings.algorithm_settings = None
        settings.afe_learning_type = AfeLearningType.Supervised
        AfeSettingsReader().assign_default_values(settings)
        self.assertIsInstance(settings.algorithm_settings, AfeSupervisedAlgorithmSettings)
        self.assertIsInstance(settings.algorithm_settings.model_settings, ModelSettings)
        self.assertIsInstance(settings.algorithm_settings.final_model_settings, ModelSettings)

    def test_assign_default_values_algorithm_settings_none_unsupervised(self):
        """tests if algorithm_settings is set when it is None and learning_type is Unsupervised"""
        args = get_settings_args()
        settings = AfeSettingsReader().get_settings(**args)
        settings.algorithm_settings = None
        settings.afe_learning_type = AfeLearningType.Unsupervised
        AfeSettingsReader().assign_default_values(settings)
        self.assertIsInstance(settings.algorithm_settings, AfeUnsupervisedAlgorithmSettings)

    def test_assign_default_values_model_settings_none(self):
        """tests if model_settings is set when it is None"""
        args = get_settings_args()
        settings = AfeSettingsReader().get_settings(**args)
        settings.afe_learning_type = AfeLearningType.Supervised
        settings.algorithm_settings.model_settings = None
        settings.algorithm_settings.final_model_settings = None
        AfeSettingsReader().assign_default_values(settings)
        self.assertIsInstance(settings.algorithm_settings.model_settings, ModelSettings)
        self.assertIsInstance(settings.algorithm_settings.final_model_settings, ModelSettings)

    def test_assign_default_values_set_trx_descriptor_reading_settings(self):
        """tests if trx reading settings is set when it is none"""
        args = get_settings_args()
        settings = AfeSettingsReader().get_settings(**args)
        settings.data_source_settings.trx_descriptor.reading_settings = None
        AfeSettingsReader().assign_default_values(settings)
        self.assertIsInstance(settings.data_source_settings.trx_descriptor.reading_settings, AfeDataReadingSettings)

    def test_assign_default_values_set_target_descriptor_reading_settings(self):
        """tests if target reading settings is set when it is none"""
        args = get_settings_args()
        settings = AfeSettingsReader().get_settings(**args)
        settings.data_source_settings.target_descriptor.reading_settings = None
        AfeSettingsReader().assign_default_values(settings)
        self.assertIsInstance(settings.data_source_settings.target_descriptor.reading_settings, AfeDataReadingSettings)

    def test_assign_default_values_set_process_settings(self):
        """tests if process settings is set when it is none"""
        args = get_settings_args()
        settings = AfeSettingsReader().get_settings(**args)
        settings.process_settings = None
        AfeSettingsReader().assign_default_values(settings)
        self.assertIsInstance(settings.process_settings, AfeProcessSettings)


def get_output_settings_dict():
    """returns example arguments dictionary for method get_output_settings"""
    return {
        "output_folder": "folder",
        "output_prefix": "ewrqw",
        "feature_name_prefix": "wefrqwf",
        "enable_feature_lookup_output_to_csv": True,
        "enable_write_output": False,
    }


def get_feature_gen_dict():
    """returns example arguments dictionary for method get_feature_generation_settings"""
    dim_cols = ["dim1", "dim2"]
    qty_cols = ["qty1", "qty2"]
    temp_grids = [{"offset": 1, "length": 1, "stride": 3}, TemporalGrid(offset=1, length=4, stride=5)]
    max_observation_date = datetime(2016, 1, 1)
    date_offset = 1
    date_column_args = {
        "date_column_name": "date-col",
        "date_column_type": "DateTime",
        "date_column_format": "%y%M%d"
    }
    args = {
        "temporal_grids": temp_grids,
        "date_resolution": "Day",
        "dimension_columns": dim_cols,
        "quantity_columns": qty_cols,
        "date_offset": date_offset,
        "max_observation_date": max_observation_date
    }
    args.update(date_column_args)
    return args, date_column_args


def get_trx_descriptor_dict():
    """returns example arguments dictionary for method get_trx_descriptor"""

    return {
        "source": "some file",
        "entity_column_name": "entity_col",
        "feature_gen_setting": {
            "date_column_name": "mme_dattim_trx",
            "date_column_type": "DateTime",
            "date_column_format": "DateTime",
            "dimension_columns": [
                "machine",
                "mms_tmtdescription"
            ],
            "quantity_columns": [
                "prm_piece_length",
                "mms_picks",
                "mms_dectim",
                "mme_runtime",
                "mme_stptime",
                "mme_picks",
                "mme_picksdoff"
            ],
            "date_resolution": "Month",
            "temporal_grids": [
                {
                    "length": 5,
                    "offset": 1,
                    "stride": 3
                }
            ],
            "date_offset": 1,
            "max_observation_date": "2019-12-29 13:13:23"
        }
        ,
        "number_of_rows_per_step": 100
    }


def get_target_descriptor_dict():
    """returns example arguments dictionary for method get_target_descriptor"""
    return {
        "entity_column_name": "mms_tmacod",
        "default_measurement_date": datetime(2019, 12, 29),
        "date_column_name": "mme_dattim",
        "date_column_type": "DateTime",
        "target_column_name": "target",
        "target_column_type": "Binary",
        "target_positive_category": "P",
        "target_negative_category": "N",
        "target_indeterminate_category": "I",
        "target_exclusion_category": "X"
    }


def get_supervised_alg_settings_dict():
    """returns example arguments dictionary for method get_supervised_algorithm_settings"""

    args = {
        "dimension_compression_ratio": 0.99,
        "dimension_max_cardinality": 200,
        "min_data_in_leaf_and_sample_size_control_ratio": 0.3,
        "training_percentage": 0.8}
    model_args = {
        "model_params": {
            "learning_rate": 0.01,
            "n_estimators": 200,
            "min_data_in_leaf": 1
        },
        "model_fit_params": {
            "early_stopping_rounds": 5
        },
        "reduction_coverage": 0.5,
        "final_model_params": {
        },
        "final_model_fit_params": {
            "early_stopping_rounds": 5
        },
        "final_reduction_coverage": 0.5
    }
    all_args = args.copy()
    all_args.update(model_args)
    return all_args, args, model_args


def get_unsupervised_alg_settings_dict():
    """returns example arguments dictionary for method get_unsupervised_algorithm_settings"""

    return {
        "bin_count": 20,
        "r_factor": 1.5,
        "max_column_count": 200,
        "is_logging": True,
        "dimension_compression_ratio": 0.99,
        "dimension_max_cardinality": 200,
    }


def get_settings_args():
    """returns example arguments dictionary for method get_settings"""
    output_settings = get_output_settings_dict()
    trx_descriptor = get_trx_descriptor_dict()
    target_desc = get_target_descriptor_dict()
    _, args, model_args = get_supervised_alg_settings_dict()
    model_settings = ModelSettings(model_params=model_args["model_params"],
                                   model_fit_params=model_args["model_fit_params"],
                                   reduction_coverage=model_args["reduction_coverage"])
    final_model_settings = ModelSettings(model_params=model_args["model_params"],
                                         model_fit_params=model_args["model_fit_params"],
                                         reduction_coverage=model_args["reduction_coverage"])
    sup_args = args.copy()
    sup_args.update({"model_settings": model_settings, "final_model_settings": final_model_settings})
    alg_settings = AfeSupervisedAlgorithmSettings(**sup_args)
    args = {
        "number_of_cores": 4,
        "scoring_target_source": "score target file",
        "scoring_raw_input_source": "score raw input file",
        "trx_descriptor": trx_descriptor,
        "target_descriptor": target_desc,
        "afe_learning_type": "Supervised",
        "algorithm_settings": alg_settings,
        "output_settings": output_settings,
        "target_record_source_list": ["some file path", "some file path",
                                      "D:\\GIT\\Organon.AiPlatform.Python\\organon\\afe_tests\\test_data\\POC_MDL_2.csv"
                                      ],
        "max_number_of_target_samples": 15,
        "max_number_of_transaction_samples": 1000,
    }
    return args


if __name__ == '__main__':
    unittest.main()
