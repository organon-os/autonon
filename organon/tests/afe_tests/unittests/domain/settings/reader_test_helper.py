"""Includes AssertionHelper class."""
import enum
import inspect
import unittest
from typing import get_args, get_origin, Dict, List, Any


class ReaderTestHelper:
    """Helper class for assertions in testing."""

    def __init__(self, test_case: unittest.TestCase, allow_enums_as_string=False):
        self.test_case = test_case
        self.allow_enums_as_string = allow_enums_as_string

    @staticmethod
    def is_typing_type(_type):
        """Checks if given type is a 'typing' type"""
        return not inspect.isclass(_type)

    def assert_not_none(self, obj, attrs):
        """asserts given attributes of obj are not None"""
        for attr in attrs:
            try:
                self.test_case.assertIsNotNone(getattr(obj, attr))
            except AssertionError as exc:
                raise AssertionError(f"{attr} is None") from exc

    def assert_attribute_values(self, obj, attr_dict_map: Dict[str, Any],
                                ignored_attrs: List[str] = None):
        """compares object attribute values to values in given dict"""
        for attr, value in attr_dict_map.items():
            if ignored_attrs is not None and attr in ignored_attrs:
                continue
            actual_val = getattr(obj, attr)
            self.test_case.assertEqual(value, actual_val)

    def assert_attribute_types(self, obj, attr_dict_map: Dict[str, type],
                               ignored_attrs: List[str] = None):
        """compares object attribute types to types in given dict"""
        for attr, _type in attr_dict_map.items():
            if ignored_attrs is not None and attr in ignored_attrs:
                continue
            val = getattr(obj, attr)
            if val is not None:
                self.assert_is_instance(val, _type)

    def assert_enum_string(self, string: str, enum_class):  # pylint: disable=no-self-use
        """Asserts given string is castable to given enum type"""
        try:
            enum_class[string]
        except KeyError as exc:
            raise AssertionError(f"{string} is not a valid string for enum classs {enum_class}") from exc

    def assert_is_instance_typing_type(self, obj, typing_type):
        """asserts isinstance for 'typing' types"""
        origin = get_origin(typing_type)
        if origin == list:
            self._assert_is_instance_typing_list(obj, typing_type)
        elif origin == dict:
            self._assert_is_instance_typing_dict(obj, typing_type)
        else:
            raise NotImplementedError

    def _assert_is_instance_typing_list(self, obj, typing_type):
        """asserts isinstance for 'typing.List' type"""
        elem_type = get_args(typing_type)[0]
        self.test_case.assertIsInstance(obj, list)
        for elem in obj:
            self.assert_is_instance(elem, elem_type)

    def _assert_is_instance_typing_dict(self, obj, typing_type):
        """asserts isinstance for 'typing.Dict' type"""
        self.test_case.assertIsInstance(obj, dict)
        key_type, val_type = get_args(typing_type)
        for key, val in obj.items():
            self.assert_is_instance(key, key_type)
            self.assert_is_instance(val, val_type)

    def assert_is_instance(self, val, expected_type):
        """unittest.TestCase.assert_is_instance extended with 'typing' types"""
        if self.is_typing_type(expected_type):
            self.assert_is_instance_typing_type(val, expected_type)
        else:
            if issubclass(expected_type, enum.Enum):
                if isinstance(val, str) and self.allow_enums_as_string:
                    self.assert_enum_string(val, expected_type)
                else:
                    self.test_case.assertIsInstance(val, expected_type)
            else:
                self.test_case.assertIsInstance(val, expected_type)
