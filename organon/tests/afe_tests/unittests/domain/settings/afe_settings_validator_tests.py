"""
This module includes SettingsValidatorTests class.
"""
import unittest
from unittest.mock import patch, MagicMock

import pandas as pd

from organon.afe.domain.enums.afe_date_column_type import AfeDateColumnType
from organon.afe.domain.enums.afe_learning_type import AfeLearningType
from organon.afe.domain.enums.afe_target_column_type import AfeTargetColumnType
from organon.afe.domain.enums.record_source_type import RecordSourceType
from organon.afe.domain.modelling.supervised.afe_supervised_algorithm_settings import AfeSupervisedAlgorithmSettings
from organon.afe.domain.reporting.afe_model_output import AfeModelOutput
from organon.afe.domain.settings import afe_settings_validator
from organon.afe.domain.settings.afe_data_settings import AfeDataSettings
from organon.afe.domain.settings.afe_date_column import AfeDateColumn
from organon.afe.domain.settings.afe_modelling_settings import AfeModellingSettings
from organon.afe.domain.settings.afe_output_settings import AfeOutputSettings
from organon.afe.domain.settings.afe_process_settings import AfeProcessSettings
from organon.afe.domain.settings.afe_reading_settings import AfeDataReadingSettings
from organon.afe.domain.settings.afe_scoring_settings import AfeScoringSettings
from organon.afe.domain.settings.afe_settings_validator import AfeSettingsValidator
from organon.afe.domain.settings.afe_target_column import AfeTargetColumn
from organon.afe.domain.settings.binary_target import BinaryTarget
from organon.afe.domain.settings.feature_generation_settings import FeatureGenerationSettings
from organon.afe.domain.settings.record_source import RecordSource
from organon.afe.domain.settings.target_descriptor import TargetDescriptor
from organon.afe.domain.settings.temporal_grid import TemporalGrid
from organon.afe.domain.settings.trx_descriptor import TrxDescriptor
from organon.fl.core.exceptionhandling.known_exception import KnownException


def _get_trx_descriptor():
    trx_desc = TrxDescriptor()
    trx_desc.modelling_raw_input_source = RecordSource(source="trx_source")
    trx_desc.entity_column_name = "mms_tmacod"
    trx_desc.reading_settings = AfeDataReadingSettings()
    trx_desc.reading_settings.number_of_rows_per_step = 20000
    trx_desc.feature_gen_setting = FeatureGenerationSettings()
    trx_desc.feature_gen_setting.date_column = AfeDateColumn(column_name="mme_dattim_trx", date_column_type="DateTime")
    trx_desc.feature_gen_setting.temporal_grids = [TemporalGrid(offset=1, length=5, stride=3)]
    trx_desc.feature_gen_setting.date_resolution = "Day"
    trx_desc.feature_gen_setting.dimension_columns = ["dim_col1"]
    trx_desc.feature_gen_setting.quantity_columns = ["qty_col1"]
    return trx_desc


def _get_target_descriptor():
    desc = TargetDescriptor()
    desc.target_column = AfeTargetColumn()
    desc.target_column.column_name = "target"
    desc.target_column.target_column_type = AfeTargetColumnType.Binary
    desc.target_column.binary_target_info = BinaryTarget(positive_category="P", negative_category="N")
    desc.entity_column_name = "target_entity_col"
    desc.date_column = AfeDateColumn(column_name="target_date_col", date_column_type=AfeDateColumnType.DateTime)
    desc.default_measurement_date = "2019-12-29 13:13:23"
    return desc


def _get_output_settings():
    output_settings = AfeOutputSettings()
    output_settings.output_folder = "folder"
    output_settings.output_prefix = "afe"
    output_settings.feature_name_prefix = "at"
    output_settings.enable_feature_lookup_output_to_csv = True
    return output_settings


def _get_alg_settings():
    alg_settings = AfeSupervisedAlgorithmSettings()
    alg_settings.dimension_compression_ratio = 1.6
    return alg_settings


def _get_feature_gen_settings():
    feature_gen_settings = FeatureGenerationSettings()
    feature_gen_settings.included_operators = ["Density", "Min", "Max"]
    return feature_gen_settings


def _get_afe_settings():
    settings = AfeModellingSettings()
    settings.process_settings = AfeProcessSettings()
    settings.process_settings.number_of_cores = 4
    settings.afe_learning_type = AfeLearningType.Supervised
    settings.data_source_settings = AfeDataSettings()
    settings.data_source_settings.trx_descriptor = _get_trx_descriptor()
    settings.data_source_settings.target_descriptor = _get_target_descriptor()
    settings.data_source_settings.target_record_source_list = [
        RecordSource(source="target_source_1"),
        RecordSource(source="target_source_2")
    ]
    settings.algorithm_settings = _get_alg_settings()

    settings.output_settings = _get_output_settings()

    return settings


def _read_csv_side_effect(file_name, *args, **kwargs):
    # pylint: disable=unused-argument
    if file_name == "trx_source":
        return pd.DataFrame(columns=["mms_tmacod", "mme_dattim_trx", "dim_col1", "qty_col1"])
    return pd.DataFrame(columns=["target_entity_col", "target", "target_date_col"])


class AfeSettingsValidatorTests(unittest.TestCase):
    """Test class for afe_settings_validator class"""

    def setUp(self) -> None:
        patcher = patch(afe_settings_validator.__name__ + ".validate_types_modelling_settings")
        patcher.start()
        self.addCleanup(patcher.stop)
        pd_read_csv_patcher = patch(afe_settings_validator.__name__ + ".pd.read_csv")
        self.mock_read_csv = pd_read_csv_patcher.start()
        self.mock_read_csv.side_effect = _read_csv_side_effect
        self.addCleanup(pd_read_csv_patcher.stop)
        self.afe_settings = _get_afe_settings()
        self.feature_settings = _get_feature_gen_settings()
        self.val_errors = AfeSettingsValidator().get_modelling_validation_errors(self.afe_settings)
        check_req_attrs_patcher = patch(afe_settings_validator.__name__ + ".check_required_attrs", return_value=[])
        self.mock_check_req_attrs = check_req_attrs_patcher.start()
        self.addCleanup(check_req_attrs_patcher.stop)

    def test_validate(self):
        """Tests overall validation"""
        self.assertEqual(len(self.val_errors), 0)

    def test_afe_output_settings_output_prefix_error(self):
        """tests output_prefix not given error"""
        self.afe_settings.output_settings.enable_all_feature_lookup_output_to_csv = True
        self.afe_settings.output_settings.output_prefix = None
        with self.assertRaisesRegex(KnownException, "Please give output_settings.output_prefix"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_temporal_grid_to_horizon_list(self):
        """Test creation of horizon_list from temporal_grid"""
        afe_mod_settings = self.afe_settings
        feature_generation_settings = \
            afe_mod_settings.data_source_settings.trx_descriptor.feature_gen_setting
        self.assertListEqual(feature_generation_settings.horizon_list, [1, 4, 7, 10, 13])

    def test_target_column_not_exist_in_source(self):
        """tests if exception is raised when target_column_name not in target source"""
        self.afe_settings.data_source_settings.target_descriptor.target_column.column_name = "dummy"
        with self.assertRaisesRegex(KnownException, r"Column \'dummy\' not found in target_record_source_list\[0\]"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_target_entity_column_not_exist_in_source(self):
        """tests if exception is raised when target entity_column not in target source"""
        self.afe_settings.data_source_settings.target_descriptor.entity_column_name = "dummy"
        with self.assertRaisesRegex(KnownException, r"Column \'dummy\' not found in target_record_source_list\[0\]"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_target_date_column_not_exist_in_source(self):
        """tests if exception is raised when target date column not in target source"""
        self.afe_settings.data_source_settings.target_descriptor.date_column.column_name = "dummy"
        with self.assertRaisesRegex(KnownException, r"Column \'dummy\' not found in target_record_source_list\[0\]"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_trx_entity_column_not_exist_in_source(self):
        """tests if exception is raised when trx entity_column not in trx source"""
        self.afe_settings.data_source_settings.trx_descriptor.entity_column_name = "dummy"
        with self.assertRaisesRegex(KnownException, r"Column \'dummy\' not found in transaction source"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_trx_date_column_not_exist_in_source(self):
        """tests if exception is raised when trx date column not in trx source"""
        self.afe_settings.data_source_settings.trx_descriptor.feature_gen_setting.date_column.column_name = "dummy"
        with self.assertRaisesRegex(KnownException, r"Column \'dummy\' not found in transaction source"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_trx_dimension_column_not_exist_in_source(self):
        """tests if exception is raised when a dimension column not in trx source"""
        self.afe_settings.data_source_settings.trx_descriptor.feature_gen_setting.dimension_columns.append("dummy")
        with self.assertRaisesRegex(KnownException, r"Column \'dummy\' not found in transaction source"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_trx_quantity_column_not_exist_in_source(self):
        """tests if exception is raised when a quantity column not in trx source"""
        self.afe_settings.data_source_settings.trx_descriptor.feature_gen_setting.quantity_columns.append("dummy")
        with self.assertRaisesRegex(KnownException, r"Column \'dummy\' not found in transaction source"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_df_target_column_not_exist_in_source(self):
        """tests if exception is raised when target_column_name not in target source"""
        self.afe_settings.data_source_settings.target_record_source_list = [
            RecordSource(source=pd.DataFrame(columns=["target_entity_col", "target_date_col", "target"]))]
        self.afe_settings.data_source_settings.target_descriptor.target_column.column_name = "dummy"
        with self.assertRaisesRegex(KnownException, r"Column \'dummy\' not found in target_record_source_list\[0\]"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_df_target_entity_column_not_exist_in_source(self):
        """tests if exception is raised when target entity_column not in target source"""
        self.afe_settings.data_source_settings.target_record_source_list = [
            RecordSource(source=pd.DataFrame(columns=["target_entity_col", "target_date_col", "target"]))]
        self.afe_settings.data_source_settings.target_descriptor.entity_column_name = "dummy"
        with self.assertRaisesRegex(KnownException, r"Column \'dummy\' not found in target_record_source_list\[0\]"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_df_target_date_column_not_exist_in_source(self):
        """tests if exception is raised when target date column not in target source"""
        self.afe_settings.data_source_settings.target_record_source_list = [
            RecordSource(source=pd.DataFrame(columns=["target_entity_col", "target_date_col", "target"]))]
        self.afe_settings.data_source_settings.target_descriptor.date_column.column_name = "dummy"
        with self.assertRaisesRegex(KnownException, r"Column \'dummy\' not found in target_record_source_list\[0\]"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_df_trx_entity_column_not_exist_in_source(self):
        """tests if exception is raised when trx entity_column not in trx source"""
        self.afe_settings.data_source_settings.trx_descriptor.modelling_raw_input_source = RecordSource(
            source=pd.DataFrame(columns=["mms_tmacod", "mme_dattim_trx", "dim_col1", "qty_col1"]))
        self.afe_settings.data_source_settings.trx_descriptor.entity_column_name = "dummy"
        with self.assertRaisesRegex(KnownException, r"Column \'dummy\' not found in transaction source"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_df_trx_date_column_not_exist_in_source(self):
        """tests if exception is raised when trx date column not in trx source"""
        self.afe_settings.data_source_settings.trx_descriptor.modelling_raw_input_source = RecordSource(
            source=pd.DataFrame(columns=["mms_tmacod", "mme_dattim_trx", "dim_col1", "qty_col1"]))
        self.afe_settings.data_source_settings.trx_descriptor.feature_gen_setting.date_column.column_name = "dummy"
        with self.assertRaisesRegex(KnownException, r"Column \'dummy\' not found in transaction source"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_df_trx_dimension_column_not_exist_in_source(self):
        """tests if exception is raised when a dimension column not in trx source"""
        self.afe_settings.data_source_settings.trx_descriptor.modelling_raw_input_source = RecordSource(
            source=pd.DataFrame(columns=["mms_tmacod", "mme_dattim_trx", "dim_col1", "qty_col1"]))
        self.afe_settings.data_source_settings.trx_descriptor.feature_gen_setting.dimension_columns.append("dummy")
        with self.assertRaisesRegex(KnownException, r"Column \'dummy\' not found in transaction source"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_df_trx_quantity_column_not_exist_in_source(self):
        """tests if exception is raised when a quantity column not in trx source"""
        self.afe_settings.data_source_settings.trx_descriptor.modelling_raw_input_source = RecordSource(
            source=pd.DataFrame(columns=["mms_tmacod", "mme_dattim_trx", "dim_col1", "qty_col1"]))
        self.afe_settings.data_source_settings.trx_descriptor.feature_gen_setting.quantity_columns.append("dummy")
        with self.assertRaisesRegex(KnownException, r"Column \'dummy\' not found in transaction source"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_validate_scoring_settings_invalid_raw_input_source(self):
        """tests validate_scoring_settings when scoring raw_input_source is not a df or csv file"""
        settings = AfeScoringSettings()
        settings.model_output = AfeModelOutput()
        settings.raw_input_source = RecordSource(source="asfd")
        settings.raw_input_source.get_type = MagicMock(return_value=RecordSourceType.DATABASE)
        with patch(afe_settings_validator.__name__ + ".validate_types_scoring_settings", return_value=[]):
            with self.assertRaisesRegex(KnownException,
                                        "Scoring raw input source should be either a DataFrame or file path"):
                AfeSettingsValidator().validate_scoring_settings(settings)

    def test_validate_scoring_settings_invalid_target_source(self):
        """tests validate_scoring_settings when scoring target source is not a df or csv file"""
        settings = AfeScoringSettings()
        settings.model_output = AfeModelOutput()
        settings.raw_input_source = RecordSource(source=pd.DataFrame())
        settings.target_record_source = RecordSource(source="adfa")
        settings.target_record_source.get_type = MagicMock(return_value=RecordSourceType.DATABASE)
        with patch(afe_settings_validator.__name__ + ".validate_types_scoring_settings", return_value=[]):
            with self.assertRaisesRegex(KnownException,
                                        "Scoring target source should be either a DataFrame or file path"):
                AfeSettingsValidator().validate_scoring_settings(settings)

    def test_temporal_grid_invalid_length(self):
        """tests if exception is raised when length is not a positive integer"""
        setting = self.afe_settings.data_source_settings.trx_descriptor.feature_gen_setting
        setting.temporal_grids = [TemporalGrid(length=-1, offset=1, stride=3)]
        with self.assertRaisesRegex(KnownException,
                                    r"data_source_settings\.trx_descriptor\.feature_gen_setting"
                                    r".temporal_grids\[0\]\.length should be a positive integer"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_temporal_grid_invalid_offset(self):
        """tests if exception is raised when offset is not a positive integer"""
        setting = self.afe_settings.data_source_settings.trx_descriptor.feature_gen_setting
        setting.temporal_grids = [TemporalGrid(length=3, offset=0, stride=3)]
        with self.assertRaisesRegex(KnownException,
                                    r"data_source_settings\.trx_descriptor\.feature_gen_setting"
                                    r".temporal_grids\[0\]\.offset should be a positive integer"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_temporal_grid_invalid_stride(self):
        """tests if exception is raised when stride is not a positive integer"""

        setting = self.afe_settings.data_source_settings.trx_descriptor.feature_gen_setting
        setting.temporal_grids = [TemporalGrid(length=3, offset=1, stride=0)]
        with self.assertRaisesRegex(KnownException,
                                    r"data_source_settings\.trx_descriptor\.feature_gen_setting"
                                    r".temporal_grids\[0\]\.stride should be a positive integer"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_temporal_grid_empty(self):
        """tests if exception is raised when no temporal grids are given"""

        setting = self.afe_settings.data_source_settings.trx_descriptor.feature_gen_setting
        setting.temporal_grids = []
        with self.assertRaisesRegex(KnownException,
                                    r"data_source_settings\.trx_descriptor\.feature_gen_setting"
                                    r".temporal_grids cannot be empty"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_target_record_source_list_empty_error(self):
        """tests if exception is raised when target_record_source_list is an empty list"""
        self.afe_settings.data_source_settings.target_record_source_list = []
        with self.assertRaisesRegex(KnownException, r"data_source_settings\.target_record_source_list cannot be empty"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_unsupervised_target_column_given_error(self):
        """tests if exception is raised when learning type is unsupervised but target_column is given"""
        self.afe_settings.data_source_settings.target_descriptor.target_column = AfeTargetColumn(
            column_name="a", target_column_type=AfeTargetColumnType.Scalar)
        self.afe_settings.afe_learning_type = AfeLearningType.Unsupervised
        with self.assertRaisesRegex(KnownException, r"data_source_settings\.target_descriptor\.target_column should be "
                                                    r"None for unsupervised modelling."):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_date_column_custom_format_not_provided_error(self):
        """tests if exception is raised when date column type is CustomFormat but custom_format not provided"""
        setting = self.afe_settings.data_source_settings.trx_descriptor.feature_gen_setting
        setting.date_column.date_column_type = AfeDateColumnType.CustomFormat
        setting.date_column.custom_format = None
        with self.assertRaisesRegex(KnownException,
                                    r"Please provide data_source_settings\.trx_descriptor\.feature_gen_setting"
                                    r"\.date_column\.custom_format"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_binary_target_info_not_given_error(self):
        """tests if exception is raised when target column type is Binary but binary_target_info not provided"""
        setting = self.afe_settings.data_source_settings.target_descriptor
        setting.target_column.binary_target_info = None
        with self.assertRaisesRegex(KnownException,
                                    r"data_source_settings.target_descriptor.target_column.binary_target_info "
                                    r"should be provided"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_binary_target_info_positive_category_not_given_error(self):
        """tests if exception is raised when binary_target_info.positive_category is not given"""
        setting = self.afe_settings.data_source_settings.target_descriptor
        setting.target_column.binary_target_info.positive_category = None
        with self.assertRaisesRegex(KnownException,
                                    r"data_source_settings.target_descriptor.target_column.binary_target_info "
                                    r"is invalid"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_binary_target_info_negative_category_not_given_error(self):
        """tests if exception is raised when binary_target_info.negative_category is not given"""
        setting = self.afe_settings.data_source_settings.target_descriptor
        setting.target_column.binary_target_info.negative_category = None
        with self.assertRaisesRegex(KnownException,
                                    r"data_source_settings.target_descriptor.target_column.binary_target_info "
                                    r"is invalid"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

    def test_binary_target_info_indeterminate_given_without_exclusion_error(self):
        """tests if exception is raised when binary_target_info.negative_category is invalid"""
        setting = self.afe_settings.data_source_settings.target_descriptor
        setting.target_column.binary_target_info.indeterminate_category = "I"
        with self.assertRaisesRegex(KnownException,
                                    r"data_source_settings.target_descriptor.target_column.binary_target_info "
                                    r"is invalid"):
            AfeSettingsValidator().validate_modelling_settings(self.afe_settings)

if __name__ == '__main__':
    unittest.main()
