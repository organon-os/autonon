"""Includes tests for validation_helper module"""
import unittest

import numpy as np

from organon.afe.domain.settings.validation_helper import check_required_attrs, cast_to_np_type, cast_attr
from organon.fl.core.exceptionhandling.known_exception import KnownException


class ValidationHelperTestCase(unittest.TestCase):
    """Unit test class for validation_helper module"""

    def test_validation_helper_check_required_attrs(self):
        """Test 'check_required_attrs' function from 'test_validation_helper'"""

        class MyObject:
            """test class"""

            def __init__(self):
                self.int_attr = 5
                self.attr_b = None
                self.list_attr = [1, 2]
                self.dict_attr = {"a": 1}
                self.not_req = None

        exc_occurred = False
        try:
            check_required_attrs(MyObject(), ["int_attr", "attr_b", "list_attr", "dict_attr"], "my_object")
        except KnownException as known_exc:
            self.assertEqual("my_object.attr_b is not provided.", str(known_exc))
            exc_occurred = True
        self.assertEqual(exc_occurred, True)

        my_obj = MyObject()
        my_obj.attr_b = 1
        exc_occurred = False
        try:
            check_required_attrs(my_obj, ["int_attr", "attr_b", "list_attr", "dict_attr"], "my_object")
        except KnownException as known_exc:
            self.assertEqual("my_object.attr_b is not provided.", str(known_exc))
            exc_occurred = True
        self.assertEqual(exc_occurred, False)

    def test_validation_helper_cast_to_np_type(self):
        """Test 'cast_to_np_type' function from 'test_validation_helper'"""
        self.assertEqual(type(cast_to_np_type("5", np.int64)), np.int64)
        self.assertEqual(type(cast_to_np_type("5.5", np.float64)), np.float64)

        exc_occurred = False
        try:
            print(cast_to_np_type(5.5, np.int64))
        except TypeError:
            exc_occurred = True
        self.assertEqual(exc_occurred, True)

    def test_validation_helper_cast_attr_from_string(self):
        """Test 'cast_attr_from_string' function from 'test_validation_helper'"""
        self.assertEqual(cast_attr("5", int), 5)
        self.assertEqual(cast_attr("4.55", float), 4.55)

        self.assertEqual(cast_attr("true", bool), True)
        self.assertEqual(cast_attr("True", bool), True)
        self.assertEqual(cast_attr("false", bool), False)
        self.assertEqual(cast_attr("False", bool), False)

        exc_occurred = False
        try:
            cast_attr(1, bool)
        except TypeError:
            exc_occurred = True

        self.assertEqual(exc_occurred, True)

        exc_occurred = False
        try:
            cast_attr("qwe", bool)
        except ValueError:
            exc_occurred = True
        self.assertEqual(exc_occurred, True)

        exc_occurred = False
        try:
            cast_attr("efrqw", list)
        except TypeError:
            exc_occurred = True
        self.assertEqual(exc_occurred, True)


if __name__ == '__main__':
    unittest.main()
