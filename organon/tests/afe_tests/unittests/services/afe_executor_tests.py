"""Includes tests for afe_executor.AFE class."""
import unittest
from typing import Callable
from unittest.mock import patch, MagicMock

import pandas as pd

from organon.afe.core.businessobjects.afe_static_objects import AfeStaticObjects
from organon.afe.domain.common.persist_helper import PersistHelper
from organon.afe.domain.enums.afe_learning_type import AfeLearningType
from organon.afe.domain.modelling.businessobjects.afe_column import AfeColumn
from organon.afe.domain.modelling.businessobjects.afe_feature import AfeFeature
from organon.afe.domain.modelling.businessobjects.transaction_file_stats import TransactionFileStats
from organon.afe.domain.modelling.supervised.multi_target_modelling_service import MultiTargetModellingService
from organon.afe.domain.modelling.unsupervised.unsupervised_feature_extraction_service import \
    UnsupervisedFeatureExtractionService
from organon.afe.domain.reporting.afe_matplotlib_report_helper import AfeMatplotlibReportHelper
from organon.afe.domain.reporting.afe_model_output import AfeModelOutput
from organon.afe.domain.reporting.afe_output_report import AfeOutputReport
from organon.afe.domain.reporting.afe_plotly_report_helper import AfePlotlyReportHelper
from organon.afe.domain.scoring.afe_scoring_service import AfeScoringService
from organon.afe.domain.settings.afe_modelling_settings import AfeModellingSettings
from organon.afe.domain.settings.afe_reading_settings import AfeDataReadingSettings
from organon.afe.domain.settings.afe_scoring_settings import AfeScoringSettings
from organon.afe.domain.settings.afe_settings_reader import AfeSettingsReader
from organon.afe.domain.settings.afe_settings_validator import AfeSettingsValidator
from organon.afe.services import afe_executor
from organon.afe.services.afe_executor import AFE
from organon.fl.core.exceptionhandling.known_exception import KnownException


class AfeExecutorTestCase(unittest.TestCase):
    """Test class for AFE class."""

    def setUp(self) -> None:
        self.mock_reader = MagicMock(name="settings_reader")
        reader_patcher = patch(afe_executor.__name__ + "." + AfeSettingsReader.__name__, return_value=self.mock_reader)
        reader_patcher.start()
        self.addCleanup(reader_patcher.stop)

        self.mock_validator = MagicMock(name="settings_validator")
        validator_patcher = patch(afe_executor.__name__ + "." + AfeSettingsValidator.__name__,
                                  return_value=self.mock_validator)
        validator_patcher.start()
        self.addCleanup(validator_patcher.stop)

        self.mock_mul_tar_modelling_service = MagicMock(name="multi_target_modelling_service")
        mul_tar_mod_ser_patcher = patch(afe_executor.__name__ + "." + MultiTargetModellingService.__name__,
                                        return_value=self.mock_mul_tar_modelling_service)
        mul_tar_mod_ser_patcher.start()
        self.addCleanup(mul_tar_mod_ser_patcher.stop)

        self.mock_unsup_modelling_service = MagicMock(name="unsupervised_modelling_service")
        unsup_mod_ser_patcher = patch(afe_executor.__name__ + "." + UnsupervisedFeatureExtractionService.__name__,
                                      return_value=self.mock_unsup_modelling_service)
        unsup_mod_ser_patcher.start()
        self.addCleanup(unsup_mod_ser_patcher.stop)

        scoring_service_patcher = patch(afe_executor.__name__ + "." + AfeScoringService.__name__)
        self.mock_scoring_service_class = scoring_service_patcher.start()
        self.addCleanup(scoring_service_patcher.stop)

        self.mock_matplotlib_helper = MagicMock(name="matplotlib_helper")
        matplotlib_helper_patcher = patch(afe_executor.__name__ + "." + AfeMatplotlibReportHelper.__name__,
                                          return_value=self.mock_matplotlib_helper)
        matplotlib_helper_patcher.start()
        self.addCleanup(matplotlib_helper_patcher.stop)

        self.mock_plotly_helper = MagicMock(name="plotly_helper")
        plotly_helper_patcher = patch(afe_executor.__name__ + "." + AfePlotlyReportHelper.__name__,
                                      return_value=self.mock_plotly_helper)
        plotly_helper_patcher.start()
        self.addCleanup(plotly_helper_patcher.stop)

        persist_helper_patcher = patch(afe_executor.__name__ + "." + PersistHelper.__name__)
        self.mock_persist_helper_class = persist_helper_patcher.start()
        self.addCleanup(persist_helper_patcher.stop)

    def test_fit_supervised(self):
        """tests fit method with supervised afe settings"""
        afe = AFE()
        settings = AfeModellingSettings()
        self.mock_mul_tar_modelling_service.execute.return_value = "supervised_result"
        settings.afe_learning_type = AfeLearningType.Supervised
        self.assertEqual("supervised_result", afe.fit(settings))
        self.mock_validator.validate_modelling_settings.assert_called_with(settings)
        self.mock_reader.assign_default_values.assert_called_with(settings)

    def test_fit_unsupervised(self):
        """tests fit method with unsupervised afe settings"""
        afe = AFE()
        settings = AfeModellingSettings()
        self.mock_unsup_modelling_service.execute.return_value = "unsupervised_result"
        settings.afe_learning_type = AfeLearningType.Unsupervised
        self.assertEqual("unsupervised_result", afe.fit(settings))
        self.mock_validator.validate_modelling_settings.assert_called_with(settings)
        self.mock_reader.assign_default_values.assert_called_with(settings)

    def test_fit_invalid_learning_type(self):
        """tests if NotImplementedError is raised when afe learning type is invalid"""
        afe = AFE()
        settings = AfeModellingSettings()
        self.mock_mul_tar_modelling_service.execute.return_value = "supervised_result"
        self.mock_unsup_modelling_service.execute.return_value = "unsupervised_result"
        settings.afe_learning_type = "invalid val"
        with self.assertRaises(NotImplementedError):
            afe.fit(settings)

    def test_transform_no_model(self):
        """tests if no model error is raised when transform is called when no model_output exists"""
        afe = AFE()
        self._check_no_model_error(lambda: afe.transform(pd.DataFrame()))

    def _check_no_model_error(self, func: Callable):
        with self.assertRaisesRegex(KnownException, "No model has been generated before"):
            func()

    def test_transform_model_no_target_or_for_date(self):
        """tests if error is raised when neither target_source nor for_date parameter is given"""
        model_output = AfeModelOutput()
        afe = AFE()
        with self.assertRaisesRegex(KnownException, "Please enter either target_source or "
                                                    "for_date parameters to transform"):
            afe.transform_model(model_output, pd.DataFrame())

    def test_transform_model(self):
        """tests transform_model method"""
        model_output = AfeModelOutput()
        afe = AFE()
        target_source = "some file"
        reading_settings = AfeDataReadingSettings(number_of_rows_per_step=100)
        afe.transform_model(model_output, pd.DataFrame(), target_source=target_source,
                            trx_reading_settings=reading_settings, num_threads=5)
        self.mock_validator.validate_scoring_settings.assert_called()
        self.mock_reader.assign_default_values_for_scoring.assert_called()

        settings = self.mock_scoring_service_class.call_args.args[0]
        self.assertIsInstance(settings, AfeScoringSettings)
        self.assertIsInstance(settings.trx_reading_settings, AfeDataReadingSettings)
        self.assertEqual(model_output, settings.model_output)
        self.assertEqual("some file", settings.target_record_source.source)
        self.assertEqual(100, settings.trx_reading_settings.number_of_rows_per_step)
        args_obj = self.mock_scoring_service_class.return_value.score.call_args
        self.assertEqual((5,), args_obj.args)
        self.assertEqual({"for_date": None}, args_obj.kwargs)

    def test_transform_model_with_for_date(self):
        """tests transform_model with for_date parameter"""
        model_output = AfeModelOutput()
        afe = AFE()
        reading_settings = AfeDataReadingSettings(number_of_rows_per_step=100)
        afe.transform_model(model_output, pd.DataFrame(), for_date=201012,
                            trx_reading_settings=reading_settings,
                            num_threads=5)
        settings = self.mock_scoring_service_class.call_args.args[0]
        self.assertIsInstance(settings, AfeScoringSettings)
        self.assertIsNone(settings.target_record_source)
        args_obj = self.mock_scoring_service_class.return_value.score.call_args
        self.assertEqual({"for_date": 201012}, args_obj.kwargs)

    def test_persist_model_output(self):
        """tests persist_model_output"""
        # pylint:disable=no-self-use
        output = AfeModelOutput()
        with patch(afe_executor.__name__ + ".serialize_to_file") as mock:
            AFE.persist_model_output(output, "some file")
            mock.assert_called_with(output, "some file", secret_key=AfeStaticObjects.get_secret_key())

    def test_persist_model_output_invalid_location_error(self):
        """tests invalid locaiton error on persist_model_output"""
        output = AfeModelOutput()
        with self.assertRaisesRegex(ValueError, r"Location invalid.*Enter a file path"):
            AFE.persist_model_output(output, 1)

    def test_get_model(self):
        """tests get_model"""
        # pylint:disable=no-self-use
        with patch(afe_executor.__name__ + ".deserialize_from_file") as mock:
            AFE.get_model("some file")
            mock.assert_called_with("some file", secret_key=AfeStaticObjects.get_secret_key())

    def test_get_model_with_secret_key(self):
        """tests get_model with secret_key entered"""
        # pylint:disable=no-self-use
        with patch(afe_executor.__name__ + ".deserialize_from_file") as mock:
            AFE.get_model("some file", secret_key="abc")
            mock.assert_called_with("some file", secret_key="abc")

    def test_show_feature_importance_report_no_model_error(self):
        """tests if no model error is raised when show_feature_importance_report is called
        when no model_output exists"""
        afe = AFE()
        self._check_no_model_error(afe.show_feature_importance_report)

    def test_show_extended_feature_report_no_model_error(self):
        """tests if no model error is raised when show_extended_feature_report is called
        when no model_output exists"""
        afe = AFE()
        self._check_no_model_error(afe.show_extended_feature_report)

    def test_persist_model_no_model_error(self):
        """tests if no model error is raised when persist_model is called
        when no model_output exists"""
        afe = AFE()
        self._check_no_model_error(lambda: afe.persist_model("abc"))

    def test_show_afe_report_no_model_error(self):
        """tests if no model error is raised when show_afe_report is called
        when no model_output exists"""
        afe = AFE()
        self._check_no_model_error(afe.show_afe_report)

    def test_show_feature_report_no_model_error(self):
        """tests if no model error is raised when show_feature_report is called
        when no model_output exists"""
        afe = AFE()
        self._check_no_model_error(afe.show_feature_report)

    def test_get_features_data_frame_no_model_error(self):
        """tests if no model error is raised when get_features_data_frame is called
        when no model_output exists"""
        afe = AFE()
        self._check_no_model_error(afe.get_features_data_frame)

    def test_get_all_features_data_frame_no_model_error(self):
        """tests if no model error is raised when get_all_features_data_frame is called
        when no model_output exists"""
        afe = AFE()
        self._check_no_model_error(afe.get_all_features_data_frame)

    def test_get_feature_importance_for_targets_no_model_error(self):
        """tests if no model error is raised when get_feature_importance_for_targets is called
        when no model_output exists"""
        afe = AFE()
        self._check_no_model_error(afe.get_feature_importance_for_targets)

    def test_get_auc_score_for_targets_no_model_error(self):
        """tests if no model error is raised when get_auc_score_for_targets is called
        when no model_output exists"""
        afe = AFE()
        self._check_no_model_error(afe.get_auc_score_for_targets)

    def test_show_roc_curve_report_no_model_error(self):
        """tests if no model error is raised when show_roc_curve_report is called
        when no model_output exists"""
        afe = AFE()
        self._check_no_model_error(afe.show_roc_curve_report)

    def test_show_resource_usage_report_no_model_error(self):
        """tests if no model error is raised when show_resource_usage_report is called
        when no model_output exists"""
        afe = AFE()
        self._check_no_model_error(afe.show_resource_usage_report)

    def test_create_feature_importance_report(self):
        """tests create_feature_importance_report"""
        output = AfeModelOutput()
        output.afe_learning_type = AfeLearningType.Supervised
        report = AfeOutputReport()
        self.mock_matplotlib_helper.generate_afe_report.return_value = report
        AFE.create_feature_importance_report(output, target_source_index=3, first_x_features=10)
        self.mock_matplotlib_helper.generate_feature_importance_dashboard.assert_called_with(report, target_index=3,
                                                                                             first_x_feature=10)
        report_2 = AfeOutputReport()
        self.mock_plotly_helper.generate_afe_report.return_value = report_2
        AFE.create_feature_importance_report(output, target_source_index=3, first_x_features=10, use_lib="plotly")
        self.mock_plotly_helper.generate_feature_importance_dashboard.assert_called_with(report_2, target_index=3,
                                                                                         first_x_feature=10)

    def test_create_feature_importance_report_unsupervised_error(self):
        """tests if error is raised when create_feature_importance_report is called with output of
        an unsupervised modellings"""
        output = AfeModelOutput()
        output.afe_learning_type = AfeLearningType.Unsupervised
        with self.assertRaisesRegex(KnownException,
                                    "Feature importance report is not available on Unsupervised models"):
            AFE.create_feature_importance_report(output)

    def test_generate_feature_importance_report_for_targets(self):
        """tests generate_feature_importance_report_for_targets"""
        output = AfeModelOutput()
        output.afe_learning_type = AfeLearningType.Supervised
        output.final_column_metrics = [{"feature_importances": pd.DataFrame({"Column": [1, 2, 3], "Value": [5, 2, 4]})},
                                       {"feature_importances": pd.DataFrame({"Column": [1, 2, 3], "Value": [10, 6, 2]})}
                                       ]
        ret_list = AFE.generate_feature_importance_report_for_targets(output)
        self.assertEqual(2, len(ret_list))
        self.assertIsInstance(ret_list[0], pd.DataFrame)
        self.assertIsInstance(ret_list[1], pd.DataFrame)
        self.assertEqual([1, 3, 2], ret_list[0]["Column"].tolist())
        self.assertEqual([1, 2, 3], ret_list[1]["Column"].tolist())

    def test_generate_feature_importance_report_for_targets_unsupervised_error(self):
        """tests if error is raised when generate_feature_importance_report_for_targets is called with output of
        an unsupervised modellings"""
        output = AfeModelOutput()
        output.afe_learning_type = AfeLearningType.Unsupervised
        with self.assertRaisesRegex(KnownException,
                                    "Feature importance report is not available on Unsupervised models"):
            AFE.generate_feature_importance_report_for_targets(output)

    def test_generate_auc_score_for_targets(self):
        """tests generate_auc_score_for_targets"""
        output = AfeModelOutput()
        output.afe_learning_type = AfeLearningType.Supervised
        output.final_column_metrics = [{"auc": {"all": 0.52}},
                                       {"auc": {"all": 0.6}}]
        ret_list = AFE.generate_auc_score_for_targets(output)
        self.assertEqual([(0, 0.52), (1, 0.6)], ret_list)
        ret_list = AFE.generate_auc_score_for_targets(output, 1)
        self.assertEqual([(1, 0.6)], ret_list)

    def test_generate_auc_score_for_targets_index_error(self):
        """tests generate_auc_score_for_targets with index larger than max index of targets"""
        output = AfeModelOutput()
        output.afe_learning_type = AfeLearningType.Supervised
        output.final_column_metrics = [{"auc": {"all": 0.52}},
                                       {"auc": {"all": 0.6}}]
        with self.assertRaisesRegex(KnownException, "Target with index 4 does not exist."):
            AFE.generate_auc_score_for_targets(output, 4)

    def test_generate_auc_score_for_targets_unsupervised_error(self):
        """tests if error is raised when generate_auc_score_for_targets is called with output of
        an unsupervised modellings"""
        output = AfeModelOutput()
        output.afe_learning_type = AfeLearningType.Unsupervised
        with self.assertRaisesRegex(KnownException,
                                    "AUC score is not available on Unsupervised models"):
            AFE.generate_auc_score_for_targets(output)

    def test_create_roc_curve_report(self):
        """tests create_roc_curve_report"""
        output = AfeModelOutput()
        output.afe_learning_type = AfeLearningType.Supervised
        report = AfeOutputReport()
        self.mock_matplotlib_helper.generate_afe_report.return_value = report
        AFE.create_roc_curve_report(output, target_source_index=3, target_class="P")
        self.mock_matplotlib_helper.generate_roc_curve_dashboard.assert_called_with(report, target_index=3,
                                                                                    target_class="P")
        report_2 = AfeOutputReport()
        self.mock_plotly_helper.generate_afe_report.return_value = report_2
        AFE.create_roc_curve_report(output, target_source_index=3, target_class="P", use_lib="plotly")
        self.mock_plotly_helper.generate_roc_curve_dashboard.assert_called_with(report_2, target_index=3,
                                                                                target_class="P")

    def test_create_roc_curve_report_unsupervised_error(self):
        """tests if error is raised when create_roc_curve_report is called with output of
        an unsupervised modellings"""
        output = AfeModelOutput()
        output.afe_learning_type = AfeLearningType.Unsupervised
        with self.assertRaisesRegex(KnownException,
                                    "ROC report is not available on Unsupervised models"):
            AFE.create_roc_curve_report(output)

    def test_create_features_data_frame(self):
        """tests create_features_data_frame"""
        output = AfeModelOutput()
        feature_1 = AfeFeature(AfeColumn())
        feature_1.feature_name = "a"
        feature_2 = AfeFeature(AfeColumn())
        feature_2.feature_name = "b"
        stats = TransactionFileStats()
        output.output_features = {"a": feature_1, "b": feature_2}
        output.transaction_file_stats = stats
        AFE.create_features_data_frame(model_output=output)
        self.mock_persist_helper_class.get_lookup_data_frame.assert_called_with([feature_1, feature_2], stats)

    def test_create_all_features_data_frame(self):
        """tests create_all_features_data_frame"""
        output = AfeModelOutput()
        feature_1 = AfeFeature(AfeColumn())
        feature_1.feature_name = "a"
        feature_2 = AfeFeature(AfeColumn())
        feature_2.feature_name = "b"
        stats = TransactionFileStats()
        output.all_features = {"a": feature_1, "b": feature_2}
        output.transaction_file_stats = stats
        AFE.create_all_features_data_frame(model_output=output)
        self.mock_persist_helper_class.get_lookup_data_frame.assert_called_with([feature_1, feature_2], stats)


if __name__ == '__main__':
    unittest.main()
