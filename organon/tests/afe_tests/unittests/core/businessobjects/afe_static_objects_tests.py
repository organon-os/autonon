"""Includes AfeStaticObjects tests."""
import unittest
from datetime import datetime

from organon.afe.core.businessobjects.afe_static_objects import AfeStaticObjects


class AfeStaticObjectsTestCase(unittest.TestCase):
    """Test class for afe date helper"""

    def setUp(self) -> None:
        self.afe_static_objects = AfeStaticObjects()

    def test_set_defaults(self):
        """Test for set defaults AfeStaticObjects """
        self.afe_static_objects.set_defaults()

        self.assertEqual(self.afe_static_objects.distinct_entities_table_name_prefix, 'DET')
        self.assertEqual(self.afe_static_objects.distinct_entities_entity_column_name, 'ENTITY_ID')
        self.assertEqual(self.afe_static_objects.event_date_column_name, 'EVENT_DATE')
        self.assertEqual(self.afe_static_objects.empty_date_col_prefix, '__DATE')
        self.assertEqual(self.afe_static_objects.no_date_col_default_date, datetime(1971, 1, 1))
        self.assertEqual(self.afe_static_objects.empty_quantity_column, 'NoQuantity')
        self.assertEqual(self.afe_static_objects.empty_dimension_column, 'NoDimension')
        self.assertEqual(self.afe_static_objects.empty_dimension_val, "NULL")
        self.assertEqual(self.afe_static_objects.empty_dimension_index, 0)
