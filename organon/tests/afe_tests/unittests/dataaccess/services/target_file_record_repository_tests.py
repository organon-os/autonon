"""
This module includes test class for TargetFileRecordRepository.
"""
import unittest
from typing import List
from unittest.mock import MagicMock, patch, mock_open

import numpy as np
import pandas as pd

from organon.afe.core.businessobjects.afe_static_objects import AfeStaticObjects
from organon.afe.dataaccess.services import target_file_record_repository
from organon.afe.dataaccess.services.target_file_record_repository import TargetFileRecordRepository
from organon.afe.domain.enums.afe_date_column_encoding_type import AfeDateColumnEncodingType
from organon.afe.domain.enums.afe_date_column_type import AfeDateColumnType
from organon.afe.domain.enums.afe_target_column_type import AfeTargetColumnType
from organon.afe.domain.enums.binary_target_class import BinaryTargetClass
from organon.afe.domain.modelling.businessobjects.target_file_record import TargetFileRecord
from organon.afe.domain.modelling.businessobjects.target_file_record_collection import TargetFileRecordCollection
from organon.afe.domain.settings.afe_date_column import AfeDateColumn
from organon.afe.domain.settings.afe_reading_settings import AfeDataReadingSettings
from organon.afe.domain.settings.afe_target_column import AfeTargetColumn
from organon.afe.domain.settings.binary_target import BinaryTarget
from organon.afe.domain.settings.record_source import RecordSource
from organon.afe.domain.settings.target_descriptor import TargetDescriptor
from organon.fl.core.exceptionhandling.known_exception import KnownException
from organon.fl.core.helpers import date_helper
from organon.tests.afe_tests import test_helper

_TARGET_SOURCE_TABLE_NAME = "dummy_target"


def get_mock_data():
    """generate trx mock data from csv as pandas DataFrame"""
    num_rows = 10
    data = pd.DataFrame()
    data["mms_tmacod"] = [str(i) for i in range(num_rows)]
    data["mme_dattim"] = ["2016-01-01" for i in range(num_rows)]
    data["target"] = ["P", "P", "I", "N", "P", "X", "P", "N", "N", "N"]

    return data


def get_target_descriptor():
    """return target file descriptor settings"""

    descriptor = TargetDescriptor()
    descriptor.entity_column_name = "mms_tmacod"
    date_column = AfeDateColumn()
    date_column.column_name = "mme_dattim"
    date_column.date_column_type = AfeDateColumnType.DateTime
    date_column.date_column_encoding_type = AfeDateColumnEncodingType.DateTime
    descriptor.date_column = date_column

    target_column = AfeTargetColumn()
    target_column.column_name = "target"
    target_column.target_column_type = AfeTargetColumnType.Binary
    target_column.binary_target_info = BinaryTarget(positive_category="P", negative_category="N",
                                                    indeterminate_category="I", exclusion_category="X")
    descriptor.target_column = target_column
    descriptor.reading_settings = AfeDataReadingSettings(number_of_rows_per_step=2)
    return descriptor


class ReadCsvObject:
    """mocks object returned from pandas.read_csv"""

    def __init__(self, data, chunksize: int):
        self.data = data
        self.data_length = len(self.data)
        self.chunksize = chunksize
        self.next_index = 0

    def __iter__(self):
        return self

    def __next__(self):
        min_index = self.next_index
        if min_index >= self.data_length:
            raise StopIteration()
        max_index = min(self.data_length, min_index + self.chunksize)

        val = self.data[min_index:max_index]
        self.next_index = max_index
        return val


class TargetFileRecordRepositoryUnitTestCase(unittest.TestCase):
    """
    Unit test class for TargetFileRecordRepository.
    """

    @classmethod
    def setUpClass(cls) -> None:
        test_helper.prepare_test()

    def setUp(self) -> None:
        self.mock_data = get_mock_data()
        self.target_descriptor = get_target_descriptor()
        self.mock_collection = MagicMock()
        self.collection_patcher = patch(
            target_file_record_repository.__name__ + "." + TargetFileRecordCollection.__name__,
            return_value=self.mock_collection)
        self.collection_patcher.start()
        self.pd_read_csv_patcher = patch(target_file_record_repository.__name__ + ".pd.read_csv")
        self.mock_pd_read_csv = self.pd_read_csv_patcher.start()
        self.mock_pd_read_csv.return_value = ReadCsvObject(self.mock_data, 2)

    def tearDown(self) -> None:
        self.collection_patcher.stop()
        self.pd_read_csv_patcher.stop()

    def test_read_txt_appended_row_count(self):
        """check if all rows are appended to collection"""
        reader = self.__get_target_reader(source_type="text")
        with patch(target_file_record_repository.__name__ + ".open", mock_open(read_data=self.mock_data.to_csv())):
            reader.read_text_source()
            self.assertEqual(10, self.mock_collection.append.call_count)

    def test_read_txt_target_values_binary(self):
        """Check if target values are set properly when target is binary"""
        reader = self.__get_target_reader("text")
        with patch(target_file_record_repository.__name__ + ".open", mock_open(read_data=self.mock_data.to_csv())):
            reader.read_text_source()
        target_file_records: List[TargetFileRecord] = \
            [call_args.args[0] for call_args in self.mock_collection.append.call_args_list]
        binary_map = {
            "P": BinaryTargetClass.POSITIVE,
            "N": BinaryTargetClass.NEGATIVE,
            "I": BinaryTargetClass.INDETERMINATE,
            "X": BinaryTargetClass.EXCLUSION
        }
        for i, record in enumerate(target_file_records):
            actual_target_val = self.mock_data.iloc[i]["target"]
            self.assertEqual(binary_map[actual_target_val], record.target_binary)
            self.assertTrue(np.isnan(record.target_scalar))
            self.assertEqual("nan", record.target_multi_class)

    def test_read_txt_row_count_with_rejected_classes(self):
        """Check if row count is correct when there are rejected target classes"""
        reader = self.__get_target_reader("text", rejected_target_classes=["I", "X"])
        with patch(target_file_record_repository.__name__ + ".open", mock_open(read_data=self.mock_data.to_csv())):
            reader.read_text_source()
        self.assertEqual(8, self.mock_collection.append.call_count)

    def test_read_txt_target_values_scalar(self):
        """Check if target values are set properly when target is scalar"""
        self.target_descriptor.target_column.target_column_type = AfeTargetColumnType.Scalar
        self.mock_data["target"] = [i * 1.0 for i in range(10)]
        reader = self.__get_target_reader("text")
        with patch(target_file_record_repository.__name__ + ".open", mock_open(read_data=self.mock_data.to_csv())):
            reader.read_text_source()
        target_file_records: List[TargetFileRecord] = \
            [call_args.args[0] for call_args in self.mock_collection.append.call_args_list]
        for i, record in enumerate(target_file_records):
            self.assertEqual(BinaryTargetClass.NAN, record.target_binary)
            self.assertEqual(i * 1.0, record.target_scalar)
            self.assertEqual("nan", record.target_multi_class)

    def test_read_txt_target_values_multiclass(self):
        """Check if target values are set properly when target is multiclass"""
        self.target_descriptor.target_column.target_column_type = AfeTargetColumnType.MultiClass
        self.mock_data["target"] = [i % 3 for i in range(10)]
        reader = self.__get_target_reader("text")
        with patch(target_file_record_repository.__name__ + ".open", mock_open(read_data=self.mock_data.to_csv())):
            reader.read_text_source()
        target_file_records: List[TargetFileRecord] = \
            [call_args.args[0] for call_args in self.mock_collection.append.call_args_list]
        for i, record in enumerate(target_file_records):
            self.assertEqual(BinaryTargetClass.NAN, record.target_binary)
            self.assertTrue(np.isnan(record.target_scalar))
            self.assertEqual(str(i % 3), record.target_multi_class)

    def test_read_txt_target_values_no_target(self):
        """Check if target values are set properly when there is no target"""
        self.target_descriptor.target_column = None
        reader = self.__get_target_reader("text")
        with patch(target_file_record_repository.__name__ + ".open", mock_open(read_data=self.mock_data.to_csv())):
            reader.read_text_source()
        target_file_records: List[TargetFileRecord] = \
            [call_args.args[0] for call_args in self.mock_collection.append.call_args_list]
        for record in target_file_records:
            self.assertEqual(BinaryTargetClass.NAN, record.target_binary)
            self.assertTrue(np.isnan(record.target_scalar))
            self.assertEqual("nan", record.target_multi_class)

    def test_read_txt_target_values_target_col_not_in_data_scoring(self):
        """Check if target values are set properly when target column does not exist in data"""
        self.mock_data.drop(columns=["target"], inplace=True)
        reader = self.__get_target_reader("text", is_scoring=True)
        with patch(target_file_record_repository.__name__ + ".open", mock_open(read_data=self.mock_data.to_csv())):
            reader.read_text_source()
        target_file_records: List[TargetFileRecord] = \
            [call_args.args[0] for call_args in self.mock_collection.append.call_args_list]
        for record in target_file_records:
            self.assertEqual(BinaryTargetClass.NAN, record.target_binary)
            self.assertTrue(np.isnan(record.target_scalar))
            self.assertEqual("nan", record.target_multi_class)

    def test_read_txt_target_values_target_col_not_in_data(self):
        """Check if target values are set properly when target column does not exist in data"""
        self.mock_data.drop(columns=["target"], inplace=True)
        reader = self.__get_target_reader("text")
        with patch(target_file_record_repository.__name__ + ".open", mock_open(read_data=self.mock_data.to_csv())):
            with self.assertRaisesRegex(KnownException, "Column target could not be found in data source."):
                reader.read_text_source()

    def test_read_txt_rows_no_date(self):
        """Check date and target values when there is no date column"""
        self.target_descriptor.date_column = None
        self.mock_data.drop(columns=["mme_dattim"], inplace=True)
        reader = self.__get_target_reader("text")
        with patch(target_file_record_repository.__name__ + ".open", mock_open(read_data=self.mock_data.to_csv())):
            reader.read_text_source()
        target_file_records: List[TargetFileRecord] = \
            [call_args.args[0] for call_args in self.mock_collection.append.call_args_list]

        default_date = date_helper.get_date_as_integer(AfeStaticObjects.no_date_col_default_date)
        for record in target_file_records:
            self.assertEqual(default_date, record.event_date)
            self.assertNotEqual(BinaryTargetClass.NAN, record.target_binary)

    def test_txt_invalid_target_type_exception(self):
        """exception should be raised when target type is invalid"""
        self.target_descriptor.target_column.target_column_type = "dummy"
        reader = self.__get_target_reader("text")
        with patch(target_file_record_repository.__name__ + ".open", mock_open(read_data=self.mock_data.to_csv())):
            with self.assertRaisesRegex(KnownException, "Unknown target type"):
                reader.read_text_source()

    def test_read_txt_none_entities(self):
        """Check if row count is correct when there are rows where entity_ids are none or nan"""
        self.mock_data["mms_tmacod"][0] = None
        self.mock_data["mms_tmacod"][3] = float("nan")
        self.mock_data["mms_tmacod"][8] = float("nan")

        reader = self.__get_target_reader("text")

        with patch(target_file_record_repository.__name__ + ".open", mock_open(read_data=self.mock_data.to_csv())):
            reader.read_text_source()
            self.assertEqual(7, self.mock_collection.append.call_count)

    def test_read_txt_none_dates(self):
        """Check if row count is correct when there are rows where event_dates are none or nan"""
        self.mock_data["mme_dattim"][0] = None
        self.mock_data["mme_dattim"][3] = float("nan")
        self.mock_data["mme_dattim"][8] = float("nan")
        reader = self.__get_target_reader("text")
        self.mock_pd_read_csv.return_value = ReadCsvObject(self.mock_data, 2)
        with patch(target_file_record_repository.__name__ + ".open", mock_open(read_data=self.mock_data.to_csv())):
            reader.read_text_source()
            self.assertEqual(7, self.mock_collection.append.call_count)

    def test_read_df_appended_row_count(self):
        """check if all rows are appended to collection"""
        reader = self.__get_target_reader(self.mock_data)
        reader.read_dataframe_source()
        self.assertEqual(10, self.mock_collection.append.call_count)

    def test_read_df_target_values_binary(self):
        """Check if target values are set properly when target is binary"""
        reader = self.__get_target_reader(self.mock_data)
        reader.read_dataframe_source()
        target_file_records: List[TargetFileRecord] = \
            [call_args.args[0] for call_args in self.mock_collection.append.call_args_list]
        binary_map = {
            "P": BinaryTargetClass.POSITIVE,
            "N": BinaryTargetClass.NEGATIVE,
            "I": BinaryTargetClass.INDETERMINATE,
            "X": BinaryTargetClass.EXCLUSION
        }
        for i, record in enumerate(target_file_records):
            actual_target_val = self.mock_data.iloc[i]["target"]
            self.assertEqual(binary_map[actual_target_val], record.target_binary)
            self.assertTrue(np.isnan(record.target_scalar))
            self.assertEqual("nan", record.target_multi_class)

    def test_read_df_row_count_with_rejected_classes(self):
        """Check if row count is correct when there are rejected target classes"""
        reader = self.__get_target_reader(self.mock_data, rejected_target_classes=["I", "X"])
        reader.read_dataframe_source()
        self.assertEqual(8, self.mock_collection.append.call_count)

    def test_read_df_target_values_scalar(self):
        """Check if target values are set properly when target is scalar"""
        self.target_descriptor.target_column.target_column_type = AfeTargetColumnType.Scalar
        self.mock_data["target"] = [i * 1.0 for i in range(10)]
        reader = self.__get_target_reader(self.mock_data, rejected_target_classes=["I", "X"])
        reader.read_dataframe_source()
        target_file_records: List[TargetFileRecord] = \
            [call_args.args[0] for call_args in self.mock_collection.append.call_args_list]
        for i, record in enumerate(target_file_records):
            self.assertEqual(BinaryTargetClass.NAN, record.target_binary)
            self.assertEqual(i * 1.0, record.target_scalar)
            self.assertEqual("nan", record.target_multi_class)

    def test_read_df_target_values_multiclass(self):
        """Check if target values are set properly when target is multiclass"""
        self.target_descriptor.target_column.target_column_type = AfeTargetColumnType.MultiClass
        self.mock_data["target"] = [i % 3 for i in range(10)]
        reader = self.__get_target_reader(self.mock_data, rejected_target_classes=["I", "X"])
        reader.read_dataframe_source()
        target_file_records: List[TargetFileRecord] = \
            [call_args.args[0] for call_args in self.mock_collection.append.call_args_list]
        for i, record in enumerate(target_file_records):
            self.assertEqual(BinaryTargetClass.NAN, record.target_binary)
            self.assertTrue(np.isnan(record.target_scalar))
            self.assertEqual(str(i % 3), record.target_multi_class)

    def test_read_df_target_values_no_target(self):
        """Check if target values are set properly when there is no target"""
        self.target_descriptor.target_column = None
        reader = self.__get_target_reader(self.mock_data, rejected_target_classes=["I", "X"])
        reader.read_dataframe_source()
        target_file_records: List[TargetFileRecord] = \
            [call_args.args[0] for call_args in self.mock_collection.append.call_args_list]
        for record in target_file_records:
            self.assertEqual(BinaryTargetClass.NAN, record.target_binary)
            self.assertTrue(np.isnan(record.target_scalar))
            self.assertEqual("nan", record.target_multi_class)

    def test_read_df_target_values_target_col_not_in_data_scoring(self):
        """Check if target values are set properly when target column does not exist in data"""
        self.mock_data.drop(columns=["target"], inplace=True)
        reader = self.__get_target_reader(self.mock_data, rejected_target_classes=["I", "X"], is_scoring=True)
        reader.read_dataframe_source()
        target_file_records: List[TargetFileRecord] = \
            [call_args.args[0] for call_args in self.mock_collection.append.call_args_list]
        for record in target_file_records:
            self.assertEqual(BinaryTargetClass.NAN, record.target_binary)
            self.assertTrue(np.isnan(record.target_scalar))
            self.assertEqual("nan", record.target_multi_class)

    def test_read_df_target_values_target_col_not_in_data(self):
        """Check if target values are set properly when target column does not exist in data"""
        self.mock_data.drop(columns=["target"], inplace=True)
        reader = self.__get_target_reader(self.mock_data, rejected_target_classes=["I", "X"])
        with self.assertRaisesRegex(KnownException, "Column target could not be found in data source."):
            reader.read_dataframe_source()

    def test_read_df_rows_no_date(self):
        """Check date and target values when there is no date column"""
        self.target_descriptor.date_column = None
        self.mock_data.drop(columns=["mme_dattim"], inplace=True)
        reader = self.__get_target_reader(self.mock_data, rejected_target_classes=["I", "X"])
        reader.read_dataframe_source()
        target_file_records: List[TargetFileRecord] = \
            [call_args.args[0] for call_args in self.mock_collection.append.call_args_list]

        default_date = date_helper.get_date_as_integer(AfeStaticObjects.no_date_col_default_date)
        for record in target_file_records:
            self.assertEqual(default_date, record.event_date)
            self.assertNotEqual(BinaryTargetClass.NAN, record.target_binary)

    def test_df_invalid_target_type_exception(self):
        """exception should be raised when target type is invalid"""
        self.target_descriptor.target_column.target_column_type = "dummy"
        reader = self.__get_target_reader(self.mock_data, rejected_target_classes=["I", "X"])
        with self.assertRaisesRegex(KnownException, "Unknown target type"):
            reader.read_dataframe_source()

    def __get_target_reader(self, source_type: str, rejected_target_classes=None, is_scoring=False):
        """Creates TargetFileRecordRepository instance"""
        if isinstance(source_type, pd.DataFrame):
            source = source_type
        elif source_type == "text":
            source = "dummy.txt"
        record_source = RecordSource(source=source)
        if is_scoring:
            return TargetFileRecordRepository(record_source, self.target_descriptor, is_scoring=True,
                                              rejected_target_classes=rejected_target_classes)
        return TargetFileRecordRepository(record_source, self.target_descriptor,
                                          rejected_target_classes=rejected_target_classes)


if __name__ == '__main__':
    unittest.main()
