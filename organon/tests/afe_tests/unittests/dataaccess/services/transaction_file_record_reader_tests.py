"""
This module includes test class for TransactionFileRecordReader.
"""
import os
import unittest
from datetime import datetime
from random import Random
from typing import List
from unittest.mock import MagicMock

import numpy as np
import pandas as pd

from organon.afe.core.businessobjects.afe_static_objects import AfeStaticObjects
from organon.afe.dataaccess.services.transaction_file_record_reader import TransactionFileRecordReader
from organon.afe.domain.enums.afe_date_column_type import AfeDateColumnType
from organon.afe.domain.modelling.businessobjects.transaction_file_record_collection import \
    TransactionFileRecordCollection
from organon.afe.domain.settings.afe_date_column import AfeDateColumn
from organon.afe.domain.settings.db_object_input import DbObjectInput
from organon.afe.domain.settings.record_source import RecordSource
from organon.fl.core.businessobjects.date_interval import DateInterval
from organon.fl.core.enums.date_interval_type import DateIntervalType
from organon.fl.core.exceptionhandling.known_exception import KnownException
from organon.fl.core.helpers import date_helper
from organon.fl.core.helpers.date_helper import get_date_as_integer
from organon.tests.afe_tests import test_helper
from organon.tests.afe_tests.test_helper import test_helper_path

TEST_TRX_FILE_PATH = os.path.join(test_helper_path, "test_data", "trxfile_1000.csv")
_ENTITY_SOURCE_ENTITY_COLUMN = "det_entity_col"
_TRX_SOURCE_TABLE_NAME = "dummy_trx"


def get_mock_data():
    """generate trx mock data from csv as pandas DataFrame"""
    data = pd.read_csv(TEST_TRX_FILE_PATH, parse_dates=['mme_dattim', 'mme_dattim_trx'])
    return data


def get_interval_per_entity_per_date_col(data: pd.DataFrame, date_columns: List[str]):
    """generates random interval_per_entity"""
    interval_per_entity_per_date_col = {}
    min_date_l = get_date_as_integer(datetime(2016, 6, 23))
    min_date_u = get_date_as_integer(datetime(2016, 6, 28))
    max_date_l = get_date_as_integer(datetime(2016, 6, 30))
    max_date_u = get_date_as_integer(datetime(2016, 7, 5))

    min_max_dates_per_date_col = {}

    for col in date_columns:
        random = Random()
        interval_per_entity = {}
        distinct_entities = {row["mms_tmacod"] for _, row in data.iterrows()}
        max_all = get_date_as_integer(datetime(1971, 1, 1))
        min_all = get_date_as_integer(datetime(2300, 1, 1))
        for entity_id in distinct_entities:
            min_date = random.randint(min_date_l, min_date_u)
            max_date = random.randint(max_date_l, max_date_u)
            interval_per_entity[entity_id] = DateInterval(min_date, max_date, DateIntervalType.CLOSED_CLOSED)
            if min_date < min_all:
                min_all = min_date
            if max_date > max_all:
                max_all = max_date
        interval_per_entity_per_date_col[col] = interval_per_entity
        min_max_dates_per_date_col[col] = (date_helper.get_integer_as_date(min_all),
                                           date_helper.get_integer_as_date(max_all))
    return interval_per_entity_per_date_col, min_max_dates_per_date_col


class TransactionFileRecordReaderUnitTestCase(unittest.TestCase):
    """
    Unit test class for TransactionFileRecordReader.
    """
    data = None

    @classmethod
    def setUpClass(cls) -> None:
        test_helper.prepare_test()
        cls.data: pd.DataFrame = get_mock_data()
        cls.entity_column = "mms_tmacod"
        cls.date_columns = ["mme_dattim", "mme_dattim_trx"]
        cls.dimension_columns = ["machine", "mms_tmtdescription"]
        cls.quantity_columns = ["prm_piece_length", "mms_picks", "mms_dectim", "mme_runtime", "mme_stptime",
                                "mme_picks", "mme_picksdoff"]

        cls.int_per_ent_per_date_col, cls.min_max_dates = \
            get_interval_per_entity_per_date_col(cls.data, cls.date_columns)

    def check_collection_for_interval_per_entity(self, collection: TransactionFileRecordCollection,
                                                 all_data: pd.DataFrame, interval_per_entity_per_date_col):
        """check if all of the rows in actual data which satisfy the interval conditions are in the collection"""
        row_indices = []
        for i, row in all_data.iterrows():
            entity = row[self.entity_column]
            date_cond = False
            for date_col, interval_per_entity in interval_per_entity_per_date_col.items():
                date_interval = interval_per_entity[entity]
                min_date = date_helper.get_integer_as_date(date_interval.lower_bound)
                max_date = date_helper.get_integer_as_date(date_interval.upper_bound)
                if max_date >= row[date_col] >= min_date:
                    date_cond = True
                    break
            if date_cond:
                row_indices.append(i)

        expected_rows = all_data.loc[row_indices]

        # compare expected data with the collection
        self.assertEqual(len(expected_rows), collection.actual_record_count)
        for entity, indices in collection.entity_index_map.items():
            self.assertEqual(sum(expected_rows[self.entity_column] == entity), len(indices))

    def test_trx_stats_given(self):
        """test if given trx_stats is used while building d_arrays"""
        reader = self.__get_trx_reader(self.int_per_ent_per_date_col, source_type="text")
        collection: TransactionFileRecordCollection = reader.read()
        mock_trx_stats = MagicMock(wraps=collection.transaction_file_stats)
        # generate a mock trx_stats where indices equal actual value plus 3
        mock_trx_stats.get_index.side_effect = lambda *args: collection.transaction_file_stats.get_index(*args) + 3
        reader = self.__get_trx_reader(self.int_per_ent_per_date_col, source_type="text", distinct_entity_source=None,
                                       transaction_file_stats=mock_trx_stats)
        new_collection = reader.read()

        # sort to make sure both collections have rows in same order
        collection.sort(self.date_columns[0])
        new_collection.sort(self.date_columns[0])

        # check if d_values in new collection equal actual collection values plus 3 (empty dim col should be same)
        addition_array = [3 for _ in collection.d_map]
        addition_array[collection.d_map[AfeStaticObjects.empty_dimension_column]] = 0
        for entity, indices in collection.entity_index_map.items():
            new_col_indices = new_collection.entity_index_map[entity]
            for i, index in enumerate(indices):
                self.assertListEqual((collection.d_arrays[index] + addition_array).tolist(),
                                     (new_collection.d_arrays[new_col_indices[i]]).tolist())

    def test_txt_distinct_entity_source(self):
        """Checks if trx source is filtered by given distinct entity source (for text source)"""
        all_entities = self.data[self.entity_column].unique()  # pylint: disable=unsubscriptable-object
        entities_to_get = all_entities[:int(len(all_entities) / 2)]

        reader = self.__get_trx_reader(self.int_per_ent_per_date_col,
                                       source_type="text",
                                       distinct_entity_source=RecordSource(
                                           source=pd.DataFrame({_ENTITY_SOURCE_ENTITY_COLUMN: entities_to_get})))
        collection = reader.read()
        self.check_collection_for_distinct_entities(collection, entities_to_get)

    def test_df_distinct_entity_source(self):
        """Checks if trx source is filtered by given distinct entity source (for df source)"""
        all_entities = self.data[self.entity_column].unique()  # pylint: disable=unsubscriptable-object
        entities_to_get = all_entities[:int(len(all_entities) / 2)]

        reader = self.__get_trx_reader(self.int_per_ent_per_date_col,
                                       source_type=self.data,
                                       distinct_entity_source=RecordSource(
                                           source=pd.DataFrame({_ENTITY_SOURCE_ENTITY_COLUMN: entities_to_get})))
        collection = reader.read()
        self.check_collection_for_distinct_entities(collection, entities_to_get)

    def check_collection_for_distinct_entities(self, collection, entities):
        """Checks if collection contains an entity which is not in given entities list"""
        for entity in collection.entity_index_map:
            self.assertIn(entity, entities)

    def test_txt_max_number_of_transaction_samples(self):
        """tests max_number_of_transaction_samples argument for txt source"""
        reader = self.__get_trx_reader(self.int_per_ent_per_date_col, source_type="text",
                                       max_number_of_transaction_samples=3)
        collection = reader.read()
        self.assertEqual(3, collection.actual_record_count)

    def test_df_max_number_of_transaction_samples(self):
        """tests max_number_of_transaction_samples argument for df source"""
        reader = self.__get_trx_reader(self.int_per_ent_per_date_col, source_type=self.data,
                                       max_number_of_transaction_samples=3)
        collection = reader.read()
        self.assertEqual(3, collection.actual_record_count)

    def test_txt_interval_per_entity(self):
        """tests interval_per_entity argument(for text source)"""
        reader = self.__get_trx_reader(self.int_per_ent_per_date_col, source_type="text")
        collection = reader.read()
        self.check_collection_for_interval_per_entity(collection, self.data, self.int_per_ent_per_date_col)

    def test_txt_min_max_dates(self):
        """tests min_max_dates argument(for txt source)"""
        min_max_dates = {'mme_dattim': (datetime(2016, 7, 1),
                                        datetime(2016, 7, 5)),
                         'mme_dattim_trx': (datetime(2016, 6, 25),
                                            datetime(2016, 7, 28))}

        reader = self.__get_trx_reader(None, source_type="text", min_max_dates_per_date_column=min_max_dates)
        collection = reader.read()
        self.assertEqual(6, collection.actual_record_count)
        self.check_collection_for_min_max_dates(collection, min_max_dates)

    def __test_interval_per_date_col(self, source):
        intervals = {'mme_dattim': (datetime(2016, 7, 5),
                                    datetime(2016, 7, 11)),
                     'mme_dattim_trx': (datetime(2016, 6, 23),
                                        datetime(2016, 6, 25))}

        reader = self.__get_trx_reader(None, source_type=source, interval_per_date_column=intervals)
        collection: TransactionFileRecordCollection = reader.read()
        self.assertEqual(2, collection.actual_record_count)
        expected_entities = ["4303_S037", "4309_S019"]
        self.assertCountEqual(expected_entities, list(collection.entity_index_map.keys()))

    def __test_min_max_dates_with_interval_per_date_col(self, source):
        intervals = {'mme_dattim': (datetime(2016, 6, 25),
                                    datetime(2016, 7, 2)),
                     'mme_dattim_trx': (datetime(2016, 6, 25),
                                        datetime(2016, 7, 20))}

        min_max_dates = {'mme_dattim': (datetime(2016, 7, 1),
                                        datetime(2016, 7, 2)),
                         'mme_dattim_trx': (datetime(2016, 6, 27),
                                            datetime(2016, 7, 2))}

        reader = self.__get_trx_reader(None, source_type=source, interval_per_date_column=intervals,
                                       min_max_dates_per_date_column=min_max_dates)
        collection: TransactionFileRecordCollection = reader.read()
        self.assertEqual(6, collection.actual_record_count)
        expected_entities = ["4309_S009", "4309_S010", "4309_S015", "4309_S016", "4309_S017", "4309_S018"]
        self.assertCountEqual(expected_entities, list(collection.entity_index_map.keys()))

    def test_df_interval_per_date_col(self):
        """tests interval_per_date_column argument(for df source)"""
        self.__test_interval_per_date_col(self.data)

    def test_txt_interval_per_date_col(self):
        """tests interval_per_date_column argument(for txt source)"""
        self.__test_interval_per_date_col("text")

    def test_df_min_max_dates_with_interval_per_date_col(self):
        """tests interval_per_date_column argument when min_max_dates_is (for df source)"""
        self.__test_min_max_dates_with_interval_per_date_col(self.data)

    def test_txt_min_max_dates_with_interval_per_date_col(self):
        """tests interval_per_date_column argument(for txt source)"""
        self.__test_min_max_dates_with_interval_per_date_col("text")

    def test_txt_min_max_dates_with_interval_per_entity(self):
        """tests min_max_dates argument when interval_per_entity is also given(for text source)"""
        self.__test_min_max_with_interval_per_entity("text")

    def test_df_min_max_dates(self):
        """tests min_max_dates argument(for df source)"""
        min_max_dates = {'mme_dattim': (datetime(2016, 7, 1),
                                        datetime(2016, 7, 5)),
                         'mme_dattim_trx': (datetime(2016, 6, 25),
                                            datetime(2016, 7, 28))}
        reader = self.__get_trx_reader(None,
                                       source_type=self.data, min_max_dates_per_date_column=min_max_dates)
        collection = reader.read()
        self.assertEqual(6, collection.actual_record_count)
        self.check_collection_for_min_max_dates(collection, min_max_dates)

    def test_df_min_max_dates_with_interval_per_entity(self):
        """tests min_max_dates argument when interval_per_entity is also given(for df source)"""
        self.__test_min_max_with_interval_per_entity(self.data)

    def __test_min_max_with_interval_per_entity(self, source):
        min_max_dates = {'mme_dattim': (datetime(2016, 6, 1),
                                        datetime(2016, 7, 5)),
                         'mme_dattim_trx': (datetime(2016, 6, 25),
                                            datetime(2016, 7, 28))}
        interval_per_date_col = {
            "mme_dattim": {
                "4303_S037": self.__get_date_interval(datetime(2016, 5, 1), datetime(2016, 8, 8)),
                "4315_S026": self.__get_date_interval(datetime(2016, 6, 1), datetime(2016, 8, 4)),
                "4309_S009": self.__get_date_interval(datetime(2016, 7, 10), datetime(2016, 8, 15)),
                "4309_S010": self.__get_date_interval(datetime(2016, 7, 4), datetime(2016, 7, 5))
            },
            "mme_dattim_trx": {
                "4303_S037": self.__get_date_interval(datetime(2016, 5, 1), datetime(2016, 8, 8)),
                "4315_S026": self.__get_date_interval(datetime(2016, 6, 2), datetime(2016, 8, 20)),
                "4309_S009": self.__get_date_interval(datetime(2016, 6, 10), datetime(2016, 8, 10)),
                "4309_S010": self.__get_date_interval(datetime(2016, 7, 4), datetime(2016, 7, 5))
            },
        }

        for entity in self.data["mms_tmacod"]:  # pylint: disable=unsubscriptable-object
            if entity not in interval_per_date_col["mme_dattim"].keys():
                interval_per_date_col["mme_dattim"][entity] = interval_per_date_col["mme_dattim"]["4303_S037"]
                interval_per_date_col["mme_dattim_trx"][entity] = interval_per_date_col["mme_dattim_trx"]["4303_S037"]
        reader = self.__get_trx_reader(interval_per_date_col,
                                       source_type=source,
                                       min_max_dates_per_date_column=min_max_dates)
        collection = reader.read()
        self.assertEqual(6, collection.actual_record_count)
        entities = list(collection.entity_index_map.keys())
        self.assertEqual(6, len(entities))
        # 4303_S037 should be eliminated since its mme_dattim_trx value is lower than min_date for mme_dattim_trx
        self.assertNotIn("4303_S037", entities)
        # 4309_S019 should be eliminated since its mme_dattim value is higher than max_date for mme_dattim
        self.assertNotIn("4309_S019", entities)

        # 4309_S010 should be eliminated since date columns values are not in expected interval for both date columns
        self.assertNotIn("4309_S010", entities)

        self.check_collection_for_min_max_dates(collection, min_max_dates)

    @staticmethod
    def __get_date_interval(lower: datetime, upper: datetime) -> DateInterval:
        return DateInterval(date_helper.get_date_as_integer(lower), date_helper.get_date_as_integer(upper),
                            DateIntervalType.CLOSED_CLOSED)

    def check_collection_for_min_max_dates(self, collection, min_max_dates):
        """Checks if min-max date conditions are satisfied in collection"""
        for date_col, index in collection.date_col_map.items():
            column = collection.dates[:collection.actual_record_count, index]
            min_date, max_date = min_max_dates[date_col]
            min_date = get_date_as_integer(min_date)
            max_date = get_date_as_integer(max_date)
            self.assertEqual(0, np.sum((column > max_date) | (column < min_date)))

    def test_df_interval_per_entity(self):
        """tests interval_per_entity argument(for df source)"""
        reader = self.__get_trx_reader(self.int_per_ent_per_date_col,
                                       source_type=self.data)
        collection = reader.read()
        self.check_collection_for_interval_per_entity(collection, self.data, self.int_per_ent_per_date_col)

    def test_txt_no_interval_per_entity(self):
        """tests if data does not get filtered when interval_per_entity is None(for text source)"""
        reader = self.__get_trx_reader(None, source_type="text")
        collection = reader.read()
        self.assertEqual(len(self.data), collection.actual_record_count)

    def test_df_no_interval_per_entity(self):
        """tests if data does not get filtered when interval_per_entity is None(for df source)"""
        reader = self.__get_trx_reader(None, source_type=self.data)
        collection = reader.read()
        self.assertEqual(len(self.data), collection.actual_record_count)

    def test_txt_invalid_distinct_entity_source(self):
        """tests if exception is raised when distinct entity source type is invalid(for txt source)"""
        reader = self.__get_trx_reader(None, source_type="text",
                                       distinct_entity_source=RecordSource(source=DbObjectInput()))
        with self.assertRaisesRegex(KnownException, "Distinct entity source should be a dataframe"):
            reader.read_text_source()

    def test_df_invalid_distinct_entity_source(self):
        """tests if exception is raised when distinct entity source type is invalid(for df source)"""
        reader = self.__get_trx_reader(None, source_type=self.data,
                                       distinct_entity_source=RecordSource(source=DbObjectInput()))
        with self.assertRaisesRegex(KnownException, "Distinct entity source should be a dataframe"):
            reader.read_dataframe_source()

    def __get_trx_reader(self, interval_per_entity_per_date_col, source_type: str or pd.DataFrame,
                         **kwargs):
        """Creates TransactionFileRecordReader instance"""
        if isinstance(source_type, pd.DataFrame):
            source = source_type
        elif source_type == "text":
            source = TEST_TRX_FILE_PATH
        record_source = RecordSource(source=source)
        date_columns = {
            "mme_dattim": AfeDateColumn(column_name="mme_dattim", date_column_type=AfeDateColumnType.DateTime, ),
            "mme_dattim_trx": AfeDateColumn(column_name="mme_dattim_trx",
                                            date_column_type=AfeDateColumnType.DateTime)
        }
        trx_entity_column = "mms_tmacod"
        dimension_columns = self.dimension_columns + [AfeStaticObjects.empty_dimension_column]
        quantity_columns = self.quantity_columns + [AfeStaticObjects.empty_quantity_column]
        if "distinct_entity_source" not in kwargs:
            kwargs["distinct_entity_source"] = None

        return TransactionFileRecordReader(record_source, date_columns, trx_entity_column, dimension_columns,
                                           quantity_columns,
                                           entity_source_entity_column=_ENTITY_SOURCE_ENTITY_COLUMN,
                                           interval_per_entity_per_date_column=interval_per_entity_per_date_col,
                                           **kwargs)


if __name__ == '__main__':
    unittest.main()
