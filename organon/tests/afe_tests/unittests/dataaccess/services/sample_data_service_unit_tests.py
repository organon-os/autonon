"""
This module includes test class for SampleDataService.
"""
import os
import unittest
from datetime import datetime
from unittest.mock import MagicMock

import pandas as pd

from organon.afe.dataaccess.services.base_record_reader import BaseRecordReader
from organon.afe.dataaccess.services.sample_data_service import SampleDataService
from organon.afe.domain.enums.afe_date_column_encoding_type import AfeDateColumnEncodingType
from organon.afe.domain.enums.afe_date_column_type import AfeDateColumnType
from organon.afe.domain.enums.afe_target_column_type import AfeTargetColumnType
from organon.afe.domain.enums.date_resolution import DateResolution
from organon.afe.domain.modelling.supervised.afe_supervised_algorithm_settings import AfeSupervisedAlgorithmSettings
from organon.afe.domain.settings.afe_data_settings import AfeDataSettings
from organon.afe.domain.settings.afe_date_column import AfeDateColumn
from organon.afe.domain.settings.afe_modelling_settings import AfeModellingSettings
from organon.afe.domain.settings.afe_reading_settings import AfeDataReadingSettings
from organon.afe.domain.settings.afe_target_column import AfeTargetColumn
from organon.afe.domain.settings.auto_column_decider_settings import AutoColumnDeciderSettings
from organon.afe.domain.settings.binary_target import BinaryTarget
from organon.afe.domain.settings.feature_generation_settings import FeatureGenerationSettings
from organon.afe.domain.settings.record_source import RecordSource
from organon.afe.domain.settings.target_descriptor import TargetDescriptor
from organon.afe.domain.settings.temporal_grid import TemporalGrid
from organon.afe.domain.settings.trx_descriptor import TrxDescriptor
from organon.fl.core.businessobjects.date_interval import DateInterval
from organon.fl.core.enums.date_interval_type import DateIntervalType
from organon.tests.afe_tests.test_helper import test_helper_path

TEST_TRX_FILE_PATH = os.path.join(test_helper_path, "test_data", "adu_afe_input_mini.csv")
TEST_TARGET_FILE_PATH = os.path.join(test_helper_path, "test_data", "adu_mdl_1_mini.csv")


def get_trx_file_descriptor():
    """return trx file descriptor settings"""

    descriptor = TrxDescriptor()

    record_source = RecordSource(source=TEST_TRX_FILE_PATH)
    descriptor.modelling_raw_input_source = record_source
    descriptor.entity_column_name = "RECORD_ID"
    feature_generation_settings = FeatureGenerationSettings()
    feature_generation_settings.date_column = AfeDateColumn(column_name="SEGMENTATION_DATE",
                                                            date_column_type=AfeDateColumnType.DateTime)
    feature_generation_settings.dimension_columns = ["EDUCATION", "DIM"]
    feature_generation_settings.quantity_columns = ["QTY"]
    feature_generation_settings.date_resolution = DateResolution.Day
    feature_generation_settings.date_offset = 0
    feature_generation_settings.temporal_grids = [TemporalGrid(offset=1, stride=31, length=4)]
    descriptor.feature_gen_setting = feature_generation_settings
    descriptor.reading_settings = AfeDataReadingSettings()
    descriptor.reading_settings.number_of_rows_per_step = 1000000

    return descriptor


def get_target_descriptor():
    """return target file descriptor settings"""

    descriptor = TargetDescriptor()
    descriptor.entity_column = "SUBSCRIBER_ID"
    date_column = AfeDateColumn()
    date_column.column_name = "SEGMENTATION_DATE"
    date_column.date_column_type = AfeDateColumnType.DateTime
    date_column.date_column_encoding_type = AfeDateColumnEncodingType.DateTime
    descriptor.date_column = date_column

    target_column = AfeTargetColumn()
    target_column.column_name = "target"
    target_column.target_column_type = AfeTargetColumnType.Binary
    target_column.binary_target_info = BinaryTarget(positive_category="P", negative_category="N")
    descriptor.target_column = target_column
    return descriptor


def get_supervised_algorithm_settings():
    """return supervised algorithm settings"""

    settings = AfeSupervisedAlgorithmSettings()
    settings.dimension_compression_ratio = 0.99
    settings.dimension_max_cardinality = 200
    return settings


def get_auto_column_decider_settings():
    """return auto column decider settings"""

    settings = AutoColumnDeciderSettings()
    settings.sampling_ratio = 0.02
    settings.numeric_to_dimension = 30
    settings.dimension_distinct_cut_off = 0.05
    settings.use_dimension_columns = False
    settings.use_quantity_columns = True
    return settings


def get_settings():
    """return afe model settings"""

    settings = AfeModellingSettings()
    data_source_settings = AfeDataSettings()
    data_source_settings.auto_column_decider_settings = get_auto_column_decider_settings()
    data_source_settings.trx_descriptor = get_trx_file_descriptor()
    data_source_settings.target_descriptor = get_target_descriptor()

    record_source_1 = RecordSource(source=TEST_TARGET_FILE_PATH)

    data_source_settings.target_record_source_list = [record_source_1]

    settings.data_source_settings = data_source_settings

    settings.algorithm_settings = get_supervised_algorithm_settings()

    return settings


# pylint: disable=abstract-method
class _SampleDataService(SampleDataService):
    def _get_unique_entities_set(self):
        data = pd.read_csv(TEST_TRX_FILE_PATH)

        distinct_entity_df = pd.DataFrame({"ENTITY_ID": data["RECORD_ID"].unique()})
        return set(distinct_entity_df["ENTITY_ID"].astype(str))


class SampleDataServiceUnitTests(unittest.TestCase):
    """
    Unit test class for SampleDataService.
    """

    def setUp(self) -> None:
        data_source_settings = get_settings().data_source_settings
        algorithm_settings = get_settings().algorithm_settings
        interval_per_entity_per_setting = {}

        interval_per_entity_per_setting["SEGMENTATION_DATE"] = {"1":
                                                                    DateInterval(1517086800000, 1519765200000,
                                                                                 DateIntervalType.CLOSED_CLOSED),
                                                                "2": DateInterval(1517086800000, 1519765200000,
                                                                                  DateIntervalType.CLOSED_CLOSED)}

        self.data = pd.read_csv(TEST_TRX_FILE_PATH)
        self.data_source_settings = data_source_settings
        self.algorithm_settings = algorithm_settings
        self.entity_column = "ENTITY_ID"
        self.interval_per_entity_per_setting = interval_per_entity_per_setting
        self.base_record_reader = MagicMock(spec=BaseRecordReader)
        self.min_date = datetime(2017, 10, 31)
        self.max_date = datetime(2018, 1, 31)

    def test_get_sample(self):
        """Test for get_sample_method"""

        service = _SampleDataService(self.data_source_settings, self.algorithm_settings,
                                     None, self.entity_column,
                                     self.interval_per_entity_per_setting)
        service.get_sample()
        self.assertEqual(True, True)

    def test_get_sample_no_date(self):
        """Test for get_sample_method"""
        self.data_source_settings.trx_descriptor.feature_gen_setting.date_column = None
        service = _SampleDataService(self.data_source_settings, self.algorithm_settings,
                                     None, self.entity_column,
                                     self.interval_per_entity_per_setting)
        service.get_sample()
        self.assertEqual(True, True)

    def test_read_df_source_distinct_entity_source(self):
        """Test read df source"""

        distinct_entity_df = pd.DataFrame({self.entity_column: [1, 1, 2, 3, 4, 5]})
        distinct_entity_source = RecordSource(source=distinct_entity_df)
        settings = get_settings()
        settings.data_source_settings.auto_column_decider_settings.sampling_ratio = 1

        settings.data_source_settings.trx_descriptor.modelling_raw_input_source = RecordSource(source=self.data)
        service = _SampleDataService(settings.data_source_settings, self.algorithm_settings,
                                     distinct_entity_source, self.entity_column,
                                     self.interval_per_entity_per_setting, min_date=self.min_date,
                                     max_date=self.max_date)
        data_frame = service.read_dataframe_source()
        self.assertEqual(2, len(data_frame))

    def test_read_df_source_distinct_entity_source_no_date(self):
        """Test read df source"""

        distinct_entity_df = pd.DataFrame({self.entity_column: [1, 1, 2, 3, 4, 5]})
        distinct_entity_source = RecordSource(source=distinct_entity_df)
        settings = get_settings()
        settings.data_source_settings.trx_descriptor.feature_gen_setting.date_column = None
        settings.data_source_settings.auto_column_decider_settings.sampling_ratio = 1

        settings.data_source_settings.trx_descriptor.modelling_raw_input_source = RecordSource(source=self.data)
        service = _SampleDataService(settings.data_source_settings, self.algorithm_settings,
                                     distinct_entity_source, self.entity_column,
                                     self.interval_per_entity_per_setting, min_date=self.min_date,
                                     max_date=self.max_date)
        data_frame = service.read_dataframe_source()
        self.assertEqual(10, len(data_frame))

    def test_read_df_source_distinct_entity_source_exception(self):
        """Test read df source that distinct entity source is not be df """
        distinct_entities = RecordSource(source=TEST_TRX_FILE_PATH)
        service = _SampleDataService(self.data_source_settings, self.algorithm_settings,
                                     distinct_entities, self.entity_column,
                                     self.interval_per_entity_per_setting)
        with self.assertRaises(Exception):
            service.read_dataframe_source()

    def test_read_trx_source_distinct_entity_source(self):
        """Test read trx source"""

        distinct_entity_df = pd.DataFrame({self.entity_column: [1, 1, 2, 3, 4, 5]})
        distinct_entity_source = RecordSource(source=distinct_entity_df)

        settings = get_settings()
        settings.data_source_settings.auto_column_decider_settings.sampling_ratio = 1
        settings.data_source_settings.trx_descriptor.modelling_raw_input_source = RecordSource(
            source=TEST_TRX_FILE_PATH)
        service = _SampleDataService(settings.data_source_settings, self.algorithm_settings,
                                     distinct_entity_source, self.entity_column,
                                     self.interval_per_entity_per_setting, min_date=self.min_date,
                                     max_date=self.max_date)
        data_frame = service.read_text_source()
        self.assertEqual(2, len(data_frame))

    def test_read_trx_source_distinct_entity_source_no_date(self):
        """Test read trx source"""

        distinct_entity_df = pd.DataFrame({self.entity_column: [1, 1, 2, 3, 4, 5]})
        distinct_entity_source = RecordSource(source=distinct_entity_df)

        settings = get_settings()
        settings.data_source_settings.trx_descriptor.feature_gen_setting.date_column = None
        settings.data_source_settings.auto_column_decider_settings.sampling_ratio = 1
        settings.data_source_settings.trx_descriptor.modelling_raw_input_source = RecordSource(
            source=TEST_TRX_FILE_PATH)
        service = _SampleDataService(settings.data_source_settings, self.algorithm_settings,
                                     distinct_entity_source, self.entity_column,
                                     self.interval_per_entity_per_setting, min_date=self.min_date,
                                     max_date=self.max_date)
        data_frame = service.read_text_source()
        self.assertEqual(10, len(data_frame))

    def test_read_trx_source_distinct_entity_source_test_sampling_ratio(self):
        """Test read trx source with different sampling ratio parameter"""

        distinct_entity_df = pd.DataFrame({self.entity_column: [1, 1, 2, 3, 4, 5]})
        distinct_entity_source = RecordSource(source=distinct_entity_df)

        settings = get_settings()
        settings.data_source_settings.auto_column_decider_settings.sampling_ratio = 0.5
        settings.data_source_settings.trx_descriptor.modelling_raw_input_source = RecordSource(
            source=TEST_TRX_FILE_PATH)
        service = _SampleDataService(settings.data_source_settings, self.algorithm_settings,
                                     distinct_entity_source, self.entity_column,
                                     self.interval_per_entity_per_setting, min_date=self.min_date,
                                     max_date=self.max_date)
        data_frame = service.read_text_source()
        self.assertEqual(1, len(data_frame))

    def test_read_trx_source_distinct_entity_source_test_sampling_ratio_no_date(self):
        """Test read trx source with different sampling ratio parameter"""

        distinct_entity_df = pd.DataFrame({self.entity_column: [1, 1, 2, 3, 4, 5]})
        distinct_entity_source = RecordSource(source=distinct_entity_df)

        settings = get_settings()
        settings.data_source_settings.trx_descriptor.feature_gen_setting.date_column = None

        settings.data_source_settings.auto_column_decider_settings.sampling_ratio = 0.5
        settings.data_source_settings.trx_descriptor.modelling_raw_input_source = RecordSource(
            source=TEST_TRX_FILE_PATH)
        service = _SampleDataService(settings.data_source_settings, self.algorithm_settings,
                                     distinct_entity_source, self.entity_column,
                                     self.interval_per_entity_per_setting, min_date=self.min_date,
                                     max_date=self.max_date)
        data_frame = service.read_text_source()
        self.assertEqual(5, len(data_frame))

    def test_read_trx_source_distinct_entity_source_exception(self):
        """Test read trx source that distinct entity source is not be df """
        distinct_entities = RecordSource(source=TEST_TRX_FILE_PATH)

        service = _SampleDataService(self.data_source_settings, self.algorithm_settings,
                                     distinct_entities, self.entity_column,
                                     self.interval_per_entity_per_setting)
        with self.assertRaises(Exception):
            service.read_text_source()

    def test_read_db_source(self):
        """Test read db source with exception"""
        distinct_entities = RecordSource(source=TEST_TRX_FILE_PATH)

        service = _SampleDataService(self.data_source_settings, self.algorithm_settings,
                                     distinct_entities, self.entity_column,
                                     self.interval_per_entity_per_setting)
        with self.assertRaises(Exception):
            service.read_db_source()


if __name__ == '__main__':
    unittest.main()
