"""
This module includes helper functions for tests of AFE.
"""
import os
import threading
from datetime import datetime
from random import Random

from organon.afe.core.businessobjects.afe_static_objects import AfeStaticObjects
from organon.afe.domain.enums.afe_date_column_type import AfeDateColumnType
from organon.afe.domain.enums.afe_target_column_type import AfeTargetColumnType
from organon.afe.domain.settings.afe_date_column import AfeDateColumn
from organon.afe.domain.settings.afe_reading_settings import AfeDataReadingSettings
from organon.afe.domain.settings.afe_target_column import AfeTargetColumn
from organon.afe.domain.settings.binary_target import BinaryTarget
from organon.afe.domain.settings.target_descriptor import TargetDescriptor
from organon.afe.services.afe_application_operations import AfeApplicationOperations
from organon.fl.core.helpers import date_helper
from organon.fl.generic.interaction.fl_initializer import FlInitializer

test_helper_path = os.path.dirname(__file__) + "\\"
MOD_SET_FILE_PATH = os.path.join(test_helper_path, "test_data", "mod_set.json")
TEST_DATA_DIR_PATH = os.path.join(test_helper_path, "test_data")

TEST_INITIALIZED = False
__INITIALIZATION_LOCK = threading.Lock()


def get_target_descriptor():
    """
    Creates a TargetFileDescriptor instance
    """
    file_descriptor = TargetDescriptor()
    file_descriptor.reading_settings = AfeDataReadingSettings()
    file_descriptor.reading_settings.number_of_rows_per_step = 200

    date_column = AfeDateColumn()
    date_column.column_name = "mme_dattim"
    date_column.date_column_type = AfeDateColumnType.DateTime

    target_column = AfeTargetColumn()
    target_column.column_name = "target"
    target_column.target_column_type = AfeTargetColumnType.Binary
    target_column.binary_target_info = BinaryTarget()
    target_column.binary_target_info.positive_category = "P"
    target_column.binary_target_info.negative_category = "N"
    target_column.binary_target_info.indeterminate_category = "I"
    target_column.binary_target_info.exclusion_category = "X"

    file_descriptor.date_column = date_column
    file_descriptor.entity_column_name = "mms_tmacod"
    file_descriptor.target_column = target_column

    return file_descriptor


def get_random_dates(min_date: datetime, max_date: datetime, count: int):
    """Return random date values between given dates"""
    min_date_int = date_helper.get_date_as_integer(min_date)
    max_date_int = date_helper.get_date_as_integer(max_date)
    random = Random()
    dates = []
    for _ in range(count):
        dates.append(date_helper.get_integer_as_date(random.randint(min_date_int, max_date_int)))
    return dates


def prepare_test():
    """Registers db types and connections and return DbUtilities object corresponding to given 'db_type'"""
    global TEST_INITIALIZED  # pylint: disable=global-statement

    with __INITIALIZATION_LOCK:
        if not TEST_INITIALIZED:
            FlInitializer.application_initialize()
            AfeStaticObjects.set_defaults()
            AfeApplicationOperations.register_types()
            TEST_INITIALIZED = True
