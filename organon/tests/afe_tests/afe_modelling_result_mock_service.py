"""
This module includes AfeModellingResultMockService class.
"""
import math
from datetime import datetime
from typing import Dict
from unittest.mock import MagicMock

import numpy as np
from sortedcontainers import SortedDict

from organon.afe.domain.enums.afe_operator import AfeOperator
from organon.afe.domain.enums.date_resolution import DateResolution
from organon.afe.domain.modelling.businessobjects.afe_column import AfeColumn
from organon.afe.domain.modelling.businessobjects.afe_feature import AfeFeature
from organon.afe.domain.reporting.afe_model_output import AfeModelOutput
from organon.afe.domain.reporting.transformation import Transformation
from organon.fl.core.helpers import guid_helper


class AfeModellingResultMockService:
    """Mock service for afe modelling results to be used in tests."""

    @staticmethod
    def create_mock_result() -> AfeModelOutput:
        """Creates AfeModelOutput instance."""
        result = AfeModelOutput()
        result.model_identifier = guid_helper.new_guid(32)
        result.build_date = datetime(2020, 10, 9)
        result.output_features = AfeModellingResultMockService.__create_output_features()
        result.transformations = AfeModellingResultMockService.__create_transformations()

        hist_mock = MagicMock(name="histogram")
        hist_mock.reverse_index = {0: "dim_group_1", 1: "dim_group_2"}
        result.transaction_file_stats = MagicMock(name="trx_file_stats")
        result.transaction_file_stats.get_histogram.return_value = hist_mock
        return result

    @staticmethod
    def __create_transformations() -> Dict[str, Transformation]:
        """Creates transformation dictionary"""
        result: Dict[str, Transformation] = {}
        map1 = SortedDict(
            [
                (0, 0.57),
                (5, 0.2),
                (15, 0.25),
                (25, 0.9),
                (40, 0.3),
                (150, 0.57),
                (np.finfo("float32").max, 0.08)
            ]
        )
        result["ft_1"] = Transformation([math.nan], 0.5, map1)
        map2 = SortedDict(
            [
                (15, 0.88),
                (28, 0.64),
                (47, 0.43),
                (55, 0.21),
                (78, 0.15),
                (np.finfo("float32").max, 0.08)
            ]
        )
        result["ft_2"] = Transformation([math.nan], 0.42, map2)

        return result

    @staticmethod
    def __create_output_features() -> Dict[str, AfeColumn]:
        """Creates output features"""
        column1 = AfeColumn()
        column1.dimension_name = "D1"
        column1.set = [0]
        column1.in_out = True
        column1.quantity_name = "Q_COL"
        column1.operator = AfeOperator.Sum
        column1.time_window = 3
        column1.date_resolution = DateResolution.Day
        column1.set_column_name()
        feature_1 = AfeFeature(column1)
        feature_1.feature_name = "ft_1"

        column2 = AfeColumn()
        column2.dimension_name = "D1"
        column2.quantity_name = "Q_COL2"
        column2.set = [1]
        column2.operator = AfeOperator.Max
        column2.time_window = 3
        column2.date_resolution = DateResolution.Day
        column2.set_column_name()
        feature_2 = AfeFeature(column2)
        feature_2.feature_name = "ft_2"

        features = [feature_1, feature_2]

        features = {feature.feature_name: feature for feature in features}
        return features
