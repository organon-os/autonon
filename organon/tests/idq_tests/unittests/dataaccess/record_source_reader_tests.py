"""RecordSourceReader unit tests"""
import unittest
from unittest.mock import patch, mock_open

import numpy as np
import pandas as pd

from organon.idq.dataaccess import record_source_reader
from organon.idq.dataaccess.record_source_reader import RecordSourceReader
from organon.idq.domain.businessobjects.record_sources.dq_df_record_source import DqDfRecordSource
from organon.idq.domain.businessobjects.record_sources.dq_file_record_source import DqFileRecordSource
from organon.idq.domain.settings.input_source.dq_df_input_source_settings import DqDfInputSourceSettings
from organon.idq.domain.settings.input_source.dq_file_input_source_settings import DqFileInputSourceSettings
from organon.tests.helpers.read_csv_object import ReadCsvObject


def get_file_settings(record_source: DqFileRecordSource,
                      number_of_rows_per_step: int,
                      is_sampling_on: bool,
                      sampling_ratio: float,
                      max_num_of_samples: int,
                      included_columns: list) -> DqFileInputSourceSettings:
    """

    :return:
    """
    settings = DqFileInputSourceSettings(record_source)
    settings.number_of_rows_per_step = number_of_rows_per_step
    settings.is_sampling_on = is_sampling_on
    settings.sampling_ratio = sampling_ratio
    settings.max_num_of_samples = max_num_of_samples
    settings.included_columns = included_columns
    settings.csv_separator = ','
    return settings


def get_df_settings(record_source: DqDfRecordSource,
                    is_sampling_on: bool,
                    sampling_ratio: float,
                    max_num_of_samples: int,
                    included_columns: list) -> DqDfInputSourceSettings:
    """

    :return:
    """
    settings = DqDfInputSourceSettings(record_source)
    settings.is_sampling_on = is_sampling_on
    settings.sampling_ratio = sampling_ratio
    settings.max_num_of_samples = max_num_of_samples
    settings.included_columns = included_columns
    return settings


class RecordSourceReaderTestCase(unittest.TestCase):
    """RecordSourceReader tests"""

    def setUp(self) -> None:
        self.mock_data = pd.DataFrame(np.array([[1, 2, 3, 10], [4, 5, 6, 11], [7, 8, 9, 12], [7, 8, 9, 12]]),
                                      columns=['a', 'b', 'c', 'd'])
        pd_read_csv_patcher = patch(record_source_reader.__name__ + ".pd.read_csv")
        self.mock_pd_read_csv = pd_read_csv_patcher.start()
        self.mock_pd_read_csv.return_value = pd.DataFrame(np.array([["a", "b", "c", "d"]]), columns=[0, 1, 2, 3])
        rs_filter_helper_patcher = patch(record_source_reader.__name__ + ".RecordSourceFilterHelper")
        self.mock_rs_filter_helper = rs_filter_helper_patcher.start()
        self.mock_rs_filter_helper.read_csv_with_chunks.return_value = ReadCsvObject(self.mock_data[['a', 'b']], 2)
        self.mock_rs_filter_helper.get_filtered_dataframe.side_effect = lambda source, *args: source

        self.addCleanup(pd_read_csv_patcher.stop)
        self.addCleanup(rs_filter_helper_patcher.stop)

    def test_read_txt_source(self):
        """tests if read_txt_source is called when txt record source is given"""
        expected_source = pd.DataFrame(np.array([[1, 2], [4, 5]]),
                                       columns=['a', 'b'])
        record_source: DqFileRecordSource = DqFileRecordSource(locator='./source.csv')
        input_source_settings: DqFileInputSourceSettings = get_file_settings(record_source, 2, True, 0.5, 3, ['a', 'b'])
        reader: RecordSourceReader = RecordSourceReader(input_source_settings)
        self.mock_pd_read_csv.return_value = pd.DataFrame(np.array([["a", "b", "c", "d"]]), columns=[0, 1, 2, 3])

        self.mock_rs_filter_helper.read_csv_with_chunks.return_value = ReadCsvObject(self.mock_data[["a", "b"]], 2)
        with patch(record_source_reader.__name__ + ".open", mock_open(read_data=self.mock_data.to_csv())):
            res_df = reader.read_text_source().sampled_data
            self.assertEqual(expected_source.columns.tolist(), res_df.columns.tolist())
            self.assertEqual(expected_source.shape, res_df.shape)

    def test_read_txt_source_no_included_columns(self):
        """tests if read_txt_source is called when txt record source is given"""
        expected_source = pd.DataFrame(np.array([[1, 2], [4, 5]]),
                                       columns=['a', 'b'])
        record_source: DqFileRecordSource = DqFileRecordSource(locator='./source.csv')
        input_source_settings: DqFileInputSourceSettings = get_file_settings(record_source, 2, True, 0.5, 3, None)
        reader: RecordSourceReader = RecordSourceReader(input_source_settings)

        self.mock_pd_read_csv.return_value = pd.DataFrame(np.array([["a", "b", "c", "d"]]), columns=[0, 1, 2, 3])

        self.mock_rs_filter_helper.read_csv_with_chunks.return_value = ReadCsvObject(self.mock_data[["a", "b"]], 2)

        with patch(record_source_reader.__name__ + ".open", mock_open(read_data=self.mock_data.to_csv())):
            res_df = reader.read_text_source().sampled_data
            self.assertEqual(expected_source.columns.tolist(), res_df.columns.tolist())
            self.assertEqual(expected_source.shape, res_df.shape)

    def test_read_txt_source_with_check_duplicate_cols(self):
        """tests if read_txt_source is called when txt record source is given"""
        mock_data_dup = pd.DataFrame(np.array([[1, 2, 3, 10], [4, 5, 6, 11], [7, 8, 9, 12], [7, 8, 9, 12]]),
                                     columns=['a', 'a', 'b', 'c'])

        record_source: DqFileRecordSource = DqFileRecordSource(locator='./source.csv')
        input_source_settings: DqFileInputSourceSettings = get_file_settings(record_source, 2, True, 0.5, 3, ['a'])
        reader: RecordSourceReader = RecordSourceReader(input_source_settings)

        self.mock_pd_read_csv.return_value = pd.DataFrame(np.array([["a", "a", "b", "c"]]), columns=[0, 1, 2, 3])

        self.mock_rs_filter_helper.read_csv_with_chunks.return_value = ReadCsvObject(mock_data_dup, 2)

        with patch(record_source_reader.__name__ + ".open", mock_open(read_data=mock_data_dup.to_csv())):
            with self.assertRaisesRegex(ValueError, ".*These columns are duplicated in the source.*"):
                reader.read_text_source()

        mock_data_unq = pd.DataFrame(np.array([[1, 2, 3, 10], [4, 5, 6, 11], [7, 8, 9, 12], [7, 8, 9, 12]]),
                                     columns=['a', 'b', 'c', 'd'])
        self.mock_pd_read_csv.return_value = pd.DataFrame(np.array([["a", "b", "c", "d"]]), columns=[0, 1, 2, 3])
        self.mock_rs_filter_helper.read_csv_with_chunks.return_value = ReadCsvObject(mock_data_unq, 2)
        with patch(record_source_reader.__name__ + ".open", mock_open(read_data=mock_data_unq.to_csv())):
            try:
                reader.read_text_source()
            except ValueError:
                self.fail()

    def test_read_txt_source_with_check_included_cols(self):
        """tests if read_txt_source is called when txt record source is given"""
        mock_data = pd.DataFrame(np.array([[1, 2, 3, 10], [4, 5, 6, 11], [7, 8, 9, 12], [7, 8, 9, 12]]),
                                 columns=['a', 'b', 'c', 'd'])

        record_source: DqFileRecordSource = DqFileRecordSource(locator='./source.csv')
        input_source_settings_1: DqFileInputSourceSettings = get_file_settings(
            record_source, 2, True, 0.5, 3, ['a', 'e'])
        reader_not_included_columns: RecordSourceReader = RecordSourceReader(input_source_settings_1)
        self.mock_pd_read_csv.return_value = pd.DataFrame(np.array([["a", "b", "c", "d"]]), columns=[0, 1, 2, 3])
        self.mock_rs_filter_helper.read_csv_with_chunks.return_value = ReadCsvObject(mock_data, 2)

        with patch(record_source_reader.__name__ + ".open", mock_open(read_data=mock_data.to_csv())):
            with self.assertRaisesRegex(ValueError, ".*These columns do not belong to the source.*"):
                reader_not_included_columns.read_text_source()

        input_source_settings_2: DqFileInputSourceSettings = get_file_settings(
            record_source, 2, True, 0.5, 3, ['a', 'b'])
        reader_with_included_columns: RecordSourceReader = RecordSourceReader(input_source_settings_2)
        self.mock_pd_read_csv.return_value = pd.DataFrame(np.array([["a", "b", "c", "d"]]), columns=[0, 1, 2, 3])
        self.mock_rs_filter_helper.read_csv_with_chunks.return_value = ReadCsvObject(mock_data, 2)

        with patch(record_source_reader.__name__ + ".open", mock_open(read_data=mock_data.to_csv())):
            try:
                reader_with_included_columns.read_text_source()
            except ValueError:
                self.fail()

    def test_read_txt_source_max_sample_size(self):
        """tests max_sample_size if read_txt_source is called
        when txt record source is given and max_sample_size less than source.count*sampling_ratio"""
        expected_source = pd.DataFrame(np.array([[1, 2], [1, 2], [1, 2]]),
                                       columns=['a', 'b'])
        record_source: DqFileRecordSource = DqFileRecordSource(locator='./source.csv')
        input_source_settings: DqFileInputSourceSettings = get_file_settings(record_source, 2, True, 1, 3, ['a', 'b'])
        reader: RecordSourceReader = RecordSourceReader(input_source_settings)
        self.mock_pd_read_csv.return_value = pd.DataFrame(np.array([["a", "b", "c", "d"]]), columns=[0, 1, 2, 3])
        self.mock_rs_filter_helper.read_csv_with_chunks.return_value = ReadCsvObject(self.mock_data[["a", "b"]], 2)

        with patch(record_source_reader.__name__ + ".open", mock_open(read_data=self.mock_data.to_csv())):
            res_df = reader.read_text_source().sampled_data
            self.assertEqual(expected_source.columns.tolist(), res_df.columns.tolist())
            self.assertEqual(expected_source.shape, res_df.shape)

    def test_read_dataframe_source(self):
        """read_dataframe_source test"""
        expected_source = pd.DataFrame(np.array([[1, 2], [4, 5]]),
                                       columns=['a', 'b'])
        data_frame = pd.DataFrame(np.array([[1, 2, 3, 10], [4, 5, 6, 11], [7, 8, 9, 12], [7, 8, 9, 12]]),
                                  columns=['a', 'b', 'c', 'd'])
        record_source: DqDfRecordSource = DqDfRecordSource(data_frame, "dummy")
        input_source_settings: DqDfInputSourceSettings = get_df_settings(record_source, True, 0.5, 3, ['a', 'b'])
        reader: RecordSourceReader = RecordSourceReader(input_source_settings)
        res_df = reader.read_dataframe_source().sampled_data
        self.assertEqual(expected_source.columns.tolist(), res_df.columns.tolist())
        self.assertEqual(expected_source.shape, res_df.shape)

    def test_read_df_source_with_check_duplicate_cols(self):
        """check_duplicate_cols test"""
        data_frame_dup = pd.DataFrame(np.array([[1, 2, 3, 10], [4, 5, 6, 11], [7, 8, 9, 12], [7, 8, 9, 12]]),
                                      columns=['a', 'a', 'b', 'c'])
        record_source_dup: DqDfRecordSource = DqDfRecordSource(data_frame_dup, "dummy")
        input_source_settings_dup: DqDfInputSourceSettings = get_df_settings(record_source_dup, True, 0.5, 3, ['a'])
        reader_dup: RecordSourceReader = RecordSourceReader(input_source_settings_dup)

        with self.assertRaisesRegex(ValueError, ".*These columns are duplicated in the source.*"):
            reader_dup.read_dataframe_source()

        data_frame_unq = pd.DataFrame(np.array([[1, 2, 3, 10], [4, 5, 6, 11], [7, 8, 9, 12], [7, 8, 9, 12]]),
                                      columns=['a', 'b', 'c', 'd'])
        record_source_unq: DqDfRecordSource = DqDfRecordSource(data_frame_unq, "dummy")
        input_source_settings_unq: DqDfInputSourceSettings = get_df_settings(record_source_unq, True, 0.5, 3, ['a'])
        reader_unq: RecordSourceReader = RecordSourceReader(input_source_settings_unq)

        try:
            reader_unq.read_dataframe_source()
        except ValueError:
            self.fail()

    def test_read_df_source_with_check_included_cols(self):
        """check_included_cols test"""
        data_frame = pd.DataFrame(np.array([[1, 2, 3, 10], [4, 5, 6, 11], [7, 8, 9, 12], [7, 8, 9, 12]]),
                                  columns=['a', 'b', 'c', 'd'])

        record_source: DqDfRecordSource = DqDfRecordSource(data_frame, "dummy")
        input_source_settings_1: DqDfInputSourceSettings = get_df_settings(record_source, True, 0.5, 3, ['a', 'e'])
        reader_not_included_columns: RecordSourceReader = RecordSourceReader(input_source_settings_1)

        with self.assertRaisesRegex(ValueError, ".*These columns do not belong to the source.*"):
            reader_not_included_columns.read_dataframe_source()

        input_source_settings_2: DqDfInputSourceSettings = get_df_settings(record_source, True, 0.5, 3, ['a', 'c'])
        reader_with_included_columns: RecordSourceReader = RecordSourceReader(input_source_settings_2)

        try:
            reader_with_included_columns.read_dataframe_source()
        except ValueError:
            self.fail()

    def test_read_df_source_max_sample_size(self):
        """max_sample_size test for read_dataframe_source"""
        expected_source = pd.DataFrame(np.array([[1, 2], [4, 5]]),
                                       columns=['a', 'b'])
        data_frame = pd.DataFrame(np.array([[1, 2, 3, 10], [4, 5, 6, 11], [7, 8, 9, 12], [7, 8, 9, 12]]),
                                  columns=['a', 'b', 'c', 'd'])
        record_source: DqDfRecordSource = DqDfRecordSource(data_frame, "dummy")
        input_source_settings: DqDfInputSourceSettings = get_df_settings(record_source, True, 0.75, 2, ['a', 'b'])
        reader: RecordSourceReader = RecordSourceReader(input_source_settings)
        res_df = reader.read_dataframe_source().sampled_data
        self.assertEqual(expected_source.columns.tolist(), res_df.columns.tolist())
        self.assertEqual(expected_source.shape, res_df.shape)

    def test_read_dataframe_source_no_included_columns(self):
        """read_dataframe_source included_columns None test"""
        expected_source = pd.DataFrame(np.array([[1, 2, 3, 10], [4, 5, 6, 11]]),
                                       columns=['a', 'b', 'c', 'd'])
        data_frame = pd.DataFrame(np.array([[1, 2, 3, 10], [4, 5, 6, 11], [7, 8, 9, 12], [7, 8, 9, 12]]),
                                  columns=['a', 'b', 'c', 'd'])
        record_source: DqDfRecordSource = DqDfRecordSource(data_frame, "dummy")
        input_source_settings: DqDfInputSourceSettings = get_df_settings(record_source, True, 0.5, 3, None)
        reader: RecordSourceReader = RecordSourceReader(input_source_settings)
        res_df = reader.read_dataframe_source().sampled_data
        self.assertEqual(expected_source.columns.tolist(), res_df.columns.tolist())
        self.assertEqual(expected_source.shape, res_df.shape)


if __name__ == '__main__':
    unittest.main()
