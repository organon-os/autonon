"""Includes unittests for RecordSourceFilterHelper."""
import unittest
from datetime import datetime
from unittest.mock import patch, MagicMock

import pandas as pd

from organon.fl.core.exceptionhandling.known_exception import KnownException
from organon.idq.dataaccess.helpers import record_source_filter_helper
from organon.idq.dataaccess.helpers.record_source_filter_helper import RecordSourceFilterHelper
from organon.idq.domain.businessobjects.record_sources.dq_df_record_source import DqDfRecordSource
from organon.idq.domain.businessobjects.record_sources.dq_file_record_source import DqFileRecordSource
from organon.idq.domain.settings.date_value_definition import DateValueDefinition
from organon.idq.domain.settings.input_source.dq_df_input_source_settings import DqDfInputSourceSettings
from organon.idq.domain.settings.input_source.dq_file_input_source_settings import DqFileInputSourceSettings
from organon.idq.domain.settings.partition_info import PartitionInfo


class RecordSourceFilterHelperTestCase(unittest.TestCase):
    """Test class for RecordSourceFilterHelper"""

    def setUp(self) -> None:
        pd_read_csv_patcher = patch(record_source_filter_helper.__name__ + ".pd.read_csv")
        self.mock_pd_read_csv = pd_read_csv_patcher.start()
        self.addCleanup(pd_read_csv_patcher.stop)

    def test_read_csv_with_chunks(self):
        """tests read_csv_with_chunks method"""
        settings = DqFileInputSourceSettings(DqFileRecordSource(locator="some file"))
        settings.csv_separator = ","
        settings.included_columns = None
        settings.number_of_rows_per_step = 10
        settings.date_columns = ["a"]
        mock_return = MagicMock()
        self.mock_pd_read_csv.return_value = mock_return
        df1 = pd.DataFrame({"a": [1, 2, 3]})
        df2 = pd.DataFrame({"a": [4, 5, 6]})
        mock_return.__next__.side_effect = [df1, df2]
        with patch.object(RecordSourceFilterHelper, "get_filtered_dataframe") as mock_get_filtered:
            mock_get_filtered.side_effect = lambda df, *args: df.iloc[[0, 1]]
            ret_val = RecordSourceFilterHelper.read_csv_with_chunks(settings)
            args = self.mock_pd_read_csv.call_args.args
            kwargs = self.mock_pd_read_csv.call_args.kwargs
            self.assertEqual(args[0], "some file")
            self.assertEqual(["a"], kwargs["parse_dates"])
            cnt = 0
            for val in ret_val:
                if cnt == 0:
                    pd.testing.assert_frame_equal(df1.iloc[[0, 1]], val)
                else:
                    pd.testing.assert_frame_equal(df2.iloc[[0, 1]], val)
                cnt += 1

        settings.included_columns = ["a", "b", "c"]
        RecordSourceFilterHelper.read_csv_with_chunks(settings)
        kwargs = self.mock_pd_read_csv.call_args.kwargs
        self.assertEqual(["a", "b", "c"], kwargs["usecols"])

    def test_get_filtered_dataframe(self):
        """tests get_filtered_dataframe"""
        partition_info = PartitionInfo()
        partition_info.column_name = "col1"
        partition_info.column_values = [1, 2, 3]

        partition_info2 = PartitionInfo()
        partition_info2.column_name = "col2"
        date_val_def = DateValueDefinition(year=2010, month=11, day=28, hour=15)
        partition_info2.column_values = [date_val_def]

        partition_info3 = PartitionInfo()
        partition_info3.column_name = "col3"
        partition_info3.column_values = ["a", "b", "c"]

        data_frame = pd.DataFrame({"col1": [1, 2, 1, 4, 3],
                                   "col2": [datetime(2010, 11, 28, 15, 1, 1), datetime(2010, 11, 28, 15, 12, 0),
                                            datetime(2015, 12, 11, 10, 1, 1), datetime(2015, 12, 11, 10, 1, 1),
                                            datetime(2010, 11, 28, 15, 10, 0)],
                                   "col3": ["a", "d", "b", "b", "e"]
                                   })
        settings = DqDfInputSourceSettings(DqDfRecordSource(data_frame, name="a"))
        settings.partition_info_list = [partition_info, partition_info2, partition_info3]
        filtered_df = RecordSourceFilterHelper.get_filtered_dataframe(data_frame, settings)
        self.assertEqual(1, len(filtered_df))
        self.assertEqual("a", filtered_df.iloc[0]["col3"])

    def test_get_filtered_dataframe_only_year_and_day(self):
        """tests get_filtered_dataframe - date column values with only year and day"""
        date_val_def = DateValueDefinition(year=2010, day=17)
        partition_info = PartitionInfo()
        partition_info.column_name = "col1"
        partition_info.column_values = [date_val_def]

        data_frame = pd.DataFrame({"col": [1, 2, 3, 4, 5],
                                   "col1": [datetime(2010, 11, 28, 15, 1, 1), datetime(2010, 11, 28, 15, 12, 0),
                                            datetime(2010, 12, 17, 10, 1, 1), datetime(2015, 12, 11, 10, 1, 1),
                                            datetime(2010, 11, 17, 15, 10, 0)],
                                   "col2": ["a", "d", "b", "b", "e"]
                                   })
        settings = DqDfInputSourceSettings(DqDfRecordSource(data_frame, name="a"))
        settings.partition_info_list = [partition_info]
        filtered_df = RecordSourceFilterHelper.get_filtered_dataframe(data_frame, settings)
        self.assertEqual(2, len(filtered_df))
        self.assertCountEqual([3, 5], filtered_df["col"].tolist())

    def test_get_filtered_dataframe_dates_single_digit(self):
        """get_filtered_dataframe - date column values with with single digit month,day etc."""
        date_val_def = DateValueDefinition(year=2010, month=3, day=7, hour=2)
        partition_info = PartitionInfo()
        partition_info.column_name = "col1"
        partition_info.column_values = [date_val_def]
        data_frame = pd.DataFrame({"col": [1, 2, 3, 4, 5],
                                   "col1": [datetime(2010, 11, 28, 15, 1, 1), datetime(2010, 11, 28, 15, 12, 0),
                                            datetime(2010, 12, 17, 10, 1, 1), datetime(2015, 12, 11, 10, 1, 1),
                                            datetime(2010, 3, 7, 2, 10, 0)]
                                   })
        settings = DqDfInputSourceSettings(DqDfRecordSource(data_frame, name="a"))
        settings.partition_info_list = [partition_info]
        filtered_df = RecordSourceFilterHelper.get_filtered_dataframe(data_frame, settings)
        self.assertEqual(1, len(filtered_df))
        self.assertCountEqual([5], filtered_df["col"].tolist())

    def test_get_filtered_dataframe_dates_date_values_with_different_tokens(self):
        """get_filtered_dataframe - when DateValueDefinition instances have different tokens"""
        partition_info = PartitionInfo()
        partition_info.column_name = "col2"
        date_val_def = DateValueDefinition(year=2010, day=28, hour=15)
        date_val_def2 = DateValueDefinition(year=2015, day=13, hour=12)
        date_val_def3 = DateValueDefinition(month=11, day=20, hour=15)
        partition_info.column_values = [date_val_def, date_val_def2, date_val_def3]

        partition_info2 = PartitionInfo()
        partition_info2.column_name = "col1"
        partition_info2.column_values = [1, 2, 3]

        data_frame = pd.DataFrame({"col1": [1, 2, 3, 4, 1],
                                   "col2": [datetime(2010, 11, 28, 15, 1, 1), datetime(2015, 10, 13, 12, 15, 0),
                                            datetime(2010, 11, 20, 15, 1, 1), datetime(2015, 12, 11, 10, 1, 1),
                                            datetime(2010, 3, 7, 2, 10, 0)],
                                   "col3": ["a", "b", "c", "d", "e"]
                                   })
        settings = DqDfInputSourceSettings(DqDfRecordSource(data_frame, name="a"))
        settings.partition_info_list = [partition_info, partition_info2]
        filtered_df = RecordSourceFilterHelper.get_filtered_dataframe(data_frame, settings)
        self.assertEqual(3, len(filtered_df))
        self.assertCountEqual(["a", "b", "c"], filtered_df["col3"].tolist())

    def test_get_filtered_dataframe_with_filter_callable(self):
        """tests get_filtered_dataframe when filter_callable is given"""
        data_frame = pd.DataFrame({"id": [1, 2, 3, 4, 5],
                                   "col1": [1, 2, 3, 4, 1],
                                   "col2": [datetime(2010, 11, 28, 15, 1, 1), datetime(2015, 10, 13, 12, 15, 0),
                                            datetime(2010, 11, 20, 15, 1, 1), datetime(2015, 12, 11, 10, 1, 1),
                                            datetime(2010, 3, 7, 2, 10, 0)],
                                   "col3": ["a", "b", "c", "d", "e"]
                                   })
        settings = DqDfInputSourceSettings(DqDfRecordSource(data_frame, name="a"))
        settings.filter_callable = lambda df: df[(df["col1"] >= 4) | df["col3"].isin(["a", "b", "c"])]
        filtered_df = RecordSourceFilterHelper.get_filtered_dataframe(data_frame, settings)
        self.assertEqual(4, len(filtered_df))
        self.assertCountEqual([1, 2, 3, 4], filtered_df["id"].tolist())

    def test_get_filtered_dataframe_with_partition_and_filter_callable(self):
        """tests get_filtered_dataframe with partition and filter callable"""
        partition_info = PartitionInfo()
        partition_info.column_name = "col1"
        partition_info.column_values = [1, 2, 3]

        data_frame = pd.DataFrame({"col1": [1, 2, 1, 4, 3],
                                   "col2": [datetime(2010, 11, 28, 15, 1, 1), datetime(2010, 11, 28, 15, 12, 0),
                                            datetime(2015, 12, 11, 10, 1, 1), datetime(2015, 12, 11, 10, 1, 1),
                                            datetime(2010, 11, 28, 15, 10, 0)],
                                   "col3": ["a", "d", "b", "b", "e"]
                                   })
        settings = DqDfInputSourceSettings(DqDfRecordSource(data_frame, name="a"))
        settings.partition_info_list = [partition_info]
        settings.filter_callable = lambda df: df[df["col3"] == "b"]
        filtered_df = RecordSourceFilterHelper.get_filtered_dataframe(data_frame, settings)
        self.assertEqual(1, len(filtered_df))
        self.assertEqual("b", filtered_df.iloc[0]["col3"])

    def test_get_filtered_dataframe_filter_callable_error(self):
        """tests get_filtered_dataframe with filter callable which raises an Exception"""
        data_frame = pd.DataFrame({"col1": [1, 2, 1, 4, 3],
                                   "col2": [datetime(2010, 11, 28, 15, 1, 1), datetime(2010, 11, 28, 15, 12, 0),
                                            datetime(2015, 12, 11, 10, 1, 1), datetime(2015, 12, 11, 10, 1, 1),
                                            datetime(2010, 11, 28, 15, 10, 0)],
                                   "col3": ["a", "d", "b", "b", "e"]
                                   })
        settings = DqDfInputSourceSettings(DqDfRecordSource(data_frame, name="a"))

        def _filter_call(dataframe):
            dataframe = dataframe[dataframe["col1"]>2]
            raise Exception("abcdef")

        settings.filter_callable = _filter_call
        with self.assertRaisesRegex(KnownException, "filter_callable"):
            RecordSourceFilterHelper.get_filtered_dataframe(data_frame, settings)

    def test_get_filtered_dataframe_filter_callable_wrong_return_value_error(self):
        """tests get_filtered_dataframe with filter callable which does not return a dataframe"""
        data_frame = pd.DataFrame({"col1": [1, 2, 1, 4, 3],
                                   "col2": [datetime(2010, 11, 28, 15, 1, 1), datetime(2010, 11, 28, 15, 12, 0),
                                            datetime(2015, 12, 11, 10, 1, 1), datetime(2015, 12, 11, 10, 1, 1),
                                            datetime(2010, 11, 28, 15, 10, 0)],
                                   "col3": ["a", "d", "b", "b", "e"]
                                   })
        settings = DqDfInputSourceSettings(DqDfRecordSource(data_frame, name="a"))
        settings.filter_callable = lambda df: 5
        with self.assertRaisesRegex(KnownException, "filter_callable.*Returned: int.*"):
            RecordSourceFilterHelper.get_filtered_dataframe(data_frame, settings)


if __name__ == '__main__':
    unittest.main()
