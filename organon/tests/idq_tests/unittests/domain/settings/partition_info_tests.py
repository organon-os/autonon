"""This module includes PartitionInfo tests"""
import unittest
from unittest.mock import MagicMock, patch

from organon.idq.domain.settings.date_value_definition import DateValueDefinition
from organon.idq.domain.settings.partition_info import PartitionInfo


class PartitionInfoTestCase(unittest.TestCase):
    """Unittests for PartitionInfo"""

    def setUp(self) -> None:
        partition_info_1 = PartitionInfo()
        partition_info_1.column_name = "int_col"
        partition_info_1.column_values = [1, 4, 3]

        partition_info_2 = PartitionInfo()
        partition_info_2.column_name = "date_col"
        date_val_def = DateValueDefinition()
        date_val_def.year = 2010
        date_val_def.month = 11
        date_val_def.day = 3
        date_val_def.hour = 16
        partition_info_2.column_values = [None, date_val_def]
        self.partitions = [partition_info_1, partition_info_2]

    def test_to_str(self):
        """tests to_str method"""
        with patch.object(PartitionInfo, 'get_col_val_str',
                          MagicMock(side_effect=["(1,2,3)", "(None,{y=2010,m=11,d=3,h=16})"])):
            self.assertEqual(self.partitions[0].to_str(), "[int_col=(1,2,3)]")
            self.assertEqual(self.partitions[1].to_str(), "[date_col=(None,{y=2010,m=11,d=3,h=16})]")

    def test_get_col_val_str(self):
        """tests get_col_val_str method"""
        self.assertEqual(self.partitions[0].get_col_val_str(), "(1,4,3)")
        self.assertEqual(self.partitions[1].get_col_val_str(), "(None,{y=2010,m=11,d=3,h=16})")
