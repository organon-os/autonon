"""Includes tests for DqUserInput"""
import unittest

import pandas as pd

from organon.idq.services.user_settings.dq_user_input import DqUserInput
from organon.idq.services.user_settings.user_date_value_definition import UserDateValueDefinition
from organon.idq.services.user_settings.user_multi_partition_calculation_params \
    import UserMultiPartitionCalcParams
from organon.idq.services.user_settings.user_partition_info import UserPartitionInfo


class DqUserInputTestCase(unittest.TestCase):
    """Unittest class for DqUserInput"""

    def test_set_comparison_params_invalid_input(self):
        """tests if ValueError is raised in set_comparison_params when an argument is given with wrong type"""
        dq_inp = DqUserInput()
        with self.assertRaisesRegex(ValueError, "minimum_cardinality should be an instance of 'int'"):
            dq_inp.set_comparison_params(minimum_cardinality="a")

    def test_set_calculation(self):
        """tests set_calculation method"""
        dq_inp = DqUserInput()
        dframe = pd.DataFrame({"a": [1, 2, 3],
                               "b": ["a", "b", "c"]
                               })
        dq_inp.set_multi_partition_calculation(
            {
                "source": dframe,
                "name": "my_df1",
                "sampling_ratio": 1.0,
                "max_num_of_samples": 100,
            },
            name="calc_1",
        )

        first_calc_params = dq_inp.calc_params
        self.assertIsInstance(first_calc_params, UserMultiPartitionCalcParams)
        self.assertEqual("calc_1", first_calc_params.calculation_name)
        self.assertEqual("my_df1", first_calc_params.input_source_settings.name)
        self.assertEqual(1.0, first_calc_params.input_source_settings.sampling_ratio)

    def test_set_calculation_with_partitions(self):
        """tests partition info list is read and set properly from user input"""
        inp = DqUserInput()
        inp.set_multi_partition_calculation(
            {
                "source": "asfas",
                "sampling_ratio": 0.8,
                "max_num_of_samples": 120
            },
            partitions=[
                [
                    {
                        "column_name": "c",
                        "column_values": [None, {"year": 2010, "day": 1}, {"month": 2, "hour": 13}],
                    },
                    {
                        "column_name": "a",
                        "column_values": [1, 2, None]
                    }
                ]
            ],
            name="calc_2"
        )
        calc_params = inp.calc_params
        self.assertIsInstance(calc_params.partitions, list)
        for partition_group in calc_params.partitions:
            self.assertIsInstance(partition_group, list)
            for info in partition_group:
                self.assertIsInstance(info, UserPartitionInfo)
        first_partition_info = calc_params.partitions[0][0]
        for val in first_partition_info.column_values:
            if val is not None:
                self.assertIsInstance(val, UserDateValueDefinition)
        self.assertIsNone(first_partition_info.column_values[0])
        self.assertEqual(2010, first_partition_info.column_values[1].year)
        self.assertEqual(1, first_partition_info.column_values[1].day)
        self.assertEqual(2, first_partition_info.column_values[2].month)
        self.assertEqual(13, first_partition_info.column_values[2].hour)

    def test_set_calculation_invalid_input(self):
        """tests if ValueError is raised in add_calculation when an argument is given with wrong type"""
        dq_inp = DqUserInput()
        df2 = pd.DataFrame({"a": [1.4, 2.2, 3.5],
                            "b": ["a", "b", "c"]
                            })
        with self.assertRaisesRegex(ValueError, "name should be an instance of 'str'"):
            dq_inp.set_multi_partition_calculation(
                {
                    "source": df2,
                    "name": "my_df1",
                    "sampling_ratio": 1,
                    "max_num_of_samples": 100,
                },
                name=1,
            )

    def test_set_calculation_invalid_attr_for_input_source_setting(self):
        """tests if ValueError is raised when invalid attribute is given for input source"""
        dq_inp = DqUserInput()
        with self.assertRaisesRegex(ValueError, "'dummy' is not a valid attribute for.*"):
            dq_inp.set_multi_partition_calculation(
                {
                    "source": {"connection_name": "OrganonPostgre",
                               "schema_name": "public", "table_name": "adult_train",
                               "dummy": "sdfas"},
                    "name": "my_df1",
                    "sampling_ratio": 1.0,
                    "max_num_of_samples": 100,
                }
            )

    def test_set_calculation_filter_callable_errors(self):
        """tests if ValueError is raised when filter_callable is invalid"""
        dq_inp = DqUserInput()
        dframe = pd.DataFrame({"a": [1, 2, 3],
                               "b": ["a", "b", "c"]
                               })
        with self.assertRaisesRegex(ValueError, "filter_callable"):
            dq_inp.set_multi_partition_calculation(
                {
                    "source": dframe,
                    "name": "my_df1",
                    "filter_callable": "asdfas"
                },
                name="calc_1",
            )
        with self.assertRaisesRegex(ValueError, "filter_callable"):
            dq_inp.set_multi_partition_calculation(
                {
                    "source": dframe,
                    "name": "my_df1",
                    "filter_callable": lambda df, abc: df
                },
                name="calc_1",
            )


if __name__ == '__main__':
    unittest.main()
