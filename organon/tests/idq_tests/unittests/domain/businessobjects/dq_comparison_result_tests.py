"""Includes tests for DqComparisonResult class."""
import unittest

from organon.idq.core.enums.data_entity_type import DataEntityType
from organon.idq.core.enums.dq_comparison_result_code import DqComparisonResultCode
from organon.idq.domain.businessobjects.dq_comparison_result import DqComparisonResult
from organon.idq.domain.enums.dq_test_group_type import DqTestGroupType
from organon.idq.domain.enums.signal_type import SignalType


class DqComparisonResultTestCase(unittest.TestCase):
    """Test class for DqComparisonResult class."""

    def setUp(self) -> None:
        self.comp_result = DqComparisonResult(
            DataEntityType.TABLE, "some table",
            DqTestGroupType.TABLE_SCHEMA_CONTROLS,
            DqComparisonResultCode.TABLE_IS_EMPTY, "sadfasd", "asdfdf"
        )
        self.comp_result.signal_type = SignalType.RED

    def test_set_property_key_str(self):
        """tests set_property_key when arguemnt is a string"""
        self.comp_result.set_property_key("testkey")
        self.assertEqual("testkey", self.comp_result.property_key_nominal)
        self.assertIsNone(self.comp_result.property_key_numeric)

    def test_set_property_key_float(self):
        """tests set_property_key when arguemnt is a float"""
        self.comp_result.set_property_key(1.5)
        self.assertEqual(1.5, self.comp_result.property_key_numeric)
        self.assertIsNone(self.comp_result.property_key_nominal)

    def test_set_property_key_value_pair_str_str(self):
        """tests set_property_key_value_pair when arguments are of types string,string"""
        self.comp_result.set_property_key_value_pair("a", "b")
        self.assertEqual("a", self.comp_result.property_key_nominal)
        self.assertEqual("b", self.comp_result.property_value_nominal)
        self.assertIsNone(self.comp_result.property_key_numeric)
        self.assertIsNone(self.comp_result.property_value_numeric)

    def test_set_property_key_value_pair_str_float(self):
        """tests set_property_key_value_pair when arguments are of types string,float"""
        self.comp_result.set_property_key_value_pair("a", 1.8)
        self.assertEqual("a", self.comp_result.property_key_nominal)
        self.assertEqual(1.8, self.comp_result.property_value_numeric)
        self.assertIsNone(self.comp_result.property_key_numeric)
        self.assertIsNone(self.comp_result.property_value_nominal)

    def test_set_property_key_value_pair_float_str(self):
        """tests set_property_key_value_pair when arguments are of types float,string"""
        self.comp_result.set_property_key_value_pair(1.8, "a")
        self.assertEqual(1.8, self.comp_result.property_key_numeric)
        self.assertEqual("a", self.comp_result.property_value_nominal)
        self.assertIsNone(self.comp_result.property_key_nominal)
        self.assertIsNone(self.comp_result.property_value_numeric)

    def test_set_property_key_value_pair_float_float(self):
        """tests set_property_key_value_pair when arguments are of types float,float"""
        self.comp_result.set_property_key_value_pair(1.2, 1.8)
        self.assertEqual(1.2, self.comp_result.property_key_numeric)
        self.assertEqual(1.8, self.comp_result.property_value_numeric)
        self.assertIsNone(self.comp_result.property_key_nominal)
        self.assertIsNone(self.comp_result.property_value_nominal)

    def test_set_property_key_invalid_type(self):
        """tests if valueerror is raised when key is not of expected types"""
        with self.assertRaisesRegex(ValueError, "key.*"):
            self.comp_result.set_property_key(True)

    def test_set_property_key_value_pair_invalid_type(self):
        """tests if valueerror is raised when value is not of expected types"""
        with self.assertRaisesRegex(ValueError, "Value.*"):
            self.comp_result.set_property_key_value_pair("a", True)


if __name__ == '__main__':
    unittest.main()
