"""Includes tests for DqControlParameters"""
import unittest

from organon.fl.core.enums.column_native_type import ColumnNativeType
from organon.idq.domain.businessobjects.data_column.dq_data_column import DqDataColumn
from organon.idq.domain.businessobjects.dq_calculation_result import DqCalculationResult
from organon.idq.domain.businessobjects.dq_control_parameters import DqControlParameters
from organon.idq.domain.businessobjects.dq_data_column_collection import DqDataColumnCollection
from organon.idq.domain.businessobjects.statistics.data_source_statistics import DataSourceStatistics
from organon.idq.domain.settings.comparison.dq_df_comparison_parameters import DqDfComparisonParameters
from organon.idq.domain.settings.dq_comparison_column_info import DqComparisonColumnInfo


def _get_data_col(col_name: str, col_native_type: ColumnNativeType):
    col = DqDataColumn()
    col.column_name = col_name
    col.column_native_type = col_native_type
    return col


def _get_test_calc_result():
    calc_result = DqCalculationResult()
    calc_result.data_source_stats = DataSourceStatistics()
    col_collection = DqDataColumnCollection()
    col_collection.add(_get_data_col("col1", ColumnNativeType.Numeric))
    col_collection.add(_get_data_col("col2", ColumnNativeType.String))
    col_collection.add(_get_data_col("col3", ColumnNativeType.Date))
    col_collection.add(_get_data_col("col4", ColumnNativeType.Other))
    calc_result.data_source_stats.data_column_collection = col_collection
    return calc_result


def _get_comp_col(col_name: str):
    comp_col = DqComparisonColumnInfo()
    comp_col.column_name = col_name
    return comp_col


def _get_comparison_parameters():
    comp_params = DqDfComparisonParameters()
    comp_params.comparison_columns = [_get_comp_col("col1"),
                                      _get_comp_col("col2"),
                                      _get_comp_col("col3"),
                                      _get_comp_col("col4")]
    return comp_params


class DqControlParametersTestCase(unittest.TestCase):
    """Test class for DqControlParameters"""

    def setUp(self):
        control_params = DqControlParameters()
        control_params.comparison_parameters = _get_comparison_parameters()
        control_params.test_calc_result = _get_test_calc_result()
        self.control_params = control_params

    def test_get_comparison_cols_by_native_type(self):
        """tests get_comparison_cols_by_native_type method"""
        numeric_cols = self.control_params.get_comparison_cols_by_native_type(ColumnNativeType.Numeric)
        self.assertEqual(1, len(numeric_cols))
        self.assertEqual("col1", numeric_cols[0].column_name)


if __name__ == '__main__':
    unittest.main()
