"""Includes tests for DqDataColumnCollection class."""
import unittest

from organon.fl.core.enums.column_native_type import ColumnNativeType
from organon.idq.domain.businessobjects.data_column.dq_df_data_column import DqDfDataColumn
from organon.idq.domain.businessobjects.dq_data_column_collection import DqDataColumnCollection


def get_dq_data_column(name: str, native_type: ColumnNativeType):
    """Generate a DqDataColumn object."""
    col = DqDfDataColumn()
    col.column_name = name
    col.column_native_type = native_type
    return col


class DqDataColumnCollectionTestCase(unittest.TestCase):
    """Test class for DqDataColumnCollection"""

    def setUp(self) -> None:
        self.col_collection = DqDataColumnCollection()
        self.col_collection.column_list = [get_dq_data_column("a", ColumnNativeType.String),
                                           get_dq_data_column("b", ColumnNativeType.Numeric)]

    def test_add(self):
        """tests add method"""
        col = get_dq_data_column("c", ColumnNativeType.String)
        self.col_collection.add(col)
        self.assertEqual("c", self.col_collection.column_list[-1].column_name)

    def test_remove(self):
        """tests remove method"""
        self.col_collection.remove("b")
        self.assertEqual(1, len(self.col_collection.column_list))
        self.assertEqual("a", self.col_collection.column_list[0].column_name)

    def test_remove_not_found(self):
        """tests remove method when column not in collection"""
        self.col_collection.remove("g")
        self.assertEqual(2, len(self.col_collection.column_list))

    def test_get_columns(self):
        """tests get_columns method"""
        cols = self.col_collection.get_columns(ColumnNativeType.String)
        self.assertEqual(1, len(cols))
        self.assertEqual("a", cols[0].column_name)

    def test_get_column_by_name(self):
        """tests get_column method"""
        col = self.col_collection.get_column("b")
        self.assertEqual("b", col.column_name)
        self.assertEqual(ColumnNativeType.Numeric, col.column_native_type)

    def test_get_column_by_name_not_found(self):
        """tests get_column method when column is not in collection"""
        self.assertIsNone(self.col_collection.get_column("g"))

    def test_get_eligible_columns(self):
        """tests get_eligible_columns method"""
        self.col_collection.column_list.append(get_dq_data_column("c", ColumnNativeType.Other))
        eligible_cols = self.col_collection.get_eligible_columns()
        self.assertEqual(2, len(eligible_cols))
        for col in eligible_cols:
            self.assertNotEqual("c", col.column_name)

    def test_get_partition_columns(self):
        """tests get_partition_columns method"""
        self.col_collection.column_list.append(get_dq_data_column("c", ColumnNativeType.Other))
        col = get_dq_data_column("d", ColumnNativeType.String)
        self.col_collection.column_list.append(col)
        self.col_collection.column_list.append(get_dq_data_column("e", ColumnNativeType.String))
        partition_cols = self.col_collection.get_partition_columns()
        self.assertEqual(4, len(partition_cols))
        for col in partition_cols:
            self.assertNotEqual(col.column_name, "c")


if __name__ == '__main__':
    unittest.main()
