"""Includes tests for PsiNumericSignalControl"""
import unittest
from typing import List
from unittest.mock import MagicMock, patch

import pandas as pd

from organon.fl.core.businessobjects.dataframe import DataFrame
from organon.fl.core.collections.sorted_dict import SortedDict
from organon.fl.mathematics.businessobjects.sets.interval import Interval
from organon.fl.mathematics.businessobjects.sets.sum_triple import SumTriple
from organon.fl.mathematics.enums.sets.interval_type import IntervalType
from organon.idq.core.enums.dq_comparison_result_code import DqComparisonResultCode
from organon.idq.domain.businessobjects.dq_calculation_result import DqCalculationResult
from organon.idq.domain.businessobjects.dq_comparison_result import DqComparisonResult
from organon.idq.domain.businessobjects.dq_control_parameters import DqControlParameters
from organon.idq.domain.businessobjects.dq_test_group import DqTestGroup
from organon.idq.domain.businessobjects.statistics.dq_sample_numerical_statistics import DqSampleNumericalStatistics
from organon.idq.domain.businessobjects.statistics.sample_statistics import SampleStatistics
from organon.idq.domain.controls import psi_numeric_signal_control
from organon.idq.domain.controls.psi_numeric_signal_control import PsiNumericSignalControl
from organon.idq.domain.helpers.statistics.dq_statistical_functions import DqStatisticalFunctions
from organon.idq.domain.settings.abstractions.dq_base_comparison_parameters import DqBaseComparisonParameters
from organon.idq.domain.settings.calculation.dq_df_calculation_parameters import DqDfCalculationParameters
from organon.idq.domain.settings.dq_comparison_column_info import DqComparisonColumnInfo

_PSI_NUMERIC_SIGNAL_CONTROL = psi_numeric_signal_control.__name__


def _get_sample_data():
    data = {"col1": [0.9, 1, 1, 1.2, 1.2, 1.2, 3, 3, 3.2, 3.2, 3.2, 3.2, 3.2, 4.5, 4.5, 4.5, 4.5, 4.5, 4.5,
                     7.0, 7.0, 7.0],
            "col2": [2.0, 2.4, 2.4, 2.4, 2.4, 2.4, 3.5, 3.5, 3.5, 3.5, 3.5, 3.5, 4.2, 4.2, 4.2, 4.2, 4.2, 4.2, 4.2, 4.2,
                     5.5, 5.5]}
    frame = DataFrame()
    frame.data_frame = pd.DataFrame(data)
    return frame


def _generate_interval_stats(lower_bound, upper_bound, interval_type, count, sum_, sum_of_squares):
    interval_stats = SortedDict()
    interval = Interval("", lower_bound, upper_bound, interval_type)
    sum_triple = SumTriple(count, sum_, sum_of_squares)
    interval_stats[interval] = sum_triple
    return interval_stats


def _get_stats(min_val, max_val, frequencies, interval_statistics):
    stats = DqSampleNumericalStatistics()
    stats.min = min_val
    stats.max = max_val
    stats.frequencies = frequencies
    stats.interval_statistics = interval_statistics
    return stats


def _get_calculation_results():
    calc_1_res = DqCalculationResult()
    calc_1_res.sample_stats = SampleStatistics()

    calc_1_res.sample_stats.numerical_statistics = {"col1": _get_stats(1.5, 6.2,
                                                                       {1.5: 25, 2: 2, 5: 6, 6: 20, 6.2: 2},
                                                                       interval_statistics
                                                                       =_generate_interval_stats(1, 6,
                                                                                                 IntervalType
                                                                                                 .OPEN_CLOSED,
                                                                                                 5, 16, 64)),
                                                    "col2": _get_stats(1.2, 4.7,
                                                                       {1.2: 25, 2: 2, 3: 20, 4: 5, 4.7: 1},
                                                                       interval_statistics
                                                                       =_generate_interval_stats(2, 4.5,
                                                                                                 IntervalType
                                                                                                 .OPEN_CLOSED,
                                                                                                 3, 6, 12))
                                                    }
    calc_1_res.calculation_name = "Calc1"
    calc_2_res = DqCalculationResult()
    calc_2_res.sample_stats = SampleStatistics()
    calc_2_res.sample_stats.numerical_statistics = {"col1": _get_stats(1.1, 5.6,
                                                                       {1.1: 2, 2: 2, 5: 6, 5.6: 20},
                                                                       interval_statistics
                                                                       =_generate_interval_stats(1, 5,
                                                                                                 IntervalType
                                                                                                 .OPEN_OPEN,
                                                                                                 5, 15, 55)
                                                                       ),
                                                    "col2": _get_stats(2.3, 4.9,
                                                                       {2.3: 2, 2.5: 1, 2.6: 1, 3: 1, 3.2: 1,
                                                                        3.5: 1,
                                                                        4.9: 1},
                                                                       interval_statistics
                                                                       =_generate_interval_stats(2, 5,
                                                                                                 IntervalType.OPEN_OPEN,
                                                                                                 8, 22, 68)
                                                                       )}
    calc_2_res.calculation_name = "Calc2"
    calc_3_res = DqCalculationResult()
    calc_3_res.sample_stats = SampleStatistics()
    calc_3_res.sample_stats.numerical_statistics = {"col1": _get_stats(0.9, 7.0,
                                                                       {0.9: 1, 1: 2, 1.2: 3, 3: 2, 3.2: 5, 4.5: 6,
                                                                        7.0: 3},
                                                                       interval_statistics
                                                                       =_generate_interval_stats(1, 6,
                                                                                                 IntervalType
                                                                                                 .OPEN_CLOSED,
                                                                                                 5, 16, 64)),
                                                    "col2": _get_stats(2.0, 5.5,
                                                                       {2.0: 1, 2.4: 5, 3.5: 6,
                                                                        4.2: 8, 5.5: 2},
                                                                       interval_statistics
                                                                       =_generate_interval_stats(2, 5,
                                                                                                 IntervalType.OPEN_OPEN,
                                                                                                 8,
                                                                                                 22,
                                                                                                 68)
                                                                       )}
    calc_3_res.calculation_name = "Calc3"
    calc_3_res.sample_data = _get_sample_data()
    return [calc_1_res, calc_2_res, calc_3_res]


def _get_test_group_info(len_calcs):
    test_group = DqTestGroup()
    test_group.test_bmh = len_calcs
    test_group.group_type = PsiNumericSignalControl.get_test_group_type()
    test_group.inclusion_flag = True
    return {
        PsiNumericSignalControl.get_test_group_type(): test_group
    }


class PsiNumericSignalControlTestCase(unittest.TestCase):
    """Test class for PsiNumericSignalControl"""

    def setUp(self) -> None:
        self.dq_stat_funcs_patcher = patch(f"{_PSI_NUMERIC_SIGNAL_CONTROL}.{DqStatisticalFunctions.__name__}")
        self.dq_stat_funcs_mock = self.dq_stat_funcs_patcher.start()
        self.control_params = DqControlParameters()
        calc_results = _get_calculation_results()
        comp_params = DqBaseComparisonParameters()
        comp_params.psi_threshold_green = 0.2
        comp_params.psi_threshold_yellow = 0.4
        self.control_params.comparison_parameters = comp_params

        self.control_params.control_results = calc_results[:-1]
        self.control_params.test_calc_result = calc_results[-1]
        self.control_params.test_group_info = _get_test_group_info(len(calc_results))
        self.control_params.test_calc_parameters = DqDfCalculationParameters()
        self.control_params.test_calc_parameters.is_existing_calculation = False
        comp_col = DqComparisonColumnInfo()
        comp_col.column_name = "col1"
        comp_col.benchmark_horizon = len(calc_results)
        comp_col2 = DqComparisonColumnInfo()
        comp_col2.column_name = "col2"
        comp_col2.benchmark_horizon = len(calc_results)
        self.comparison_columns = [comp_col, comp_col2]
        self.control_params.get_comparison_cols_by_native_type = MagicMock(return_value=self.comparison_columns)

    def tearDown(self) -> None:
        self.dq_stat_funcs_patcher.stop()

    def _get_control(self):
        return PsiNumericSignalControl(self.control_params)

    def test_execute_control(self):
        """execute psi nominal signal control for one columns of red signal"""

        self.dq_stat_funcs_mock.population_stability_index.side_effect = [(3, 0.2), (6, 0.1), (2, 0.6), (3, 0.8)]
        results = self._get_control().execute()
        self.assertEqual(len(results), 22)
        result_codes = [result.result_code for result in results]
        expected_result_codes = [DqComparisonResultCode.PSI_YELLOW_SIGNAL for i in range(11)]
        expected_result_codes.extend([DqComparisonResultCode.PSI_RED_SIGNAL for i in range(11)])
        self.assertEqual(expected_result_codes, result_codes)
        col_names = [result.data_entity_name for result in results]
        expected_col_names = ["col1" for i in range(11)]
        expected_col_names.extend(["col2" for i in range(11)])
        self.assertEqual(col_names, expected_col_names)
        self.check_comparison_results_for_property_key_nominal(results, "PsiSignal", "col1", "YELLOW")
        self.check_comparison_results_for_property_key_numeric(results, "MaximumPsi", "col1", 0.2)
        self.check_comparison_results_for_property_key_nominal(results, "MaximumPsiCategory", "col1", 3)
        self.check_comparison_results_for_property_key_nominal(results, "MaximumPsiInterval", "col1", 3)
        self.check_comparison_results_for_property_key_nominal(results, "MaximumPsiControlSet", "col1", 'Calc2')

        self.check_comparison_results_for_property_key_nominal(results, "PsiSignal", "col2", "RED")
        self.check_comparison_results_for_property_key_numeric(results, "MaximumPsi", "col2", 0.8)
        self.check_comparison_results_for_property_key_nominal(results, "MaximumPsiCategory", "col2", 3)
        self.check_comparison_results_for_property_key_nominal(results, "MaximumPsiInterval", "col2", 3)
        self.check_comparison_results_for_property_key_nominal(results, "MaximumPsiControlSet", "col2", 'Calc1')

    def test_comp_col_bmh(self):
        """tests when comparison column has a  bmh which is lower then calculation count"""
        self.comparison_columns[0].benchmark_horizon = 1
        self.dq_stat_funcs_mock.population_stability_index.side_effect = [(3, 0.1), (6, 0.2), (2, 0.6)]
        results = self._get_control().execute()
        result_codes = [result.result_code for result in results]
        expected_result_codes = [DqComparisonResultCode.PSI_RED_SIGNAL for i in range(11)]
        self.assertEqual(11, len(results))
        self.assertEqual(expected_result_codes, result_codes)

        self.check_comparison_results_for_property_key_nominal(results, "PsiSignal", "col2", "RED")
        self.check_comparison_results_for_property_key_numeric(results, "MaximumPsi", "col2", 0.6)
        self.check_comparison_results_for_property_key_nominal(results, "MaximumPsiCategory", "col2", 2)
        self.check_comparison_results_for_property_key_nominal(results, "MaximumPsiInterval", "col2", 2)
        self.check_comparison_results_for_property_key_nominal(results, "MaximumPsiControlSet", "col2", 'Calc1')

    def test_test_group_bmh(self):
        """tests when test_group has a bmh which is lower then calculation count"""
        self.dq_stat_funcs_mock.population_stability_index.side_effect = [(3, 0.1), (6, 0.2)]
        self.control_params.test_group_info[PsiNumericSignalControl.get_test_group_type()].test_bmh = 1
        results = self._get_control().execute()
        self.assertEqual(10, len(results))  # calc_3 compared only with calc_2
        result_codes = [result.result_code for result in results]
        expected_result_codes = [DqComparisonResultCode.PSI_YELLOW_SIGNAL for i in range(10)]
        self.assertEqual(expected_result_codes, result_codes)
        self.check_comparison_results_for_property_key_nominal(results, "PsiSignal", "col2", "YELLOW")
        self.check_comparison_results_for_property_key_numeric(results, "MaximumPsi", "col2", 0.2)
        self.check_comparison_results_for_property_key_nominal(results, "MaximumPsiCategory", "col2", 6)
        self.check_comparison_results_for_property_key_nominal(results, "MaximumPsiInterval", "col2", 6)
        self.check_comparison_results_for_property_key_nominal(results, "MaximumPsiControlSet", "col2", 'Calc2')

    def test_when_existing_calculation(self):
        """Tests if any signals generated when test calculation is an existing calculation
        and contains no sample data in results"""
        self.control_params.test_calc_parameters.is_existing_calculation = True
        self.control_params.test_calc_result.sample_data = None
        self.assertEqual([], self._get_control().execute())

    # pylint: disable=inconsistent-return-statements
    def check_comparison_results_for_property_key_nominal(self, results: List[DqComparisonResult], property_code,
                                                          col_name: str,
                                                          property_key_nominal):
        """Checks if results contains a result with given values"""
        for res in results:
            if res.property_key_nominal == property_key_nominal and res.data_entity_name == col_name \
                    and res.property_code == property_code:
                return res
        self.fail("Expected comparison result not found")

    # pylint: disable=inconsistent-return-statements
    def check_comparison_results_for_property_key_numeric(self, results: List[DqComparisonResult], property_code,
                                                          col_name: str,
                                                          property_key_numeric):
        """Checks if results contains a result with given values"""
        for res in results:
            if res.property_key_numeric == property_key_numeric and res.data_entity_name == col_name \
                    and res.property_code == property_code:
                return res
        self.fail("Expected comparison result not found")
