"""This module includes DuplicateKeysControlTestCase class"""
import unittest
from unittest.mock import patch

import pandas as pd

from organon.idq.domain.businessobjects.dq_calculation_result import DqCalculationResult
from organon.idq.domain.businessobjects.dq_control_parameters import DqControlParameters
from organon.idq.domain.businessobjects.record_sources.dq_df_record_source import DqDfRecordSource
from organon.idq.domain.businessobjects.record_sources.dq_file_record_source import DqFileRecordSource
from organon.idq.domain.businessobjects.statistics.data_source_statistics import DataSourceStatistics
from organon.idq.domain.controls import duplicate_keys_control
from organon.idq.domain.controls.duplicate_keys_control import DuplicateKeysControl
from organon.idq.domain.settings.calculation.dq_df_calculation_parameters import DqDfCalculationParameters
from organon.idq.domain.settings.calculation.dq_file_calculation_parameters import DqFileCalculationParameters
from organon.idq.domain.settings.comparison.dq_df_comparison_parameters import DqDfComparisonParameters
from organon.idq.domain.settings.comparison.dq_file_comparison_parameters import DqFileComparisonParameters
from organon.idq.domain.settings.dq_comparison_column_info import DqComparisonColumnInfo
from organon.idq.domain.settings.input_source.dq_df_input_source_settings import DqDfInputSourceSettings
from organon.idq.domain.settings.input_source.dq_file_input_source_settings import DqFileInputSourceSettings
from organon.tests.helpers.read_csv_object import ReadCsvObject


def _get_comparison_column(col_name: str, duplicate_col_control=False):
    col = DqComparisonColumnInfo()
    col.column_name = col_name
    col.duplicate_column_control = duplicate_col_control
    return col


_MODULE_NAME = duplicate_keys_control.__name__


class DuplicateKeysControlTestCase(unittest.TestCase):
    """Class for DuplicateKeysControl tests"""

    def setUp(self) -> None:
        pd_read_csv_patcher = patch(_MODULE_NAME + ".pd.read_csv")
        self.mock_pd_read_csv = pd_read_csv_patcher.start()
        self.addCleanup(pd_read_csv_patcher.stop)

    @staticmethod
    def _get_control_params(test_calculation_parameters, comparison_parameters, row_count):
        control_params = DqControlParameters()
        control_params.test_calc_result = DqCalculationResult()
        control_params.test_calc_result.data_source_stats = DataSourceStatistics()
        control_params.test_calc_result.data_source_stats.row_count = row_count
        control_params.test_calc_parameters = test_calculation_parameters
        control_params.comparison_parameters = comparison_parameters
        return control_params

    def test_df(self):
        """tests control execution for df source"""
        test_data = pd.DataFrame({"a": [1, 2, 3, 3, 2], "b": [1, 3, 4, 4, 5], "c": ["a", "b", "c", "c", "b"]})
        test_calc_parameters = DqDfCalculationParameters()
        test_calc_parameters.input_source_settings = DqDfInputSourceSettings(DqDfRecordSource(test_data,
                                                                                              "mydf"))
        comparison_parameters = DqDfComparisonParameters()
        comparison_parameters.comparison_columns = [_get_comparison_column("a", True),
                                                    _get_comparison_column("b"),
                                                    _get_comparison_column("c", True)]
        control_params = self._get_control_params(test_calc_parameters, comparison_parameters, 5)
        results = DuplicateKeysControl(control_params).execute()
        self.assertEqual(1, len(results))
        self.assertEqual("mydf", results[0].data_entity_name)
        self.assertIn("Row count: 5", results[0].message)
        self.assertIn("Cardinality: 3", results[0].message)
        self.assertIn("Number of rows with duplicate keys: 2", results[0].message)

    def test_df_no_duplicate(self):
        """tests control execution when no duplicate keys exist"""
        test_data = pd.DataFrame({"a": [1, 2, 3, 4, 5], "b": [1, 2, 3, 4, 5], "c": ["a", "b", "c", "d", "e"]})
        test_calc_parameters = DqDfCalculationParameters()
        test_calc_parameters.input_source_settings = DqDfInputSourceSettings(DqDfRecordSource(test_data,
                                                                                              "mydf"))
        comparison_parameters = DqDfComparisonParameters()
        comparison_parameters.comparison_columns = [_get_comparison_column("a", True),
                                                    _get_comparison_column("b"),
                                                    _get_comparison_column("c", True)]
        control_params = self._get_control_params(test_calc_parameters, comparison_parameters, 4)
        results = DuplicateKeysControl(control_params).execute()
        self.assertEqual(0, len(results))

    def test_no_key_columns(self):
        """tests control execution when no key columns exist"""
        test_data = pd.DataFrame({"a": [1, 2, 3, 3, 2], "b": [1, 3, 4, 4, 5], "c": ["a", "b", "c", "c", "b"]})
        test_calc_parameters = DqDfCalculationParameters()
        test_calc_parameters.input_source_settings = DqDfInputSourceSettings(DqDfRecordSource(test_data,
                                                                                              "mydf"))
        comparison_parameters = DqDfComparisonParameters()
        comparison_parameters.comparison_columns = [_get_comparison_column("a"),
                                                    _get_comparison_column("b"),
                                                    _get_comparison_column("c")]
        control_params = self._get_control_params(test_calc_parameters, comparison_parameters, 4)
        results = DuplicateKeysControl(control_params).execute()
        self.assertEqual(0, len(results))

    def test_text(self):
        """tests control execution for text source"""
        test_data = pd.DataFrame({"a": [1, 2, 3, 3, 2], "b": [1, 3, 4, 4, 5], "c": ["a", "b", "c", "c", "b"]})
        self.mock_pd_read_csv.return_value = ReadCsvObject(test_data, chunksize=2)
        record_source = DqFileRecordSource("some file")
        test_calc_parameters = DqFileCalculationParameters()
        test_calc_parameters.input_source_settings = DqFileInputSourceSettings(record_source)
        comparison_parameters = DqFileComparisonParameters()
        comparison_parameters.comparison_columns = [_get_comparison_column("a", True),
                                                    _get_comparison_column("b"),
                                                    _get_comparison_column("c", True)]
        control_params = self._get_control_params(test_calc_parameters, comparison_parameters, 5)
        results = DuplicateKeysControl(control_params).execute()
        self.assertEqual(1, len(results))
        self.assertIn("some file", results[0].data_entity_name)
        self.assertIn("Row count: 5", results[0].message)
        self.assertIn("Cardinality: 3", results[0].message)
        self.assertIn("Number of rows with duplicate keys: 2", results[0].message)

    def test_text_no_duplicate(self):
        """tests control execution for text source when no duplicate keys exist"""
        test_data = pd.DataFrame({"a": [1, 2, 3, 4, 5], "b": [1, 2, 3, 4, 5], "c": ["a", "b", "c", "d", "e"]})
        self.mock_pd_read_csv.return_value = ReadCsvObject(test_data, chunksize=2)
        record_source = DqFileRecordSource("some file")
        test_calc_parameters = DqFileCalculationParameters()
        test_calc_parameters.input_source_settings = DqFileInputSourceSettings(record_source)
        comparison_parameters = DqFileComparisonParameters()
        comparison_parameters.comparison_columns = [_get_comparison_column("a", True),
                                                    _get_comparison_column("b"),
                                                    _get_comparison_column("c", True)]
        control_params = self._get_control_params(test_calc_parameters, comparison_parameters, 5)
        results = DuplicateKeysControl(control_params).execute()
        self.assertEqual(0, len(results))
