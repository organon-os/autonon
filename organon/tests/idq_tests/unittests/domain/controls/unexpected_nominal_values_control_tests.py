"""Includes tests for UnexpectedNominalValuesControl"""
import unittest
from datetime import datetime
from typing import Dict, List
from unittest.mock import MagicMock, patch

from organon.fl.core.enums.column_native_type import ColumnNativeType
from organon.fl.logging.helpers.log_helper import LogHelper
from organon.idq.domain.businessobjects.dq_calculation_result import DqCalculationResult
from organon.idq.domain.businessobjects.dq_comparison_result import DqComparisonResult
from organon.idq.domain.businessobjects.dq_control_parameters import DqControlParameters
from organon.idq.domain.businessobjects.dq_test_group import DqTestGroup
from organon.idq.domain.businessobjects.statistics.dq_categorical_statistics import DqCategoricalStatistics
from organon.idq.domain.businessobjects.statistics.population_statistics import PopulationStatistics
from organon.idq.domain.controls import unexpected_nominal_values_control
from organon.idq.domain.controls.unexpected_nominal_values_control import UnexpectedNominalValuesControl
from organon.idq.domain.settings.calculation.dq_df_calculation_parameters import DqDfCalculationParameters
from organon.idq.domain.settings.dq_comparison_column_info import DqComparisonColumnInfo


def _get_stats(missing_values: list, frequencies: Dict[object, float]):
    stats = DqCategoricalStatistics(missing_values, frequencies)
    return stats


def _get_calculation_results():
    calc_1_res = DqCalculationResult()
    calc_1_res.population_stats = PopulationStatistics()

    calc_1_res.population_stats.nominal_statistics = {"col1": _get_stats([], {"a": 5, "b": 2}),
                                                      "col2": _get_stats([], {datetime(2010, 1, 1): 5,
                                                                              datetime(2010, 1, 2): 3})
                                                      }

    calc_2_res = DqCalculationResult()
    calc_2_res.population_stats = PopulationStatistics()
    calc_2_res.population_stats.nominal_statistics = {"col1": _get_stats([], {"a": 5}),
                                                      "col2": _get_stats([], {datetime(2010, 1, 1): 5})
                                                      }

    calc_3_res = DqCalculationResult()
    calc_3_res.population_stats = PopulationStatistics()
    calc_3_res.population_stats.nominal_statistics = {"col1": _get_stats([], {"a": 5, "b": 2, "c": 3, "d": 3}),
                                                      "col2": _get_stats([], {datetime(2010, 1, 1): 5,
                                                                              datetime(2010, 1, 2): 3,
                                                                              datetime(2010, 1, 3): 3})
                                                      }

    return [calc_1_res, calc_2_res, calc_3_res]


def _get_test_group_info(len_calcs):
    test_group = DqTestGroup()
    test_group.test_bmh = len_calcs
    test_group.group_type = UnexpectedNominalValuesControl.get_test_group_type()
    test_group.inclusion_flag = True
    return {
        UnexpectedNominalValuesControl.get_test_group_type(): test_group
    }


class UnexpectedNominalValuesControlTestCase(unittest.TestCase):
    """Test class for UnexpectedNominalValuesControl"""

    def setUp(self) -> None:
        self.control_params = DqControlParameters()
        calc_results = _get_calculation_results()
        self.control_params.test_calc_parameters = DqDfCalculationParameters()
        self.control_params.test_calc_parameters.nominal_column_types = [ColumnNativeType.String, ColumnNativeType.Date]
        self.control_params.control_results = calc_results[:-1]
        self.control_params.test_calc_result = calc_results[-1]
        self.control_params.test_group_info = _get_test_group_info(len(calc_results))
        comp_col = DqComparisonColumnInfo()
        comp_col.column_name = "col1"
        comp_col.benchmark_horizon = len(calc_results)
        comp_col2 = DqComparisonColumnInfo()
        comp_col2.column_name = "col2"
        comp_col2.benchmark_horizon = len(calc_results)
        comp_col3 = DqComparisonColumnInfo()
        comp_col3.column_name = "col3"
        comp_col3.benchmark_horizon = len(calc_results)
        self.comparison_columns = [comp_col, comp_col2, comp_col3]
        self.control_params.get_comparison_cols_by_native_type = MagicMock(side_effect=[[comp_col, comp_col3],
                                                                                        [comp_col2]])

    def _get_control(self):
        return UnexpectedNominalValuesControl(self.control_params)

    def test_default_data(self):
        """tests if with default calculation result data"""
        results = self._get_control().execute()
        self.assertEqual(3, len(results))
        self.check_comparison_results(results, "col1", "c")
        self.check_comparison_results(results, "col1", "d")
        self.check_comparison_results(results, "col2", str(datetime(2010, 1, 3)))

    def test_comp_col_bmh(self):
        """tests when comparison column has a  bmh which is lower then calculation count"""
        self.comparison_columns[0].benchmark_horizon = 1
        results = self._get_control().execute()
        self.assertEqual(4, len(results))
        self.check_comparison_results(results, "col1", "b")  # t_calc compared only with calc_2 for column col1
        self.check_comparison_results(results, "col1", "c")
        self.check_comparison_results(results, "col1", "d")
        self.check_comparison_results(results, "col2", str(datetime(2010, 1, 3)))

    def test_test_group_bmh(self):
        """tests when test_group has a bmh which is lower then calculation count"""
        self.control_params.test_group_info[UnexpectedNominalValuesControl.get_test_group_type()].test_bmh = 1
        results = self._get_control().execute()
        self.assertEqual(5, len(results))  # calc_3 compared only with calc_2
        self.check_comparison_results(results, "col1", "b")
        self.check_comparison_results(results, "col1", "c")
        self.check_comparison_results(results, "col1", "d")
        self.check_comparison_results(results, "col2", str(datetime(2010, 1, 2)))
        self.check_comparison_results(results, "col2", str(datetime(2010, 1, 3)))

    def test_message_unexpected_value_is_empty_string(self):
        """tests if message is generated properly when empty string is in unexpected values"""
        self.control_params.test_calc_result.population_stats.nominal_statistics["col1"].frequencies[""] = 3
        results = self._get_control().execute()
        self.assertEqual(4, len(results))
        result = self.check_comparison_results(results, "col1", "")
        self.assertIn("col1", result.message)
        self.assertIn("NULL", result.message.split("col1")[1])

    def test_message_unexpected_value_is_none(self):
        """tests if message is generated properly when None is in unexpected values"""
        self.control_params.test_calc_result.population_stats.nominal_statistics["col1"].frequencies[None] = 3
        results = self._get_control().execute()
        self.assertEqual(4, len(results))
        result = self.check_comparison_results(results, "col1", None)
        self.assertIn("col1", result.message)
        self.assertIn("NULL", result.message.split("col1")[1])

    @patch(unexpected_nominal_values_control.__name__ + "." + LogHelper.__name__)
    def test_no_stats_log(self, mock_log_helper):
        """tests if info is logged when there is test_stats_collection is None"""
        self.control_params.test_calc_result.population_stats.nominal_statistics = None
        self._get_control().execute()
        mock_log_helper.info.assert_called_with("Test statistics collection is null, column values tests skipped")

    # pylint: disable=inconsistent-return-statements
    def check_comparison_results(self, results: List[DqComparisonResult], col_name: str, property_key_nominal):
        """Checks if results contains a result with given values"""
        for res in results:
            if res.property_key_nominal == property_key_nominal and res.data_entity_name == col_name:
                return res
        self.fail("Expected comparison result not found")


if __name__ == '__main__':
    unittest.main()
