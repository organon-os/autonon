"""Includes tests for DataSourceStatsTrafficLightControl class."""
import unittest
from typing import List
from unittest.mock import patch, MagicMock

import pandas as pd

from organon.idq.domain.algorithms.objects.mean_ci_comparison_input import MeanConfidenceIntervalComparisonInput
from organon.idq.domain.algorithms.objects.traffic_light_comparison_input import TrafficLightComparisonInput
from organon.idq.domain.businessobjects.dq_calculation_result import DqCalculationResult
from organon.idq.domain.businessobjects.dq_control_parameters import DqControlParameters
from organon.idq.domain.businessobjects.dq_test_group import DqTestGroup
from organon.idq.domain.businessobjects.record_sources.dq_df_record_source import DqDfRecordSource
from organon.idq.domain.businessobjects.statistics.data_source_statistics import DataSourceStatistics
from organon.idq.domain.controls import data_source_stats_traffic_light_control
from organon.idq.domain.controls.data_source_stats_traffic_light_control import DataSourceStatsTrafficLightControl
from organon.idq.domain.controls.helpers.traffic_light_comparer import TrafficLightComparer
from organon.idq.domain.settings.calculation.dq_df_calculation_parameters import DqDfCalculationParameters
from organon.idq.domain.settings.comparison.dq_df_comparison_parameters import DqDfComparisonParameters
from organon.idq.domain.settings.input_source.dq_df_input_source_settings import DqDfInputSourceSettings


def _get_test_group_info(len_calcs):
    test_group = DqTestGroup()
    test_group.test_bmh = len_calcs
    test_group.group_type = DataSourceStatsTrafficLightControl.get_test_group_type()
    test_group.inclusion_flag = True
    return {
        DataSourceStatsTrafficLightControl.get_test_group_type(): test_group
    }


def _get_calculation_results(row_counts: List[int]):
    results = []
    for count in row_counts:
        calc_1_res = DqCalculationResult()
        calc_1_res.data_source_stats = DataSourceStatistics()
        calc_1_res.data_source_stats.row_count = count
        results.append(calc_1_res)
    return results


_MODULE_NAME = data_source_stats_traffic_light_control.__name__


class DataSourceStatsTrafficLightControlTestCase(unittest.TestCase):
    """Test class for DataSourceStatsTrafficLightControl."""

    def setUp(self) -> None:
        self.record_source = DqDfRecordSource(pd.DataFrame(), name="dummy_table")
        self.control_params = DqControlParameters()
        self.control_params.test_calc_parameters = DqDfCalculationParameters()
        self.control_params.test_calc_parameters.input_source_settings = DqDfInputSourceSettings(self.record_source)

        self.control_params.test_group_info = _get_test_group_info(4)

        self.control_params.control_results = _get_calculation_results([3, 4, 2, 3])
        self.control_params.test_calc_result = _get_calculation_results([1])[0]

        comparison_parameters = DqDfComparisonParameters()
        comparison_parameters.traffic_light_threshold_yellow = 0.5
        comparison_parameters.traffic_light_threshold_green = 0.7
        self.control_params.comparison_parameters = comparison_parameters

        traffic_light_comparer_patcher = patch(_MODULE_NAME + "." + TrafficLightComparer.__name__)
        self.traffic_light_comparer_mock = traffic_light_comparer_patcher.start()
        self.addCleanup(traffic_light_comparer_patcher.stop)

    def test_execute(self):
        """tests execute"""
        comp_res1 = MagicMock(name="mock result 1")
        comp_res2 = MagicMock(name="mock result 2")
        self.traffic_light_comparer_mock.get_mi_tl_comparison_results.return_value = [comp_res1, comp_res2]
        results = DataSourceStatsTrafficLightControl(self.control_params).execute()
        ci_args, tl_args = self.traffic_light_comparer_mock.get_mi_tl_comparison_results.call_args.args
        self.assertIsInstance(ci_args, MeanConfidenceIntervalComparisonInput)
        self.assertIsInstance(tl_args, TrafficLightComparisonInput)

        self.assertIn("dummy_table", ci_args.data_entity_name)
        self.assertListEqual([3.0, 4.0, 2.0, 3.0], ci_args.past_series)
        self.assertEqual(1.0, ci_args.current_value)
        self.assertIn("dummy_table", tl_args.data_entity_name)
        self.assertListEqual([3.0, 4.0, 2.0, 3.0], tl_args.past_series)
        self.assertEqual(1.0, tl_args.current_value)

        self.assertEqual(2, len(results))
        self.assertEqual(comp_res1, results[0])
        self.assertEqual(comp_res2, results[1])

    def test_execute_bmh_less_than_calc_count(self):
        """tests execute when test_bmh is less than calculation count"""

        comp_res1 = MagicMock(name="mock result 1")
        comp_res2 = MagicMock(name="mock result 2")
        self.traffic_light_comparer_mock.get_mi_tl_comparison_results.return_value = [comp_res1, comp_res2]
        self.control_params.test_group_info[DataSourceStatsTrafficLightControl.get_test_group_type()].test_bmh = 2
        results = DataSourceStatsTrafficLightControl(self.control_params).execute()
        ci_args, tl_args = self.traffic_light_comparer_mock.get_mi_tl_comparison_results.call_args.args
        self.assertIsInstance(ci_args, MeanConfidenceIntervalComparisonInput)
        self.assertIsInstance(tl_args, TrafficLightComparisonInput)

        self.assertIn("dummy_table", ci_args.data_entity_name)
        self.assertListEqual([2.0, 3.0], ci_args.past_series)
        self.assertEqual(1.0, ci_args.current_value)
        self.assertIn("dummy_table", tl_args.data_entity_name)
        self.assertListEqual([2.0, 3.0], tl_args.past_series)
        self.assertEqual(1.0, tl_args.current_value)

        self.assertEqual(2, len(results))
        self.assertEqual(comp_res1, results[0])
        self.assertEqual(comp_res2, results[1])

    def test_execute_invalid_bmh(self):
        """tests if ValueError is raised when test group bmh is less than 1"""
        self.control_params.test_group_info[DataSourceStatsTrafficLightControl.get_test_group_type()].test_bmh = 0
        with self.assertRaisesRegex(ValueError, "Bmh should be greater than or equal to 1"):
            DataSourceStatsTrafficLightControl(self.control_params).execute()


if __name__ == "__main__":
    unittest.main()
