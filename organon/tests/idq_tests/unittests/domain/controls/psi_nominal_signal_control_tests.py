"""Includes tests for PsiNominalSignalControl"""
import unittest
from typing import List
from unittest.mock import MagicMock, patch

from organon.fl.core.enums.column_native_type import ColumnNativeType
from organon.idq.core.enums.dq_comparison_result_code import DqComparisonResultCode
from organon.idq.domain.businessobjects.dq_calculation_result import DqCalculationResult
from organon.idq.domain.businessobjects.dq_comparison_result import DqComparisonResult
from organon.idq.domain.businessobjects.dq_control_parameters import DqControlParameters
from organon.idq.domain.businessobjects.dq_test_group import DqTestGroup
from organon.idq.domain.businessobjects.statistics.dq_categorical_statistics import DqCategoricalStatistics
from organon.idq.domain.businessobjects.statistics.population_statistics import PopulationStatistics
from organon.idq.domain.controls import psi_nominal_signal_control
from organon.idq.domain.controls.psi_nominal_signal_control import PsiNominalSignalControl
from organon.idq.domain.helpers.statistics.dq_statistical_functions import DqStatisticalFunctions
from organon.idq.domain.settings.calculation.dq_df_calculation_parameters import DqDfCalculationParameters
from organon.idq.domain.settings.comparison.dq_df_comparison_parameters import DqDfComparisonParameters
from organon.idq.domain.settings.dq_comparison_column_info import DqComparisonColumnInfo

_PSI_NOMINAL_SIGNAL_CONTROL = psi_nominal_signal_control.__name__


def _get_calculation_results():
    calc_1_res = DqCalculationResult()
    calc_1_res.population_stats = PopulationStatistics()
    nominal_dict = {}
    nominal_stats_1 = DqCategoricalStatistics([])
    frequencies_dict = {"EDU": 5, "ADU": 10}
    nominal_stats_1.frequencies = frequencies_dict
    nominal_dict["col3"] = nominal_stats_1
    nominal_stats_2 = DqCategoricalStatistics([])
    frequencies_dict = {"BR": 5, "SCR": 15, "ASCR": 20}
    nominal_stats_2.frequencies = frequencies_dict
    nominal_dict["col4"] = nominal_stats_2
    calc_1_res.population_stats.nominal_statistics = nominal_dict
    calc_1_res.calculation_name = "Calc1"
    calc_2_res = DqCalculationResult()
    calc_2_res.population_stats = PopulationStatistics()
    nominal_dict_2 = {}
    nominal_stats_3 = DqCategoricalStatistics([])
    frequencies_dict = {"EDU": 3, "ADU": 9}
    nominal_stats_3.frequencies = frequencies_dict
    nominal_dict_2["col3"] = nominal_stats_3
    nominal_stats_4 = DqCategoricalStatistics([])
    frequencies_dict = {"BR": 5, "FPR": 15, "ASCR": 20}
    nominal_stats_4.frequencies = frequencies_dict
    nominal_dict_2["col4"] = nominal_stats_4

    calc_2_res.population_stats.nominal_statistics = nominal_dict_2
    calc_2_res.calculation_name = "Calc2"

    calc_3_res = DqCalculationResult()
    calc_3_res.population_stats = PopulationStatistics()
    nominal_dict_3 = {}
    nominal_stats_5 = DqCategoricalStatistics([])
    frequencies_dict = {"EDU": 6, "ADU": 9}
    nominal_stats_5.frequencies = frequencies_dict
    nominal_dict_3["col3"] = nominal_stats_5
    nominal_stats_6 = DqCategoricalStatistics([])
    frequencies_dict = {"BR": 5, "FPR": 15, "ASCR": 21}
    nominal_stats_6.frequencies = frequencies_dict
    nominal_dict_3["col4"] = nominal_stats_6
    calc_3_res.population_stats.nominal_statistics = nominal_dict_3
    calc_3_res.calculation_name = "Calc3"

    return [calc_1_res, calc_2_res, calc_3_res]


def _get_test_group_info(len_calcs):
    test_group = DqTestGroup()
    test_group.test_bmh = len_calcs
    test_group.group_type = PsiNominalSignalControl.get_test_group_type()
    test_group.inclusion_flag = True
    return {
        PsiNominalSignalControl.get_test_group_type(): test_group
    }


class PsiNominalSignalControlTestCase(unittest.TestCase):
    """Test class for PsiNominalSignalControl"""

    def setUp(self) -> None:
        self.dq_stat_funcs_patcher = patch(f"{_PSI_NOMINAL_SIGNAL_CONTROL}.{DqStatisticalFunctions.__name__}")
        self.dq_stat_funcs_mock = self.dq_stat_funcs_patcher.start()
        self.control_params = DqControlParameters()
        calc_results = _get_calculation_results()
        comp_params = DqDfComparisonParameters()
        comp_params.psi_threshold_green = 0.2
        comp_params.psi_threshold_yellow = 0.4
        self.control_params.comparison_parameters = comp_params

        self.control_params.control_results = calc_results[:-1]
        self.control_params.test_calc_result = calc_results[-1]
        self.control_params.test_group_info = _get_test_group_info(len(calc_results))

        comp_col = DqComparisonColumnInfo()
        comp_col.column_name = "col3"
        comp_col.benchmark_horizon = len(calc_results)
        comp_col_2 = DqComparisonColumnInfo()
        comp_col_2.column_name = "col4"
        comp_col_2.benchmark_horizon = len(calc_results)
        self.comparison_columns = [comp_col, comp_col_2]
        self.control_params.get_nominal_comparison_columns = MagicMock(return_value=self.comparison_columns)
        self.control_params.test_calc_parameters = DqDfCalculationParameters()
        self.control_params.test_calc_parameters.nominal_column_types = [ColumnNativeType.String, ColumnNativeType.Date]

    def _get_control(self):
        return PsiNominalSignalControl(self.control_params)

    def test_execute_control_one_column_red_signal(self):
        """execute psi nominal signal control for one columns of red signal"""

        self.dq_stat_funcs_mock.population_stability_index.side_effect = [(3, 0.1), (6, 0.1), (2, 0.6), (3, 0.8)]
        results = self._get_control().execute()
        self.assertEqual(len(results), 11)
        result_codes = [result.result_code for result in results]
        expected_result_codes = [DqComparisonResultCode.PSI_RED_SIGNAL for i in range(11)]
        self.assertEqual(expected_result_codes, result_codes)
        col_names = [result.data_entity_name for result in results]
        expected_col_names = ["col4" for i in range(11)]
        self.assertEqual(col_names, expected_col_names)

        self.check_comparison_results_for_property_key_numeric_with_prop_code(results, "MaximumPsi", "col4", 0.8)
        self.check_comparison_results_for_property_key_nominal_with_prop_code(results, "MaximumPsiCategory", "col4", 3)
        self.check_comparison_results_for_property_key_nominal_with_prop_code(results, "MaximumPsiControlSet", "col4",
                                                                              'Calc1')

        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnTestFrequencies', "BR", 5)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnTestFrequencies', "FPR", 15)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnTestFrequencies', "ASCR", 21)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnMaximumPsiControlFrequencies',
                                                               "BR", 5)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnMaximumPsiControlFrequencies',
                                                               "SCR", 15)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnMaximumPsiControlFrequencies',
                                                               "ASCR", 20)

    def test_execute_control_one_column_yellow_signal(self):
        """execute psi nominal signal control for one columns of yellow signal"""

        self.control_params.test_calc_result.population_stats.nominal_statistics['col4'].frequencies = {"BR": 5,
                                                                                                        "FPR": 15,
                                                                                                        "ASCR": 20}
        self.control_params.control_results[0].population_stats.nominal_statistics['col4'].frequencies = {"BR": 5,
                                                                                                          "FPR": 15,
                                                                                                          "ASCR": 20}
        self.control_params.control_results[1].population_stats.nominal_statistics['col4'].frequencies = {"BR": 5,
                                                                                                          "FPR": 5,
                                                                                                          "ASCR": 20}
        self.dq_stat_funcs_mock.population_stability_index.side_effect = [(3, 0.1), (6, 0.1), (2, 0.2), (3, 0)]

        results = self._get_control().execute()
        self.assertEqual(len(results), 11)
        result_codes = [result.result_code for result in results]
        expected_result_codes = [DqComparisonResultCode.PSI_YELLOW_SIGNAL for i in range(11)]
        self.assertEqual(expected_result_codes, result_codes)
        col_names = [result.data_entity_name for result in results]
        expected_col_names = ["col4" for i in range(11)]
        self.assertEqual(col_names, expected_col_names)
        self.check_comparison_results_for_property_key_numeric_with_prop_code(results, "MaximumPsi", "col4", 0.2)
        self.check_comparison_results_for_property_key_nominal_with_prop_code(results, "MaximumPsiCategory", "col4", 2)
        self.check_comparison_results_for_property_key_nominal_with_prop_code(results, "MaximumPsiControlSet", "col4",
                                                                              'Calc2')

        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnTestFrequencies', "BR", 5)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnTestFrequencies', "FPR", 15)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnTestFrequencies', "ASCR", 20)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnMaximumPsiControlFrequencies',
                                                               "BR", 5)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnMaximumPsiControlFrequencies',
                                                               "FPR", 5)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnMaximumPsiControlFrequencies',
                                                               "ASCR", 20)

    def test_execute_control_one_column_no_signal(self):
        """execute psi nominal signal control for no signal"""
        self.dq_stat_funcs_mock.population_stability_index.side_effect = [(3, 0.1), (6, 0.1), (2, 0.1), (3, 0.1)]

        self.control_params.test_calc_result.population_stats.nominal_statistics['col4'].frequencies = {"BR": 5,
                                                                                                        "FPR": 15,
                                                                                                        "ASCR": 20}
        self.control_params.control_results[0].population_stats.nominal_statistics['col4'].frequencies = {"BR": 5,
                                                                                                          "FPR": 15,
                                                                                                          "ASCR": 20}
        self.control_params.control_results[1].population_stats.nominal_statistics['col4'].frequencies = {"BR": 5,
                                                                                                          "FPR": 15,
                                                                                                          "ASCR": 20}
        results = self._get_control().execute()
        self.assertEqual(len(results), 0)

    def test_execute_control_two_columns_signal(self):
        """execute psi nominal signal control for 2 columns of signal"""

        self.control_params.test_calc_result.population_stats.nominal_statistics['col4'].frequencies = {"BR": 5,
                                                                                                        "FPR": 15,
                                                                                                        "ASCR": 20}
        self.control_params.control_results[0].population_stats.nominal_statistics['col4'].frequencies = {"BR": 5,
                                                                                                          "FPR": 15,
                                                                                                          "ASCR": 20}
        self.control_params.control_results[1].population_stats.nominal_statistics['col4'].frequencies = {"BR": 5,
                                                                                                          "FPR": 5,
                                                                                                          "ASCR": 20}
        self.control_params.test_calc_result.population_stats.nominal_statistics['col3'].frequencies = {"EDU": 6,
                                                                                                        "ADU": 20}
        self.control_params.control_results[0].population_stats.nominal_statistics['col3'].frequencies = {"EDU": 6,
                                                                                                          "ADU": 9}
        self.control_params.control_results[1].population_stats.nominal_statistics['col3'].frequencies = {"EDU": 6,
                                                                                                          "ADU": 600}
        self.dq_stat_funcs_mock.population_stability_index.side_effect = [(3, 0.1), (6, 0.2), (2, 0.6), (3, 0.8)]

        results = self._get_control().execute()
        self.assertEqual(len(results), 20)
        result_codes = [result.result_code for result in results]
        result_code_check = bool((DqComparisonResultCode.PSI_YELLOW_SIGNAL in result_codes
                                  and DqComparisonResultCode.PSI_RED_SIGNAL in result_codes))
        col_names = [result.data_entity_name for result in results]
        self.assertEqual(True, result_code_check)
        col_names_check = bool("col3" in col_names and "col4" in col_names)
        self.assertEqual(True, col_names_check)
        self.check_comparison_results_for_property_key_numeric_with_prop_code(results, "MaximumPsi", "col3", 0.2)
        self.check_comparison_results_for_property_key_nominal_with_prop_code(results, "MaximumPsiCategory", "col3", 6)
        self.check_comparison_results_for_property_key_nominal_with_prop_code(results, "MaximumPsiControlSet", "col3",
                                                                              'Calc1')

        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnTestFrequencies', "EDU", 6)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnTestFrequencies', "ADU", 20)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnMaximumPsiControlFrequencies',
                                                               "EDU", 6)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnMaximumPsiControlFrequencies',
                                                               "ADU", 9)

        self.check_comparison_results_for_property_key_numeric_with_prop_code(results, "MaximumPsi", "col4", 0.8)
        self.check_comparison_results_for_property_key_nominal_with_prop_code(results, "MaximumPsiCategory", "col4", 3)
        self.check_comparison_results_for_property_key_nominal_with_prop_code(results, "MaximumPsiControlSet", "col4",
                                                                              'Calc1')

        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnTestFrequencies', "BR", 5)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnTestFrequencies', "FPR", 15)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnTestFrequencies', "ASCR", 20)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnMaximumPsiControlFrequencies',
                                                               "BR", 5)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnMaximumPsiControlFrequencies',
                                                               "FPR", 15)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnMaximumPsiControlFrequencies',
                                                               "ASCR", 20)

    @patch("organon.idq.domain.controls.psi_nominal_signal_control.LogHelper")
    def test_execute_test_nominal_stat_collection_is_none(self, mock_log_helper):
        """execute psi nominal signal control without test nominal stat collection"""
        self.control_params.test_calc_result.population_stats.nominal_statistics = None
        results = self._get_control().execute()
        self.assertEqual(len(results), 0)
        arg = mock_log_helper.info.call_args.args[0]
        self.assertEqual("Test statistics collection is null" in arg, True)

    def test_comp_col_bmh(self):
        """tests when comparison column has a  bmh which is lower then calculation count"""
        self.comparison_columns[0].benchmark_horizon = 1
        self.dq_stat_funcs_mock.population_stability_index.side_effect = [(3, 0.1), (6, 0.2), (2, 0.6)]

        results = self._get_control().execute()
        self.assertEqual(11, len(results))
        self.check_comparison_results_for_property_key_nominal(results, "col4", "BR")
        self.check_comparison_results_for_property_key_nominal(results, "col4", "FPR")
        self.check_comparison_results_for_property_key_nominal(results, "col4", "ASCR")
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnTestFrequencies', "BR", 5)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnTestFrequencies', "FPR", 15)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnTestFrequencies', "ASCR", 21)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnMaximumPsiControlFrequencies',
                                                               "BR", 5)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnMaximumPsiControlFrequencies',
                                                               "SCR", 15)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnMaximumPsiControlFrequencies',
                                                               "ASCR", 20)

    def test_test_group_bmh(self):
        """tests when test_group has a bmh which is lower then calculation count"""
        self.control_params.test_group_info[PsiNominalSignalControl.get_test_group_type()].test_bmh = 1
        self.dq_stat_funcs_mock.population_stability_index.side_effect = [(3, 0.1), (6, 0.2)]

        results = self._get_control().execute()
        self.assertEqual(10, len(results))  # calc_3 compared only with calc_2
        self.check_comparison_results_for_property_key_nominal(results, "col4", "BR")
        self.check_comparison_results_for_property_key_nominal(results, "col4", "FPR")
        self.check_comparison_results_for_property_key_nominal(results, "col4", "ASCR")
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnTestFrequencies', "BR", 5)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnTestFrequencies', "FPR", 15)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnTestFrequencies', "ASCR", 21)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnMaximumPsiControlFrequencies',
                                                               "BR", 5)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnMaximumPsiControlFrequencies',
                                                               "FPR", 15)
        self.check_comparison_results_for_property_key_numeric(results, 'NominalColumnMaximumPsiControlFrequencies',
                                                               "ASCR", 20)

    # pylint: disable=inconsistent-return-statements
    def check_comparison_results_for_property_key_nominal(self, results: List[DqComparisonResult], col_name: str,
                                                          property_key_nominal):
        """Checks if results contains a result with given values"""
        for res in results:
            if res.property_key_nominal == property_key_nominal and res.data_entity_name == col_name:
                return res
        self.fail("Expected comparison result not found")

    # pylint: disable=inconsistent-return-statements
    def check_comparison_results_for_property_key_numeric(self, results: List[DqComparisonResult], property_code: str,
                                                          property_key_nominal: str, property_key_numeric):
        """Checks if results contains a result with given values"""
        for res in results:
            if res.property_key_numeric == property_key_numeric and res.property_key_nominal == property_key_nominal \
                    and res.property_code == property_code:
                return res
        self.fail("Expected comparison result not found")

    # pylint: disable=inconsistent-return-statements
    def check_comparison_results_for_property_key_nominal_with_prop_code(self, results: List[DqComparisonResult],
                                                                         property_code,
                                                                         col_name: str,
                                                                         property_key_nominal):
        """Checks if results contains a result with given values"""
        for res in results:
            if res.property_key_nominal == property_key_nominal and res.data_entity_name == col_name \
                    and res.property_code == property_code:
                return res
        self.fail("Expected comparison result not found")

    # pylint: disable=inconsistent-return-statements
    def check_comparison_results_for_property_key_numeric_with_prop_code(self, results: List[DqComparisonResult],
                                                                         property_code,
                                                                         col_name: str,
                                                                         property_key_numeric):
        """Checks if results contains a result with given values"""
        for res in results:
            if res.property_key_numeric == property_key_numeric and res.data_entity_name == col_name \
                    and res.property_code == property_code:
                return res
        self.fail("Expected comparison result not found")
