"""This module includes StableColumnControlTestCase class"""
import unittest
from typing import Dict, List
from unittest.mock import MagicMock

import pandas as pd

from organon.fl.core.collections.sorted_dict import SortedDict
from organon.idq.domain.businessobjects.dq_calculation_result import DqCalculationResult
from organon.idq.domain.businessobjects.dq_comparison_result import DqComparisonResult
from organon.idq.domain.businessobjects.dq_control_parameters import DqControlParameters
from organon.idq.domain.businessobjects.record_sources.dq_df_record_source import DqDfRecordSource
from organon.idq.domain.businessobjects.statistics.dq_categorical_statistics import DqCategoricalStatistics
from organon.idq.domain.businessobjects.statistics.dq_sample_numerical_statistics import DqSampleNumericalStatistics
from organon.idq.domain.businessobjects.statistics.sample_statistics import SampleStatistics
from organon.idq.domain.controls.stable_column_control import StableColumnControl
from organon.idq.domain.settings.calculation.dq_df_calculation_parameters import DqDfCalculationParameters
from organon.idq.domain.settings.comparison.dq_df_comparison_parameters import DqDfComparisonParameters
from organon.idq.domain.settings.dq_comparison_column_info import DqComparisonColumnInfo
from organon.idq.domain.settings.input_source.dq_df_input_source_settings import DqDfInputSourceSettings


def get_sample_stats() -> SampleStatistics:
    """:returns sample stat"""
    sample_stats = SampleStatistics()
    numerical_statistics_dict = {}
    num_col_list = ["a", "b", "d", "e"]
    num_freq_list = [{1: 4}, {1: 1, 2: 1, 3: 1, 4: 1}, {1: 3, 2: 1}, {0: 4}]
    num_cardinality_list = [1, 4, 2, 1]
    for col, freq, cardinality in zip(num_col_list, num_freq_list, num_cardinality_list):
        numerical_stats = DqSampleNumericalStatistics()
        numerical_stats.n_val = len(freq)
        numerical_stats.n_miss = 0
        numerical_stats.frequencies = SortedDict(freq)
        numerical_stats.cardinality = cardinality
        numerical_stats.missing_values = [None]
        numerical_stats.missing_values_frequencies = {}
        numerical_stats.min = min(freq.keys())
        numerical_stats.max = max(freq.keys())
        numerical_statistics_dict[col] = numerical_stats

    sample_stats.numerical_statistics = numerical_statistics_dict

    nominal_statistics_dict = {}
    nom_col_list = ["c", "f"]
    nom_freq_list = [{None: 3, 'x': 1}, {None: 4}]
    nom_cardinality_list = [2, 1]
    for col, freq, cardinality in zip(nom_col_list, nom_freq_list, nom_cardinality_list):
        nominal_stats = DqCategoricalStatistics([])
        nominal_stats.frequencies = freq
        nominal_stats.cardinality = cardinality
        nominal_statistics_dict[col] = nominal_stats
    sample_stats.nominal_statistics = nominal_statistics_dict
    return sample_stats


def _get_comparison_column_info(col_name: str):
    info = DqComparisonColumnInfo()
    info.column_name = col_name
    return info


class StableColumnControlTestCase(unittest.TestCase):
    """Class for StableColumnControl tests"""

    def setUp(self) -> None:
        self.record_source = DqDfRecordSource(pd.DataFrame(), name="dummy")
        self.control_params = DqControlParameters()
        self.control_params.test_calc_parameters = DqDfCalculationParameters()
        self.control_params.test_calc_parameters.max_nominal_cardinality_count = 2
        self.control_params.test_calc_parameters.input_source_settings = DqDfInputSourceSettings(self.record_source)
        self.control_params.comparison_parameters = DqDfComparisonParameters()

        self.control_params.comparison_parameters.maximum_nom_cardinality = 2
        self.control_params.comparison_parameters.minimum_cardinality = 4
        self.control_params.test_calc_result = DqCalculationResult()
        self.control_params.test_calc_result.sample_stats = SampleStatistics()
        get_comp_cols_by_native_mock = MagicMock()
        get_comp_cols_by_native_mock.return_value = [_get_comparison_column_info("a"),
                                                     _get_comparison_column_info("b"),
                                                     _get_comparison_column_info("d"),
                                                     _get_comparison_column_info("e")]
        self.control_params.get_comparison_cols_by_native_type = get_comp_cols_by_native_mock
        self.control_params.get_nominal_comparison_columns = MagicMock(return_value=[_get_comparison_column_info("c"),
                                                                                     _get_comparison_column_info("f")])

    def test_check_data_source_empty_none_result(self):
        """test if ValueError is raised when check_data_source_empty is called with t_cal_result=None"""
        self.control_params.test_calc_result = None
        with self.assertRaisesRegex(ValueError, ".*T-Calculation result is None.*"):
            StableColumnControl(self.control_params).execute()

    def test_check_data_source_stats_none_result(self):
        """tests ValueError raises when source is None"""
        self.control_params.test_calc_result = DqCalculationResult()
        with self.assertRaisesRegex(ValueError, ".*T-Calculation result sample stats is None.*"):
            StableColumnControl(self.control_params).execute()

    def test_check_nominal_sample_stat_none_result(self):
        """tests ValueError raises when source is None"""
        self.control_params.test_calc_result = DqCalculationResult()
        self.control_params.test_calc_result.sample_stats = SampleStatistics()
        self.control_params.test_calc_result.sample_stats.numerical_statistics = Dict[str, DqSampleNumericalStatistics]
        with self.assertRaisesRegex(ValueError, ".*T-Calculation result nominal sample stats is None.*"):
            StableColumnControl(self.control_params).execute()

    def test_check_numeric_sample_stat_none_result(self):
        """tests ValueError raises when source is None"""
        calc_res = DqCalculationResult()
        calc_res.sample_stats = SampleStatistics()
        calc_res.sample_stats.nominal_statistics = Dict[str, DqCategoricalStatistics]
        self.control_params.test_calc_result = calc_res
        with self.assertRaisesRegex(ValueError, ".*T-Calculation result numeric sample stats is None.*"):
            StableColumnControl(self.control_params).execute()

    def test_check_data_source_parameters_none(self):
        """tests ValueError raises when source parameter is None"""
        calc_res = DqCalculationResult()
        calc_res.sample_stats = SampleStatistics()
        calc_res.sample_stats.nominal_statistics = Dict[str, DqCategoricalStatistics]
        calc_res.sample_stats.numerical_statistics = Dict[str, DqSampleNumericalStatistics]
        self.control_params.test_calc_result = calc_res
        self.control_params.comparison_parameters = None
        with self.assertRaisesRegex(ValueError, ".*T-Calculation comparison parameters is None..*"):
            StableColumnControl(self.control_params).execute()

    def test_check_max_min_cardinality_none(self):
        """tests when there are columns which has cardinality greater than threshold,
        if max cardinality parameter is none, returns no result"""
        calc_res = DqCalculationResult()
        calc_res.sample_stats = get_sample_stats()
        self.control_params.test_calc_result = calc_res
        self.control_params.comparison_parameters.maximum_nom_cardinality = None
        self.control_params.comparison_parameters.minimum_cardinality = None
        comp_results = StableColumnControl(self.control_params).execute()
        self.assertFalse(self.__check_signal_inside("CARDINALITY_EXCEEDS_MAXIMUM_CARDINALITY", comp_results))
        self.assertFalse(self.__check_signal_inside("CARDINALITY_IS_BELOW_MINIMUM_THRESHOLD", comp_results))
        self.control_params.comparison_parameters.minimum_cardinality = 1
        self.control_params.comparison_parameters.maximum_nom_cardinality = 2
        comp_results = StableColumnControl(self.control_params).execute()
        self.assertTrue(self.__check_signal_inside("CARDINALITY_EXCEEDS_MAXIMUM_CARDINALITY", comp_results))
        self.assertTrue(self.__check_signal_inside("CARDINALITY_IS_BELOW_MINIMUM_THRESHOLD", comp_results))

    @staticmethod
    def __check_signal_inside(signal: str, comp_results: List[DqComparisonResult]):
        """:returns True if given signal inside signal list"""
        return signal in [res.result_code.name for res in comp_results]

    @staticmethod
    def __get_signal_cols(signal: str, comp_results: List[DqComparisonResult]):
        """:returns cols that cause signal"""
        return [res.data_entity_name for res in comp_results if res.result_code.name == signal]

    @staticmethod
    def __get_signal_messages(signal: str, comp_results: List[DqComparisonResult]):
        """:returns messages of given signal in signal list"""
        return [res.message for res in comp_results if res.result_code.name == signal]

    def test_check_all_column_equal(self):
        """tests when there are stable columns with a value except None or zero,
        returns DqComparisonResultCode.COLUMN_IS_CONSTANT result"""
        calc_res = DqCalculationResult()
        calc_res.sample_stats = get_sample_stats()
        self.control_params.test_calc_result = calc_res
        self.control_params.comparison_parameters.maximum_nom_cardinality = None
        self.control_params.comparison_parameters.minimum_cardinality = None
        comp_results = StableColumnControl(self.control_params).execute()
        self.assertTrue(self.__check_signal_inside("COLUMN_IS_CONSTANT", comp_results))
        self.assertEqual(4, len(comp_results))
        self.assertCountEqual(self.__get_signal_cols("COLUMN_IS_CONSTANT", comp_results), ["a", "e"])
        self.assertEqual(self.__get_signal_messages("COLUMN_IS_CONSTANT", comp_results)[0],
                         "Column consists of a single value (1) for the column: a")

    def test_check_all_column_zero(self):
        """tests when there are stable columns with zero,
        returns DqComparisonResultCode.COLUMN_VALUES_ARE_ALL_ZERO result"""
        calc_res = DqCalculationResult()
        calc_res.sample_stats = get_sample_stats()
        self.control_params.test_calc_result = calc_res
        self.control_params.comparison_parameters.maximum_nom_cardinality = None
        self.control_params.comparison_parameters.minimum_cardinality = None
        comp_results = StableColumnControl(self.control_params).execute()
        self.assertTrue(self.__check_signal_inside("COLUMN_VALUES_ARE_ALL_ZERO", comp_results))
        self.assertEqual(4, len(comp_results))
        self.assertListEqual(self.__get_signal_cols("COLUMN_VALUES_ARE_ALL_ZERO", comp_results), ["e"])
        self.assertEqual(self.__get_signal_messages("COLUMN_VALUES_ARE_ALL_ZERO", comp_results)[0],
                         "Non-default column values consist of only zeros for the column: e")

    def test_check_all_column_none(self):
        """tests when there are stable columns with None,
        returns DqComparisonResultCode.COLUMN_VALUES_ARE_ALL_NULL result"""
        calc_res = DqCalculationResult()
        calc_res.sample_stats = get_sample_stats()
        self.control_params.test_calc_result = calc_res
        self.control_params.comparison_parameters.maximum_nom_cardinality = None
        self.control_params.comparison_parameters.minimum_cardinality = None
        comp_results = StableColumnControl(self.control_params).execute()
        self.assertTrue(self.__check_signal_inside("COLUMN_VALUES_ARE_ALL_NULL", comp_results))
        self.assertEqual(4, len(comp_results))
        self.assertListEqual(self.__get_signal_cols("COLUMN_VALUES_ARE_ALL_NULL", comp_results), ["f"])
        self.assertEqual(self.__get_signal_messages("COLUMN_VALUES_ARE_ALL_NULL", comp_results)[0],
                         "Column consists of only NULL values for the column: f")

    def test_check_exceed_columns(self):
        """tests when there are columns which has cardinality greater than threshold,
        returns DqComparisonResultCode.CARDINALITY_EXCEEDS_MAXIMUM_CARDINALITY result"""
        calc_res = DqCalculationResult()
        calc_res.sample_stats = get_sample_stats()
        self.control_params.test_calc_result = calc_res
        self.control_params.comparison_parameters.maximum_nom_cardinality = 2
        self.control_params.comparison_parameters.minimum_cardinality = None
        comp_results = StableColumnControl(self.control_params).execute()
        self.assertTrue(self.__check_signal_inside("CARDINALITY_EXCEEDS_MAXIMUM_CARDINALITY", comp_results))
        self.assertEqual(5, len(comp_results))
        self.assertListEqual(self.__get_signal_cols("CARDINALITY_EXCEEDS_MAXIMUM_CARDINALITY", comp_results), ["c"])
        self.assertRegex(self.__get_signal_messages("CARDINALITY_EXCEEDS_MAXIMUM_CARDINALITY", comp_results)[0],
                         ".*Column cardinality is greater than or equal to 2 for the column: c.*")

    def test_check_below_min_cols(self):
        """tests when there are columns which has cardinality less than threshold,
        returns DqComparisonResultCode.CARDINALITY_IS_BELOW_MINIMUM_THRESHOLD result"""
        calc_res = DqCalculationResult()
        calc_res.sample_stats = get_sample_stats()
        self.control_params.test_calc_result = calc_res
        self.control_params.comparison_parameters.maximum_nom_cardinality = None
        self.control_params.comparison_parameters.minimum_cardinality = 2
        comp_results = StableColumnControl(self.control_params).execute()
        self.assertTrue(self.__check_signal_inside("CARDINALITY_IS_BELOW_MINIMUM_THRESHOLD", comp_results))
        self.assertEqual(9, len(comp_results))
        self.assertListEqual(self.__get_signal_cols("CARDINALITY_IS_BELOW_MINIMUM_THRESHOLD", comp_results),
                             ["a", "d", "e", "c", "f"])
        self.assertRegex(self.__get_signal_messages("CARDINALITY_IS_BELOW_MINIMUM_THRESHOLD", comp_results)[0],
                         ".*Column cardinality is less than or equal to 2 for the column: a.*")


if __name__ == "__main__":
    unittest.main()
