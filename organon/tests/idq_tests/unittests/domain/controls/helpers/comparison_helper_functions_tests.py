""" This module includes ComparisonHelperFunctions Test class."""
import unittest

from organon.idq.domain.controls.helpers.comparison_helper_functions import ComparisonHelperFunctions
from organon.idq.domain.enums.signal_type import SignalType


class ComparisonHelperFunctionsTestCase(unittest.TestCase):
    """ Tests ComparisonHelperFunctions"""

    def test_get_traffic_light_signal_green(self):
        """tests get_traffic_light_signal green signal"""
        signal = ComparisonHelperFunctions.get_traffic_light_signal(5, 30, 10)
        self.assertEqual(SignalType.GREEN, signal)

    def test_get_traffic_light_signal_yellow(self):
        """tests get_traffic_light_signal yellow signal"""
        signal = ComparisonHelperFunctions.get_traffic_light_signal(20, 30, 10)
        self.assertEqual(SignalType.YELLOW, signal)

    def test_get_traffic_light_signal_red(self):
        """tests get_traffic_light_signal red signal"""
        signal = ComparisonHelperFunctions.get_traffic_light_signal(40, 30, 10)
        self.assertEqual(SignalType.RED, signal)

    def test_get_percentage_change(self):
        """ tests get_percentage_change"""
        change = ComparisonHelperFunctions.get_percentage_change(120, 100)
        self.assertEqual(0.2, change)

    def test_get_percentage_change_reference_zero(self):
        """tests get_percentage_change when reference is zero"""
        change = ComparisonHelperFunctions.get_percentage_change(100, 0)
        self.assertEqual(1, change)

    def test_get_percentage_change_cur_val_zero(self):
        """tests get_percentage_change when current value is zero"""
        change = ComparisonHelperFunctions.get_percentage_change(0, 100)
        self.assertEqual(1, change)

    def test_get_percentage_change_reference_and_cur_val_zero(self):
        """tests get_percentage_change when none of values greater than zero"""
        change = ComparisonHelperFunctions.get_percentage_change(0, 0)
        self.assertEqual(0, change)

    def test_compute_control_mean_std_avg_weight_above_two(self):
        """ tests compute_control_mean_std_avg when weight exceeds two"""
        avg, std = ComparisonHelperFunctions.compute_control_mean_std_avg([3, 4, 5, 6, 7, 8])
        self.assertEqual(5.5, avg)
        self.assertEqual(0.76, round(std, 2))

    def test_compute_control_mean_std_avg_weight_below_two(self):
        """ tests compute_control_mean_std_avg when weight does not exceed two"""
        avg, std = ComparisonHelperFunctions.compute_control_mean_std_avg([8])
        self.assertEqual(8.0, avg)
        self.assertEqual(0, round(std, 2))

    def test_is_outside_confidence_interval(self):
        """ tests is_outside_confidence_interval"""
        inside = ComparisonHelperFunctions.is_outside_confidence_interval(1.96, 3, 0.5, 3.5)
        self.assertFalse(inside)
        outside = ComparisonHelperFunctions.is_outside_confidence_interval(1.96, 3, 0.5, 4)
        self.assertTrue(outside)
