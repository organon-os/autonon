""" This module includes RandomForestPredictorTestCase"""
import unittest

import numpy as np

from organon.idq.domain.controls.helpers.algorithms.predictor_factory import PredictorFactory
from organon.idq.domain.controls.helpers.algorithms.random_forest_predictor import RandomForestPredictor
from organon.idq.domain.enums.dq_pred_algorithm_type import DqPredAlgorithmType


class RandomForestPredictorTestCase(unittest.TestCase):
    """ Tests RandomForestPredictor class"""

    def setUp(self) -> None:
        self.predictor = PredictorFactory.get_predictor(DqPredAlgorithmType.RANDOM_FOREST_REG)

    def test_factory(self):
        """ tests predictor factory"""
        self.assertTrue(isinstance(self.predictor, RandomForestPredictor))

    def test_predict(self):
        """ tests prediction"""
        pred_result = self.predictor.predict([1.5, 2, 2.5, 1, 4, 3.2], 8)
        predictions, actual = pred_result.predictions, pred_result.actual
        self.assertEqual(6, len(predictions))
        self.assertEqual(6, len(actual))

    def test_predict_with_nan_values(self):
        """ tests prediction with nan values"""
        input_data = [float("nan"), float("nan"), 4]
        test_data = float("nan")
        pred_result = self.predictor.predict(input_data, test_data)
        predictions, actual = pred_result.predictions, pred_result.actual
        self.assertEqual(4, len(predictions))
        self.assertEqual(4, len(actual))

    def test_generate_train_data_bmh_lower_than_five(self):
        """tests generate_train_data_with_lags when bmh < 5"""
        val_lag, target = self.predictor.generate_train_data_with_lags(list(range(1, 3)), 5)
        self.assertTrue(np.array_equal(np.array([list(range(1, 4))]).T, val_lag))
        self.assertTrue(np.array_equal(np.array([1, 2, 5]), target))

    def test_generate_train_data_bmh_between_five_and_twelve(self):
        """tests generate_train_data_with_lags when 5 <= bmh < 12"""
        val_lag, target = self.predictor.generate_train_data_with_lags(list(range(1, 6)), 3)
        self.assertTrue(np.array_equal(np.array([list(range(1, 6)), list(range(1, 6))]).T, val_lag))
        self.assertTrue(np.array_equal(np.array([2, 3, 4, 5, 3]), target))

    def test_generate_train_data_bmh_between_twelve_and_twentyfour(self):
        """tests generate_train_data_with_lags when 12 <= bmh < 24"""
        val_lag, target = self.predictor.generate_train_data_with_lags(list(range(1, 13)), 5)
        self.assertTrue(
            np.array_equal(np.array([list(range(1, 11)), list(range(3, 13)), list(range(2, 12)), list(range(1, 11))]).T,
                           val_lag))
        self.assertTrue(np.array_equal(np.array(list(range(4, 13)) + [5]), target))

    def test_generate_train_data_bmh_between_twentyfour_and_fortyeight(self):
        """tests generate_train_data_with_lags when 24 <= bmh < 48"""
        val_lag, target = self.predictor.generate_train_data_with_lags(list(range(1, 25)), 3)
        self.assertTrue(
            np.array_equal(np.array(
                [list(range(1, 19)), list(range(7, 25)), list(range(6, 24)), list(range(5, 23)), list(range(4, 22)),
                 list(range(3, 21)), list(range(2, 20)), list(range(1, 19))]).T, val_lag))
        self.assertTrue(np.array_equal(np.array(list(range(8, 25)) + [3]), target))

    def test_generate_train_data_bmh_greater_than_fortyeight(self):
        """tests generate_train_data_with_lags when bmh > 48"""
        val_lag, target = self.predictor.generate_train_data_with_lags(list(range(1, 49)), 3)
        self.assertTrue(
            np.array_equal(np.array(
                [list(range(1, 36)), list(range(14, 49)), list(range(13, 48)), list(range(12, 47)), list(range(11, 46)),
                 list(range(10, 45)), list(range(9, 44)), list(range(8, 43)), list(range(7, 42)), list(range(6, 41)),
                 list(range(5, 40)), list(range(4, 39)), list(range(3, 38)), list(range(2, 37)), list(range(1, 36))]).T,
                           val_lag))
        self.assertTrue(np.array_equal(np.array(list(range(15, 49)) + [3]), target))
