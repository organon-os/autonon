"""This module includes TrafficLightComparerTestCase"""
import math
import unittest
from typing import List
from unittest.mock import patch, MagicMock

from organon.idq.core.enums.data_entity_type import DataEntityType
from organon.idq.core.enums.dq_comparison_result_code import DqComparisonResultCode
from organon.idq.domain.algorithms.objects.mean_ci_comparison_input import MeanConfidenceIntervalComparisonInput
from organon.idq.domain.algorithms.objects.time_series_comparison_input import TimeSeriesComparisonInput
from organon.idq.domain.algorithms.objects.traffic_light_comparison_input import TrafficLightComparisonInput
from organon.idq.domain.businessobjects.dq_comparison_result import DqComparisonResult
from organon.idq.domain.controls.helpers import traffic_light_comparer
from organon.idq.domain.controls.helpers.algorithms.prediction_result import PredictionResult
from organon.idq.domain.controls.helpers.algorithms.predictor_factory import PredictorFactory
from organon.idq.domain.controls.helpers.traffic_light_comparer import TrafficLightComparer
from organon.idq.domain.enums.dq_test_group_type import DqTestGroupType
from organon.idq.domain.enums.signal_type import SignalType


def get_tl_input():
    """:returns tl input"""
    tl_input = TrafficLightComparisonInput()
    tl_input.current_value = 10
    tl_input.message = "Mean value for the historical row-count is statistically different from current " \
                       "row-count "
    tl_input.green_threshold = 10
    tl_input.yellow_threshold = 30
    tl_input.result_code = DqComparisonResultCode.COLUMN_MEAN_IS_OUTSIDE_MEAN_TRAFFIC_LIGHT_INTERVAL
    tl_input.test_group = DqTestGroupType.RULE_BASED_CONTROLS_COLUMN_SET
    tl_input.data_entity = DataEntityType.COLUMN
    tl_input.data_entity_name = "test"
    tl_input.past_series = [3.5, 3, 6, 4, 2]
    return tl_input


def get_ci_input():
    """:returns ci input"""
    ci_input = MeanConfidenceIntervalComparisonInput()
    ci_input.current_value = 10
    ci_input.message = "Mean value for the historical row-count series is statistically different from " \
                       "current row-count "
    ci_input.z_score = 1.96
    ci_input.result_code = DqComparisonResultCode.COLUMN_MEAN_IS_OUTSIDE_MEAN_CONFIDENCE_INTERVAL
    ci_input.test_group = DqTestGroupType.RULE_BASED_CONTROLS_COLUMN_SET
    ci_input.data_entity = DataEntityType.COLUMN
    ci_input.data_entity_name = "test"
    ci_input.past_series = [3.5, 3, 6, 4, 2]
    return ci_input


def get_ts_input():
    """ :returns ts input"""
    ts_input = TimeSeriesComparisonInput()
    ts_input.current_value = 10
    ts_input.ad_message = "ad mesajı"
    ts_input.ci_message = "ci mesajı"
    ts_input.tl_message = "tl mesajı"
    ts_input.green_threshold = 10
    ts_input.yellow_threshold = 30
    ts_input.z_score = 1.96
    ts_input.ad_code = DqComparisonResultCode.COLUMN_MEAN_ABSOLUTE_DEVIATION_FROM_PREDICTION_IS_MAXIMUM
    ts_input.ci_code = DqComparisonResultCode.COLUMN_MEAN_IS_OUTSIDE_PREDICTION_CONFIDENCE_INTERVAL
    ts_input.tl_code = DqComparisonResultCode.COLUMN_MEAN_IS_OUTSIDE_PREDICTION_TRAFFIC_LIGHT_INTERVAL
    ts_input.test_group = DqTestGroupType.RULE_BASED_CONTROLS_COLUMN_SET
    ts_input.data_entity = DataEntityType.COLUMN
    ts_input.data_entity_name = "test"
    ts_input.past_series = [3.5, 3, 6, 4, 2]
    ts_input.bmh = 5
    return ts_input


class TrafficLightComparerTestCase(unittest.TestCase):
    """ Tests TrafficLightComparer"""

    def setUp(self) -> None:
        self.tl_input = get_tl_input()
        self.ci_input = get_ci_input()
        self.ts_input = get_ts_input()
        self.comp_helper = patch(traffic_light_comparer.__name__ + ".ComparisonHelperFunctions")
        self.mock_comp = self.comp_helper.start()
        self.mock_comp.get_percentage_change.side_effect = [170, 170]
        self.mock_comp.get_traffic_light_signal.side_effect = [SignalType.RED, SignalType.RED]
        self.mock_comp.is_outside_confidence_interval.side_effect = [True, True]

        self.predictor_factory_patcher = patch(traffic_light_comparer.__name__ + "." + PredictorFactory.__name__)
        self.predictor_factory = self.predictor_factory_patcher.start()

        mock_pred_res = PredictionResult([3, 6, 4, 2, 10])
        mock_pred_res.predictions = [3.685, 4.975, 3.705, 2.645, 4.405]
        mock_pred_res.mean_square_err = 0.57

        mock_predictor = MagicMock()
        mock_predictor.predict.return_value = mock_pred_res
        self.predictor_factory.get_predictor.return_value = mock_predictor

    def tearDown(self) -> None:
        self.comp_helper.stop()
        self.predictor_factory.stop()

    def test_get_mi_tl_comparison_results(self):
        """ tests get_mi_tl_comparison_results"""
        self.mock_comp.compute_control_mean_std_avg.return_value = 3.5, 0.5
        results = TrafficLightComparer.get_mi_tl_comparison_results(self.ci_input, self.tl_input)
        self.assertEqual(7, len(results))

        self.assertEqual(DqComparisonResultCode.COLUMN_MEAN_IS_OUTSIDE_MEAN_CONFIDENCE_INTERVAL,
                         results[0].result_code)
        self.assertEqual('TestValue', results[0].property_code)
        self.assertEqual(10, results[0].property_key_numeric)
        self.assertEqual("LowerConfidenceLevel", results[1].property_code)
        self.assertEqual(2.5, round(results[1].property_key_numeric, 1))
        self.assertEqual('UpperConfidenceLevel', results[2].property_code)
        self.assertEqual(4, round(results[2].property_key_numeric, 0))

        self.assertEqual(DqComparisonResultCode.COLUMN_MEAN_IS_OUTSIDE_MEAN_TRAFFIC_LIGHT_INTERVAL,
                         results[3].result_code)
        self.assertEqual('TrafficLightSignal', results[3].property_code)
        self.assertEqual('RED', results[3].property_key_nominal)
        self.assertEqual("ControlValue", results[4].property_code)
        self.assertEqual(3.5, results[4].property_key_numeric)
        self.assertEqual('TestValue', results[5].property_code)
        self.assertEqual(10, results[5].property_key_numeric)
        self.assertEqual('PercentageChange', results[6].property_code)
        self.assertEqual(170, round(results[6].property_key_numeric, 1))

    def test_get_dq_comparison_results_for_time_series(self):
        """ tests get_dq_comparison_results_for_time_series"""
        self.mock_comp.compute_control_mean_std_avg.return_value = 3.5, 8
        results = TrafficLightComparer.get_dq_comparison_results_for_time_series(self.ts_input)
        self.assertEqual(len(results), 27)
        self.assertEqual([10, 10, 10],
                         [res.property_key_numeric for res in results if res.property_code == "CurrentValue"])
        self.assertEqual([4.405, 4.405, 4.405],
                         [res.property_key_numeric for res in results if res.property_code == "PredictedValue"])
        self.assertEqual([2.93], [round(res.property_key_numeric, 2) for res in results if
                                  res.property_code == "ConfidenceIntervalLowerBound"])
        self.assertEqual([5.88], [round(res.property_key_numeric, 2) for res in results if
                                  res.property_code == "ConfidenceIntervalUpperBound"])
        self.assertEqual(["(3, 3.685)", "(6, 4.975)", "(4, 3.705)", "(2, 2.645)", "(10, 4.405)"],
                         [res.property_key_nominal for res in results if
                          res.property_code == "TimeSeriesAndPredictions"][:5])
        self.assertEqual([5.59], [round(res.property_key_numeric, 2) for res in results if
                                  res.property_code == "AbsoluteDeviationBetweenActualAndPredicted"])
        self.assertEqual([170], [round(res.property_key_numeric, 2) for res in results if
                                 res.property_code == "PercentageDeviationBetweenActualAndPredicted"])
        self.assertEqual(["RED"], [res.property_key_nominal for res in results if
                                   res.property_code == "SignalType"])

    def test_get_dq_comparison_results_for_time_series_all_null(self):
        """No signals should be generated when past_series is all null"""
        self.ts_input.past_series = [float("nan"), float("nan"), float("nan")]
        self.ts_input.current_value = float("nan")
        results = TrafficLightComparer.get_dq_comparison_results_for_time_series(self.ts_input)
        self.assertEqual(0, len(results))

    def test_get_dq_comparison_results_for_time_series_t_null(self):
        """No signals should be generated when current values is null"""
        self.ts_input.past_series = [100, float("nan"), 300]
        self.ts_input.current_value = float("nan")
        results = TrafficLightComparer.get_dq_comparison_results_for_time_series(self.ts_input)
        self._check_signal_exists(results,
                                  DqComparisonResultCode.COLUMN_MEAN_IS_OUTSIDE_PREDICTION_TRAFFIC_LIGHT_INTERVAL,
                                  "CurrentValue", property_key_numeric=float("nan"))
        self._check_signal_exists(results,
                                  DqComparisonResultCode.COLUMN_MEAN_IS_OUTSIDE_PREDICTION_TRAFFIC_LIGHT_INTERVAL,
                                  "PercentageDeviationBetweenActualAndPredicted", property_key_numeric=1)
        self._check_signal_exists(results,
                                  DqComparisonResultCode.COLUMN_MEAN_ABSOLUTE_DEVIATION_FROM_PREDICTION_IS_MAXIMUM,
                                  "AbsoluteDeviationBetweenActualAndPredicted", property_key_numeric=float("nan"))
        self._check_signal_exists(results,
                                  DqComparisonResultCode.COLUMN_MEAN_IS_OUTSIDE_PREDICTION_CONFIDENCE_INTERVAL,
                                  "CurrentValue", property_key_numeric=float("nan"))

    def _check_signal_exists(self, results: List[DqComparisonResult], result_code: DqComparisonResultCode,
                             property_code: str, property_key_numeric: float = None):
        for result in results:
            if result.result_code == result_code and result.property_code == property_code and \
                    self.__nan_eq(result.property_key_numeric, property_key_numeric):
                return

        self.fail("Expected signal was not generated")

    @staticmethod
    def __nan_eq(val1: float, val2: float):
        if isinstance(val1, float) and math.isnan(val1):
            return math.isnan(val2)
        return val1 == val2
