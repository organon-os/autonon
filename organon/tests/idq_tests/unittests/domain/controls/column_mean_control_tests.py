"""This module includes ColumnMeanControlTestCase"""
import unittest
from unittest.mock import MagicMock, patch

from organon.fl.core.enums.column_native_type import ColumnNativeType
from organon.idq.core.enums.data_entity_type import DataEntityType
from organon.idq.domain.algorithms.objects.mean_ci_comparison_input import MeanConfidenceIntervalComparisonInput
from organon.idq.domain.algorithms.objects.time_series_comparison_input import TimeSeriesComparisonInput
from organon.idq.domain.algorithms.objects.traffic_light_comparison_input import TrafficLightComparisonInput
from organon.idq.domain.businessobjects.dq_calculation_result import DqCalculationResult
from organon.idq.domain.businessobjects.dq_comparison_result import DqComparisonResult
from organon.idq.domain.businessobjects.dq_control_parameters import DqControlParameters
from organon.idq.domain.businessobjects.dq_test_group import DqTestGroup
from organon.idq.domain.businessobjects.statistics.dq_sample_numerical_statistics import DqSampleNumericalStatistics
from organon.idq.domain.businessobjects.statistics.sample_statistics import SampleStatistics
from organon.idq.domain.controls import column_mean_control
from organon.idq.domain.controls.column_mean_control import ColumnMeanControl
from organon.idq.domain.controls.helpers.traffic_light_comparer import TrafficLightComparer
from organon.idq.domain.enums.dq_test_group_type import DqTestGroupType
from organon.idq.domain.settings.abstractions.dq_base_comparison_parameters import DqBaseComparisonParameters
from organon.idq.domain.settings.dq_comparison_column_info import DqComparisonColumnInfo


def _get_stats(trimmed_mean, std_dev):
    """returns stats with mean and std"""
    stats = DqSampleNumericalStatistics()
    stats.trimmed_mean = trimmed_mean
    stats.std_dev = std_dev
    return stats


def _get_calculation_results():
    """returns calculation results"""
    calc_1_res = DqCalculationResult()
    calc_1_res.sample_stats = SampleStatistics()

    calc_1_res.sample_stats.numerical_statistics = {"col1": _get_stats(1.5, 6.2), "col2": _get_stats(1.2, 4.7)}

    calc_2_res = DqCalculationResult()
    calc_2_res.sample_stats = SampleStatistics()
    calc_2_res.sample_stats.numerical_statistics = {"col1": _get_stats(1.1, 5.6), "col2": _get_stats(2.3, 4.9)}

    calc_3_res = DqCalculationResult()
    calc_3_res.sample_stats = SampleStatistics()
    calc_3_res.sample_stats.numerical_statistics = {"col1": _get_stats(0.9, 7.0), "col2": _get_stats(2.0, 5.5)}

    return [calc_1_res, calc_2_res, calc_3_res]


def _get_test_group_info(len_calcs):
    """returns test group type"""
    test_group = DqTestGroup()
    test_group.test_bmh = len_calcs
    test_group.group_type = ColumnMeanControl.get_test_group_type()
    test_group.inclusion_flag = True
    return {
        ColumnMeanControl.get_test_group_type(): test_group
    }


class ColumnMeanControlTestCase(unittest.TestCase):
    """ tests ColumnMeanControl class"""

    def setUp(self) -> None:
        self.control_params = DqControlParameters()
        calc_results = _get_calculation_results()
        self.control_params.control_results = calc_results[:-1]
        self.control_params.test_calc_result = calc_results[-1]
        self.control_params.test_group_info = _get_test_group_info(len(calc_results))
        comp_col = DqComparisonColumnInfo()
        comp_col.column_name = "col1"
        comp_col.benchmark_horizon = len(calc_results)
        comp_col2 = DqComparisonColumnInfo()
        comp_col2.column_name = "col2"
        comp_col2.benchmark_horizon = len(calc_results)
        self.comparison_columns = [comp_col, comp_col2]
        self.control_params.comparison_parameters = DqBaseComparisonParameters()
        self.control_params.comparison_parameters.z_score = 1.96
        self.control_params.comparison_parameters.traffic_light_threshold_green = 0.2
        self.control_params.comparison_parameters.traffic_light_threshold_yellow = 0.3
        self.control_params.get_comparison_cols_by_native_type = MagicMock(return_value=self.comparison_columns)

        self.tlc_patcher = patch(column_mean_control.__name__ + "." + TrafficLightComparer.__name__)
        self.tlc = self.tlc_patcher.start()

        mock_comp_res = [DqComparisonResult(data_entity=DataEntityType.COLUMN,
                                            data_entity_name="dummy",
                                            test_group=DqTestGroupType.MODELLING_AND_CI_CONTROLS_COLUMN_SET)]

        self.tlc.get_mi_tl_comparison_results.side_effect = [mock_comp_res, mock_comp_res]
        self.tlc.get_dq_comparison_results_for_time_series.side_effect = [mock_comp_res, mock_comp_res]

    def tearDown(self) -> None:
        self.tlc.stop()

    def test_mean_controls(self):
        """test mean alerts"""
        comp_results = ColumnMeanControl(self.control_params).execute()
        self.assertEqual(4, len(comp_results))

        time_series_input = self.tlc.get_dq_comparison_results_for_time_series.call_args.args[0]
        self.assertIsInstance(time_series_input, TimeSeriesComparisonInput)
        self.assertIn("col2", time_series_input.data_entity_name)
        self.assertEqual([1.2, 2.3], time_series_input.past_series)
        self.assertEqual(2.0, time_series_input.current_value)

        mi_input = self.tlc.get_mi_tl_comparison_results.call_args.args[0]
        self.assertIsInstance(mi_input, MeanConfidenceIntervalComparisonInput)
        self.assertIn("col2", mi_input.data_entity_name)
        self.assertEqual([1.2, 2.3], time_series_input.past_series)
        self.assertEqual(2.0, time_series_input.current_value)

        tl_input = self.tlc.get_mi_tl_comparison_results.call_args.args[1]
        self.assertIsInstance(tl_input, TrafficLightComparisonInput)
        self.assertIn("col2", tl_input.data_entity_name)
        self.assertEqual([1.2, 2.3], tl_input.past_series)
        self.assertEqual(2.0, tl_input.current_value)

    def test_test_calc_none(self):
        """ tests raise error when test calculation none"""
        self.control_params.test_calc_result = None
        with self.assertRaisesRegex(ValueError, "Test result None"):
            ColumnMeanControl(self.control_params).execute()

    def test_control_result_none(self):
        """test raise error when control results none"""
        self.control_params.control_results = None
        with self.assertRaisesRegex(ValueError, "Control results None"):
            ColumnMeanControl(self.control_params).execute()

    def test_execute_test_bmh_zero(self):
        """tests zero results when test_bmh 0"""
        self.control_params.test_group_info[ColumnMeanControl.get_test_group_type()].test_bmh = 0
        self.assertEqual(0, len(ColumnMeanControl(self.control_params).execute()))

    def test_execute_benchmark_horizon_zero(self):
        """ tests 0 results from a column when benchmark_horizon is zero"""
        self.control_params.get_comparison_cols_by_native_type(ColumnNativeType.Numeric)[0].benchmark_horizon = 0
        self.assertEqual(2, len(ColumnMeanControl(self.control_params).execute()))

    def test_execute_test_bmh_1(self):
        """ tests 0 results from a column when benchmark_horizon is zero"""
        self.control_params.test_group_info[ColumnMeanControl.get_test_group_type()].test_bmh = 1
        ColumnMeanControl(self.control_params).execute()
        time_series_input = self.tlc.get_dq_comparison_results_for_time_series.call_args.args[0]
        self.assertIsInstance(time_series_input, TimeSeriesComparisonInput)
        self.assertIn("col2", time_series_input.data_entity_name)
        self.assertEqual([2.3], time_series_input.past_series)
        self.assertEqual(2.0, time_series_input.current_value)


if __name__ == "__main__":
    unittest.main()
