"""Includes tests for EmptyTableControl class."""
import unittest

import pandas as pd

from organon.fl.core.exceptionhandling.known_exception import KnownException
from organon.idq.domain.businessobjects.dq_calculation_result import DqCalculationResult
from organon.idq.domain.businessobjects.dq_comparison_result import DqComparisonResult
from organon.idq.domain.businessobjects.dq_control_parameters import DqControlParameters
from organon.idq.domain.businessobjects.record_sources.dq_df_record_source import DqDfRecordSource
from organon.idq.domain.businessobjects.statistics.data_source_statistics import DataSourceStatistics
from organon.idq.domain.controls.empty_table_control import EmptyTableControl
from organon.idq.domain.settings.calculation.dq_df_calculation_parameters import DqDfCalculationParameters
from organon.idq.domain.settings.input_source.dq_df_input_source_settings import DqDfInputSourceSettings


class EmptyTableControlTestCase(unittest.TestCase):
    """Test class for EmptyTableControl."""

    def setUp(self) -> None:
        self.record_source = DqDfRecordSource(pd.DataFrame(), name="dummy")
        self.control_params = DqControlParameters()
        self.control_params.test_calc_parameters = DqDfCalculationParameters()
        self.control_params.test_calc_parameters.input_source_settings = DqDfInputSourceSettings(self.record_source)

    def test_check_data_source_empty_none_result(self):
        """test if ValueError is raised when check_data_source_empty is called with t_cal_result=None"""
        with self.assertRaises(ValueError):
            EmptyTableControl(self.control_params).execute()

    def test_check_data_source_empty_table_stats_not_given(self):
        """test if ValueError is raised when check_data_source_empty is called
        when t_cal_Result.table_stats=None"""
        self.control_params.test_calc_result = DqCalculationResult()

        with self.assertRaisesRegex(KnownException, ".*Calculation does not contain data source statistics.*"):
            EmptyTableControl(self.control_params).execute()

    def test_check_data_source_empty_not_empty(self):
        """test if None returned if data source not empty"""
        result = DqCalculationResult()
        result.data_source_stats = DataSourceStatistics()
        result.data_source_stats.row_count = 100
        self.control_params.test_calc_result = result
        self.assertEqual(0, len(EmptyTableControl(self.control_params).execute()))

    def test_check_data_source_empty(self):
        """test if proper DqComparisonResult is returned if data source is empty"""
        result = DqCalculationResult()
        result.data_source_stats = DataSourceStatistics()
        result.data_source_stats.row_count = 0
        self.control_params.test_calc_result = result
        comp_results = EmptyTableControl(self.control_params).execute()
        self.assertNotEqual(0, len(comp_results))
        self.assertIsInstance(comp_results[0], DqComparisonResult)


if __name__ == "__main__":
    unittest.main()
