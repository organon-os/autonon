"""Includes tests for TableColumnsControl class."""
import unittest
from typing import List
from unittest.mock import MagicMock

from organon.fl.core.exceptionhandling.known_exception import KnownException
from organon.idq.core.enums.dq_comparison_result_code import DqComparisonResultCode
from organon.idq.domain.businessobjects.data_column.dq_data_column import DqDataColumn
from organon.idq.domain.businessobjects.data_column.dq_df_data_column import DqDfDataColumn
from organon.idq.domain.businessobjects.data_column.dq_file_data_column import DqFileDataColumn
from organon.idq.domain.businessobjects.dq_calculation_result import DqCalculationResult
from organon.idq.domain.businessobjects.dq_comparison_result import DqComparisonResult
from organon.idq.domain.businessobjects.dq_control_parameters import DqControlParameters
from organon.idq.domain.businessobjects.dq_data_column_collection import DqDataColumnCollection
from organon.idq.domain.businessobjects.dq_test_group import DqTestGroup
from organon.idq.domain.businessobjects.statistics.data_source_statistics import DataSourceStatistics
from organon.idq.domain.controls.table_columns_control import TableColumnsControl
from organon.idq.domain.settings.calculation.dq_df_calculation_parameters import DqDfCalculationParameters


def get_df_data_col(column_name: str, data_type: type):
    """get DqDfDataColumn instance"""
    col = DqDfDataColumn()
    col.column_name = column_name
    col.col_np_dtype = data_type
    return col


def get_file_data_col(column_name: str, data_type: type):
    """get DqFileDataColumn instance"""

    col = DqFileDataColumn()
    col.column_name = column_name
    col.col_np_dtype = data_type
    return col


def __get_col_side_effect_helper(collection: DqDataColumnCollection, col_name: str):
    vals = [col for col in collection if col.column_name == col_name]
    if len(vals) == 0:
        return None
    return vals[0]


def get_data_col_collection(items: List[DqDataColumn]):
    """generate DqDataColumnCollection from list od DqDataColumns"""
    collection = MagicMock(spec=DqDataColumnCollection)
    collection.item_list = items
    collection.__iter__.side_effect = collection.item_list.__iter__
    collection.get_column.side_effect = lambda name: __get_col_side_effect_helper(collection, name)
    return collection


class TableColumnsControlTestCase(unittest.TestCase):
    """Test class for TableColumnsControl."""

    def setUp(self) -> None:
        self.control_params = DqControlParameters()
        self.control_params.control_results = []
        self.control_params.control_parameters = []
        test_group = DqTestGroup()
        test_group.group_type = TableColumnsControl.get_test_group_type()
        test_group.test_bmh = 3
        self.control_params.test_group_info = {
            test_group.group_type: test_group
        }
        self.control_params.test_calc_result = DqCalculationResult()

    def __add_calc_result_with_name(self, result: DqCalculationResult, name: str):
        self.control_params.control_results.append(result)
        params = DqDfCalculationParameters()
        params.calculation_name = name
        self.control_params.control_parameters.append(params)

    def test_bmh_error(self):
        """tests if ValueError is raised when bmh is less than 1"""
        test_gr = DqTestGroup()
        test_gr.test_bmh = 0
        self.control_params.test_group_info = {TableColumnsControl.get_test_group_type(): test_gr}
        self.__add_calc_result_with_name(DqCalculationResult(), "some_calc")
        with self.assertRaisesRegex(ValueError, ".*Bmh value cannot be less than 1.*"):
            TableColumnsControl(self.control_params).execute()

    def test_empty_control_results(self):
        """tests if ValueError is raised when control_results is empty"""
        self.control_params.control_results = []

        with self.assertRaisesRegex(ValueError, ".*Benchmark result collection can not be empty.*"):
            TableColumnsControl(self.control_params).execute()

    def test_no_data_source_stats_in_t_calc(self):
        """tests if ValueError is raised when t_Calc_result does not contain data source stats"""
        result = DqCalculationResult()
        result.data_source_stats = DataSourceStatistics()
        result.data_source_stats.row_count = 0
        self.__add_calc_result_with_name(result, "calc_1")
        with self.assertRaisesRegex(KnownException, ".*T calculation result does not contain data source statistics.*"):
            TableColumnsControl(self.control_params).execute()

    def test_column_removed(self):
        """tests compare_columns method for ColumnRemoved signals."""
        result = DqCalculationResult()
        result.data_source_stats = DataSourceStatistics()
        result.data_source_stats.data_column_collection = get_data_col_collection([
            get_df_data_col("col1", int),
            get_df_data_col("col2", float),
            get_df_data_col("col3", str),

        ])

        result2 = DqCalculationResult()
        result2.data_source_stats = DataSourceStatistics()
        result2.data_source_stats.data_column_collection = get_data_col_collection([
            get_df_data_col("col1", int),
            get_df_data_col("col2", float),
            get_df_data_col("col4", float),
        ])

        t_result = DqCalculationResult()
        t_result.data_source_stats = DataSourceStatistics()
        t_result.data_source_stats.data_column_collection = get_data_col_collection([
            get_df_data_col("col1", int),
            get_df_data_col("col2", float),
        ])
        self.__add_calc_result_with_name(result, "calc_1")
        self.__add_calc_result_with_name(result2, "calc_2")
        self.control_params.test_calc_result = t_result

        results = TableColumnsControl(self.control_params).execute()
        self.assertEqual(2, len(results))
        for res in results:
            self.assertEqual(DqComparisonResultCode.COLUMN_REMOVED, res.result_code)
        self.__assert_comp_result(results[0], "col3", "calc_1", "RemovedColumnName", "Column col3 is removed")
        self.__assert_comp_result(results[1], "col4", "calc_2", "RemovedColumnName", "Column col4 is removed")

    def test_column_added(self):
        """tests compare_columns method for ColumnAdded signals."""
        result = DqCalculationResult()
        result.data_source_stats = DataSourceStatistics()
        result.data_source_stats.data_column_collection = get_data_col_collection([
            get_df_data_col("col1", int),

        ])

        result2 = DqCalculationResult()
        result2.data_source_stats = DataSourceStatistics()
        result2.data_source_stats.data_column_collection = get_data_col_collection([
            get_df_data_col("col1", int),
            get_df_data_col("col2", float),
        ])

        t_result = DqCalculationResult()
        t_result.data_source_stats = DataSourceStatistics()
        t_result.data_source_stats.data_column_collection = get_data_col_collection([
            get_df_data_col("col1", int),
            get_df_data_col("col2", float),
            get_df_data_col("col3", str),
        ])
        self.__add_calc_result_with_name(result, "calc_1")
        self.__add_calc_result_with_name(result2, "calc_2")
        self.control_params.test_calc_result = t_result

        results = TableColumnsControl(self.control_params).execute()
        self.assertEqual(3, len(results))
        for res in results:
            self.assertEqual(DqComparisonResultCode.COLUMN_ADDED, res.result_code)
        self.__assert_comp_result(results[0], "col2", "calc_1", "AddedColumnName", "Column col2 is added")
        self.__assert_comp_result(results[1], "col3", "calc_1", "AddedColumnName", "Column col3 is added")
        self.__assert_comp_result(results[2], "col3", "calc_2", "AddedColumnName", "Column col3 is added")

    def __assert_comp_result(self, result: DqComparisonResult, prop_key_nom: str, prop_val_nom: str, prop_code: str,
                             message_part: str, ):
        self.assertEqual(prop_key_nom, result.property_key_nominal)
        self.assertEqual(prop_val_nom, result.property_value_nominal)
        self.assertEqual(prop_code, result.property_code)
        self.assertIn(message_part, result.message)

    def test_no_data_source_stats_in_control(self):
        """tests if ValueError is raised when control_results is empty"""
        result = DqCalculationResult()
        result.data_source_stats = DataSourceStatistics()
        result.data_source_stats.data_column_collection = get_data_col_collection([
            get_df_data_col("col1", int),
            get_df_data_col("col2", float),
            get_df_data_col("col3", str),

        ])

        result2 = DqCalculationResult()

        t_result = DqCalculationResult()
        t_result.data_source_stats = DataSourceStatistics()
        t_result.data_source_stats.data_column_collection = get_data_col_collection([
            get_df_data_col("col1", int),
            get_df_data_col("col2", float),
            get_df_data_col("col3", str),
        ])
        self.__add_calc_result_with_name(result, "calc_1")
        self.__add_calc_result_with_name(result2, "calc_2")
        self.control_params.test_calc_result = t_result

        with self.assertRaisesRegex(KnownException, ".*Calculation calc_2 does not contain data source statistics.*"):
            TableColumnsControl(self.control_params).execute()

    def test_df_column_type_changed(self):
        """tests compare_columns method for ColumnTypeChanged signals. (dataframe source)"""
        result = DqCalculationResult()
        result.data_source_stats = DataSourceStatistics()
        result.data_source_stats.data_column_collection = get_data_col_collection([
            get_df_data_col("col1", int),
            get_df_data_col("col2", str),
        ])

        t_result = DqCalculationResult()
        t_result.data_source_stats = DataSourceStatistics()
        t_result.data_source_stats.data_column_collection = get_data_col_collection([
            get_df_data_col("col1", float),
            get_df_data_col("col2", str)
        ])
        self.__add_calc_result_with_name(result, "calc_1")
        self.control_params.test_calc_result = t_result
        results = TableColumnsControl(self.control_params).execute()
        self.assertEqual(2, len(results))
        for res in results:
            self.assertEqual(DqComparisonResultCode.COLUMN_TYPE_CHANGED, res.result_code)

        self.__assert_comp_result(results[0], "col1", "calc_1", "PreviousColumn", "Data Type")
        self.__assert_comp_result(results[1], "col1", "calc_1", "CurrentColumn", "Data Type")

    def test_file_column_type_changed(self):
        """tests compare_columns method for ColumnTypeChanged signals. (csv file source)"""
        result = DqCalculationResult()
        result.data_source_stats = DataSourceStatistics()
        result.data_source_stats.data_column_collection = get_data_col_collection([
            get_file_data_col("col1", int),
            get_file_data_col("col2", str),
        ])

        t_result = DqCalculationResult()
        t_result.data_source_stats = DataSourceStatistics()
        t_result.data_source_stats.data_column_collection = get_data_col_collection([
            get_file_data_col("col1", float),
            get_file_data_col("col2", str)
        ])
        self.__add_calc_result_with_name(result, "calc_1")
        self.control_params.test_calc_result = t_result
        results = TableColumnsControl(self.control_params).execute()
        self.assertEqual(2, len(results))
        for res in results:
            self.assertEqual(DqComparisonResultCode.COLUMN_TYPE_CHANGED, res.result_code)

        self.__assert_comp_result(results[0], "col1", "calc_1", "PreviousColumn", "Data Type")
        self.__assert_comp_result(results[1], "col1", "calc_1", "CurrentColumn", "Data Type")


if __name__ == "__main__":
    unittest.main()
