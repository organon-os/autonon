"""Includes tests for UnexpectedNumericalValuesControl"""
import unittest
from unittest.mock import MagicMock

from organon.idq.core.enums.dq_comparison_result_code import DqComparisonResultCode
from organon.idq.domain.businessobjects.dq_calculation_result import DqCalculationResult
from organon.idq.domain.businessobjects.dq_control_parameters import DqControlParameters
from organon.idq.domain.businessobjects.dq_test_group import DqTestGroup
from organon.idq.domain.businessobjects.statistics.dq_sample_numerical_statistics import DqSampleNumericalStatistics
from organon.idq.domain.businessobjects.statistics.sample_statistics import SampleStatistics
from organon.idq.domain.controls.unexpected_numerical_values_control import UnexpectedNumericalValuesControl
from organon.idq.domain.settings.dq_comparison_column_info import DqComparisonColumnInfo


def _get_stats(min_val, max_val):
    stats = DqSampleNumericalStatistics()
    stats.min = min_val
    stats.max = max_val
    return stats


def _get_calculation_results():
    calc_1_res = DqCalculationResult()
    calc_1_res.sample_stats = SampleStatistics()

    calc_1_res.sample_stats.numerical_statistics = {"col1": _get_stats(1.5, 6.2), "col2": _get_stats(1.2, 4.7)}

    calc_2_res = DqCalculationResult()
    calc_2_res.sample_stats = SampleStatistics()
    calc_2_res.sample_stats.numerical_statistics = {"col1": _get_stats(1.1, 5.6), "col2": _get_stats(2.3, 4.9)}

    calc_3_res = DqCalculationResult()
    calc_3_res.sample_stats = SampleStatistics()
    calc_3_res.sample_stats.numerical_statistics = {"col1": _get_stats(0.9, 7.0), "col2": _get_stats(2.0, 5.5)}

    return [calc_1_res, calc_2_res, calc_3_res]


def _get_test_group_info(len_calcs):
    test_group = DqTestGroup()
    test_group.test_bmh = len_calcs
    test_group.group_type = UnexpectedNumericalValuesControl.get_test_group_type()
    test_group.inclusion_flag = True
    return {
        UnexpectedNumericalValuesControl.get_test_group_type(): test_group
    }


class UnexpectedNumericalValuesControlTestCase(unittest.TestCase):
    """Test class for UnexpectedNumericalValuesControl"""

    def setUp(self) -> None:
        self.control_params = DqControlParameters()
        calc_results = _get_calculation_results()
        self.control_params.control_results = calc_results[:-1]
        self.control_params.test_calc_result = calc_results[-1]
        self.control_params.test_group_info = _get_test_group_info(len(calc_results))
        comp_col = DqComparisonColumnInfo()
        comp_col.column_name = "col1"
        comp_col.benchmark_horizon = len(calc_results)
        comp_col2 = DqComparisonColumnInfo()
        comp_col2.column_name = "col2"
        comp_col2.benchmark_horizon = len(calc_results)
        self.comparison_columns = [comp_col, comp_col2]
        self.control_params.get_comparison_cols_by_native_type = MagicMock(return_value=self.comparison_columns)

    def _get_control(self):
        return UnexpectedNumericalValuesControl(self.control_params)

    def test_both_min_and_max(self):
        """tests if unexpectedminimum and unexpectedmaximum results are returned"""
        results = self._get_control().execute()
        self.assertEqual(3, len(results))
        result_codes = [result.result_code for result in results]
        self.assertEqual([DqComparisonResultCode.UNEXPECTED_MINIMUM, DqComparisonResultCode.UNEXPECTED_MAXIMUM,
                          DqComparisonResultCode.UNEXPECTED_MAXIMUM], result_codes)
        self.assertRegex(results[0].message, r".*\(0\.9\) is smaller .* \(1\.1\) .* col1")
        self.assertRegex(results[1].message, r".*\(7\.0\) is greater .* \(6\.2\) .* col1")
        self.assertRegex(results[2].message, r".*\(5\.5\) is greater .* \(4\.9\) .* col2")

    def test_stat_not_in_calc_results(self):
        """case when numerical stats o a column does not exist in one of the calculation results"""
        self.control_params.control_results[1].sample_stats.numerical_statistics.pop("col2")
        results = self._get_control().execute()
        self.assertEqual(3, len(results))
        result_codes = [result.result_code for result in results]
        self.assertEqual([DqComparisonResultCode.UNEXPECTED_MINIMUM, DqComparisonResultCode.UNEXPECTED_MAXIMUM,
                          DqComparisonResultCode.UNEXPECTED_MAXIMUM],
                         result_codes)
        self.assertRegex(results[0].message, r".*\(0\.9\) is smaller .* \(1\.1\).* col1")
        self.assertRegex(results[1].message, r".*\(7\.0\) is greater .* \(6\.2\).* col1")
        self.assertRegex(results[2].message, r".*\(5\.5\) is greater .* \(4\.7\).* col2")

    def test_stat_not_in_test_calc_result(self):
        """case when numerical stats of a column does not exist in test calculation results"""
        self.control_params.test_calc_result.sample_stats.numerical_statistics.pop("col1")
        results = self._get_control().execute()
        self.assertEqual(1, len(results))  # col1 results should not be generated

    def test_nan_max_value(self):
        """case when max value in one of the stats is nan"""
        self.control_params.control_results[0].sample_stats.numerical_statistics["col1"].max = float("nan")
        results = self._get_control().execute()
        self.assertRegex(results[0].message, r".*\(0\.9\) is smaller .* \(1\.1\).* col1")
        self.assertRegex(results[1].message, r".*\(7\.0\) is greater .* \(5\.6\).* col1")
        self.assertRegex(results[2].message, r".*\(5\.5\) is greater .* \(4\.9\).* col2")

    def test_nan_min_value(self):
        """case when min value in one of the stats is nan"""
        self.control_params.control_results[1].sample_stats.numerical_statistics["col1"].min = float("nan")
        results = self._get_control().execute()
        self.assertRegex(results[0].message, r".*\(0\.9\) is smaller .* \(1\.5\).* col1")
        self.assertRegex(results[1].message, r".*\(7\.0\) is greater .* \(6\.2\).* col1")
        self.assertRegex(results[2].message, r".*\(5\.5\) is greater .* \(4\.9\).* col2")

    def test_nan_min_value_in_test_calc(self):
        """case when min value in one of the stats is nan"""
        self.control_params.test_calc_result.sample_stats.numerical_statistics["col1"].max = float("nan")
        self.control_params.test_calc_result.sample_stats.numerical_statistics["col1"].min = float("nan")
        results = self._get_control().execute()
        self.assertEqual(1, len(results))
        self.assertRegex(results[0].message, r".*\(5\.5\) is greater .* \(4\.9\).* col2")

    def test_comp_col_bmh(self):
        """tests when a comparison column has a bmh which is lower then calculation count"""
        self.comparison_columns[0].benchmark_horizon = 1
        results = self._get_control().execute()
        self.assertEqual(3, len(results))
        result_codes = [result.result_code for result in results]
        self.assertEqual([DqComparisonResultCode.UNEXPECTED_MINIMUM, DqComparisonResultCode.UNEXPECTED_MAXIMUM,
                          DqComparisonResultCode.UNEXPECTED_MAXIMUM], result_codes)
        self.assertRegex(results[0].message, r".*\(0\.9\) is smaller .* \(1\.1\) .* col1")
        self.assertRegex(results[1].message, r".*\(7\.0\) is greater .* \(5\.6\) .* col1")
        self.assertRegex(results[2].message, r".*\(5\.5\) is greater .* \(4\.9\) .* col2")

    def test_test_group_bmh(self):
        """tests when test_group has a bmh which is lower then calculation count"""
        self.control_params.test_group_info[UnexpectedNumericalValuesControl.get_test_group_type()].test_bmh = 1
        results = self._get_control().execute()
        self.assertEqual(4, len(results))
        result_codes = [result.result_code for result in results]
        self.assertEqual([DqComparisonResultCode.UNEXPECTED_MINIMUM, DqComparisonResultCode.UNEXPECTED_MAXIMUM,
                          DqComparisonResultCode.UNEXPECTED_MINIMUM,
                          DqComparisonResultCode.UNEXPECTED_MAXIMUM], result_codes)
        self.assertRegex(results[0].message, r".*\(0\.9\) is smaller .* \(1\.1\) .* col1")
        self.assertRegex(results[1].message, r".*\(7\.0\) is greater .* \(5\.6\) .* col1")
        self.assertRegex(results[2].message, r".*\(2\.0\) is smaller .* \(2\.3\) .* col2")
        self.assertRegex(results[3].message, r".*\(5\.5\) is greater .* \(4\.9\) .* col2")


if __name__ == '__main__':
    unittest.main()
