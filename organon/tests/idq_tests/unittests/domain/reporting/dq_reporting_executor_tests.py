"""Includes tests for DqReportingTaskExecutor class."""
import unittest
from unittest.mock import patch

import pandas as pd

from organon.fl.core.enums.column_native_type import ColumnNativeType
from organon.idq.core.enums.data_entity_type import DataEntityType
from organon.idq.core.enums.dq_comparison_result_code import DqComparisonResultCode
from organon.idq.domain.businessobjects.data_column.dq_data_column import DqDataColumn
from organon.idq.domain.businessobjects.dq_calculation_result import DqCalculationResult
from organon.idq.domain.businessobjects.dq_comparison_result import DqComparisonResult
from organon.idq.domain.businessobjects.dq_data_column_collection import DqDataColumnCollection
from organon.idq.domain.businessobjects.record_sources.dq_df_record_source import DqDfRecordSource
from organon.idq.domain.businessobjects.statistics.data_source_statistics import DataSourceStatistics
from organon.idq.domain.businessobjects.statistics.population_statistics import PopulationStatistics
from organon.idq.domain.businessobjects.statistics.sample_statistics import SampleStatistics
from organon.idq.domain.enums.dq_run_type import DqRunType
from organon.idq.domain.enums.dq_test_group_type import DqTestGroupType
from organon.idq.domain.enums.signal_type import SignalType
from organon.idq.domain.reporting import dq_reporting_executor
from organon.idq.domain.reporting.dq_reporting_executor import DqReportingExecutor
from organon.idq.domain.reporting.objects.single_source_report_helper_params import SingleSourceReportHelperParams
from organon.idq.domain.reporting.objects.source_with_partitions_report_helper_params import \
    SourceWithPartitionsReportHelperParams
from organon.idq.domain.reporting.single_source_report_helper import SingleSourceReportHelper
from organon.idq.domain.reporting.source_with_partitions_report_helper import SourceWithPartitionsReportHelper
from organon.idq.domain.settings.calculation.dq_df_calculation_parameters import DqDfCalculationParameters
from organon.idq.domain.settings.comparison.dq_df_comparison_parameters import DqDfComparisonParameters
from organon.idq.domain.settings.dq_comparison_column_info import DqComparisonColumnInfo
from organon.idq.domain.settings.full_process.dq_df_full_process_input import DqDfFullProcessInput
from organon.idq.domain.settings.input_source.dq_df_input_source_settings import DqDfInputSourceSettings
from organon.idq.domain.settings.partition_info import PartitionInfo


def generate_dq_comparison_column_info():
    """Generate dq comparison column info"""
    dq_comparison_column_info_list_1 = []

    dq_comparison_column_info_1 = DqComparisonColumnInfo()
    dq_comparison_column_info_1.column_name = "Col1"
    dq_comparison_column_info_1.benchmark_horizon = 3
    dq_comparison_column_info_1.duplicate_column_control = True

    dq_comparison_column_info_2 = DqComparisonColumnInfo()
    dq_comparison_column_info_2.column_name = "Col2"
    dq_comparison_column_info_2.benchmark_horizon = None
    dq_comparison_column_info_2.duplicate_column_control = False

    dq_comparison_column_info_3 = DqComparisonColumnInfo()
    dq_comparison_column_info_3.column_name = "Col3"
    dq_comparison_column_info_3.benchmark_horizon = 3
    dq_comparison_column_info_3.duplicate_column_control = True

    dq_comparison_column_info_4 = DqComparisonColumnInfo()
    dq_comparison_column_info_4.column_name = "Col4"
    dq_comparison_column_info_4.benchmark_horizon = 3
    dq_comparison_column_info_4.duplicate_column_control = False

    dq_comparison_column_info_list_1.extend([dq_comparison_column_info_1, dq_comparison_column_info_2,
                                             dq_comparison_column_info_3, dq_comparison_column_info_4])

    return dq_comparison_column_info_list_1


def _filter_func(dataframe):
    return dataframe


def _get_full_process_input():
    inp = DqDfFullProcessInput()

    calc_params = DqDfCalculationParameters()
    calc_params.input_source_settings = DqDfInputSourceSettings(DqDfRecordSource(pd.DataFrame(), name="my_df"))
    calc_params.calculation_name = "calc1"
    partition_info = PartitionInfo()
    partition_info.column_name = "a"
    partition_info.column_values = [1, 2]
    calc_params.input_source_settings.partition_info_list = [partition_info]

    calc_params2 = DqDfCalculationParameters()
    calc_params2.input_source_settings = DqDfInputSourceSettings(DqDfRecordSource(pd.DataFrame(), name="my_df2"))
    calc_params2.calculation_name = "calc2"
    partition_info = PartitionInfo()
    partition_info.column_name = "a"
    partition_info.column_values = [3, 4]
    calc_params2.input_source_settings.partition_info_list = [partition_info]
    calc_params2.input_source_settings.filter_callable = _filter_func

    inp.calculation_parameters = [calc_params, calc_params2]
    comp_params = DqDfComparisonParameters()
    inp.comparison_parameters = comp_params
    inp.comparison_parameters.comparison_columns = generate_dq_comparison_column_info()
    return inp


def _get_calculation_result(calculation_name:str):
    res = DqCalculationResult()
    res.data_source_stats = DataSourceStatistics()
    dq_data_column = DqDataColumn()
    dq_data_column.column_name = "Col1"
    dq_data_column.column_native_type = ColumnNativeType.Numeric

    dq_data_column_2 = DqDataColumn()
    dq_data_column_2.column_name = "Col2"
    dq_data_column_2.column_native_type = ColumnNativeType.Numeric

    dq_data_column_3 = DqDataColumn()
    dq_data_column_3.column_name = "Col3"
    dq_data_column_3.column_native_type = ColumnNativeType.String

    dq_data_column_collection = DqDataColumnCollection()
    dq_data_column_list = [dq_data_column, dq_data_column_2]
    if calculation_name == "calc2":
        dq_data_column_list.append(dq_data_column_3)
    dq_data_column_collection.column_list = dq_data_column_list
    res.data_source_stats.data_column_collection = dq_data_column_collection
    res.sample_stats = SampleStatistics()
    res.population_stats = PopulationStatistics()
    res.calculation_name = calculation_name
    return res


class DqReportingExecutorTestCase(unittest.TestCase):
    """Test class for DqReportingExecutor class."""

    def setUp(self) -> None:
        comparison_result = DqComparisonResult(
            DataEntityType.TABLE, "df",
            DqTestGroupType.TABLE_SCHEMA_CONTROLS,
            DqComparisonResultCode.TABLE_IS_EMPTY, "table empty control", "table empty control"
        )
        comparison_result.signal_type = SignalType.RED
        comparison_result2 = DqComparisonResult(
            DataEntityType.COLUMN, "Col1",
            DqTestGroupType.TRAFFIC_LIGHT_CONTROLS,
            DqComparisonResultCode.COLUMN995_IS_OUTSIDE_MEAN_TRAFFIC_LIGHT_INTERVAL,
            "TL Control", "TL Control")
        comparison_result2.signal_type = SignalType.RED

        comparison_result3 = DqComparisonResult(
            DataEntityType.COLUMN, "Col2",
            DqTestGroupType.DISTRIBUTIONAL_COMPARISONS,
            DqComparisonResultCode.COLUMN005_ABSOLUTE_DEVIATION_FROM_PREDICTION_IS_MAXIMUM,
            "DISTRIBUTIONAL_COMPARISONS", "DISTRIBUTIONAL_COMPARISONS")
        comparison_result3.signal_type = SignalType.RED
        self.list_comparison_results = [comparison_result, comparison_result2, comparison_result3]
        self.calculation_results = [_get_calculation_result("calc1"), _get_calculation_result("calc2")]
        self.full_process_input = _get_full_process_input()

        one_df_dq_report_helper_patcher = patch(
            dq_reporting_executor.__name__ + "." + SingleSourceReportHelper.__name__)
        self.mock_one_df_dq_report_helper = one_df_dq_report_helper_patcher.start()
        self.addCleanup(one_df_dq_report_helper_patcher.stop)
        one_df_wit_date_dq_report_helper_patcher = patch(
            dq_reporting_executor.__name__ + "." + SourceWithPartitionsReportHelper.__name__)
        self.mock_one_df_with_partitions_dq_report_helper = one_df_wit_date_dq_report_helper_patcher.start()
        self.addCleanup(one_df_wit_date_dq_report_helper_patcher.stop)

    def _get_executor(self, run_type: DqRunType):
        return DqReportingExecutor(self.full_process_input, self.calculation_results,
                                   self.list_comparison_results,
                                   run_type)

    def test_execute_single_source(self):
        """Test execute SingleSourceReportHelper execute"""
        executor = self._get_executor(DqRunType.RUN_ONE_DATA_SOURCE)
        executor.execute()
        report_helper_params: SingleSourceReportHelperParams = \
            self.mock_one_df_dq_report_helper.call_args_list[0].args[0]

        self.assertEqual("my_df2", report_helper_params.data_source_name)
        self.assertEqual(self.list_comparison_results, report_helper_params.comparison_results)
        self.assertEqual(report_helper_params.run_type, DqRunType.RUN_ONE_DATA_SOURCE)
        self.assertEqual(3, len(report_helper_params.data_column_collection.column_list))
        self.assertEqual(self.full_process_input.calculation_parameters[-1].input_source_settings.partition_info_list,
                         report_helper_params.partition)
        self.assertEqual("_filter_func", report_helper_params.filter_str)

    def test_execute_one_source_with_partitions(self):
        """Test execute SingleSourceReportHelper execute"""
        executor = self._get_executor(DqRunType.RUN_ONE_DATA_SOURCE_WITH_PARTITIONS)
        executor.execute()
        report_helper_params: SourceWithPartitionsReportHelperParams = \
            self.mock_one_df_with_partitions_dq_report_helper.call_args_list[0].args[0]

        self.assertEqual("my_df2", report_helper_params.data_source_name)
        self.assertEqual(self.list_comparison_results, report_helper_params.comparison_results)
        self.assertEqual(report_helper_params.run_type, DqRunType.RUN_ONE_DATA_SOURCE_WITH_PARTITIONS)
        self.assertEqual(3, len(report_helper_params.data_column_collection.column_list))
        self.assertEqual(self.full_process_input.calculation_parameters[-1].input_source_settings.partition_info_list,
                         report_helper_params.partition)
        self.assertEqual("_filter_func", report_helper_params.filter_str)
        self.assertEqual(2, report_helper_params.calculation_count)
        self.assertEqual(self.full_process_input.comparison_parameters.comparison_columns,
                         report_helper_params.comparison_column_info)

        self.assertEqual(self.calculation_results[0].sample_stats, report_helper_params.sample_statistics["calc1"])
        self.assertEqual(self.calculation_results[0].population_stats,
                         report_helper_params.population_statistics["calc1"])
        self.assertEqual(self.calculation_results[0].data_source_stats,
                         report_helper_params.data_source_statistics["calc1"])
        self.assertEqual(self.calculation_results[1].sample_stats, report_helper_params.sample_statistics["calc2"])
        self.assertEqual(self.calculation_results[1].population_stats,
                         report_helper_params.population_statistics["calc2"])
        self.assertEqual(self.calculation_results[1].data_source_stats,
                         report_helper_params.data_source_statistics["calc2"])

    def test_execute_raise_exception(self):
        """Test execute DqReportingExecutor with exception"""
        executor = self._get_executor("asdfasdf")
        with self.assertRaises(Exception):
            executor.execute()
