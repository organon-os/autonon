"""Includes tests for SingleSourceReportHelper class."""
import unittest

import pandas as pd

from organon.fl.core.enums.column_native_type import ColumnNativeType
from organon.idq.core.enums.data_entity_type import DataEntityType
from organon.idq.core.enums.dq_comparison_result_code import DqComparisonResultCode
from organon.idq.domain.businessobjects.data_column.dq_data_column import DqDataColumn
from organon.idq.domain.businessobjects.dq_comparison_result import DqComparisonResult
from organon.idq.domain.businessobjects.dq_data_column_collection import DqDataColumnCollection
from organon.idq.domain.enums.dq_run_type import DqRunType
from organon.idq.domain.enums.dq_test_group_type import DqTestGroupType
from organon.idq.domain.enums.signal_type import SignalType
from organon.idq.domain.reporting.objects.dq_comparison_sample_report_input import DqComparisonSampleReportInput
from organon.idq.domain.reporting.objects.single_source_report_helper_params import SingleSourceReportHelperParams
from organon.idq.domain.reporting.single_source_report_helper import SingleSourceReportHelper
from organon.idq.domain.settings.date_value_definition import DateValueDefinition
from organon.idq.domain.settings.partition_info import PartitionInfo


def get_partition_infos():
    """generates mock partition info list"""
    info1 = PartitionInfo()
    info1.column_name = "a"
    info1.column_values = [1, 2]

    info2 = PartitionInfo()
    info2.column_name = "b"
    info2.column_values = [DateValueDefinition(year=2010, month=1, day=10, hour=13),
                           DateValueDefinition(year=2010, month=1, day=10, hour=15)]
    return [info1, info2]


class SingleSourceReportHelperTestCase(unittest.TestCase):
    """Test class for SingleSourceReportHelper class."""

    def setUp(self) -> None:
        dq_data_column = DqDataColumn()
        dq_data_column.column_name = "Col1"
        dq_data_column.column_native_type = ColumnNativeType.Numeric

        dq_data_column_2 = DqDataColumn()
        dq_data_column_2.column_name = "Col2"
        dq_data_column_2.column_native_type = ColumnNativeType.Numeric
        dq_data_column_collection = DqDataColumnCollection()
        dq_data_column_list = [dq_data_column, dq_data_column_2]
        dq_data_column_collection.column_list = dq_data_column_list
        self.dq_data_column_collection = dq_data_column_collection

        comparison_result = DqComparisonResult(
            DataEntityType.TABLE, "df",
            DqTestGroupType.TABLE_SCHEMA_CONTROLS,
            DqComparisonResultCode.TABLE_IS_EMPTY, "table empty control", "table empty control"
        )
        comparison_result.signal_type = SignalType.RED

        comparison_result2 = DqComparisonResult(
            DataEntityType.COLUMN, "Col1",
            DqTestGroupType.TRAFFIC_LIGHT_CONTROLS,
            DqComparisonResultCode.COLUMN995_IS_OUTSIDE_MEAN_TRAFFIC_LIGHT_INTERVAL,
            "TL Control", "TL Control")
        comparison_result2.signal_type = SignalType.RED

        comparison_result3 = DqComparisonResult(
            DataEntityType.COLUMN, "Col2",
            DqTestGroupType.DISTRIBUTIONAL_COMPARISONS,
            DqComparisonResultCode.COLUMN005_ABSOLUTE_DEVIATION_FROM_PREDICTION_IS_MAXIMUM,
            "DISTRIBUTIONAL_COMPARISONS", "DISTRIBUTIONAL_COMPARISONS")
        comparison_result3.signal_type = SignalType.RED

        list_comparison_results = [comparison_result, comparison_result2, comparison_result3]
        self.comparison_result = list_comparison_results
        self.dq_comparison_report_input = DqComparisonSampleReportInput()
        self.dq_comparison_report_input.dq_data_column_collection = self.dq_data_column_collection
        self.dq_comparison_report_input.dq_comparison_result = self.comparison_result
        self.params = SingleSourceReportHelperParams("df", self.comparison_result,
                                                     self.dq_data_column_collection,
                                                     DqRunType.RUN_ONE_DATA_SOURCE)
        self.dq_report_helper_service = SingleSourceReportHelper(self.params)

    def test_execute(self):
        """Test for execute"""
        output_report = self.dq_report_helper_service.execute()
        data_alert = [['df', DataEntityType.TABLE.name, DqRunType.RUN_ONE_DATA_SOURCE.name],
                      ['df', "Col1", DqRunType.RUN_ONE_DATA_SOURCE.name],
                      ['df', "Col2", DqRunType.RUN_ONE_DATA_SOURCE.name]]
        expected_alert_df = pd.DataFrame(data_alert, columns=['data_source_name', 'alert', 'run_type'])
        pd.testing.assert_frame_equal(output_report.alerts_df, expected_alert_df, check_dtype=True)
        data_signal = [["df", DataEntityType.TABLE.name, DqComparisonResultCode.TABLE_IS_EMPTY.name],
                       ["Col1", ColumnNativeType.Numeric.name,
                        DqComparisonResultCode.COLUMN995_IS_OUTSIDE_MEAN_TRAFFIC_LIGHT_INTERVAL.name],
                       ["Col2", ColumnNativeType.Numeric.name,
                        DqComparisonResultCode.COLUMN005_ABSOLUTE_DEVIATION_FROM_PREDICTION_IS_MAXIMUM.name]
                       ]
        expected_signal_df = pd.DataFrame(data_signal, columns=['data_entity_name', 'column_type', 'signal'])
        pd.testing.assert_frame_equal(output_report.signals_df, expected_signal_df, check_dtype=True)

    def test_execute_with_partitions(self):
        """Test for execute when partition not None"""
        self.params.partition = get_partition_infos()
        partition_str = '[a=(1,2)] & [b=({y=2010,m=1,d=10,h=13},{y=2010,m=1,d=10,h=15})]'
        output_report = self.dq_report_helper_service.execute()
        data_alert = [['df', partition_str, DataEntityType.TABLE.name, DqRunType.RUN_ONE_DATA_SOURCE.name],
                      ['df', partition_str, "Col1", DqRunType.RUN_ONE_DATA_SOURCE.name],
                      ['df', partition_str, "Col2", DqRunType.RUN_ONE_DATA_SOURCE.name]]
        expected_alert_df = pd.DataFrame(data_alert, columns=['data_source_name', 'filter', 'alert', 'run_type'])
        pd.testing.assert_frame_equal(output_report.alerts_df, expected_alert_df, check_dtype=True)
        data_signal = [["df", DataEntityType.TABLE.name, DqComparisonResultCode.TABLE_IS_EMPTY.name],
                       ["Col1", ColumnNativeType.Numeric.name,
                        DqComparisonResultCode.COLUMN995_IS_OUTSIDE_MEAN_TRAFFIC_LIGHT_INTERVAL.name],
                       ["Col2", ColumnNativeType.Numeric.name,
                        DqComparisonResultCode.COLUMN005_ABSOLUTE_DEVIATION_FROM_PREDICTION_IS_MAXIMUM.name]
                       ]
        expected_signal_df = pd.DataFrame(data_signal, columns=['data_entity_name', 'column_type', 'signal'])
        pd.testing.assert_frame_equal(output_report.signals_df, expected_signal_df, check_dtype=True)

    def test_execute_with_filter_str(self):
        """Test for execute when filter_str not None"""
        self.params.filter_str = "some filter"
        filter_val = f"Filtered By:'{self.params.filter_str}'"
        output_report = self.dq_report_helper_service.execute()
        data_alert = [['df', filter_val, DataEntityType.TABLE.name, DqRunType.RUN_ONE_DATA_SOURCE.name],
                      ['df', filter_val, "Col1", DqRunType.RUN_ONE_DATA_SOURCE.name],
                      ['df', filter_val, "Col2", DqRunType.RUN_ONE_DATA_SOURCE.name]]
        expected_alert_df = pd.DataFrame(data_alert, columns=['data_source_name', 'filter', 'alert', 'run_type'])
        pd.testing.assert_frame_equal(output_report.alerts_df, expected_alert_df, check_dtype=True)
        data_signal = [["df", DataEntityType.TABLE.name, DqComparisonResultCode.TABLE_IS_EMPTY.name],
                       ["Col1", ColumnNativeType.Numeric.name,
                        DqComparisonResultCode.COLUMN995_IS_OUTSIDE_MEAN_TRAFFIC_LIGHT_INTERVAL.name],
                       ["Col2", ColumnNativeType.Numeric.name,
                        DqComparisonResultCode.COLUMN005_ABSOLUTE_DEVIATION_FROM_PREDICTION_IS_MAXIMUM.name]
                       ]
        expected_signal_df = pd.DataFrame(data_signal, columns=['data_entity_name', 'column_type', 'signal'])
        pd.testing.assert_frame_equal(output_report.signals_df, expected_signal_df, check_dtype=True)

    def test_execute_with_partition_and_filter_str(self):
        """Test for execute when partition and  filter_str not None"""
        self.params.filter_str = "some filter"
        partition_str = '[a=(1,2)] & [b=({y=2010,m=1,d=10,h=13},{y=2010,m=1,d=10,h=15})]'
        self.params.partition = get_partition_infos()
        filter_val = f"Partition:{partition_str} - Filtered By:'{self.params.filter_str}'"
        output_report = self.dq_report_helper_service.execute()
        data_alert = [['df', filter_val, DataEntityType.TABLE.name, DqRunType.RUN_ONE_DATA_SOURCE.name],
                      ['df', filter_val, "Col1", DqRunType.RUN_ONE_DATA_SOURCE.name],
                      ['df', filter_val, "Col2", DqRunType.RUN_ONE_DATA_SOURCE.name]]
        expected_alert_df = pd.DataFrame(data_alert, columns=['data_source_name', 'filter', 'alert', 'run_type'])
        pd.testing.assert_frame_equal(output_report.alerts_df, expected_alert_df, check_dtype=True)
        data_signal = [["df", DataEntityType.TABLE.name, DqComparisonResultCode.TABLE_IS_EMPTY.name],
                       ["Col1", ColumnNativeType.Numeric.name,
                        DqComparisonResultCode.COLUMN995_IS_OUTSIDE_MEAN_TRAFFIC_LIGHT_INTERVAL.name],
                       ["Col2", ColumnNativeType.Numeric.name,
                        DqComparisonResultCode.COLUMN005_ABSOLUTE_DEVIATION_FROM_PREDICTION_IS_MAXIMUM.name]
                       ]
        expected_signal_df = pd.DataFrame(data_signal, columns=['data_entity_name', 'column_type', 'signal'])
        pd.testing.assert_frame_equal(output_report.signals_df, expected_signal_df, check_dtype=True)
