"""Includes tests for NumericColumnControlDetails class."""
import unittest

from organon.fl.core.enums.column_native_type import ColumnNativeType
from organon.idq.domain.reporting.objects.numeric_column_control_details import NumericColumnControlDetails


class NumericColumnControlDetailsTestCase(unittest.TestCase):
    """Test class for NumericColumnControlDetails class."""

    def test_get_dict(self):
        """Test for get dict """
        numeric_column_control_detail = NumericColumnControlDetails()
        numeric_column_control_detail.column_name = 'Col1'
        numeric_column_control_detail.calculation_name = "Col1 Calculation"
        numeric_column_control_detail.column_type = ColumnNativeType.Numeric
        numeric_column_control_detail.mean = 3
        numeric_column_control_detail.trimmed_mean = 3.2
        numeric_column_control_detail.state_of_benchmark = 2
        numeric_column_control_detail_dict = numeric_column_control_detail.to_dict()

        expected_numeric_column_control_detail_dict = {'column_name': 'Col1',
                                                       'calculation_name': "Col1 Calculation",
                                                       'column_type': ColumnNativeType.Numeric.name,
                                                       'mean': 3,
                                                       'trimmed_mean': 3.2, 'state_of_benchmark': 2
                                                       }
        self.assertEqual(numeric_column_control_detail_dict == expected_numeric_column_control_detail_dict, True)
