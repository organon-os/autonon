"""Includes tests for NominalColumnControlDetails class."""
import unittest

from organon.fl.core.enums.column_native_type import ColumnNativeType
from organon.idq.domain.reporting.objects.nominal_column_control_details import NominalColumnControlDetails


class NominalColumnControlDetailsTestCase(unittest.TestCase):
    """Test class for NominalColumnControlDetails class."""

    def test_get_dict(self):
        """Test for get dict """
        nominal_column_control_detail = NominalColumnControlDetails()
        nominal_column_control_detail.column_name = 'Col1'
        nominal_column_control_detail.calculation_name = "Col1 Calculation"
        nominal_column_control_detail.column_type = ColumnNativeType.String
        nominal_column_control_detail.value = "ADU"
        nominal_column_control_detail.percentage = 0.5
        nominal_column_control_detail.state_of_benchmark = 2
        nominal_column_control_detail_dict = nominal_column_control_detail.to_dict()

        expected_nominal_column_control_detail_dict = {'column_name': 'Col1',
                                                       'calculation_name': "Col1 Calculation",
                                                       'column_type': ColumnNativeType.String.name,
                                                       'value': "ADU",
                                                       'percentage': 0.5, 'state_of_benchmark': 2
                                                       }
        self.assertEqual(nominal_column_control_detail_dict == expected_nominal_column_control_detail_dict, True)
