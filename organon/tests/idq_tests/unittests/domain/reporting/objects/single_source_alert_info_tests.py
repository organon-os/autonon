"""Includes tests for SingleSourceAlertInfo class."""

import unittest

from organon.idq.domain.enums.dq_run_type import DqRunType
from organon.idq.domain.reporting.objects.single_source_alert_info import SingleSourceAlertInfo


class SingleSourceAlertInfoTestCase(unittest.TestCase):
    """Test class for SingleSourceAlertInfo class."""

    def test_get_dict(self):
        """Test for get dict """

        alert_info = SingleSourceAlertInfo()
        alert_info.data_source_name = "df1"
        alert_info.alert = "col1"
        alert_info.run_type = DqRunType.RUN_ONE_DATA_SOURCE
        alert_info_dict = alert_info.to_dict()
        expected_alert_info_dict = {'data_source_name': 'df1',
                                    'alert': 'col1',
                                    'run_type': DqRunType.RUN_ONE_DATA_SOURCE.name}
        self.assertEqual(alert_info_dict == expected_alert_info_dict, True)
