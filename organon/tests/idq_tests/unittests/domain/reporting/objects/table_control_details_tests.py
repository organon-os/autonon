"""Includes tests for TableControlDetails class."""
import unittest

from organon.idq.core.enums.data_entity_type import DataEntityType
from organon.idq.domain.reporting.objects.table_control_details import TableControlDetails


class TableControlDetailsTestCase(unittest.TestCase):
    """Test class for TableControlDetails class."""

    def test_get_dict(self):
        """Test for get dict """
        table_control_detail = TableControlDetails()
        table_control_detail.type = DataEntityType.TABLE
        table_control_detail.calculation_name = "Table Control"
        table_control_detail.property_key = "RowCount"
        table_control_detail.value = 250
        table_control_detail.state_of_benchmark = 1
        table_control_detail_dict = table_control_detail.to_dict()

        expected_table_control_detail = {'type': DataEntityType.TABLE.name,
                                         'calculation_name': "Table Control",
                                         'property_key': "RowCount",
                                         'value': 250, 'state_of_benchmark': 1
                                         }
        self.assertEqual(table_control_detail_dict == expected_table_control_detail, True)
