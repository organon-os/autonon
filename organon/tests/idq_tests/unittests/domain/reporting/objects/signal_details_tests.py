"""Includes tests for SignalDetails class."""
import unittest

from organon.idq.core.enums.dq_comparison_result_code import DqComparisonResultCode
from organon.idq.domain.reporting.objects.signal_details import SignalDetails


class SignalDetailsTestCase(unittest.TestCase):
    """Test class for SignalDetails class."""

    def test_get_dict(self):
        """Test for get dict """
        signal_detail = SignalDetails()
        signal_detail.data_entity_name = "col1"
        signal_detail.column_type = "Table"
        signal_detail.signal = DqComparisonResultCode.TABLE_IS_EMPTY.name
        signal_detail = signal_detail.to_dict()

        expected_signal_detail_dict = {'data_entity_name': 'col1',
                                       'column_type': 'Table',
                                       'signal': DqComparisonResultCode.TABLE_IS_EMPTY.name}
        self.assertEqual(signal_detail == expected_signal_detail_dict, True)
