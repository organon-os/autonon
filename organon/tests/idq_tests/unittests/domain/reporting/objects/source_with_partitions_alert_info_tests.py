"""Includes tests for SourceWithDateAlertInfo class."""

import unittest

from organon.idq.domain.enums.dq_run_type import DqRunType
from organon.idq.domain.reporting.objects.source_with_partitions_alert_info import SourceWithPartitionsAlertInfo


class SourceWithPartitionsAlertInfoTestCase(unittest.TestCase):
    """Test class for SourceWithDateAlertInfo class."""

    def test_get_dict(self):
        """Test for get dict """

        alert_info = SourceWithPartitionsAlertInfo()
        alert_info.data_source_name = "df1"
        alert_info.alert = "col1"
        alert_info.run_type = DqRunType.RUN_ONE_DATA_SOURCE_WITH_PARTITIONS
        alert_info_dict = alert_info.to_dict()
        expected_alert_info_dict = {'data_source_name': 'df1',
                                    'alert': 'col1',
                                    'run_type': DqRunType.RUN_ONE_DATA_SOURCE_WITH_PARTITIONS.name,
                                    }
        self.assertDictEqual(expected_alert_info_dict, alert_info_dict)

    def test_get_dict_with_partitions(self):
        """Test for get dict """

        alert_info = SourceWithPartitionsAlertInfo()
        alert_info.data_source_name = "df1"
        alert_info.alert = "col1"
        alert_info.full_filter_str = "dummy"
        alert_info.run_type = DqRunType.RUN_ONE_DATA_SOURCE_WITH_PARTITIONS
        alert_info_dict = alert_info.to_dict()
        expected_alert_info_dict = {'data_source_name': 'df1',
                                    'filter': "dummy",
                                    'alert': 'col1',
                                    'run_type': DqRunType.RUN_ONE_DATA_SOURCE_WITH_PARTITIONS.name,
                                    }
        self.assertDictEqual(expected_alert_info_dict, alert_info_dict)
