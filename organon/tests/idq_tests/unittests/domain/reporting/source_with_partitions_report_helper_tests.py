"""Includes tests for SourceWithDateReportHelper class."""
import unittest
from typing import List
from unittest.mock import patch, MagicMock

import pandas as pd

from organon.fl.core.enums.column_native_type import ColumnNativeType
from organon.idq.core.enums.data_entity_type import DataEntityType
from organon.idq.core.enums.dq_comparison_result_code import DqComparisonResultCode
from organon.idq.domain.businessobjects.data_column.dq_data_column import DqDataColumn
from organon.idq.domain.businessobjects.dq_comparison_result import DqComparisonResult
from organon.idq.domain.businessobjects.dq_data_column_collection import DqDataColumnCollection
from organon.idq.domain.businessobjects.statistics.data_source_statistics import DataSourceStatistics
from organon.idq.domain.businessobjects.statistics.dq_categorical_statistics import DqCategoricalStatistics
from organon.idq.domain.businessobjects.statistics.dq_sample_numerical_statistics import DqSampleNumericalStatistics
from organon.idq.domain.businessobjects.statistics.population_statistics import PopulationStatistics
from organon.idq.domain.businessobjects.statistics.sample_statistics import SampleStatistics
from organon.idq.domain.enums.dq_run_type import DqRunType
from organon.idq.domain.enums.dq_test_group_type import DqTestGroupType
from organon.idq.domain.enums.signal_type import SignalType
from organon.idq.domain.reporting.objects.source_with_partitions_alert_info import SourceWithPartitionsAlertInfo
from organon.idq.domain.reporting.objects.source_with_partitions_report_helper_params import \
    SourceWithPartitionsReportHelperParams
from organon.idq.domain.reporting.source_with_partitions_report_helper import SourceWithPartitionsReportHelper
from organon.idq.domain.settings.date_value_definition import DateValueDefinition
from organon.idq.domain.settings.dq_comparison_column_info import DqComparisonColumnInfo
from organon.idq.domain.settings.partition_info import PartitionInfo


def generate_dq_comparison_column_info():
    """Generate dq comparison column info"""
    dq_comparison_column_info_list_1 = []

    dq_comparison_column_info_1 = DqComparisonColumnInfo()
    dq_comparison_column_info_1.column_name = "Col1"
    dq_comparison_column_info_1.benchmark_horizon = 3
    dq_comparison_column_info_1.duplicate_column_control = True

    dq_comparison_column_info_2 = DqComparisonColumnInfo()
    dq_comparison_column_info_2.column_name = "Col2"
    dq_comparison_column_info_2.benchmark_horizon = None
    dq_comparison_column_info_2.duplicate_column_control = False

    dq_comparison_column_info_3 = DqComparisonColumnInfo()
    dq_comparison_column_info_3.column_name = "Col3"
    dq_comparison_column_info_3.benchmark_horizon = 3
    dq_comparison_column_info_3.duplicate_column_control = True

    dq_comparison_column_info_4 = DqComparisonColumnInfo()
    dq_comparison_column_info_4.column_name = "Col4"
    dq_comparison_column_info_4.benchmark_horizon = 3
    dq_comparison_column_info_4.duplicate_column_control = False

    dq_comparison_column_info_list_1.extend([dq_comparison_column_info_1, dq_comparison_column_info_2,
                                             dq_comparison_column_info_3, dq_comparison_column_info_4])

    return dq_comparison_column_info_list_1


def get_data_column_list():
    """Get data column list"""
    data_column_list = []
    dq_data_column_1 = DqDataColumn()
    dq_data_column_1.column_name = "Col1"
    dq_data_column_1.column_native_type = ColumnNativeType.Numeric
    dq_data_column_1.default_values = [1, 2]

    dq_data_column_2 = DqDataColumn()
    dq_data_column_2.column_name = "Col2"
    dq_data_column_2.column_native_type = ColumnNativeType.String
    dq_data_column_2.default_values = ["1", "2"]

    dq_data_column_3 = DqDataColumn()
    dq_data_column_3.column_name = "Col3"
    dq_data_column_3.column_native_type = ColumnNativeType.Numeric
    dq_data_column_3.default_values = []

    dq_data_column_4 = DqDataColumn()
    dq_data_column_4.column_name = "Col4"
    dq_data_column_4.column_native_type = ColumnNativeType.String
    dq_data_column_4.default_values = None

    data_column_list.extend([dq_data_column_1, dq_data_column_2, dq_data_column_3, dq_data_column_4])
    return data_column_list


def get_idq_comparison_result():
    """Get Idq comparison result"""
    comparison_result1 = DqComparisonResult(
        DataEntityType.TABLE, "df",
        DqTestGroupType.TABLE_SCHEMA_CONTROLS,
        DqComparisonResultCode.TABLE_IS_EMPTY, "table empty control", "table empty control"
    )
    comparison_result1.signal_type = SignalType.RED

    comparison_result2 = DqComparisonResult(
        DataEntityType.COLUMN, "Col1",
        DqTestGroupType.DISTRIBUTIONAL_COMPARISONS,
        DqComparisonResultCode.UNEXPECTED_MAXIMUM,
        "UNEXPECTED_MAXIMUM", "MaximumPsiCategory")
    comparison_result2.signal_type = SignalType.RED

    comparison_result3 = DqComparisonResult(
        DataEntityType.COLUMN, "Col2",
        DqTestGroupType.DISTRIBUTIONAL_COMPARISONS,
        DqComparisonResultCode.PSI_RED_SIGNAL,
        "PSI_RED_SIGNAL", "MaximumPsiCategory")
    comparison_result3.property_key_nominal = "ADU"
    comparison_result3.signal_type = SignalType.RED

    comparison_result4 = DqComparisonResult(
        DataEntityType.COLUMN, "Col3",
        DqTestGroupType.DISTRIBUTIONAL_COMPARISONS,
        DqComparisonResultCode.PSI_RED_SIGNAL,
        "PSI_RED_SIGNAL", "PSI_RED_SIGNAL")
    comparison_result4.signal_type = SignalType.RED

    comparison_result5 = DqComparisonResult(
        DataEntityType.COLUMN, "Col4",
        DqTestGroupType.DISTRIBUTIONAL_COMPARISONS,
        DqComparisonResultCode.COLUMNN_VALUES_CONSIST_OF_DEFAULT_VALUES_ONLY,
        "COLUMNN_VALUES_CONSIST_OF_DEFAULT_VALUES_ONLY", "COLUMNN_VALUES_CONSIST_OF_DEFAULT_VALUES_ONLY")
    comparison_result5.signal_type = SignalType.RED

    comparison_result6 = DqComparisonResult(
        DataEntityType.TABLE, "df",
        DqTestGroupType.TABLE_SCHEMA_CONTROLS,
        DqComparisonResultCode.ROW_COUNT_MEAN_IS_OUTSIDE_MEAN_TRAFFIC_LIGHT_INTERVAL,
        "ROW_COUNT_MEAN_IS_OUTSIDE_MEAN_TRAFFIC_LIGHT_INTERVAL",
        "ROW_COUNT_MEAN_IS_OUTSIDE_MEAN_TRAFFIC_LIGHT_INTERVAL"
    )
    comparison_result6.signal_type = SignalType.RED

    comparison_result7 = DqComparisonResult(
        DataEntityType.COLUMN, "Col3",
        DqTestGroupType.DISTRIBUTIONAL_COMPARISONS,
        DqComparisonResultCode.COLUMN_IS_CONSTANT,
        "ColumnIsConstant", "ColumnIsConstant")
    comparison_result7.signal_type = SignalType.RED

    comparison_result_list = [comparison_result1, comparison_result2, comparison_result3,
                              comparison_result4, comparison_result5, comparison_result6, comparison_result7]

    return comparison_result_list


def get_idq_comparison_result2():
    """Get Idq comparison result for test bmh smaller five"""
    comparison_result1 = DqComparisonResult(
        DataEntityType.TABLE, "df",
        DqTestGroupType.TABLE_SCHEMA_CONTROLS,
        DqComparisonResultCode.TABLE_IS_EMPTY, "table empty control", "table empty control"
    )
    comparison_result1.signal_type = SignalType.RED

    comparison_result2 = DqComparisonResult(
        DataEntityType.COLUMN, "Col1",
        DqTestGroupType.DISTRIBUTIONAL_COMPARISONS,
        DqComparisonResultCode.UNEXPECTED_MAXIMUM,
        "UNEXPECTED_MAXIMUM", "MaximumPsiCategory")
    comparison_result2.signal_type = SignalType.RED

    comparison_result3 = DqComparisonResult(
        DataEntityType.COLUMN, "Col2",
        DqTestGroupType.DISTRIBUTIONAL_COMPARISONS,
        DqComparisonResultCode.COLUMN_IS_CONSTANT,
        "ColumnIsConstant", "ColumnIsConstant")
    comparison_result3.property_key_nominal = "ADU"
    comparison_result3.signal_type = SignalType.RED

    comparison_result4 = DqComparisonResult(
        DataEntityType.COLUMN, "Col3",
        DqTestGroupType.DISTRIBUTIONAL_COMPARISONS,
        DqComparisonResultCode.PSI_RED_SIGNAL,
        "PSI_RED_SIGNAL", "PSI_RED_SIGNAL")
    comparison_result4.signal_type = SignalType.RED

    comparison_result5 = DqComparisonResult(
        DataEntityType.COLUMN, "Col4",
        DqTestGroupType.DISTRIBUTIONAL_COMPARISONS,
        DqComparisonResultCode.COLUMNN_VALUES_CONSIST_OF_DEFAULT_VALUES_ONLY,
        "COLUMNN_VALUES_CONSIST_OF_DEFAULT_VALUES_ONLY", "COLUMNN_VALUES_CONSIST_OF_DEFAULT_VALUES_ONLY")
    comparison_result5.signal_type = SignalType.RED

    comparison_result6 = DqComparisonResult(
        DataEntityType.TABLE, "df",
        DqTestGroupType.TABLE_SCHEMA_CONTROLS,
        DqComparisonResultCode.ROW_COUNT_MEAN_IS_OUTSIDE_MEAN_TRAFFIC_LIGHT_INTERVAL,
        "ROW_COUNT_MEAN_IS_OUTSIDE_MEAN_TRAFFIC_LIGHT_INTERVAL",
        "ROW_COUNT_MEAN_IS_OUTSIDE_MEAN_TRAFFIC_LIGHT_INTERVAL"
    )
    comparison_result6.signal_type = SignalType.RED

    comparison_result7 = DqComparisonResult(
        DataEntityType.COLUMN, "Col3",
        DqTestGroupType.MODELLING_AND_CI_CONTROLS_COLUMN_SET,
        DqComparisonResultCode.COLUMN_MEAN_IS_OUTSIDE_MEAN_CONFIDENCE_INTERVAL,
        "ColumnIsConstant", "ColumnIsConstant")
    comparison_result7.signal_type = SignalType.RED

    comparison_result8 = DqComparisonResult(
        DataEntityType.COLUMN, "Col3",
        DqTestGroupType.MODELLING_AND_CI_CONTROLS_COLUMN_SET,
        DqComparisonResultCode.COLUMN_MEAN_IS_OUTSIDE_MEAN_TRAFFIC_LIGHT_INTERVAL,
        "ColumnIsConstant", "ColumnIsConstant")
    comparison_result8.signal_type = SignalType.RED

    comparison_result_list = [comparison_result1, comparison_result2, comparison_result3,
                              comparison_result4, comparison_result5, comparison_result6,
                              comparison_result7, comparison_result8]

    return comparison_result_list


def get_table_stats():
    """Return data source stats"""
    table_stats_dict = {}
    data_source_statistics_1 = DataSourceStatistics()
    data_source_statistics_1.row_count = 200
    data_source_statistics_1.size_in_bytes = 250
    data_column_collection = DqDataColumnCollection()
    data_column_collection.column_list = get_data_column_list()
    data_source_statistics_1.data_column_collection = data_column_collection
    data_source_statistics_2 = DataSourceStatistics()
    data_source_statistics_2.row_count = 100
    data_source_statistics_2.size_in_bytes = 150
    data_source_statistics_2.data_column_collection = data_column_collection

    data_source_statistics_3 = DataSourceStatistics()
    data_source_statistics_3.row_count = 220
    data_source_statistics_3.size_in_bytes = 240
    data_source_statistics_3.data_column_collection = data_column_collection

    data_source_statistics_4 = DataSourceStatistics()
    data_source_statistics_4.row_count = 200
    data_source_statistics_4.size_in_bytes = 250
    data_source_statistics_4.data_column_collection = data_column_collection

    data_source_statistics_5 = DataSourceStatistics()
    data_source_statistics_5.row_count = 200
    data_source_statistics_5.size_in_bytes = 250
    data_source_statistics_5.data_column_collection = data_column_collection
    table_stats_dict["adu_afe_202110"] = data_source_statistics_1
    table_stats_dict["adu_afe_202111"] = data_source_statistics_2
    table_stats_dict["adu_afe_202112"] = data_source_statistics_3
    table_stats_dict["adu_afe_202113"] = data_source_statistics_4
    table_stats_dict["adu_afe_202114"] = data_source_statistics_5
    return table_stats_dict


def get_sample_stats():
    """Get Sample stats"""
    # pylint: disable=too-many-locals
    # pylint: disable=too-many-statements

    table_stats_dict = {}
    sample_stats = SampleStatistics()
    numerical_statistics_dict = {}
    numerical_stats_1 = DqSampleNumericalStatistics()
    numerical_stats_1.mean = 2
    numerical_stats_1.trimmed_mean = 2.2
    numerical_statistics_dict["Col3"] = numerical_stats_1

    nominal_nominal_dict = {}
    nominal_stats_1 = DqCategoricalStatistics([])
    frequencies_dict = {"EDU": 5, "ADU": 10}
    nominal_stats_1.frequencies = frequencies_dict
    nominal_nominal_dict["Col2"] = nominal_stats_1
    sample_stats.nominal_statistics = nominal_nominal_dict
    sample_stats.numerical_statistics = numerical_statistics_dict

    sample_stats_2 = SampleStatistics()
    numerical_statistics_dict_2 = {}
    numerical_stats_2 = DqSampleNumericalStatistics()
    numerical_stats_2.mean = 2.1
    numerical_stats_2.trimmed_mean = 2.5
    numerical_statistics_dict_2["Col3"] = numerical_stats_2

    nominal_nominal_dict_2 = {}
    nominal_stats_2 = DqCategoricalStatistics([])
    frequencies_dict_2 = {"EDU": 6, "ADU": 12}
    nominal_stats_2.frequencies = frequencies_dict_2
    nominal_nominal_dict_2["Col2"] = nominal_stats_2
    sample_stats_2.nominal_statistics = nominal_nominal_dict_2
    sample_stats_2.numerical_statistics = numerical_statistics_dict_2

    sample_stats_3 = SampleStatistics()
    numerical_statistics_dict_3 = {}
    numerical_stats_3 = DqSampleNumericalStatistics()
    numerical_stats_3.mean = 2.4
    numerical_stats_3.trimmed_mean = 2.7
    numerical_statistics_dict_3["Col3"] = numerical_stats_3

    nominal_nominal_dict_3 = {}
    nominal_stats_3 = DqCategoricalStatistics([])
    frequencies_dict_3 = {"EDU": 4, "ADU": 10}
    nominal_stats_3.frequencies = frequencies_dict_3
    nominal_nominal_dict_3["Col2"] = nominal_stats_3
    sample_stats_3.nominal_statistics = nominal_nominal_dict_3
    sample_stats_3.numerical_statistics = numerical_statistics_dict_3

    sample_stats_4 = SampleStatistics()
    numerical_statistics_dict_4 = {}
    numerical_stats_4 = DqSampleNumericalStatistics()
    numerical_stats_4.mean = 2.5
    numerical_stats_4.trimmed_mean = 2.7
    numerical_statistics_dict_4["Col3"] = numerical_stats_4

    nominal_nominal_dict_4 = {}
    nominal_stats_4 = DqCategoricalStatistics([])
    frequencies_dict_4 = {"EDU": 4, "ADU": 10}
    nominal_stats_4.frequencies = frequencies_dict_4
    nominal_nominal_dict_4["Col2"] = nominal_stats_4
    sample_stats_4.nominal_statistics = nominal_nominal_dict_4
    sample_stats_4.numerical_statistics = numerical_statistics_dict_4

    sample_stats_5 = SampleStatistics()
    numerical_statistics_dict_5 = {}
    numerical_stats_5 = DqSampleNumericalStatistics()
    numerical_stats_5.mean = 2
    numerical_stats_5.trimmed_mean = 2
    numerical_statistics_dict_5["Col3"] = numerical_stats_5

    nominal_nominal_dict_5 = {}
    nominal_stats_5 = DqCategoricalStatistics([])
    frequencies_dict_5 = {"EDU": 5, "ADU": 10}
    nominal_stats_5.frequencies = frequencies_dict_5
    nominal_nominal_dict_5["Col2"] = nominal_stats_5
    sample_stats_5.nominal_statistics = nominal_nominal_dict_5
    sample_stats_5.numerical_statistics = numerical_statistics_dict_5

    table_stats_dict["adu_afe_202110"] = sample_stats
    table_stats_dict["adu_afe_202111"] = sample_stats_2
    table_stats_dict["adu_afe_202112"] = sample_stats_3
    table_stats_dict["adu_afe_202113"] = sample_stats_4
    table_stats_dict["adu_afe_202114"] = sample_stats_5

    return table_stats_dict


def get_population_stats():
    """Get Sample stats"""
    table_stats_dict = {}
    population_stats = PopulationStatistics()
    nominal_nominal_dict = {}
    nominal_stats_1 = DqCategoricalStatistics([])
    frequencies_dict = {"EDU": 5, "ADU": 10}
    nominal_stats_1.frequencies = frequencies_dict
    nominal_nominal_dict["Col2"] = nominal_stats_1
    population_stats.nominal_statistics = nominal_nominal_dict

    population_stats_2 = PopulationStatistics()
    nominal_nominal_dict_2 = {}
    nominal_stats_2 = DqCategoricalStatistics([])
    frequencies_dict_2 = {"EDU": 6, "ADU": 12}
    nominal_stats_2.frequencies = frequencies_dict_2
    nominal_nominal_dict_2["Col2"] = nominal_stats_2
    population_stats_2.nominal_statistics = nominal_nominal_dict_2

    population_stats_3 = PopulationStatistics()
    nominal_nominal_dict_3 = {}
    nominal_stats_3 = DqCategoricalStatistics([])
    frequencies_dict_3 = {"EDU": 4, "ADU": 10}
    nominal_stats_3.frequencies = frequencies_dict_3
    nominal_nominal_dict_3["Col2"] = nominal_stats_3
    population_stats_3.nominal_statistics = nominal_nominal_dict_3

    population_stats_4 = PopulationStatistics()
    nominal_nominal_dict_4 = {}
    nominal_stats_4 = DqCategoricalStatistics([])
    frequencies_dict_4 = {"EDU": 4, "ADU": 10}
    nominal_stats_4.frequencies = frequencies_dict_4
    nominal_nominal_dict_4["Col2"] = nominal_stats_4
    population_stats_4.nominal_statistics = nominal_nominal_dict_4

    population_stats_5 = PopulationStatistics()
    nominal_nominal_dict_5 = {}
    nominal_stats_5 = DqCategoricalStatistics([])
    frequencies_dict_5 = {"EDU": 5, "ADU": 10}
    nominal_stats_5.frequencies = frequencies_dict_5
    nominal_nominal_dict_5["Col2"] = nominal_stats_5
    population_stats_5.nominal_statistics = nominal_nominal_dict_5

    table_stats_dict["adu_afe_202110"] = population_stats
    table_stats_dict["adu_afe_202111"] = population_stats_2
    table_stats_dict["adu_afe_202112"] = population_stats_3
    table_stats_dict["adu_afe_202113"] = population_stats_4
    table_stats_dict["adu_afe_202114"] = population_stats_5

    return table_stats_dict


def get_alert_list():
    """Get alert list"""
    alert1 = SourceWithPartitionsAlertInfo()
    alert1.data_source_name = "df"
    alert1.alert = DataEntityType.TABLE.name
    alert1.run_type = DqRunType.RUN_ONE_DATA_SOURCE_WITH_PARTITIONS

    alert2 = SourceWithPartitionsAlertInfo()
    alert2.data_source_name = "df"
    alert2.alert = "Col2"
    alert2.run_type = DqRunType.RUN_ONE_DATA_SOURCE_WITH_PARTITIONS

    alert3 = SourceWithPartitionsAlertInfo()
    alert3.data_source_name = "df"
    alert3.alert = "Col3"
    alert3.run_type = DqRunType.RUN_ONE_DATA_SOURCE_WITH_PARTITIONS

    alert4 = SourceWithPartitionsAlertInfo()
    alert4.data_source_name = "adu_scr"
    alert4.alert = "Col2"
    alert4.run_type = DqRunType.RUN_MULTIPLE_DATA_SOURCE

    alert_list = [alert1, alert2, alert3, alert4]

    return alert_list


def get_partition_info_list() -> List[PartitionInfo]:
    """Returns partition info mock list"""
    partition_info = PartitionInfo()
    partition_info.column_name = "int_col"
    partition_info.column_values = [5, 2, 3]

    partition_info_2 = PartitionInfo()
    partition_info_2.column_name = "date_col"
    date_val_def = DateValueDefinition()
    date_val_def.year = 2010
    date_val_def.month = 11
    date_val_def.day = 3
    date_val_def.hour = 16
    partition_info_2.column_values = [None, date_val_def]

    return [partition_info, partition_info_2]


class SourceWithPartitionsReportHelperTestCase(unittest.TestCase):
    """Test class for SourceWithDateReportHelper class."""

    def setUp(self) -> None:
        self.dq_data_column_collection = get_data_column_list()
        self.comparison_result = get_idq_comparison_result()
        self.data_column_collection = DqDataColumnCollection()
        self.data_column_collection.column_list = self.dq_data_column_collection

        self.alert_list = get_alert_list()
        one_df_with_date_dq_report_helper_params = SourceWithPartitionsReportHelperParams(
            "df", self.comparison_result,
            self.data_column_collection,
            DqRunType.RUN_ONE_DATA_SOURCE_WITH_PARTITIONS)
        one_df_with_date_dq_report_helper_params.calculation_count = 5
        one_df_with_date_dq_report_helper_params.partition = get_partition_info_list()
        one_df_with_date_dq_report_helper_params.comparison_column_info = generate_dq_comparison_column_info()
        one_df_with_date_dq_report_helper_params.data_source_statistics = get_table_stats()
        one_df_with_date_dq_report_helper_params.sample_statistics = get_sample_stats()
        one_df_with_date_dq_report_helper_params.population_statistics = get_population_stats()
        self.params = one_df_with_date_dq_report_helper_params
        self.dq_report_helper_service = SourceWithPartitionsReportHelper(self.params)

        one_df_with_date_dq_report_helper_params2 = SourceWithPartitionsReportHelperParams(
            "df", get_idq_comparison_result2(),
            self.data_column_collection,
            DqRunType.RUN_ONE_DATA_SOURCE_WITH_PARTITIONS)
        one_df_with_date_dq_report_helper_params2.calculation_count = 2
        one_df_with_date_dq_report_helper_params2.partition = get_partition_info_list()
        one_df_with_date_dq_report_helper_params2.comparison_column_info = generate_dq_comparison_column_info()
        one_df_with_date_dq_report_helper_params2.data_source_statistics = get_table_stats()
        one_df_with_date_dq_report_helper_params2.sample_statistics = get_sample_stats()
        one_df_with_date_dq_report_helper_params2.population_statistics = get_population_stats()
        self.params2 = one_df_with_date_dq_report_helper_params2
        self.dq_report_helper_service2 = SourceWithPartitionsReportHelper(self.params2)

    def test_execute(self):
        """Test for execute"""
        with patch.object(PartitionInfo, 'to_str',
                          MagicMock(
                              side_effect=["[int_col=(1,2,3)]", "[date_col=(None,{y=2010,m=11,d=3,h=16})]",
                                           "[int_col=(1,2,3)]", "[date_col=(None,{y=2010,m=11,d=3,h=16})]",
                                           "[int_col=(1,2,3)]", "[date_col=(None,{y=2010,m=11,d=3,h=16})]"])):
            output_report = self.dq_report_helper_service.execute()
        self.assertEqual(output_report.alerts_df.shape, (2, 4))
        self.assertEqual(output_report.numeric_column_control_details_df.shape, (5, 6))
        self.assertEqual(output_report.nominal_column_control_details_df.shape, (5, 6))
        self.assertEqual(output_report.signals_df.shape, (7, 3))
        self.assertEqual(output_report.table_control_details_df.shape, (0, 0))
        data_alert = [['df', '[int_col=(1,2,3)] & [date_col=(None,{y=2010,m=11,d=3,h=16})]', "Col3",
                       DqRunType.RUN_ONE_DATA_SOURCE_WITH_PARTITIONS.name],
                      ['df', '[int_col=(1,2,3)] & [date_col=(None,{y=2010,m=11,d=3,h=16})]', "Col2",
                       DqRunType.RUN_ONE_DATA_SOURCE_WITH_PARTITIONS.name]]
        expected_alert_df = pd.DataFrame(data_alert, columns=['data_source_name', 'filter', 'alert', 'run_type'])
        pd.testing.assert_frame_equal(output_report.alerts_df, expected_alert_df, check_dtype=True)
        data_signal = [["df", DataEntityType.TABLE.name, DqComparisonResultCode.TABLE_IS_EMPTY.name],
                       ["Col1", ColumnNativeType.Numeric.name,
                        DqComparisonResultCode.UNEXPECTED_MAXIMUM.name],
                       ["Col2", ColumnNativeType.String.name,
                        DqComparisonResultCode.PSI_RED_SIGNAL.name],
                       ["Col3", ColumnNativeType.Numeric.name,
                        DqComparisonResultCode.PSI_RED_SIGNAL.name],
                       ["Col4", ColumnNativeType.String.name,
                        DqComparisonResultCode.COLUMNN_VALUES_CONSIST_OF_DEFAULT_VALUES_ONLY.name],
                       ["df", DataEntityType.TABLE.name,
                        DqComparisonResultCode.ROW_COUNT_MEAN_IS_OUTSIDE_MEAN_TRAFFIC_LIGHT_INTERVAL.name],
                       ["Col3", ColumnNativeType.Numeric.name,
                        DqComparisonResultCode.COLUMN_IS_CONSTANT.name],
                       ]
        expected_signal_df = pd.DataFrame(data_signal, columns=['data_entity_name', 'column_type', 'signal'])
        pd.testing.assert_frame_equal(output_report.signals_df, expected_signal_df, check_dtype=True)

        nominal_column_alert = [["adu_afe_202110", "Col2", ColumnNativeType.String.name, 0.66667, 1, "ADU"],
                                ["adu_afe_202111", "Col2", ColumnNativeType.String.name, 0.66667, 2, "ADU"],
                                ["adu_afe_202112", "Col2", ColumnNativeType.String.name, 0.71429, 3, "ADU"],
                                ["adu_afe_202113", "Col2", ColumnNativeType.String.name, 0.71429, 4, "ADU"],
                                ["adu_afe_202114", "Col2", ColumnNativeType.String.name, 0.66667, 5, "ADU"]]
        expected_nominal_column_alert_details = pd.DataFrame(nominal_column_alert,
                                                             columns=['calculation_name', 'column_name',
                                                                      'column_type', 'percentage', 'state_of_benchmark',
                                                                      'value'])
        pd.testing.assert_frame_equal(output_report.nominal_column_control_details_df,
                                      expected_nominal_column_alert_details, check_dtype=True)

        numeric_column_alert = [["adu_afe_202110", "Col3", ColumnNativeType.Numeric.name, 2, 2.2, 1],
                                ["adu_afe_202111", "Col3", ColumnNativeType.Numeric.name, 2.1, 2.5, 2],
                                ["adu_afe_202112", "Col3", ColumnNativeType.Numeric.name, 2.4, 2.7, 3],
                                ["adu_afe_202113", "Col3", ColumnNativeType.Numeric.name, 2.5, 2.7, 4],
                                ["adu_afe_202114", "Col3", ColumnNativeType.Numeric.name, 2, 2, 5]]
        expected_numeric_column_alert_details = pd.DataFrame(numeric_column_alert,
                                                             columns=['calculation_name', 'column_name',
                                                                      'column_type', 'mean', 'trimmed_mean',
                                                                      'state_of_benchmark',
                                                                      ])
        pd.testing.assert_frame_equal(output_report.numeric_column_control_details_df,
                                      expected_numeric_column_alert_details, check_dtype=True)
        self.assertEqual(True, True)

    def test_execute_bmh_smaller_five(self):
        """Test for execute for bmh smaller five"""
        with patch.object(PartitionInfo, 'to_str',
                          MagicMock(
                              side_effect=["[int_col=(1,2,3)]", "[date_col=(None,{y=2010,m=11,d=3,h=16})]",
                                           "[int_col=(1,2,3)]", "[date_col=(None,{y=2010,m=11,d=3,h=16})]",
                                           "[int_col=(1,2,3)]", "[date_col=(None,{y=2010,m=11,d=3,h=16})]"])):
            output_report = self.dq_report_helper_service2.execute()
        self.assertEqual(output_report.alerts_df.shape, (2, 4))
        self.assertEqual(output_report.numeric_column_control_details_df.shape, (5, 6))
        self.assertEqual(output_report.nominal_column_control_details_df.shape, (5, 6))
        self.assertEqual(output_report.signals_df.shape, (8, 3))
        self.assertEqual(output_report.table_control_details_df.shape, (0, 0))
        data_alert = [['df', '[int_col=(1,2,3)] & [date_col=(None,{y=2010,m=11,d=3,h=16})]', "Col3",
                       DqRunType.RUN_ONE_DATA_SOURCE_WITH_PARTITIONS.name],
                      ['df', '[int_col=(1,2,3)] & [date_col=(None,{y=2010,m=11,d=3,h=16})]', "Col2",
                       DqRunType.RUN_ONE_DATA_SOURCE_WITH_PARTITIONS.name]]
        expected_alert_df = pd.DataFrame(data_alert, columns=['data_source_name', 'filter', 'alert', 'run_type'])
        pd.testing.assert_frame_equal(output_report.alerts_df, expected_alert_df, check_dtype=True)
        data_signal = [["df", DataEntityType.TABLE.name, DqComparisonResultCode.TABLE_IS_EMPTY.name],
                       ["Col1", ColumnNativeType.Numeric.name,
                        DqComparisonResultCode.UNEXPECTED_MAXIMUM.name],
                       ["Col2", ColumnNativeType.String.name,
                        DqComparisonResultCode.COLUMN_IS_CONSTANT.name],
                       ["Col3", ColumnNativeType.Numeric.name,
                        DqComparisonResultCode.PSI_RED_SIGNAL.name],
                       ["Col4", ColumnNativeType.String.name,
                        DqComparisonResultCode.COLUMNN_VALUES_CONSIST_OF_DEFAULT_VALUES_ONLY.name],
                       ["df", DataEntityType.TABLE.name,
                        DqComparisonResultCode.ROW_COUNT_MEAN_IS_OUTSIDE_MEAN_TRAFFIC_LIGHT_INTERVAL.name],
                       ["Col3", ColumnNativeType.Numeric.name,
                        DqComparisonResultCode.COLUMN_MEAN_IS_OUTSIDE_MEAN_CONFIDENCE_INTERVAL.name],
                       ["Col3", ColumnNativeType.Numeric.name,
                        DqComparisonResultCode.COLUMN_MEAN_IS_OUTSIDE_MEAN_TRAFFIC_LIGHT_INTERVAL.name],
                       ]
        expected_signal_df = pd.DataFrame(data_signal, columns=['data_entity_name', 'column_type', 'signal'])
        pd.testing.assert_frame_equal(output_report.signals_df, expected_signal_df, check_dtype=True)

        nominal_column_alert = [["adu_afe_202110", "Col2", ColumnNativeType.String.name, 0, 1, None],
                                ["adu_afe_202111", "Col2", ColumnNativeType.String.name, 0, 2, None],
                                ["adu_afe_202112", "Col2", ColumnNativeType.String.name, 0, 3, None],
                                ["adu_afe_202113", "Col2", ColumnNativeType.String.name, 0, 4, None],
                                ["adu_afe_202114", "Col2", ColumnNativeType.String.name, 0, 5, None]]
        expected_nominal_column_alert_details = pd.DataFrame(nominal_column_alert,
                                                             columns=['calculation_name', 'column_name',
                                                                      'column_type', 'percentage', 'state_of_benchmark',
                                                                      'value'])
        pd.testing.assert_frame_equal(output_report.nominal_column_control_details_df,
                                      expected_nominal_column_alert_details, check_dtype=True)

        numeric_column_alert = [["adu_afe_202110", "Col3", ColumnNativeType.Numeric.name, 2, 2.2, 1],
                                ["adu_afe_202111", "Col3", ColumnNativeType.Numeric.name, 2.1, 2.5, 2],
                                ["adu_afe_202112", "Col3", ColumnNativeType.Numeric.name, 2.4, 2.7, 3],
                                ["adu_afe_202113", "Col3", ColumnNativeType.Numeric.name, 2.5, 2.7, 4],
                                ["adu_afe_202114", "Col3", ColumnNativeType.Numeric.name, 2, 2, 5]]
        expected_numeric_column_alert_details = pd.DataFrame(numeric_column_alert,
                                                             columns=['calculation_name', 'column_name',
                                                                      'column_type', 'mean', 'trimmed_mean',
                                                                      'state_of_benchmark',
                                                                      ])
        pd.testing.assert_frame_equal(output_report.numeric_column_control_details_df,
                                      expected_numeric_column_alert_details, check_dtype=True)
        self.assertEqual(True, True)

    def test_execute_with_only_filter_str(self):
        """tests alert df when filter_str is given"""
        self.params.partition = None
        self.params.filter_str = "some filter"
        expected_filter_str = "Filtered By:'some filter'"
        output_report = self.dq_report_helper_service.execute()
        data_alert = [['df', expected_filter_str, "Col3",
                       DqRunType.RUN_ONE_DATA_SOURCE_WITH_PARTITIONS.name],
                      ['df', expected_filter_str, "Col2",
                       DqRunType.RUN_ONE_DATA_SOURCE_WITH_PARTITIONS.name]]
        expected_alert_df = pd.DataFrame(data_alert, columns=['data_source_name', 'filter', 'alert', 'run_type'])
        pd.testing.assert_frame_equal(output_report.alerts_df, expected_alert_df, check_dtype=True)

    def test_execute_with_partition_and_filter_str(self):
        """tests alert df when filter_str and partition is given"""
        self.params.filter_str = "some filter"
        expected_filter_str = "Partition:[int_col=(5,2,3)] & [date_col=(None,{y=2010,m=11,d=3,h=16})]" \
                              " - Filtered By:'some filter'"
        output_report = self.dq_report_helper_service.execute()
        data_alert = [['df', expected_filter_str, "Col3",
                       DqRunType.RUN_ONE_DATA_SOURCE_WITH_PARTITIONS.name],
                      ['df', expected_filter_str, "Col2",
                       DqRunType.RUN_ONE_DATA_SOURCE_WITH_PARTITIONS.name]]
        expected_alert_df = pd.DataFrame(data_alert, columns=['data_source_name', 'filter', 'alert', 'run_type'])
        pd.testing.assert_frame_equal(output_report.alerts_df, expected_alert_df, check_dtype=True)
