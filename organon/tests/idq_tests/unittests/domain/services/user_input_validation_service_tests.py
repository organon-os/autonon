"""Includes UserInputValidationService tests."""
import unittest
from datetime import datetime
from unittest.mock import patch

import pandas as pd

from organon.fl.core.enums.column_native_type import ColumnNativeType
from organon.fl.core.exceptionhandling.known_exception import KnownException
from organon.fl.logging.helpers.log_helper import LogHelper
from organon.idq.domain.enums.dq_test_group_type import DqTestGroupType
from organon.idq.domain.services import base_user_input_validation_service
from organon.idq.domain.services.user_input_validation_service import UserInputValidationService
from organon.idq.services.user_settings.dq_user_input import DqUserInput
from organon.idq.services.user_settings.user_input_source_settings import UserInputSourceSettings
from organon.idq.services.user_settings.user_multi_partition_calculation_params import UserMultiPartitionCalcParams


def _get_user_input() -> DqUserInput:
    inp = DqUserInput()
    inp.use_supplied_calcs_as_comp_inputs = True
    inp.minimum_cardinality = 10
    inp.traffic_light_threshold_yellow = 0.3
    inp.traffic_light_threshold_green = 0.2
    inp.psi_threshold_green = 0.05
    inp.psi_threshold_yellow = 0.15
    inp.z_score = 1.5

    inp_settings = UserInputSourceSettings()
    inp_settings.source = pd.DataFrame({"a": [1, 2, 3], "b": ["a", "b", "c"]})
    inp_settings.sampling_ratio = 0.7
    calc_params = UserMultiPartitionCalcParams("calc_1", inp_settings, None, None, None)
    inp.calc_params = calc_params
    inp.column_default_values = {"a": ["a", "b"]}
    inp.duplicate_control_columns = ["a"]
    inp.column_benchmark_horizons = {"a": 0}
    inp.test_group_benchmark_horizons = {DqTestGroupType.TABLE_SCHEMA_CONTROLS.name: 0}

    return inp


class UserInputValidationServiceTestCase(unittest.TestCase):
    """Test class for UserInputValidationService"""

    def setUp(self) -> None:
        self.user_input = _get_user_input()
        self.column_native_types = {"a": ColumnNativeType.String, "b": ColumnNativeType.Numeric,
                                    "c": ColumnNativeType.Other}
        self.validation_service = UserInputValidationService(self.user_input, self.column_native_types)

    def test_all(self):
        """Default input should be validated"""
        self.validation_service.validate()

    def test_invalid_name_in_comparison_columns_for_df(self):
        """Column name in comparison columns should exist in the input source(test df source)"""
        inp_settings = UserInputSourceSettings()
        inp_settings.source = pd.DataFrame({"a": [1, 2, 3]})
        t_calc_params = UserMultiPartitionCalcParams("calc_1", inp_settings, None, None, None)
        self.user_input.calc_params = t_calc_params
        self.__run_invalid_name_in_comparison_columns_test()

    def test_invalid_name_in_comparison_columns_for_file(self):
        """Column name in comparison columns should exist in the input source(test file source)"""
        inp_settings = UserInputSourceSettings()
        inp_settings.source = "some file"
        t_calc_params = UserMultiPartitionCalcParams("calc_1", inp_settings, None, None, None)
        self.user_input.calc_params = t_calc_params
        self.__run_invalid_name_in_comparison_columns_test()

    def __run_invalid_name_in_comparison_columns_test(self):
        """Column name in comparison column info should exist in the input source"""
        self.user_input.column_benchmark_horizons = {"dummy": 4}
        with self.assertRaisesRegex(KnownException, r"Following comparison columns"
                                                    r"\(given in column_benchmark_horizons\) do not exist in test "
                                                    "calculation input source.*dummy.*"):
            self.validation_service.validate()
        self.user_input.column_benchmark_horizons = None
        self.user_input.duplicate_control_columns = ["dummy2"]
        with self.assertRaisesRegex(KnownException, r"Following comparison columns"
                                                    r"\(given in duplicate_control_columns\) do not exist in test "
                                                    "calculation input source.*dummy2.*"):
            self.validation_service.validate()

    def test_invalid_name_in_column_metadata_for_df(self):
        """Column name in column metadata list should exist in the input source(test df source)"""
        inp_settings = UserInputSourceSettings()
        inp_settings.source = pd.DataFrame({"a": [1, 2, 3]})
        t_calc_params = UserMultiPartitionCalcParams("calc_1", inp_settings, None, None, None)
        self.user_input.calc_params = t_calc_params
        self.__run_invalid_name_in_column_metadata_test()

    def test_invalid_name_in_column_metadata_for_file(self):
        """Column name in column metadata list should exist in the input source(test file source)"""
        inp_settings = UserInputSourceSettings()
        inp_settings.source = "some file"
        t_calc_params = UserMultiPartitionCalcParams("calc_1", inp_settings, None, None, None)
        self.user_input.calc_params = t_calc_params
        self.__run_invalid_name_in_column_metadata_test()

    def __run_invalid_name_in_column_metadata_test(self):
        """Column name in column metadata information should exist in the input source"""
        self.user_input.excluded_column_names = ["dummy"]
        with self.assertRaisesRegex(KnownException, r"Following columns\(given in excluded_column_names\) "
                                                    "do not exist in test calculation input source.*dummy.*"):
            self.validation_service.validate()
        self.user_input.excluded_column_names = None
        self.user_input.column_default_values = {"dummy2": [1, 2]}
        with self.assertRaisesRegex(KnownException, r"Following columns\(given in column_default_values\) do not exist "
                                                    "in test calculation input source.*dummy2.*"):
            self.validation_service.validate()

    def test_min_cardinality_control(self):
        """checks if error is raised when min_cardinality is outside acceptable range"""
        self.user_input.minimum_cardinality = 101
        with self.assertRaisesRegex(KnownException, "Minimum cardinality should be between .*"):
            self.validation_service.validate()

    def test_traffic_light_threshold_green_control(self):
        """checks if error is raised when traffic_light_threshold_green is outside acceptable range"""
        self.user_input.traffic_light_threshold_green = 0.5
        with self.assertRaisesRegex(KnownException, "Traffic Light Threshold Green should be between .*"):
            self.validation_service.validate()

    def test_traffic_light_threshold_yellow_control(self):
        """checks if error is raised when traffic_light_threshold_yellow is outside acceptable range"""
        self.user_input.traffic_light_threshold_yellow = 0.9
        with self.assertRaisesRegex(KnownException, "Traffic Light Threshold Yellow should be between .*"):
            self.validation_service.validate()

    def test_psi_threshold_green_control(self):
        """checks if error is raised when psi_threshold_green is outside acceptable range"""
        self.user_input.psi_threshold_green = 0.5
        with self.assertRaisesRegex(KnownException, "Psi Threshold Green should be between .*"):
            self.validation_service.validate()

    def test_psi_threshold_yellow_control(self):
        """checks if error is raised when psi_threshold_yellow is outside acceptable range"""
        self.user_input.psi_threshold_yellow = 0.9
        with self.assertRaisesRegex(KnownException, "Psi Threshold Yellow should be between .*"):
            self.validation_service.validate()

    def test_z_score_control(self):
        """checks if error is raised when z_score is outside acceptable range"""
        self.user_input.z_score = 10
        with self.assertRaisesRegex(KnownException, "Z Score should be between .*"):
            self.validation_service.validate()

    def test_sampling_ratio_control(self):
        """checks if error is raised when sampling ratio for an input source is outside acceptable range"""
        self.user_input.calc_params.input_source_settings.sampling_ratio = 1.5
        with self.assertRaisesRegex(KnownException, "Sampling ratio should be between .*"):
            self.validation_service.validate()

    def test_max_nominal_cardinality_count_control(self):
        """checks if error is raised when max_nominal_cardinality_count for a calculation is outside acceptable range"""
        self.user_input.calc_params.max_nominal_cardinality_count = 20000000
        with self.assertRaisesRegex(KnownException, "Max. nominal cardinality count .*"):
            self.validation_service.validate()

    def test_outlier_parameter_control(self):
        """checks if error is raised when outlier_parameter for a calculation is not in acceptable values"""
        self.user_input.calc_params.outlier_parameter = 2
        with self.assertRaisesRegex(KnownException, "Outlier parameter must be either 1.5 or 3"):
            self.validation_service.validate()

    def test_eligible_columns_control_df(self):
        """checks if error is raised when there are no eligible columns in data(df source)"""
        inp_settings = UserInputSourceSettings()
        inp_settings.source = pd.DataFrame({"a": [1, 2, 3]})
        self.column_native_types["a"] = ColumnNativeType.Other
        self.column_native_types.pop("b")
        self.column_native_types.pop("c")
        t_calc_params = UserMultiPartitionCalcParams("calc_1", inp_settings, None, None, None)
        self.user_input.calc_params = t_calc_params
        with self.assertRaisesRegex(KnownException, "No eligible columns found in data source of Calculation-1"):
            self.validation_service.validate()

    def test_eligible_columns_dates_excluded(self):
        """checks if error is raised when there are no eligible columns in data(df source) when date columns excluded"""
        inp_settings = UserInputSourceSettings()
        inp_settings.source = pd.DataFrame(
            {"a": [1, 2, 3], "b": [datetime(2010, 1, 1), datetime(2010, 1, 1), datetime(2010, 1, 1)]})
        self.column_native_types["a"] = ColumnNativeType.Other
        self.column_native_types["b"] = ColumnNativeType.Date
        self.column_native_types.pop("c")
        t_calc_params = UserMultiPartitionCalcParams("calc_1", inp_settings, None, None, None)
        self.user_input.calc_params = t_calc_params
        self.user_input.include_date_columns = False
        with self.assertRaisesRegex(KnownException, "No eligible columns found in data source of Calculation-1"):
            self.validation_service.validate()

    def test_input_source_none_control(self):
        """check if error is raised when source is none"""
        self.user_input.calc_params.input_source_settings.source = None
        with self.assertRaisesRegex(KnownException, "Input source cannot be None"):
            self.validation_service.validate()

    def test_file_input_source_empty_file_path_control(self):
        """check if error is raised when file input source file path is empty string"""
        self.user_input.calc_params.input_source_settings.source = ""
        with self.assertRaisesRegex(KnownException, "Input file name is empty"):
            self.validation_service.validate()

    def test_included_excluded_col_validation_non_existing_column(self):
        """tests if error is raised when a non existing column is given in included columns"""
        self.user_input.included_column_names = ["d"]
        with self.assertRaisesRegex(KnownException, r"Column \'d\' \(in included_column_names\) does not exist in "
                                                    "all data sources"):
            self.validation_service.validate()

    def test_included_excluded_col_validation_common_column_name(self):
        """tests if error is raised when a column is given in both included and excluded columns"""

        self.user_input.included_column_names = ["a", "b"]
        self.user_input.excluded_column_names = ["b", "c"]
        with self.assertRaisesRegex(KnownException, r"Columns \[\'b\'\] are given in both included_column_names "
                                                    "and excluded_columns_names"):
            self.validation_service.validate()

    def test_column_benchmark_horizon_greater_than_num_control_calcs(self):
        """tests if warning is logged if column benchmark horizon is greater than number of control calculations"""
        with patch(base_user_input_validation_service.__name__ + "." + LogHelper.__name__) as mock:
            self.user_input.column_benchmark_horizons = {"a": 5}
            self.validation_service.validate()
            mock.warning.assert_called_with("There are 0 control calculations but benchmark horizon is given "
                                            "5 for column 'a'")

    def test_test_group_benchmark_horizon_greater_than_num_control_calcs(self):
        """tests if warning is logged if test group benchmark horizon is greater than number of control calculations"""
        with patch(base_user_input_validation_service.__name__ + "." + LogHelper.__name__) as mock:
            self.user_input.test_group_benchmark_horizons = {DqTestGroupType.TABLE_SCHEMA_CONTROLS.name: 5}
            self.validation_service.validate()
            mock.warning.assert_called_with("There are 0 control calculations but benchmark horizon is given "
                                            f"5 for test group '{DqTestGroupType.TABLE_SCHEMA_CONTROLS.name}'")


if __name__ == '__main__':
    unittest.main()
