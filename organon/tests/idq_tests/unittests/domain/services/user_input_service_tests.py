"""Includes tests for UserInputService"""
import sys
import unittest
from typing import List
from unittest.mock import patch, MagicMock

import pandas as pd

from organon.fl.core.enums.column_native_type import ColumnNativeType
from organon.idq.domain.enums.dq_test_group_type import DqTestGroupType
from organon.idq.domain.services import base_user_input_service, user_input_service
from organon.idq.domain.services.user_input_service import UserInputService
from organon.idq.domain.settings.calculation.dq_df_calculation_parameters import DqDfCalculationParameters
from organon.idq.domain.settings.calculation.dq_file_calculation_parameters import DqFileCalculationParameters
from organon.idq.domain.settings.comparison.dq_df_comparison_parameters import DqDfComparisonParameters
from organon.idq.domain.settings.comparison.dq_file_comparison_parameters import DqFileComparisonParameters
from organon.idq.domain.settings.date_value_definition import DateValueDefinition
from organon.idq.domain.settings.dq_column_metadata import DqColumnMetadata
from organon.idq.domain.settings.full_process.dq_df_full_process_input import DqDfFullProcessInput
from organon.idq.domain.settings.full_process.dq_file_full_process_input import DqFileFullProcessInput
from organon.idq.domain.settings.input_source.dq_df_input_source_settings import DqDfInputSourceSettings
from organon.idq.domain.settings.input_source.dq_file_input_source_settings import DqFileInputSourceSettings
from organon.idq.domain.settings.partition_info import PartitionInfo
from organon.idq.services.user_settings.dq_user_input import DqUserInput
from organon.idq.services.user_settings.user_date_value_definition import UserDateValueDefinition
from organon.idq.services.user_settings.user_input_source_settings import UserInputSourceSettings
from organon.idq.services.user_settings.user_multi_partition_calculation_params import UserMultiPartitionCalcParams
from organon.idq.services.user_settings.user_partition_info import UserPartitionInfo


def get_partition_info_list() -> List[UserPartitionInfo]:
    """Returns partition info mock list"""
    partition_info = UserPartitionInfo()
    partition_info.column_name = "int_col"
    partition_info.column_values = [1, 2, 3]

    partition_info_2 = UserPartitionInfo()
    partition_info_2.column_name = "date_col"
    date_val_def = UserDateValueDefinition()
    date_val_def.year = 2010
    date_val_def.month = 11
    date_val_def.day = 3
    date_val_def.hour = 16
    partition_info_2.column_values = [None, date_val_def]

    return [partition_info, partition_info_2]


def get_user_input():
    """returns a mock DqUserInput instance"""
    inp = DqUserInput()
    inp.use_supplied_calcs_as_comp_inputs = True
    inp.minimum_cardinality = 10
    inp.traffic_light_threshold_yellow = 0.5
    inp.traffic_light_threshold_green = 0.2
    inp.psi_threshold_green = 0.2
    inp.psi_threshold_yellow = 0.7
    inp.z_score = 1.5

    inp_settings = UserInputSourceSettings()
    inp_settings.source = pd.DataFrame({"a": [1, 2, 3], "b": ["a", "b", "c"]})
    inp_settings.sampling_ratio = 0.7

    calc_params = UserMultiPartitionCalcParams("calc_1", inp_settings, None, None, None)
    calc_params.partitions = [get_partition_info_list()]
    inp.calc_params = calc_params
    inp.column_default_values = {"b": ["a", "b"]}

    return inp


class UserInputServiceTestCase(unittest.TestCase):
    """Unittest class for UserInputService"""

    def setUp(self) -> None:
        self.user_input = get_user_input()
        pd_read_csv_patcher = patch(base_user_input_service.__name__ + ".pd.read_csv")
        self.mock_pd_read_csv = pd_read_csv_patcher.start()
        self.addCleanup(pd_read_csv_patcher.stop)
        validation_service_patcher = patch(user_input_service.__name__ + ".UserInputValidationService")
        validation_service_patcher.start()
        self.addCleanup(validation_service_patcher.stop)
        partition_info_patcher = patch.object(PartitionInfo, 'to_str',
                                              MagicMock(
                                                  side_effect=["[int_col=(1,2,3)]",
                                                               "[date_col=(None,{y=2010,m=11,d=3,h=16})]",
                                                               "[int_col=(1,2,3)]",
                                                               "[date_col=(None,{y=2010,m=11,d=3,h=16})]",
                                                               "[int_col=(1,2,3)]",
                                                               "[date_col=(None,{y=2010,m=11,d=3,h=16})]"]))
        self.mock_partition_info = partition_info_patcher.start()
        self.addCleanup(partition_info_patcher.stop)

    def test_convert_to_full_process_input_calc_params(self):
        """tests conversion of calculation parameters"""
        service = UserInputService(self.user_input)
        full_proc_input = service.convert_to_full_process_input()
        self.assertIsInstance(full_proc_input, DqDfFullProcessInput)
        self.assertEqual(1, len(full_proc_input.calculation_parameters))
        calc_params = full_proc_input.calculation_parameters[0]
        self.assertIsInstance(calc_params, DqDfCalculationParameters)

        self.assertIsInstance(calc_params.input_source_settings, DqDfInputSourceSettings)
        self.assertEqual("Partition-[int_col=(1,2,3)] & [date_col=(None,{y=2010,m=11,d=3,h=16})]",
                         calc_params.calculation_name)
        self.assertTrue(calc_params.input_source_settings.is_sampling_on)
        self.assertEqual(0.7, calc_params.input_source_settings.sampling_ratio)
        self.assertEqual(sys.maxsize, calc_params.input_source_settings.max_num_of_samples)

        partition_info_list = calc_params.input_source_settings.partition_info_list
        self.assertEqual(2, len(partition_info_list))
        self.assertIsInstance(partition_info_list[0], PartitionInfo)
        self.assertCountEqual([1, 2, 3], partition_info_list[0].column_values)

        self.assertIsInstance(partition_info_list[1], PartitionInfo)
        self.assertIsNone(partition_info_list[1].column_values[0])
        self.assertIsInstance(partition_info_list[1].column_values[1], DateValueDefinition)
        self.assertEqual(2010, partition_info_list[1].column_values[1].year)
        self.assertEqual(11, partition_info_list[1].column_values[1].month)
        self.assertEqual(3, partition_info_list[1].column_values[1].day)
        self.assertEqual(16, partition_info_list[1].column_values[1].hour)

        self.assertEqual(1, len(calc_params.column_dq_metadata_list))
        self.assertIsInstance(calc_params.column_dq_metadata_list[0], DqColumnMetadata)
        self.assertEqual([ColumnNativeType.String, ColumnNativeType.Date], calc_params.nominal_column_types)

    def test_convert_to_full_process_input_comp_params(self):
        """tests conversion of comparison parameters"""
        service = UserInputService(self.user_input)
        full_proc_input = service.convert_to_full_process_input()
        self.assertIsInstance(full_proc_input, DqDfFullProcessInput)
        self.assertIsInstance(full_proc_input.comparison_parameters, DqDfComparisonParameters)
        self.assertEqual(10, full_proc_input.comparison_parameters.minimum_cardinality)
        self.assertEqual(0.2, full_proc_input.comparison_parameters.traffic_light_threshold_green)
        self.assertEqual(0.5, full_proc_input.comparison_parameters.traffic_light_threshold_yellow)
        self.assertEqual(0.2, full_proc_input.comparison_parameters.psi_threshold_green)
        self.assertEqual(0.7, full_proc_input.comparison_parameters.psi_threshold_yellow)
        self.assertEqual(1.5, full_proc_input.comparison_parameters.z_score)

    def test_convert_to_full_process_input_calc_name_multi_partition(self):
        """tests if calculation name is set properly when there are multiple partitions"""
        self.user_input.calc_params.partitions.append(get_partition_info_list())
        service = UserInputService(self.user_input)
        full_proc_input = service.convert_to_full_process_input()
        self.assertEqual(2, len(full_proc_input.calculation_parameters))
        self.assertEqual("Partition-[int_col=(1,2,3)] & [date_col=(None,{y=2010,m=11,d=3,h=16})]",
                         full_proc_input.calculation_parameters[0].calculation_name)
        self.assertEqual("Partition-[int_col=(1,2,3)] & [date_col=(None,{y=2010,m=11,d=3,h=16})]",
                         full_proc_input.calculation_parameters[1].calculation_name)

    def test_convert_to_full_process_input_calc_default_name_multi_partition(self):
        """tests if default calculation name is set when calculation name is not given and
        there are multiple partitions"""
        self.user_input.calc_params.calculation_name = None
        self.user_input.calc_params.partitions.append(get_partition_info_list())
        service = UserInputService(self.user_input)
        full_proc_input = service.convert_to_full_process_input()
        self.assertEqual(2, len(full_proc_input.calculation_parameters))
        self.assertEqual("Partition-[int_col=(1,2,3)] & [date_col=(None,{y=2010,m=11,d=3,h=16})]",
                         full_proc_input.calculation_parameters[0].calculation_name)
        self.assertEqual("Partition-[int_col=(1,2,3)] & [date_col=(None,{y=2010,m=11,d=3,h=16})]",
                         full_proc_input.calculation_parameters[1].calculation_name)

    def test_convert_to_full_process_input_file(self):
        """tests file specific calculation parameters"""

        self.user_input.calc_params.input_source_settings.source = "some file path"
        service = UserInputService(self.user_input)
        full_proc_input = service.convert_to_full_process_input()
        self.assertIsInstance(full_proc_input, DqFileFullProcessInput)
        calc_params = full_proc_input.calculation_parameters[0]
        self.assertIsInstance(calc_params, DqFileCalculationParameters)
        self.assertIsInstance(calc_params.input_source_settings, DqFileInputSourceSettings)
        self.assertIsInstance(full_proc_input.comparison_parameters, DqFileComparisonParameters)

    def test_init_with_invalid_source(self):
        """tests if ValueError is raised on initialization of UserInputService if source type is invalid"""
        self.user_input.calc_params.input_source_settings.source = 1
        with self.assertRaisesRegex(ValueError, "Invalid input source type"):
            UserInputService(self.user_input)

    def test_convert_to_full_process_input_default_sampling(self):
        """tests if default sampling values are set properly"""

        self.user_input.calc_params.input_source_settings.sampling_ratio = None
        full_proc_input = UserInputService(self.user_input).convert_to_full_process_input()
        self.assertEqual(1.0, full_proc_input.calculation_parameters[0].input_source_settings.sampling_ratio)
        self.assertEqual(False, full_proc_input.calculation_parameters[0].input_source_settings.is_sampling_on)

    def test_convert_to_full_process_input_default_col_meta_list(self):
        """tests if default column_metadata_list is set properly"""
        self.user_input.column_default_values = None
        self.user_input.excluded_column_names = None
        full_proc_input = UserInputService(self.user_input).convert_to_full_process_input()
        self.assertEqual([], full_proc_input.calculation_parameters[0].column_dq_metadata_list)

    def test_convert_to_full_process_input_default_test_groups(self):
        """tests if default test_groups are set properly"""
        self.user_input.excluded_test_groups = None
        self.user_input.test_group_benchmark_horizons = None
        full_proc_input = UserInputService(self.user_input).convert_to_full_process_input()
        test_groups = full_proc_input.comparison_parameters.test_groups
        self.assertEqual(7, len(test_groups))
        for test_group in test_groups:
            self.assertTrue(test_group.inclusion_flag)
            self.assertTrue(test_group.inclusion_flag)

    def test_convert_to_full_process_input_invalid_test_group(self):
        """tests if default test_groups are set properly"""
        self.user_input.excluded_test_groups = ["dummy"]
        with self.assertRaisesRegex(ValueError, "'dummy' is not a valid value"):
            UserInputService(self.user_input).convert_to_full_process_input()

    def test_convert_to_full_process_input_test_group_settings_override(self):
        """tests if default test_groups defaults are overridden properly"""
        self.user_input.excluded_test_groups = [DqTestGroupType.TRAFFIC_LIGHT_CONTROLS.name]
        self.user_input.test_group_benchmark_horizons = {DqTestGroupType.TABLE_SCHEMA_CONTROLS.name: 2}
        full_proc_input = UserInputService(self.user_input).convert_to_full_process_input()
        test_groups_dict = {group.group_type: group for group in full_proc_input.comparison_parameters.test_groups}
        self.assertEqual(7, len(test_groups_dict))
        self.assertEqual(2, test_groups_dict[DqTestGroupType.TABLE_SCHEMA_CONTROLS].test_bmh)
        self.assertFalse(test_groups_dict[DqTestGroupType.TRAFFIC_LIGHT_CONTROLS].inclusion_flag)

    def test_convert_to_full_process_input_included_columns(self):
        """Checks if input_source_settings.included_columns set properly"""
        self.user_input.included_column_names = ["a", "b"]
        full_proc_input = UserInputService(self.user_input).convert_to_full_process_input()
        for calc_param in full_proc_input.calculation_parameters:
            self.assertCountEqual(["a", "b"], calc_param.input_source_settings.included_columns)

    def test_convert_to_full_process_input_included_columns_when_excluded_given(self):
        """Checks if input_source_settings.included_columns set properly when excluded_column_names are given"""
        self.user_input.excluded_column_names = ["b"]
        full_proc_input = UserInputService(self.user_input).convert_to_full_process_input()
        for calc_param in full_proc_input.calculation_parameters:
            self.assertCountEqual(["a"], calc_param.input_source_settings.included_columns)

    def test_convert_to_full_process_input_included_columns_when_excluded_given_for_file(self):
        """Checks if input_source_settings.included_columns set properly when excluded_column_names are given(file)"""
        self.user_input.excluded_column_names = ["b"]
        self.user_input.calc_params.input_source_settings.source = "some file path"
        self.mock_pd_read_csv.return_value = pd.DataFrame({"a": [1, 2], "b": [1, 2]})
        full_proc_input = UserInputService(self.user_input).convert_to_full_process_input()
        for calc_param in full_proc_input.calculation_parameters:
            self.assertCountEqual(["a"], calc_param.input_source_settings.included_columns)

    def test_include_date_columns(self):
        """tests date columns exclusion"""
        self.user_input.include_date_columns = False
        full_proc_input = UserInputService(self.user_input).convert_to_full_process_input()
        for params in full_proc_input.calculation_parameters:
            self.assertEqual([ColumnNativeType.String], params.nominal_column_types)


if __name__ == '__main__':
    unittest.main()
