"""Includes unittests for DqStatisticsDomainService"""
import unittest
from unittest.mock import patch, MagicMock

import pandas as pd

from organon.fl.core.businessobjects.dataframe import DataFrame
from organon.fl.mathematics.helpers.dataframe_operations_factory import DataFrameOperationsFactory
from organon.idq.core.dq_constants import DqConstants
from organon.idq.domain.helpers.statistics.dq_statistical_functions import DqStatisticalFunctions
from organon.idq.domain.services.statistics import dq_statistics_domain_service
from organon.idq.domain.services.statistics.dq_statistics_domain_service import DqStatisticsDomainService

_STATS_DOM_SERVICE_MODULE = dq_statistics_domain_service.__name__


def _get_mock_pd_ops():
    ops = MagicMock()
    ops.get_mean_value.side_effect = lambda df, col, **kwargs: df.data_frame[col].mean()
    ops.get_min_value.side_effect = lambda df, col, **kwargs: df.data_frame[col].min()
    ops.get_sum_value.side_effect = lambda df, col, **kwargs: df.data_frame[col].sum()
    ops.get_max_value.side_effect = lambda df, col, **kwargs: df.data_frame[col].max()
    ops.get_variance_value.side_effect = lambda df, col, **kwargs: df.data_frame[col].var()
    return ops


class DqStatisticsDomainServiceTestCase(unittest.TestCase):
    """Test class for DqStatisticsDomainService"""

    def setUp(self) -> None:
        self.df_ops_patcher = patch(f"{_STATS_DOM_SERVICE_MODULE}.{DataFrameOperationsFactory.__name__}")
        self.dq_stat_funcs_patcher = patch(f"{_STATS_DOM_SERVICE_MODULE}.{DqStatisticalFunctions.__name__}")
        self.dq_stat_funcs_mock = self.dq_stat_funcs_patcher.start()
        self.mock_pd_ops = _get_mock_pd_ops()
        self.df_ops_patcher.start().get_dataframe_operations.return_value = self.mock_pd_ops
        self.data = DataFrame()
        self.data.data_frame = pd.DataFrame({"col1": [1, 2, 3, 0]})

    def tearDown(self) -> None:
        self.df_ops_patcher.stop()
        self.dq_stat_funcs_patcher.stop()

    def test_compute_numerical_statistics(self):
        """tests compute_numerical_statistics (when n > 1)"""
        self.mock_pd_ops.get_frequencies.return_value = {2.4: 3, 4.5: 5, float("nan"): 3}
        self.mock_pd_ops.get_size.return_value = 4
        self.mock_pd_ops.get_column_distinct_counts.return_value = {
            "col1": DqConstants.NUMERICAL_STATISTICS_MIN_CARDINALITY + 5}
        self.mock_pd_ops.get_col_df_without_values.return_value = self.data
        self.mock_pd_ops.get_percentile_values.return_value = {25: 0, 50: 2, 75: 3}
        self.mock_pd_ops.get_trimmed_mean_value.return_value = 1.7
        stats_dict = DqStatisticsDomainService.compute_numerical_statistics(self.data,
                                                                            {"col1": [2.4, 4.5, float("nan")]},
                                                                            tukey_parameter=2)

        stats = stats_dict["col1"]
        self.assertEqual(stats.mean, 1.5)
        self.assertEqual(stats.min, 0)
        self.assertEqual(stats.max, 3)
        self.assertEqual(stats.sum, 6)
        self.assertAlmostEqual(stats.variance, 5 / 3)
        self.assertEqual(2, stats.median)
        self.assertIsNotNone(stats.interval_statistics)
        self.assertEqual(1.7, stats.trimmed_mean)
        self.assertEqual(3, stats.inter_quartile_range)
        self.assertEqual(-6, stats.tukey_lower_limit)
        self.assertEqual(9, stats.tukey_upper_limit)

    def test_compute_numerical_statistics_all_missing(self):
        """compute_numerical_statistics - when all values are in missing values"""
        self.mock_pd_ops.get_frequencies.return_value = {2.4: 3, 4.5: 5, float("nan"): 3}
        self.mock_pd_ops.get_size.return_value = 0
        stats_dict = DqStatisticsDomainService.compute_numerical_statistics(self.data,
                                                                            {"col1": [2.4, 4.5, float("nan")]})
        stats = stats_dict["col1"]
        self.assertEqual(11, stats.n_miss)
        self.assertEqual(11, stats.n_val)
        self.assertEqual(0, stats.cardinality)

    def test_compute_numerical_statistics_frequencies_not_calculated(self):
        """compute_numerical_statistics - check if frequencies calculated when cardinality is high"""
        self.mock_pd_ops.get_frequencies.return_value = {2.4: 3, 4.5: 5, float("nan"): 3}
        self.mock_pd_ops.get_size.return_value = 10
        self.mock_pd_ops.get_percentile_values.return_value = {25: 0, 50: 2, 75: 3}
        self.mock_pd_ops.get_column_distinct_counts.return_value = {
            "col1": DqConstants.NUMERICAL_STATISTICS_MIN_CARDINALITY + 5}
        stats_dict = DqStatisticsDomainService.compute_numerical_statistics(self.data,
                                                                            {"col1": [2.4, 4.5, float("nan")]})
        stats = stats_dict["col1"]
        self.assertEqual(0, len(stats.frequencies))
        self.assertEqual(False, stats.compute_frequencies)

    def test_compute_numerical_statistics_when_n_1(self):
        """compute_numerical_statistics - check trimmed mean value when n=1"""
        self.mock_pd_ops.get_frequencies.return_value = {2.4: 3, 4.5: 5, float("nan"): 3}
        self.mock_pd_ops.get_size.return_value = 1
        self.mock_pd_ops.get_column_distinct_counts.return_value = {"col1": 4}
        self.mock_pd_ops.get_col_df_without_values.return_value = self.data
        self.mock_pd_ops.get_percentile_values.return_value = {25: 0, 50: 2, 75: 3}
        self.mock_pd_ops.get_trimmed_mean_value.return_value = 1.7
        stats_dict = DqStatisticsDomainService.compute_numerical_statistics(self.data,
                                                                            {"col1": [2.4, 4.5, float("nan")]})

        stats = stats_dict["col1"]
        self.assertEqual(1.5, stats.trimmed_mean)  # should equal mean
        self.assertIsNone(stats.interval_statistics)

    def test_compute_categorical_statistics_with_missing_value(self):
        """Test compute categorical stats with missing value"""

        data = DataFrame()
        data.data_frame = pd.DataFrame({"col1": ["a", "a", "a", "b", "b", "b", "b", "b", "c", "c", "c"]})

        self.mock_pd_ops.get_frequencies.side_effect = [{"a": 3, "b": 5}, {"a": 3, "b": 5, "c": 3}]
        self.mock_pd_ops.get_size.return_value = 11

        stats_dict = DqStatisticsDomainService.compute_categorical_statistics(self.data,
                                                                              {"col1": ["a", "b"]})
        self.assertEqual(3, stats_dict["col1"].cardinality)
        self.assertEqual(8, stats_dict["col1"].n_miss)
        self.assertEqual(11, stats_dict["col1"].n_val)
        self.assertEqual(True, stats_dict["col1"].frequencies == {"a": 3, "b": 5, "c": 3})

    def test_compute_categorical_statistics_without_missing_value(self):
        """Test compute categorical stats without missing value"""
        data = DataFrame()
        data.data_frame = pd.DataFrame({"col1": ["a", "a", "a", "b", "b", "b", "b", "b", "c", "c", "c"]})

        self.mock_pd_ops.get_frequencies.side_effect = [{}, {"a": 3, "b": 5, "c": 3}]

        self.mock_pd_ops.get_size.return_value = 11
        self.mock_pd_ops.get_column_distinct_counts.return_value = {"col1": 3}
        self.mock_pd_ops.get_col_df_without_values.return_value = self.data
        self.mock_pd_ops.get_percentile_values.return_value = {"a": 0.2, "b": 0.5, "c": 0.3}

        stats_dict = DqStatisticsDomainService.compute_categorical_statistics(data,
                                                                              {"col1": []})
        self.assertEqual(3, stats_dict["col1"].cardinality)
        self.assertEqual(0, stats_dict["col1"].n_miss)
        self.assertEqual(11, stats_dict["col1"].n_val)
        self.assertEqual(True, stats_dict["col1"].frequencies == {"a": 3, "b": 5, "c": 3})


if __name__ == '__main__':
    unittest.main()
