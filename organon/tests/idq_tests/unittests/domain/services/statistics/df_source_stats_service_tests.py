"""Includes tests for DqSourceStatsBaseService class."""
import unittest
from datetime import datetime
from typing import List
from unittest.mock import patch

from organon.fl.mathematics.helpers.pandas_dataframe_operations import PandasDataFrameOperations
from organon.idq.domain.businessobjects.record_sources.dq_df_record_source import DqDfRecordSource
from organon.idq.domain.services.statistics.source_statistics import df_source_stats_service
from organon.idq.domain.services.statistics.source_statistics.df_source_stats_service import DfSourceStatsService
from organon.idq.domain.services.statistics.source_statistics.dq_source_stats_base_service import \
    DqSourceStatsBaseService
from organon.idq.domain.settings.abstractions.dq_base_calculation_parameters import DqBaseCalculationParameters
from organon.idq.domain.settings.calculation.dq_df_calculation_parameters import DqDfCalculationParameters
from organon.idq.domain.settings.dq_column_metadata import DqColumnMetadata
from organon.idq.domain.settings.input_source.dq_df_input_source_settings import DqDfInputSourceSettings
from organon.tests.idq_tests.unittests.domain.services.statistics.dq_source_stats_base_test_case import \
    DqSourceStatsServiceBaseTestCase


class DfSourceStatsServiceTestCase(DqSourceStatsServiceBaseTestCase, unittest.TestCase):
    """Test class for DfSourceStatsService"""

    def setUp(self) -> None:
        super().setUp()
        df_ops_patcher = patch(df_source_stats_service.__name__ + "." + PandasDataFrameOperations.__name__,
                               name="pandasops")
        self.df_ops_mock = df_ops_patcher.start()
        self.addCleanup(df_ops_patcher.stop)

    def _get_service(self, calc_params) -> DqSourceStatsBaseService:
        return DfSourceStatsService(calc_params)

    def _get_calculation_params_for_base_tests(self,
                                               metadata_list: List[DqColumnMetadata]) -> DqBaseCalculationParameters:
        params = DqDfCalculationParameters()
        params.input_source_settings = DqDfInputSourceSettings(DqDfRecordSource(self.test_data, name="mydf"))
        params.column_dq_metadata_list = metadata_list
        return params

    def test_filter_high_cardinality_columns(self):
        """tests filter_high_cardinality_columns"""

        self.calc_params.max_nominal_cardinality_count = 3
        self.df_ops_mock.get_column_distinct_counts.return_value = {
            "col2": 10, "col3": 2, "col4": 3
        }
        filtered = self._get_service(self.calc_params).filter_high_cardinality_columns(self.col_collection)

        args = self.df_ops_mock.get_column_distinct_counts.call_args.args
        self.assertCountEqual(["col2", "col3", "col4"], args[1])
        self.assertNotIn("col2", filtered)
        self.assertIn("col3", filtered)
        self.assertIn("col4", filtered)

    def test_filter_high_cardinality_columns_with_included_columns(self):
        """tests filter_high_cardinality_columns when include_columns is not None"""
        self.calc_params.input_source_settings.included_columns = ["col2", "col3"]
        self.calc_params.max_nominal_cardinality_count = 3
        self.df_ops_mock.get_column_distinct_counts.return_value = {
            "col2": 10, "col3": 2
        }
        filtered = self._get_service(self.calc_params).filter_high_cardinality_columns(self.col_collection)

        args = self.df_ops_mock.get_column_distinct_counts.call_args.args
        self.assertCountEqual(["col2", "col3"], args[1])
        self.assertNotIn("col4", self.df_ops_mock.get_column_distinct_counts.call_args.args[1])
        self.assertNotIn("col2", filtered)
        self.assertIn("col3", filtered)
        self.assertNotIn("col4", filtered)

    def test_filter_high_cardinality_columns_no_nominal_columns(self):
        """tests filter_high_cardinality_columns when no nominal columns are in included_columns"""
        self.calc_params.input_source_settings.included_columns = ["col10", "col11"]
        self.calc_params.max_nominal_cardinality_count = 3
        self.df_ops_mock.get_column_distinct_counts.return_value = {
            "col2": 10, "col3": 2
        }
        filtered = self._get_service(self.calc_params).filter_high_cardinality_columns(self.col_collection)
        self.assertEqual([], filtered)

    def test_get_population_nominal_statistics(self):
        """tests if stats are returned correctly (get_population_nominal_statistics method)"""
        # pylint: disable=inconsistent-return-statements,unused-argument
        def _get_freq_side_effect(frame, col):
            if col == "col2":
                return {"a": 2, "b": 1, "c":1}
            if col == "col4":
                return {datetime(2022, 1, 1): 2, datetime(2022, 1, 2): 1, datetime(2022, 1, 3): 1, }
            self.fail("side effect should not have called with given column name")

        self.df_ops_mock.get_frequencies.side_effect = _get_freq_side_effect
        stats = self._get_service(self.calc_params).get_population_nominal_statistics(["col2", "col4"],
                                                                                      self.col_collection)
        self.assertEqual(2, len(stats))
        self.assertIn("col2", stats)
        self.assertIn("col4", stats)
        self.assertEqual(2, stats["col2"].frequencies["a"])
        self.assertEqual(1, stats["col2"].frequencies["b"])
        self.assertEqual(1, stats["col2"].frequencies["c"])
        self.assertEqual(2, stats["col4"].frequencies["20220101"])
        self.assertEqual(1, stats["col4"].frequencies["20220102"])
        self.assertEqual(1, stats["col4"].frequencies["20220103"])

    def test_get_population_nominal_statistics_not_included_columns(self):
        """population stats should not be calculated for columns with inclusion_flag==False"""
        stats = self._get_service(self.calc_params).get_population_nominal_statistics(["col2", "col3", "col4"],
                                                                                      self.col_collection)
        self.assertNotIn("col3", stats)


if __name__ == '__main__':
    unittest.main()
