"""Includes tests for DqSourceStatsBaseService class."""
import math
from datetime import datetime
from typing import List
from unittest.mock import patch, MagicMock

import pandas as pd

from organon.fl.core.enums.column_native_type import ColumnNativeType
from organon.fl.core.exceptionhandling.known_exception import KnownException
from organon.idq.domain.businessobjects.data_column.dq_data_column import DqDataColumn
from organon.idq.domain.businessobjects.dq_data_column_collection import DqDataColumnCollection
from organon.idq.domain.businessobjects.statistics.dq_categorical_statistics import DqCategoricalStatistics
from organon.idq.domain.services.statistics.dq_statistics_domain_service import DqStatisticsDomainService
from organon.idq.domain.services.statistics.source_statistics import dq_source_stats_base_service
from organon.idq.domain.services.statistics.source_statistics.dq_source_stats_base_service import \
    DqSourceStatsBaseService
from organon.idq.domain.settings.abstractions.dq_base_calculation_parameters import DqBaseCalculationParameters
from organon.idq.domain.settings.dq_column_metadata import DqColumnMetadata


def get_col_metadata(col_name, inclusion_flag, default_values=None):
    """generates DqColumnMetadata with given values"""
    meta = DqColumnMetadata()
    meta.column_name = col_name
    meta.inclusion_flag = inclusion_flag
    meta.default_values = default_values or []
    return meta


def get_data_col(col_name: str, native_type: ColumnNativeType):
    """generates DqDataColumn with given values"""
    col = DqDataColumn()
    col.column_name = col_name
    col.column_native_type = native_type
    return col


def _get_dq_col_collection():
    collection = DqDataColumnCollection()
    collection.add(get_data_col("col1", ColumnNativeType.Numeric))
    collection.add(get_data_col("col2", ColumnNativeType.String))
    collection.add(get_data_col("col3", ColumnNativeType.String))
    collection.add(get_data_col("col4", ColumnNativeType.Date))
    collection.add(get_data_col("col5", ColumnNativeType.Other))
    return collection


def categorical_stats_side_effect(missing_values):
    """side effect for mock DqCategoricalStatistics initialize"""
    stats = DqCategoricalStatistics(missing_values)

    def _add(key, freq):
        stats.frequencies[key] = freq

    stats.add = MagicMock(side_effect=_add)
    return stats


# pylint: disable=invalid-name,no-member
class DqSourceStatsServiceBaseTestCase:
    """Test class for DqSourceStatsBaseService"""

    def setUp(self) -> None:
        """setUp testcase"""
        self.test_data = pd.DataFrame({"col1": [1, 2, 2, 3],
                                       "col2": ["a", "a", "b", "c"],
                                       "col3": ["s", "s", "s", "f"],
                                       "col4": [datetime(2022, 1, 1), datetime(2022, 1, 1), datetime(2022, 1, 2),
                                                datetime(2022, 1, 3)],
                                       "col5": [b"asdfasd", b"adsf", b"sdfasd", b"asdfasd"]
                                       })
        self.calc_params = self._get_calculation_params_for_base_tests(
            [get_col_metadata("col1", True), get_col_metadata("col2", True),
             get_col_metadata("col3", False), get_col_metadata("col4", True),
             get_col_metadata("col5", True)])
        self.calc_params.nominal_column_types = [ColumnNativeType.String, ColumnNativeType.Date]

        self.sample_data = pd.DataFrame({"col1": [1, 2],
                                         "col2": ["a", "a"],
                                         "col3": ["s", "s"],
                                         "col4": [datetime(2022, 1, 1), datetime(2022, 1, 1)],
                                         "col5": [b"asdfasd", b"adsf"]
                                         })
        self.col_collection = _get_dq_col_collection()
        stats_dom_service_patcher = patch(
            dq_source_stats_base_service.__name__ + "." + DqStatisticsDomainService.__name__)
        self.stats_dom_service = stats_dom_service_patcher.start()
        self.addCleanup(stats_dom_service_patcher.stop)

    def _get_service(self, calc_params) -> DqSourceStatsBaseService:
        raise NotImplementedError

    def _get_calculation_params_for_base_tests(self, metadata_list: List[DqColumnMetadata]) \
            -> DqBaseCalculationParameters:
        raise NotImplementedError

    def test_get_sample_stats(self):
        """tests get_sample_stats with empty default values"""
        self._get_service(self.calc_params).get_sample_stats(self.sample_data, self.col_collection)
        args = self.stats_dom_service.compute_numerical_statistics.call_args.args
        default_dict = args[1]
        self.assertEqual(1, len(default_dict))  # only numeric column should be in the dict
        default_values = next(iter(default_dict.values()))
        self.assertEqual(1, len(default_values))
        self.assertTrue(math.isnan(default_values[0]))  # only nan value since default_values is given as empty

    def test_get_sample_stats_empty_frame_error(self):
        """tests get_sample_stats when sample_data is None"""
        with self.assertRaisesRegex(KnownException, ".* table is empty"):
            self._get_service(self.calc_params).get_sample_stats(None, self.col_collection)

    def test_get_sample_stats_column_not_in_data(self):
        """tests get_sample_stats, checks if exception is raised when column_dq_metadata_list includes a
        column which does not exist in data"""
        self.calc_params.column_dq_metadata_list.append(get_col_metadata("col7", ColumnNativeType.Numeric))
        with self.assertRaisesRegex(KnownException, ".* not in dataframe"):
            self._get_service(self.calc_params).get_sample_stats(self.sample_data, self.col_collection)

    def test_get_sample_stats_default_values(self):
        """get_sample_stats - missing_values generation"""
        self.calc_params.column_dq_metadata_list[0].default_values = ["1.6", "b", "nan"]
        self._get_service(self.calc_params).get_sample_stats(self.sample_data, self.col_collection)
        args = self.stats_dom_service.compute_numerical_statistics.call_args.args
        defaults_dict = args[1]
        self.assertEqual(1, len(defaults_dict))
        self.assertIn("col1", defaults_dict)
        col1_defaults = defaults_dict["col1"]
        self.assertEqual(2, len(col1_defaults))
        self.assertIn(1.6, col1_defaults)
        self.assertEqual(1, sum(1 for x in col1_defaults if math.isnan(x)))

    def test_get_sample_stats_default_values_nan_added(self):
        """get_sample_stats - checks if nan is added to default_values even if not given"""
        self.calc_params.column_dq_metadata_list[0].default_values = ["1.6"]
        self._get_service(self.calc_params).get_sample_stats(self.sample_data, self.col_collection)
        args = self.stats_dom_service.compute_numerical_statistics.call_args.args
        defaults_dict = args[1]
        self.assertEqual(1, len(defaults_dict))
        self.assertIn("col1", defaults_dict)
        col1_defaults = defaults_dict["col1"]
        self.assertEqual(2, len(col1_defaults))
        self.assertIn(1.6, col1_defaults)
        self.assertEqual(1, sum(1 for x in col1_defaults if math.isnan(x)))
