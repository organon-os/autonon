"""Includes tests for DqStatisticalFunctions"""
import unittest

import numpy as np
import pandas as pd
from numpy.testing import assert_almost_equal

from organon.fl.core.businessobjects.dataframe import DataFrame
from organon.fl.core.collections.sorted_dict import SortedDict
from organon.fl.mathematics.helpers.pandas_dataframe_operations import PandasDataFrameOperations
from organon.idq.domain.helpers.statistics.dq_statistical_functions import DqStatisticalFunctions


class DqStatisticalFunctionsTestCase(unittest.TestCase):
    """Test class for DqStatisticalFunctions."""

    def setUp(self) -> None:
        self.dq_statistical_functions = DqStatisticalFunctions()

    def test_get_percentages_from_frequencies_for_sorted(self):
        """tests get_percentages_from_frequencies_for_sorted method"""
        _dict = SortedDict({"a": 1, "b": 2, "c": 3})
        val = DqStatisticalFunctions.get_percentages_from_frequencies_for_sorted(_dict)
        self.assertIsInstance(val, SortedDict)
        self.assertEqual(1 / 6, val["a"])
        self.assertEqual(2 / 6, val["b"])
        self.assertEqual(3 / 6, val["c"])

    def test_get_percentages_from_frequencies(self):
        """tests get_percentages_from_frequencies method"""
        _dict = {"a": 1, "b": 2, "c": 3}
        val = DqStatisticalFunctions.get_percentages_from_frequencies(_dict)
        self.assertEqual(1 / 6, val["a"])
        self.assertEqual(2 / 6, val["b"])
        self.assertEqual(3 / 6, val["c"])

    def test_get_balanced_intervals_and_statistics(self):
        """Test for balanced intervals and stats"""
        np.random.seed(1234)
        values = np.sort(np.random.uniform(low=10, high=35.3, size=(50,)))
        values = np.sort(values)
        data_frame = DataFrame()
        data_frame.data_frame = pd.DataFrame({"col": values})
        frequencies = SortedDict(PandasDataFrameOperations.get_frequencies(data_frame, "col"))

        intervals = self.dq_statistical_functions.get_balanced_intervals_and_statistics(frequencies, 10, 5)
        self.assertEqual(len(intervals), 10)
        for i in range(10):
            self.assertEqual(intervals[intervals.keys()[i]].sum_, np.sum(values[i * 5:(i + 1) * 5]))
            self.assertEqual(intervals[intervals.keys()[i]].sum_of_squares,
                             np.sum(values[i * 5:(i + 1) * 5] * values[i * 5:(i + 1) * 5]))
            self.assertEqual(intervals[intervals.keys()[i]].average, np.average(values[i * 5:(i + 1) * 5]))
            assert_almost_equal(intervals[intervals.keys()[i]].std_dev, np.std(values[i * 5:(i + 1) * 5]))

    def test_get_balanced_intervals_and_statistics_big_min_bucket_size(self):
        """Test for balanced intervals and stats"""
        np.random.seed(1234)
        values = np.sort(np.random.uniform(low=10, high=35.3, size=(50,)))
        data_frame = DataFrame()
        data_frame.data_frame = pd.DataFrame({"col": values})
        frequencies = SortedDict(PandasDataFrameOperations.get_frequencies(data_frame, "col"))

        intervals = self.dq_statistical_functions.get_balanced_intervals_and_statistics(frequencies, 10, 6)
        self.assertEqual(len(intervals), 9)
        for i in range(8):
            self.assertEqual(intervals[intervals.keys()[i]].sum_, np.sum(values[i * 5:(i + 1) * 5]))
            self.assertEqual(intervals[intervals.keys()[i]].sum_of_squares,
                             np.sum(values[i * 5:(i + 1) * 5] * values[i * 5:(i + 1) * 5]))
            self.assertEqual(intervals[intervals.keys()[i]].average, np.average(values[i * 5:(i + 1) * 5]))
            assert_almost_equal(intervals[intervals.keys()[i]].std_dev, np.std(values[i * 5:(i + 1) * 5]))
        expected_sum_of_squares = np.sum(values[40:45] * values[40:45] + values[45:50] * values[45:50])
        expected_average = np.sum(values[40:45] + (values[45:50])) / 10
        expected_std = np.sqrt(expected_sum_of_squares / 10 - expected_average * expected_average)
        self.assertEqual(intervals[intervals.keys()[8]].sum_, np.sum(values[40:45] + (values[45:50])))
        self.assertEqual(intervals[intervals.keys()[8]].sum_of_squares, expected_sum_of_squares)
        self.assertEqual(intervals[intervals.keys()[8]].average, expected_average)
        assert_almost_equal(intervals[intervals.keys()[8]].std_dev, expected_std)

    def test_get_balanced_intervals_and_statistic_with_null_values(self):
        """Test for balanced intervals and stats with empty values"""
        with self.assertRaises(Exception):
            self.dq_statistical_functions.get_balanced_intervals_and_statistics(SortedDict(), 10, 5)

    def test_get_balanced_intervals_and_statistic_with_small_number_of_intervals(self):
        """Test for balanced intervals and stats for max_num_of_intervals <= 1"""
        with self.assertRaises(Exception):
            self.dq_statistical_functions.get_balanced_intervals_and_statistics(SortedDict(), 1, 5)

    def test_get_balanced_intervals_and_statistic_with_small_size_values(self):
        """Test for balanced intervals and stats for values of length smaller than max_num_of_intervals"""
        freqs = SortedDict({1: 1, 2: 2})
        with self.assertRaises(Exception):
            self.dq_statistical_functions.get_balanced_intervals_and_statistics(freqs, 1, 5)

    def test_population_stability_index(self):
        """Test for psi key-value """
        left_dict = {"a": 5, "b": 10, "c": 2, "f": 50}
        right_dict = {"a": 3, "b": 15, "d": 3, "e": 30}
        psi_tuple = self.dq_statistical_functions.population_stability_index(left_dict, right_dict, 5)
        self.assertEqual(psi_tuple[0], "f")
        self.assertEqual(round(psi_tuple[1], 5), 2.18574)


if __name__ == '__main__':
    unittest.main()
