"""This module includes tests for EncryptionHelper"""
import unittest

from organon.fl.security.helpers.encryption_helper import EncryptionHelper


class EncryptionHelperTests(unittest.TestCase):
    """Test class for EncryptionHelper"""

    def test_encrypt_decrypt(self):
        """
        Test decrypt
        """
        password = "8organon+"
        secret_key = "abcdef"
        encrypted = EncryptionHelper.encrypt_string(password, secret_key)
        decrypted = EncryptionHelper.decrypt_string(encrypted, secret_key)
        self.assertEqual(decrypted, password)


if __name__ == '__main__':
    unittest.main()
