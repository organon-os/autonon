"""
This module keeps FlInitializer class tests
"""
import unittest

from organon.fl.core.businessobjects.fl_core_static_objects import FlCoreStaticObjects
from organon.fl.generic.interaction.fl_initializer import FlInitializer


class FlInitializerTests(unittest.TestCase):
    """
    Test class for FlInitializer
    """

    def test_initialize(self):
        """
        Test of application_initialize function in the FlInitializer class
        :return: nothing
        """
        FlCoreStaticObjects.application_initialize_called = False
        FlInitializer.application_initialize()
        self.assertEqual(FlCoreStaticObjects.application_initialize_called, True)


if __name__ == '__main__':
    unittest.main()
