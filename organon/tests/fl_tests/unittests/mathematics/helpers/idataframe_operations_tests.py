"""This module includes IDataFrameOperationsTestCase class"""
import unittest
from unittest.mock import MagicMock

from organon.fl.core.businessobjects.idataframe import IDataFrame
from organon.fl.mathematics.helpers.idataframe_operations import IDataFrameOperations


class IDataFrameOperationsTestCase(unittest.TestCase):
    """Class for IDataFrameOperations tests"""

    def setUp(self) -> None:
        self.mock_idf = MagicMock(spec=IDataFrame)
        self.mock_idf_operations = MagicMock(spec=IDataFrameOperations)
        self.mock_idf_operations.join.side_effect = NotImplementedError
        self.mock_idf_operations.get_column_stability.side_effect = NotImplementedError
        self.mock_idf_operations.get_column_distinct_counts.side_effect = NotImplementedError

    def test_join(self):
        """tests join method"""
        with self.assertRaises(NotImplementedError):
            self.mock_idf_operations.join(self.mock_idf, self.mock_idf)

    def test_get_column_stability(self):
        """tests get_column_stability method"""
        with self.assertRaises(NotImplementedError):
            self.mock_idf_operations.get_column_stability(self.mock_idf)

    def test_get_column_distinct_counts(self):
        """tests get_column_distinct_counts method"""
        with self.assertRaises(NotImplementedError):
            self.mock_idf_operations.get_column_distinct_counts(self.mock_idf)
