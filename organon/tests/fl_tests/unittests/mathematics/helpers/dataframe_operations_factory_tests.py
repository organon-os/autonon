"""This module includes DataFrameOperationsFactoryTestCase class"""
import unittest
from unittest.mock import MagicMock

from organon.fl.core.businessobjects.dataframe import DataFrame
from organon.fl.core.businessobjects.dict_dataframe import DictDataFrame
from organon.fl.core.businessobjects.idataframe import IDataFrame
from organon.fl.mathematics.helpers.dataframe_operations_factory import DataFrameOperationsFactory
from organon.fl.mathematics.helpers.dict_dataframe_operations import DictDataFrameOperations
from organon.fl.mathematics.helpers.pandas_dataframe_operations import PandasDataFrameOperations


class DataFrameOperationsFactoryTestCase(unittest.TestCase):
    """Class for DataFrameOperationsFactory tests"""

    def test_get_dataframe_operations(self):
        """tests get_dataframe_operations method works as expected."""
        mock_idf = MagicMock(spec=IDataFrame)
        df1 = DataFrame()
        self.assertEqual(PandasDataFrameOperations, type(DataFrameOperationsFactory.get_dataframe_operations(df1)))
        df2 = DictDataFrame()
        self.assertEqual(DictDataFrameOperations, type(DataFrameOperationsFactory.get_dataframe_operations(df2)))
        with self.assertRaises(NotImplementedError):
            DataFrameOperationsFactory().get_dataframe_operations(mock_idf)
