"""This module includes PandasDataFrameOperationsTestCase class."""
import math
import unittest

import numpy as np
import pandas as pd

from organon.fl.core.businessobjects.dataframe import DataFrame
from organon.fl.mathematics.helpers.pandas_dataframe_operations import PandasDataFrameOperations


class PandasDataFrameOperationsTestCase(unittest.TestCase):
    """Class for PandasDataFrameOperations tests"""

    def test_join(self):
        """tests join method"""
        df1 = DataFrame()
        df1.data_frame = pd.DataFrame({"a": [1, 2, 3]})
        df2 = DataFrame()
        df2.data_frame = pd.DataFrame({"b": [4, 5, 6]})
        joined_df = DataFrame()
        joined_df.data_frame = pd.DataFrame({"a": [1, 2, 3], "b": [4, 5, 6]})
        self.assertTrue(joined_df.data_frame.equals(PandasDataFrameOperations.join(df1, df2).data_frame))

    def test_get_column_stability(self):
        """tests get_column_stability method"""
        df_obj = DataFrame()
        df_obj.data_frame = pd.DataFrame({"a": [2, 2, 2], "b": [3, 5, 10], "c": [0, 0, 0]})
        unique_indexes = np.where(PandasDataFrameOperations.get_column_stability(df_obj))[0].tolist()
        self.assertEqual([0, 2], unique_indexes)

    def test_get_column_distinct_counts(self):
        """tests get_column_distinct_counts method"""
        df_obj = DataFrame()
        df_obj.data_frame = pd.DataFrame({"a": [2, 2, 2], "b": [3, 5, float("nan")], "c": [0, 0, 0]})
        distinct_counts = PandasDataFrameOperations.get_column_distinct_counts(df_obj)
        self.assertEqual({"a": 1, "b": 3, "c": 1}, distinct_counts)

    def test_get_min_value(self):
        """tests get_min_value"""
        df_obj = DataFrame()
        df_obj.data_frame = pd.DataFrame({"a": [2, 2, 2], "b": [3, 5, float("nan")], "c": [0, 0, 0]})
        min_val = PandasDataFrameOperations.get_min_value(df_obj, "b")
        self.assertEqual(3, min_val)

    def test_get_min_value_not_skip_na(self):
        """tests get_min_value with skipna=False"""
        df_obj = DataFrame()
        df_obj.data_frame = pd.DataFrame({"a": [2, 2, 2], "b": [3, 5, float("nan")], "c": [0, 0, 0]})
        min_val = PandasDataFrameOperations.get_min_value(df_obj, "b", skipna=False)
        self.assertTrue(math.isnan(min_val))

    def test_get_max_value(self):
        """tests get_max_value"""
        df_obj = DataFrame()
        df_obj.data_frame = pd.DataFrame({"a": [2, 2, 2], "b": [3, 5, float("nan")], "c": [0, 0, 0]})
        max_val = PandasDataFrameOperations.get_max_value(df_obj, "b")
        self.assertEqual(5, max_val)

    def test_get_max_value_not_skip_na(self):
        """tests get_max_value with skipna=False"""
        df_obj = DataFrame()
        df_obj.data_frame = pd.DataFrame({"a": [2, 2, 2], "b": [3, 5, float("nan")], "c": [0, 0, 0]})
        max_val = PandasDataFrameOperations.get_max_value(df_obj, "b", skipna=False)
        self.assertTrue(math.isnan(max_val))

    def test_get_mean_value(self):
        """tests get_mean_value"""
        df_obj = DataFrame()
        df_obj.data_frame = pd.DataFrame({"a": [2, 2, 2], "b": [3, 5, float("nan")], "c": [0, 0, 0]})
        mean_val = PandasDataFrameOperations.get_mean_value(df_obj, "b")
        self.assertEqual(4, mean_val)

    def test_get_mean_value_not_skipna(self):
        """tests get_mean_value with skipna=False"""
        df_obj = DataFrame()
        df_obj.data_frame = pd.DataFrame({"a": [2, 2, 2], "b": [3, 5, float("nan")], "c": [0, 0, 0]})
        mean_val = PandasDataFrameOperations.get_mean_value(df_obj, "b", skipna=False)
        self.assertTrue(math.isnan(mean_val))

    def test_get_variance_value(self):
        """tests get_mean_value"""
        df_obj = DataFrame()
        df_obj.data_frame = pd.DataFrame({"a": [2, 2, 2], "b": [3, 5, float("nan")], "c": [0, 0, 0]})
        variance = PandasDataFrameOperations.get_variance_value(df_obj, "b")
        self.assertEqual(2, variance)

    def test_get_variance_value_not_skipna(self):
        """tests get_variance_value with skipna=False"""
        df_obj = DataFrame()
        df_obj.data_frame = pd.DataFrame({"a": [2, 2, 2], "b": [3, 5, float("nan")], "c": [0, 0, 0]})
        variance = PandasDataFrameOperations.get_variance_value(df_obj, "b", skipna=False)
        self.assertTrue(math.isnan(variance))

    def test_get_sum_value(self):
        """tests get_sum_value"""
        df_obj = DataFrame()
        df_obj.data_frame = pd.DataFrame({"a": [2, 2, 2], "b": [3, 5, float("nan")], "c": [0, 0, 0]})
        _sum = PandasDataFrameOperations.get_sum_value(df_obj, "b")
        self.assertEqual(8, _sum)

    def test_get_sum_value_not_skipna(self):
        """tests get_sum_value with skipna=False"""
        df_obj = DataFrame()
        df_obj.data_frame = pd.DataFrame({"a": [2, 2, 2], "b": [3, 5, float("nan")], "c": [0, 0, 0]})
        _sum = PandasDataFrameOperations.get_sum_value(df_obj, "b", skipna=False)
        self.assertTrue(math.isnan(_sum))

    def test_get_percentile_values(self):
        """tests get_percentile_values"""
        df_obj = DataFrame()
        df_obj.data_frame = pd.DataFrame({"b": [0, 4, 1, 3, 2, 5, 6, float("nan")]})
        perc_dict = PandasDataFrameOperations.get_percentile_values(df_obj, "b", [0, 25, 50, 75, 100])
        self.assertEqual({0: 0.0, 25: 1.0, 50: 3.0, 75: 4.0, 100: 6.0}, perc_dict)

    def test_get_col_df_without_values(self):
        """tests get_col_df_without_values"""
        df_obj = DataFrame()
        df_obj.data_frame = pd.DataFrame({"a": [2, 2, 2], "b": [3, 5, float("nan")], "c": [0, 0, 0]})
        col = PandasDataFrameOperations.get_col_df_without_values(df_obj, "b", [3])
        self.assertIsInstance(col, DataFrame)
        self.assertEqual(2, len(col.data_frame))
        self.assertEqual(5, col.data_frame["b"].iloc[0])
        self.assertTrue(math.isnan(col.data_frame["b"].iloc[1]))

    def test_get_frequencies(self):
        """tests get_frequencies"""
        df_obj = DataFrame()
        df_obj.data_frame = pd.DataFrame({"b": [3, 5, 5, float("nan"), 3, 3, float("nan")]})
        freqs = PandasDataFrameOperations.get_frequencies(df_obj, "b")
        self.assertEqual(3, len(freqs))
        self.assertCountEqual([3, 5, None], freqs)
        self.assertEqual(2, freqs[None])
        self.assertEqual(3, freqs[3])
        self.assertEqual(2, freqs[5])

    def test_get_frequencies_specific_values(self):
        """tests get_frequencies with values parameter given"""
        df_obj = DataFrame()
        df_obj.data_frame = pd.DataFrame({"b": [3, 5, 5, float("nan"), 3, 3, float("nan")]})
        freqs = PandasDataFrameOperations.get_frequencies(df_obj, "b", values=[5, float("nan")])
        self.assertEqual(2, len(freqs))
        self.assertIn(5, freqs)
        self.assertIn(None, freqs)
        self.assertEqual(2, freqs[5])
        self.assertEqual(2, freqs[None])

    def test_get_col_values(self):
        """tests get_col_values"""
        df_obj = DataFrame()
        df_obj.data_frame = pd.DataFrame({"a": [2, 2, 2], "b": [3, 5, float("nan")], "c": [0, 0, 0]})
        col = PandasDataFrameOperations.get_col_values(df_obj, "b")
        self.assertEqual(3, col[0])
        self.assertEqual(5, col[1])
        self.assertTrue(math.isnan(col[2]))

    def test_get_size(self):
        """tests get_size"""
        df_obj = DataFrame()
        df_obj.data_frame = pd.DataFrame({"a": [2, 2, 2], "b": [3, 5, float("nan")], "c": [0, 0, 0]})
        self.assertEqual(3, PandasDataFrameOperations.get_size(df_obj))

    def test_get_trimmed_mean_value(self):
        """tests get_trimmed_mean_value"""
        df_obj = DataFrame()
        df_obj.data_frame = pd.DataFrame({"a": [3, 7, -1, 11, 3, 5, 6, 13]})
        res = PandasDataFrameOperations.get_trimmed_mean_value(df_obj, "a", -6, 12)
        df_obj2 = DataFrame()
        df_obj2.data_frame = pd.DataFrame({"a": [-8, 3, 7, -1, 11, 3, 5, 6, 13]})
        res2 = PandasDataFrameOperations.get_trimmed_mean_value(df_obj2, "a", -6, 12)
        self.assertEqual(round(res, 2), 4.86)
        self.assertEqual(round(res2, 2), 4.86)
