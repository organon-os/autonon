"""Includes tests for ScipyBlasService"""
import unittest

import numpy as np

from organon.fl.mathematics.linearalgebra.scipy_blas_service import ScipyBlasService


# pylint: disable=no-self-use,invalid-name

class ScipyBlasServiceTestCase(unittest.TestCase):
    """Test class for ScipyBlasService"""

    def test_saxpy(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.single)
        alpha = np.single(1.0)
        y = np.array([5, 4, 3, 2, 1], dtype=np.single)
        n = 3
        incx = 1
        incy = 1
        expected = np.array([6, 6, 6, 2, 1], dtype=np.single)

        blas = ScipyBlasService()
        blas.saxpy(n, alpha, x, incx, y, incy)
        np.testing.assert_array_equal(y, expected)

    def test_saxpy2(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.single)
        alpha = np.single(1.0)
        y = np.array([5, 4, 3, 2, 1], dtype=np.single)
        n = 3
        incx = 1
        incy = 1
        expected = np.array([5, 6, 6, 6, 1], dtype=np.single)

        blas = ScipyBlasService()
        blas.saxpy(n, alpha, x[1:], incx, y[1:], incy)
        np.testing.assert_array_equal(y, expected)

    def test_daxpy(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.double)
        alpha = np.single(1.0)
        y = np.array([5, 4, 3, 2, 1], dtype=np.double)
        n = 3
        incx = 1
        incy = 1
        expected = np.array([6, 6, 6, 2, 1], dtype=np.double)

        ScipyBlasService.daxpy(n, alpha, x, incx, y, incy)
        np.testing.assert_array_equal(y, expected)

    def test_daxpy2(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.double)
        alpha = np.single(1.0)
        y = np.array([5, 4, 3, 2, 1], dtype=np.double)
        n = 3
        incx = 1
        incy = 1
        expected = np.array([5, 6, 6, 6, 1], dtype=np.double)

        ScipyBlasService.daxpy(n, alpha, x[1:], incx, y[1:], incy)
        np.testing.assert_array_equal(y, expected)

    def test_saxpby(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.single)
        alpha = np.single(1.0)
        beta = np.single(2.0)
        y = np.array([5, 4, 3, 2, 1], dtype=np.single)
        n = 3
        incx = 1
        incy = 1
        expected = np.array([11, 10, 9, 2, 1], dtype=np.single)

        blas = ScipyBlasService()
        blas.saxpby(n, alpha, x, incx, beta, y, incy)
        np.testing.assert_array_equal(y, expected)

    def test_saxpby2(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.single)
        alpha = np.single(1.0)
        beta = np.single(2.0)
        y = np.array([5, 4, 3, 2, 1], dtype=np.single)
        n = 3
        incx = 1
        incy = 1
        expected = np.array([5, 10, 9, 8, 1], dtype=np.single)

        blas = ScipyBlasService()
        blas.saxpby(n, alpha, x[1:], incx, beta, y[1:], incy)
        np.testing.assert_array_equal(y, expected)

    def test_daxpby(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.double)
        alpha = np.single(1.0)
        beta = np.single(2.0)
        y = np.array([5, 4, 3, 2, 1], dtype=np.double)
        n = 3
        incx = 1
        incy = 1
        expected = np.array([11, 10, 9, 2, 1], dtype=np.double)

        blas = ScipyBlasService()
        blas.daxpby(n, alpha, x, incx, beta, y, incy)
        np.testing.assert_array_equal(y, expected)

    def test_daxpby2(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.double)
        alpha = np.single(1.0)
        beta = np.single(2.0)
        y = np.array([5, 4, 3, 2, 1], dtype=np.double)
        n = 3
        incx = 1
        incy = 1
        expected = np.array([5, 10, 9, 8, 1], dtype=np.double)

        blas = ScipyBlasService()
        blas.daxpby(n, alpha, x[1:], incx, beta, y[1:], incy)
        np.testing.assert_array_equal(y, expected)

    def test_scopy(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.single)
        y = np.array([5, 4, 3, 2, 1], dtype=np.single)
        n = 3
        incx = 1
        incy = 1
        expected = np.array([1, 2, 3, 2, 1], dtype=np.single)

        ScipyBlasService.scopy(n, x, incx, y, incy)
        np.testing.assert_array_equal(y, expected)

    def test_scopy2(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.single)
        y = np.array([5, 4, 3, 2, 1], dtype=np.single)
        n = 2
        incx = 2
        incy = 2
        expected = np.array([5, 2, 3, 4, 1], dtype=np.single)

        ScipyBlasService.scopy(n, x[1:], incx, y[1:], incy)
        np.testing.assert_array_equal(y, expected)

    def test_dcopy(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.double)
        y = np.array([5, 4, 3, 2, 1], dtype=np.double)
        n = 3
        incx = 1
        incy = 1
        expected = np.array([1, 2, 3, 2, 1], dtype=np.double)

        ScipyBlasService.dcopy(n, x, incx, y, incy)
        np.testing.assert_array_equal(y, expected)

    def test_dcopy2(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.double)
        y = np.array([5, 4, 3, 2, 1], dtype=np.double)
        n = 2
        incx = 2
        incy = 2
        expected = np.array([5, 2, 3, 4, 1], dtype=np.double)

        ScipyBlasService.dcopy(n, x[1:], incx, y[1:], incy)
        np.testing.assert_array_equal(y, expected)

    def test_scopynan(self):
        """
        Test description
        """
        x = np.array([1, np.NAN, 3, 4, 5], dtype=np.single)
        y = np.array([5, 4, 3, 2, 1], dtype=np.single)
        n = 3
        incx = 1
        incy = 1
        expected = np.array([1, 3, 4, 2, 1], dtype=np.single)

        ScipyBlasService.scopynan(n, x, incx, y, incy)
        np.testing.assert_array_equal(y, expected)

    def test_scopynan2(self):
        """
        Test description
        """
        x = np.array([np.NAN, np.NAN, np.NAN, 1, 5], dtype=np.single)
        y = np.array([5, 4, 3, 2, 1], dtype=np.single)
        n = 1
        incx = 2
        incy = 2
        expected = np.array([5, 1, 3, 2, 1], dtype=np.single)

        ScipyBlasService.scopynan(n, x[1:], incx, y[1:], incy)
        np.testing.assert_array_equal(y, expected)

    def test_dcopynan(self):
        """
        Test description
        """
        x = np.array([1, np.NAN, 3, 4, 5], dtype=np.double)
        y = np.array([5, 4, 3, 2, 1], dtype=np.double)
        n = 3
        incx = 1
        incy = 1
        expected = np.array([1, 3, 4, 2, 1], dtype=np.double)

        ScipyBlasService.dcopynan(n, x, incx, y, incy)
        np.testing.assert_array_equal(y, expected)

    def test_dcopynan2(self):
        """
        Test description
        """
        x = np.array([np.NAN, np.NAN, np.NAN, 1, 5], dtype=np.double)
        y = np.array([5, 4, 3, 2, 1], dtype=np.double)
        n = 1
        incx = 2
        incy = 2
        expected = np.array([5, 1, 3, 2, 1], dtype=np.double)

        ScipyBlasService.dcopynan(n, x[1:], incx, y[1:], incy)
        np.testing.assert_array_equal(y, expected)

    def test_sdot(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.single)
        y = np.array([5, 4, 3, 2, 1], dtype=np.single)
        n = 3
        incx = 1
        incy = 1
        expected = 22.0

        res = ScipyBlasService.sdot(n, x, incx, y, incy)
        self.assertAlmostEqual(res, expected)

    def test_sdot2(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.single)
        y = np.array([5, 4, 3, 2, 1], dtype=np.single)
        n = 3
        incx = 2
        incy = 2
        expected = 19.0

        res = ScipyBlasService.sdot(n, x, incx, y, incy)
        self.assertAlmostEqual(res, expected)

    def test_ddot(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.double)
        y = np.array([5, 4, 3, 2, 1], dtype=np.double)
        n = 3
        incx = 1
        incy = 1
        expected = 22.0

        res = ScipyBlasService.ddot(n, x, incx, y, incy)
        self.assertAlmostEqual(res, expected)

    def test_ddot2(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.double)
        y = np.array([5, 4, 3, 2, 1], dtype=np.double)
        n = 3
        incx = 2
        incy = 2
        expected = 19.0

        res = ScipyBlasService.ddot(n, x, incx, y, incy)
        self.assertAlmostEqual(res, expected)

    def test_snrm2(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 1, 1], dtype=np.single)
        n = 5
        incx = 1
        expected = 4.0

        res = ScipyBlasService.snrm2(n, x, incx)
        self.assertAlmostEqual(res, expected)

    def test_snrm22(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.single)
        n = 3
        incx = 2
        expected = np.linalg.norm(x[::incx])

        res = ScipyBlasService.snrm2(n, x, incx)
        self.assertAlmostEqual(res, expected)

    def test_dnrm2(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 1, 1], dtype=np.double)
        n = 5
        incx = 1
        expected = 4.0

        res = ScipyBlasService.dnrm2(n, x, incx)
        self.assertAlmostEqual(res, expected)

    def test_dnrm22(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.double)
        n = 3
        incx = 2
        expected = np.linalg.norm(x[::incx])

        res = ScipyBlasService.dnrm2(n, x, incx)
        self.assertAlmostEqual(res, expected)

    def test_sscal(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.single)
        alpha = np.double(2.0)
        n = 3
        incx = 1
        expected = np.array([2, 4, 6, 4, 5], dtype=np.single)

        blas = ScipyBlasService()
        blas.sscal(n, alpha, x, incx)
        np.testing.assert_array_equal(x, expected)

    def test_sscal2(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.single)
        alpha = np.double(3.0)
        n = 3
        incx = 1
        expected = np.array([1, 6, 9, 12, 5], dtype=np.single)

        blas = ScipyBlasService()
        blas.sscal(n, alpha, x[1:], incx)
        np.testing.assert_array_equal(x, expected)

    def test_dscal(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.double)
        alpha = np.double(2.0)
        n = 3
        incx = 1
        expected = np.array([2, 4, 6, 4, 5], dtype=np.double)

        blas = ScipyBlasService()
        blas.dscal(n, alpha, x, incx)
        np.testing.assert_array_equal(x, expected)

    def test_dscal2(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.double)
        alpha = np.double(3.0)
        n = 3
        incx = 1
        expected = np.array([1, 6, 9, 12, 5], dtype=np.double)

        blas = ScipyBlasService()
        blas.dscal(n, alpha, x[1:], incx)
        np.testing.assert_array_equal(x, expected)

    def test_sswap(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.single)
        y = np.array([5, 4, 3, 2, 1], dtype=np.single)
        n = 3
        incx = 1
        incy = 1

        expected_x = np.array([5, 4, 3, 4, 5], dtype=np.single)
        expected_y = np.array([1, 2, 3, 2, 1], dtype=np.single)

        ScipyBlasService.sswap(n, x, incx, y, incy)
        np.testing.assert_array_equal(x, expected_x)
        np.testing.assert_array_equal(y, expected_y)

    def test_sswap2(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.single)
        y = np.array([5, 4, 3, 2, 1], dtype=np.single)
        n = 2
        incx = 2
        incy = 2

        expected_x = np.array([1, 4, 3, 2, 5], dtype=np.single)
        expected_y = np.array([5, 2, 3, 4, 1], dtype=np.single)

        ScipyBlasService.sswap(n, x[1:], incx, y[1:], incy)
        np.testing.assert_array_equal(x, expected_x)
        np.testing.assert_array_equal(y, expected_y)

    def test_dswap(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.double)
        y = np.array([5, 4, 3, 2, 1], dtype=np.double)
        n = 3
        incx = 1
        incy = 1

        expected_x = np.array([5, 4, 3, 4, 5], dtype=np.double)
        expected_y = np.array([1, 2, 3, 2, 1], dtype=np.double)

        ScipyBlasService.dswap(n, x, incx, y, incy)
        np.testing.assert_array_equal(x, expected_x)
        np.testing.assert_array_equal(y, expected_y)

    def test_dswap2(self):
        """
        Test description
        """
        x = np.array([1, 2, 3, 4, 5], dtype=np.double)
        y = np.array([5, 4, 3, 2, 1], dtype=np.double)
        n = 2
        incx = 2
        incy = 2

        expected_x = np.array([1, 4, 3, 2, 5], dtype=np.double)
        expected_y = np.array([5, 2, 3, 4, 1], dtype=np.double)

        ScipyBlasService.dswap(n, x[1:], incx, y[1:], incy)
        np.testing.assert_array_equal(x, expected_x)
        np.testing.assert_array_equal(y, expected_y)

    def test_isamax(self):
        """
        Test description
        """
        x = np.array([1, 2, -3, -1, 1], dtype=np.single)
        n = 5
        incx = 1
        expected = 2

        res = ScipyBlasService.isamax(n, x, incx)
        self.assertAlmostEqual(res, expected)

    def test_isamax2(self):
        """
        Test description
        """
        x = np.array([1, -2, -3, 4, 5], dtype=np.single)
        n = 3
        incx = 2
        expected = 2

        res = ScipyBlasService.isamax(n, x, incx)
        self.assertAlmostEqual(res, expected)

    def test_idamax(self):
        """
        Test description
        """
        x = np.array([1, 2, -3, -1, 1], dtype=np.double)
        n = 5
        incx = 1
        expected = 2

        res = ScipyBlasService.idamax(n, x, incx)
        self.assertAlmostEqual(res, expected)

    # TODO: integer return leyenlerde almost equal duzelt
    def test_idamax2(self):
        """
        Test description
        """
        x = np.array([1, -2, -3, 4, 5], dtype=np.double)
        n = 3
        incx = 2
        expected = 2

        res = ScipyBlasService.idamax(n, x, incx)
        self.assertAlmostEqual(res, expected)


if __name__ == '__main__':
    unittest.main()
