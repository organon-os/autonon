"""this module includes tests for Interval"""
import unittest

from organon.fl.mathematics.businessobjects.sets.interval import Interval
from organon.fl.mathematics.enums.sets.interval_type import IntervalType


class IntervalTestCase(unittest.TestCase):
    """test class for Interval"""

    def setUp(self) -> None:
        self.interval_service_closed_closed = Interval("Test1", 2, 3, IntervalType.CLOSED_CLOSED)
        self.interval_service_open_closed = Interval("Test1", 2, 3, IntervalType.OPEN_CLOSED)
        self.interval_service_closed_open = Interval("Test1", 2, 3, IntervalType.CLOSED_OPEN)
        self.interval_service_open_open = Interval("Test1", 2, 3, IntervalType.OPEN_OPEN)
        self.interval_service_degenerate = Interval("Test1", 2, 2, IntervalType.OPEN_CLOSED)

    def test_interval_id_property(self):
        """Test for interval id property"""
        self.assertEqual(self.interval_service_open_closed.interval_id, "Test1")

    def test_lower_bound_property(self):
        """Test for lower bound property"""
        self.assertEqual(self.interval_service_open_closed.lower_bound, 2)

    def test_upper_bound_property(self):
        """Test for upper bound property"""
        self.assertEqual(self.interval_service_open_closed.upper_bound, 3)

    def test_interval_type_property(self):
        """Test for interval_type property"""
        self.assertEqual(self.interval_service_open_closed.interval_type, IntervalType.OPEN_CLOSED)

    def test_is_degenerate_property_no_degenerate(self):
        """Test for is_degenerate property"""
        self.assertEqual(self.interval_service_open_closed.is_degenerate, False)

    def test_is_degenerate_property__degenerate(self):
        """Test for is_degenerate property"""
        self.assertEqual(self.interval_service_degenerate.is_degenerate, True)

    def test_contains_with_interval_type_closed_closed_true(self):
        """Test contains for closed closed interval type"""
        self.assertTrue(self.interval_service_closed_closed.contains(2.0))

    def test_contains_with_interval_type_closed_closed_false(self):
        """Test contains for closed closed interval type"""
        self.assertFalse(self.interval_service_closed_closed.contains(1.0))

    def test_contains_with_interval_type_open_closed_true(self):
        """Test contains for open closed interval type"""
        self.assertTrue(self.interval_service_open_closed.contains(2.1))

    def test_contains_with_interval_type_open_closed_false(self):
        """Test contains for open closed interval type"""
        self.assertFalse(self.interval_service_open_closed.contains(2.0))

    def test_contains_with_interval_type_closed_open_true(self):
        """Test contains for closed open interval type"""
        self.assertTrue(self.interval_service_closed_open.contains(2.0))

    def test_contains_with_interval_type_closed_open_false(self):
        """Test contains for closed open interval type"""
        self.assertFalse(self.interval_service_closed_open.contains(3))

    def test_contains_with_interval_type_open_open_true(self):
        """Test contains for open open interval type"""
        self.assertTrue(self.interval_service_open_open.contains(2.1))

    def test_contains_with_interval_type_open_open_false(self):
        """Test contains for open open interval type"""
        self.assertFalse(self.interval_service_open_open.contains(2.0))

    def test_compare_interval_less_than(self):
        """Test compare two interval for less than"""
        interval_2 = Interval("Test1", 1.9, 3, IntervalType.CLOSED_CLOSED)
        self.assertTrue(interval_2 < self.interval_service_closed_open)

    def test_compare_interval_less_than_degenerate(self):
        """Test compare two interval for less than"""
        interval_2 = Interval("Test1", 1.9, 1.9, IntervalType.CLOSED_CLOSED)
        self.assertFalse(interval_2 < self.interval_service_closed_open)
        self.assertTrue(self.interval_service_closed_open < interval_2)

    def test_compare_interval_less_or_equal(self):
        """Test compare two interval for less or equal"""
        interval_2 = Interval("Test1", 2, 3, IntervalType.CLOSED_CLOSED)
        self.assertTrue(interval_2 <= self.interval_service_closed_open)

    def test_compare_interval_greater_than(self):
        """Test compare two interval for greater than"""
        interval_2 = Interval("Test1", 2.1, 3, IntervalType.CLOSED_CLOSED)
        self.assertTrue(interval_2 > self.interval_service_closed_open)

    def test_compare_interval_greater_or_equal(self):
        """Test compare two interval for greater or equal"""
        interval_2 = Interval("Test1", 2, 3, IntervalType.CLOSED_CLOSED)
        self.assertTrue(interval_2 >= self.interval_service_closed_open)

    def test_compare_equal(self):
        """Test compare two interval for equal"""
        interval_2 = Interval("Test1", 2, 3, IntervalType.CLOSED_CLOSED)
        self.assertTrue(interval_2 == self.interval_service_closed_open)

    def test_compare_not_equal(self):
        """Test compare two interval for not equal"""
        interval_2 = Interval("Test1", 2.5, 3, IntervalType.CLOSED_CLOSED)
        self.assertTrue(interval_2 != self.interval_service_closed_open)

    def test_compare_equal_degenerate(self):
        """Test compare two interval for not equal"""
        interval_2 = Interval("Test1", 2, 2, IntervalType.CLOSED_CLOSED)
        self.assertFalse(interval_2 == self.interval_service_closed_open)

    def test_to_string_closed_closed(self):
        """Test to_string function for Closed-Closed Interval type"""
        expected_result = '[2,3]'
        self.assertEqual(expected_result, self.interval_service_closed_closed.to_string())

    def test_to_string_closed_open(self):
        """Test to_string function for Closed-Open Interval type"""
        expected_result = '[2,3)'
        self.assertEqual(expected_result, self.interval_service_closed_open.to_string())

    def test_to_string_open_open(self):
        """Test to_string function for Open-Open Interval type"""
        expected_result = '(2,3)'
        self.assertEqual(expected_result, self.interval_service_open_open.to_string())

    def test_to_string_open_closed(self):
        """Test to_string function for Open-Closed Interval type"""
        expected_result = '(2,3]'
        self.assertEqual(expected_result, self.interval_service_open_closed.to_string())


    def test_deep_copy(self):
        """Test deep copy """
        interval_1 = Interval("Test1", 2.5, 3, IntervalType.CLOSED_CLOSED)
        interval_2 = interval_1.deep_copy()
        self.assertEqual(interval_1.interval_id, interval_2.interval_id)
        self.assertEqual(interval_1.lower_bound, interval_2.lower_bound)
        self.assertEqual(interval_1.upper_bound, interval_2.upper_bound)
        self.assertEqual(interval_1.interval_type, interval_2.interval_type)
