"""this module includes tests for SumTriple"""
import unittest
import numpy as np

from organon.fl.mathematics.businessobjects.sets.sum_triple import SumTriple


class SumTripleTestCase(unittest.TestCase):
    """test class for SumTriple"""

    def setUp(self) -> None:
        self.sum_triple_with_params = SumTriple(3, 6, 15)
        self.sum_triple_with_empty = SumTriple()

    def test_average(self):
        """Test for average of sum triple"""
        self.assertEqual(2.0, self.sum_triple_with_params.average)

    def test_variance(self):
        """Test for variance of sum triple"""
        self.assertEqual(1.0, self.sum_triple_with_params.variance)

    def test_std_dev(self):
        """Test for standard deviation of sum triple"""
        self.assertEqual(1.0, self.sum_triple_with_params.std_dev)

    def test_update_with_data(self):
        """Test update sum triple object with data"""
        self.sum_triple_with_params.update_with_data(3, 5)
        self.assertEqual(6, self.sum_triple_with_params.count)
        self.assertEqual(21, self.sum_triple_with_params.sum_)
        self.assertEqual(90, self.sum_triple_with_params.sum_of_squares)

    def test_update_with_sum_triple(self):
        """Test update sum triple object with another sum triple object"""
        self.sum_triple_with_params.update_with_sum_triple(self.sum_triple_with_params)
        self.assertEqual(6, self.sum_triple_with_params.count)
        self.assertEqual(12, self.sum_triple_with_params.sum_)
        self.assertEqual(30, self.sum_triple_with_params.sum_of_squares)

    def test_average_with_count_zero(self):
        """Test for average for empty sum triple"""
        self.assertTrue(np.isnan(self.sum_triple_with_empty.average))

    def test_variance_with_count_zero(self):
        """Test for variance for empty sum triple"""
        self.assertTrue(np.isnan(self.sum_triple_with_empty.variance))

    def test_std_dev_with_count_zero(self):
        """Test for variance for empty sum triple"""
        self.assertTrue(np.isnan(self.sum_triple_with_empty.std_dev))
