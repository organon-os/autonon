"""this module includes tests for TupleGenericCollection"""
import unittest

from organon.fl.mathematics.businessobjects.tuple_generic_collection import TupleGenericCollection


def get_collection():
    """
    get example collection
    """
    collection = TupleGenericCollection(3)
    collection.try_add(("c", "d"), "key2")
    collection.try_add(("a", "b"), "key1")
    return collection


class TupleGenericCollectionTestCase(unittest.TestCase):
    """test class for TupleGenericCollection"""

    def test_all(self):
        """tests all methods"""
        collection = get_collection()
        self.assertTrue(collection.try_add(("g", "h"), "key4"))
        self.assertFalse(collection.try_add(("e", "f"), "key3"))
        self.assertTrue(collection.try_add(("a", "b"), "key5"))
        self.assertEqual(collection.count, 3)
        self.assertEqual(collection["key5"], ("a", "b"))
        values = list(collection.list_per_tuple.values())
        self.assertListEqual(values, [["key1", "key5"], ["key2"], ["key4"]])


if __name__ == '__main__':
    unittest.main()
