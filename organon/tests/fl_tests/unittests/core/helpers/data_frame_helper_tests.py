"""Includes unittests for data_frame_helper module"""
import unittest

import numpy as np
import pandas as pd

from organon.fl.core.helpers import data_frame_helper
from organon.fl.core.helpers.data_frame_helper import get_correlation_matrix_memory_efficient
from organon.tests import test_helper


def _exec_parallel_side_effect(args_list, func, *args, **kwargs):
    # pylint: disable=unused-argument
    for arg in args_list:
        func(*arg)


class DataFrameHelperTestCase(unittest.TestCase):
    """Unittest class for data_frame_helper module"""

    def setUp(self) -> None:
        self.mock_parallel_exec_helper = test_helper.get_mock(self,
                                                              data_frame_helper.__name__ + ".parallel_execution_helper")
        self.mock_parallel_exec_helper.execute_parallel.side_effect = _exec_parallel_side_effect

    def test_get_correlation_matrix_memory_efficient(self):
        """tests get_correlation_matrix_memory_efficient with default args"""
        # pylint: disable=no-self-use
        data_frame = pd.DataFrame(np.random.rand(100, 10))
        corr_df = get_correlation_matrix_memory_efficient(data_frame)
        pd.testing.assert_frame_equal(data_frame.corr(), corr_df)

    def test_get_correlation_matrix_memory_efficient_columns_given(self):
        """tests get_correlation_matrix_memory_efficient with columns specified"""
        # pylint: disable=no-self-use
        data_frame = pd.DataFrame(np.random.rand(100, 10))
        cols = [2, 5, 8]
        corr_df = get_correlation_matrix_memory_efficient(data_frame, columns=cols)
        pd.testing.assert_frame_equal(data_frame[cols].corr(), corr_df)

    def test_get_correlation_matrix_memory_efficient_n_threads_given(self):
        """tests get_correlation_matrix_memory_efficient with columns specified"""
        # pylint: disable=no-self-use
        data_frame = pd.DataFrame(np.random.rand(100, 10))
        corr_df = get_correlation_matrix_memory_efficient(data_frame, n_threads=3)
        pd.testing.assert_frame_equal(data_frame.corr(), corr_df)
        self.assertEqual(3, self.mock_parallel_exec_helper.execute_parallel.call_args.kwargs["num_jobs"])

    def test_get_correlation_matrix_memory_efficient_only_half_filled(self):
        """tests get_correlation_matrix_memory_efficient with only_half_filled=True"""
        # pylint: disable=no-self-use
        data_frame = pd.DataFrame(np.random.rand(100, 10))
        corr_df = get_correlation_matrix_memory_efficient(data_frame, only_half_filled=True)

        expected_df = data_frame.corr()
        expected_df.values[np.tril_indices_from(expected_df.values)] = np.nan
        pd.testing.assert_frame_equal(expected_df, corr_df)

    def test_get_correlation_matrix_memory_efficient_with_diagonal_values(self):
        """tests get_correlation_matrix_memory_efficient with diagonal_values given"""
        # pylint: disable=no-self-use
        data_frame = pd.DataFrame(np.random.rand(100, 10))
        corr_df = get_correlation_matrix_memory_efficient(data_frame, diagonal_values=2)
        expected_df = data_frame.corr()
        for i in range(10):
            expected_df.iloc[i, i] = 2
        pd.testing.assert_frame_equal(expected_df, corr_df)


if __name__ == '__main__':
    unittest.main()
