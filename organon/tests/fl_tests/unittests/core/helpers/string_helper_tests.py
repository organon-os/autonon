"""
This module keeps StringHelper class tests
"""
import unittest
from organon.fl.core.helpers.string_helper import concatenate_with_pattern, concatenate_objects_with_pattern, \
    concatenate_objects_with_default_pattern


class StringHelperTestCase(unittest.TestCase):
    """
    Test class for StringHelper
    """
    def test_concatenate_with_pattern(self):
        """
        Test of concatenate_with_pattern function in the StringHelper class
        :return: nothing
        """
        self.assertEqual(concatenate_with_pattern("{0} = :{0}", " AND ", "name", "age", "id", "sex"),
                         "name = :name AND age = :age AND id = :id AND sex = :sex")

    def test_concatenate_objects_with_pattern(self):
        """
        Test of concatenate_objects_with_pattern function in the StringHelper class
        :return: nothing
        """
        self.assertEqual(concatenate_objects_with_pattern("{0} = :{0}", " AND ", "   ", "age", "name", "sex"),
                         "empty = :empty AND age = :age AND name = :name AND sex = :sex")

    def test_concatenate_objects_with_default_pattern(self):
        """
        Test of concatenate_objects_with_default_pattern function in the StringHelper class
        :return: nothing
        """
        self.assertEqual(concatenate_objects_with_default_pattern("   ", "age", "name", "sex"), "empty,age,name,sex,")


if __name__ == '__main__':
    unittest.main()
