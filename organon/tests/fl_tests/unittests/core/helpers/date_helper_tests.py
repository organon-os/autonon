"""
This module includes test class for 'date_helper' module.
"""
import unittest
from datetime import datetime

from organon.fl.core.helpers.date_helper import get_date_from_string, add_to_date


class DateHelperTests(unittest.TestCase):
    """Test class for 'date_helper' module"""

    def test_get_date_from_string(self):
        """Tests 'get_date_from_string' method."""
        self.assertEqual(get_date_from_string("18-01-2020 15:14:13"), datetime(2020, 1, 18, 15, 14, 13))
        self.assertEqual(get_date_from_string("18/01/2020 15:14:13.3"), datetime(2020, 1, 18, 15, 14, 13, 300000))
        self.assertEqual(get_date_from_string("01-18-2020 15.14.13", "%m-%d-%Y %H.%M.%S"),
                         datetime(2020, 1, 18, 15, 14, 13))
        self.assertEqual(get_date_from_string("20200118"), datetime(2020, 1, 18))
        self.assertEqual(get_date_from_string("2016-01-01"), datetime(2016, 1, 1))

    def test_add_to_date(self):
        """Tests 'add_to_date' method."""
        date = datetime(2020, 12, 22)
        self.assertEqual(add_to_date(date, years=-2, months=-2, days=-10), datetime(2018, 10, 12))
        self.assertEqual(add_to_date(date, months=2, days=7), datetime(2021, 3, 1))  # test leap year (Feb 29)
        self.assertEqual(add_to_date(date, hours=2, minutes=5, seconds=-10, microseconds=200),
                         datetime(2020, 12, 22, 2, 4, 50, 200))


if __name__ == '__main__':
    unittest.main()
