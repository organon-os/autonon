"""Includes tests for float_helper module functions."""
import unittest

from organon.fl.core.helpers import float_helper
from organon.fl.mathematics.constants import DOUBLE_MIN, DOUBLE_MAX


class FloatHelperTests(unittest.TestCase):
    """Test class for functions in float_helper module."""

    def test_round_1(self):
        """test rounding with 1 decimal places"""
        self.assertEqual(3.8, float_helper.round_value(3.75, 1))

    def test_round_2(self):
        """test rounding with 2 decimal places"""
        self.assertEqual(4.0, float_helper.round_value(3.75, 0))

    def test_is_extreme_nan(self):
        """tests is_extreme when value is nan"""
        self.assertTrue(float_helper.is_extreme(float("nan")))

    def test_is_extreme_inf(self):
        """tests is_extreme when value is infinity"""
        self.assertTrue(float_helper.is_extreme(float("inf")))

    def test_is_extreme_negative_inf(self):
        """tests is_extreme when value is negative infinity"""
        self.assertTrue(float_helper.is_extreme(float("-inf")))

    def test_is_extreme_less_than_min_float(self):
        """tests is_extreme when value is less than min float value"""
        self.assertTrue(float_helper.is_extreme(DOUBLE_MIN - 4))

    def test_is_extreme_greater_than_max_float(self):
        """tests is_extreme when value is greater than max float value"""
        self.assertTrue(float_helper.is_extreme(DOUBLE_MAX + 4))

    def test_is_extreme_normal_value(self):
        """tests is_extreme when value is not extreme"""
        self.assertFalse(float_helper.is_extreme(4.2))

    def test_equals(self):
        """tests equals method"""
        self.assertTrue(float_helper.equals(1.5, 1.5))

    def test_equals_nans(self):
        """tests equals method when both values are nan"""
        self.assertTrue(float_helper.equals(float("nan"), float("nan")))


if __name__ == '__main__':
    unittest.main()
