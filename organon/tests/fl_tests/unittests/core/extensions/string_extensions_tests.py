"""
This module keeps StringExtensions class tests
"""
import unittest
from organon.fl.core.extensions.string_extensions import get_bytes, is_alpha_numeric, is_word, is_number, \
    get_db_column_name_compatible_string, is_empty, remove_single_character, contains_white_space, nullable_length, \
    effective_value, equals_ignoring_case, to_upper_eng, to_lower_eng


class StringExtensionsTestCase(unittest.TestCase):
    """
    Test class for StringExtensions
    """
    def test_get_bytes(self):
        """
        Test of get_bytes function in the StringExtensions class
        :return: nothing
        """
        self.assertEqual(get_bytes("organon"), b'organon')

    def test_is_alpha_numeric(self):
        """
        Test of is_alpha_numeric function in the StringExtensions class
        :return: nothing
        """
        self.assertEqual(is_alpha_numeric("organon123"), True)

    def test_is_word(self):
        """
        Test of is_word function in the StringExtensions class
        :return: nothing
        """
        self.assertEqual(is_word("analytics"), True)

    def test_is_number(self):
        """
        Test of is_number function in the StringExtensions class
        :return: nothing
        """
        self.assertEqual(is_number("2011"), True)

    def test_get_db_column_name_compatible_string(self):
        """
        Test of get_db_column_name_compatible_string function in the StringExtensions class
        :return: nothing
        """
        self.assertEqual(get_db_column_name_compatible_string("Test", 3), "Tes")

    def test_is_empty(self):
        """
        Test of is_empty function in the StringExtensions class
        :return: nothing
        """
        self.assertEqual(is_empty(""), True)

    def test_remove_single_character(self):
        """
        Test of remove_single_character function in the StringExtensions class
        :return: nothing
        """
        self.assertEqual(remove_single_character("Organon", "o"), "Organn")

    def test_contains_white_space(self):
        """
        Test of contains_white_space function in the StringExtensions class
        :return: nothing
        """
        self.assertEqual(contains_white_space("Organon Analytics"), True)

    def test_nullable_length(self):
        """
        Test of nullable_length function in the StringExtensions class
        :return: nothing
        """
        self.assertEqual(nullable_length("Nullable Test"), "13")
        self.assertEqual(nullable_length(), "null")

    def test_effective_value(self):
        """
        Test of effective_value function in the StringExtensions class
        :return: nothing
        """
        self.assertEqual(effective_value("    "), "empty")

    def test_equals_ignoring_case(self):
        """
        Test of equals_ignoring_case function in the StringExtensions class
        :return: nothing
        """
        self.assertEqual(equals_ignoring_case("TEST", "test"), True)

    def test_to_upper_eng(self):
        """
        Test of to_upper_eng function in the StringExtensions class
        :return: nothing
        """
        self.assertEqual(to_upper_eng("high"), "HIGH")

    def test_to_lower_eng(self):
        """
        Test of to_lower_eng function in the StringExtensions class
        :return: nothing
        """
        self.assertEqual(to_lower_eng("TOLOWER"), "tolower")


if __name__ == '__main__':
    unittest.main()
