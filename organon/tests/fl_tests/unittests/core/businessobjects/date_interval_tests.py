"""
This module keeps DateInterval class tests
"""
import unittest
import datetime
from organon.fl.core.businessobjects.date_interval import DateInterval
from organon.fl.core.enums.date_interval_type import DateIntervalType


class DateIntervalTestCase(unittest.TestCase):
    """
    Test class for DateInterval
    """
    def test_get_func(self):
        """
        Tests of the functions in the DateInterval class
        :return: nothing
        """
        now = datetime.datetime.now()
        yesterday = now - datetime.timedelta(days=1)
        tomorrow = now - datetime.timedelta(days=-1)
        date_int = DateInterval(yesterday, tomorrow, DateIntervalType.CLOSED_CLOSED)
        self.assertEqual(date_int.contains(now - datetime.timedelta(days=-0.9)), True)
        self.assertEqual(date_int.contains(now - datetime.timedelta(days=-1)), True)
        self.assertEqual(date_int.contains(now - datetime.timedelta(days=-1.1)), False)


if __name__ == '__main__':
    unittest.main()
