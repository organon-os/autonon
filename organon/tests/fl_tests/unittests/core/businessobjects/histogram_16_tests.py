"""
This module keeps Histogram16 class tests
"""
import unittest
from organon.fl.core.businessobjects.histogram_16 import Histogram16


class Histogram16TestCase(unittest.TestCase):
    """
    Test class for Histogram16
    """

    def test_build_indices_for_compress(self):
        """
        Test of the build_indices function in the Histogram16 class
        :return: nothing
        """
        hist = Histogram16()
        hist.add("student", 140)
        hist.add("teacher", 80)
        hist.add("worker", 90)
        hist.add("doctor", 2)
        hist.add("nurse", 3)
        hist.build_indices()
        self.assertEqual(hist.bulk_set, ['nurse', 'doctor'])
        self.assertEqual(hist.compressed_index, {'student': 0, 'worker': 2, 'teacher': 1, 'nurse': -1, 'doctor': -1})
        self.assertEqual(hist.compressed_reverse_index, {0: ['student'], 2: ['worker'],
                                                         1: ['teacher'], -1: ['nurse', 'doctor']})
        self.assertEqual(hist.frequencies, {'student': 140, 'teacher': 80, 'worker': 90, 'doctor': 2, 'nurse': 3})
        self.assertEqual(hist.original_to_compressed, {0: 0, 1: 1, 2: 2, 3: -1, 4: -1})
        self.assertEqual(hist.reverse_index, {0: 'student', 1: 'teacher', 2: 'worker', 3: 'doctor', 4: 'nurse'})
        self.assertEqual(hist.index, {'student': 0, 'teacher': 1, 'worker': 2, 'doctor': 3, 'nurse': 4})

    def test_build_indices(self):
        """
        Test of the build_indices function in the Histogram16 class
        :return: nothing
        """
        hist = Histogram16()
        hist.add("student", 140)
        hist.add("teacher", 80)
        hist.add("worker", 90)
        hist.build_indices()
        self.assertEqual(hist.bulk_set, [])
        self.assertEqual(hist.compressed_index, {'student': 0, 'worker': 2, 'teacher': 1})
        self.assertEqual(hist.compressed_reverse_index, {0: ['student'], 2: ['worker'], 1: ['teacher']})
        self.assertEqual(hist.frequencies, {'student': 140, 'teacher': 80, 'worker': 90})
        self.assertEqual(hist.original_to_compressed, {0: 0, 1: 1, 2: 2})
        self.assertEqual(hist.reverse_index, {0: 'student', 1: 'teacher', 2: 'worker'})
        self.assertEqual(hist.index, {'student': 0, 'teacher': 1, 'worker': 2})

    def test_build_indices_with_params_for_compress(self):
        """
        Test of the build_indices_with_params function in the Histogram16 class
        :return: nothing
        """
        hist = Histogram16()
        hist.add("student", 1005)
        hist.add("teacher", 1000)
        hist.add("engineer", 200)
        hist.add("architect", 3000)
        hist.add("nurse", 5000)
        hist.add("worker", 2)
        hist.add("doctor", 2)
        hist.build_indices_with_params(0.4, 200)
        self.assertEqual(hist.index, {'student': 0, 'teacher': 1, 'engineer': 2, 'architect': 3, 'nurse': 4,
                                      'worker': 5, 'doctor': 6})
        self.assertEqual(hist.reverse_index, {0: 'student', 1: 'teacher', 2: 'engineer', 3: 'architect', 4: 'nurse',
                                              5: 'worker', 6: 'doctor'})
        self.assertEqual(hist.frequencies, {'student': 1005, 'teacher': 1000, 'engineer': 200, 'architect': 3000,
                                            'nurse': 5000, 'worker': 2, 'doctor': 2})
        self.assertEqual(hist.original_to_compressed, {0: -1, 1: -1, 2: -1, 3: -1, 4: 4, 5: -1, 6: -1})
        self.assertEqual(hist.compressed_reverse_index, {4: ['nurse'], -1: ['architect', 'student', 'teacher',
                                                                            'engineer', 'doctor', 'worker']})
        self.assertEqual(hist.compressed_index, {'nurse': 4, 'architect': -1, 'student': -1, 'teacher': -1,
                                                 'engineer': -1, 'doctor': -1, 'worker': -1})
        self.assertEqual(hist.bulk_set, ['architect', 'student', 'teacher', 'engineer', 'doctor', 'worker'])

    def test_add_with_frequencies(self):
        """
        Test of the add function in the Histogram16 class
        :return: nothing
        """
        hist = Histogram16()
        data = {"student": [2, 3], "teacher": [2]}
        for value in data.items():
            for freq in value[1]:
                hist.add(value[0], freq)
        self.assertEqual(hist.index, {"student": 0, "teacher": 1})
        self.assertEqual(hist.frequencies, {"student": 5, "teacher": 2})

    def test_add_without_frequencies(self):
        """
        Test of the add function in the Histogram16 class
        :return: nothing
        """
        hist = Histogram16()
        data = ["student", "teacher"]
        for value in data:
            hist.add(value)
        self.assertEqual(hist.bulk_set_index, -1)
        self.assertEqual(hist.index, {"student": 0, "teacher": 1})
        self.assertEqual(hist.reverse_index, {0: "student", 1: "teacher"})
        self.assertEqual(hist.frequencies, {"student": 1, "teacher": 1})
