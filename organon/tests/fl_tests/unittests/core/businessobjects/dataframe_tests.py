"""
Module for Organon Dataframe tests
"""
import unittest

from organon.fl.core.businessobjects.dataframe import DataFrame


class DataFrameTests(unittest.TestCase):
    """
    Tests for Organon DataFrame
    """

    def test_basic_methods(self):
        """
        Test basic methods like add
        """
        data_frame = DataFrame(10)
        self.assertRaises(ValueError, lambda: data_frame.try_add("shortcolumn", [1, 2, 3, 4]))
        data_frame.try_add("testcol", [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
        data_frame.try_add("testcol2", [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
        self.assertRaises(ValueError, lambda: data_frame.try_add("testcol", [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]))
        self.assertFalse(data_frame.contains_column("shortcolumn"))
        self.assertTrue(data_frame.contains_column("testcol"))
        self.assertListEqual(data_frame.get_column_names(), ["testcol", "testcol2"])
        data_frame.remove("testcol")
        self.assertListEqual(data_frame.get_column_names(), ["testcol2"])

    def test_set_value(self):
        """
        Test set value
        """
        data_frame = DataFrame(10)
        data_frame.try_add("testcol", [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
        data_frame.try_add("testcol2", [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
        data_frame.set_value(0, "testcol", 4)
        self.assertEqual(data_frame.data_frame.iloc[4]["testcol"], 0)
        data_frame.set_value(3, "testcol3", 2)  # new column should be generated with other cells having nan.
        self.assertEqual(data_frame.data_frame.iloc[2]["testcol3"], 3)


if __name__ == '__main__':
    unittest.main()
