"""Includes unittests for parallel_execution_helper"""
import unittest

from organon.fl.core.executionutil import parallel_execution_helper
from organon.tests import test_helper


def _func(val1, val2):
    return val1 + val2


class ParallelExecutionHelperTestCase(unittest.TestCase):
    """Unittest class for parallel_execution_helper"""

    def setUp(self) -> None:
        self.mock_thread = test_helper.get_mock(self, parallel_execution_helper.__name__ + ".Thread")
        self.mock_parallel = test_helper.get_mock(self, parallel_execution_helper.__name__ + ".Parallel")
        self.mock_delayed = test_helper.get_mock(self, parallel_execution_helper.__name__ + ".delayed")

    def test_execute_parallel(self):
        """tests execute_parallel with defaults"""
        all_params = [(1, 2), (3, 4), (5, 6), (7, 8)]
        parallel_execution_helper.execute_parallel(all_params, _func, a=5)
        delayed_generator = self.mock_parallel.return_value.call_args.args[0]
        self.mock_parallel.assert_called_once_with(n_jobs=-1, backend=None, require=None, prefer=None, a=5)
        cnt = 0
        for i, _ in enumerate(delayed_generator):
            self.mock_delayed.assert_called_with(_func)
            self.mock_delayed.return_value.assert_called_with(*all_params[i])
            cnt += 1
        self.assertEqual(len(all_params), cnt)

    def test_execute_parallel_require_shared_mem(self):
        """tests execute_parallel with require_shared_mem"""
        all_params = [(1, 2), (3, 4), (5, 6), (7, 8)]
        parallel_execution_helper.execute_parallel(all_params, _func, require_shared_memory=True)
        delayed_generator = self.mock_parallel.return_value.call_args.args[0]
        self.mock_parallel.assert_called_once_with(n_jobs=-1, backend=None, require="sharedmem", prefer=None)
        cnt = 0
        for i, _ in enumerate(delayed_generator):
            self.mock_delayed.assert_called_with(_func)
            self.mock_delayed.return_value.assert_called_with(*all_params[i])
            cnt += 1
        self.assertEqual(len(all_params), cnt)

    def test_execute_parallel_prefer_threads(self):
        """tests execute_parallel with prefer_threads"""
        all_params = [(1, 2), (3, 4), (5, 6), (7, 8)]
        parallel_execution_helper.execute_parallel(all_params, _func, prefer_threads=True)
        delayed_generator = self.mock_parallel.return_value.call_args.args[0]
        self.mock_parallel.assert_called_once_with(n_jobs=-1, backend=None, require=None, prefer="threads")
        cnt = 0
        for i, _ in enumerate(delayed_generator):
            self.mock_delayed.assert_called_with(_func)
            self.mock_delayed.return_value.assert_called_with(*all_params[i])
            cnt += 1
        self.assertEqual(len(all_params), cnt)

    def test_execute_parallel_single_argument_list(self):
        """tests execute_parallel when arguments are given as single values"""

        def _single_arg_func(arg):
            return arg

        all_params = [1, 2, 3]
        parallel_execution_helper.execute_parallel(all_params, _single_arg_func)
        delayed_generator = self.mock_parallel.return_value.call_args.args[0]
        self.mock_parallel.assert_called_once_with(n_jobs=-1, backend=None, require=None, prefer=None)
        cnt = 0
        for i, _ in enumerate(delayed_generator):
            self.mock_delayed.assert_called_with(_single_arg_func)
            self.mock_delayed.return_value.assert_called_with(all_params[i])
            cnt += 1
        self.assertEqual(len(all_params), cnt)

    def test_execute_parallel_no_argument_list(self):
        """tests execute_parallel when arguments are given as single values"""

        def _no_arg_func():
            return 5

        all_params = [(), (), ()]
        parallel_execution_helper.execute_parallel(all_params, _no_arg_func)
        delayed_generator = self.mock_parallel.return_value.call_args.args[0]
        self.mock_parallel.assert_called_once_with(n_jobs=-1, backend=None, require=None, prefer=None)
        cnt = 0
        for _ in delayed_generator:
            self.mock_delayed.assert_called_with(_no_arg_func)
            self.mock_delayed.return_value.assert_called_with()
            cnt += 1
        self.assertEqual(3, cnt)

    def test_execute_multithreaded_len_params_divisible_by_num_threads(self):
        """tests execute_multithreaded when length of params is divisible by num threads"""
        all_params = [(1, 2), (3, 4), (5, 6), (7, 8)]
        parallel_execution_helper.execute_multithreaded(all_params, _func, num_threads=2)
        self.assertEqual(2, self.mock_thread.call_count)
        self.assertEqual((_func, [(1, 2), (3, 4)]), self.mock_thread.call_args_list[0].kwargs["args"])
        self.assertEqual((_func, [(5, 6), (7, 8)]), self.mock_thread.call_args_list[1].kwargs["args"])

    def test_execute_multithreaded_len_params_not_divisible_by_num_threads(self):
        """tests execute_multithreaded when length of params is not divisible by num threads"""
        all_params = [(1, 2), (3, 4), (5, 6), (7, 8), (9, 10)]
        parallel_execution_helper.execute_multithreaded(all_params, _func, num_threads=3)
        self.assertEqual(3, self.mock_thread.call_count)
        self.assertEqual((_func, [(1, 2), (3, 4)]), self.mock_thread.call_args_list[0].kwargs["args"])
        self.assertEqual((_func, [(5, 6), (7, 8)]), self.mock_thread.call_args_list[1].kwargs["args"])
        self.assertEqual((_func, [(9, 10)]), self.mock_thread.call_args_list[2].kwargs["args"])

    def test_execute_multithreaded_num_threads_not_given(self):
        """tests execute_multithreaded when num_threads not given"""
        all_params = [(1, 2), (3, 4), (5, 6)]
        parallel_execution_helper.execute_multithreaded(all_params, _func, num_threads=None)
        self.assertEqual(3, self.mock_thread.call_count)
        self.assertEqual((_func, [(1, 2)]), self.mock_thread.call_args_list[0].kwargs["args"])
        self.assertEqual((_func, [(3, 4)]), self.mock_thread.call_args_list[1].kwargs["args"])
        self.assertEqual((_func, [(5, 6)]), self.mock_thread.call_args_list[2].kwargs["args"])


if __name__ == '__main__':
    unittest.main()
