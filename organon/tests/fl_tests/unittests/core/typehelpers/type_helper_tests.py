"""
This module keeps all testcases of helpers folder
"""
import unittest
from organon.fl.core.helpers import boolean_helper
from organon.fl.core.helpers import float_helper
from organon.fl.core.helpers import guid_helper
from organon.fl.core.helpers import list_helper
from organon.fl.core.helpers import object_helper
from organon.fl.core.helpers import string_helper


class TypeHelperTests(unittest.TestCase):
    """
    Test class for all modules in helpers folder
    """
    def test_boolean(self):
        """
        Test of functions in the boolean_helper module
        :return: nothing
        """
        self.assertEqual(boolean_helper.to_int(True), 1)
        self.assertEqual(boolean_helper.from_int(3), False)

    def test_date(self):
        """
        Test of functions in the date_helper module
        :return: nothing
        """
        self.assertEqual(True, True)

    def test_double(self):
        """
        Test of function in the double_helper module
        :return: nothing
        """
        self.assertEqual(float_helper.round_value(12.239845129871204, 5), 12.23985)

    def test_guid(self):
        """
        Test of function in the guid_helper module
        :return: nothing
        """
        self.assertEqual(len(guid_helper.new_guid(35)), 32)

    def test_list(self):
        """
        Test of functions in the list_helper module
        :return: nothing
        """
        self.assertEqual(list_helper.is_null_or_empty([]), True)
        self.assertEqual(list_helper.is_null([1, 2, 3]), False)

    def test_object(self):
        """
        Test of function in the object_helper module
        :return: nothing
        """
        self.assertEqual(object_helper.is_null(None), True)

    def test_string(self):
        """
        Test of function in the string_helper module
        :return: nothing
        """
        self.assertEqual(string_helper.filter_non_alpha_numeric("1asdf'\"\\#23"), "1asdf23")
        self.assertEqual(string_helper.remove_single_character("character", 'c'), "harater")
        self.assertEqual(string_helper.equals_ignoring_case("organon", "organon"), True)
        self.assertEqual(string_helper.to_upper_eng("test"), "TEST")
        self.assertEqual(string_helper.to_lower_eng("TEST"), "test")
        self.assertEqual(string_helper.concatenate_all("concatenate", "all", "strings"), "concatenateallstrings")
        self.assertEqual(string_helper.is_null_or_empty(""), True)
        self.assertEqual(string_helper.to_null_safe_string(7), "7")
        self.assertEqual(string_helper.is_null_or_white_space("  "), True)


if __name__ == '__main__':
    unittest.main()
