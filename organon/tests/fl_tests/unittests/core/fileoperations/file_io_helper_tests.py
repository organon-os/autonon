"""
This module keeps FileIoHelper class tests
"""
import unittest
import os
from organon.fl.core.fileoperations import file_io_helper
from organon.fl.core.fileoperations.file_read_options import FileReadOptions


def create_file():
    """
    Creates a file to use in the test cases.
    :return: nothing
    """
    with open(os.path.abspath("new_file.txt"), 'wb') as new_file:
        new_file.write("Organon\nAnalytics\nDeneme\n".encode("utf-8"))


class FileIoHelperTests(unittest.TestCase):
    """
    Test class for FileIoHelper
    """
    def test_check_file_exists(self):
        """
        Test of check_file_exists function in the FileIoHelper class
        :return: nothing
        """
        create_file()
        self.assertEqual(file_io_helper.check_file_exists(os.path.abspath("new_file.txt")), True)
        os.remove('new_file.txt')

    def test_read_all_lines(self):
        """
        Test of read_all_lines function in the FileIoHelper class
        :return: nothing
        """
        create_file()
        file_read_opt = FileReadOptions()
        file_read_opt.file_full_path = os.path.abspath("new_file.txt")
        self.assertEqual(len(file_io_helper.read_all_lines(file_read_opt)), 3)
        os.remove('new_file.txt')

    def test_save_content_to_file(self):
        """
        Test of save_content_to_file function in the FileIoHelper class
        :return: nothing
        """
        create_file()
        with open(os.path.abspath('new_file.txt'), 'rb') as in_file:
            file_detail = in_file.read()
            file_io_helper.write_to_file(os.path.abspath("output_new.txt"), file_detail.decode('utf-8'))

        with open(os.path.abspath("output_new.txt"), 'rb') as save_file:
            self.assertEqual(len(file_detail), len(save_file.read().decode('utf-8')))
        os.remove("output_new.txt")
        os.remove('new_file.txt')

    def test_read_all_text_from_resource_file(self):
        """
        Test of read_all_text_from_resource_file function in the FileIoHelper class
        :return: nothing
        """
        path = os.path.abspath("test_file.txt")
        with open(path, 'wb') as test_file:
            test_file.write("Test\nFor\nResource\nFile".encode("utf-8"))
        file_io_helper.read_all_text_from_resource_file(path)
        os.remove(path)
        self.assertEqual(True, True)


if __name__ == '__main__':
    unittest.main()
