"""
This module keeps DirectoryHelper class tests
"""
import unittest
import os
from organon.fl.core.fileoperations import directory_helper


class DirectoryHelperTest(unittest.TestCase):
    """
    Test class for DirectoryHelper
    """
    def test_get_current_directory(self):
        """
        Test of get_current_directory function in the DirectoryHelper class
        :return: nothing
        """
        self.assertEqual(directory_helper.get_current_directory(), os.path.dirname(__file__))


if __name__ == '__main__':
    unittest.main()
