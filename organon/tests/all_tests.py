"""
Runs unit tests in different folders
"""
import logging
import os
import sys
import unittest

import xmlrunner

ORGANON_DIR_PATH = os.path.dirname(os.path.dirname(__file__))
BASE_PATH = os.path.join(ORGANON_DIR_PATH, "tests")


def get_test_suite_for_path(path: str):
    """
    gets a test suite
    """
    loader = unittest.TestLoader()
    start_dir = path
    suite = loader.discover(start_dir, pattern="*_tests.py", top_level_dir=os.path.dirname(ORGANON_DIR_PATH))
    return suite


def add_test_suite(source_suite, target_suite):
    """
    Concatenates two suites
    """
    for test in target_suite:
        source_suite.addTest(test)
    return source_suite


logging.disable(logging.CRITICAL)

suite1 = get_test_suite_for_path(BASE_PATH + "/fl_tests/unittests")
suite2 = get_test_suite_for_path(BASE_PATH + "/afe_tests/unittests")
suite3 = get_test_suite_for_path(BASE_PATH + "/idq_tests/unittests")
suite4 = get_test_suite_for_path(BASE_PATH + "/ml_tests/unittests")

suite1 = add_test_suite(suite1, suite2)
suite1 = add_test_suite(suite1, suite3)
suite1 = add_test_suite(suite1, suite4)

if not os.path.exists("reports"):
    os.makedirs("reports")
TEST_REPORT_PATH = "reports/testResults.xml"
with open(f'{TEST_REPORT_PATH}', 'wb') as output:
    runner = xmlrunner.XMLTestRunner(output=output)
    result = runner.run(suite1)

sys.exit(not result.wasSuccessful())
