"""Includes ReadCsvObject class"""
import pandas as pd


class ReadCsvObject:
    """mocks object returned from pandas.read_csv"""

    def __init__(self, data: pd.DataFrame, chunksize: int):
        self.data = data
        self.columns = self.data.columns
        self.data_length = len(self.data)
        self.chunksize = chunksize
        self.next_index = 0

    def __iter__(self):
        return self

    def __next__(self):
        min_index = self.next_index
        if min_index >= self.data_length:
            raise StopIteration()
        max_index = min(self.data_length, min_index + self.chunksize)

        val = self.data[min_index:max_index]
        self.next_index = max_index
        return val
