"""Includes helper methods for tests"""
import os
import threading
from unittest import TestCase
from unittest.mock import patch

from organon.fl.generic.interaction.fl_initializer import FlInitializer

TEST_INITIALIZED = False
test_helper_path = os.path.dirname(__file__) + "\\"
__INITIALIZATION_LOCK = threading.Lock()
COMMON_TEST_DATA_PATH = os.path.join(test_helper_path, "common_test_data")


def prepare_test():
    """Prepares test"""
    global TEST_INITIALIZED  # pylint: disable=global-statement

    with __INITIALIZATION_LOCK:
        if not TEST_INITIALIZED:
            FlInitializer.application_initialize()
            TEST_INITIALIZED = True


def get_test_data_path(file_name: str):
    """Returns absolute path of given file under common_test_data"""
    return os.path.join(COMMON_TEST_DATA_PATH, file_name)


def get_mock(test_case: TestCase, *args, **kwargs):
    """Patches and returns mock. args and kwargs should be args and kwargs for mock.patch"""
    patcher = patch(*args, **kwargs)
    test_case.addCleanup(patcher.stop)
    return patcher.start()
