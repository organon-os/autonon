"""console tests for Text Classification Module"""
import os
import unittest

import datasets
import pandas as pd
from sklearn.metrics import accuracy_score, f1_score

from organon.ml.text_classification.services.text_classifier import TextClassifier


class TextClassificationConsoleTests(unittest.TestCase):
    """Text Classification Console Test class"""
    def setUp(self):
        os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

    def test_model_predict_tweet_eval_from_csv(self):
        """test model fit"""

        txt_classifier = TextClassifier("efficient", "bert_base", text_column_name="text", target_column_name="label")
        txt_classifier.fit(r"C:\GIT\autoML2\autonon-premium\organon\ml\text_classification\services\tweet.csv",
                           r"C:\GIT\autoML2\autonon-premium\organon\ml\text_classification\services\tweet_val.csv")
        test = pd.read_csv(r"C:\GIT\autoML2\autonon-premium\organon\ml\text_classification\services\tweet_val.csv")
        probabilities, roc_auc_score = txt_classifier.predict_proba(test)
        class_predictions = txt_classifier.predict(test)
        print(f"probabilities: {probabilities}")
        print(f"roc_auc_score: {roc_auc_score}")
        print(f"class predictions: {class_predictions}")
        self.assertIsNotNone(probabilities)
        self.assertIsNotNone(roc_auc_score)
        self.assertIsNotNone(class_predictions)

    def test_model_predict_tweet_eval_from_dataframe_val_ratio_balanced(self):
        """test model fit"""
        train_data = pd.read_csv(r"C:\GIT\autoML\autonon-premium\organon\ml\text_classification\services\tweet.csv")
        val_data = pd.read_csv(r"C:\GIT\autoML\autonon-premium\organon\ml\text_classification\services\tweet_val.csv")
        train_data = train_data[:100]
        val_data = val_data[:50]
        train_data = train_data.rename(columns={"text": "metin", "label": "tur"})
        val_data = val_data.rename(columns={"text": "metin", "label": "tur"})
        txt_classifier = TextClassifier("efficient", "bert_base", text_column_name="metin", target_column_name="tur")
        txt_classifier.set_mdl_settings(batch_size=1, epoch=3)
        txt_classifier.fit(data=train_data, validation_data=val_data, validation_data_ratio=0.5)

        test = pd.read_csv(r"C:\GIT\autoML\autonon-premium\organon\ml\text_classification\services\tweet_test30.csv")
        test = test.rename(columns={"text": "metin", "label": "tur"})
        probabilities, roc_auc_score = txt_classifier.predict_proba(test)
        class_predictions = txt_classifier.predict(test)
        print(f"probabilities : {probabilities}")
        print(f"roc_auc_score: {roc_auc_score}")
        print(f"class predictions: {class_predictions}")
        self.assertIsNotNone(probabilities)
        self.assertIsNotNone(roc_auc_score)
        self.assertIsNotNone(class_predictions)
        scores_dataframe = pd.DataFrame({
            'Test Acc': [accuracy_score(test['tur'], class_predictions)],
            'Test ROC': [roc_auc_score],
            'f1score': [f1_score(test['tur'], class_predictions, average='weighted')]})
        print(scores_dataframe)

    def test_model_predict_tweet_eval_from_dataframe_val_ratio_default(self):
        """test model fit"""
        train_data = pd.read_csv(r"C:\GIT\autoML\autonon-premium\organon\ml\text_classification\services\tweet.csv")
        val_data = pd.read_csv(r"C:\GIT\autoML\autonon-premium\organon\ml\text_classification\services\tweet_val.csv")
        train_data = train_data[:100]
        val_data = val_data[:50]
        train_data = train_data.rename(columns={"text": "metin", "label": "tur"})
        val_data = val_data.rename(columns={"text": "metin", "label": "tur"})
        txt_classifier = TextClassifier("efficient", "bert_base", text_column_name="metin", target_column_name="tur")
        txt_classifier.set_mdl_settings(batch_size=1, epoch=5)
        txt_classifier.fit(data=train_data, validation_data=val_data)

        test = pd.read_csv(r"C:\GIT\autoML\autonon-premium\organon\ml\text_classification\services\tweet_test30.csv")
        test = test.rename(columns={"text": "metin", "label": "tur"})
        probabilities, roc_auc_score = txt_classifier.predict_proba(test)
        class_predictions = txt_classifier.predict(test)
        print(f"probabilities : {probabilities}")
        print(f"roc_auc_score: {roc_auc_score}")
        print(f"class predictions: {class_predictions}")
        self.assertIsNotNone(probabilities)
        self.assertIsNotNone(roc_auc_score)
        self.assertIsNotNone(class_predictions)
        scores_dataframe = pd.DataFrame({
            'Test Acc': [accuracy_score(test['tur'], class_predictions)],
            'Test ROC': [roc_auc_score],
            'f1score': [f1_score(test['tur'], class_predictions, average='weighted')]})
        print(scores_dataframe)

    def test_model_predict_tweet_eval_from_csv_file(self):
        """test model fit"""
        txt_classifier = TextClassifier("efficient", "bert_base", text_column_name="text",
                                        target_column_name="label")
        txt_classifier.set_mdl_settings(batch_size=1, epoch=3)
        txt_classifier.fit(r"C:\GIT\autoML\autonon-premium\organon\ml\text_classification\services\tweet30.csv",
                           r"C:\GIT\autoML\autonon-premium\organon\ml\text_classification\services\tweet_val20.csv")

        test = pd.read_csv(r"C:\GIT\autoML\autonon-premium\organon\ml\text_classification\services\tweet_test30.csv")
        probabilities, roc_auc_score = txt_classifier.predict_proba(test)
        class_predictions = txt_classifier.predict(test)
        print(f"probabilities : {probabilities}")
        print(f"roc_auc_score: {roc_auc_score}")
        print(f"class predictions: {class_predictions}")
        self.assertIsNotNone(probabilities)
        self.assertIsNotNone(roc_auc_score)
        self.assertIsNotNone(class_predictions)
        scores_dataframe = pd.DataFrame({
            'Test Acc': [accuracy_score(test['label'], class_predictions)],
            'Test ROC': [roc_auc_score],
            'f1score': [f1_score(test['label'], class_predictions, average='weighted')]})
        print(scores_dataframe)

    def test_model_predict_tweet_eval_from_dataframe_high_perf(self):
        """test model fit"""
        train_data = pd.read_csv(r"C:\GIT\autoML\autonon-premium\organon\ml\text_classification\services\tweet.csv")
        val_data = pd.read_csv(r"C:\GIT\autoML\autonon-premium\organon\ml\text_classification\services\tweet_test.csv")
        train_data = train_data[10:30]
        val_data = val_data[10:30]
        train_data = train_data.rename(columns={"text": "metin", "label": "tur"})
        val_data = val_data.rename(columns={"text": "metin", "label": "tur"})
        txt_classifier = TextClassifier("high_performance", "bert_base", text_column_name="metin",
                                        target_column_name="tur")
        txt_classifier.fit(data=train_data, validation_data=val_data)

        test = pd.read_csv(r"C:\GIT\autoML\autonon-premium\organon\ml\text_classification\services\tweet_val.csv")
        test = test[10:30]
        test = test.rename(columns={"text": "metin", "label": "tur"})
        probabilities, roc_auc_score = txt_classifier.predict_proba(test)
        class_predictions = txt_classifier.predict(test)
        print(f"probabilities : {probabilities}")
        print(f"roc_auc_score: {roc_auc_score}")
        print(f"class predictions: {class_predictions}")
        self.assertIsNotNone(probabilities)
        self.assertIsNotNone(roc_auc_score)
        self.assertIsNotNone(class_predictions)

    def test_model_fit_hf_dataset_high_perf(self):
        """using hf datasets, fit the model with runtype High-Performance"""
        txt_classifier = TextClassifier("high_performance", text_column_name="text",
                                        target_column_name="label")
        txt_classifier.fit(data=("tweet_eval", "emotion"))
        class_predictions = txt_classifier.predict(("tweet_eval", "emotion"))
        print(f"class predictions: {class_predictions}")
        self.assertIsNotNone(class_predictions)

    def test_model_fit_dataframe_high_perf(self):
        """using hf datasets, fit the model with runtype High-Performance"""
        txt_classifier = TextClassifier("high_performance", text_column_name="text",
                                        target_column_name="label")
        train_data = pd.read_csv(r"C:\GIT\autoML\autonon-premium\organon\ml\text_classification\services\tweet.csv")
        val_data = pd.read_csv(r"C:\GIT\autoML\autonon-premium\organon\ml\text_classification\services\tweet_test.csv")
        train_data = train_data[10:15]
        val_data = val_data[10:15]
        txt_classifier.fit(data=train_data, validation_data=val_data)
        class_predictions = txt_classifier.predict(("tweet_eval", "emotion"))
        print(f"class predictions: {class_predictions}")
        self.assertIsNotNone(class_predictions)

    def test_model_fit_hf_dataset_test_data_dataframe(self):
        """using hf datasets, fit the model"""
        txt_classifier = TextClassifier("efficient", "bert_base", text_column_name="text",
                                        target_column_name="label")
        txt_classifier.fit(data=("tweet_eval", "emotion"))
        test = pd.DataFrame(datasets.load_dataset("tweet_eval", "emotion", split="test"))
        probabilities, roc_auc_score = txt_classifier.predict_proba(test)
        class_predictions = txt_classifier.predict(test)
        self.assertIsNotNone(probabilities)
        self.assertIsNotNone(roc_auc_score)
        self.assertIsNotNone(class_predictions)

    def test_model_fit_hf_dataset_test_with_dataset_has_no_val_set(self):
        """using hf datasets, fit the model"""
        txt_classifier = TextClassifier("efficient", "bert_base", text_column_name="text",
                                        target_column_name="label")
        txt_classifier.fit(data=("imdb", None), validation_data_ratio=0.3)
        test = pd.DataFrame(datasets.load_dataset("tweet_eval", "emotion", split="test"))
        probabilities, roc_auc_score = txt_classifier.predict_proba(test)
        class_predictions = txt_classifier.predict(test)
        self.assertIsNotNone(probabilities)
        self.assertIsNotNone(roc_auc_score)
        self.assertIsNotNone(class_predictions)

    def test_model_fit_turkish_dataset(self):
        """using hf datasets, fit the model with turkish dataset"""
        txt_classifier = TextClassifier("efficient", language='turkish', checkpoint="BERT_BASE_TR_SENTIMENT",
                                        text_column_name="text", target_column_name="label")
        train = pd.DataFrame(datasets.load_dataset("Toygar/turkish-offensive-language-detection", split="train"))
        val = pd.DataFrame(datasets.load_dataset("Toygar/turkish-offensive-language-detection", split="validation"))
        test = pd.DataFrame(datasets.load_dataset("Toygar/turkish-offensive-language-detection", split="test"))
        train = train[10:15]
        val = val[10:15]
        test = test[10:15]
        txt_classifier.set_mdl_settings(batch_size=1, epoch=3)
        txt_classifier.fit(data=train, validation_data=val)
        probabilities, roc_auc_score = txt_classifier.predict_proba(test)
        class_predictions = txt_classifier.predict(test)
        print(f"probabilities : {probabilities}")
        print(f"roc_auc_score: {roc_auc_score}")
        print(f"class predictions: {class_predictions}")
        self.assertIsNotNone(probabilities)
        self.assertIsNotNone(roc_auc_score)

    def test_if_model_is_appropriate(self):
        """using hf datasets, fit the model with runtype High-Performance"""
        txt_classifier = TextClassifier("efficient", "bert_base_tr_128", language="english", text_column_name="text",
                                        target_column_name="label")
        txt_classifier.fit(data=("tweet_eval", "emotion"))
        class_predictions = txt_classifier.predict(test_data=("tweet_eval", "emotion"))
        print(f"class predictions: {class_predictions}")
        self.assertIsNotNone(class_predictions)
