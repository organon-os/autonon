"""Feature Reduction Drop Cols Console Test"""
import unittest

import pandas as pd
from organon.ml.feature_reduction.services.feature_reduction import FeatureReduction
from organon.ml.feature_reduction.domain.enums.feature_reduction_types import FeatureReductionType


class FeatureReductionConsoleTests(unittest.TestCase):
    """Feature Reduction Drop Cols Test"""

    def setUp(self) -> None:
        self.data = pd.DataFrame(
            {"col1": [1, None, 3, 3, 5, 9, 16, 78, 56, 13, 65, 2, 5, 6, 7],
             "col2": ['o1', 'o2', 'o1', 'o1', 'o1', 'o2', 'o2', 'o2', 'o3', 'o3', 'o3', 'o3', 'o3', 'o3', 'o3'],
             "col3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
             "col4": [9, 10, 11, 12, 13, 14, 15, 1, 2, 3, 4, 5, 6, 7, 8],
             "col50": [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
             "col5": [10, 11, 12, 13, 1, 2, 3, 4, 5, 6, 7, 8, 9, 14, 15],
             "col6": [16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30],
             "col7": [24, 25, 26, 27, 28, 29, 30, 16, 17, 18, 19, 20, 21, 22, 23],
             "col8": [0, 1, 1, 4, 0, 1, 1, 1, 5, 1, 1, 1, 1, 8, 0],
             "col9": [1, 2, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3],
             "col10": ['organon', 'organon', 'non', 'organon', 'non', 'organon', 'non', 'non', 'non', 'organon', 'non',
                       'organon', 'non', 'organon', 'organon'],
             "target": [0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0]
             })


    def test_feature_reduction_drop_cols(self):
        """drop_cols test"""

        data = pd.concat([self.data] * 32)
        drop_cols = True

        feature_red = FeatureReduction()
        feature_red.set_null_feature_reduction_settings(0.9)
        feature_red.set_similar_distribution_feature_reduction_settings(5)
        feature_red.set_univariate_performance_feature_reduction_settings(0.6)

        output = feature_red.execute(data, drop_cols,
                                     included_reduction_types=[FeatureReductionType.UNIVARIATE_PERFORMANCE.name,
                                                               FeatureReductionType.NULL.name,
                                                               FeatureReductionType.SIMILAR_DISTRIBUTION.name,
                                                               FeatureReductionType.HIGH_CORRELATION.name,
                                                               FeatureReductionType.STABILITY.name,
                                                               ],
                                     target_type="BINARY", target_column_name="target",
                                     performance_metric="roc_auc")
        for rep in output:
            print(rep.feature_reduction_type.name + ":" + rep.reduced_column_list.__str__())

        self.assertEqual(1, len(output[0].reduced_column_list))
        self.assertEqual(set(output[0].reduced_column_list).issubset(data.columns), False)
        self.assertEqual(0, len(output[1].reduced_column_list))
        self.assertEqual(set(output[1].reduced_column_list).issubset(data.columns), True)
        self.assertEqual(2, len(output[2].reduced_column_list))
        self.assertEqual(set(output[2].reduced_column_list).issubset(data.columns), False)
        self.assertEqual(3, len(output[3].reduced_column_list))
        self.assertEqual(set(output[3].reduced_column_list).issubset(data.columns), False)
        self.assertEqual(1, len(output[4].reduced_column_list))
        self.assertEqual(set(output[4].reduced_column_list).issubset(data.columns), False)

    def test_excluded_columns(self):
        "exclude columns test"
        data = pd.concat([self.data] * 32)
        drop_cols = True

        feature_red = FeatureReduction()
        feature_red.set_null_feature_reduction_settings(0.9)
        feature_red.set_similar_distribution_feature_reduction_settings(5)
        feature_red.set_univariate_performance_feature_reduction_settings(0.6)

        output = feature_red.execute(data, drop_cols,
                                     included_reduction_types=[FeatureReductionType.UNIVARIATE_PERFORMANCE.name,
                                                               FeatureReductionType.NULL.name,
                                                               FeatureReductionType.SIMILAR_DISTRIBUTION.name,
                                                               FeatureReductionType.HIGH_CORRELATION.name,
                                                               FeatureReductionType.STABILITY.name,
                                                               ],
                                     target_type="BINARY", target_column_name="target",
                                     performance_metric="roc_auc",
                                     excluded_columns=["col2", "col6", "col50", "col3", "col9"])
        for rep in output:
            print(rep.feature_reduction_type.name + ":" + rep.reduced_column_list.__str__())

        self.assertEqual(0, len(output[0].reduced_column_list))
        self.assertEqual(set(output[0].reduced_column_list).issubset(data.columns), True)
        self.assertEqual(0, len(output[1].reduced_column_list))
        self.assertEqual(set(output[1].reduced_column_list).issubset(data.columns), True)
        self.assertEqual(0, len(output[2].reduced_column_list))
        self.assertEqual(set(output[2].reduced_column_list).issubset(data.columns), True)
        self.assertEqual(1, len(output[3].reduced_column_list))
        self.assertEqual(set(output[3].reduced_column_list).issubset(data.columns), False)
        self.assertEqual(1, len(output[4].reduced_column_list))
        self.assertEqual(set(output[4].reduced_column_list).issubset(data.columns), False)


if __name__ == '__main__':
    unittest.main()
