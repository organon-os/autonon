"""This module includes MulticlassReporterServiceConsoleTests class."""
import unittest
import numpy as np
import pandas as pd
from organon.ml.reporting.domain.services.multiclass_reporter_service import MulticlassReporterService
from organon.ml.reporting.settings.objects.multiclass_reporter_settings import MultiClassReporterSettings


class MulticlassReporterServiceConsoleTests(unittest.TestCase):
    """Console Tests MulticlassReporterService class."""

    def test_with_probabilities(self):
        """tests multiclass reporting on numerical data when target is given with millions of data"""
        data = pd.DataFrame(
            {"target": [1, 0, 2, 1, 2, 1]})
        # 1.8M data
        data = pd.concat([data] * 300000)
        probability_values = np.array(
            [[0.1, 0.8, 0.7], [0.1, 0.2, 0.4], [0.9, 0.5, 0.7], [0.4, 0.5, 0.7], [0.1, 0.8, 0.7],
             [0.1, 0.9, 0.7]])
        # 1.8M probability_values
        probability_values = np.repeat(probability_values, 300000, axis=0)
        ordered_cls_names = [0, 1, 2]
        service = MulticlassReporterService()
        settings = MultiClassReporterSettings(data, "target", "predicted", None, None, None, 1, probability_values,
                                              ordered_class_names=ordered_cls_names)
        output = service.execute(settings) # takes around 10 secs
        self.assertIsNotNone(output.scores)
        self.assertIsNotNone(output.confusion_matrices)
        pd.testing.assert_frame_equal(output.scores,
                                      pd.DataFrame({'Accuracy': [0.3888888888888889], 'Roc-Auc Score': [0.5]},
                                                   index=[0]))
