"""Object Classification Console Test"""

import unittest

import tensorflow_datasets as tfds

from organon.ml.object_classification.services.object_classifier import ObjectClassifier


class ObjectClassificationConsoleTests(unittest.TestCase):
    """Object Classification Test"""

    def test_predict_binary_unlabeled_dir(self):
        """fit test"""

        classifier = ObjectClassifier(
            train_data_dir=r"C:\Users\mertcem\Desktop\dosyalar\predict", batch_size=1)
        classifier.set_fine_tuning_settings(epoch=2)
        classifier.set_transfer_learning_settings(epoch=2, optimizer="sgd")
        classifier.fit()
        pred = classifier.predict(data=r"C:\Users\mertcem\Desktop\dosyalar\test_pred")
        print(pred)

    def test_evaluate_binary_labeled_ds(self):
        """fit test"""

        classifier = ObjectClassifier(
            train_data_dir=r"C:\Users\mertcem\Desktop\predict", batch_size=1)
        classifier.set_transfer_learning_settings(epoch=2)
        classifier.set_fine_tuning_settings(epoch=2)
        classifier.set_data_augmentation_settings()
        classifier.fit()
        ds = tfds.load('cats_vs_dogs', split='train[:1%]', as_supervised=True)
        pred, df = classifier.evaluate(data=ds)
        print(pred)
        print(df)

    def test_evaluate_binary_labeled_dir(self):
        """fit test"""

        classifier = ObjectClassifier(
            train_data_dir=r"C:\Users\mertcem\Desktop\dosyalar\predict", batch_size=1)
        classifier.set_transfer_learning_settings(epoch=2)
        classifier.set_fine_tuning_settings(epoch=2)
        classifier.set_data_augmentation_settings()
        classifier.fit()
        pred, df = classifier.evaluate(data=r"C:\Users\mertcem\Desktop\dosyalar\predict", average="weighted")
        print(pred)
        print(df)

    def test_predict_multiclass_labeled_ds(self):
        classifier = ObjectClassifier(
            train_data_dir=r"C:\Users\mertcem\Desktop\multi_test", batch_size=1)
        classifier.set_transfer_learning_settings(epoch=2)
        classifier.set_fine_tuning_settings(epoch=2)
        classifier.set_data_augmentation_settings()
        classifier.fit()
        ds = tfds.load('cifar10', split='train[:1%]', as_supervised=True)
        eval, df = classifier.evaluate(ds)
        print(eval, df)


    def test_evaluate_multiclass_labeled_dir(self):
        classifier = ObjectClassifier(
            train_data_dir=r"C:\Users\mertcem\Desktop\dosyalar\multi_test", batch_size=1)
        classifier.set_transfer_learning_settings(epoch=2)
        classifier.set_fine_tuning_settings(epoch=2)
        classifier.set_data_augmentation_settings()
        classifier.fit()
        eval, df = classifier.evaluate(data=r"C:\Users\mertcem\Desktop\dosyalar\multi_test")
        print(eval, df)


    def test_predict_multiclass_unlabeled_dir(self):
        classifier = ObjectClassifier(
            train_data_dir=r"C:\Users\mertcem\Desktop\dosyalar\multi_test", batch_size=1)
        classifier.set_transfer_learning_settings(epoch=2)
        classifier.fit()
        prob = classifier.predict_proba(data=r"C:\Users\mertcem\Desktop\dosyalar\test_pred")
        pred = classifier.predict(data=r"C:\Users\mertcem\Desktop\dosyalar\test_pred")
        print(prob)
        print(pred)


    def test_predict_preprocess(self):
        classifier = ObjectClassifier(
            train_data_dir=r"C:\Users\mertcem\Desktop\multi_test", batch_size=1)
        classifier.set_transfer_learning_settings(epoch=2)
        classifier.fit()
        data = classifier.get_processed_data(r"C:\Users\mertcem\Desktop\test_pred")
        pred = classifier.predict(data=data, is_processed=True)
        print(pred)


if __name__ == '__main__':
    unittest.main()
