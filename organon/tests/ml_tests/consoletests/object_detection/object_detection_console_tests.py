"""console tests for Object Detection Module"""
import unittest

from organon.ml.object_detection.services.object_detection_trainer import ObjectDetectionTrainer
from organon.ml.object_detection.services.object_detector import ObjectDetector


class ObjectDetectionConsoleTests(unittest.TestCase):
    """Object Detection Console Test class"""
    def test_train_model(self):
        """train a pretrained model using new yaml and datafiles"""
        trainer = ObjectDetectionTrainer(pretrained_model_path="C:/GIT/detectiontest/yolov8n.pt",
                                         yaml_data_dir="C:/GIT/detectiontest/dataset.yaml", epochs=3)
        trainer.train(sampling=True, sampling_ratio=0.7)
        self.assertFalse(trainer.is_trained_on_full_data)

    def test_predict_on_pretrained_model_woutm(self):
        """test train"""
        detector = ObjectDetector(pretrained_model_path=r"C:\GIT\detectiontest\train12\weights\best.pt")
        print(detector.get_all_object_names())
        detection_info = detector.detect(r"C:\GIT\detectiontest\to_predict", objects_to_detect=["without mask"])
        self.assertIsNotNone(detection_info[0].confidence)
        self.assertIsNotNone(detection_info[0].box_points)
        self.assertIsNotNone(detection_info[0].object_name)

    def test_predict_on_pretrained_model_wm(self):
        """test train"""
        detector = ObjectDetector(pretrained_model_path=r"C:\GIT\detectiontest\train12\weights\best.pt")
        print(detector.get_all_object_names())
        detection_info = detector.detect(r"C:\GIT\detectiontest\to_predict", objects_to_detect=["with mask"])
        self.assertIsNotNone(detection_info[0].confidence)
        self.assertIsNotNone(detection_info[0].box_points)
        self.assertIsNotNone(detection_info[0].object_name)

    def test_predict_on_pretrained_model_single_img(self):
        """test train"""
        detector = ObjectDetector(pretrained_model_path=r"C:\GIT\detectiontest\train12\weights\best.pt")
        print(detector.get_all_object_names())
        detection_info = detector.detect(r"C:\GIT\detectiontest\to_predict\unmasked (1487).jpg",
                                         output_folder=r"C:\GIT")
        self.assertIsNotNone(detection_info[0].confidence)
        self.assertIsNotNone(detection_info[0].box_points)
        self.assertIsNotNone(detection_info[0].object_name)

    def test_detect_on_pretrained_model(self):
        """detect"""

        detector = ObjectDetector.from_custom_model("C:/GIT/detectiontest/yolov8n.pt")
        detections = detector.detect("C:/GIT/detectiontest/predict")
        x_detected = detections[0]
        self.assertIsNotNone(x_detected.object_name)
        self.assertIsNotNone(x_detected.confidence)
        self.assertIsNotNone(x_detected.box_points)
