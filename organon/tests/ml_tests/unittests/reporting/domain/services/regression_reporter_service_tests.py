"""This module includes RegressionReporterServiceTestCase class"""
import unittest
from unittest.mock import patch

import numpy as np
import pandas as pd

from organon.ml.reporting.domain.services import regression_reporter_service
from organon.ml.reporting.domain.services.regression_reporter_service import RegressionReporterService
from organon.ml.reporting.settings.objects.reporter_settings import ReporterSettings


class RegressionReporterServiceTestCase(unittest.TestCase):
    """Tests RegressionReporterService class. """

    def setUp(self) -> None:
        self.data = pd.DataFrame(
            {"target": [1, 2, 2, 2, 2, 1, 1, 1],
             "predicted": [1, 2, 1, 2, 2, 2, 1, 1],
             "split": ["Train", "Train", "Train", "Train", "Val", "Val", "Val", "Val"],
             "idstr": [4, 3, 4, 3, 3, 4, 4, 3]})
        self.service = RegressionReporterService()

    @patch(regression_reporter_service.__name__ + ".get_bins")
    def test_both_idstr_split(self, mock_get_bins):
        """tests execute when idstr and split columns are given"""
        mock_get_bins.side_effect = [pd.Series(data=[0, 1], index=[1, 3], dtype=np.int8),
                                     pd.Series(data=[1, 0], index=[4, 7], dtype=np.int8),
                                     pd.Series(data=[0, 1], index=[0, 2], dtype=np.int8),
                                     pd.Series(data=[1, 0], index=[5, 6], dtype=np.int8),
                                     pd.Series(data=[0, 1, 1, 0], index=[1, 3, 4, 7], dtype=np.int8),
                                     pd.Series(data=[0, 0, 1, 1], index=[0, 2, 5, 6], dtype=np.int8),
                                     pd.Series(data=[0, 1, 0, 1], index=[0, 1, 2, 3], dtype=np.int8),
                                     pd.Series(data=[1, 1, 0, 0], index=[4, 5, 6, 7], dtype=np.int8),
                                     pd.Series(data=[0, 1, 0, 1, 1, 1, 0, 0], dtype=np.int8)]
        settings = ReporterSettings(self.data, "target", "predicted", None, "idstr", "split", 2)
        output = self.service.execute(settings)
        pd.testing.assert_frame_equal(output.performance_summary, pd.DataFrame(
            data=[[3, "Train", 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 2.0, 2.0, 2.0, 0.0, 2.0, 2.0, 2.0, 2.0, 2.0],
                  [3, "Val", 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 2.0, 1.5, 1.5, 0.7071067811865476, 1.0, 1.25, 1.5, 1.75,
                   2.0],
                  [4, "Train", 0.5, 0.7071067811865476, 0.5, 25.0, 0.08220097694658277, -1.0, 2.0, 1.5, 1.0, 0.0, 1.0,
                   1.0, 1.0, 1.0, 1.0],
                  [4, "Val", 0.5, 0.7071067811865476, 0.5, 50.0, 0.08220097694658277, 0.0, 2.0, 1.0, 1.5,
                   0.7071067811865476, 1.0, 1.25, 1.5, 1.75, 2.0]],
            columns=['IdStr', 'Data', 'MSE', 'RMSE', 'MAA', 'MAPE', 'MSLOGE', 'R2', 'ROW COUNT', 'TARGET AVG',
                     'SCORE AVG', 'SCORE STD', 'SCORE MIN', 'SCORE P25', 'SCORE MEDIAN', 'SCORE P75',
                     'SCORE MAX']), check_dtype=False)
        pd.testing.assert_frame_equal(output.lift_table, pd.DataFrame(
            data=[[3, 'Train', 0, 1, 2.0, 2.0, 1.0],
                  [3, 'Train', 1, 1, 2.0, 2.0, 1.0],
                  [3, 'Val', 0, 1, 1.0, 1.0, 1.0],
                  [3, 'Val', 1, 1, 2.0, 2.0, 1.3333333333333333],
                  [4, 'Train', 0, 1, 1.0, 1.0, 1.0],
                  [4, 'Train', 1, 1, 2.0, 1.0, 1.3333333333333333],
                  [4, 'Val', 0, 1, 1.0, 1.0, 1.0],
                  [4, 'Val', 1, 1, 1.0, 2.0, 1.0]],
            columns=['IdStr', 'Data', 'Bin', 'Total Count', 'Target Average',
                     'Score Average', 'Cumulative Lift']), check_dtype=False)

    @patch(regression_reporter_service.__name__ + ".get_bins")
    def test_with_idstr(self, mock_get_bins):
        """tests execute when idstr column is given"""
        mock_get_bins.side_effect = [pd.Series(data=[0, 1, 1, 0], index=[1, 3, 4, 7], dtype=np.int8),
                                     pd.Series(data=[0, 0, 1, 1], index=[0, 2, 5, 6], dtype=np.int8)]
        settings = ReporterSettings(self.data, "target", "predicted", None, "idstr", None, 2)
        output = self.service.execute(settings)
        pd.testing.assert_frame_equal(output.performance_summary, pd.DataFrame(
            data=[[3, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 4.0, 1.75, 1.75, 0.5, 1.0, 1.75, 2.0, 2.0, 2.0],
                  [4, 0.5, 0.7071067811865476, 0.5, 37.5, 0.08220097694658277, -1.6666666666666665, 4.0, 1.25, 1.25,
                   0.5, 1.0, 1.0, 1.0, 1.25, 2.0]],
            columns=["IdStr", 'MSE', 'RMSE', 'MAA', 'MAPE', 'MSLOGE', 'R2', 'ROW COUNT', 'TARGET AVG', 'SCORE AVG',
                     'SCORE STD', 'SCORE MIN', 'SCORE P25', 'SCORE MEDIAN', 'SCORE P75',
                     'SCORE MAX']))
        pd.testing.assert_frame_equal(output.lift_table, pd.DataFrame(
            data=[[3, 0, 2, 1.5, 1.5, 1.0],
                  [3, 1, 2, 2.0, 2.0, 1.1428571428571428],
                  [4, 0, 2, 1.5, 1.0, 1.0],
                  [4, 1, 2, 1.0, 1.5, 0.8]],
            columns=['IdStr', 'Bin', 'Total Count', 'Target Average', 'Score Average',
                     'Cumulative Lift']), check_dtype=False)

    @patch(regression_reporter_service.__name__ + ".get_bins")
    def test_with_split(self, mock_get_bins):
        """tests execute when split column is given"""
        mock_get_bins.side_effect = [pd.Series(data=[0, 1, 0, 1], dtype=np.int8),
                                     pd.Series(data=[1, 1, 0, 0], index=[4, 5, 6, 7], dtype=np.int8)]
        settings = ReporterSettings(self.data, "target", "predicted", None, None, "split", 2)
        output = self.service.execute(settings)
        pd.testing.assert_frame_equal(output.lift_table, pd.DataFrame(
            data=[['Train', 0, 2, 1.5, 1.0, 1.0],
                  ['Train', 1, 2, 2.0, 2.0, 1.1428571428571428],
                  ['Val', 0, 2, 1.0, 1.0, 1.0],
                  ['Val', 1, 2, 1.5, 2.0, 1.2]],
            columns=['Data', 'Bin', 'Total Count', 'Target Average', 'Score Average',
                     'Cumulative Lift']), check_dtype=False)
        pd.testing.assert_frame_equal(output.performance_summary, pd.DataFrame(
            data=[["Train", 0.25, 0.5, 0.25, 12.5, 0.04110048847329138, -0.33333333333333326, 4.0, 1.75, 1.5,
                   0.5773502691896257, 1.0,
                   1.0, 1.5, 2.0, 2.0],
                  ["Val", 0.25, 0.5, 0.25, 25.0, 0.04110048847329138, -0.33333333333333326, 4.0, 1.25, 1.5,
                   0.5773502691896257, 1.0,
                   1.0, 1.5, 2.0, 2.0]],
            columns=["Data", 'MSE', 'RMSE', 'MAA', 'MAPE', 'MSLOGE', 'R2', 'ROW COUNT', 'TARGET AVG', 'SCORE AVG',
                     'SCORE STD', 'SCORE MIN', 'SCORE P25', 'SCORE MEDIAN', 'SCORE P75',
                     'SCORE MAX']))

    @patch(regression_reporter_service.__name__ + ".get_bins")
    def test_without_split_idstr(self, mock_get_bins):
        """tests execute when none of idstr and split columns is given"""
        mock_get_bins.side_effect = [pd.Series(data=[0, 1, 0, 1, 1, 1, 0, 0], dtype=np.int8)]
        settings = ReporterSettings(self.data, "target", "predicted", None, None, None, 2)
        output = self.service.execute(settings)
        pd.testing.assert_frame_equal(output.lift_table, pd.DataFrame(
            data=[[0, 4, 1.25, 1.0, 1.0],
                  [1, 4, 1.75, 2.0, 1.1666666666666667]],
            columns=['Bin', 'Total Count', 'Target Average', 'Score Average',
                     'Cumulative Lift']))
        pd.testing.assert_frame_equal(output.performance_summary, pd.DataFrame(
            data=[[0.25, 0.5, 0.25, 18.75, 0.04110048847329138, 0.0, 8.0, 1.5, 1.5, 0.5345224838248488, 1.0, 1.0, 1.5,
                   2.0, 2.0]],
            columns=['MSE', 'RMSE', 'MAA', 'MAPE', 'MSLOGE', 'R2', 'ROW COUNT', 'TARGET AVG', 'SCORE AVG',
                     'SCORE STD', 'SCORE MIN', 'SCORE P25', 'SCORE MEDIAN', 'SCORE P75',
                     'SCORE MAX']))


if __name__ == '__main__':
    unittest.main()
