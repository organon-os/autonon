"""Includes unittests for BinaryReporterService"""
import unittest

import numpy as np
import pandas as pd

from organon.ml.reporting.domain.services import binary_reporter_service
from organon.ml.reporting.domain.services.binary_reporter_service import BinaryReporterService
from organon.ml.reporting.settings.objects.reporter_settings import ReporterSettings
from organon.tests import test_helper


class BinaryReporterServiceTestCase(unittest.TestCase):
    """Unittest class for BinaryReporterService"""

    def setUp(self) -> None:
        self.mock_get_bins = test_helper.get_mock(self, binary_reporter_service.__name__ + ".get_bins")

    def test_execute(self):
        """tests execution with no split and no idstr column"""
        data = pd.DataFrame({"target": [1, 0, 0, 1, 1], "score": [0.8, 0.6, 0.2, 0.9, 0.4]})
        self.mock_get_bins.return_value = pd.Series([1, 0, 0, 1, 0])
        service = BinaryReporterService()
        settings = ReporterSettings(data, "target", "score", None, None, None, num_bins=2)
        output = service.execute(settings)
        performance_summary = output.performance_summary.T
        self._assert_performance_summary_df(performance_summary, 0.833, 5, 3, 2, 0.6, 0.4, 1.5, 0.58,
                                            0.28635, 0.2, 0.4, 0.6, 0.8, 0.9)
        self.assertEqual(2, len(output.detailed_performance_summary))

        self._assert_detailed_performance_summary_df(
            output.detailed_performance_summary[output.detailed_performance_summary["BIN"] == 0], _bin=0, pos_count=1,
            neg_count=2, total_count=3,
            target_ratio=1 / 3, score_avg=1.2 / 3, tp_count=3, fp_count=2, tn_count=0, fn_count=0, tp_rate=1,
            fp_rate=1,
            precision=3 / 5, accuracy=3 / 5, f1_score=0.75, lift=1.0
        )
        self._assert_detailed_performance_summary_df(
            output.detailed_performance_summary[output.detailed_performance_summary["BIN"] == 1], _bin=1, pos_count=2,
            neg_count=0, total_count=2,
            target_ratio=2 / 2, score_avg=1.7 / 2, tp_count=2, fp_count=0, tn_count=2, fn_count=1, tp_rate=2 / 3,
            fp_rate=0, precision=1, accuracy=4 / 5, f1_score=0.8, lift=5 / 3
        )

        self._assert_target_based_performance_df(
            output.target_based_performance_summary.T, pos_count=3, pos_std=0.265, pos_min=0.4, pos_mean=0.7,
            pos_percentiles=pd.Series([0.4, 0.8, 0.9]).quantile([i * 0.05 for i in range(1, 20)]).to_list(),
            pos_max=0.9, neg_count=2, neg_std=0.283, neg_min=0.2, neg_mean=0.4,
            neg_percentiles=pd.Series([0.2, 0.6]).quantile([i * 0.05 for i in range(1, 20)]).to_list(), neg_max=0.6, )

    def test_execute_with_idstr_col(self):
        """tests execution with idstr column"""
        data = pd.DataFrame({"target": [1, 0, 0, 1, 1, 0, 0, 1, 1, 0],
                             "score": [0.8, 0.6, 0.2, 0.9, 0.4, 0.3, 0.2, 0.9, 0.8, 0.7],
                             "idstr": [0, 1, 1, 1, 0, 1, 0, 0, 1, 0]})
        self.mock_get_bins.side_effect = [pd.Series([1, 0, 0, 1, 0], index=[0, 4, 6, 7, 9]),
                                          pd.Series([0, 0, 1, 0, 1], index=[1, 2, 3, 5, 8])
                                          ]
        service = BinaryReporterService()
        settings = ReporterSettings(data, "target", "score", None, "idstr", None, num_bins=2)
        output = service.execute(settings)
        performance_summary = output.performance_summary.T
        target_based_perf_summary = output.target_based_performance_summary.T
        self.assertEqual(2, len(performance_summary))
        self.assertEqual(4, len(output.detailed_performance_summary))
        self.assertEqual(2, len(target_based_perf_summary))

        self._assert_performance_summary_df(performance_summary[performance_summary["ID_STR"] == 0],
                                            0.833, 5, 3, 2, 0.6, 0.4, 1.5, 0.6,
                                            0.292, 0.2, 0.4, 0.7, 0.8, 0.9, id_str=0)
        self._assert_performance_summary_df(performance_summary[performance_summary["ID_STR"] == 1],
                                            1, 5, 2, 3, 0.4, 0.6, 2 / 3,
                                            2.8 / 5, 0.305, 0.2, 0.3, 0.6, 0.8, 0.9, id_str=1)

        self._assert_detailed_performance_summary_df(
            output.detailed_performance_summary[
                (output.detailed_performance_summary["ID_STR"] == 0) &
                (output.detailed_performance_summary["BIN"] == 0)],
            _bin=0, pos_count=1,
            neg_count=2, total_count=3,
            target_ratio=1 / 3, score_avg=1.3 / 3, tp_count=3, fp_count=2, tn_count=0, fn_count=0, tp_rate=1,
            fp_rate=1,
            precision=3 / 5, accuracy=0.6, f1_score=0.75, lift=1.0, id_str=0
        )
        self._assert_detailed_performance_summary_df(
            output.detailed_performance_summary[
                (output.detailed_performance_summary["ID_STR"] == 0) &
                (output.detailed_performance_summary["BIN"] == 1)],
            _bin=1, pos_count=2,
            neg_count=0, total_count=2,
            target_ratio=2 / 2, score_avg=1.7 / 2, tp_count=2, fp_count=0, tn_count=2, fn_count=1, tp_rate=2 / 3,
            fp_rate=0, precision=1, accuracy=0.8, f1_score=0.8, lift=5 / 3, id_str=0
        )

        self._assert_detailed_performance_summary_df(
            output.detailed_performance_summary[
                (output.detailed_performance_summary["ID_STR"] == 1) &
                (output.detailed_performance_summary["BIN"] == 0)],
            _bin=0, pos_count=0,
            neg_count=3, total_count=3,
            target_ratio=0, score_avg=1.1 / 3, tp_count=2, fp_count=3, tn_count=0, fn_count=0, tp_rate=1,
            fp_rate=1, precision=2 / 5, accuracy=2/5, f1_score=4 / 7, lift=1.0, id_str=1
        )
        self._assert_detailed_performance_summary_df(
            output.detailed_performance_summary[
                (output.detailed_performance_summary["ID_STR"] == 1) &
                (output.detailed_performance_summary["BIN"] == 1)],
            _bin=1, pos_count=2,
            neg_count=0, total_count=2,
            target_ratio=2 / 2, score_avg=1.7 / 2, tp_count=2, fp_count=0, tn_count=3, fn_count=0, tp_rate=1,
            fp_rate=0, precision=1, accuracy=1, f1_score=1, lift=5 / 2, id_str=1
        )
        self._assert_target_based_performance_df(
            target_based_perf_summary[target_based_perf_summary["ID_STR"] == 0],
            pos_count=3, pos_std=0.265, pos_min=0.4, pos_mean=0.7,
            pos_percentiles=pd.Series([0.4, 0.8, 0.9]).quantile([i * 0.05 for i in range(1, 20)]).to_list(),
            pos_max=0.9, neg_count=2, neg_std=0.354, neg_min=0.2, neg_mean=0.45,
            neg_percentiles=pd.Series([0.2, 0.7]).quantile([i * 0.05 for i in range(1, 20)]).to_list(), neg_max=0.7,
            id_str=0)
        self._assert_target_based_performance_df(
            target_based_perf_summary[target_based_perf_summary["ID_STR"] == 1],
            pos_count=2, pos_std=0.07, pos_min=0.8, pos_mean=0.85,
            pos_percentiles=pd.Series([0.8, 0.9]).quantile([i * 0.05 for i in range(1, 20)]).to_list(),
            pos_max=0.9, neg_count=3, neg_std=0.208, neg_min=0.2, neg_mean=0.367,
            neg_percentiles=pd.Series([0.2, 0.3, 0.6]).quantile([i * 0.05 for i in range(1, 20)]).to_list(),
            neg_max=0.6, id_str=1)

    def test_execute_with_idstr_and_split_col(self):
        """tests execution with split and idstr column"""
        data = pd.DataFrame({"target": [0, 1, 0, 1, 1, 0, 0, 1, 1, 0],
                             "score": [0.8, 0.6, 0.2, 0.9, 0.4, 0.3, 0.2, 0.9, 0.8, 0.7],
                             "idstr": [0, 1, 1, 1, 0, 1, 0, 0, 1, 0],
                             "split_col": ["train", "train", "train", "test", "train", "test", "test", "test", "test",
                                           "test"]
                             })
        self.mock_get_bins.side_effect = [pd.Series([0, 1, 0], index=[6, 7, 9]),
                                          pd.Series([1, 0], index=[0, 4]),
                                          pd.Series([1, 0, 0], index=[3, 5, 8]),
                                          pd.Series([1, 0], index=[1, 2])]
        service = BinaryReporterService()
        settings = ReporterSettings(data, "target", "score", None, "idstr", "split_col", num_bins=2)
        output = service.execute(settings)

        performance_summary = output.performance_summary.T
        target_based_perf_summary = output.target_based_performance_summary.T

        self.assertEqual(4, len(performance_summary))
        self.assertEqual(8, len(output.detailed_performance_summary))
        self.assertEqual(4, len(target_based_perf_summary))

        self._assert_performance_summary_df(
            performance_summary[(performance_summary["ID_STR"] == 0) &
                                (performance_summary["SPLIT"] == "train")],
            0, 2, 1, 1, 0.5, 0.5, 1.0, 0.6, 0.283, 0.4, 0.5, 0.6, 0.7, 0.8, id_str=0, split_val="train")
        self._assert_performance_summary_df(
            performance_summary[(performance_summary["ID_STR"] == 0) &
                                (performance_summary["SPLIT"] == "test")],
            1, 3, 1, 2, 1 / 3, 2 / 3, 1 / 2, 0.6, 0.361, 0.2, 0.45, 0.7, 0.8, 0.9, id_str=0, split_val="test")
        self._assert_performance_summary_df(
            performance_summary[(performance_summary["ID_STR"] == 1) &
                                (performance_summary["SPLIT"] == "train")],
            1, 2, 1, 1, 1 / 2, 1 / 2, 1, 0.4, 0.283, 0.2, 0.3, 0.4, 0.5, 0.6, id_str=1, split_val="train")
        self._assert_performance_summary_df(
            performance_summary[(performance_summary["ID_STR"] == 1) &
                                (performance_summary["SPLIT"] == "test")],
            1, 3, 2, 1, 2 / 3, 1 / 3, 2, 2.0 / 3, 0.321, 0.3, 0.55, 0.8, 0.85, 0.9, id_str=1, split_val="test")

        self._assert_detailed_performance_summary_df(
            output.detailed_performance_summary[
                (output.detailed_performance_summary["ID_STR"] == 0) &
                (output.detailed_performance_summary["BIN"] == 0) &
                (output.detailed_performance_summary["SPLIT"] == "train")
                ],
            _bin=0, pos_count=1,
            neg_count=0, total_count=1,
            target_ratio=1, score_avg=0.4, tp_count=1, fp_count=1, tn_count=0, fn_count=0, tp_rate=1,
            fp_rate=1, precision=1 / 2, accuracy=1 / 2, f1_score=2 / 3, lift=1.0, id_str=0, split_val="train"
        )
        self._assert_detailed_performance_summary_df(
            output.detailed_performance_summary[
                (output.detailed_performance_summary["ID_STR"] == 0) &
                (output.detailed_performance_summary["BIN"] == 1) &
                (output.detailed_performance_summary["SPLIT"] == "train")
                ],
            _bin=1, pos_count=0,
            neg_count=1, total_count=1,
            target_ratio=0, score_avg=0.8, tp_count=0, fp_count=1, tn_count=0, fn_count=1, tp_rate=0,
            fp_rate=1, precision=0, accuracy=0, f1_score=0, lift=0, id_str=0, split_val="train"
        )
        self._assert_detailed_performance_summary_df(
            output.detailed_performance_summary[
                (output.detailed_performance_summary["ID_STR"] == 0) &
                (output.detailed_performance_summary["BIN"] == 0) &
                (output.detailed_performance_summary["SPLIT"] == "test")
                ],
            _bin=0, pos_count=0,
            neg_count=2, total_count=2,
            target_ratio=0, score_avg=0.9 / 2, tp_count=1, fp_count=2, tn_count=0, fn_count=0, tp_rate=1,
            fp_rate=1, precision=1/3, accuracy=1/3, f1_score=1/2, lift=1.0, id_str=0, split_val="test"
        )
        self._assert_detailed_performance_summary_df(
            output.detailed_performance_summary[
                (output.detailed_performance_summary["ID_STR"] == 0) &
                (output.detailed_performance_summary["BIN"] == 1) &
                (output.detailed_performance_summary["SPLIT"] == "test")
                ],
            _bin=1, pos_count=1,
            neg_count=0, total_count=1,
            target_ratio=1, score_avg=0.9, tp_count=1, fp_count=0, tn_count=2, fn_count=0, tp_rate=1,
            fp_rate=0, precision=1, accuracy=1, f1_score=1, lift=3.0, id_str=0, split_val="test"
        )
        self._assert_detailed_performance_summary_df(
            output.detailed_performance_summary[
                (output.detailed_performance_summary["ID_STR"] == 1) &
                (output.detailed_performance_summary["BIN"] == 0) &
                (output.detailed_performance_summary["SPLIT"] == "train")
                ],
            _bin=0, pos_count=0,
            neg_count=1, total_count=1,
            target_ratio=0, score_avg=0.2, tp_count=1, fp_count=1, tn_count=0, fn_count=0, tp_rate=1,
            fp_rate=1, precision=1 / 2, accuracy=1/2, f1_score=2 / 3, lift=1.0, id_str=1, split_val="train"
        )
        self._assert_detailed_performance_summary_df(
            output.detailed_performance_summary[
                (output.detailed_performance_summary["ID_STR"] == 1) &
                (output.detailed_performance_summary["BIN"] == 1) &
                (output.detailed_performance_summary["SPLIT"] == "train")
                ],
            _bin=1, pos_count=1,
            neg_count=0, total_count=1,
            target_ratio=1, score_avg=0.6, tp_count=1, fp_count=0, tn_count=1, fn_count=0, tp_rate=1,
            fp_rate=0, precision=1, accuracy=1, f1_score=1, lift=2.0, id_str=1, split_val="train"
        )
        self._assert_detailed_performance_summary_df(
            output.detailed_performance_summary[
                (output.detailed_performance_summary["ID_STR"] == 1) &
                (output.detailed_performance_summary["BIN"] == 0) &
                (output.detailed_performance_summary["SPLIT"] == "test")
                ],
            _bin=0, pos_count=1,
            neg_count=1, total_count=2,
            target_ratio=1 / 2, score_avg=1.1 / 2, tp_count=2, fp_count=1, tn_count=0, fn_count=0, tp_rate=1,
            fp_rate=1, precision=2 / 3, accuracy=2/3, f1_score=0.8, lift=1.0, id_str=1, split_val="test"
        )
        self._assert_detailed_performance_summary_df(
            output.detailed_performance_summary[
                (output.detailed_performance_summary["ID_STR"] == 1) &
                (output.detailed_performance_summary["BIN"] == 1) &
                (output.detailed_performance_summary["SPLIT"] == "test")
                ],
            _bin=1, pos_count=1,
            neg_count=0, total_count=1,
            target_ratio=1, score_avg=0.9, tp_count=1, fp_count=0, tn_count=1, fn_count=1, tp_rate=1 / 2,
            fp_rate=0, precision=1, accuracy=2 / 3, f1_score=2 / 3, lift=3 / 2, id_str=1, split_val="test"
        )
        self._assert_target_based_performance_df(
            target_based_perf_summary[
                (target_based_perf_summary["ID_STR"] == 0) &
                (target_based_perf_summary["SPLIT"] == "train")
                ],
            pos_count=1, pos_std=float("nan"), pos_min=0.4, pos_mean=0.4,
            pos_percentiles=[0.4 for _ in range(1, 20)],
            pos_max=0.4, neg_count=1, neg_std=float("nan"), neg_min=0.8, neg_mean=0.8,
            neg_percentiles=[0.8 for _ in range(1, 20)], neg_max=0.8,
            id_str=0, split_val="train")

        self._assert_target_based_performance_df(
            target_based_perf_summary[
                (target_based_perf_summary["ID_STR"] == 0) &
                (target_based_perf_summary["SPLIT"] == "test")
                ],
            pos_count=1, pos_std=float("nan"), pos_min=0.9, pos_mean=0.9,
            pos_percentiles=[0.9 for _ in range(1, 20)],
            pos_max=0.9, neg_count=2, neg_std=pd.Series([0.2, 0.7]).std(), neg_min=0.2, neg_mean=0.45,
            neg_percentiles=pd.Series([0.2, 0.7]).quantile([0.05 * i for i in range(1, 20)]).to_list(), neg_max=0.7,
            id_str=0, split_val="test")

        self._assert_target_based_performance_df(
            target_based_perf_summary[
                (target_based_perf_summary["ID_STR"] == 1) &
                (target_based_perf_summary["SPLIT"] == "train")
                ],
            pos_count=1, pos_std=float("nan"), pos_min=0.6, pos_mean=0.6,
            pos_percentiles=[0.6 for _ in range(1, 20)],
            pos_max=0.6, neg_count=1, neg_std=float("nan"), neg_min=0.2, neg_mean=0.2,
            neg_percentiles=[0.2 for _ in range(1, 20)], neg_max=0.2,
            id_str=1, split_val="train")

        self._assert_target_based_performance_df(
            target_based_perf_summary[
                (target_based_perf_summary["ID_STR"] == 1) &
                (target_based_perf_summary["SPLIT"] == "test")
                ],
            pos_count=2, pos_std=pd.Series([0.8, 0.9]).std(), pos_min=0.8, pos_mean=0.85,
            pos_percentiles=pd.Series([0.8, 0.9]).quantile([0.05 * i for i in range(1, 20)]).to_list(),
            pos_max=0.9, neg_count=1, neg_std=float("nan"), neg_min=0.3, neg_mean=0.3,
            neg_percentiles=[0.3 for _ in range(1, 20)], neg_max=0.3,
            id_str=1, split_val="test")

    def test_execute_num_bins_too_big(self):
        """tests error when num_bins is greater than row count"""
        data = pd.DataFrame({"target": [1, 0, 0, 1, 1], "score": [0.8, 0.6, 0.2, 0.9, 0.4]})
        service = BinaryReporterService()
        settings = ReporterSettings(data, "target", "score", None, None, None, num_bins=10)
        with self.assertRaisesRegex(ValueError, "num_bins cannot be higher than number of rows in a set"):
            service.execute(settings)

    def _assert_performance_summary_df(self, data_frame, auc, row_count, pos_count, neg_count, pos_ratio, neg_ratio,
                                       odds, score_avg, score_std, score_min, score_p25, score_median, score_p75,
                                       score_max, id_str=None, split_val=None):
        # pylint: disable=too-many-arguments
        expected_perf_sum = {}
        if id_str is not None:
            self.assertEqual(id_str, data_frame.iloc[0]["ID_STR"])
        if split_val is not None:
            self.assertEqual(split_val, data_frame.iloc[0]["SPLIT"])

        expected_perf_sum.update({
            "AUC": auc,
            "ROW COUNT": row_count,
            "POSITIVE COUNT": pos_count,
            "NEGATIVE COUNT": neg_count,
            "POSITIVE RATIO": pos_ratio,
            "NEGATIVE RATIO": neg_ratio,
            "ODDS": odds,
            "SCORE AVG": score_avg,
            "SCORE STD": score_std,
            "SCORE MIN": score_min,
            "SCORE P25": score_p25,
            "SCORE MEDIAN": score_median,
            "SCORE P75": score_p75,
            "SCORE MAX": score_max,
        })
        numerical_cols = [col for col in data_frame.columns if col not in ["ID_STR", "SPLIT"]]
        np.testing.assert_almost_equal(pd.DataFrame([expected_perf_sum], columns=numerical_cols).values,
                                       data_frame[numerical_cols].values, 3)

    def _assert_detailed_performance_summary_df(self, data_frame, _bin, pos_count, neg_count, total_count, target_ratio,
                                                score_avg, tp_count, fp_count, tn_count, fn_count, tp_rate,
                                                fp_rate, precision, accuracy, f1_score, lift, id_str=None,
                                                split_val=None):
        # pylint: disable=too-many-arguments

        expected_perf_sum = {}
        if id_str is not None:
            self.assertEqual(id_str, data_frame.iloc[0]["ID_STR"])
        if split_val is not None:
            self.assertEqual(split_val, data_frame.iloc[0]["SPLIT"])

        expected_perf_sum.update({
            "BIN": _bin,
            "POSITIVE COUNT": pos_count,
            "NEGATIVE COUNT": neg_count,
            "TOTAL COUNT": total_count,
            "TARGET RATIO": target_ratio,
            "SCORE AVG": score_avg,
            "TRUE POSITIVE COUNT": tp_count,
            "FALSE POSITIVE COUNT": fp_count,
            "TRUE NEGATIVE COUNT": tn_count,
            "FALSE NEGATIVE COUNT": fn_count,
            "TRUE POSITIVE RATE": tp_rate,
            "FALSE POSITIVE RATE": fp_rate,
            "PRECISION": precision,
            "ACCURACY": accuracy,
            "F1 SCORE": f1_score,
            "CUMULATIVE LIFT": lift
        })
        numerical_cols = [col for col in data_frame.columns if col not in ["ID_STR", "SPLIT"]]
        np.testing.assert_almost_equal(pd.DataFrame([expected_perf_sum], columns=numerical_cols).values.astype(float),
                                       data_frame[numerical_cols].values.astype(float), 3)

    def _assert_target_based_performance_df(self, data_frame, pos_count, pos_std, pos_min, pos_mean,
                                            pos_percentiles, pos_max,
                                            neg_count, neg_std, neg_min, neg_mean, neg_percentiles, neg_max,
                                            id_str=None,
                                            split_val=None):
        # pylint: disable=too-many-arguments
        expected_perf_sum = {}
        if id_str is not None:
            self.assertEqual(id_str, data_frame.iloc[0]["ID_STR"])
        if split_val is not None:
            self.assertEqual(split_val, data_frame.iloc[0]["SPLIT"])

        expected_perf_sum.update({
            "POSITIVES COUNT": pos_count,
            "POSITIVES STD": pos_std,
            "POSITIVES MIN": pos_min,
            "POSITIVES MEAN": pos_mean,
            "POSITIVES MAX": pos_max,
            "NEGATIVES COUNT": neg_count,
            "NEGATIVES STD": neg_std,
            "NEGATIVES MIN": neg_min,
            "NEGATIVES MEAN": neg_mean,
            "NEGATIVES MAX": neg_max,
        })
        for i in range(1, 20):
            expected_perf_sum[f"POSITIVES P{5 * i}"] = pos_percentiles[i - 1]
            expected_perf_sum[f"NEGATIVES P{5 * i}"] = neg_percentiles[i - 1]
        numerical_cols = [col for col in data_frame.columns if col not in ["ID_STR", "SPLIT"]]
        np.testing.assert_almost_equal(pd.DataFrame([expected_perf_sum], columns=numerical_cols).values.astype(float),
                                       data_frame[numerical_cols].values.astype(float), 3)


if __name__ == '__main__':
    unittest.main()
