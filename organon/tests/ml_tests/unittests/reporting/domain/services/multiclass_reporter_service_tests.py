"""This module includes MulticlassReporterServiceTestCase class."""
import unittest

import numpy as np
import pandas as pd

from organon.ml.reporting.domain.services.multiclass_reporter_service import MulticlassReporterService
from organon.ml.reporting.settings.objects.multiclass_reporter_settings import MultiClassReporterSettings


class MulticlassReporterServiceTestCase(unittest.TestCase):
    """Tests MulticlassReporterService class."""

    def test_execute_if_string_values_throws_error(self):
        """tests multiclass reporting on categorical data"""
        data = pd.DataFrame(
            {"target": ["happy", "sad", "angry", "happy", "happy"],
             "predicted": ["happy", "sad", "happy", "angry", "happy"]})
        service = MulticlassReporterService()
        settings = MultiClassReporterSettings(data, "target", "predicted", None, None, None, 1, None)
        with self.assertRaisesRegex(ValueError, "Target Column can only contain numeric values."):
            service.execute(settings)

    def test_execute_numerical(self):
        """tests multiclass reporting on categorical data"""
        data = pd.DataFrame(
            {"target": [0, 1, 2, 0, 1],
             "predicted": [0, 1, 2, 2, 0]})
        service = MulticlassReporterService()
        settings = MultiClassReporterSettings(data, "target", "predicted", None, None, None, 1, None)
        output = service.execute(settings)
        self.assertIsNotNone(output.scores)
        self.assertIsNotNone(output.confusion_matrices)
        pd.testing.assert_frame_equal(output.scores, pd.DataFrame({'Accuracy': [0.6], 'Roc-Auc Score': [None]}))
        pd.testing.assert_frame_equal(output.confusion_matrices[("ALL",)],
                                      pd.DataFrame({0: [1, 1, 0], 1: [0, 1, 0], 2: [1, 0, 1]},
                                                   index=[0, 1, 2]))
        cls_report_list = [(0.5, 0.5, 0.5, 2.0),
                           (1.0, 0.5, 0.6666666666666666, 2.0),
                           (0.5, 1.0, 0.6666666666666666, 1.0),
                           (0.6, 0.6, 0.6, 0.6),
                           (0.6666666666666666, 0.6666666666666666, 0.611111111111111, 5.0),
                           (0.7, 0.6, 0.5999999999999999, 5.0)]
        pd.testing.assert_frame_equal(output.classification_reports[("ALL",)],
                                      pd.DataFrame(data=cls_report_list,
                                                   columns=["precision", "recall", "f1-score", "support"],
                                                   index=["0", "1", "2", "accuracy", "macro avg", "weighted avg"]))

    def test_execute_class_shapes_mismatch(self):
        """tests multiclass reporting on categorical data"""
        data = pd.DataFrame(
            {"target": [0, 1, 2, 0, 0]})
        probability_values = np.array(
            [[0.1, 0.8, 0.7], [0.1, 0.2, 0.4], [0.9, 0.5, 0.7], [0.4, 0.5, 0.7], [0.1, 0.8, 0.7]])
        ordered_class_names = [0, 1, 2, 3]
        service = MulticlassReporterService()
        settings = MultiClassReporterSettings(data, "target", "predicted", None, None, None, 1,
                                              probability_values, ordered_class_names=ordered_class_names)
        with self.assertRaisesRegex(ValueError,
                                    "Given class names array shape mismatches with probability_values array."):
            service.execute(settings)

    def test_execute_one_unique_target_type(self):
        """tests multiclass reporting on categorical data"""
        data = pd.DataFrame(
            {"target": [0, 0, 0, 0, 0]})
        probability_values = np.array(
            [[0.1, 0.8, 0.7], [0.1, 0.2, 0.4], [0.9, 0.5, 0.7], [0.4, 0.5, 0.7], [0.1, 0.8, 0.7]])
        ordered_class_names = [0, 1, 2]
        service = MulticlassReporterService()
        settings = MultiClassReporterSettings(data, "target", "predicted", None, None, None, 1,
                                              probability_values, ordered_class_names=ordered_class_names)
        output = service.execute(settings)
        self.assertIsNotNone(output.scores)
        self.assertIsNotNone(output.classification_reports)
        self.assertIsNotNone(output.confusion_matrices)
        pd.testing.assert_frame_equal(output.scores, pd.DataFrame({'Accuracy': [0.2], 'Roc-Auc Score': [None]}))

    def test_execute_target_column_length_mismatch_with_probability_values(self):
        """tests when target column length and probability_values length mismatch throws a ValueError"""
        data = pd.DataFrame(
            {"target": [0, 0, 0, 0]})
        probability_values = np.array(
            [[0.1, 0.8, 0.7], [0.1, 0.2, 0.4], [0.9, 0.5, 0.7], [0.4, 0.5, 0.7], [0.1, 0.8, 0.7]])
        ordered_class_names = [0, 1, 2]
        service = MulticlassReporterService()
        settings = MultiClassReporterSettings(data, "target", "predicted", None, None, None, 1,
                                              probability_values, ordered_class_names=ordered_class_names)
        with self.assertRaisesRegex(ValueError, "Given probability values mismatches with target column's length."):
            service.execute(settings)

    def test_idstr_exists(self):
        """tests multiclass reporting on numerical data when idstr is given"""
        data = pd.DataFrame(
            {"target": [1, 0, 2, 1, 1],
             "predicted": [1, 0, 1, 2, 1],
             "id": [1, 1, 1, 2, 2]})
        service = MulticlassReporterService()
        settings = MultiClassReporterSettings(data, "target", "predicted", None, "id", None, 1, None)
        output = service.execute(settings)
        self.assertIsNotNone(output.scores)
        self.assertIsNotNone(output.confusion_matrices)
        pd.testing.assert_frame_equal(output.scores,
                                      pd.DataFrame(data=[[1, 0.666667, None], [2, 0.5, None]],
                                                   columns=["IdStr", "Accuracy", "Roc-Auc Score"]))
        pd.testing.assert_frame_equal(output.confusion_matrices[(1,)],
                                      pd.DataFrame({0: [1, 0, 0], 1: [0, 1, 1], 2: [0, 0, 0]},
                                                   index=[0, 1, 2]))
        pd.testing.assert_frame_equal(output.confusion_matrices[(2,)],
                                      pd.DataFrame({1: [1, 0], 2: [1, 0]},
                                                   index=[1, 2]))
        cls_report_1 = [[1.00000, 1.00000, 1.00000, 1.00000],
                        [0.50000, 1.00000, 0.66667, 1.00000],
                        [1.00000, 0.00000, 0.00000, 1.00000],
                        [0.66667, 0.66667, 0.66667, 0.66667],
                        [0.83333, 0.66667, 0.55556, 3.00000],
                        [0.83333, 0.66667, 0.55556, 3.00000]]
        pd.testing.assert_frame_equal(output.classification_reports[(1,)],
                                      pd.DataFrame(data=cls_report_1,
                                                   columns=["precision", "recall", "f1-score", "support"],
                                                   index=["0", "1", "2", "accuracy", "macro avg", "weighted avg"]))
        cls_report_2 = [[1.00000, 0.50000, 0.66667, 2.00000],
                        [0.00000, 1.00000, 0.00000, 0.00000],
                        [0.50000, 0.50000, 0.50000, 0.50000],
                        [0.50000, 0.75000, 0.33333, 2.00000],
                        [1.00000, 0.50000, 0.66667, 2.00000]]
        pd.testing.assert_frame_equal(output.classification_reports[(2,)],
                                      pd.DataFrame(data=cls_report_2,
                                                   columns=["precision", "recall", "f1-score", "support"],
                                                   index=["1", "2", "accuracy", "macro avg", "weighted avg"]))

    def test_idstr_split_exists(self):
        """tests multiclass reporting on numerical data when idstr and split columns are given"""
        data = pd.DataFrame(
            {"target": [0, 1, 2, 0, 0],
             "predicted": [0, 1, 0, 2, 0],
             "id": [1, 1, 1, 2, 2],
             "data": ["train", "train", "val", "val", "train"]})
        service = MulticlassReporterService()
        settings = MultiClassReporterSettings(data, "target", "predicted", None, "id", "data", 1, None)
        output = service.execute(settings)
        self.assertIsNotNone(output.scores)
        self.assertIsNotNone(output.confusion_matrices)
        pd.testing.assert_frame_equal(output.scores,
                                      pd.DataFrame({"IdStr": [1, 1, 2, 2], "Data": ["train", "val", "train", "val"],
                                                    "Accuracy": [1.0, 0.0, 1.0, 0.0],
                                                    "Roc-Auc Score": [None, None, None, None]}))
        cls_report_1_train = [[1.00000, 1.00000, 1.00000, 1.00000],
                              [1.00000, 1.00000, 1.00000, 1.00000],
                              [1.00000, 1.00000, 1.00000, 1.00000],
                              [1.00000, 1.00000, 1.00000, 2.00000],
                              [1.00000, 1.00000, 1.00000, 2.00000]]
        cls_report_1_val = [[0.00000, 1.00000, 0.00000, 0.00000],
                            [1.00000, 0.00000, 0.00000, 1.00000],
                            [0.00000, 0.00000, 0.00000, 0.00000],
                            [0.50000, 0.50000, 0.00000, 1.00000],
                            [1.00000, 0.00000, 0.00000, 1.00000]]
        cls_report_2_train = [[1.00000, 1.00000, 1.00000, 1.00000],
                              [1.00000, 1.00000, 1.00000, 1.00000],
                              [1.00000, 1.00000, 1.00000, 1.00000],
                              [1.00000, 1.00000, 1.00000, 1.00000]]
        cls_report_2_val = [[1.00000, 0.00000, 0.00000, 1.00000],
                            [0.00000, 1.00000, 0.00000, 0.00000],
                            [0.00000, 0.00000, 0.00000, 0.00000],
                            [0.50000, 0.50000, 0.00000, 1.00000],
                            [1.00000, 0.00000, 0.00000, 1.00000]]
        pd.testing.assert_frame_equal(output.confusion_matrices[(1, "train")],
                                      pd.DataFrame({0: [1, 0], 1: [0, 1]},
                                                   index=[0, 1]))
        pd.testing.assert_frame_equal(output.classification_reports[(1, "train")],
                                      pd.DataFrame(data=cls_report_1_train,
                                                   columns=["precision", "recall", "f1-score", "support"],
                                                   index=["0", "1", "accuracy", "macro avg", "weighted avg"]
                                                   ))
        pd.testing.assert_frame_equal(output.confusion_matrices[(1, "val")],
                                      pd.DataFrame({0: [0, 1], 2: [0, 0]},
                                                   index=[0, 2]))
        pd.testing.assert_frame_equal(output.classification_reports[(1, "val")],
                                      pd.DataFrame(data=cls_report_1_val,
                                                   columns=["precision", "recall", "f1-score", "support"],
                                                   index=["0", "2", "accuracy", "macro avg", "weighted avg"]
                                                   ))
        pd.testing.assert_frame_equal(output.confusion_matrices[(2, "train")],
                                      pd.DataFrame({0: [1]},
                                                   index=[0]))
        pd.testing.assert_frame_equal(output.classification_reports[(2, "train")],
                                      pd.DataFrame(data=cls_report_2_train,
                                                   columns=["precision", "recall", "f1-score", "support"],
                                                   index=["0", "accuracy", "macro avg", "weighted avg"]
                                                   ))
        pd.testing.assert_frame_equal(output.confusion_matrices[(2, "val")],
                                      pd.DataFrame({0: [0, 0], 2: [1, 0]},
                                                   index=[0, 2]))
        pd.testing.assert_frame_equal(output.classification_reports[(2, "val")],
                                      pd.DataFrame(data=cls_report_2_val,
                                                   columns=["precision", "recall", "f1-score", "support"],
                                                   index=["0", "2", "accuracy", "macro avg", "weighted avg"]
                                                   ))

    def test_score_column_contains_probabilities(self):
        """test if score column contains predict_proba values"""
        data = pd.DataFrame(
            {"target": [1, 0, 2, 1, 1]})
        probability_values = np.array(
            [[0.1, 0.8, 0.7], [0.1, 0.2, 0.4], [0.9, 0.5, 0.7], [0.4, 0.5, 0.7], [0.1, 0.8, 0.7]])
        class_orders = [0, 1, 2]
        service = MulticlassReporterService()
        settings = MultiClassReporterSettings(data, "target", "predicted", None, None, None, 1, probability_values,
                                              ordered_class_names=class_orders)
        output = service.execute(settings)
        self.assertIsNotNone(output.scores)
        self.assertIsNotNone(output.confusion_matrices)
        pd.testing.assert_frame_equal(output.scores,
                                      pd.DataFrame({'Accuracy': [0.4], 'Roc-Auc Score': [0.725]}, index=[0]))

    def test_probability_values_given_raise_error_when_class_names_are_empty(self):
        """test when probability values were given, but ordered_class_names are None, raises a ValueError"""
        data = pd.DataFrame(
            {"target": [1, 0, 2, 1, 1]})
        probability_values = np.array(
            [[0.1, 0.8, 0.7], [0.1, 0.2, 0.4], [0.9, 0.5, 0.7], [0.4, 0.5, 0.7], [0.1, 0.8, 0.7]])
        service = MulticlassReporterService()
        settings = MultiClassReporterSettings(data, "target", "predicted", None, None, None, 1, probability_values)
        with self.assertRaisesRegex(ValueError,
                                    "When probability_values were given, ordered_class_names cannot be empty."):
            service.execute(settings)

    def test_idstr_exists_with_probabilities_with_respect_class_orders(self):
        """tests multiclass reporting on numerical data when idstr is given"""
        data = pd.DataFrame(
            {"target": [1, 0, 2, 1, 2, 1],
             "id": [1, 1, 1, 2, 2, 2]})
        probability_values = np.array(
            [[0.8, 0.1, 0.7], [0.1, 0.2, 0.4], [0.9, 0.5, 0.7], [0.4, 0.5, 0.7], [0.1, 0.8, 0.7],
             [0.1, 0.9, 0.7]])
        class_orders = [1, 0, 2]

        service = MulticlassReporterService()
        settings = MultiClassReporterSettings(data, "target", "predicted", None, "id", None, 1, probability_values,
                                              ordered_class_names=class_orders)
        output = service.execute(settings)
        self.assertIsNotNone(output.scores)
        self.assertIsNotNone(output.confusion_matrices)
        pd.testing.assert_frame_equal(output.scores,
                                      pd.DataFrame(data=[[1, 0.33333, 0.51944], [2, 0, 0.51944]],
                                                   index=[0, 1],
                                                   columns=['IdStr', 'Accuracy', 'Roc-Auc Score']))
        class_orders = [0, 1, 2]
        settings = MultiClassReporterSettings(data, "target", "predicted", None, "id", None, 1, probability_values,
                                              ordered_class_names=class_orders)
        output = service.execute(settings)
        pd.testing.assert_frame_equal(output.scores,
                                      pd.DataFrame(data=[[1, 0, 0.49167], [2, 0.33333, 0.49167]],
                                                   index=[0, 1],
                                                   columns=['IdStr', 'Accuracy', 'Roc-Auc Score']))

    def test_probability_score_indexes_match_with_proba_value_counterparts(self):
        """test with manually given indexes matches with proba_values data indexes"""
        data = pd.DataFrame(
            {"target": [1, 0, 2, 1, 1]}, index=[1, 2, 0, 4, 3])
        probability_values = np.array(
            [[0.1, 0.8, 0.7], [0.1, 0.2, 0.4], [0.9, 0.5, 0.7], [0.4, 0.5, 0.7], [0.1, 0.8, 0.7]])
        service = MulticlassReporterService()
        class_orders = [0, 1, 2]
        settings = MultiClassReporterSettings(data, "target", "predicted", None, None, None, 1, probability_values,
                                              class_orders)
        output1 = service.execute(settings)
        self.assertIsNotNone(output1.scores)
        pd.testing.assert_frame_equal(output1.scores,
                                      pd.DataFrame({'Accuracy': [0.4], 'Roc-Auc Score': [0.725]}, index=[0]))
        pd.testing.assert_frame_equal(output1.confusion_matrices[("ALL",)],
                                      pd.DataFrame({1: [2, 0, 0], 0: [0, 0, 1], 2: [1, 1, 0]},
                                                   index=[1, 0, 2]))
        data2 = pd.DataFrame(
            {"target": [1, 0, 2, 1, 1]})
        settings = MultiClassReporterSettings(data2, "target", "predicted", None, None, None, 1, probability_values,
                                              class_orders)
        output2 = service.execute(settings)
        self.assertIsNotNone(output2.scores)
        pd.testing.assert_frame_equal(output1.scores, output2.scores)
        pd.testing.assert_frame_equal(output1.confusion_matrices[("ALL",)], output2.confusion_matrices[("ALL",)])
        pd.testing.assert_frame_equal(output1.classification_reports[("ALL",)],
                                      output2.classification_reports[("ALL",)])


if __name__ == '__main__':
    unittest.main()
