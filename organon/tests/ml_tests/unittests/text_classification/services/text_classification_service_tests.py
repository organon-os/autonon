"""Unit Test class for Text Classification Service"""
import unittest
from unittest.mock import MagicMock, call, patch

import sklearn
from numpy.testing import assert_array_equal
import pandas as pd
import numpy as np

from organon.ml.text_classification.domain.enums.classification_languages import ClassificationLanguages
from organon.ml.text_classification.domain.enums.model_checkpoints import ModelCheckpoints
from organon.ml.text_classification.domain.enums.model_run_type import ModelRunType
from organon.ml.text_classification.domain.enums.selection_metrics import SelectionMetrics
from organon.ml.text_classification.domain.objects.grid_search_settings import GridSearchSettings
from organon.ml.text_classification.domain.objects.optimizer_settings import OptimizerSettings

from organon.ml.text_classification.domain.objects.text_classification_settings import TextClassificationSettings
from organon.ml.text_classification.domain.services import text_classification_service
from organon.ml.text_classification.domain.services.text_classification_service import TextClassificationService
from organon.tests import test_helper


class TextClassificationServiceTests(unittest.TestCase):  # pylint: disable=too-many-instance-attributes
    """Text Classification Service Unit Tests"""

    def setUp(self) -> None:
        module = text_classification_service.__name__
        self.train_data = \
            pd.DataFrame({'text': [
                "“Worry is a down payment on a problem you may never have'. Joyce Meyer.",
                "My roommate: it's okay that we can't spell because we have autocorrect. #terrible #firstworldprobs",
                "No but that's so cute. Atsu was probably shy about photos before but cherry helped her out uwu"
            ],
                'tur': [2, 0, 1]
            })
        self.val_data = pd.DataFrame({'text': [
            'if not has done is provoke her by tweeting shady shit and trying to be hard bitch begging for a fight',
            'Hey @user #Fields in #skibbereen give your online delivery service a horrible name.',
            'Why have #Emmerdale had to rob of having their first child together for that vile woman #bitter'
        ],
            'tur': [0, 2, 1]
        })
        self.test_data = pd.DataFrame({
            'text': ['My visit to hospital for care triggered #trauma from accident. Feeling symptoms of #depression',
                     '@user Welcome to #MPSVT! We are delighted to have you! #grateful #MPSVT #relationships',
                     'What makes you feel #joyful?'
                     ],
            'tur': [2, 1, 0]
        })
        self.mock_model = MagicMock()
        self.mock_optimizer = MagicMock()
        self.mock_tokenizer = MagicMock()

        self.mock_keras_callbacks = MagicMock()
        self.mock_keras_callbacks_patcher = patch.dict("sys.modules", {"keras.callbacks": self.mock_keras_callbacks})
        self.mock_keras_callbacks_patcher.start()

        self.mock_transformers = MagicMock()
        self.mock_transformers_patcher = patch.dict("sys.modules", {"transformers": self.mock_transformers})
        self.mock_transformers_patcher.start()

        self.mock_sklearn_metrics = MagicMock(spec=sklearn.metrics)
        self.mock_sklearn_metrics_patcher = patch.dict("sys.modules",
                                                       {"sklearn.metrics": self.mock_sklearn_metrics})
        self.mock_sklearn_metrics_patcher.start()

        self.mock_early_stopping = MagicMock()
        self.mock_early_stopping_patcher = patch.dict("sys.modules", {"EarlyStopping": self.mock_early_stopping})
        self.mock_early_stopping_patcher.start()

        self.mock_service_dataset = test_helper.get_mock(self, module + "." + "Dataset")
        self.mock_service_tensorflow = test_helper.get_mock(self, module + "." + "tf")

        self.mock_data_collator = MagicMock()
        self.mock_data_collator_patcher = patch.dict("sys.modules",
                                                     {"DataCollatorWithPadding": self.mock_data_collator})
        self.mock_data_collator_patcher.start()

        self.mock_scipy_special = MagicMock()
        self.mock_scipy_special_patcher = patch.dict("sys.modules",
                                                     {"scipy.special": self.mock_scipy_special})
        self.mock_scipy_special_patcher.start()

        self.mock_train_test_split = \
            test_helper.get_mock(self, module + "." + sklearn.model_selection.train_test_split.__name__)

        self.mock_transformers.TFAutoModelForSequenceClassification.from_pretrained.return_value = self.mock_model
        self.mock_transformers.AutoTokenizer.from_pretrained.return_value = self.mock_tokenizer

    def test_train(self):
        """test model training phase"""
        optimizer_settings = OptimizerSettings(learning_rate=1e-5, steps_per_epoch=0, early_stopping=2,
                                               early_stopping_min_delta=1e-3)
        settings = TextClassificationSettings(ClassificationLanguages.ENGLISH,
                                              checkpoint=ModelCheckpoints.ROBERTA_BASE,
                                              mdl_run_type=ModelRunType.EFFICIENT,
                                              text_column="metin", target_column="tur",
                                              opt_settings=optimizer_settings)
        service = TextClassificationService(settings)
        self.assertFalse(service.is_trained)
        self.mock_model.evaluate.return_value = 0.7
        self.mock_transformers.create_optimizer.return_value = self.mock_optimizer, "schedule"
        service.fit(data=self.train_data, validation_data=self.val_data, validation_data_ratio=0.5)
        self.mock_transformers.create_optimizer.assert_called_with(
            init_lr=settings.opt_settings.learning_rate, num_warmup_steps=0,
            num_train_steps=0)
        self.mock_keras_callbacks. \
            EarlyStopping.assert_called_with(patience=settings.opt_settings.early_stopping,
                                             min_delta=settings.opt_settings.early_stopping_min_delta,
                                             monitor='val_loss')
        self.mock_model.compile.assert_called_with(optimizer=self.mock_optimizer)
        self.mock_transformers.TFAutoModelForSequenceClassification.from_pretrained.assert_called_with('roberta-base',
                                                                                                       num_labels=3,
                                                                                                       from_pt=True)
        self.mock_transformers.AutoTokenizer.from_pretrained.assert_called_with("roberta-base")
        self.assertTrue(service.is_trained)
        self.assertIsNone(service.best_params_dict)
        self.assertEqual(service.metrics['Loss'], 0.7)

    def test_train_grid_search_with_default_selection_metric_loss(self):
        """test model training phase"""
        grd_src_settings = GridSearchSettings(models=[ModelCheckpoints.ROBERTA_BASE, ModelCheckpoints.BERT_BASE],
                                              epochs=[10, 15], batch_sizes=[16, 32], learning_rates=[1e-5],
                                              early_stopping_min_deltas=[1e-3, 1e-4], early_stopping_patiences=[2])
        settings = TextClassificationSettings(ClassificationLanguages.ENGLISH,
                                              mdl_run_type=ModelRunType.HIGH_PERFORMANCE,
                                              text_column="text", target_column="tur",
                                              grid_search_settings=grd_src_settings)
        service = TextClassificationService(settings)
        self.assertFalse(service.is_trained)
        self.mock_model.evaluate.side_effect = [0.52, 0.54, 0.78, 0.84, 0.36,
                                                0.45, 0.50, 0.62, 1.42, 0.55,
                                                0.80, 0.81, 0.58, 0.7, 0.8,
                                                1.0]
        self.mock_sklearn_metrics.roc_auc_score.side_effect = [0.75, 0.78, 0.71, 0.54, 0.55,
                                                               0.65, 0.57, 0.58, 0.70, 0.8,
                                                               0.62, 0.53, 0.56, 0.50, 0.45,
                                                               0.44]
        self.mock_sklearn_metrics.accuracy_score.side_effect = [72.0, 76.0, 65.0, 56.0, 82.0,
                                                                78.0, 77.0, 73.0, 12.0, 70.0,
                                                                62.5, 55.0, 79.0, 90.0, 35.0,
                                                                48.0]
        self.mock_sklearn_metrics.f1_score.side_effect = [0.23, 0.22, 0.23, 0.24, 0.35,
                                                          0.37, 0.41, 0.54, 0.55, 0.61,
                                                          0.60, 0.64, 0.37, 0.44, 0.48,
                                                          0.5]

        self.mock_transformers.create_optimizer.return_value = self.mock_optimizer, "schedule"
        self.mock_service_dataset.from_pandas.return_value.map.return_value.__len__.return_value = 50
        self.mock_scipy_special.softmax.return_value = [[0.498664, 0.501336],
                                                        [0.501653, 0.498348],
                                                        [0.495578, 0.504422]]
        service.fit(data=self.train_data, validation_data=self.val_data)
        mock_model_calls = self.mock_transformers.TFAutoModelForSequenceClassification.from_pretrained
        # grid search settings for models are ROBERTA_BASE and BERT_BASE, declared above
        mock_model_calls.assert_any_call('roberta-base', num_labels=3, from_pt=True)
        mock_model_calls.assert_any_call('bert-base-cased', num_labels=3, from_pt=True)
        expected_optimizer_call_list = TextClassificationServiceTests.get_expected_calls_for_optimizer()
        expected_early_stopping_call_list = TextClassificationServiceTests.get_expected_calls_for_early_stopping()

        for idx, called_one in enumerate(self.mock_transformers.create_optimizer.call_args_list):
            self.assertEqual(expected_optimizer_call_list[idx], called_one)

        for idx, called_one in enumerate(self.mock_early_stopping.call_args_list):
            self.assertEqual(expected_early_stopping_call_list[idx], called_one)

        params_dict = TextClassificationServiceTests.get_params_dict_for_grid_search()

        best_params_dict = {'Checkpoint': 'ROBERTA_BASE', 'Batch Size': 32, 'Epoch': 10, 'Learning Rate': 1e-05,
                            'Early Stopping Patience': 2, 'Early Stopping Min Delta': 0.001}

        self.assertEqual(16, mock_model_calls.call_count)
        self.mock_transformers.AutoTokenizer.from_pretrained.assert_called_with('bert-base-cased')
        self.assertEqual(service.grid_search_params_dict, params_dict)
        self.assertEqual(service.best_params_dict, best_params_dict)
        self.assertEqual(service.metrics["Loss"], 0.36)
        self.assertEqual(service.metrics["Accuracy"], 82.0)
        self.assertTrue(service.is_trained)

    def test_train_grid_search_with_selection_metric_accuracy(self):
        """test model training phase with selection metric accuracy"""
        grd_src_settings = GridSearchSettings(models=[ModelCheckpoints.ROBERTA_BASE, ModelCheckpoints.BERT_BASE],
                                              epochs=[10, 15], batch_sizes=[16, 32], learning_rates=[1e-5],
                                              early_stopping_min_deltas=[1e-3, 1e-4], early_stopping_patiences=[2],
                                              model_selection_metric=SelectionMetrics.ACC_SCORE)
        settings = TextClassificationSettings(ClassificationLanguages.ENGLISH,
                                              mdl_run_type=ModelRunType.HIGH_PERFORMANCE,
                                              text_column="text", target_column="tur",
                                              grid_search_settings=grd_src_settings)
        service = TextClassificationService(settings)
        self.assertFalse(service.is_trained)
        self.mock_model.evaluate.side_effect = [0.52, 0.54, 0.78, 0.84, 0.36,
                                                0.45, 0.50, 0.62, 1.42, 0.55,
                                                0.80, 0.81, 0.58, 0.7, 0.8,
                                                1.0]
        self.mock_sklearn_metrics.roc_auc_score.side_effect = [0.75, 0.78, 0.71, 0.54, 0.55,
                                                               0.65, 0.57, 0.58, 0.70, 0.8,
                                                               0.62, 0.53, 0.56, 0.50, 0.45,
                                                               0.44]
        self.mock_sklearn_metrics.accuracy_score.side_effect = [72.0, 76.0, 65.0, 56.0, 82.0,
                                                                78.0, 77.0, 73.0, 12.0, 70.0,
                                                                62.5, 55.0, 79.0, 90.0, 35.0,
                                                                48.0]
        self.mock_sklearn_metrics.f1_score.side_effect = [0.23, 0.22, 0.23, 0.24, 0.35,
                                                          0.37, 0.41, 0.54, 0.55, 0.61,
                                                          0.60, 0.64, 0.37, 0.44, 0.48,
                                                          0.5]

        self.mock_transformers.create_optimizer.return_value = self.mock_optimizer, "schedule"
        self.mock_service_dataset.from_pandas.return_value.map.return_value.__len__.return_value = 50
        self.mock_scipy_special.softmax.return_value = [[0.498664, 0.501336],
                                                  [0.501653, 0.498348],
                                                  [0.495578, 0.504422]]
        service.fit(data=self.train_data, validation_data=self.val_data)
        mock_model_calls = self.mock_transformers.TFAutoModelForSequenceClassification.from_pretrained
        # grid search settings for models are ROBERTA_BASE and BERT_BASE, declared above
        mock_model_calls.assert_any_call('roberta-base', num_labels=3, from_pt=True)
        mock_model_calls.assert_any_call('bert-base-cased', num_labels=3, from_pt=True)
        expected_optimizer_call_list = TextClassificationServiceTests.get_expected_calls_for_optimizer()
        expected_early_stopping_call_list = TextClassificationServiceTests.get_expected_calls_for_early_stopping()

        for idx, called_one in enumerate(self.mock_transformers.create_optimizer.call_args_list):
            self.assertEqual(expected_optimizer_call_list[idx], called_one)

        for idx, called_one in enumerate(self.mock_early_stopping.call_args_list):
            self.assertEqual(expected_early_stopping_call_list[idx], called_one)

        params_dict = TextClassificationServiceTests.get_params_dict_for_grid_search()

        best_params_dict = {'Checkpoint': 'BERT_BASE', 'Batch Size': 32, 'Epoch': 10, 'Learning Rate': 1e-05,
                            'Early Stopping Patience': 2, 'Early Stopping Min Delta': 0.0001}

        self.assertEqual(16, mock_model_calls.call_count)
        self.mock_transformers.AutoTokenizer.from_pretrained.assert_called_with('bert-base-cased')
        self.assertEqual(service.grid_search_params_dict, params_dict)
        self.assertEqual(service.best_params_dict, best_params_dict)
        self.assertEqual(service.metrics["Loss"], 0.7)
        self.assertEqual(service.metrics["Accuracy"], 90.0)
        self.assertTrue(service.is_trained)

    def test_predict(self):
        """test prediction phase - predict"""
        settings = TextClassificationSettings(ClassificationLanguages.ENGLISH,
                                              checkpoint=ModelCheckpoints.ROBERTA_BASE,
                                              mdl_run_type=ModelRunType.EFFICIENT,
                                              text_column="text", target_column="tur")
        service = TextClassificationService(settings)
        self.assertFalse(service.is_trained)
        self.mock_model.evaluate.return_value = 0.7, 81.0
        self.mock_transformers.create_optimizer.return_value = self.mock_optimizer, "schedule"
        service.fit(data=self.train_data, validation_data=self.val_data)
        self.mock_transformers.AutoTokenizer.from_pretrained.assert_called_with('roberta-base')
        self.mock_transformers.TFAutoModelForSequenceClassification.from_pretrained.assert_called_with('roberta-base',
                                                                                                       num_labels=3,
                                                                                                       from_pt=True)
        self.assertTrue(service.is_trained)

        expected_class_predictions = [1, 0, 3, 1, 0]
        self.mock_model.predict.return_value = {
            "loss": [1.4426726, 1.3722024, 1.4431531, 1.3014103, 1.4383782],
            "logits": [
                [0.08355551, 0.0888994, -0.07564338, 0.06770059],
                [0.09211624, 0.08550246, -0.07299915, 0.08186924],
                [0.07115794, 0.08884606, -0.07170564, 0.09547395],
                [0.06520867, 0.09478951, -0.07292116, 0.08321001],
                [0.0921608, 0.09143362, -0.07879359, 0.07665707]]}
        class_predictions = service.predict(test_data=self.test_data)
        assert_array_equal(expected_class_predictions, class_predictions)

    def test_predict_proba(self):
        """test prediction phase - predict_proba"""
        settings = TextClassificationSettings(ClassificationLanguages.ENGLISH,
                                              checkpoint=ModelCheckpoints.ROBERTA_BASE,
                                              mdl_run_type=ModelRunType.EFFICIENT,
                                              text_column="text", target_column="tur")
        service = TextClassificationService(settings)
        self.assertFalse(service.is_trained)
        self.mock_model.evaluate.return_value = 0.7, 81.0
        self.mock_transformers.create_optimizer.return_value = self.mock_optimizer, "schedule"
        service.fit(data=self.train_data, validation_data=self.val_data)
        self.mock_transformers.TFAutoModelForSequenceClassification.from_pretrained.assert_called_with('roberta-base',
                                                                                                       num_labels=3,
                                                                                                       from_pt=True)
        self.mock_transformers.AutoTokenizer.from_pretrained.assert_called_with("roberta-base")
        self.assertTrue(service.is_trained)

        self.mock_model.predict.return_value = {
            "loss": [1.4426726, 1.3722024, 1.4431531],
            "logits": [
                [0.08355551, 0.0888994, -0.07564338],
                [0.09211624, 0.08550246, -0.07299915],
                [0.07115794, 0.08884606, -0.07170564]]}
        self.mock_sklearn_metrics.roc_auc_score.return_value = 0.6
        self.mock_scipy_special.softmax.return_value = np.array([[0.835, 0.88, 0.756],
                                                                 [0.921, 0.855, 0.729],
                                                                 [0.711, 0.888, 0.717]])
        predictions_softmax, roc_scr = service.predict_proba(test_data=self.test_data)
        expected_predictions_softmax = np.array([[0.835, 0.88, 0.756],
                                                 [0.921, 0.855, 0.729],
                                                 [0.711, 0.888, 0.717]])
        self.assertEqual(0.6, roc_scr)
        assert_array_equal(expected_predictions_softmax, predictions_softmax)

    def test_predict_proba_binary(self):
        """test prediction phase - predict_proba binary text classification"""
        settings = TextClassificationSettings(ClassificationLanguages.ENGLISH,
                                              checkpoint=ModelCheckpoints.ROBERTA_BASE,
                                              mdl_run_type=ModelRunType.EFFICIENT,
                                              text_column="text", target_column="tur")
        service = TextClassificationService(settings)
        self.assertFalse(service.is_trained)
        self.mock_model.evaluate.return_value = 0.7, 81.0
        self.mock_transformers.create_optimizer.return_value = self.mock_optimizer, "schedule"
        train_data, val_data, test_data = TextClassificationServiceTests.get_binary_data()
        service.fit(data=train_data, validation_data=val_data)
        self.mock_transformers.TFAutoModelForSequenceClassification.from_pretrained.assert_called_with('roberta-base',
                                                                                                       num_labels=2,
                                                                                                       from_pt=True)
        self.mock_transformers.AutoTokenizer.from_pretrained.assert_called_with("roberta-base")
        self.assertTrue(service.is_trained)

        self.mock_model.predict.return_value = {
            "loss": [1.4426726, 1.3722024, 1.4431531],
            "logits": [
                [0.08355551, 0.0888994],
                [0.09211624, 0.08550246],
                [0.07115794, 0.08884606]]}
        self.mock_sklearn_metrics.roc_auc_score.return_value = 0.535
        self.mock_scipy_special.softmax.return_value = np.array([[0.835, 0.888],
                                                                 [0.921, 0.855],
                                                                 [0.711, 0.888]])
        predictions_softmax, roc_scr = service.predict_proba(test_data=test_data)
        expected_predictions_softmax = np.array([[0.835, 0.888],
                                                 [0.921, 0.855],
                                                 [0.711, 0.888]])
        self.assertEqual(0.535, roc_scr)
        assert_array_equal(expected_predictions_softmax, predictions_softmax)

    def test_predict_method_without_train(self):
        """test if user tries to predict something before the model training"""
        settings = TextClassificationSettings(ClassificationLanguages.ENGLISH,
                                              checkpoint=ModelCheckpoints.ROBERTA_BASE,
                                              mdl_run_type=ModelRunType.EFFICIENT)
        service = TextClassificationService(settings)
        self.assertFalse(service.is_trained)
        # asserts Exception because the model has not been trained yet
        with self.assertRaisesRegex(Exception, "Trainer is not trained yet."):
            service.predict(test_data=self.test_data)
        with self.assertRaisesRegex(Exception, "Trainer is not trained yet."):
            service.predict_proba(test_data=self.test_data)

    def test_check_if_dataframe_is_empty(self):
        """check if class throws error when the hf data keywords do not exist"""
        data = pd.DataFrame()
        val_data = pd.DataFrame()
        settings = TextClassificationSettings(ClassificationLanguages.ENGLISH,
                                              checkpoint=ModelCheckpoints.BERT_BASE_TR_128,
                                              mdl_run_type=ModelRunType.EFFICIENT, text_column='text',
                                              target_column='tur')
        service = TextClassificationService(settings)
        with self.assertRaisesRegex(ValueError, "DataFrames can not be empty"):
            service.fit(data=data, validation_data=val_data)

    def test_fit_pd_dataframe_without_text_column_name_raises_error(self):
        """test if training pd.DataFrame input object occurs error without giving text and target col names"""
        settings = TextClassificationSettings(ClassificationLanguages.ENGLISH,
                                              checkpoint=ModelCheckpoints.ROBERTA_BASE,
                                              mdl_run_type=ModelRunType.EFFICIENT)
        service = TextClassificationService(settings)
        self.assertFalse(service.is_trained)
        with self.assertRaisesRegex(ValueError, "DataFrame text column name must be specified"):
            service.fit(self.train_data, self.val_data)

    def test_fit_pd_dataframe_without_target_column_name_raises_error(self):
        """test if training pd.DataFrame input object occurs error without giving text and target col names"""
        settings = TextClassificationSettings(ClassificationLanguages.ENGLISH,
                                              checkpoint=ModelCheckpoints.ROBERTA_BASE,
                                              mdl_run_type=ModelRunType.EFFICIENT, text_column="text")
        service = TextClassificationService(settings)
        self.assertFalse(service.is_trained)
        with self.assertRaisesRegex(ValueError, "DataFrame target column name must be specified"):
            service.fit(self.train_data, self.val_data)

    def test_checkpoint_mismatch_for_english(self):
        """test if model checkpoint mismatches with the language specified"""
        settings = TextClassificationSettings(ClassificationLanguages.ENGLISH,
                                              checkpoint=ModelCheckpoints.BERT_BASE_TR_128,
                                              mdl_run_type=ModelRunType.EFFICIENT, text_column="text",
                                              target_column="tur")
        service = TextClassificationService(settings)
        # asserts ValueError because the checkpoint is for turkish data, but specified language is english
        with self.assertRaisesRegex(ValueError,
                                    f"Checkpoint named {ModelCheckpoints.BERT_BASE_TR_128} is non-English"):
            service.fit(data=self.train_data, validation_data=self.val_data)

    def test_checkpoint_mismatch_for_turkish(self):
        """test if model checkpoint mismatches with the language specified"""
        settings = TextClassificationSettings(ClassificationLanguages.TURKISH,
                                              checkpoint=ModelCheckpoints.ROBERTA_BASE,
                                              mdl_run_type=ModelRunType.EFFICIENT, text_column="text",
                                              target_column="tur")
        service = TextClassificationService(settings)
        # asserts ValueError because the checkpoint is for english data, but specified language is turkish
        with self.assertRaisesRegex(ValueError,
                                    f"Checkpoint named {ModelCheckpoints.ROBERTA_BASE} is non-Turkish"):
            service.fit(data=self.train_data, validation_data=self.val_data)

    def test_if_checkpoint_is_appropriate_for_multiclass(self):
        """test if model checkpoint mismatches with classification type"""
        settings = TextClassificationSettings(ClassificationLanguages.TURKISH,
                                              checkpoint=ModelCheckpoints.BERT_BASE_TR_128,
                                              mdl_run_type=ModelRunType.EFFICIENT, text_column='text',
                                              target_column='tur')
        service = TextClassificationService(settings)
        self.mock_model.evaluate.return_value = 0.7, 81.0
        # asserts ValueError because given data is not binary, but specified checkpoint is for binary classification
        with self.assertRaisesRegex(TypeError, "This type of checkpoint can't be used in Multiclass Classification"):
            service.fit(data=self.train_data, validation_data=self.val_data)

    def test_if_value_error_occurs_csv_input_without_specifying_text_col_name(self):
        """test when using dataframe objects as input, without col names the service must throw an exception"""

        settings = TextClassificationSettings(ClassificationLanguages.ENGLISH,
                                              checkpoint=ModelCheckpoints.ROBERTA_BASE,
                                              mdl_run_type=ModelRunType.EFFICIENT)
        service = TextClassificationService(settings)
        # asserts ValueError because given data is not binary, but specified checkpoint is for binary classification
        with self.assertRaisesRegex(ValueError, "CSV text column name must be specified"):
            service.fit(data="some_path", validation_data="val_path")

    def test_if_value_error_occurs_dataframe_input_without_target_col_name(self):
        """test when using dataframe objects as input, without col names the service must throw an exception"""

        settings = TextClassificationSettings(ClassificationLanguages.ENGLISH,
                                              checkpoint=ModelCheckpoints.ROBERTA_BASE,
                                              mdl_run_type=ModelRunType.EFFICIENT, text_column="text")
        service = TextClassificationService(settings)
        # asserts ValueError because given data is not binary, but specified checkpoint is for binary classification
        with self.assertRaisesRegex(ValueError, "CSV target column name must be specified"):
            service.fit(data="some_path", validation_data="val_path")

    def test_if_csv_data_cannot_located(self):
        """test if csv data cannot be located"""
        settings = TextClassificationSettings(ClassificationLanguages.TURKISH,
                                              checkpoint=ModelCheckpoints.BERT_BASE_TR_128,
                                              mdl_run_type=ModelRunType.EFFICIENT, text_column="text",
                                              target_column="tur")
        service = TextClassificationService(settings)
        path = "some_path"
        val_path = "val_path"
        with self.assertRaisesRegex(FileNotFoundError, "csv path can not be located."):
            service.fit(data=path, validation_data=val_path)

    def test_if_the_target_column_name_changed_with_dataframe_input(self):
        """test if the user dataframe was not changed after the fit operation"""
        settings = TextClassificationSettings(ClassificationLanguages.ENGLISH,
                                              checkpoint=ModelCheckpoints.ROBERTA_BASE,
                                              mdl_run_type=ModelRunType.EFFICIENT,
                                              text_column="text", target_column="tur")
        service = TextClassificationService(settings)
        self.assertFalse(service.is_trained)
        self.mock_model.evaluate.return_value = 0.7, 81.0
        self.mock_transformers.create_optimizer.return_value = self.mock_optimizer, "schedule"
        service.fit(data=self.train_data, validation_data=self.val_data)
        self.mock_transformers.TFAutoModelForSequenceClassification.from_pretrained.assert_called_with('roberta-base',
                                                                                                       num_labels=3,
                                                                                                       from_pt=True)
        self.mock_transformers.AutoTokenizer.from_pretrained.assert_called_with("roberta-base")
        self.assertTrue(service.is_trained)
        self.assertFalse('label' in self.train_data.columns)

    @staticmethod
    def get_params_dict_for_grid_search():
        """returns expected params dict for grid search"""
        return {'Step 1': {'Checkpoint': 'ROBERTA_BASE', 'Batch Size': 16, 'Epoch': 10, 'Learning Rate': 1e-05,
                           'Early Stopping Patience': 2, 'Early Stopping Min Delta': 0.001, 'Accuracy': 72.0,
                           'Loss': 0.52, 'Roc-Auc Score': 0.75, 'F1-Score': 0.23},
                'Step 2': {'Checkpoint': 'ROBERTA_BASE', 'Batch Size': 16, 'Epoch': 10, 'Learning Rate': 1e-05,
                           'Early Stopping Patience': 2, 'Early Stopping Min Delta': 0.0001, 'Accuracy': 76.0,
                           'Loss': 0.54, 'Roc-Auc Score': 0.78, 'F1-Score': 0.22},
                'Step 3': {'Checkpoint': 'ROBERTA_BASE', 'Batch Size': 16, 'Epoch': 15, 'Learning Rate': 1e-05,
                           'Early Stopping Patience': 2, 'Early Stopping Min Delta': 0.001, 'Accuracy': 65.0,
                           'Loss': 0.78, 'Roc-Auc Score': 0.71, 'F1-Score': 0.23},
                'Step 4': {'Checkpoint': 'ROBERTA_BASE', 'Batch Size': 16, 'Epoch': 15, 'Learning Rate': 1e-05,
                           'Early Stopping Patience': 2, 'Early Stopping Min Delta': 0.0001, 'Accuracy': 56.0,
                           'Loss': 0.84, 'Roc-Auc Score': 0.54, 'F1-Score': 0.24},
                'Step 5': {'Checkpoint': 'ROBERTA_BASE', 'Batch Size': 32, 'Epoch': 10, 'Learning Rate': 1e-05,
                           'Early Stopping Patience': 2, 'Early Stopping Min Delta': 0.001, 'Accuracy': 82.0,
                           'Loss': 0.36, 'Roc-Auc Score': 0.55, 'F1-Score': 0.35},
                'Step 6': {'Checkpoint': 'ROBERTA_BASE', 'Batch Size': 32, 'Epoch': 10, 'Learning Rate': 1e-05,
                           'Early Stopping Patience': 2, 'Early Stopping Min Delta': 0.0001, 'Accuracy': 78.0,
                           'Loss': 0.45, 'Roc-Auc Score': 0.65, 'F1-Score': 0.37},
                'Step 7': {'Checkpoint': 'ROBERTA_BASE', 'Batch Size': 32, 'Epoch': 15, 'Learning Rate': 1e-05,
                           'Early Stopping Patience': 2, 'Early Stopping Min Delta': 0.001, 'Accuracy': 77.0,
                           'Loss': 0.5, 'Roc-Auc Score': 0.57, 'F1-Score': 0.41},
                'Step 8': {'Checkpoint': 'ROBERTA_BASE', 'Batch Size': 32, 'Epoch': 15, 'Learning Rate': 1e-05,
                           'Early Stopping Patience': 2, 'Early Stopping Min Delta': 0.0001, 'Accuracy': 73.0,
                           'Loss': 0.62, 'Roc-Auc Score': 0.58, 'F1-Score': 0.54},
                'Step 9': {'Checkpoint': 'BERT_BASE', 'Batch Size': 16, 'Epoch': 10, 'Learning Rate': 1e-05,
                           'Early Stopping Patience': 2, 'Early Stopping Min Delta': 0.001, 'Accuracy': 12.0,
                           'Loss': 1.42, 'Roc-Auc Score': 0.70, 'F1-Score': 0.55},
                'Step 10': {'Checkpoint': 'BERT_BASE', 'Batch Size': 16, 'Epoch': 10, 'Learning Rate': 1e-05,
                            'Early Stopping Patience': 2, 'Early Stopping Min Delta': 0.0001, 'Accuracy': 70.0,
                            'Loss': 0.55, 'Roc-Auc Score': 0.8, 'F1-Score': 0.61},
                'Step 11': {'Checkpoint': 'BERT_BASE', 'Batch Size': 16, 'Epoch': 15, 'Learning Rate': 1e-05,
                            'Early Stopping Patience': 2, 'Early Stopping Min Delta': 0.001, 'Accuracy': 62.5,
                            'Loss': 0.8, 'Roc-Auc Score': 0.62, 'F1-Score': 0.60},
                'Step 12': {'Checkpoint': 'BERT_BASE', 'Batch Size': 16, 'Epoch': 15, 'Learning Rate': 1e-05,
                            'Early Stopping Patience': 2, 'Early Stopping Min Delta': 0.0001, 'Accuracy': 55.0,
                            'Loss': 0.81, 'Roc-Auc Score': 0.53, 'F1-Score': 0.64},
                'Step 13': {'Checkpoint': 'BERT_BASE', 'Batch Size': 32, 'Epoch': 10, 'Learning Rate': 1e-05,
                            'Early Stopping Patience': 2, 'Early Stopping Min Delta': 0.001, 'Accuracy': 79.0,
                            'Loss': 0.58, 'Roc-Auc Score': 0.56, 'F1-Score': 0.37},
                'Step 14': {'Checkpoint': 'BERT_BASE', 'Batch Size': 32, 'Epoch': 10, 'Learning Rate': 1e-05,
                            'Early Stopping Patience': 2, 'Early Stopping Min Delta': 0.0001, 'Accuracy': 90.0,
                            'Loss': 0.7, 'Roc-Auc Score': 0.50, 'F1-Score': 0.44},
                'Step 15': {'Checkpoint': 'BERT_BASE', 'Batch Size': 32, 'Epoch': 15, 'Learning Rate': 1e-05,
                            'Early Stopping Patience': 2, 'Early Stopping Min Delta': 0.001, 'Accuracy': 35.0,
                            'Loss': 0.8, 'Roc-Auc Score': 0.45, 'F1-Score': 0.48},
                'Step 16': {'Checkpoint': 'BERT_BASE', 'Batch Size': 32, 'Epoch': 15, 'Learning Rate': 1e-05,
                            'Early Stopping Patience': 2, 'Early Stopping Min Delta': 0.0001, 'Accuracy': 48.0,
                            'Loss': 1.0, 'Roc-Auc Score': 0.44, 'F1-Score': 0.5}}

    @staticmethod
    def get_expected_calls_for_optimizer():
        """returns expected calls list for optimizer"""
        return [call(init_lr=1e-05, num_warmup_steps=0, num_train_steps=30),
                call(init_lr=1e-05, num_warmup_steps=0, num_train_steps=30),
                call(init_lr=1e-05, num_warmup_steps=0, num_train_steps=45),
                call(init_lr=1e-05, num_warmup_steps=0, num_train_steps=45),
                call(init_lr=1e-05, num_warmup_steps=0, num_train_steps=10),
                call(init_lr=1e-05, num_warmup_steps=0, num_train_steps=10),
                call(init_lr=1e-05, num_warmup_steps=0, num_train_steps=15),
                call(init_lr=1e-05, num_warmup_steps=0, num_train_steps=15),
                call(init_lr=1e-05, num_warmup_steps=0, num_train_steps=30),
                call(init_lr=1e-05, num_warmup_steps=0, num_train_steps=30),
                call(init_lr=1e-05, num_warmup_steps=0, num_train_steps=45),
                call(init_lr=1e-05, num_warmup_steps=0, num_train_steps=45),
                call(init_lr=1e-05, num_warmup_steps=0, num_train_steps=10),
                call(init_lr=1e-05, num_warmup_steps=0, num_train_steps=10),
                call(init_lr=1e-05, num_warmup_steps=0, num_train_steps=15),
                call(init_lr=1e-05, num_warmup_steps=0, num_train_steps=15)]

    @staticmethod
    def get_expected_calls_for_early_stopping():
        """returns expected calls list for optimizer"""
        return [call(patience=2, min_delta=0.001, monitor='train_loss'),
                call(patience=2, min_delta=0.0001, monitor='train_loss'),
                call(patience=2, min_delta=0.001, monitor='train_loss'),
                call(patience=2, min_delta=0.0001, monitor='train_loss'),
                call(patience=2, min_delta=0.001, monitor='train_loss'),
                call(patience=2, min_delta=0.0001, monitor='train_loss'),
                call(patience=2, min_delta=0.001, monitor='train_loss'),
                call(patience=2, min_delta=0.0001, monitor='train_loss'),
                call(patience=2, min_delta=0.001, monitor='train_loss'),
                call(patience=2, min_delta=0.0001, monitor='train_loss'),
                call(patience=2, min_delta=0.001, monitor='train_loss'),
                call(patience=2, min_delta=0.0001, monitor='train_loss'),
                call(patience=2, min_delta=0.001, monitor='train_loss'),
                call(patience=2, min_delta=0.0001, monitor='train_loss'),
                call(patience=2, min_delta=0.001, monitor='train_loss'),
                call(patience=2, min_delta=0.0001, monitor='train_loss')]

    @staticmethod
    def get_binary_data():
        """get dataset for binary classification"""
        train_data = \
            pd.DataFrame({'text': [
                "“Worry is a down payment on a problem you may never have'. Joyce Meyer.",
                "My roommate: it's okay that we can't spell because we have autocorrect. #terrible #firstworldprobs",
                "No but that's so cute. Atsu was probably shy about photos before but cherry helped her out uwu"
            ],
                'tur': [1, 0, 1]
            })
        val_data = pd.DataFrame({'text': [
            'if not has done is provoke her by tweeting shady shit and trying to be hard bitch begging for a fight',
            'Hey @user #Fields in #skibbereen give your online delivery service a horrible name.',
            'Why have #Emmerdale had to rob of having their first child together for that vile woman #bitter'
        ],
            'tur': [0, 1, 1]
        })
        test_data = pd.DataFrame({
            'text': ['My visit to hospital for care triggered #trauma from accident. Feeling symptoms of #depression',
                     '@user Welcome to #MPSVT! We are delighted to have you! #grateful #MPSVT #relationships',
                     'What makes you feel #joyful?'
                     ],
            'tur': [0, 1, 0]
        })
        return train_data, val_data, test_data
