"""Unit Tests for Text Classifier"""
import unittest
from unittest.mock import patch

from organon.ml.text_classification.domain.enums.model_checkpoints import ModelCheckpoints
from organon.ml.text_classification.domain.enums.selection_metrics import SelectionMetrics
from organon.ml.text_classification.domain.services.text_classification_service import TextClassificationService
from organon.ml.text_classification.services import text_classifier
from organon.ml.text_classification.services.text_classifier import TextClassifier
from organon.tests import test_helper


def _get_enum_side_effect(val, enum_type):
    if val is None:
        return val
    return enum_type[val]


class TextClassifierTests(unittest.TestCase):
    """Unit Test Class for Text Classifier"""

    def setUp(self) -> None:
        get_enum_patcher = patch(text_classifier.__name__ + ".get_enum",
                                 side_effect=_get_enum_side_effect)
        get_enum_patcher.start()
        self.addCleanup(get_enum_patcher.stop)
        self.text_classifier = TextClassifier(mdl_run_type="efficient", text_column_name="text",
                                              target_column_name="label")
        self.mock_service = test_helper.get_mock(self,
                                                 text_classifier.__name__ + "." + TextClassificationService.__name__)

    def test_set_grid_search_settings_parameters_none(self):
        """check if the grid search settings parameters has given properly by the user"""
        with self.assertRaisesRegex(ValueError, "Checkpoint List can not be empty"):
            self.text_classifier.set_grid_search_settings(models=[],
                                                          batch_sizes=[8], epochs=[10], learning_rates=[1e-5],
                                                          early_stopping_patience=[2], early_stopping_min_deltas=[1e-3])
        with self.assertRaisesRegex(ValueError, "Batch sizes List can not be empty"):
            self.text_classifier.set_grid_search_settings(models=['bert_base', 'roberta_base'],
                                                          batch_sizes=[], epochs=[10], learning_rates=[1e-5],
                                                          early_stopping_patience=[2], early_stopping_min_deltas=[1e-3])
        with self.assertRaisesRegex(ValueError, "Epochs List can not be empty"):
            self.text_classifier.set_grid_search_settings(models=['bert_base', 'roberta_base'],
                                                          batch_sizes=[10], epochs=[], learning_rates=[1e-5],
                                                          early_stopping_patience=[2], early_stopping_min_deltas=[1e-3])
        with self.assertRaisesRegex(ValueError, "Learning rates List can not be empty"):
            self.text_classifier.set_grid_search_settings(models=['bert_base', 'roberta_base'],
                                                          batch_sizes=[10], epochs=[10], learning_rates=[],
                                                          early_stopping_patience=[2], early_stopping_min_deltas=[1e-3])
        with self.assertRaisesRegex(ValueError, "Stopping patience List can not be empty"):
            self.text_classifier.set_grid_search_settings(models=['bert_base', 'roberta_base'],
                                                          batch_sizes=[10], epochs=[10], learning_rates=[1e-5],
                                                          early_stopping_patience=[], early_stopping_min_deltas=[1e-3])
        with self.assertRaisesRegex(ValueError, "Stopping min deltas List can not be empty"):
            self.text_classifier.set_grid_search_settings(models=['bert_base', 'roberta_base'],
                                                          batch_sizes=[10], epochs=[10], learning_rates=[1e-5],
                                                          early_stopping_patience=[2], early_stopping_min_deltas=[])

    def test_set_optimizer_settings_parameters_passed_through_service(self):
        """using set optimizer settings method, check if the parameters are true"""
        self.text_classifier.set_optimizer_settings(learning_rate=1e-4, early_stopping=5, steps_per_epoch=0,
                                                    early_stopping_min_delta=1e-8)
        self.text_classifier.fit(data="some_data")
        service_opt_settings = self.mock_service.call_args[0][0].opt_settings
        self.assertEqual(1e-4, service_opt_settings.learning_rate)
        self.assertEqual(5, service_opt_settings.early_stopping)
        self.assertEqual(0, service_opt_settings.steps_per_epoch)
        self.assertEqual(1e-8, service_opt_settings.early_stopping_min_delta)

    def test_set_mdl_settings_parameters_passed_through_service(self):
        """using set mdl settings method, check if the parameters are true"""
        self.text_classifier.set_mdl_settings(batch_size=32, epoch=10)
        self.text_classifier.fit(data="some_data")
        service_mdl_settings = self.mock_service.call_args[0][0].mdl_param_settings
        self.assertEqual(32, service_mdl_settings.batch_size)
        self.assertEqual(10, service_mdl_settings.epochs)

    def test_set_tokenizer_settings_parameters_passed_through_service(self):
        """using set tokenizer settings method, check if the parameters are true"""
        self.text_classifier.set_tokenizer_settings(padding="pad", truncation=False, max_length=256)
        self.text_classifier.fit(data="some_data")
        service_tokenizer_settings = self.mock_service.call_args[0][0].tokenizer_settings
        self.assertEqual("pad", service_tokenizer_settings.padding)
        self.assertEqual(False, service_tokenizer_settings.truncation)
        self.assertEqual(256, service_tokenizer_settings.max_length)

    def test_set_grid_settings_parameters_passed_through_service(self):
        """using set grid search settings method, check if the parameters are true"""
        self.text_classifier.set_grid_search_settings(models=['bert_base', 'roberta_base'],
                                                      batch_sizes=[10, 32], epochs=[10, 15], learning_rates=[1e-5],
                                                      early_stopping_patience=[2], early_stopping_min_deltas=[1e-10],
                                                      model_selection_metric="f1_score")
        self.text_classifier.fit(data="some_data")
        service_grd_src_settings = self.mock_service.call_args[0][0].grid_src_settings
        self.assertEqual([ModelCheckpoints.BERT_BASE, ModelCheckpoints.ROBERTA_BASE], service_grd_src_settings.models)
        self.assertEqual([10, 15], service_grd_src_settings.epochs)
        self.assertEqual([10, 32], service_grd_src_settings.batch_sizes)
        self.assertEqual([1e-5], service_grd_src_settings.learning_rates)
        self.assertEqual([2], service_grd_src_settings.early_stopping_patiences)
        self.assertEqual([1e-10], service_grd_src_settings.early_stopping_min_deltas)
        self.assertEqual(SelectionMetrics.F1_SCORE, service_grd_src_settings.model_selection_metric)
