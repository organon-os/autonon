"""Includes unittests for ensembling_helper module"""
import unittest

import numpy as np
import pandas as pd

from organon.ml.modelling.algorithms.core.enums.modeller_type import ModellerType
from organon.ml.modelling.algorithms.helpers import ensembling_helper
from organon.tests import test_helper


class EnsemblingHelperTestCase(unittest.TestCase):
    """Unittest class for ensembling_helper"""

    def setUp(self) -> None:
        self.mock_random_choice = test_helper.get_mock(self, ensembling_helper.__name__ + ".np.random.choice")
        self.mock_random_choice.side_effect = lambda _list, *args: min(_list)

    def test_get_voting_prediction_with_classifier(self):
        """tests get_voting_prediction with classifier type"""
        all_preds_df = pd.DataFrame({
            "pred_1": [2, 3, 2],
            "pred_2": [1, 3, 1],
            "pred_3": [2, 2, 1],
        })
        ret_series = ensembling_helper.get_voting_prediction(all_preds_df, modeller_type=ModellerType.CLASSIFIER)
        self.assertEqual([2, 3, 1], ret_series.tolist())

    def test_get_voting_prediction_with_regressor(self):
        """tests get_voting_prediction with regressor type"""
        all_preds_df = pd.DataFrame({
            "pred_1": [2, 3, 2],
            "pred_2": [1, 3, 1],
            "pred_3": [2, 2, 1],
        })
        ret_series = ensembling_helper.get_voting_prediction(all_preds_df, modeller_type=ModellerType.REGRESSOR)
        self.assertEqual([5 / 3, 8 / 3, 4 / 3], ret_series.tolist())

    def test_get_voting_classifier_prediction(self):
        """tests get_voting_classifier_prediction"""
        all_preds_df = pd.DataFrame({
            "pred_1": [2, 3, 2],
            "pred_2": [1, 3, 1],
            "pred_3": [2, 2, 1],
        })
        ret_series = ensembling_helper.get_voting_classifier_prediction(all_preds_df)
        self.assertEqual([2, 3, 1], ret_series.tolist())

    def test_get_voting_classifier_prediction_equal_number_of_occurrences(self):
        """tests get_voting_classifier_prediction when there is an equal number of occurrences for two values"""
        all_preds_df = pd.DataFrame({
            "pred_1": [2, 3, 2],
            "pred_2": [1, 3, 1],
            "pred_3": [2, 2, 1],
            "pred_4": [1, 2, 1],
        })
        ret_series = ensembling_helper.get_voting_classifier_prediction(all_preds_df)
        self.assertEqual([1, 2, 1], ret_series.tolist())

    def test_get_voting_predict_proba(self):
        """tests get_voting_predict_proba"""
        # pylint: disable=no-self-use
        all_pred_proba_dfs = [
            pd.DataFrame({
                "pred_1": [0.1, 0.3, 0.8],
                "pred_2": [0.2, 0.3, 0.1],
                "pred_3": [0.7, 0.4, 0.1],
            }),
            pd.DataFrame({
                "pred_1": [0.3, 0.4, 0.2],
                "pred_2": [0.5, 0.2, 0.5],
                "pred_3": [0.2, 0.4, 0.3],
            }),
        ]
        ret_df = ensembling_helper.get_voting_predict_proba(all_pred_proba_dfs)
        np.testing.assert_almost_equal(np.array([[0.2, 0.35, 0.5], [0.35, 0.25, 0.3], [0.45, 0.4, 0.2]]),
                                       ret_df.T.values)

    def test_get_voting_regressor_prediction(self):
        """tests get_voting_regressor_prediction"""
        all_preds_df = pd.DataFrame({
            "pred_1": [2, 3, 2],
            "pred_2": [1, 3, 1],
            "pred_3": [2, 2, 1],
        })
        ret_series = ensembling_helper.get_voting_regressor_prediction(all_preds_df)
        self.assertEqual([5 / 3, 8 / 3, 4 / 3], ret_series.tolist())


if __name__ == '__main__':
    unittest.main()
