"""Includes tests for MultiLayerPerceptronRegressor class."""
import unittest
from unittest.mock import patch, MagicMock
import numpy as np
import pandas as pd
from sklearn.neural_network import MLPRegressor

from organon.ml.modelling.algorithms.regressors import multi_layer_perceptron_regressor
from organon.ml.modelling.algorithms.regressors.multi_layer_perceptron_regressor import MultiLayerPerceptronRegressor


class MultiLayerPerceptronRegressorTestCase(unittest.TestCase):
    """Test class for MultiLayerPerceptronRegressor class."""

    def setUp(self) -> None:
        mlp_regressor_patcher = patch(
            multi_layer_perceptron_regressor.__name__ + "." + MLPRegressor.__name__)
        self.mock_mlp_regressor = mlp_regressor_patcher.start()
        self.addCleanup(mlp_regressor_patcher.stop)
        target = np.random.uniform(low=0, high=5, size=(500,))
        x_1 = np.random.uniform(low=0, high=10, size=(500,))
        x_2 = np.random.uniform(low=10, high=30, size=(500,))
        self.data = pd.DataFrame({"x1": x_1, "x2": x_2})
        self.df_target = pd.DataFrame({"target": target})

    def test_mlp_regressor_object_defaults(self):
        """Test for MultiLayerPerceptronRegressor with default params"""
        MultiLayerPerceptronRegressor()
        self.mock_mlp_regressor.assert_called()

    def test_mlp_regressor_object_with_params(self):
        """Test for MultiLayerPerceptronRegressor with  params"""

        params = {"hidden_layer_sizes": (100,),
                  "activation": "relu"}
        MultiLayerPerceptronRegressor(**params)
        self.mock_mlp_regressor.assert_called_with(**params)

    def test_mlp_regressor_fit(self):
        """Test for MultiLayerPerceptronRegressor fit method."""

        regressor = MultiLayerPerceptronRegressor()
        regressor.fit(self.data, self.df_target, sample_weight=None)
        self.mock_mlp_regressor.return_value.fit.assert_called_with(self.data, self.df_target["target"],
                                                                    sample_weight=None)

    def test_mlp_regressor_predict(self):
        """Test for MultiLayerPerceptronRegressor predict method."""

        regressor = MultiLayerPerceptronRegressor()
        regressor.is_fitted = MagicMock(return_value=True)
        regressor.predict(self.df_target)
        self.mock_mlp_regressor.return_value.predict.assert_called_with(self.df_target)
