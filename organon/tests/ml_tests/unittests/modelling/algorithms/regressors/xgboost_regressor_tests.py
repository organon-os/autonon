"""Includes tests for XGBoostRegressor class."""
import unittest
from unittest.mock import patch, MagicMock
import numpy as np
import pandas as pd
from xgboost import XGBRegressor

from organon.ml.modelling.algorithms.regressors import xgboost_regressor
from organon.ml.modelling.algorithms.regressors.xgboost_regressor import XGBoostRegressor


class XGBoostRegressorTestCase(unittest.TestCase):
    """Test class for XGBoostRegressor class."""

    def setUp(self) -> None:
        xgboost_regressor_patcher = patch(
            xgboost_regressor.__name__ + "." + XGBRegressor.__name__)
        self.mock_xgboost_regressor = xgboost_regressor_patcher.start()
        self.addCleanup(xgboost_regressor_patcher.stop)
        target = np.random.uniform(low=0, high=5, size=(500,))
        x_1 = np.random.uniform(low=0, high=10, size=(500,))
        x_2 = np.random.uniform(low=10, high=30, size=(500,))
        self.data = pd.DataFrame({"x1": x_1, "x2": x_2})
        self.df_target = pd.DataFrame({"target": target})

    def test_xgboost_regressor_object_defaults(self):
        """Test for XGBoostRegressor with default params"""
        XGBoostRegressor()
        self.mock_xgboost_regressor.assert_called()

    def test_xgboost_regressor_object_with_params(self):
        """Test for XGBoostRegressor with  params"""

        params = {"learning_rate": 0.01,
                  "n_estimators": 200,
                  "subsample": 0.8}
        XGBoostRegressor(**params)
        self.mock_xgboost_regressor.assert_called_with(**params)

    def test_xgboost_regressor_fit(self):
        """Test for XGBoostRegressor fit method."""

        regressor = XGBoostRegressor()
        regressor.fit(self.data, self.df_target, sample_weight=None)
        self.mock_xgboost_regressor.return_value.fit.assert_called_with(self.data, self.df_target["target"],
                                                                        sample_weight=None)

    def test_xgboost_regressor_predict(self):
        """Test for XGBoostRegressor predict method."""

        regressor = XGBoostRegressor()
        regressor.is_fitted = MagicMock(return_value=True)
        regressor.predict(self.df_target)
        self.mock_xgboost_regressor.return_value.predict.assert_called_with(self.df_target)
