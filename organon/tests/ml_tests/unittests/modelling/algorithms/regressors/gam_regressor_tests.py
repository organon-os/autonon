"""Includes unittests for GamRegressor"""
import unittest
from unittest.mock import patch, MagicMock

import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LinearRegression, Lasso
from sklearn.model_selection import GridSearchCV

from organon.fl.core.exceptionhandling.known_exception import KnownException
from organon.ml.modelling.algorithms.core.abstractions import gam_mixin
from organon.ml.modelling.algorithms.regressors import gam_regressor
from organon.ml.modelling.algorithms.regressors.gam_regressor import GamRegressor
from organon.ml.preprocessing.services.preprocessor import Preprocessor
from organon.tests import test_helper


class GamRegressorTestCase(unittest.TestCase):
    """Unittest class for GamRegressor"""

    def setUp(self) -> None:
        module_name = gam_regressor.__name__
        self.mock_linear_reg = test_helper.get_mock(self, module_name + "." + LinearRegression.__name__)
        self.mock_linear_reg.return_value.coef_ = np.array([[0.8, 1.0]])

        self.mock_random_forest_regressor = test_helper.get_mock(self,
                                                                 module_name + "." + RandomForestRegressor.__name__)
        self.mock_random_forest_regressor.return_value.feature_importances_ = np.array([[0.7], [0.5]])

        mixin_module_name = gam_mixin.__name__

        self.mock_preprocessor = test_helper.get_mock(self, mixin_module_name + "." + Preprocessor.__name__)
        self.mock_cc_service = MagicMock()
        self.mock_cc_service.transform.side_effect = \
            lambda frame: frame.rename(columns={col: f"Z_{col}" for col in frame.columns})
        self.mock_preprocessor.get_coarse_class_service.return_value = self.mock_cc_service

        self.mock_lasso = test_helper.get_mock(self, mixin_module_name + "." + Lasso.__name__)
        self.mock_lasso.return_value.coef_ = np.array([[0.5], [0.7]])

        self.mock_grid_search = test_helper.get_mock(self, mixin_module_name + "." + GridSearchCV.__name__)
        self.mock_grid_search.return_value.best_params_ = {"alpha": 1}
        self.mock_grid_search.return_value.best_estimator_.coef_ = np.array([0.5, 0.7])

        self.train_data = pd.DataFrame({"col1": [1, 2, 3, 4, 5], "col2": [3, 4, 5, 6, 7]})
        self.target_data = pd.DataFrame({"target_col": [3, 7, 3, 8, 1]})

        vif_patcher = patch(gam_mixin.__name__ + ".variance_inflation_factor")
        self.vif_func_mock = vif_patcher.start()
        self.addCleanup(vif_patcher.stop)
        self.vif_func_mock.side_effect = [0.5, 0.7]

    def test_fit_with_no_columns_to_eliminate(self):
        """tests fit method when no columns will be eliminated"""
        regressor = GamRegressor(grid_search_param_set=[1, 2, 3], grid_search_cv=20,
                                 grid_search_scoring="dummy", max_vif_limit=10)
        regressor.fit(self.train_data, self.target_data)
        self.assertEqual([1, 2, 3], self.mock_grid_search.call_args.args[1][0]["alpha"])
        self.assertEqual({"cv": 20, "scoring": "dummy"}, self.mock_grid_search.call_args.kwargs)
        self.assertEqual(1, regressor.best_alpha)
        self.assertEqual(["Z_col1", "Z_col2"], regressor.initial_selected_features)
        self.assertEqual(["Z_col1", "Z_col2"], regressor.selected_features)
        self.assertEqual(self.mock_linear_reg.return_value, regressor.final_model)
        # vif values are small. randomforestregressor should not be called
        self.mock_random_forest_regressor.return_value.fit.assert_not_called()

    def test_fit_with_coarse_class_rejected_list(self):
        """tests fit method when some of the columns are in rejected list of coarse class service"""

        self.train_data = pd.DataFrame({"col1": [1, 2, 3, 4, 5], "col2": [3, 7, 3, 6, 1],
                                        "col3": [3, 2, 5, 1, 7]})
        self.mock_cc_service.fit_output.rejected_list = ["col1"]
        regressor = GamRegressor(grid_search_param_set=[1, 2, 3], grid_search_cv=20,
                                 grid_search_scoring="dummy", max_vif_limit=10)
        regressor.fit(self.train_data, self.target_data)
        self.assertEqual([1, 2, 3], self.mock_grid_search.call_args.args[1][0]["alpha"])
        self.assertEqual({"cv": 20, "scoring": "dummy"}, self.mock_grid_search.call_args.kwargs)
        self.assertEqual(1, regressor.best_alpha)
        self.assertCountEqual(["Z_col2", "Z_col3"], regressor.initial_selected_features)
        self.assertCountEqual(["Z_col2", "Z_col3"], regressor.selected_features)
        self.assertEqual(self.mock_linear_reg.return_value, regressor.final_model)
        # vif values are small. randomforestregressor should not be called
        self.mock_random_forest_regressor.return_value.fit.assert_not_called()

    def test_fit_with_one_high_vif_column(self):
        """tests fit method -
        one of the columns have a high vif in first iteration and should be eliminated"""
        lasso_mock_1 = MagicMock()
        lasso_mock_1.coef_ = np.array([[0.5], [0.7]])
        lasso_mock_2 = MagicMock()
        lasso_mock_2.coef_ = np.array([[0.8]])
        self.mock_lasso.side_effect = [None, lasso_mock_1, lasso_mock_2]

        self.vif_func_mock.side_effect = [15, 0.7]

        regressor = GamRegressor(grid_search_param_set=[1, 2, 3], grid_search_cv=20,
                                 grid_search_scoring="dummy", max_vif_limit=10)
        regressor.fit(self.train_data, self.target_data)
        self.assertEqual([1, 2, 3], self.mock_grid_search.call_args.args[1][0]["alpha"])
        self.assertEqual({"cv": 20, "scoring": "dummy"}, self.mock_grid_search.call_args.kwargs)
        self.assertEqual(1, regressor.best_alpha)
        self.assertEqual(["Z_col1", "Z_col2"], regressor.initial_selected_features)
        self.assertEqual(["Z_col2"], regressor.selected_features)
        self.assertEqual(self.mock_linear_reg.return_value, regressor.final_model)
        self.mock_random_forest_regressor.return_value.fit.assert_not_called()

    def test_fit_with_two_negative_coef_column_in_first_iteration(self):
        """tests fit method - feature with less importance should be eliminated if both features
        have high vif values"""
        lasso_mock_1 = MagicMock()
        lasso_mock_1.coef_ = np.array([[0.5], [0.7]])
        lasso_mock_2 = MagicMock()
        lasso_mock_2.coef_ = np.array([[0.8]])
        self.mock_lasso.side_effect = [None, lasso_mock_1, lasso_mock_2]

        self.vif_func_mock.side_effect = [15, 20]
        self.mock_random_forest_regressor.return_value.feature_importances_ = np.array([[0.7], [0.5]])

        regressor = GamRegressor(grid_search_param_set=[1, 2, 3], grid_search_cv=20,
                                 grid_search_scoring="dummy", max_vif_limit=10)
        regressor.fit(self.train_data, self.target_data)
        self.assertEqual([1, 2, 3], self.mock_grid_search.call_args.args[1][0]["alpha"])
        self.assertEqual({"cv": 20, "scoring": "dummy"}, self.mock_grid_search.call_args.kwargs)
        self.assertEqual(1, regressor.best_alpha)
        self.assertEqual(["Z_col1", "Z_col2"], regressor.initial_selected_features)

        # col2 should be eliminated since its feature_importance is less than col1 (set above)
        self.assertEqual(["Z_col1"], regressor.selected_features)

        # randomforestregressor should be called once
        self.mock_random_forest_regressor.return_value.fit.assert_called_once()
        self.assertCountEqual(["Z_col1", "Z_col2"],
                              self.mock_random_forest_regressor.return_value.fit.call_args.args[0].columns.tolist())

    def test_fit_initial_selected_features_when_one_coef_zero(self):
        """tests if initial_selected_features are returned properly when some of the columns are eliminated
         after grid search with lasso"""
        self.mock_grid_search.return_value.best_estimator_.coef_ = np.array([0, 0.7])
        classifier = GamRegressor(grid_search_param_set=[1, 2, 3], grid_search_cv=20,
                                  grid_search_scoring="dummy", max_vif_limit=10)
        classifier.fit(self.train_data, self.target_data)
        self.assertEqual(["Z_col2"],
                         classifier.initial_selected_features)  # col1 should be eliminated since its coef is 0
        self.assertEqual(["Z_col1", "Z_col2"], classifier.selected_features)

    def test_fit_no_significant_features_error(self):
        """tests if exception is raised when all coefficients are zero after first lasso"""
        self.mock_lasso.return_value.coef_ = np.array([0, 0])
        classifier = GamRegressor()
        with self.assertRaisesRegex(KnownException, "No significant features found. Cannot build model."):
            classifier.fit(self.train_data, self.target_data)

    def test_predict_before_fit_error(self):
        """tests if exception is raised when predict is called without calling fit first"""
        with self.assertRaisesRegex(ValueError, "Modeller not fitted yet"):
            GamRegressor().predict(pd.DataFrame({"a": [0, 1, 1, 0]}))

    def test_predict_after_fit(self):
        """tests predict after fitting"""
        self.train_data = pd.DataFrame({"col1": [1, 2, 3, 4, 5], "col2": [3, 7, 3, 6, 1],
                                        "col3": [3, 2, 5, 1, 7]})
        self.mock_cc_service.fit_output.rejected_list = ["col1"]
        regressor = GamRegressor(grid_search_param_set=[1, 2, 3], grid_search_cv=20,
                                 grid_search_scoring="dummy", max_vif_limit=10)
        regressor.fit(self.train_data, self.target_data)
        self.assertEqual([1, 2, 3], self.mock_grid_search.call_args.args[1][0]["alpha"])
        self.assertEqual({"cv": 20, "scoring": "dummy"}, self.mock_grid_search.call_args.kwargs)
        self.assertEqual(1, regressor.best_alpha)
        self.assertCountEqual(["Z_col2", "Z_col3"], regressor.initial_selected_features)
        self.assertCountEqual(["Z_col2", "Z_col3"], regressor.selected_features)
        self.assertEqual(self.mock_linear_reg.return_value, regressor.final_model)
        self.mock_linear_reg.return_value.predict.return_value = np.random.rand(len(self.train_data))
        pred_df = regressor.predict(self.train_data)
        self.assertEqual(list(self.mock_linear_reg.return_value.predict.return_value),
                         pred_df["prediction"].to_list())
        self.assertCountEqual(["Z_col2", "Z_col3"],
                              self.mock_linear_reg.return_value.predict.call_args.args[0].columns.to_list())


if __name__ == '__main__':
    unittest.main()
