"""Includes tests for VotingEnsembleRegressor"""
import unittest
from unittest.mock import MagicMock

import numpy as np
import pandas as pd

from organon.ml.modelling.algorithms.classifiers.gbm_classifier import GBMClassifier
from organon.ml.modelling.algorithms.regressors import voting_ensemble_regressor
from organon.ml.modelling.algorithms.regressors.gbm_regressor import GBMRegressor
from organon.ml.modelling.algorithms.regressors.rf_regressor import RFRegressor
from organon.ml.modelling.algorithms.regressors.voting_ensemble_regressor import VotingEnsembleRegressor
from organon.ml.modelling.algorithms.regressors.xgboost_regressor import XGBoostRegressor
from organon.tests import test_helper


class VotingEnsembleRegressorTestCase(unittest.TestCase):
    """Unittest class for VotingEnsembleRegressor"""

    def setUp(self) -> None:
        mock_estimator_1 = MagicMock(spec=GBMRegressor)
        mock_estimator_1.predict.return_value = pd.DataFrame({"target": [1.5, 2.2, 3.4]})

        mock_estimator_2 = MagicMock(spec=XGBoostRegressor)
        mock_estimator_2.predict.return_value = pd.DataFrame({"target": [1.2, 3.5, 2.9]})

        mock_estimator_3 = MagicMock(spec=RFRegressor)
        mock_estimator_3.predict.return_value = pd.DataFrame({"target": [2.2, 3.9, 1.1]})

        self.mock_voting_regressor = \
            test_helper.get_mock(self, voting_ensemble_regressor.__name__+".get_voting_regressor_prediction")
        self.estimators = [mock_estimator_1, mock_estimator_2, mock_estimator_3]

    def test_fit_prefit(self):
        """tests fit when prefit=True"""
        clf = VotingEnsembleRegressor(estimators=self.estimators, prefit=True)
        clf.fit(pd.DataFrame({"a": [1.1, 2.1, 3.2]}), pd.DataFrame({"target": [1.2, 2.3, 3.8]}))
        for estimator in self.estimators:
            estimator.fit.assert_not_called()

    def test_fit_prefit_false(self):
        """tests fit when prefit=False"""
        clf = VotingEnsembleRegressor(estimators=self.estimators, prefit=False)
        train_df = pd.DataFrame({"a": [1.1, 2.5, 3.5]})
        target_df = pd.DataFrame({"a": [1.2, 2.1, 3.2]})
        clf.fit(train_df, target_df)
        for estimator in self.estimators:
            estimator.fit.assert_called_with(train_df, target_df)

    def test_fit_no_estimator_error(self):
        """tests error when fit called without setting estimators"""
        clf = VotingEnsembleRegressor()
        with self.assertRaisesRegex(ValueError, "Estimators not given"):
            clf.fit(pd.DataFrame({"a": [1, 2, 3]}), pd.DataFrame({"target": [1, 2, 3]}))

    def test_fit_no_estimator_error_empty_list(self):
        """tests error when fit called without setting estimators"""
        clf = VotingEnsembleRegressor(estimators=[])
        with self.assertRaisesRegex(ValueError, "Estimators not given"):
            clf.fit(pd.DataFrame({"a": [1, 2, 3]}), pd.DataFrame({"target": [1, 2, 3]}))

    def test_fit_invalid_estimator_error(self):
        """tests if error is raised when one of the estimators is not a classifier"""
        clf = VotingEnsembleRegressor(estimators=[GBMRegressor(), GBMClassifier()])
        with self.assertRaisesRegex(ValueError, "All estimators should be an instance of BaseRegressor"):
            clf.fit(pd.DataFrame({"a": [1, 2, 3]}), pd.DataFrame({"target": [1, 2, 3]}))

    def test_predict(self):
        """tests predict method"""
        clf = VotingEnsembleRegressor(estimators=self.estimators, prefit=True)
        self.mock_voting_regressor.return_value = pd.Series([4.9 / 3, 3.2, 7.4 / 3])
        clf.fit(pd.DataFrame({"a": [1.2, 2.1, 3.1]}), pd.DataFrame({"target": [1.3, 2.4, 3.3]}))
        ret = clf.predict(pd.DataFrame({"a": [3.2, 4.1, 5.0]}))
        all_preds_df = self.mock_voting_regressor.call_args.args[0]
        np.testing.assert_almost_equal(
            pd.DataFrame({
                "pred_0": self.estimators[0].predict.return_value.iloc[:, 0],
                "pred_1": self.estimators[1].predict.return_value.iloc[:, 0],
                "pred_2": self.estimators[2].predict.return_value.iloc[:, 0]
            }).values, all_preds_df)
        self.assertEqual(["target"], ret.columns.tolist())
        self.assertEqual([4.9 / 3, 3.2, 7.4 / 3], ret["target"].values.tolist())

    if __name__ == '__main__':
        unittest.main()
