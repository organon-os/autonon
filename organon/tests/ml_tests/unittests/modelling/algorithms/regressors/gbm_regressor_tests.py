"""Includes tests for GBMRegressor class."""
import unittest
from unittest.mock import patch, MagicMock
import numpy as np
import pandas as pd
from sklearn.ensemble import GradientBoostingRegressor

from organon.ml.modelling.algorithms.regressors import gbm_regressor
from organon.ml.modelling.algorithms.regressors.gbm_regressor import GBMRegressor


class GBMRegressorTestCase(unittest.TestCase):
    """Test class for GBMRegressor class."""

    def setUp(self) -> None:
        gbm_regressor_patcher = patch(
            gbm_regressor.__name__ + "." + GradientBoostingRegressor.__name__)
        self.mock_gbm_regressor = gbm_regressor_patcher.start()
        self.addCleanup(gbm_regressor_patcher.stop)
        target = np.random.uniform(low=0, high=5, size=(500,))
        x_1 = np.random.uniform(low=0, high=10, size=(500,))
        x_2 = np.random.uniform(low=10, high=30, size=(500,))
        self.data = pd.DataFrame({"x1": x_1, "x2": x_2})
        self.df_target = pd.DataFrame({"target": target})

    def test_gbm_regressor_object_defaults(self):
        """Test for GBMRegressor with default params"""
        GBMRegressor()
        self.mock_gbm_regressor.assert_called()

    def test_gbm_regressor_object_with_params(self):
        """Test for GBMRegressor with  params"""

        params = {"learning_rate": 0.01,
                  "n_estimators": 200,
                  "subsample": 0.8}
        GBMRegressor(**params)
        self.mock_gbm_regressor.assert_called_with(**params)

    def test_gbm_regressor_fit(self):
        """Test for GBMRegressor fit method."""

        regressor = GBMRegressor()
        regressor.fit(self.data, self.df_target, sample_weight=None)
        self.mock_gbm_regressor.return_value.fit.assert_called_with(self.data, self.df_target["target"],
                                                                    sample_weight=None)

    def test_gbm_regressor_predict(self):
        """Test for GBMRegressor predict method."""

        regressor = GBMRegressor()
        regressor.is_fitted = MagicMock(return_value=True)
        regressor.predict(self.df_target)
        self.mock_gbm_regressor.return_value.predict.assert_called_with(self.df_target)
