"""Includes tests for LightGBMRegressor class."""
import unittest
from unittest.mock import patch, MagicMock
import numpy as np
import pandas as pd
from lightgbm import LGBMRegressor

from organon.ml.modelling.algorithms.regressors import lightgbm_regressor
from organon.ml.modelling.algorithms.regressors.lightgbm_regressor import LightGBMRegressor


class LightGBMRegressorTestCase(unittest.TestCase):
    """Test class for LightGBMRegressor class."""

    def setUp(self) -> None:
        lightgbm_regressor_patcher = patch(
            lightgbm_regressor.__name__ + "." + LGBMRegressor.__name__)
        self.mock_light_gbm_regressor = lightgbm_regressor_patcher.start()
        self.addCleanup(lightgbm_regressor_patcher.stop)
        target = np.random.uniform(low=0, high=5, size=(500,))
        x_1 = np.random.uniform(low=0, high=10, size=(500,))
        x_2 = np.random.uniform(low=10, high=30, size=(500,))
        self.data = pd.DataFrame({"x1": x_1, "x2": x_2})
        self.df_target = pd.DataFrame({"target": target})

    def test_lightgbm_regressor_object_defaults(self):
        """Test for LightGBMRegressor with default params"""
        LightGBMRegressor()
        self.mock_light_gbm_regressor.assert_called()

    def test_lightgbm_regressor_object_with_params(self):
        """Test for LightGBMRegressor with  params"""

        params = {"learning_rate": 0.01,
                  "n_estimators": 200,
                  "num_leaves": 10,
                  "max_depth": 3}
        LightGBMRegressor(**params)
        self.mock_light_gbm_regressor.assert_called_with(**params)

    def test_lightgbm_regressor_fit(self):
        """Test for LightGBMRegressor fit method."""

        regressor = LightGBMRegressor()
        regressor.fit(self.data, self.df_target, sample_weight=None)
        self.mock_light_gbm_regressor.return_value.fit.assert_called_with(self.data, self.df_target["target"],
                                                                          sample_weight=None)

    def test_lightgbm_regressor_predict(self):
        """Test for LightGBMRegressor predict method."""

        regressor = LightGBMRegressor()
        regressor.is_fitted = MagicMock(return_value=True)
        regressor.predict(self.df_target)
        self.mock_light_gbm_regressor.return_value.predict.assert_called_with(self.df_target)
