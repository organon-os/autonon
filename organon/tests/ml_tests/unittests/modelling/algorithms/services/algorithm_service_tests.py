"""Includes tests for AlgortihmService class."""
import unittest
from unittest.mock import patch

from organon.ml.modelling.algorithms.classifiers.gbm_classifier import GBMClassifier
from organon.ml.modelling.algorithms.classifiers.lightgbm_classifier import LightGBMClassifier
from organon.ml.modelling.algorithms.classifiers.logistic_regression_classifier import LogisticRegressionClassifier
from organon.ml.modelling.algorithms.classifiers.multi_layer_perceptron_classifier import MultiLayerPerceptronClassifier
from organon.ml.modelling.algorithms.classifiers.rf_classifier import RFClassifier
from organon.ml.modelling.algorithms.classifiers.xgboost_classifier import XGBoostClassifier
from organon.ml.modelling.algorithms.core.enums.modeller import Modeller
from organon.ml.modelling.algorithms.regressors.gbm_regressor import GBMRegressor
from organon.ml.modelling.algorithms.regressors.lasso_regressor import LassoRegressor
from organon.ml.modelling.algorithms.regressors.lightgbm_regressor import LightGBMRegressor
from organon.ml.modelling.algorithms.regressors.multi_layer_perceptron_regressor import MultiLayerPerceptronRegressor
from organon.ml.modelling.algorithms.regressors.rf_regressor import RFRegressor
from organon.ml.modelling.algorithms.regressors.ridge_regressor import RidgeRegressor
from organon.ml.modelling.algorithms.regressors.xgboost_regressor import XGBoostRegressor
from organon.ml.modelling.algorithms.services import ml_application_operations
from organon.ml.modelling.algorithms.services.algorithm_service import AlgorithmService


class AlgorithmServiceTestCase(unittest.TestCase):
    """Test class for AlgorithmService class."""

    @classmethod
    def setUpClass(cls) -> None:
        cls.rf_classifier_patcher = patch(
            ml_application_operations.__name__ + "." + RFClassifier.__name__)
        cls.mock_rf_classifier = cls.rf_classifier_patcher.start()

        cls.gbm_classifier_patcher = patch(
            ml_application_operations.__name__ + "." + GBMClassifier.__name__)
        cls.mock_gbm_classifier = cls.gbm_classifier_patcher.start()

        cls.lightgbm_classifier_patcher = patch(
            ml_application_operations.__name__ + "." + LightGBMClassifier.__name__)
        cls.mock_lightgbm_classifier = cls.lightgbm_classifier_patcher.start()

        cls.lr_classifier_patcher = patch(
            ml_application_operations.__name__ + "." + LogisticRegressionClassifier.__name__)
        cls.mock_lr_classifier = cls.lr_classifier_patcher.start()

        cls.xgboost_classifier_patcher = patch(
            ml_application_operations.__name__ + "." + XGBoostClassifier.__name__)
        cls.mock_xgboost_classifier = cls.xgboost_classifier_patcher.start()

        cls.multi_layer_perceptron_classifier_patcher = patch(
            ml_application_operations.__name__ + "." + MultiLayerPerceptronClassifier.__name__)
        cls.mock_multi_layer_perceptron_classifier = cls.multi_layer_perceptron_classifier_patcher.start()

        cls.gbm_regressor_patcher = patch(
            ml_application_operations.__name__ + "." + GBMRegressor.__name__)
        cls.mock_gbm_regressor = cls.gbm_regressor_patcher.start()

        cls.lasso_regressor_patcher = patch(
            ml_application_operations.__name__ + "." + LassoRegressor.__name__)
        cls.mock_lasso_regressor = cls.lasso_regressor_patcher.start()

        cls.lightgbm_regressor_patcher = patch(
            ml_application_operations.__name__ + "." + LightGBMRegressor.__name__)
        cls.mock_lightgbm_regressor = cls.lightgbm_regressor_patcher.start()

        cls.mlp_regressor_patcher = patch(
            ml_application_operations.__name__ + "." + MultiLayerPerceptronRegressor.__name__)
        cls.mock_mlp_regressor = cls.mlp_regressor_patcher.start()

        cls.rf_regressor_patcher = patch(
            ml_application_operations.__name__ + "." + RFRegressor.__name__)
        cls.mock_rf_regressor = cls.rf_regressor_patcher.start()

        cls.ridge_regressor_patcher = patch(
            ml_application_operations.__name__ + "." + RidgeRegressor.__name__)
        cls.mock_ridge_regressor = cls.ridge_regressor_patcher.start()

        cls.xgboost_regressor_patcher = patch(
            ml_application_operations.__name__ + "." + XGBoostRegressor.__name__)
        cls.mock_xgboost_regressor = cls.xgboost_regressor_patcher.start()

        cls.service = AlgorithmService()

    def tearDown(self) -> None:
        self.rf_classifier_patcher.stop()
        self.gbm_classifier_patcher.stop()
        self.lightgbm_classifier_patcher.stop()
        self.lr_classifier_patcher.stop()
        self.xgboost_classifier_patcher.stop()
        self.multi_layer_perceptron_classifier_patcher.stop()
        self.gbm_regressor_patcher.stop()
        self.lasso_regressor_patcher.stop()
        self.lightgbm_regressor_patcher.stop()
        self.mlp_regressor_patcher.stop()
        self.rf_regressor_patcher.stop()
        self.ridge_regressor_patcher.stop()
        self.xgboost_regressor_patcher.stop()

    def test_get_classifier_for_random_forest(self):
        """Test for get_classifier method for RFClassifier"""

        params = {"n_estimators": 200,
                  "max_depth": 8,
                  "min_samples_split": 2}
        self.service.get_classifier(Modeller.RF_CLASSIFIER, **params)
        self.mock_rf_classifier.assert_called_with(**params)

    def test_get_classifier_for_gbm(self):
        """Test for get_classifier method for GBMClassifier"""

        params = {"learning_rate": 0.01,
                  "n_estimators": 200,
                  "subsample": 0.8}

        self.service.get_classifier(Modeller.GBM_CLASSIFIER, **params)
        self.mock_gbm_classifier.assert_called_with(**params)

    def test_get_classifier_for_lightgbm(self):
        """Test for get_classifier method for LightGBMClassifier"""

        params = {"learning_rate": 0.01,
                  "n_estimators": 200,
                  "num_leaves": 10,
                  "max_depth": 3}

        self.service.get_classifier(Modeller.LIGHTGBM_CLASSIFIER, **params)
        self.mock_lightgbm_classifier.assert_called_with(**params)

    def test_get_classifier_for_lr(self):
        """Test for get_classifier method for LogisticRegressionClassifier"""

        params = {"penalty": "l1",
                  "max_iter": 200}

        self.service.get_classifier(Modeller.LOGISTIC_REGRESSION_CLASSIFIER, **params)
        self.mock_lr_classifier.assert_called_with(**params)

    def test_get_classifier_for_xgboost(self):
        """Test for get_classifier method for XGBoostClassifier"""

        params = {"learning_rate": 0.01,
                  "n_estimators": 200,
                  "subsample": 0.8}
        self.service.get_classifier(Modeller.XGBOOST_CLASSIFIER, **params)
        self.mock_xgboost_classifier.assert_called_with(**params)

    def test_get_classifier_for_mlp(self):
        """Test for get_classifier method for MultiLayerPerceptronClassifier"""

        params = {"max_iter": 200,
                  "hidden_layer_sizes": (150,)}
        self.service.get_classifier(Modeller.MULTI_LAYER_PERCEPTRON_CLASSIFIER, **params)
        self.mock_multi_layer_perceptron_classifier.assert_called_with(**params)

    def test_get_regressor_for_gbm(self):
        """Test for get_regressor method for GBMRegressor"""

        params = {"learning_rate": 0.01,
                  "n_estimators": 200,
                  "subsample": 0.8}

        self.service.get_regressor(Modeller.GBM_REGRESSOR, **params)
        self.mock_gbm_regressor.assert_called_with(**params)

    def test_get_regressor_for_lasso(self):
        """Test for get_regressor method for LassoRegressor"""

        params = {"alpha": 0.01,
                  "max_iter": 100}

        self.service.get_regressor(Modeller.LASSO, **params)
        self.mock_lasso_regressor.assert_called_with(**params)

    def test_get_regressor_for_lightgbm(self):
        """Test for get_regressor method for LightGBMRegressor"""

        params = {"learning_rate": 0.01,
                  "n_estimators": 200,
                  "num_leaves": 10,
                  "max_depth": 3}

        self.service.get_regressor(Modeller.LIGHTGBM_REGRESSOR, **params)
        self.mock_lightgbm_regressor.assert_called_with(**params)

    def test_get_regressor_for_mlp(self):
        """Test for get_regressor method for MultiLayerPerceptronRegressor"""

        params = {"max_iter": 200,
                  "hidden_layer_sizes": (150,)}
        self.service.get_regressor(Modeller.MULTI_LAYER_PERCEPTRON_REGRESSOR, **params)
        self.mock_mlp_regressor.assert_called_with(**params)

    def test_get_regressor_for_random_forest(self):
        """Test for get_regressor method for RFRegressor"""

        params = {"n_estimators": 200,
                  "max_depth": 8,
                  "min_samples_split": 2}
        self.service.get_regressor(Modeller.RANDOM_FOREST_REGRESSOR, **params)
        self.mock_rf_regressor.assert_called_with(**params)

    def test_get_regressor_for_ridge(self):
        """Test for get_regressor method for RidgeRegressor"""

        params = {"alpha": 0.01,
                  "max_iter": 100}
        self.service.get_regressor(Modeller.RIDGE, **params)
        self.mock_ridge_regressor.assert_called_with(**params)

    def test_get_regressor_for_xgboost(self):
        """Test for get_regressor method for XGBoostRegressor"""

        params = {"learning_rate": 0.01,
                  "n_estimators": 200,
                  "subsample": 0.8}
        self.service.get_regressor(Modeller.XGBOOST_REGRESSOR, **params)
        self.mock_xgboost_regressor.assert_called_with(**params)
