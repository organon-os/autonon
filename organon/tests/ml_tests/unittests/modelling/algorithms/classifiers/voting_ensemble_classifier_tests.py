"""Includes tests for VotingEnsembleClassifier"""
import unittest
from unittest.mock import MagicMock

import numpy as np
import pandas as pd

from organon.ml.modelling.algorithms.classifiers import voting_ensemble_classifier
from organon.ml.modelling.algorithms.classifiers.gbm_classifier import GBMClassifier
from organon.ml.modelling.algorithms.classifiers.rf_classifier import RFClassifier
from organon.ml.modelling.algorithms.classifiers.voting_ensemble_classifier import VotingEnsembleClassifier
from organon.ml.modelling.algorithms.classifiers.xgboost_classifier import XGBoostClassifier
from organon.ml.modelling.algorithms.regressors.gbm_regressor import GBMRegressor
from organon.tests import test_helper


class VotingEnsembleClassifierTestCase(unittest.TestCase):
    """Unittest class for VotingEnsembleClassifier"""

    def setUp(self) -> None:
        mock_estimator_1 = MagicMock(spec=GBMClassifier)
        mock_estimator_1.predict.return_value = pd.DataFrame({"target": [1, 2, 3]})
        mock_estimator_1.predict_proba.return_value = pd.DataFrame(
            [(0.4, 0.1, 0.3), (0.2, 0.3, 0.1), (0.2, 0.1, 0.3)], columns=["1", "2", "3"]
        )
        mock_estimator_1.classes_ = ["1", "2", "3"]
        mock_estimator_2 = MagicMock(spec=XGBoostClassifier)
        mock_estimator_2.predict.return_value = pd.DataFrame({"target": [1, 3, 2]})
        mock_estimator_2.predict_proba.return_value = pd.DataFrame(
            [(0.1, 0.4, 0.3), (0.1, 0.2, 0.3), (0.3, 0.2, 0.1)], columns=["2", "1", "3"]
        )
        mock_estimator_2.classes_ = ["2", "1", "3"]

        mock_estimator_3 = MagicMock(spec=RFClassifier)
        mock_estimator_3.predict.return_value = pd.DataFrame({"target": [2, 3, 1]})
        mock_estimator_3.predict_proba.return_value = pd.DataFrame(
            [(0.3, 0.1, 0.4), (0.3, 0.2, 0.1), (0.1, 0.2, 0.3)], columns=["3", "1", "2"]
        )
        mock_estimator_3.classes_ = ["3", "1", "2"]
        self.mock_get_voting_prediction = test_helper.get_mock(
            self, voting_ensemble_classifier.__name__ + ".get_voting_classifier_prediction")
        self.estimators = [mock_estimator_1, mock_estimator_2, mock_estimator_3]

    def test_fit_prefit(self):
        """tests fit when prefit=True"""
        clf = VotingEnsembleClassifier(estimators=self.estimators, prefit=True)
        clf.fit(pd.DataFrame({"a": [1, 2, 3]}), pd.DataFrame({"target": [1, 2, 3]}))
        for estimator in self.estimators:
            estimator.fit.assert_not_called()

    def test_fit_prefit_false(self):
        """tests fit when prefit=False"""
        clf = VotingEnsembleClassifier(estimators=self.estimators, prefit=False)
        train_df = pd.DataFrame({"a": [1, 2, 3]})
        target_df = pd.DataFrame({"a": [1, 2, 3]})
        clf.fit(train_df, target_df)
        for estimator in self.estimators:
            estimator.fit.assert_called_with(train_df, target_df)

    def test_fit_no_estimator_error(self):
        """tests error when fit called without setting estimators"""
        clf = VotingEnsembleClassifier()
        with self.assertRaisesRegex(ValueError, "Estimators not given"):
            clf.fit(pd.DataFrame({"a": [1, 2, 3]}), pd.DataFrame({"target": [1, 2, 3]}))

    def test_fit_no_estimator_error_empty_list(self):
        """tests error when fit called without setting estimators"""
        clf = VotingEnsembleClassifier(estimators=[])
        with self.assertRaisesRegex(ValueError, "Estimators not given"):
            clf.fit(pd.DataFrame({"a": [1, 2, 3]}), pd.DataFrame({"target": [1, 2, 3]}))

    def test_fit_invalid_estimator_error(self):
        """tests if error is raised when one of the estimators is not a classifier"""
        clf = VotingEnsembleClassifier(estimators=[GBMClassifier(), GBMRegressor()])
        with self.assertRaisesRegex(ValueError, "All estimators should be an instance of BaseClassifier"):
            clf.fit(pd.DataFrame({"a": [1, 2, 3]}), pd.DataFrame({"target": [1, 2, 3]}))

    def test_predict(self):
        """tests predict method"""
        clf = VotingEnsembleClassifier(estimators=self.estimators, prefit=True)
        clf.fit(pd.DataFrame({"a": [1, 2, 3]}), pd.DataFrame({"target": [1, 2, 3]}))
        self.mock_get_voting_prediction.return_value = pd.Series([1, 3, 1])
        ret = clf.predict(pd.DataFrame({"a": [3, 4, 5]}))
        all_pred_df = self.mock_get_voting_prediction.call_args.args[0]
        pd.testing.assert_frame_equal(
            pd.DataFrame({
                "pred_0": self.estimators[0].predict.return_value.iloc[:, 0],
                "pred_1": self.estimators[1].predict.return_value.iloc[:, 0],
                "pred_2": self.estimators[2].predict.return_value.iloc[:, 0]
            }), all_pred_df)
        self.assertEqual(["target"], ret.columns.tolist())
        self.assertListEqual([1, 3, 1], ret["target"].tolist())

    def test_predict_proba(self):
        """tests predict_proba method"""
        clf = VotingEnsembleClassifier(estimators=self.estimators, prefit=True)
        clf.fit(pd.DataFrame({"a": [1, 2, 3, 4, 5]}), pd.DataFrame({"target": [1, 2, 3, 4, 5]}))
        ret = clf.predict_proba(pd.DataFrame({"a": [1, 2, 3, 4, 5]}))
        expected_values = np.array([
            [0.3, 0.2, 0.3], [0.2, 0.5 / 3, 0.7 / 3], [0.2, 0.7 / 3, 0.5 / 3]
        ])
        np.testing.assert_almost_equal(expected_values, ret.values)

    if __name__ == '__main__':
        unittest.main()
