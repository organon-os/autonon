"""Includes tests for GBMClassifier class."""
import unittest
from unittest.mock import patch, MagicMock
import numpy as np
import pandas as pd
from sklearn.ensemble import GradientBoostingClassifier

from organon.ml.modelling.algorithms.classifiers import gbm_classifier
from organon.ml.modelling.algorithms.classifiers.gbm_classifier import GBMClassifier


class GBMClassifierTestCase(unittest.TestCase):
    """Test class for GBMClassifier class."""

    def setUp(self) -> None:
        gbm_classifier_patcher = patch(
            gbm_classifier.__name__ + "." + GradientBoostingClassifier.__name__)
        self.mock_gbm_classifier = gbm_classifier_patcher.start()
        self.addCleanup(gbm_classifier_patcher.stop)
        target = np.random.randint(2, size=500)
        x_1 = np.random.uniform(low=0, high=10, size=(500,))
        x_2 = np.random.uniform(low=10, high=30, size=(500,))
        self.data = pd.DataFrame({"x1": x_1, "x2": x_2})
        self.df_target = pd.DataFrame({"target": target})

    def test_gbm_classifier_object_defaults(self):
        """Test for GBMClassifier with default params"""
        GBMClassifier()
        self.mock_gbm_classifier.assert_called()

    def test_gbm_classifier_object_with_params(self):
        """Test for GBMClassifier with  params"""

        params = {"learning_rate": 0.01,
                  "n_estimators": 200,
                  "subsample": 0.8}
        GBMClassifier(**params)
        self.mock_gbm_classifier.assert_called_with(**params)

    def test_gbm_classifier_fit(self):
        """Test for GBMClassifier fit method."""

        classifier = GBMClassifier()
        classifier.fit(self.data, self.df_target, sample_weight=None)
        self.mock_gbm_classifier.return_value.fit.assert_called_with(self.data, self.df_target["target"],
                                                                     sample_weight=None)

    def test_gbm_classifier_predict(self):
        """Test for GBMClassifier predict method."""

        classifier = GBMClassifier()
        classifier.is_fitted = MagicMock(return_value=True)
        classifier.predict(self.df_target)
        self.mock_gbm_classifier.return_value.predict.assert_called_with(self.df_target)

    def test_gbm_classifier_predict_proba(self):
        """Test for GBMClassifier predict_proba method."""

        classifier = GBMClassifier()
        classifier.is_fitted = MagicMock(return_value=True)
        classifier.predict_proba(self.df_target)
        self.mock_gbm_classifier.return_value.predict_proba.assert_called_with(self.df_target)
