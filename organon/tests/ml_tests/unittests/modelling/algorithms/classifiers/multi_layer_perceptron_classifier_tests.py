"""Includes tests for MultiLayerPerceptronClassifier class."""
import unittest
from unittest.mock import patch, MagicMock
import numpy as np
import pandas as pd
from sklearn.neural_network import MLPClassifier

from organon.ml.modelling.algorithms.classifiers import multi_layer_perceptron_classifier
from organon.ml.modelling.algorithms.classifiers.multi_layer_perceptron_classifier import MultiLayerPerceptronClassifier


class MultiLayerPerceptronClassifierTestCase(unittest.TestCase):
    """Test class for MultiLayerPerceptronClassifier class."""

    def setUp(self) -> None:
        mlp_classifier_patcher = patch(
            multi_layer_perceptron_classifier.__name__ + "." + MLPClassifier.__name__)
        self.mock_mlp_classifier = mlp_classifier_patcher.start()
        self.addCleanup(mlp_classifier_patcher.stop)
        target = np.random.randint(2, size=500)
        x_1 = np.random.uniform(low=0, high=10, size=(500,))
        x_2 = np.random.uniform(low=10, high=30, size=(500,))
        self.data = pd.DataFrame({"x1": x_1, "x2": x_2})
        self.df_target = pd.DataFrame({"target": target})

    def test_mlp_classifier_object_defaults(self):
        """Test for MultiLayerPerceptronClassifier with default params"""
        MultiLayerPerceptronClassifier()
        self.mock_mlp_classifier.assert_called()

    def test_mlp_classifier_object_with_params(self):
        """Test for MultiLayerPerceptronClassifier with  params"""

        params = {"hidden_layer_sizes": (100,),
                  "activation": "relu"}
        MultiLayerPerceptronClassifier(**params)
        self.mock_mlp_classifier.assert_called_with(**params)

    def test_mlp_classifier_fit(self):
        """Test for MultiLayerPerceptronClassifier fit method."""

        classifier = MultiLayerPerceptronClassifier()
        classifier.fit(self.data, self.df_target, a=4)
        self.mock_mlp_classifier.return_value.fit.assert_called_with(self.data, self.df_target["target"],
                                                                     a=4)

    def test_mlp_classifier_predict(self):
        """Test for MultiLayerPerceptronClassifier predict method."""

        classifier = MultiLayerPerceptronClassifier()
        classifier.is_fitted = MagicMock(return_value=True)
        classifier.predict(self.df_target)
        self.mock_mlp_classifier.return_value.predict.assert_called_with(self.df_target)

    def test_mlp_classifier_predict_proba(self):
        """Test for MultiLayerPerceptronClassifier predict_proba method."""

        classifier = MultiLayerPerceptronClassifier()
        classifier.is_fitted = MagicMock(return_value=True)
        classifier.predict_proba(self.df_target)
        self.mock_mlp_classifier.return_value.predict_proba.assert_called_with(self.df_target)
