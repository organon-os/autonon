"""Includes unittests for GamClassifier"""
import unittest
from unittest.mock import MagicMock

import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV

from organon.fl.core.enums.column_native_type import ColumnNativeType
from organon.fl.core.exceptionhandling.known_exception import KnownException
from organon.ml.modelling.algorithms.classifiers import gam_classifier
from organon.ml.modelling.algorithms.classifiers.gam_classifier import GamClassifier
from organon.ml.modelling.algorithms.core.abstractions import gam_mixin
from organon.ml.preprocessing.services.preprocessor import Preprocessor
from organon.tests import test_helper


class GamClassifierTestCase(unittest.TestCase):
    """Unittest class for GamClassifier"""

    def setUp(self) -> None:
        module_name = gam_classifier.__name__
        self.mock_logistic_reg = test_helper.get_mock(self, module_name + "." + LogisticRegression.__name__)
        self.mock_logistic_reg.return_value.coef_ = np.array([[0.8, 1.0]])

        self.mock_random_forest_classifier = test_helper.get_mock(self,
                                                                  module_name + "." + RandomForestClassifier.__name__)
        self.mock_random_forest_classifier.return_value.feature_importances_ = np.array([[0.7], [0.5]])

        mixin_module_name = gam_mixin.__name__
        self.mock_preprocessor = test_helper.get_mock(self, mixin_module_name + "." + Preprocessor.__name__)
        self.mock_cc_service = MagicMock()
        self.mock_cc_service.transform.side_effect = \
            lambda frame: frame.rename(columns={col: f"Z_{col}" for col in frame.columns})
        self.mock_preprocessor.get_coarse_class_service.return_value = self.mock_cc_service

        self.mock_lasso = test_helper.get_mock(self, mixin_module_name + ".Lasso")
        self.mock_lasso.return_value.coef_ = np.array([[0.5], [0.7]])

        self.mock_grid_search = test_helper.get_mock(self, mixin_module_name + "." + GridSearchCV.__name__)
        self.mock_grid_search.return_value.best_params_ = {"alpha": 1}
        self.mock_grid_search.return_value.best_estimator_.coef_ = np.array([0.5, 0.7])

        self.train_data = pd.DataFrame({"col1": [1, 2, 3, 4, 5], "col2": ["a", "b", "c", "d", "e"]})
        self.target_data = pd.DataFrame({"target_col": [0, 1, 1, 1, 0]})

        self.vif_func_mock = test_helper.get_mock(self, mixin_module_name + ".variance_inflation_factor")
        self.vif_func_mock.side_effect = [0.5, 0.7]

        self.mock_get_native_type = test_helper.get_mock(self, module_name + ".get_native_type_from_dtype")
        self.mock_get_native_type.return_value = ColumnNativeType.Numeric
        self.mock_prep_input_helper = test_helper.get_mock(self, module_name + ".preprocessing_input_helper")
        self.mock_prep_input_helper.convert_str_series_to_binary.side_effect = lambda ser, pos, neg: pd.Series(np.where(
            ser == pos, 1, 0))
        self.mock_prep_input_helper.convert_binary_series_to_str.side_effect = lambda ser, pos, neg: pd.Series(np.where(
            ser == 1, pos, neg))

    def test_fit_with_no_columns_to_eliminate(self):
        """tests fit method when no columns will be eliminated"""
        classifier = GamClassifier(grid_search_param_set=[1, 2, 3], grid_search_cv=20,
                                   grid_search_scoring="dummy", max_vif_limit=10)
        classifier.fit(self.train_data, self.target_data)
        self.assertEqual([1, 2, 3], self.mock_grid_search.call_args.args[1][0]["alpha"])
        self.assertEqual({"cv": 20, "scoring": "dummy"}, self.mock_grid_search.call_args.kwargs)
        self.assertEqual(1, classifier.best_alpha)
        self.assertEqual(["Z_col1", "Z_col2"], classifier.initial_selected_features)
        self.assertEqual(["Z_col1", "Z_col2"], classifier.selected_features)
        self.assertEqual(self.mock_logistic_reg.return_value, classifier.final_model)
        # vif values are small and coefficients are all positive. randomforestclassifier should not be called
        self.mock_random_forest_classifier.return_value.fit.assert_not_called()

    def test_fit_with_coarse_class_rejected_list(self):
        """tests fit method when some of the columns are in rejected list of coarse class service"""
        self.train_data = pd.DataFrame({"col1": [1, 2, 3, 4, 5], "col2": ["a", "b", "c", "d", "e"],
                                        "col3": [3, 2, 5, 1, 7]})
        self.mock_cc_service.fit_output.rejected_list = ["col1"]
        classifier = GamClassifier(grid_search_param_set=[1, 2, 3], grid_search_cv=20,
                                   grid_search_scoring="dummy", max_vif_limit=10)
        classifier.fit(self.train_data, self.target_data)
        self.assertEqual([1, 2, 3], self.mock_grid_search.call_args.args[1][0]["alpha"])
        self.assertEqual({"cv": 20, "scoring": "dummy"}, self.mock_grid_search.call_args.kwargs)
        self.assertEqual(1, classifier.best_alpha)
        self.assertCountEqual(["Z_col2", "Z_col3"], classifier.initial_selected_features)
        self.assertCountEqual(["Z_col2", "Z_col3"], classifier.selected_features)
        self.assertEqual(self.mock_logistic_reg.return_value, classifier.final_model)
        # vif values are small and coefficients are all positive. randomforestclassifier should not be called
        self.mock_random_forest_classifier.return_value.fit.assert_not_called()

    def test_fit_with_one_negative_coef_column(self):
        """tests fit method -
        one of the columns have a negative coefficient in first iteration and should be eliminated"""
        log_reg_mock_1 = MagicMock()
        log_reg_mock_1.coef_ = np.array([[-0.5, 1.0]])
        log_reg_mock_2 = MagicMock()
        log_reg_mock_2.coef_ = np.array([[0.6]])
        mock_final_model = MagicMock()
        self.mock_logistic_reg.side_effect = [log_reg_mock_1, log_reg_mock_2, mock_final_model]

        lasso_mock_1 = MagicMock()
        lasso_mock_1.coef_ = np.array([[0.5], [0.7]])
        lasso_mock_2 = MagicMock()
        lasso_mock_2.coef_ = np.array([[0.8]])
        self.mock_lasso.side_effect = [None, lasso_mock_1, lasso_mock_2]

        classifier = GamClassifier(grid_search_param_set=[1, 2, 3], grid_search_cv=20,
                                   grid_search_scoring="dummy", max_vif_limit=10)
        classifier.fit(self.train_data, self.target_data)
        self.assertEqual([1, 2, 3], self.mock_grid_search.call_args.args[1][0]["alpha"])
        self.assertEqual({"cv": 20, "scoring": "dummy"}, self.mock_grid_search.call_args.kwargs)
        self.assertEqual(1, classifier.best_alpha)
        self.assertEqual(["Z_col1", "Z_col2"], classifier.initial_selected_features)
        self.assertEqual(["Z_col2"], classifier.selected_features)
        self.assertEqual(mock_final_model, classifier.final_model)
        # randomforestclassifier should not be called
        self.mock_random_forest_classifier.return_value.fit.assert_not_called()

    def test_fit_with_one_high_vif_column(self):
        """tests fit method -
        one of the columns have a high vif in first iteration and should be eliminated"""

        log_reg_mock_1 = MagicMock()
        log_reg_mock_1.coef_ = np.array([[-0.5, 1.0]])
        log_reg_mock_2 = MagicMock()
        log_reg_mock_2.coef_ = np.array([[0.6]])
        mock_final_model = MagicMock()
        self.mock_logistic_reg.side_effect = [log_reg_mock_1, log_reg_mock_2, mock_final_model]

        lasso_mock_1 = MagicMock()
        lasso_mock_1.coef_ = np.array([[0.5], [0.7]])
        lasso_mock_2 = MagicMock()
        lasso_mock_2.coef_ = np.array([[0.8]])
        self.mock_lasso.side_effect = [None, lasso_mock_1, lasso_mock_2]

        self.vif_func_mock.side_effect = [15, 0.7]

        classifier = GamClassifier(grid_search_param_set=[1, 2, 3], grid_search_cv=20,
                                   grid_search_scoring="dummy", max_vif_limit=10)
        classifier.fit(self.train_data, self.target_data)
        self.assertEqual([1, 2, 3], self.mock_grid_search.call_args.args[1][0]["alpha"])
        self.assertEqual({"cv": 20, "scoring": "dummy"}, self.mock_grid_search.call_args.kwargs)
        self.assertEqual(1, classifier.best_alpha)
        self.assertEqual(["Z_col1", "Z_col2"], classifier.initial_selected_features)
        self.assertEqual(["Z_col2"], classifier.selected_features)
        self.assertEqual(mock_final_model, classifier.final_model)
        # randomforestclassifier should not be called
        self.mock_random_forest_classifier.return_value.fit.assert_not_called()

    def test_fit_with_two_negative_coef_column_in_first_iteration(self):
        """tests fit method - feature with less importance should be eliminated if both features
        have negative coefficient values"""
        log_reg_mock_1 = MagicMock()
        log_reg_mock_1.coef_ = np.array([[-0.5, -1.0]])
        log_reg_mock_2 = MagicMock()
        log_reg_mock_2.coef_ = np.array([[0.6]])
        mock_final_model = MagicMock()
        self.mock_logistic_reg.side_effect = [log_reg_mock_1, log_reg_mock_2, mock_final_model]

        lasso_mock_1 = MagicMock()
        lasso_mock_1.coef_ = np.array([[0.5], [0.7]])
        lasso_mock_2 = MagicMock()
        lasso_mock_2.coef_ = np.array([[0.8]])
        self.mock_lasso.side_effect = [None, lasso_mock_1, lasso_mock_2]

        self.mock_random_forest_classifier.return_value.feature_importances_ = np.array([[0.7], [0.5]])

        classifier = GamClassifier(grid_search_param_set=[1, 2, 3], grid_search_cv=20,
                                   grid_search_scoring="dummy", max_vif_limit=10)
        classifier.fit(self.train_data, self.target_data)
        self.assertEqual([1, 2, 3], self.mock_grid_search.call_args.args[1][0]["alpha"])
        self.assertEqual({"cv": 20, "scoring": "dummy"}, self.mock_grid_search.call_args.kwargs)
        self.assertEqual(1, classifier.best_alpha)
        self.assertEqual(["Z_col1", "Z_col2"], classifier.initial_selected_features)

        # col2 should be eliminated since its feature_importance is less than col1 (set above)
        self.assertEqual(["Z_col1"], classifier.selected_features)
        self.assertEqual(mock_final_model, classifier.final_model)

        # randomforestclassifier should be called once
        self.mock_random_forest_classifier.return_value.fit.assert_called_once()
        self.assertCountEqual(["Z_col1", "Z_col2"],
                              self.mock_random_forest_classifier.return_value.fit.call_args.args[0].columns.tolist())

    def test_fit_with_all_columns_eliminated(self):
        """tests fit method when all columns will be eliminated because of negative coefficients
         in LogisticRegression"""
        log_reg_mock_1 = MagicMock()
        log_reg_mock_1.coef_ = np.array([[-0.5, 1.0]])  # col_1 will be eliminated in first iteration
        log_reg_mock_2 = MagicMock()
        log_reg_mock_2.coef_ = np.array([[-0.6]])  # col_2 will be eliminated in second iteration
        mock_final_model = MagicMock()
        self.mock_logistic_reg.side_effect = [log_reg_mock_1, log_reg_mock_2, mock_final_model]

        lasso_mock_1 = MagicMock()
        lasso_mock_1.coef_ = np.array([[0.5], [0.7]])
        lasso_mock_2 = MagicMock()
        lasso_mock_2.coef_ = np.array([[0.8]])
        self.mock_lasso.side_effect = [None, lasso_mock_1, lasso_mock_2]

        rf_mock_1 = MagicMock()
        rf_mock_1.feature_importances_ = np.array([[0.7], [0.5]])
        rf_mock_2 = MagicMock()
        rf_mock_2.feature_importances_ = np.array([[0.7]])
        self.mock_random_forest_classifier.side_effect = [rf_mock_1, rf_mock_2]

        classifier = GamClassifier(grid_search_param_set=[1, 2, 3], grid_search_cv=20,
                                   grid_search_scoring="dummy", max_vif_limit=10)
        classifier.fit(self.train_data, self.target_data)
        self.assertEqual([1, 2, 3], self.mock_grid_search.call_args.args[1][0]["alpha"])
        self.assertEqual({"cv": 20, "scoring": "dummy"}, self.mock_grid_search.call_args.kwargs)
        self.assertEqual(1, classifier.best_alpha)
        self.assertEqual(["Z_col1", "Z_col2"], classifier.initial_selected_features)
        # all columns should be selected if all eliminated
        self.assertEqual(["Z_col1", "Z_col2"], classifier.selected_features)
        self.assertEqual(mock_final_model, classifier.final_model)

    def test_fit_initial_selected_features_when_one_coef_zero(self):
        """tests if initial_selected_features are returned properly when some of the columns are eliminated
         after grid search with lasso"""
        self.mock_grid_search.return_value.best_estimator_.coef_ = np.array([0, 0.7])
        classifier = GamClassifier(grid_search_param_set=[1, 2, 3], grid_search_cv=20,
                                   grid_search_scoring="dummy", max_vif_limit=10)
        classifier.fit(self.train_data, self.target_data)
        self.assertEqual(["Z_col2"],
                         classifier.initial_selected_features)  # col1 should be eliminated since its coef is 0
        self.assertEqual(["Z_col1", "Z_col2"], classifier.selected_features)

    def test_fit_no_significant_features_error(self):
        """tests if exception is raised when all coefficients are zero after first lasso"""
        self.mock_lasso.return_value.coef_ = np.array([0, 0])
        classifier = GamClassifier()
        with self.assertRaisesRegex(KnownException, "No significant features found. Cannot build model."):
            classifier.fit(self.train_data, self.target_data)

    def test_predict_before_fit_error(self):
        """tests if exception is raised when predict is called without calling fit first"""
        with self.assertRaisesRegex(ValueError, "Modeller not fitted yet"):
            GamClassifier().predict(pd.DataFrame({"a": [0, 1, 1, 0]}))

    def test_predict_proba_before_fit_error(self):
        """tests if exception is raised when predict_proba is called without calling fit first"""
        with self.assertRaisesRegex(ValueError, "Modeller not fitted yet"):
            GamClassifier().predict_proba(pd.DataFrame({"a": [0, 1, 1, 0]}))

    def test_multiclass_error(self):
        """tests if exception is raised when multiclass target is given on fit"""
        dataframe = pd.DataFrame({"a": np.random.randint(1, 1000, 10),
                                  "b": np.random.randint(1, 1000, 10),
                                  "e": np.random.randint(1, 1000, 10)})
        target_df = pd.DataFrame({"c": np.random.randint(1, 1000, 10)})
        classifier = GamClassifier()
        with self.assertRaisesRegex(ValueError, "GamClassifier cannot be used for multiclass targets"):
            classifier.fit(dataframe, target_df)

    def test_with_non_numeric_target(self):
        """tests if non-numeric target is converted to numerical before modelling"""
        self.mock_get_native_type.return_value = ColumnNativeType.String
        classifier = GamClassifier(target_positive_class="b", target_negative_class="a")
        classifier.fit(self.train_data, pd.DataFrame({"target_col": ["a", "b", "a", "b", "b"]}, dtype="category"))
        self.assertEqual([0, 1, 0, 1, 1], self.mock_grid_search.return_value.fit.call_args.args[1].tolist())

    def test_with_non_numeric_target_classes_not_given_error(self):
        """tests exception when for non-numeric target, target pos-neg class value not given"""
        self.mock_get_native_type.return_value = ColumnNativeType.String
        classifier = GamClassifier(target_positive_class=None, target_negative_class=None)
        with self.assertRaisesRegex(ValueError, "Target positive and negative classes should be given for"
                                                " non-numeric target data"):
            classifier.fit(self.train_data, pd.DataFrame({"target_col": ["a", "b", "a", "b", "b"]}))

    def test_predict_after_fit(self):
        """tests predict after fitting"""
        self.train_data = pd.DataFrame({"col1": [1, 2, 3, 4, 5], "col2": ["a", "b", "c", "d", "e"],
                                        "col3": [3, 2, 5, 1, 7]})
        self.mock_cc_service.fit_output.rejected_list = ["col1"]
        classifier = GamClassifier(grid_search_param_set=[1, 2, 3], grid_search_cv=20,
                                   grid_search_scoring="dummy", max_vif_limit=10)
        classifier.fit(self.train_data, self.target_data)
        self.assertEqual([1, 2, 3], self.mock_grid_search.call_args.args[1][0]["alpha"])
        self.assertEqual({"cv": 20, "scoring": "dummy"}, self.mock_grid_search.call_args.kwargs)
        self.assertCountEqual(["Z_col2", "Z_col3"], classifier.initial_selected_features)
        self.assertCountEqual(["Z_col2", "Z_col3"], classifier.selected_features)
        self.assertEqual(self.mock_logistic_reg.return_value, classifier.final_model)
        self.mock_logistic_reg.return_value.predict.return_value = np.random.choice([0, 1], len(self.train_data))
        pred_df = classifier.predict(self.train_data)
        self.assertEqual(list(self.mock_logistic_reg.return_value.predict.return_value),
                         pred_df["prediction"].to_list())
        self.assertCountEqual(["Z_col2", "Z_col3"],
                              self.mock_logistic_reg.return_value.predict.call_args.args[0].columns.to_list())

    def test_predict_proba_after_fit(self):
        """tests predict_proba after fitting"""

        self.train_data = pd.DataFrame({"col1": [1, 2, 3, 4, 5], "col2": ["a", "b", "c", "d", "e"],
                                        "col3": [3, 2, 5, 1, 7]})
        self.mock_cc_service.fit_output.rejected_list = ["col1"]
        classifier = GamClassifier(grid_search_param_set=[1, 2, 3], grid_search_cv=20,
                                   grid_search_scoring="dummy", max_vif_limit=10)
        classifier.fit(self.train_data, self.target_data)
        self.assertEqual([1, 2, 3], self.mock_grid_search.call_args.args[1][0]["alpha"])
        self.assertEqual({"cv": 20, "scoring": "dummy"}, self.mock_grid_search.call_args.kwargs)
        self.assertCountEqual(["Z_col2", "Z_col3"], classifier.initial_selected_features)
        self.assertCountEqual(["Z_col2", "Z_col3"], classifier.selected_features)
        self.assertEqual(self.mock_logistic_reg.return_value, classifier.final_model)
        proba_arr = np.array([[0.7, 0.5, 0.2, 0.6, 0.1],
                              [0.3, 0.5, 0.8, 0.4, 0.9]]).T
        self.mock_logistic_reg.return_value.predict_proba.return_value = proba_arr
        self.mock_logistic_reg.return_value.classes_ = [1, 0]
        pred_df = classifier.predict_proba(self.train_data)
        self.assertEqual(proba_arr[:, 0].tolist(), pred_df[1].to_list())
        self.assertEqual(proba_arr[:, 1].tolist(), pred_df[0].to_list())

        self.assertCountEqual(["Z_col2", "Z_col3"],
                              self.mock_logistic_reg.return_value.predict_proba.call_args.args[0].columns.to_list())

    def test_predict_with_non_numeric_target(self):
        """tests predict result with non-numeric result"""
        self.mock_get_native_type.return_value = ColumnNativeType.String
        self.target_data = pd.DataFrame({"target_col": ["a", "b", "b", "a", "a"]})
        classifier = GamClassifier(grid_search_param_set=[1, 2, 3], grid_search_cv=20,
                                   grid_search_scoring="dummy", max_vif_limit=10, target_positive_class="b",
                                   target_negative_class="a")
        classifier.fit(self.train_data, self.target_data)
        self.mock_logistic_reg.return_value.predict.return_value = np.array([0, 1, 1, 0, 0])
        pred_df = classifier.predict(self.train_data)
        self.assertEqual(["a", "b", "b", "a", "a"], pred_df["prediction"].to_list())

    def test_predict_proba_with_non_numeric_target(self):
        """tests predict_proba result with non-numeric result"""
        self.mock_get_native_type.return_value = ColumnNativeType.String
        self.target_data = pd.DataFrame({"target_col": ["a", "b", "b", "a", "a"]})
        classifier = GamClassifier(grid_search_param_set=[1, 2, 3], grid_search_cv=20,
                                   grid_search_scoring="dummy", max_vif_limit=10, target_positive_class="b",
                                   target_negative_class="a")
        classifier.fit(self.train_data, self.target_data)
        proba_arr = np.array([[0.7, 0.5, 0.2, 0.6, 0.1],
                              [0.3, 0.5, 0.8, 0.4, 0.9]]).T
        self.mock_logistic_reg.return_value.predict_proba.return_value = proba_arr
        self.mock_logistic_reg.return_value.classes_ = [1, 0]
        pred_df = classifier.predict_proba(self.train_data)
        self.assertCountEqual(["a", "b"], pred_df.columns.to_list())
        self.assertEqual(proba_arr[:, 0].tolist(), pred_df["b"].to_list())
        self.assertEqual(proba_arr[:, 1].tolist(), pred_df["a"].to_list())


if __name__ == '__main__':
    unittest.main()
