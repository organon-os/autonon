"""Includes tests for RFClassifier class."""
import unittest
from unittest.mock import patch, MagicMock
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier

from organon.ml.modelling.algorithms.classifiers import rf_classifier
from organon.ml.modelling.algorithms.classifiers.rf_classifier import RFClassifier


class RFClassifierTestCase(unittest.TestCase):
    """Test class for RFClassifier class."""

    def setUp(self) -> None:
        rf_classifier_patcher = patch(
            rf_classifier.__name__ + "." + RandomForestClassifier.__name__)
        self.mock_rf_classifier = rf_classifier_patcher.start()
        self.addCleanup(rf_classifier_patcher.stop)
        target = np.random.randint(2, size=500)
        x_1 = np.random.uniform(low=0, high=10, size=(500,))
        x_2 = np.random.uniform(low=10, high=30, size=(500,))
        self.data = pd.DataFrame({"x1": x_1, "x2": x_2})
        self.df_target = pd.DataFrame({"target": target})

    def test_rf_classifier_object_defaults(self):
        """Test for RFClassifier with default params"""
        RFClassifier()
        self.mock_rf_classifier.assert_called()

    def test_rf_classifier_object_with_params(self):
        """Test for RFClassifier with  params"""

        params = {"n_estimators": 200,
                  "max_depth": 8,
                  "min_samples_split": 2}
        RFClassifier(**params)
        self.mock_rf_classifier.assert_called_with(**params)

    def test_rf_classifier_fit(self):
        """Test for RFClassifier fit method."""

        classifier = RFClassifier()
        classifier.fit(self.data, self.df_target, sample_weight=None)
        self.mock_rf_classifier.return_value.fit.assert_called_with(self.data, self.df_target["target"],
                                                                    sample_weight=None)

    def test_rf_classifier_predict(self):
        """Test for RFClassifier predict method."""

        classifier = RFClassifier()
        classifier.is_fitted = MagicMock(return_value=True)
        classifier.predict(self.df_target)
        self.mock_rf_classifier.return_value.predict.assert_called_with(self.df_target)

    def test_rf_classifier_predict_proba(self):
        """Test for RFClassifier predict_proba method."""

        classifier = RFClassifier()
        classifier.is_fitted = MagicMock(return_value=True)
        classifier.predict_proba(self.df_target)
        self.mock_rf_classifier.return_value.predict_proba.assert_called_with(self.df_target)
