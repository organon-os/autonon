"""Includes tests for LogisticRegressionClassifier class."""
import unittest
from unittest.mock import patch, MagicMock
import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression

from organon.ml.modelling.algorithms.classifiers import logistic_regression_classifier
from organon.ml.modelling.algorithms.classifiers.logistic_regression_classifier import LogisticRegressionClassifier


class LogisticRegressionClassifierTestCase(unittest.TestCase):
    """Test class for LogisticRegressionClassifier class."""

    def setUp(self) -> None:
        lr_classifier_patcher = patch(
            logistic_regression_classifier.__name__ + "." + LogisticRegression.__name__)
        self.mock_lr_classifier = lr_classifier_patcher.start()
        self.addCleanup(lr_classifier_patcher.stop)
        target = np.random.randint(2, size=500)
        x_1 = np.random.uniform(low=0, high=10, size=(500,))
        x_2 = np.random.uniform(low=10, high=30, size=(500,))
        self.data = pd.DataFrame({"x1": x_1, "x2": x_2})
        self.df_target = pd.DataFrame({"target": target})

    def test_lr_classifier_object_defaults(self):
        """Test for LogisticRegressionClassifier with default params"""
        LogisticRegressionClassifier()
        self.mock_lr_classifier.assert_called()

    def test_lr_classifier_object_with_params(self):
        """Test for LogisticRegressionClassifier with  params"""

        params = {"penalty":"l1",
                  "max_iter": 200}
        LogisticRegressionClassifier(**params)
        self.mock_lr_classifier.assert_called_with(**params)

    def test_lr_classifier_fit(self):
        """Test for LogisticRegressionClassifier fit method."""

        classifier = LogisticRegressionClassifier()
        classifier.fit(self.data, self.df_target, sample_weight=None)
        self.mock_lr_classifier.return_value.fit.assert_called_with(self.data, self.df_target["target"],
                                                                    sample_weight=None)

    def test_lr_classifier_predict(self):
        """Test for LogisticRegressionClassifier predict method."""

        classifier = LogisticRegressionClassifier()
        classifier.is_fitted = MagicMock(return_value=True)
        classifier.predict(self.df_target)
        self.mock_lr_classifier.return_value.predict.assert_called_with(self.df_target)

    def test_lr_classifier_predict_proba(self):
        """Test for LogisticRegressionClassifier predict_proba method."""

        classifier = LogisticRegressionClassifier()
        classifier.is_fitted = MagicMock(return_value=True)
        classifier.predict_proba(self.df_target)
        self.mock_lr_classifier.return_value.predict_proba.assert_called_with(self.df_target)
