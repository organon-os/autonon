"""Includes tests for XGBoostClassifier class."""
import unittest
from unittest.mock import patch, MagicMock
import numpy as np
import pandas as pd
from xgboost import XGBClassifier

from organon.ml.modelling.algorithms.classifiers import xgboost_classifier
from organon.ml.modelling.algorithms.classifiers.xgboost_classifier import XGBoostClassifier


class XGBoostClassifierTestCase(unittest.TestCase):
    """Test class for XGBoostClassifier class."""

    def setUp(self) -> None:
        xgboost_classifier_patcher = patch(
            xgboost_classifier.__name__ + "." + XGBClassifier.__name__)
        self.mock_xgboost_classifier = xgboost_classifier_patcher.start()
        self.addCleanup(xgboost_classifier_patcher.stop)
        target = np.random.randint(2, size=500)
        x_1 = np.random.uniform(low=0, high=10, size=(500,))
        x_2 = np.random.uniform(low=10, high=30, size=(500,))
        self.data = pd.DataFrame({"x1": x_1, "x2": x_2})
        self.df_target = pd.DataFrame({"target": target})

    def test_xgboost_classifier_object_defaults(self):
        """Test for XGBoostClassifier with default params"""
        XGBoostClassifier()
        self.mock_xgboost_classifier.assert_called()

    def test_xgboostgbm_classifier_object_with_params(self):
        """Test for XGBoostClassifier with  params"""

        params = {"learning_rate": 0.01,
                  "n_estimators": 200,
                  "subsample": 0.8}
        XGBoostClassifier(**params)
        self.mock_xgboost_classifier.assert_called_with(**params)

    def test_xgboost_classifier_fit(self):
        """Test for XGBoostClassifier fit method."""

        classifier = XGBoostClassifier()
        classifier.fit(self.data, self.df_target, sample_weight=None)
        self.mock_xgboost_classifier.return_value.fit.assert_called_with(self.data, self.df_target["target"],
                                                                         sample_weight=None)

    def test_xgboost_classifier_predict(self):
        """Test for XGBoostClassifier predict method."""

        classifier = XGBoostClassifier()
        classifier.is_fitted = MagicMock(return_value=True)
        classifier.predict(self.df_target)
        self.mock_xgboost_classifier.return_value.predict.assert_called_with(self.df_target)

    def test_xgboost_classifier_predict_proba(self):
        """Test for XGBoostClassifier predict_proba method."""

        classifier = XGBoostClassifier()
        classifier.is_fitted = MagicMock(return_value=True)
        classifier.predict_proba(self.df_target)
        self.mock_xgboost_classifier.return_value.predict_proba.assert_called_with(self.df_target)
