"""Includes tests for LightGBMClassifier class."""
import unittest
from unittest.mock import patch, MagicMock
import numpy as np
import pandas as pd

from lightgbm import LGBMClassifier

from organon.ml.modelling.algorithms.classifiers import lightgbm_classifier
from organon.ml.modelling.algorithms.classifiers.lightgbm_classifier import LightGBMClassifier


class LightGBMClassifierTestCase(unittest.TestCase):
    """Test class for LightGBMClassifier class."""

    def setUp(self) -> None:
        lightgbm_classifier_patcher = patch(
            lightgbm_classifier.__name__ + "." + LGBMClassifier.__name__)
        self.mock_lightgbm_classifier = lightgbm_classifier_patcher.start()
        self.addCleanup(lightgbm_classifier_patcher.stop)
        target = np.random.randint(2, size=500)
        x_1 = np.random.uniform(low=0, high=10, size=(500,))
        x_2 = np.random.uniform(low=10, high=30, size=(500,))
        self.data = pd.DataFrame({"x1": x_1, "x2": x_2})
        self.df_target = pd.DataFrame({"target": target})

    def test_lightgbm_classifier_object_defaults(self):
        """Test for LightGBMClassifier with default params"""

        LightGBMClassifier()
        self.mock_lightgbm_classifier.assert_called()

    def test_lightgbm_classifier_object_with_params(self):
        """Test for LightGBMClassifier with  params"""

        params = {"learning_rate": 0.01,
                  "n_estimators": 200,
                  "num_leaves": 10,
                  "max_depth": 3}
        LightGBMClassifier(**params)
        self.mock_lightgbm_classifier.assert_called_with(**params)

    def test_lightgbm_classifier_fit(self):
        """Test for LightGBMClassifier fit method."""

        classifier = LightGBMClassifier()
        classifier.fit(self.data, self.df_target, sample_weight=None)
        self.mock_lightgbm_classifier.return_value.fit.assert_called_with(self.data, self.df_target["target"],
                                                                          sample_weight=None)

    def test_lightgbm_classifier_predict(self):
        """Test for LightGBMClassifier predict method."""

        classifier = LightGBMClassifier()
        classifier.is_fitted = MagicMock(return_value=True)
        classifier.predict(self.df_target)
        self.mock_lightgbm_classifier.return_value.predict.assert_called_with(self.df_target)

    def test_lightgbm_classifier_predict_proba(self):
        """Test for LightGBMClassifier predict_proba method."""

        classifier = LightGBMClassifier()
        classifier.is_fitted = MagicMock(return_value=True)
        classifier.predict_proba(self.df_target)
        self.mock_lightgbm_classifier.return_value.predict_proba.assert_called_with(self.df_target)
