"""Includes unittests for HPOService"""
import unittest
from unittest.mock import MagicMock

import numpy as np
import pandas as pd
from sklearn.datasets import make_classification
from sklearn.model_selection import PredefinedSplit


from organon.ml.modelling.algorithms.core.enums.modeller import Modeller
from organon.ml.modelling.algorithms.services.algorithm_service import AlgorithmService
from organon.ml.modelling.algorithms.services.ml_application_operations import MLApplicationOperations
from organon.ml.modelling.model_selection.domain.services import hpo_service
from organon.ml.modelling.model_selection.domain.services.cross_validation_service import CrossValidationService
from organon.ml.modelling.model_selection.domain.services.hpo_service import HPOService
from organon.ml.modelling.model_selection.settings.enums.search_type import SearchType
from organon.ml.modelling.model_selection.settings.objects.hp_optimization_settings import HPOptimizationSettings
from organon.tests import test_helper


class HPOServiceTestCase(unittest.TestCase):
    """Unittest class for HPOService"""

    @classmethod
    def setUpClass(cls) -> None:
        MLApplicationOperations.initialize_app()

    def setUp(self) -> None:
        train_arr, target_arr = make_classification(100, 20)
        self.settings = HPOptimizationSettings(
            pd.DataFrame(train_arr), pd.Series(target_arr),
            modellers=[Modeller.RF_CLASSIFIER, Modeller.LOGISTIC_REGRESSION_CLASSIFIER],
            search_method=SearchType.GRID,
            cv_fold=5,
            search_params={"a": 5}
        )
        self.mock_rf_classifier = MagicMock(name="rf_clf")
        self.mock_log_reg_classifier = MagicMock(name="log_reg_clf")
        self.mock_rf_regressor = MagicMock(name="rf_reg")
        self.mock_lasso_regressor = MagicMock(name="lasso_reg")
        self.mock_grid_search_cv = test_helper.get_mock(self, hpo_service.__name__ + ".GridSearchCV")
        self.mock_cross_val_service = test_helper.get_mock(self,
                                                           hpo_service.__name__ + "." + CrossValidationService.__name__)
        self.mock_bayes_search_cv = test_helper.get_mock(self, hpo_service.__name__ + ".BayesSearchCV")
        self.mock_random_search_cv = test_helper.get_mock(self, hpo_service.__name__ + ".RandomizedSearchCV")
        self.mock_algorithm_service = test_helper.get_mock(self, hpo_service.__name__ + "." + AlgorithmService.__name__)
        self.mock_algorithm_service.get_modeller.side_effect = self._get_modeller_side_effect

    def _get_modeller_side_effect(self, modeller_type):
        if modeller_type == Modeller.RF_CLASSIFIER:
            return self.mock_rf_classifier
        if modeller_type == Modeller.LOGISTIC_REGRESSION_CLASSIFIER:
            return self.mock_log_reg_classifier
        if modeller_type == Modeller.RANDOM_FOREST_REGRESSOR:
            return self.mock_rf_regressor
        if modeller_type == Modeller.LASSO:
            return self.mock_lasso_regressor
        return None

    def test_invalid_search_params_for_bayes_search(self):
        """tests error when invalid params given for bayes"""
        self.settings.search_params = {"cv": 3, "search_spaces": [], "scoring": "roc_auc", "refit": True,
                                       "estimator": None}
        self.settings.search_method = SearchType.MODEL_BASED
        with self.assertRaisesRegex(ValueError, "search_params cannot include these params: "
                                                "cv,search_spaces,scoring,refit,estimator"):
            HPOService.execute(self.settings)

    def test_invalid_search_params_for_grid_search(self):
        """tests error when invalid params given for grid search"""
        self.settings.search_params = {"cv": 3, "param_grid": [], "scoring": "roc_auc", "refit": True,
                                       "estimator": None}
        self.settings.search_method = SearchType.GRID
        with self.assertRaisesRegex(ValueError, "search_params cannot include these params: "
                                                "cv,param_grid,scoring,refit,estimator"):
            HPOService.execute(self.settings)

    def test_invalid_search_params_for_random_search(self):
        """tests error when invalid params given for randomized search"""
        self.settings.search_params = {"cv": 3, "param_distributions": [], "scoring": "roc_auc", "refit": True,
                                       "estimator": None}
        self.settings.search_method = SearchType.RANDOM
        with self.assertRaisesRegex(ValueError, "search_params cannot include these params: "
                                                "cv,param_distributions,scoring,refit,estimator"):
            HPOService.execute(self.settings)

    def test_multiple_metrics_with_bayes_error(self):
        """tests error multiple scoring metrics given for bayes search"""
        self.settings.search_method = SearchType.MODEL_BASED
        self.settings.scoring_metrics = ["roc_auc", "accuracy"]
        with self.assertRaisesRegex(ValueError, "Model Based Search cannot be run with multiple scoring metrics"):
            HPOService.execute(self.settings)

    def test_model_based_search_with_cv_and_single_metric(self):
        """tests model based search when cv_fold is given and a single scoring metric is used"""
        self.settings.search_method = SearchType.MODEL_BASED
        self.settings.cv_fold = 3
        self.settings.scoring_metrics = ["roc_auc"]
        self.settings.modeller_params = [{"dummy_param_for_rf": [1, 2], }, {"dummy_param_for_reg": ["a", "b"]}]
        results = {
            "params": [{"mdl": self.mock_rf_classifier, "mdl__dummy_param_for_rf": 1},
                       {"mdl": self.mock_rf_classifier, "mdl__dummy_param_for_rf": 2},
                       {"mdl": self.mock_log_reg_classifier, "mdl__dummy_param_for_reg": "a"},
                       {"mdl": self.mock_log_reg_classifier, "mdl__dummy_param_for_reg": "b"},
                       ],
            "mean_test_score": np.array([0.2, 0.4, 0.3, 0.6]),
            "split0_test_score": np.array([0.2, 0.4, 0.5, 0.6]),
            "split1_test_score": np.array([0.3, 0.5, 0.2, 0.3]),
            "split2_test_score": np.array([0.1, 0.6, 0.2, 0.9]),
        }
        self.mock_bayes_search_cv.return_value.cv_results_ = results
        self.mock_bayes_search_cv.return_value.n_splits_ = 3
        self.mock_bayes_search_cv.return_value.scoring = "roc_auc"
        output = HPOService.execute(self.settings)
        self.mock_bayes_search_cv.assert_called()
        cv_args = self.mock_bayes_search_cv.call_args.args
        cv_kwargs = self.mock_bayes_search_cv.call_args.kwargs
        params = cv_args[1]
        self.assertEqual([
            {
                "mdl": [self.mock_rf_classifier],
                "mdl__dummy_param_for_rf": [1, 2],
            },
            {
                "mdl": [self.mock_log_reg_classifier],
                "mdl__dummy_param_for_reg": ["a", "b"],
            }
        ], params)
        self.assertIn("a", cv_kwargs)
        self.assertEqual(cv_kwargs["a"], 5)
        self.assertEqual(self.mock_log_reg_classifier, output.best_modeller)
        self.assertEqual({"dummy_param_for_reg": "b"}, output.best_params)
        pd.testing.assert_frame_equal(pd.DataFrame(
            {"modeller": [Modeller.LOGISTIC_REGRESSION_CLASSIFIER.name, Modeller.RF_CLASSIFIER.name,
                          Modeller.LOGISTIC_REGRESSION_CLASSIFIER.name, Modeller.RF_CLASSIFIER.name],
             "params": [{"dummy_param_for_reg": "b"}, {"dummy_param_for_rf": 2},
                        {"dummy_param_for_reg": "a"}, {"dummy_param_for_rf": 1}],
             "fold-0 score": [0.6, 0.4, 0.5, 0.2],
             "fold-1 score": [0.3, 0.5, 0.2, 0.3],
             "fold-2 score": [0.9, 0.6, 0.2, 0.1],
             "mean score": [0.6, 0.4, 0.3, 0.2]
             }),
            output.summary)

    def test_model_based_search_on_regressors__with_cv_and_single_metric(self):
        """tests model based search on regressors when cv_fold is given and a single scoring metric is used"""
        self.settings.modellers = [Modeller.RANDOM_FOREST_REGRESSOR, Modeller.LASSO]
        self.settings.search_method = SearchType.MODEL_BASED
        self.settings.cv_fold = 3
        self.settings.scoring_metrics = ["roc_auc"]
        self.settings.modeller_params = [{"dummy_param_for_rf": [1, 2], }, {"dummy_param_for_reg": ["a", "b"]}]
        results = {
            "params": [{"mdl": self.mock_rf_regressor, "mdl__dummy_param_for_rf": 1},
                       {"mdl": self.mock_rf_regressor, "mdl__dummy_param_for_rf": 2},
                       {"mdl": self.mock_lasso_regressor, "mdl__dummy_param_for_reg": "a"},
                       {"mdl": self.mock_lasso_regressor, "mdl__dummy_param_for_reg": "b"},
                       ],
            "mean_test_score": np.array([0.2, 0.4, 0.3, 0.6]),
            "split0_test_score": np.array([0.2, 0.4, 0.5, 0.6]),
            "split1_test_score": np.array([0.3, 0.5, 0.2, 0.3]),
            "split2_test_score": np.array([0.1, 0.6, 0.2, 0.9]),
        }
        self.mock_bayes_search_cv.return_value.cv_results_ = results
        self.mock_bayes_search_cv.return_value.n_splits_ = 3
        self.mock_bayes_search_cv.return_value.scoring = "roc_auc"
        output = HPOService.execute(self.settings)
        self.mock_bayes_search_cv.assert_called()
        cv_args = self.mock_bayes_search_cv.call_args.args
        cv_kwargs = self.mock_bayes_search_cv.call_args.kwargs
        params = cv_args[1]
        self.assertEqual([
            {
                "mdl": [self.mock_rf_regressor],
                "mdl__dummy_param_for_rf": [1, 2],
            },
            {
                "mdl": [self.mock_lasso_regressor],
                "mdl__dummy_param_for_reg": ["a", "b"],
            }
        ], params)
        self.assertIn("a", cv_kwargs)
        self.assertEqual(cv_kwargs["a"], 5)
        self.assertEqual(self.mock_lasso_regressor, output.best_modeller)
        self.assertEqual({"dummy_param_for_reg": "b"}, output.best_params)
        pd.testing.assert_frame_equal(pd.DataFrame(
            {"modeller": [Modeller.LASSO.name, Modeller.RANDOM_FOREST_REGRESSOR.name,
                          Modeller.LASSO.name, Modeller.RANDOM_FOREST_REGRESSOR.name],
             "params": [{"dummy_param_for_reg": "b"}, {"dummy_param_for_rf": 2},
                        {"dummy_param_for_reg": "a"}, {"dummy_param_for_rf": 1}],
             "fold-0 score": [0.6, 0.4, 0.5, 0.2],
             "fold-1 score": [0.3, 0.5, 0.2, 0.3],
             "fold-2 score": [0.9, 0.6, 0.2, 0.1],
             "mean score": [0.6, 0.4, 0.3, 0.2]
             }),
            output.summary)

    def test_model_based_search_with_test_data_and_single_metric(self):
        """tests model based search when test data is given and a single scoring metric is used"""
        self.settings.search_method = SearchType.MODEL_BASED
        self.settings.test_data = pd.DataFrame(self.settings.train_data.loc[:50])
        self.settings.test_target_data = pd.DataFrame(self.settings.target_data.loc[:50])
        self.settings.scoring_metrics = ["roc_auc"]
        self.settings.modeller_params = [{"dummy_param_for_rf": [1, 2], }, {"dummy_param_for_reg": ["a", "b"]}]
        results = {
            "params": [{"mdl": self.mock_rf_classifier, "mdl__dummy_param_for_rf": 1},
                       {"mdl": self.mock_rf_classifier, "mdl__dummy_param_for_rf": 2},
                       {"mdl": self.mock_log_reg_classifier, "mdl__dummy_param_for_reg": "a"},
                       {"mdl": self.mock_log_reg_classifier, "mdl__dummy_param_for_reg": "b"},
                       ],
            "mean_test_score": np.array([0.2, 0.4, 0.3, 0.6]),
            "split0_test_score": np.array([0.2, 0.4, 0.3, 0.6]),
        }
        self.mock_bayes_search_cv.return_value.cv_results_ = results
        self.mock_bayes_search_cv.return_value.n_splits_ = 1
        self.mock_bayes_search_cv.return_value.scoring = "roc_auc"
        output = HPOService.execute(self.settings)
        self.mock_bayes_search_cv.assert_called()
        cv_args = self.mock_bayes_search_cv.call_args.args
        cv_kwargs = self.mock_bayes_search_cv.call_args.kwargs
        params = cv_args[1]
        self.assertEqual([
            {
                "mdl": [self.mock_rf_classifier],
                "mdl__dummy_param_for_rf": [1, 2],
            },
            {
                "mdl": [self.mock_log_reg_classifier],
                "mdl__dummy_param_for_reg": ["a", "b"],
            }
        ], params)
        self.assertIn("a", cv_kwargs)
        self.assertEqual(cv_kwargs["a"], 5)
        self.assertEqual(self.mock_log_reg_classifier, output.best_modeller)
        self.assertEqual({"dummy_param_for_reg": "b"}, output.best_params)
        pd.testing.assert_frame_equal(pd.DataFrame(
            {"modeller": [Modeller.LOGISTIC_REGRESSION_CLASSIFIER.name, Modeller.RF_CLASSIFIER.name,
                          Modeller.LOGISTIC_REGRESSION_CLASSIFIER.name, Modeller.RF_CLASSIFIER.name],
             "params": [{"dummy_param_for_reg": "b"}, {"dummy_param_for_rf": 2},
                        {"dummy_param_for_reg": "a"}, {"dummy_param_for_rf": 1}],
             "score": [0.6, 0.4, 0.3, 0.2]
             }),
            output.summary)
        predefined_split = self.mock_bayes_search_cv.call_args.kwargs["cv"]
        self.assertIsInstance(predefined_split, PredefinedSplit)
        self.assertEqual([-1] * len(self.settings.train_data) + [1] * len(self.settings.test_data),
                         list(predefined_split.test_fold))

    def test_grid_search_with_cv_and_single_metric(self):
        """tests grid search when cv_fold is given and a single scoring metric is used"""
        self.settings.search_method = SearchType.GRID
        self.settings.cv_fold = 3
        self.settings.scoring_metrics = ["roc_auc"]
        self.settings.modeller_params = [{"dummy_param_for_rf": [1, 2], }, {"dummy_param_for_reg": ["a", "b"]}]
        results = {
            "params": [{"mdl": self.mock_rf_classifier, "mdl__dummy_param_for_rf": 1},
                       {"mdl": self.mock_rf_classifier, "mdl__dummy_param_for_rf": 2},
                       {"mdl": self.mock_log_reg_classifier, "mdl__dummy_param_for_reg": "a"},
                       {"mdl": self.mock_log_reg_classifier, "mdl__dummy_param_for_reg": "b"},
                       ],
            "mean_test_score": np.array([0.2, 0.4, 0.3, 0.6]),
            "split0_test_score": np.array([0.2, 0.4, 0.5, 0.6]),
            "split1_test_score": np.array([0.3, 0.5, 0.2, 0.3]),
            "split2_test_score": np.array([0.1, 0.6, 0.2, 0.9]),
        }
        self.mock_grid_search_cv.return_value.cv_results_ = results
        self.mock_grid_search_cv.return_value.n_splits_ = 3
        self.mock_grid_search_cv.return_value.scoring = "roc_auc"
        output = HPOService.execute(self.settings)
        self.mock_grid_search_cv.assert_called()
        cv_args = self.mock_grid_search_cv.call_args.args
        cv_kwargs = self.mock_grid_search_cv.call_args.kwargs
        params = cv_args[1]
        self.assertEqual([
            {
                "mdl": [self.mock_rf_classifier],
                "mdl__dummy_param_for_rf": [1, 2],
            },
            {
                "mdl": [self.mock_log_reg_classifier],
                "mdl__dummy_param_for_reg": ["a", "b"],
            }
        ], params)
        self.assertIn("a", cv_kwargs)
        self.assertEqual(cv_kwargs["a"], 5)
        self.assertEqual(self.mock_log_reg_classifier, output.best_modeller)
        self.assertEqual({"dummy_param_for_reg": "b"}, output.best_params)
        pd.testing.assert_frame_equal(pd.DataFrame(
            {"modeller": [Modeller.LOGISTIC_REGRESSION_CLASSIFIER.name, Modeller.RF_CLASSIFIER.name,
                          Modeller.LOGISTIC_REGRESSION_CLASSIFIER.name, Modeller.RF_CLASSIFIER.name],
             "params": [{"dummy_param_for_reg": "b"}, {"dummy_param_for_rf": 2},
                        {"dummy_param_for_reg": "a"}, {"dummy_param_for_rf": 1}],
             "fold-0 score": [0.6, 0.4, 0.5, 0.2],
             "fold-1 score": [0.3, 0.5, 0.2, 0.3],
             "fold-2 score": [0.9, 0.6, 0.2, 0.1],
             "mean score": [0.6, 0.4, 0.3, 0.2]
             }),
            output.summary)

    def test_grid_search_with_test_data_and_single_metric(self):
        """tests grid search when test_data is given and a single scoring metric is used"""
        self.settings.search_method = SearchType.GRID
        self.settings.test_data = pd.DataFrame(self.settings.train_data.loc[:50])
        self.settings.test_target_data = pd.DataFrame(self.settings.target_data.loc[:50])
        self.settings.scoring_metrics = ["roc_auc"]
        self.settings.modeller_params = [{"dummy_param_for_rf": [1, 2], }, {"dummy_param_for_reg": ["a", "b"]}]
        results = {
            "params": [{"mdl": self.mock_rf_classifier, "mdl__dummy_param_for_rf": 1},
                       {"mdl": self.mock_rf_classifier, "mdl__dummy_param_for_rf": 2},
                       {"mdl": self.mock_log_reg_classifier, "mdl__dummy_param_for_reg": "a"},
                       {"mdl": self.mock_log_reg_classifier, "mdl__dummy_param_for_reg": "b"},
                       ],
            "mean_test_score": np.array([0.2, 0.4, 0.3, 0.6]),
            "split0_test_score": np.array([0.2, 0.4, 0.3, 0.6]),
        }
        self.mock_grid_search_cv.return_value.cv_results_ = results
        self.mock_grid_search_cv.return_value.n_splits_ = 1
        self.mock_grid_search_cv.return_value.scoring = "roc_auc"
        output = HPOService.execute(self.settings)
        self.mock_grid_search_cv.assert_called()
        cv_args = self.mock_grid_search_cv.call_args.args
        cv_kwargs = self.mock_grid_search_cv.call_args.kwargs
        params = cv_args[1]
        self.assertEqual([
            {
                "mdl": [self.mock_rf_classifier],
                "mdl__dummy_param_for_rf": [1, 2],
            },
            {
                "mdl": [self.mock_log_reg_classifier],
                "mdl__dummy_param_for_reg": ["a", "b"],
            }
        ], params)
        self.assertIn("a", cv_kwargs)
        self.assertEqual(cv_kwargs["a"], 5)
        self.assertEqual(self.mock_log_reg_classifier, output.best_modeller)
        self.assertEqual({"dummy_param_for_reg": "b"}, output.best_params)
        pd.testing.assert_frame_equal(pd.DataFrame(
            {"modeller": [Modeller.LOGISTIC_REGRESSION_CLASSIFIER.name, Modeller.RF_CLASSIFIER.name,
                          Modeller.LOGISTIC_REGRESSION_CLASSIFIER.name, Modeller.RF_CLASSIFIER.name],
             "params": [{"dummy_param_for_reg": "b"}, {"dummy_param_for_rf": 2},
                        {"dummy_param_for_reg": "a"}, {"dummy_param_for_rf": 1}],
             "score": [0.6, 0.4, 0.3, 0.2],
             }),
            output.summary)
        predefined_split = self.mock_grid_search_cv.call_args.kwargs["cv"]
        self.assertIsInstance(predefined_split, PredefinedSplit)
        self.assertEqual([-1] * len(self.settings.train_data) + [1] * len(self.settings.test_data),
                         list(predefined_split.test_fold))

    def test_grid_search_with_cv_and_multiple_metric(self):
        """tests grid searhc when cv_fold is given and multiple scoring metrics are used"""
        self.settings.search_method = SearchType.GRID
        self.settings.cv_fold = 3
        self.settings.scoring_metrics = ["roc_auc", "accuracy"]
        self.settings.modeller_params = [{"dummy_param_for_rf": [1, 2], }, {"dummy_param_for_reg": ["a", "b"]}]
        results = {
            "params": [{"mdl": self.mock_rf_classifier, "mdl__dummy_param_for_rf": 1},
                       {"mdl": self.mock_rf_classifier, "mdl__dummy_param_for_rf": 2},
                       {"mdl": self.mock_log_reg_classifier, "mdl__dummy_param_for_reg": "a"},
                       {"mdl": self.mock_log_reg_classifier, "mdl__dummy_param_for_reg": "b"},
                       ],
            "mean_test_roc_auc": np.array([0.2, 0.4, 0.3, 0.6]),
            "split0_test_roc_auc": np.array([0.2, 0.4, 0.5, 0.6]),
            "split1_test_roc_auc": np.array([0.3, 0.5, 0.2, 0.3]),
            "split2_test_roc_auc": np.array([0.1, 0.6, 0.2, 0.9]),
            "mean_test_accuracy": np.array([0.1, 0.2, 0.3, 0.7]),
            "split0_test_accuracy": np.array([0.05, 0.2, 0.3, 0.6]),
            "split1_test_accuracy": np.array([0.05, 0.1, 0.3, 0.9]),
            "split2_test_accuracy": np.array([0.2, 0.3, 0.3, 0.6]),
        }
        self.mock_grid_search_cv.return_value.cv_results_ = results
        self.mock_grid_search_cv.return_value.n_splits_ = 3
        self.mock_grid_search_cv.return_value.scoring = ["roc_auc", "accuracy"]
        output = HPOService.execute(self.settings)
        self.mock_grid_search_cv.assert_called()
        cv_args = self.mock_grid_search_cv.call_args.args
        cv_kwargs = self.mock_grid_search_cv.call_args.kwargs
        params = cv_args[1]
        self.assertEqual([
            {
                "mdl": [self.mock_rf_classifier],
                "mdl__dummy_param_for_rf": [1, 2],
            },
            {
                "mdl": [self.mock_log_reg_classifier],
                "mdl__dummy_param_for_reg": ["a", "b"],
            }
        ], params)
        self.assertIn("a", cv_kwargs)
        self.assertEqual(cv_kwargs["a"], 5)
        self.assertEqual(self.mock_log_reg_classifier, output.best_modeller)
        self.assertEqual({"dummy_param_for_reg": "b"}, output.best_params)
        pd.testing.assert_frame_equal(pd.DataFrame(
            {"modeller": [Modeller.LOGISTIC_REGRESSION_CLASSIFIER.name, Modeller.RF_CLASSIFIER.name,
                          Modeller.LOGISTIC_REGRESSION_CLASSIFIER.name, Modeller.RF_CLASSIFIER.name],
             "params": [{"dummy_param_for_reg": "b"}, {"dummy_param_for_rf": 2},
                        {"dummy_param_for_reg": "a"}, {"dummy_param_for_rf": 1}],
             "fold-0 roc_auc": [0.6, 0.4, 0.5, 0.2],
             "fold-1 roc_auc": [0.3, 0.5, 0.2, 0.3],
             "fold-2 roc_auc": [0.9, 0.6, 0.2, 0.1],
             "mean roc_auc": [0.6, 0.4, 0.3, 0.2],
             "fold-0 accuracy": [0.6, 0.2, 0.3, 0.05],
             "fold-1 accuracy": [0.9, 0.1, 0.3, 0.05],
             "fold-2 accuracy": [0.6, 0.3, 0.3, 0.2],
             "mean accuracy": [0.7, 0.2, 0.3, 0.1]
             }),
            output.summary)

    def test_grid_search_with_test_data_and_multiple_metric(self):
        """tests grid search when test_data is given and multiple scoring metrics are used"""
        self.settings.search_method = SearchType.GRID
        self.settings.test_data = pd.DataFrame(self.settings.train_data.loc[:50])
        self.settings.test_target_data = pd.DataFrame(self.settings.target_data.loc[:50])
        self.settings.scoring_metrics = ["roc_auc", "accuracy"]
        self.settings.modeller_params = [{"dummy_param_for_rf": [1, 2], }, {"dummy_param_for_reg": ["a", "b"]}]
        results = {
            "params": [{"mdl": self.mock_rf_classifier, "mdl__dummy_param_for_rf": 1},
                       {"mdl": self.mock_rf_classifier, "mdl__dummy_param_for_rf": 2},
                       {"mdl": self.mock_log_reg_classifier, "mdl__dummy_param_for_reg": "a"},
                       {"mdl": self.mock_log_reg_classifier, "mdl__dummy_param_for_reg": "b"},
                       ],
            "mean_test_roc_auc": np.array([0.2, 0.4, 0.3, 0.6]),
            "split0_test_roc_auc": np.array([0.2, 0.4, 0.3, 0.6]),
            "mean_test_accuracy": np.array([0.1, 0.2, 0.3, 0.7]),
            "split0_test_accuracy": np.array([0.1, 0.2, 0.3, 0.7]),
        }
        self.mock_grid_search_cv.return_value.cv_results_ = results
        self.mock_grid_search_cv.return_value.n_splits_ = 1
        self.mock_grid_search_cv.return_value.scoring = ["roc_auc", "accuracy"]
        output = HPOService.execute(self.settings)
        self.mock_grid_search_cv.assert_called()
        cv_args = self.mock_grid_search_cv.call_args.args
        cv_kwargs = self.mock_grid_search_cv.call_args.kwargs
        params = cv_args[1]
        self.assertEqual([
            {
                "mdl": [self.mock_rf_classifier],
                "mdl__dummy_param_for_rf": [1, 2],
            },
            {
                "mdl": [self.mock_log_reg_classifier],
                "mdl__dummy_param_for_reg": ["a", "b"],
            }
        ], params)
        self.assertIn("a", cv_kwargs)
        self.assertEqual(cv_kwargs["a"], 5)
        self.assertEqual(self.mock_log_reg_classifier, output.best_modeller)
        self.assertEqual({"dummy_param_for_reg": "b"}, output.best_params)
        pd.testing.assert_frame_equal(pd.DataFrame(
            {"modeller": [Modeller.LOGISTIC_REGRESSION_CLASSIFIER.name, Modeller.RF_CLASSIFIER.name,
                          Modeller.LOGISTIC_REGRESSION_CLASSIFIER.name, Modeller.RF_CLASSIFIER.name],
             "params": [{"dummy_param_for_reg": "b"}, {"dummy_param_for_rf": 2},
                        {"dummy_param_for_reg": "a"}, {"dummy_param_for_rf": 1}],
             "roc_auc": [0.6, 0.4, 0.3, 0.2],
             "accuracy": [0.7, 0.2, 0.3, 0.1]
             }),
            output.summary)
        predefined_split = self.mock_grid_search_cv.call_args.kwargs["cv"]
        self.assertIsInstance(predefined_split, PredefinedSplit)
        self.assertEqual([-1] * len(self.settings.train_data) + [1] * len(self.settings.test_data),
                         list(predefined_split.test_fold))

    def test_random_search_with_cv_and_single_metric(self):
        """tests random search when cv_fold is given and a single scoring metric is used"""
        self.settings.search_method = SearchType.RANDOM
        self.settings.cv_fold = 3
        self.settings.scoring_metrics = ["roc_auc"]
        self.settings.modeller_params = [{"dummy_param_for_rf": [1, 2], }, {"dummy_param_for_reg": ["a", "b"]}]
        results = {
            "params": [{"mdl": self.mock_rf_classifier, "mdl__dummy_param_for_rf": 1},
                       {"mdl": self.mock_rf_classifier, "mdl__dummy_param_for_rf": 2},
                       {"mdl": self.mock_log_reg_classifier, "mdl__dummy_param_for_reg": "a"},
                       {"mdl": self.mock_log_reg_classifier, "mdl__dummy_param_for_reg": "b"},
                       ],
            "mean_test_score": np.array([0.2, 0.4, 0.3, 0.6]),
            "split0_test_score": np.array([0.2, 0.4, 0.5, 0.6]),
            "split1_test_score": np.array([0.3, 0.5, 0.2, 0.3]),
            "split2_test_score": np.array([0.1, 0.6, 0.2, 0.9]),
        }
        self.mock_random_search_cv.return_value.cv_results_ = results
        self.mock_random_search_cv.return_value.n_splits_ = 3
        self.mock_random_search_cv.return_value.scoring = "roc_auc"
        output = HPOService.execute(self.settings)
        self.mock_random_search_cv.assert_called()
        cv_args = self.mock_random_search_cv.call_args.args
        cv_kwargs = self.mock_random_search_cv.call_args.kwargs
        params = cv_args[1]
        self.assertEqual([
            {
                "mdl": [self.mock_rf_classifier],
                "mdl__dummy_param_for_rf": [1, 2],
            },
            {
                "mdl": [self.mock_log_reg_classifier],
                "mdl__dummy_param_for_reg": ["a", "b"],
            }
        ], params)
        self.assertIn("a", cv_kwargs)
        self.assertEqual(cv_kwargs["a"], 5)
        self.assertEqual(self.mock_log_reg_classifier, output.best_modeller)
        self.assertEqual({"dummy_param_for_reg": "b"}, output.best_params)
        pd.testing.assert_frame_equal(pd.DataFrame(
            {"modeller": [Modeller.LOGISTIC_REGRESSION_CLASSIFIER.name, Modeller.RF_CLASSIFIER.name,
                          Modeller.LOGISTIC_REGRESSION_CLASSIFIER.name, Modeller.RF_CLASSIFIER.name],
             "params": [{"dummy_param_for_reg": "b"}, {"dummy_param_for_rf": 2},
                        {"dummy_param_for_reg": "a"}, {"dummy_param_for_rf": 1}],
             "fold-0 score": [0.6, 0.4, 0.5, 0.2],
             "fold-1 score": [0.3, 0.5, 0.2, 0.3],
             "fold-2 score": [0.9, 0.6, 0.2, 0.1],
             "mean score": [0.6, 0.4, 0.3, 0.2]
             }),
            output.summary)

    def test_random_search_with_cv_and_multiple_metric(self):
        """tests random search when cv_fold is given and multiple scoring metrics are used"""
        self.settings.search_method = SearchType.RANDOM
        self.settings.cv_fold = 3
        self.settings.scoring_metrics = ["roc_auc", "accuracy"]
        self.settings.modeller_params = [{"dummy_param_for_rf": [1, 2], }, {"dummy_param_for_reg": ["a", "b"]}]
        results = {
            "params": [{"mdl": self.mock_rf_classifier, "mdl__dummy_param_for_rf": 1},
                       {"mdl": self.mock_rf_classifier, "mdl__dummy_param_for_rf": 2},
                       {"mdl": self.mock_log_reg_classifier, "mdl__dummy_param_for_reg": "a"},
                       {"mdl": self.mock_log_reg_classifier, "mdl__dummy_param_for_reg": "b"},
                       ],
            "mean_test_roc_auc": np.array([0.2, 0.4, 0.3, 0.6]),
            "split0_test_roc_auc": np.array([0.2, 0.4, 0.5, 0.6]),
            "split1_test_roc_auc": np.array([0.3, 0.5, 0.2, 0.3]),
            "split2_test_roc_auc": np.array([0.1, 0.6, 0.2, 0.9]),
            "mean_test_accuracy": np.array([0.1, 0.2, 0.3, 0.7]),
            "split0_test_accuracy": np.array([0.05, 0.2, 0.3, 0.6]),
            "split1_test_accuracy": np.array([0.05, 0.1, 0.3, 0.9]),
            "split2_test_accuracy": np.array([0.2, 0.3, 0.3, 0.6]),
        }
        self.mock_random_search_cv.return_value.cv_results_ = results
        self.mock_random_search_cv.return_value.n_splits_ = 3
        self.mock_random_search_cv.return_value.scoring = ["roc_auc", "accuracy"]
        output = HPOService.execute(self.settings)
        self.mock_random_search_cv.assert_called()
        cv_args = self.mock_random_search_cv.call_args.args
        cv_kwargs = self.mock_random_search_cv.call_args.kwargs
        params = cv_args[1]
        self.assertEqual([
            {
                "mdl": [self.mock_rf_classifier],
                "mdl__dummy_param_for_rf": [1, 2],
            },
            {
                "mdl": [self.mock_log_reg_classifier],
                "mdl__dummy_param_for_reg": ["a", "b"],
            }
        ], params)
        self.assertIn("a", cv_kwargs)
        self.assertEqual(cv_kwargs["a"], 5)
        self.assertEqual(self.mock_log_reg_classifier, output.best_modeller)
        self.assertEqual({"dummy_param_for_reg": "b"}, output.best_params)
        pd.testing.assert_frame_equal(pd.DataFrame(
            {"modeller": [Modeller.LOGISTIC_REGRESSION_CLASSIFIER.name, Modeller.RF_CLASSIFIER.name,
                          Modeller.LOGISTIC_REGRESSION_CLASSIFIER.name, Modeller.RF_CLASSIFIER.name],
             "params": [{"dummy_param_for_reg": "b"}, {"dummy_param_for_rf": 2},
                        {"dummy_param_for_reg": "a"}, {"dummy_param_for_rf": 1}],
             "fold-0 roc_auc": [0.6, 0.4, 0.5, 0.2],
             "fold-1 roc_auc": [0.3, 0.5, 0.2, 0.3],
             "fold-2 roc_auc": [0.9, 0.6, 0.2, 0.1],
             "mean roc_auc": [0.6, 0.4, 0.3, 0.2],
             "fold-0 accuracy": [0.6, 0.2, 0.3, 0.05],
             "fold-1 accuracy": [0.9, 0.1, 0.3, 0.05],
             "fold-2 accuracy": [0.6, 0.3, 0.3, 0.2],
             "mean accuracy": [0.7, 0.2, 0.3, 0.1]
             }),
            output.summary)

    def test_random_search_with_test_data_and_multiple_metric(self):
        """tests random search when test_data is given and multiple scoring metrics are used"""
        self.settings.search_method = SearchType.RANDOM
        self.settings.test_data = pd.DataFrame(self.settings.train_data.loc[:50])
        self.settings.test_target_data = pd.DataFrame(self.settings.target_data.loc[:50])
        self.settings.scoring_metrics = ["roc_auc", "accuracy"]
        self.settings.modeller_params = [{"dummy_param_for_rf": [1, 2], }, {"dummy_param_for_reg": ["a", "b"]}]
        results = {
            "params": [{"mdl": self.mock_rf_classifier, "mdl__dummy_param_for_rf": 1},
                       {"mdl": self.mock_rf_classifier, "mdl__dummy_param_for_rf": 2},
                       {"mdl": self.mock_log_reg_classifier, "mdl__dummy_param_for_reg": "a"},
                       {"mdl": self.mock_log_reg_classifier, "mdl__dummy_param_for_reg": "b"},
                       ],
            "mean_test_roc_auc": np.array([0.2, 0.4, 0.3, 0.6]),
            "split0_test_roc_auc": np.array([0.2, 0.4, 0.3, 0.6]),
            "mean_test_accuracy": np.array([0.1, 0.2, 0.3, 0.7]),
            "split0_test_accuracy": np.array([0.1, 0.2, 0.3, 0.7]),
        }
        self.mock_random_search_cv.return_value.cv_results_ = results
        self.mock_random_search_cv.return_value.n_splits_ = 1
        self.mock_random_search_cv.return_value.scoring = ["roc_auc", "accuracy"]
        output = HPOService.execute(self.settings)
        self.mock_random_search_cv.assert_called()
        cv_args = self.mock_random_search_cv.call_args.args
        cv_kwargs = self.mock_random_search_cv.call_args.kwargs
        params = cv_args[1]
        self.assertEqual([
            {
                "mdl": [self.mock_rf_classifier],
                "mdl__dummy_param_for_rf": [1, 2],
            },
            {
                "mdl": [self.mock_log_reg_classifier],
                "mdl__dummy_param_for_reg": ["a", "b"],
            }
        ], params)
        self.assertIn("a", cv_kwargs)
        self.assertEqual(cv_kwargs["a"], 5)
        self.assertEqual(self.mock_log_reg_classifier, output.best_modeller)
        self.assertEqual({"dummy_param_for_reg": "b"}, output.best_params)
        pd.testing.assert_frame_equal(pd.DataFrame(
            {"modeller": [Modeller.LOGISTIC_REGRESSION_CLASSIFIER.name, Modeller.RF_CLASSIFIER.name,
                          Modeller.LOGISTIC_REGRESSION_CLASSIFIER.name, Modeller.RF_CLASSIFIER.name],
             "params": [{"dummy_param_for_reg": "b"}, {"dummy_param_for_rf": 2},
                        {"dummy_param_for_reg": "a"}, {"dummy_param_for_rf": 1}],
             "roc_auc": [0.6, 0.4, 0.3, 0.2],
             "accuracy": [0.7, 0.2, 0.3, 0.1]
             }),
            output.summary)
        predefined_split = self.mock_random_search_cv.call_args.kwargs["cv"]
        self.assertIsInstance(predefined_split, PredefinedSplit)
        self.assertEqual([-1] * len(self.settings.train_data) + [1] * len(self.settings.test_data),
                         list(predefined_split.test_fold))


if __name__ == '__main__':
    unittest.main()
