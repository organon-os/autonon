"""Includes unittests for SelectionService"""
import unittest
from unittest.mock import MagicMock

import numpy as np
import pandas as pd
from sklearn.model_selection import PredefinedSplit

from organon.ml.common.enums.target_type import TargetType
from organon.ml.modelling.algorithms.classifiers.gam_classifier import GamClassifier
from organon.ml.modelling.algorithms.classifiers.rf_classifier import RFClassifier
from organon.ml.modelling.algorithms.classifiers.stacking_ensemble_classifier import StackingEnsembleClassifier
from organon.ml.modelling.algorithms.classifiers.voting_ensemble_classifier import VotingEnsembleClassifier
from organon.ml.modelling.algorithms.core.abstractions.base_classifier import BaseClassifier
from organon.ml.modelling.algorithms.core.abstractions.base_regressor import BaseRegressor
from organon.ml.modelling.algorithms.core.enums.modeller_type import ModellerType
from organon.ml.modelling.algorithms.regressors.gam_regressor import GamRegressor
from organon.ml.modelling.algorithms.regressors.stacking_ensemble_regressor import StackingEnsembleRegressor
from organon.ml.modelling.algorithms.regressors.voting_ensemble_regressor import VotingEnsembleRegressor
from organon.ml.modelling.model_selection.domain.services import selection_service
from organon.ml.modelling.model_selection.domain.services.cross_validation_service import CrossValidationService
from organon.ml.modelling.model_selection.domain.services.selection_service import SelectionService
from organon.ml.modelling.model_selection.settings.objects.selection_settings import SelectionSettings
from organon.tests import test_helper


def _exec_multithreaded_side_effect(all_params, func, *args, **kwargs):
    # pylint: disable=unused-argument
    for params in all_params:
        func(*params)


class SelectionServiceTestCase(unittest.TestCase):
    """Unittest class for SelectionService"""

    # pylint: disable= too-many-instance-attributes

    def setUp(self) -> None:
        self.mock_gam_classifier = test_helper.get_mock(self, selection_service.__name__ + "." + GamClassifier.__name__)
        self.mock_gam_regressor = test_helper.get_mock(self, selection_service.__name__ + "." + GamRegressor.__name__)
        self.mock_rf_classifier = test_helper.get_mock(self, selection_service.__name__ + "." + RFClassifier.__name__)
        self.mock_stacking_classifier = \
            test_helper.get_mock(self, selection_service.__name__ + "." + StackingEnsembleClassifier.__name__)
        self.mock_stacking_regressor = \
            test_helper.get_mock(self, selection_service.__name__ + "." + StackingEnsembleRegressor.__name__)
        self.mock_voting_classifier = \
            test_helper.get_mock(self, selection_service.__name__ + "." + VotingEnsembleClassifier.__name__)
        self.mock_voting_regressor = \
            test_helper.get_mock(self, selection_service.__name__ + "." + VotingEnsembleRegressor.__name__)
        self.mock_cv_service = \
            test_helper.get_mock(self, selection_service.__name__ + "." + CrossValidationService.__name__)
        self.mock_validation_helper = test_helper.get_mock(self, selection_service.__name__ + ".validation_helper")

        self.mock_clf_1_clone = MagicMock(spec=BaseClassifier)
        self.mock_clf_1 = MagicMock(spec=BaseClassifier)
        self.mock_clf_1.clone.return_value = self.mock_clf_1_clone
        self.mock_clf_1.modeller_type = ModellerType.CLASSIFIER
        self.mock_clf_2_clone = MagicMock(spec=BaseClassifier)
        self.mock_clf_2 = MagicMock(spec=BaseClassifier)
        self.mock_clf_2.modeller_type = ModellerType.CLASSIFIER
        self.mock_clf_2.clone.return_value = self.mock_clf_2_clone

        self.train_data = pd.DataFrame({"a": [1, 2, 3, 4, 5]})
        self.target_data = pd.Series([0, 1, 1, 0, 1])
        self.settings = SelectionSettings([self.mock_clf_1, self.mock_clf_2], self.train_data, self.target_data,
                                          TargetType.BINARY, 5, None, None, True, True)
        self.mock_parallel_exec_helper = \
            test_helper.get_mock(self, selection_service.__name__ + ".parallel_execution_helper")
        self.mock_parallel_exec_helper.execute_parallel.side_effect = _exec_multithreaded_side_effect
        self.mock_cv_service.get_predefined_split.return_value = PredefinedSplit([0, 1, 2, 3, 4])

    def test_no_cv_and_no_test_data(self):
        """tests error when neither cv_count nor test_data is given"""
        self.settings.cv_count = None
        self.settings.test_data = None
        with self.assertRaisesRegex(ValueError, "Give a cv_count or test_data"):
            SelectionService().select(self.settings)

    def test_selection_different_types_of_modellers_error(self):
        """tests error on selection if modellers are not from the same type"""
        mock_clf = MagicMock()
        mock_clf.modeller_type = ModellerType.CLASSIFIER
        mock_reg = MagicMock()
        mock_reg.modeller_type = ModellerType.REGRESSOR
        self.settings.modellers = [mock_clf, mock_reg]
        with self.assertRaisesRegex(ValueError, "All modellers should be of same type"):
            SelectionService().select(self.settings)

    def test_selection_invalid_modeller_type_error(self):
        """tests error on selection if a modeller is not a classifier or regressor"""
        invalid_type = "asdf"
        mock_invalid = MagicMock()
        mock_invalid.modeller_type = invalid_type
        mock_invalid2 = MagicMock()
        mock_invalid2.modeller_type = invalid_type
        self.settings.modellers = [mock_invalid, mock_invalid2]
        with self.assertRaisesRegex(ValueError, "Only classifiers and regressors are supported in selection service"):
            SelectionService().select(self.settings)

    def test_selection_cv_binary_target(self):
        """tests selection with cv and binary target (best modeller is not stacking or voting)"""
        expected_best_modeller = MagicMock()
        expected_best_modeller.fit.return_value = expected_best_modeller

        # first five for fitting in cv, last one for the best modeller
        self.mock_clf_1.clone.side_effect = [self.mock_clf_1_clone] * 5 + [expected_best_modeller]

        clf_1_scores = [0.5, 0.6, 0.3, 0.4, 0.5]
        self.mock_clf_1_clone.score.side_effect = clf_1_scores
        self.mock_clf_1_clone.predict.return_value = pd.DataFrame({"target": [1, 0, 1, 1, 0]})
        clf_2_scores = [0.2, 0.4, 0.3, 0.3, 0.2]
        self.mock_clf_2_clone.score.side_effect = clf_2_scores
        self.mock_clf_2_clone.predict.return_value = pd.DataFrame({"target": [0, 1, 1, 0, 0]})
        stacking_scores = [0.3, 0.5, 0.2, 0.1, 0.1]
        self.mock_stacking_classifier.return_value.score.side_effect = stacking_scores
        voting_scores = [0.2, 0.4, 0.2, 0.1, 0.1]
        self.mock_voting_classifier.return_value.score.side_effect = voting_scores
        output = SelectionService().select(self.settings)
        self.assertEqual([clf_1_scores, clf_2_scores], output.modeller_fold_scores)
        self.assertEqual([np.mean(clf_1_scores), np.mean(clf_2_scores)], output.modeller_scores)
        self.assertEqual(stacking_scores, output.stacking_fold_scores)
        self.assertEqual(np.mean(stacking_scores), output.stacking_score)
        self.assertEqual(voting_scores, output.voting_fold_scores)
        self.assertEqual(np.mean(voting_scores), output.voting_score)

        self.assertEqual(expected_best_modeller, output.best_modeller)
        expected_best_modeller.fit.assert_called_with(self.settings.train_data, self.settings.target_data)

        # A new stacking classifier generated for every cv fold, with GamClassifier as final classifier
        self.assertEqual(5, self.mock_stacking_classifier.call_count)
        for call_args in self.mock_stacking_classifier.call_args_list:
            self.assertEqual(self.mock_gam_classifier.return_value, call_args.kwargs["final_estimator"])

    def test_selection_cv_binary_target_stacking_as_best_modeller(self):
        """tests selection with cv and binary target (best modeller is stacking)"""
        expected_best_modeller = MagicMock()
        expected_best_modeller.fit.return_value = expected_best_modeller

        mock_stacking_clf = MagicMock()
        # first five for fitting in cv, last one for generating the best modeller in _get_output
        self.mock_stacking_classifier.side_effect = [mock_stacking_clf] * 5 + [expected_best_modeller]

        clf_1_scores = [0.5, 0.6, 0.3, 0.4, 0.5]
        self.mock_clf_1_clone.score.side_effect = clf_1_scores
        self.mock_clf_1_clone.predict.return_value = pd.DataFrame({"target": [1, 0, 1, 1, 0]})
        clf_2_scores = [0.2, 0.4, 0.3, 0.3, 0.2]
        self.mock_clf_2_clone.score.side_effect = clf_2_scores
        self.mock_clf_2_clone.predict.return_value = pd.DataFrame({"target": [0, 1, 1, 0, 0]})
        stacking_scores = [0.9, 0.8, 0.8, 0.9, 0.8]
        mock_stacking_clf.score.side_effect = stacking_scores
        voting_scores = [0.2, 0.4, 0.2, 0.1, 0.1]
        self.mock_voting_classifier.return_value.score.side_effect = voting_scores
        output = SelectionService().select(self.settings)
        self.assertEqual([clf_1_scores, clf_2_scores], output.modeller_fold_scores)
        self.assertEqual([np.mean(clf_1_scores), np.mean(clf_2_scores)], output.modeller_scores)
        self.assertEqual(stacking_scores, output.stacking_fold_scores)
        self.assertEqual(np.mean(stacking_scores), output.stacking_score)
        self.assertEqual(voting_scores, output.voting_fold_scores)
        self.assertEqual(np.mean(voting_scores), output.voting_score)

        self.assertEqual(expected_best_modeller, output.best_modeller)
        expected_best_modeller.fit.assert_called_once_with(self.settings.train_data, self.settings.target_data)

        # A new stacking classifier generated for every cv fold and once as best_modeller,
        # with GamClassifier as final classifier
        self.assertEqual(6, self.mock_stacking_classifier.call_count)
        for call_args in self.mock_stacking_classifier.call_args_list:
            self.assertEqual(self.mock_gam_classifier.return_value, call_args.kwargs["final_estimator"])

    def test_selection_cv_binary_target_voting_as_best_modeller(self):
        """tests selection with cv and binary target (best modeller is voting)"""
        expected_best_modeller = MagicMock()
        expected_best_modeller.fit.return_value = expected_best_modeller

        mock_voting_clf = MagicMock()
        # first five for fitting in cv, last one for generating the best modeller in _get_output
        self.mock_voting_classifier.side_effect = [mock_voting_clf] * 5 + [expected_best_modeller]

        clf_1_scores = [0.5, 0.6, 0.3, 0.4, 0.5]
        self.mock_clf_1_clone.score.side_effect = clf_1_scores
        self.mock_clf_1_clone.predict.return_value = pd.DataFrame({"target": [1, 0, 1, 1, 0]})
        clf_2_scores = [0.2, 0.4, 0.3, 0.3, 0.2]
        self.mock_clf_2_clone.score.side_effect = clf_2_scores
        self.mock_clf_2_clone.predict.return_value = pd.DataFrame({"target": [0, 1, 1, 0, 0]})
        stacking_scores = [0.2, 0.4, 0.2, 0.1, 0.1]
        self.mock_stacking_classifier.return_value.score.side_effect = stacking_scores
        voting_scores = [0.9, 0.8, 0.8, 0.9, 0.8]
        mock_voting_clf.score.side_effect = voting_scores
        output = SelectionService().select(self.settings)
        self.assertEqual([clf_1_scores, clf_2_scores], output.modeller_fold_scores)
        self.assertEqual([np.mean(clf_1_scores), np.mean(clf_2_scores)], output.modeller_scores)
        self.assertEqual(stacking_scores, output.stacking_fold_scores)
        self.assertEqual(np.mean(stacking_scores), output.stacking_score)
        self.assertEqual(voting_scores, output.voting_fold_scores)
        self.assertEqual(np.mean(voting_scores), output.voting_score)

        self.assertEqual(expected_best_modeller, output.best_modeller)
        expected_best_modeller.fit.assert_called_once_with(self.settings.train_data, self.settings.target_data)

        # A new stacking classifier generated for every cv fold, with GamClassifier as final classifier
        self.assertEqual(5, self.mock_stacking_classifier.call_count)
        for call_args in self.mock_stacking_classifier.call_args_list:
            self.assertEqual(self.mock_gam_classifier.return_value, call_args.kwargs["final_estimator"])

    def test_selection_no_stacking(self):
        """tests selection with add_stacking=False"""
        self.settings.add_stacking = False

        expected_best_modeller = MagicMock()
        expected_best_modeller.fit.return_value = expected_best_modeller

        # first five for fitting in cv, last one for the best modeller
        self.mock_clf_1.clone.side_effect = [self.mock_clf_1_clone] * 5 + [expected_best_modeller]

        clf_1_scores = [0.5, 0.6, 0.3, 0.4, 0.5]
        self.mock_clf_1_clone.score.side_effect = clf_1_scores
        self.mock_clf_1_clone.predict.return_value = pd.DataFrame({"target": [1, 0, 1, 1, 0]})
        clf_2_scores = [0.2, 0.4, 0.3, 0.3, 0.2]
        self.mock_clf_2_clone.score.side_effect = clf_2_scores
        self.mock_clf_2_clone.predict.return_value = pd.DataFrame({"target": [0, 1, 1, 0, 0]})
        stacking_scores = [0.2, 0.4, 0.2, 0.1, 0.1]
        self.mock_stacking_classifier.return_value.score.side_effect = stacking_scores
        voting_scores = [0.1, 0.1, 0.1, 0.1, 0.1]
        self.mock_voting_classifier.return_value.score.side_effect = voting_scores
        output = SelectionService().select(self.settings)
        self.assertIsNone(output.stacking_fold_scores)
        self.assertIsNone(output.stacking_score)
        self.mock_stacking_classifier.assert_not_called()
        self.assertEqual([clf_1_scores, clf_2_scores], output.modeller_fold_scores)
        self.assertEqual([np.mean(clf_1_scores), np.mean(clf_2_scores)], output.modeller_scores)
        self.assertEqual(voting_scores, output.voting_fold_scores)
        self.assertEqual(np.mean(voting_scores), output.voting_score)

        self.assertEqual(expected_best_modeller, output.best_modeller)
        expected_best_modeller.fit.assert_called_with(self.settings.train_data, self.settings.target_data)

    def test_selection_no_voting(self):
        """tests selection with add_voting=False"""
        self.settings.add_voting = False

        expected_best_modeller = MagicMock()
        expected_best_modeller.fit.return_value = expected_best_modeller

        # first five for fitting in cv, last one for the best modeller
        self.mock_clf_1.clone.side_effect = [self.mock_clf_1_clone] * 5 + [expected_best_modeller]

        clf_1_scores = [0.5, 0.6, 0.3, 0.4, 0.5]
        self.mock_clf_1_clone.score.side_effect = clf_1_scores
        self.mock_clf_1_clone.predict.return_value = pd.DataFrame({"target": [1, 0, 1, 1, 0]})
        clf_2_scores = [0.2, 0.4, 0.3, 0.3, 0.2]
        self.mock_clf_2_clone.score.side_effect = clf_2_scores
        self.mock_clf_2_clone.predict.return_value = pd.DataFrame({"target": [0, 1, 1, 0, 0]})
        stacking_scores = [0.2, 0.4, 0.2, 0.1, 0.1]
        self.mock_stacking_classifier.return_value.score.side_effect = stacking_scores
        voting_scores = [0.1, 0.1, 0.1, 0.1, 0.1]
        self.mock_voting_classifier.return_value.score.side_effect = voting_scores
        output = SelectionService().select(self.settings)
        self.assertIsNone(output.voting_fold_scores)
        self.assertIsNone(output.voting_score)
        self.mock_voting_classifier.assert_not_called()
        self.assertEqual([clf_1_scores, clf_2_scores], output.modeller_fold_scores)
        self.assertEqual([np.mean(clf_1_scores), np.mean(clf_2_scores)], output.modeller_scores)
        self.assertEqual(stacking_scores, output.stacking_fold_scores)
        self.assertEqual(np.mean(stacking_scores), output.stacking_score)

        self.assertEqual(expected_best_modeller, output.best_modeller)
        expected_best_modeller.fit.assert_called_with(self.settings.train_data, self.settings.target_data)

    def test_selection_no_stacking_no_voting(self):
        """tests selection with add_stacking=False and add_voting=False"""
        self.settings.add_stacking = False
        self.settings.add_voting = False

        expected_best_modeller = MagicMock()
        expected_best_modeller.fit.return_value = expected_best_modeller

        # first five for fitting in cv, last one for the best modeller
        self.mock_clf_1.clone.side_effect = [self.mock_clf_1_clone] * 5 + [expected_best_modeller]

        clf_1_scores = [0.5, 0.6, 0.3, 0.4, 0.5]
        self.mock_clf_1_clone.score.side_effect = clf_1_scores
        self.mock_clf_1_clone.predict.return_value = pd.DataFrame({"target": [1, 0, 1, 1, 0]})
        clf_2_scores = [0.2, 0.4, 0.3, 0.3, 0.2]
        self.mock_clf_2_clone.score.side_effect = clf_2_scores
        self.mock_clf_2_clone.predict.return_value = pd.DataFrame({"target": [0, 1, 1, 0, 0]})
        stacking_scores = [0.2, 0.4, 0.2, 0.1, 0.1]
        self.mock_stacking_classifier.return_value.score.side_effect = stacking_scores
        voting_scores = [0.1, 0.1, 0.1, 0.1, 0.1]
        self.mock_voting_classifier.return_value.score.side_effect = voting_scores
        output = SelectionService().select(self.settings)
        self.assertIsNone(output.stacking_fold_scores)
        self.assertIsNone(output.stacking_score)
        self.assertIsNone(output.voting_fold_scores)
        self.assertIsNone(output.voting_score)
        self.mock_stacking_classifier.assert_not_called()
        self.mock_voting_classifier.assert_not_called()
        self.assertEqual([clf_1_scores, clf_2_scores], output.modeller_fold_scores)
        self.assertEqual([np.mean(clf_1_scores), np.mean(clf_2_scores)], output.modeller_scores)

        self.assertEqual(expected_best_modeller, output.best_modeller)
        expected_best_modeller.fit.assert_called_with(self.settings.train_data, self.settings.target_data)

    def test_selection_with_test_data_binary_target(self):
        """tests selection with test_data and binary target (best modeller is not stacking or voting)"""
        self.settings.test_data = pd.DataFrame({"a": [1, 2, 3, 4, 5]})
        self.settings.test_target_data = pd.Series([1, 0, 1, 1, 0])

        self.mock_clf_1_clone.score.return_value = 0.6
        self.mock_clf_1_clone.predict.return_value = pd.DataFrame({"target": [1, 0, 1, 1, 0]})
        self.mock_clf_2_clone.score.return_value = 0.9
        self.mock_clf_2_clone.predict.return_value = pd.DataFrame({"target": [0, 1, 1, 0, 0]})
        self.mock_stacking_classifier.return_value.score.return_value = 0.7
        self.mock_voting_classifier.return_value.score.return_value = 0.5
        output = SelectionService().select(self.settings)

        self.assertEqual([[0.6], [0.9]], output.modeller_fold_scores)
        self.assertEqual([0.6, 0.9], output.modeller_scores)
        self.assertEqual([0.7], output.stacking_fold_scores)
        self.assertEqual(0.7, output.stacking_score)
        self.assertEqual([0.5], output.voting_fold_scores)
        self.assertEqual(0.5, output.voting_score)

        self.assertEqual(self.mock_clf_2_clone, output.best_modeller)
        # should be fitted only once
        self.mock_clf_2_clone.fit.assert_called_once_with(self.settings.train_data, self.settings.target_data)

        # Stacking classifier should be generated once with GamClassifier as final estimator
        self.assertEqual(1, self.mock_stacking_classifier.call_count)
        kwargs = self.mock_stacking_classifier.call_args.kwargs
        self.assertEqual(self.mock_gam_classifier.return_value, kwargs["final_estimator"])

    def test_selection_with_test_data_binary_target_stacking_as_best_modeller(self):
        """tests selection with test_data and binary target (best modeller is stacking)"""
        self.settings.test_data = pd.DataFrame({"a": [1, 2, 3, 4, 5]})
        self.settings.test_target_data = pd.Series([1, 0, 1, 1, 0])

        self.mock_clf_1_clone.score.return_value = 0.6
        self.mock_clf_1_clone.predict.return_value = pd.DataFrame({"target": [1, 0, 1, 1, 0]})
        self.mock_clf_2_clone.score.return_value = 0.7
        self.mock_clf_2_clone.predict.return_value = pd.DataFrame({"target": [0, 1, 1, 0, 0]})
        self.mock_stacking_classifier.return_value.score.return_value = 1.0
        self.mock_voting_classifier.return_value.score.return_value = 0.5
        output = SelectionService().select(self.settings)

        self.assertEqual([[0.6], [0.7]], output.modeller_fold_scores)
        self.assertEqual([0.6, 0.7], output.modeller_scores)
        self.assertEqual([1.0], output.stacking_fold_scores)
        self.assertEqual(1.0, output.stacking_score)
        self.assertEqual([0.5], output.voting_fold_scores)
        self.assertEqual(0.5, output.voting_score)

        self.assertEqual(self.mock_stacking_classifier.return_value, output.best_modeller)
        # should be fitted only once
        self.mock_stacking_classifier.return_value.fit.assert_called_once_with(self.settings.train_data,
                                                                               self.settings.target_data)

        # Stacking classifier should be generated once with GamClassifier as final estimator
        self.assertEqual(1, self.mock_stacking_classifier.call_count)
        kwargs = self.mock_stacking_classifier.call_args.kwargs
        self.assertEqual(self.mock_gam_classifier.return_value, kwargs["final_estimator"])

    def test_selection_with_test_data_binary_target_voting_as_best_modeller(self):
        """tests selection with test_data and binary target (best modeller is voting)"""
        self.settings.test_data = pd.DataFrame({"a": [1, 2, 3, 4, 5]})
        self.settings.test_target_data = pd.Series([1, 0, 1, 1, 0])

        self.mock_clf_1_clone.score.return_value = 0.6
        self.mock_clf_1_clone.predict.return_value = pd.DataFrame({"target": [1, 0, 1, 1, 0]})
        self.mock_clf_2_clone.score.return_value = 0.7
        self.mock_clf_2_clone.predict.return_value = pd.DataFrame({"target": [0, 1, 1, 0, 0]})
        self.mock_stacking_classifier.return_value.score.return_value = 0.2
        self.mock_voting_classifier.return_value.score.return_value = 1.0
        output = SelectionService().select(self.settings)

        self.assertEqual([[0.6], [0.7]], output.modeller_fold_scores)
        self.assertEqual([0.6, 0.7], output.modeller_scores)
        self.assertEqual([0.2], output.stacking_fold_scores)
        self.assertEqual(0.2, output.stacking_score)
        self.assertEqual([1.0], output.voting_fold_scores)
        self.assertEqual(1.0, output.voting_score)

        self.assertEqual(self.mock_voting_classifier.return_value, output.best_modeller)
        # should be fitted only once
        self.mock_voting_classifier.return_value.fit.assert_called_once_with(self.settings.train_data,
                                                                             self.settings.target_data)

        # Stacking classifier should be generated once with GamClassifier as final estimator
        self.assertEqual(1, self.mock_stacking_classifier.call_count)
        kwargs = self.mock_stacking_classifier.call_args.kwargs
        self.assertEqual(self.mock_gam_classifier.return_value, kwargs["final_estimator"])

    def test_selection_with_cv_for_multiclass(self):
        """tests selection with cv and multiclass target (best modeller is not stacking or voting)"""
        expected_best_modeller = MagicMock()
        expected_best_modeller.fit.return_value = expected_best_modeller
        self.settings.target_type = TargetType.MULTICLASS

        # first five for fitting in cv, last one for the best modeller
        self.mock_clf_1.clone.side_effect = [self.mock_clf_1_clone] * 5 + [expected_best_modeller]

        clf_1_scores = [0.5, 0.6, 0.3, 0.4, 0.5]  # this has the highest mean
        self.mock_clf_1_clone.score.side_effect = clf_1_scores
        self.mock_clf_1_clone.predict.return_value = pd.DataFrame({"target": [1, 0, 1, 1, 0]})
        clf_2_scores = [0.2, 0.4, 0.3, 0.3, 0.2]
        self.mock_clf_2_clone.score.side_effect = clf_2_scores
        self.mock_clf_2_clone.predict.return_value = pd.DataFrame({"target": [0, 1, 1, 0, 0]})
        stacking_scores = [0.3, 0.5, 0.2, 0.1, 0.1]
        self.mock_stacking_classifier.return_value.score.side_effect = stacking_scores
        voting_scores = [0.2, 0.4, 0.2, 0.1, 0.1]
        self.mock_voting_classifier.return_value.score.side_effect = voting_scores
        output = SelectionService().select(self.settings)
        self.assertEqual([clf_1_scores, clf_2_scores], output.modeller_fold_scores)
        self.assertEqual([np.mean(clf_1_scores), np.mean(clf_2_scores)], output.modeller_scores)
        self.assertEqual(stacking_scores, output.stacking_fold_scores)
        self.assertEqual(np.mean(stacking_scores), output.stacking_score)
        self.assertEqual(voting_scores, output.voting_fold_scores)
        self.assertEqual(np.mean(voting_scores), output.voting_score)

        self.assertEqual(expected_best_modeller, output.best_modeller)
        expected_best_modeller.fit.assert_called_with(self.settings.train_data, self.settings.target_data)

        # A new stacking classifier generated for every cv fold, with RFClassifier as final classifier
        self.assertEqual(5, self.mock_stacking_classifier.call_count)
        for call_args in self.mock_stacking_classifier.call_args_list:
            self.assertEqual(self.mock_rf_classifier.return_value, call_args.kwargs["final_estimator"])

    def test_selection_with_cv_for_regression(self):
        """tests selection with cv and scalar target"""

        mock_reg_1_clone = MagicMock(spec=BaseRegressor)
        mock_reg_1 = MagicMock(spec=BaseRegressor)
        mock_reg_1.clone.return_value = mock_reg_1_clone
        mock_reg_1.modeller_type = ModellerType.REGRESSOR
        mock_reg_2_clone = MagicMock(spec=BaseRegressor)
        mock_reg_2 = MagicMock(spec=BaseRegressor)
        mock_reg_2.modeller_type = ModellerType.REGRESSOR
        mock_reg_2.clone.return_value = mock_reg_2_clone
        self.settings.modellers = [mock_reg_1, mock_reg_2]
        self.settings.target_type = TargetType.SCALAR

        expected_best_modeller = MagicMock()
        expected_best_modeller.fit.return_value = expected_best_modeller

        # first five for fitting in cv, last one for the best modeller
        mock_reg_1.clone.side_effect = [mock_reg_1_clone] * 5 + [expected_best_modeller]

        reg_1_scores = [0.5, 0.6, 0.3, 0.4, 0.5]  # this has the highest mean
        mock_reg_1_clone.score.side_effect = reg_1_scores
        mock_reg_1_clone.predict.return_value = pd.DataFrame({"target": [1, 0, 1, 1, 0]})
        reg_2_scores = [0.2, 0.4, 0.3, 0.3, 0.2]
        mock_reg_2_clone.score.side_effect = reg_2_scores
        mock_reg_2_clone.predict.return_value = pd.DataFrame({"target": [0, 1, 1, 0, 0]})
        stacking_scores = [0.3, 0.5, 0.2, 0.1, 0.1]
        self.mock_stacking_regressor.return_value.score.side_effect = stacking_scores
        voting_scores = [0.2, 0.4, 0.2, 0.1, 0.1]
        self.mock_voting_regressor.return_value.score.side_effect = voting_scores
        output = SelectionService().select(self.settings)
        self.assertEqual([reg_1_scores, reg_2_scores], output.modeller_fold_scores)
        self.assertEqual([np.mean(reg_1_scores), np.mean(reg_2_scores)], output.modeller_scores)
        self.assertEqual(stacking_scores, output.stacking_fold_scores)
        self.assertEqual(np.mean(stacking_scores), output.stacking_score)
        self.assertEqual(voting_scores, output.voting_fold_scores)
        self.assertEqual(np.mean(voting_scores), output.voting_score)

        self.assertEqual(expected_best_modeller, output.best_modeller)
        expected_best_modeller.fit.assert_called_with(self.settings.train_data, self.settings.target_data)

        # A new stacking regressor generated for every cv fold, with GamRegressor as final classifier
        self.assertEqual(5, self.mock_stacking_regressor.call_count)
        for call_args in self.mock_stacking_regressor.call_args_list:
            self.assertEqual(self.mock_gam_regressor.return_value, call_args.kwargs["final_estimator"])

    def test_selection_with_parallel_cv(self):
        """tests execution with cv parallelized"""
        self.settings.num_threads_for_parallel_cv = 3
        expected_best_modeller = MagicMock()
        expected_best_modeller.fit.return_value = expected_best_modeller

        # first five for fitting in cv, last one for the best modeller
        self.mock_clf_1.clone.side_effect = [self.mock_clf_1_clone] * 5 + [expected_best_modeller]

        clf_1_scores = [0.5, 0.6, 0.3, 0.4, 0.5]
        self.mock_clf_1_clone.score.side_effect = clf_1_scores
        self.mock_clf_1_clone.predict.return_value = pd.DataFrame({"target": [1, 0, 1, 1, 0]})
        clf_2_scores = [0.2, 0.4, 0.3, 0.3, 0.2]
        self.mock_clf_2_clone.score.side_effect = clf_2_scores
        self.mock_clf_2_clone.predict.return_value = pd.DataFrame({"target": [0, 1, 1, 0, 0]})
        stacking_scores = [0.3, 0.5, 0.2, 0.1, 0.1]
        self.mock_stacking_classifier.return_value.score.side_effect = stacking_scores
        voting_scores = [0.2, 0.4, 0.2, 0.1, 0.1]
        self.mock_voting_classifier.return_value.score.side_effect = voting_scores
        output = SelectionService().select(self.settings)
        self.assertEqual([clf_1_scores, clf_2_scores], output.modeller_fold_scores)
        self.assertEqual([np.mean(clf_1_scores), np.mean(clf_2_scores)], output.modeller_scores)
        self.assertEqual(stacking_scores, output.stacking_fold_scores)
        self.assertEqual(np.mean(stacking_scores), output.stacking_score)
        self.assertEqual(voting_scores, output.voting_fold_scores)
        self.assertEqual(np.mean(voting_scores), output.voting_score)

        self.assertEqual(expected_best_modeller, output.best_modeller)
        expected_best_modeller.fit.assert_called_with(self.settings.train_data, self.settings.target_data)

        # A new stacking classifier generated for every cv fold, with GamClassifier as final classifier
        self.assertEqual(5, self.mock_stacking_classifier.call_count)
        for call_args in self.mock_stacking_classifier.call_args_list:
            self.assertEqual(self.mock_gam_classifier.return_value, call_args.kwargs["final_estimator"])

        self.assertEqual(3, self.mock_parallel_exec_helper.execute_parallel.call_args.kwargs["num_jobs"])

    def test_selection_with_parallel_fit(self):
        """tests execution with modeller fitting parallelized"""
        self.settings.num_threads_for_parallel_fit = 3
        expected_best_modeller = MagicMock()
        expected_best_modeller.fit.return_value = expected_best_modeller

        # first five for fitting in cv, last one for the best modeller
        self.mock_clf_1.clone.side_effect = [self.mock_clf_1_clone] * 5 + [expected_best_modeller]

        clf_1_scores = [0.5, 0.6, 0.3, 0.4, 0.5]
        self.mock_clf_1_clone.score.side_effect = clf_1_scores
        self.mock_clf_1_clone.predict.return_value = pd.DataFrame({"target": [1, 0, 1, 1, 0]})
        clf_2_scores = [0.2, 0.4, 0.3, 0.3, 0.2]
        self.mock_clf_2_clone.score.side_effect = clf_2_scores
        self.mock_clf_2_clone.predict.return_value = pd.DataFrame({"target": [0, 1, 1, 0, 0]})
        stacking_scores = [0.3, 0.5, 0.2, 0.1, 0.1]
        self.mock_stacking_classifier.return_value.score.side_effect = stacking_scores
        voting_scores = [0.2, 0.4, 0.2, 0.1, 0.1]
        self.mock_voting_classifier.return_value.score.side_effect = voting_scores
        output = SelectionService().select(self.settings)
        self.assertEqual([clf_1_scores, clf_2_scores], output.modeller_fold_scores)
        self.assertEqual([np.mean(clf_1_scores), np.mean(clf_2_scores)], output.modeller_scores)
        self.assertEqual(stacking_scores, output.stacking_fold_scores)
        self.assertEqual(np.mean(stacking_scores), output.stacking_score)
        self.assertEqual(voting_scores, output.voting_fold_scores)
        self.assertEqual(np.mean(voting_scores), output.voting_score)

        self.assertEqual(expected_best_modeller, output.best_modeller)
        expected_best_modeller.fit.assert_called_with(self.settings.train_data, self.settings.target_data)

        # A new stacking classifier generated for every cv fold, with GamClassifier as final classifier
        self.assertEqual(5, self.mock_stacking_classifier.call_count)
        for call_args in self.mock_stacking_classifier.call_args_list:
            self.assertEqual(self.mock_gam_classifier.return_value, call_args.kwargs["final_estimator"])

        # there will be a parallel fit in every fold
        self.assertEqual(5, self.mock_parallel_exec_helper.execute_parallel.call_count)
        for i in range(5):
            self.assertEqual(
                3, self.mock_parallel_exec_helper.execute_parallel.call_args_list[i].kwargs["num_jobs"])

    def test_selection_with_parallel_fit_stacking_as_best_modeller(self):
        """tests execution with modeller fitting parallelized (stacking as best modeller)"""
        self.settings.num_threads_for_parallel_fit = 3
        expected_best_modeller = MagicMock()
        expected_best_modeller.fit.return_value = expected_best_modeller

        mock_stacking_clf = MagicMock()
        # first five for fitting in cv, last one for generating the best modeller in _get_output
        self.mock_stacking_classifier.side_effect = [mock_stacking_clf] * 5 + [expected_best_modeller]

        clf_1_scores = [0.5, 0.6, 0.3, 0.4, 0.5]
        self.mock_clf_1_clone.score.side_effect = clf_1_scores
        self.mock_clf_1_clone.predict.return_value = pd.DataFrame({"target": [1, 0, 1, 1, 0]})
        clf_2_scores = [0.2, 0.4, 0.3, 0.3, 0.2]
        self.mock_clf_2_clone.score.side_effect = clf_2_scores
        self.mock_clf_2_clone.predict.return_value = pd.DataFrame({"target": [0, 1, 1, 0, 0]})
        stacking_scores = [0.9, 0.8, 0.8, 0.9, 0.8]
        mock_stacking_clf.score.side_effect = stacking_scores
        voting_scores = [0.2, 0.4, 0.2, 0.1, 0.1]
        self.mock_voting_classifier.return_value.score.side_effect = voting_scores
        output = SelectionService().select(self.settings)
        self.assertEqual([clf_1_scores, clf_2_scores], output.modeller_fold_scores)
        self.assertEqual([np.mean(clf_1_scores), np.mean(clf_2_scores)], output.modeller_scores)
        self.assertEqual(stacking_scores, output.stacking_fold_scores)
        self.assertEqual(np.mean(stacking_scores), output.stacking_score)
        self.assertEqual(voting_scores, output.voting_fold_scores)
        self.assertEqual(np.mean(voting_scores), output.voting_score)

        self.assertEqual(expected_best_modeller, output.best_modeller)
        expected_best_modeller.fit.assert_called_once_with(self.settings.train_data, self.settings.target_data)

        # A new stacking classifier generated for every cv fold(5), and once more as the best modeller,
        # with GamClassifier as final classifier
        self.assertEqual(6, self.mock_stacking_classifier.call_count)
        for call_args in self.mock_stacking_classifier.call_args_list:
            self.assertEqual(self.mock_gam_classifier.return_value, call_args.kwargs["final_estimator"])

        # there will be a parallel fit in every fold,
        # also a parallel fit for generating stacking modeller as the best modeller
        self.assertEqual(6, self.mock_parallel_exec_helper.execute_parallel.call_count)
        for i in range(6):
            self.assertEqual(
                3, self.mock_parallel_exec_helper.execute_parallel.call_args_list[i].kwargs["num_jobs"])

    def test_selection_with_parallel_fit_and_parallel_cv(self):
        """tests execution with modeller fitting and cv parallelized"""
        self.settings.num_threads_for_parallel_fit = 3
        self.settings.num_threads_for_parallel_cv = 4
        expected_best_modeller = MagicMock()
        expected_best_modeller.fit.return_value = expected_best_modeller

        # first five for fitting in cv, last one for the best modeller
        self.mock_clf_1.clone.side_effect = [self.mock_clf_1_clone] * 5 + [expected_best_modeller]

        clf_1_scores = [0.5, 0.6, 0.3, 0.4, 0.5]
        self.mock_clf_1_clone.score.side_effect = clf_1_scores
        self.mock_clf_1_clone.predict.return_value = pd.DataFrame({"target": [1, 0, 1, 1, 0]})
        clf_2_scores = [0.2, 0.4, 0.3, 0.3, 0.2]
        self.mock_clf_2_clone.score.side_effect = clf_2_scores
        self.mock_clf_2_clone.predict.return_value = pd.DataFrame({"target": [0, 1, 1, 0, 0]})
        stacking_scores = [0.3, 0.5, 0.2, 0.1, 0.1]
        self.mock_stacking_classifier.return_value.score.side_effect = stacking_scores
        voting_scores = [0.2, 0.4, 0.2, 0.1, 0.1]
        self.mock_voting_classifier.return_value.score.side_effect = voting_scores
        output = SelectionService().select(self.settings)
        self.assertEqual([clf_1_scores, clf_2_scores], output.modeller_fold_scores)
        self.assertEqual([np.mean(clf_1_scores), np.mean(clf_2_scores)], output.modeller_scores)
        self.assertEqual(stacking_scores, output.stacking_fold_scores)
        self.assertEqual(np.mean(stacking_scores), output.stacking_score)
        self.assertEqual(voting_scores, output.voting_fold_scores)
        self.assertEqual(np.mean(voting_scores), output.voting_score)

        self.assertEqual(expected_best_modeller, output.best_modeller)
        expected_best_modeller.fit.assert_called_with(self.settings.train_data, self.settings.target_data)

        # A new stacking classifier generated for every cv fold, with GamClassifier as final classifier
        self.assertEqual(5, self.mock_stacking_classifier.call_count)
        for call_args in self.mock_stacking_classifier.call_args_list:
            self.assertEqual(self.mock_gam_classifier.return_value, call_args.kwargs["final_estimator"])

        # first call for parallelizing cv, then there will be a parallel fit in every fold (5)
        self.assertEqual(6, self.mock_parallel_exec_helper.execute_parallel.call_count)
        self.assertEqual(
            4, self.mock_parallel_exec_helper.execute_parallel.call_args_list[0].kwargs["num_jobs"])
        for i in range(1, 6):
            self.assertEqual(
                3, self.mock_parallel_exec_helper.execute_parallel.call_args_list[i].kwargs["num_jobs"])

    def test_selection_with_test_data_parallel_fit(self):
        """tests selection with test_data with parallel fitting of modellers"""
        self.settings.num_threads_for_parallel_fit = 3
        self.settings.test_data = pd.DataFrame({"a": [1, 2, 3, 4, 5]})
        self.settings.test_target_data = pd.Series([1, 0, 1, 1, 0])

        self.mock_clf_1_clone.score.return_value = 0.6
        self.mock_clf_1_clone.predict.return_value = pd.DataFrame({"target": [1, 0, 1, 1, 0]})
        self.mock_clf_2_clone.score.return_value = 0.9
        self.mock_clf_2_clone.predict.return_value = pd.DataFrame({"target": [0, 1, 1, 0, 0]})
        self.mock_stacking_classifier.return_value.score.return_value = 0.7
        self.mock_voting_classifier.return_value.score.return_value = 0.5
        output = SelectionService().select(self.settings)

        self.assertEqual([[0.6], [0.9]], output.modeller_fold_scores)
        self.assertEqual([0.6, 0.9], output.modeller_scores)
        self.assertEqual([0.7], output.stacking_fold_scores)
        self.assertEqual(0.7, output.stacking_score)
        self.assertEqual([0.5], output.voting_fold_scores)
        self.assertEqual(0.5, output.voting_score)

        self.assertEqual(self.mock_clf_2_clone, output.best_modeller)
        # should be fitted only once
        self.mock_clf_2_clone.fit.assert_called_once_with(self.settings.train_data, self.settings.target_data)

        # Stacking classifier should be generated once with GamClassifier as final estimator
        self.assertEqual(1, self.mock_stacking_classifier.call_count)
        kwargs = self.mock_stacking_classifier.call_args.kwargs
        self.assertEqual(self.mock_gam_classifier.return_value, kwargs["final_estimator"])

        self.assertEqual(1, self.mock_parallel_exec_helper.execute_parallel.call_count)
        self.assertEqual(3,
                         self.mock_parallel_exec_helper.execute_parallel.call_args.kwargs["num_jobs"])


if __name__ == '__main__':
    unittest.main()
