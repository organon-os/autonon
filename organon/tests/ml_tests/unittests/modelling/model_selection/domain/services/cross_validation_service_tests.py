"""Includes unittests for CrossValidationService"""
import unittest
from unittest.mock import patch

import numpy as np
import pandas as pd

from organon.ml.modelling.algorithms.classifiers.gbm_classifier import GBMClassifier
from organon.ml.modelling.algorithms.classifiers.logistic_regression_classifier import LogisticRegressionClassifier
from organon.ml.modelling.algorithms.core.enums.modeller_type import ModellerType
from organon.ml.modelling.algorithms.regressors.gbm_regressor import GBMRegressor
from organon.ml.modelling.algorithms.regressors.xgboost_regressor import XGBoostRegressor
from organon.ml.modelling.model_selection.domain.services import cross_validation_service
from organon.ml.modelling.model_selection.domain.services.cross_validation_service import CrossValidationService
from organon.ml.modelling.model_selection.settings.objects.cross_validation_settings import CrossValidationSettings


class CrossValidationServiceTestCase(unittest.TestCase):
    """Unittest class for CrossValidationService"""

    def setUp(self) -> None:
        cross_val_patcher = patch(cross_validation_service.__name__ + ".cross_val_score")
        self.mock_cross_val_score = cross_val_patcher.start()
        self.addCleanup(cross_val_patcher.stop)

    def test_with_classifiers(self):
        """tests cross validation with classifiers"""
        classifiers = [GBMClassifier(), LogisticRegressionClassifier()]
        train_data = pd.DataFrame({"a": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]})
        target_data = pd.Series([0, 1, 0, 1, 1, 0, 2, 2, 2, 1, 1, 1])
        settings = CrossValidationSettings(classifiers, train_data, target_data, cv_count=3,
                                           bin_count=5,
                                           return_test_fold=True)
        expected_results = [[0.5, 0.6, 0.4], [0.3, 0.7, 0.8]]
        self.mock_cross_val_score.side_effect = [np.array(expected_results[0]), np.array(expected_results[1])]
        service = CrossValidationService()
        output = service.execute(settings)
        self.assertEqual(2, self.mock_cross_val_score.call_count)
        self.assertEqual(expected_results, output.fold_scores)
        self.assertEqual([0.5, 0.6], output.mean_scores)
        self.assertEqual(classifiers[1], output.best_modeller)
        self.assertIsNotNone(output.test_fold)
        self.assertEqual({0: 4, 1: 4, 2: 4}, pd.Series(output.test_fold).value_counts().to_dict())

        # check test fold is stratified
        for test_fold_index in [0, 1, 2]:
            self.assertEqual(1, sum(target_data[output.test_fold == test_fold_index] == 0))
            self.assertEqual(2, sum(target_data[output.test_fold == test_fold_index] == 1))
            self.assertEqual(1, sum(target_data[output.test_fold == test_fold_index] == 2))

    def test_with_regressors(self):
        """tests cross validation with regressors"""
        classifiers = [GBMRegressor(), XGBoostRegressor()]
        train_data = pd.DataFrame({"a": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]})
        target_data = pd.Series([0.15, 0.5, 0.9, 0.85, 0.3, 0.22, 0.72, 1.0, 1.0, 0.4, 0.39, 0.58])
        settings = CrossValidationSettings(classifiers, train_data, target_data, cv_count=3,
                                           bin_count=4,
                                           return_test_fold=True)
        expected_results = [[0.5, 0.6, 0.4], [0.3, 0.7, 0.8]]
        self.mock_cross_val_score.side_effect = [np.array(expected_results[0]), np.array(expected_results[1])]
        service = CrossValidationService()
        output = service.execute(settings)
        self.assertEqual(2, self.mock_cross_val_score.call_count)
        self.assertEqual(expected_results, output.fold_scores)
        self.assertEqual([0.5, 0.6], output.mean_scores)
        self.assertEqual(classifiers[1], output.best_modeller)
        self.assertIsNotNone(output.test_fold)
        self.assertEqual({0: 4, 1: 4, 2: 4}, pd.Series(output.test_fold).value_counts().to_dict())
        expected_target_bands = [[0.15, 0.22, 0.3], [0.39, 0.4, 0.5], [0.58, 0.72, 0.85], [0.9, 1.0, 1.0]]

        # check test fold is stratified after proper conversion from continuous to categorical
        for test_fold_index in [0, 1, 2]:
            self.assertEqual(1, sum(target_data[output.test_fold == test_fold_index].isin(expected_target_bands[0])))
            self.assertEqual(1, sum(target_data[output.test_fold == test_fold_index].isin(expected_target_bands[1])))
            self.assertEqual(1, sum(target_data[output.test_fold == test_fold_index].isin(expected_target_bands[2])))
            self.assertEqual(1, sum(target_data[output.test_fold == test_fold_index].isin(expected_target_bands[3])))

    def test_get_predefined_split_for_stratified_target(self):
        """tests get_predefined_split_for_stratified_target method"""
        train_data = pd.DataFrame({"a": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]})
        target_data = pd.Series([0, 1, 0, 1, 1, 0, 2, 2, 2, 1, 1, 1])
        split = CrossValidationService.get_predefined_split_for_stratified_target(train_data, target_data, cv_count=3)
        for test_fold_index in [0, 1, 2]:
            self.assertEqual(1, sum(target_data[split.test_fold == test_fold_index] == 0))
            self.assertEqual(2, sum(target_data[split.test_fold == test_fold_index] == 1))
            self.assertEqual(1, sum(target_data[split.test_fold == test_fold_index] == 2))

    def test_get_predefined_split_for_classification(self):
        """tests get_predefined_split method for classification"""
        train_data = pd.DataFrame({"a": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]})
        target_data = pd.Series([0, 1, 0, 1, 1, 0, 2, 2, 2, 1, 1, 1])
        split = CrossValidationService.get_predefined_split(train_data, target_data, ModellerType.CLASSIFIER,
                                                            cv_count=3)
        for test_fold_index in [0, 1, 2]:
            self.assertEqual(1, sum(target_data[split.test_fold == test_fold_index] == 0))
            self.assertEqual(2, sum(target_data[split.test_fold == test_fold_index] == 1))
            self.assertEqual(1, sum(target_data[split.test_fold == test_fold_index] == 2))

    def test_get_predefined_split_for_regression(self):
        """tests get_predefined_split method for regression"""
        train_data = pd.DataFrame({"a": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]})
        target_data = pd.Series([0.15, 0.5, 0.9, 0.85, 0.3, 0.22, 0.72, 1.0, 1.0, 0.4, 0.39, 0.58])
        split = CrossValidationService.get_predefined_split(train_data, target_data, ModellerType.REGRESSOR,
                                                            cv_count=3, bin_count=4)
        expected_target_bands = [[0.15, 0.22, 0.3], [0.39, 0.4, 0.5], [0.58, 0.72, 0.85], [0.9, 1.0, 1.0]]

        for test_fold_index in [0, 1, 2]:
            self.assertEqual(1, sum(target_data[split.test_fold == test_fold_index].isin(expected_target_bands[0])))
            self.assertEqual(1, sum(target_data[split.test_fold == test_fold_index].isin(expected_target_bands[1])))
            self.assertEqual(1, sum(target_data[split.test_fold == test_fold_index].isin(expected_target_bands[2])))
            self.assertEqual(1, sum(target_data[split.test_fold == test_fold_index].isin(expected_target_bands[3])))

    def test_get_predefined_split_for_regression_bin_count_not_given(self):
        """tests exception when get_predefined_split called for regression but bin_count not given"""
        train_data = pd.DataFrame({"a": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]})
        target_data = pd.Series([0.15, 0.5, 0.9, 0.85, 0.3, 0.22, 0.72, 1.0, 1.0, 0.4, 0.39, 0.58])

        with self.assertRaisesRegex(ValueError, "bin_count should not be None for regression"):
            CrossValidationService.get_predefined_split(train_data, target_data, ModellerType.REGRESSOR,
                                                        cv_count=3, bin_count=None)


if __name__ == '__main__':
    unittest.main()
