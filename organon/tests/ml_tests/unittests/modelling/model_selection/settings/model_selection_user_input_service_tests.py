"""Include unittests for ModelSelectionUserInputService"""
import unittest
from unittest.mock import MagicMock

import pandas as pd

from organon.ml.modelling.algorithms.classifiers.gbm_classifier import GBMClassifier
from organon.ml.modelling.algorithms.classifiers.rf_classifier import RFClassifier
from organon.ml.modelling.algorithms.core.enums.modeller import Modeller
from organon.ml.modelling.algorithms.regressors.xgboost_regressor import XGBoostRegressor
from organon.ml.modelling.model_selection.services.user_settings.user_hp_optimization_settings import \
    UserHPOptimizationSettings
from organon.ml.modelling.model_selection.settings.enums.search_type import SearchType
from organon.ml.modelling.model_selection.settings.model_selection_user_input_service import \
    ModelSelectionUserInputService


class ModelSelectionUserInputServiceTestCase(unittest.TestCase):
    """Unittest class for ModelSelectionUserInputService"""

    def setUp(self) -> None:
        self.service = ModelSelectionUserInputService()
        self.user_hpo_settings = UserHPOptimizationSettings(
            pd.DataFrame({"a": [1, 2, 3]}), pd.Series([0, 1, 0]), SearchType.GRID.name,
            modellers=[Modeller.LIGHTGBM_CLASSIFIER.name, Modeller.RF_CLASSIFIER.name],
            modeller_params=[{}, {}], cv_fold=10, scoring_metrics=None
        )

    def test_get_cross_validation_settings_no_estimator_error(self):
        """tests error when no modellers are given"""
        with self.assertRaisesRegex(ValueError, "Modellers not given"):
            self.service.get_cross_validation_settings([], pd.DataFrame({"a": [1, 2, 3]}),
                                                       pd.Series([1, 2, 3]), 3, 5, True)

    def test_get_cross_validation_settings_empty_train_data_error(self):
        """tests error when train data is empty"""
        with self.assertRaisesRegex(ValueError, "train_data cannot be empty"):
            self.service.get_cross_validation_settings([GBMClassifier()], pd.DataFrame({"a": []}),
                                                       pd.Series([1, 2, 3]), 3, 5, True)

    def test_get_cross_validation_settings_invalid_cv_count_error(self):
        """tests error when cv count is invalid"""
        with self.assertRaisesRegex(ValueError, "cv_count should be higher than 1"):
            self.service.get_cross_validation_settings([GBMClassifier()], pd.DataFrame({"a": [1, 2, 3]}),
                                                       pd.Series([1, 2, 3]), 0, 5, True)

    def test_get_cross_validation_settings_invalid_modeller_type(self):
        """tests error when modeller type is invalid"""
        mock = MagicMock()
        mock.modeller_type = None
        with self.assertRaisesRegex(ValueError, "Modellers should be either classifiers or regressors"):
            self.service.get_cross_validation_settings([mock, GBMClassifier()], pd.DataFrame({"a": [1, 2, 3]}),
                                                       pd.Series([1, 2, 3]), 3, 5, True)

    def test_get_cross_validation_settings_different_modeller_types_error(self):
        """tests error when modeller types are not the same"""
        with self.assertRaisesRegex(ValueError, "All modellers should be of same type"):
            self.service.get_cross_validation_settings([GBMClassifier(), XGBoostRegressor()],
                                                       pd.DataFrame({"a": [1, 2, 3]}), pd.Series([1, 2, 3]), 3, 5, True)

    def test_get_cross_validation_settings_bin_count_given_for_classification(self):
        """tests error when bin_count parameter is given when modellers are classifiers"""
        with self.assertRaisesRegex(ValueError, "bin_count is used only in regression modelling"):
            self.service.get_cross_validation_settings([GBMClassifier(), RFClassifier()],
                                                       pd.DataFrame({"a": [1, 2, 3]}), pd.Series([1, 2, 3]), 3, 2, True)

    def test_get_hp_optimization_settings_empty_train_data(self):
        """tests error when train_data is None"""
        self.user_hpo_settings.train_data = None
        with self.assertRaisesRegex(ValueError, "train_data should not be empty"):
            self.service.get_hp_optimization_settings(self.user_hpo_settings)

    def test_get_hp_optimization_settings_empty_target_data(self):
        """tests error when train_data is None"""
        self.user_hpo_settings.target_data = None
        with self.assertRaisesRegex(ValueError, "target_data should not be empty"):
            self.service.get_hp_optimization_settings(self.user_hpo_settings)

    def test_get_hp_optimization_settings_none_cv_and_none_test(self):
        """tests error when neither cv_fold nor test_data is given"""
        self.user_hpo_settings.test_data = None
        self.user_hpo_settings.cv_fold = None
        with self.assertRaisesRegex(ValueError, "Either cv_fold or test_data should be given"):
            self.service.get_hp_optimization_settings(self.user_hpo_settings)

    def test_get_hp_optimization_settings_one_of_test_data_or_test_target_missing(self):
        """tests error when only one of test_data and test_target_data is given"""
        self.user_hpo_settings.test_data = None
        self.user_hpo_settings.test_target_data = pd.Series([1, 2, 3])
        with self.assertRaisesRegex(ValueError, "Both test_data and test_target_data should be given"):
            self.service.get_hp_optimization_settings(self.user_hpo_settings)

    def test_get_hp_optimization_settings_different_modeller_types_error(self):
        """tests error when modellers have from different types"""
        self.user_hpo_settings.modellers = [Modeller.LIGHTGBM_CLASSIFIER.name, Modeller.LASSO.name]
        with self.assertRaisesRegex(ValueError,
                                    "Either all modellers should be classifiers or all of them should be regressors"):
            self.service.get_hp_optimization_settings(self.user_hpo_settings)

    def test_get_hp_optimization_settings_not_all_modeller_params_given_error(self):
        """tests error when not all modeller params are given"""
        self.user_hpo_settings.modellers = [Modeller.LIGHTGBM_CLASSIFIER.name, Modeller.RF_CLASSIFIER.name]
        self.user_hpo_settings.modeller_params = [{"a": 5}]
        with self.assertRaisesRegex(ValueError, "Please enter parameters for all modellers. "):
            self.service.get_hp_optimization_settings(self.user_hpo_settings)


if __name__ == '__main__':
    unittest.main()
