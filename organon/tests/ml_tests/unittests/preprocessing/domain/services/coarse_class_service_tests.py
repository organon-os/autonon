"""This module includes CoarseClassServiceTests"""
import unittest
from typing import Dict
from unittest.mock import patch

import numpy as np
import pandas as pd

from organon.fl.core.enums.column_native_type import ColumnNativeType
from organon.ml.common.enums.target_type import TargetType
from organon.ml.preprocessing.domain.objects.coarse_class_fit_output import CoarseClassFitOutput
from organon.ml.preprocessing.domain.services import coarse_class_service
from organon.ml.preprocessing.domain.services.coarse_class_service import CoarseClassService
from organon.ml.preprocessing.settings.objects.coarse_class_settings import CoarseClassSettings
from organon.tests import test_helper


class CoarseClassServiceTestCase(unittest.TestCase):
    """Test class for coarse classing"""

    def setUp(self) -> None:
        module_name = coarse_class_service.__name__
        self.mock_get_type = test_helper.get_mock(self, module_name + ".get_column_native_type")

    def test_fit_empty_data_error(self):
        """"tests error when data is empty"""
        settings = CoarseClassSettings(0.3, 1, TargetType.BINARY, 20, True, 0.3, 42, None, None)
        cc_service = CoarseClassService(settings)
        with self.assertRaisesRegex(ValueError, "Data cannot be empty or None"):
            cc_service.fit(pd.DataFrame({"b": []}), pd.Series([]))

    def test_fit_none_data_error(self):
        """"tests error when data is none"""
        settings = CoarseClassSettings(0.3, 1, TargetType.BINARY, 20, True, 0.3, 42, None, None)
        cc_service = CoarseClassService(settings)
        with self.assertRaisesRegex(ValueError, "Data cannot be empty or None"):
            cc_service.fit(None, pd.Series([], dtype=int))

    def test_fit_invalid_target_type(self):
        """tests error when target_type_is invalid"""
        settings = CoarseClassSettings(0.3, 1, TargetType.MULTICLASS, 20, True, 0.3, 42, None, None)
        with self.assertRaisesRegex(ValueError, "Target type should be scalar or binary"):
            CoarseClassService(settings)

    def test_fit_invalid_target_data(self):
        """tests error when target_type_is invalid"""
        settings = CoarseClassSettings(0.3, 1, TargetType.BINARY, 20, True, 0.3, 42, None, None)
        cc_service = CoarseClassService(settings)
        fit_data = pd.DataFrame(
            {"b": ["r", "p", "i", None, None, "k"], "c": [3, 1, None, 2, 8, 9]})
        with self.assertRaisesRegex(ValueError, "Target type is binary but target contains more than two classes."):
            cc_service.fit(fit_data, pd.Series([1, 0, 1, 2, 2]))

    def test_fit_mismatching_indices(self):
        """tests error when index of data and target_data is different"""
        settings = CoarseClassSettings(0.3, 1, TargetType.BINARY, 20, True, 0.3, 42, None, None)
        cc_service = CoarseClassService(settings)
        fit_data = pd.DataFrame(
            {"b": ["r", "p", "i", None, None, "k"], "c": [3, 1, None, 2, 8, 9]}, index=[6, 2, 1, 3, 4, 5])
        with self.assertRaisesRegex(ValueError, "Data and target indexes are different."):
            cc_service.fit(fit_data, pd.Series([1, 0, 1, 1, 1]))

    # pylint: disable=no-self-use
    def test_transform(self):
        """tests transform function on binary target with stability check."""
        self.mock_get_type.side_effect = self._get_mock_native_type_side_effect_method(
            {"b": ColumnNativeType.String, "c": ColumnNativeType.Numeric}
        )
        fit_data = pd.DataFrame(
            {"b": ["r", "p", "i", None, None, "k"], "c": [3, 1, None, 2, 8, 9]})
        cc_out = CoarseClassFitOutput()
        cc_out.target_mean = 5.25
        cc_out.rejected_list = []
        cc_out.char_table = pd.DataFrame(
            {"char_value": ["i", "k", "NULL", "NULL"], "num_value": [3.0, 7.0, 5.5, 3.0],
             "variable": ["b", "b", "b", "c"]})
        cc_out.coarse_class_table = pd.DataFrame(
            {"class_min": [3, 5, 2, 3], "class_max": [np.inf, 6, np.inf, 3], "class_target_mean": [5, 5.5, 6, 3],
             "class_no": [1, "nulls", 1, "nulls"], "class": ["i,k", "null", "2.0-8.0,8.0-inf", "null"],
             "variable": ["b", "b", "c", "c"]})
        # dummy service
        cc_service = CoarseClassService(
            CoarseClassSettings(0.3, 1, TargetType.SCALAR, 20, True, 0.3, 42, None, None))
        cc_service.fit_output = cc_out
        transformed = cc_service.transform(fit_data, True)
        pd.testing.assert_frame_equal(transformed,
                                      pd.DataFrame({"Z_b": [0.0, 0.0, -0.25, 0.25, 0.25, -0.25],
                                                    "C_b": ["Other (Unseen)", "Other (Unseen)", "i,k", "null", "null",
                                                            "i,k"],
                                                    "Z_c": [0.75, 0.0, -2.25, 0.75, 0.75, 0.75],
                                                    "C_c": ["2.0-8.0,8.0-inf", "outlier_<2", "null", "2.0-8.0,8.0-inf",
                                                            "2.0-8.0,8.0-inf", "2.0-8.0,8.0-inf"]}))

    @patch(coarse_class_service.__name__ + ".get_train_test_split_from_strata")
    def test_stability_check_true(self, mock_split):
        """tests binary prediction with stability check"""
        mock_split.return_value = (pd.DataFrame({"b": ["i", None, None, "k"], "c": [None, 2, 8, 9]}),
                                   pd.Series(data=[0, 0, 0, 1]),
                                   pd.DataFrame({"b": ["r", "p"], "c": [3, 1]}),
                                   pd.Series(data=[0, 0]))
        self.mock_get_type.side_effect = self._get_mock_native_type_side_effect_method(
            {"b": ColumnNativeType.String, "c": ColumnNativeType.Numeric,
             "target": ColumnNativeType.Numeric}
        )
        fit_data = pd.DataFrame(
            {"b": ["r", "p", "i", None, None, "k"], "c": [3, 1, None, 2, 8, 9]})
        settings = CoarseClassSettings(0.3, 1, TargetType.BINARY, 20, True, 0.3, 42, None, None)
        cc_service = CoarseClassService(settings)
        cc_service.fit(fit_data, pd.Series(data=[0, 0, 0, 0, 0, 1]))
        self.assertEqual(0.25, cc_service.fit_output.target_mean)
        self.assertTrue(cc_service.fit_output.char_table.equals(pd.DataFrame(
            {"char_value": ["i", "k", "NULL", "NULL"], "num_value": [0.0, 1.0, 0.0, 0.0],
             "variable": ["b", "b", "b", "c"]}, index=[0, 1, 2, 3])))
        self.assertListEqual(cc_service.fit_output.rejected_list, [])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_min"].to_list(), [0.0, 0.0, 2.0, 0.0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_max"].to_list(),
                             [np.inf, 0, np.inf, 0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_target_mean"].to_list(),
                             [0.5, 0.0, 0.3333333333333333, 0.0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_count"].to_list(),
                             [2, 2, 3, 1])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_target_count"].to_list(),
                             [1, 0, 1, 0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_no"].to_list(),
                             ["1", "nulls", "1", "nulls"])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_percent"].to_list(),
                             [0.5, 0.5, 0.75, 0.25])
        out = cc_service.transform(fit_data, True)
        pd.testing.assert_frame_equal(out,
                                      pd.DataFrame({"Z_b": [0.0, 0.0, 0.25, -0.25, -0.25, 0.25],
                                                    "C_b": ["Other (Unseen)", "Other (Unseen)", "i,k", "null", "null",
                                                            "i,k"],
                                                    "Z_c": [0.083333, 0.0, -0.25, 0.083333, 0.083333, 0.083333],
                                                    "C_c": ["9.0-inf,2.0-9.0", "outlier_<2.0", "null",
                                                            "9.0-inf,2.0-9.0", "9.0-inf,2.0-9.0", "9.0-inf,2.0-9.0"]}))

    @patch(coarse_class_service.__name__ + ".get_train_test_split_from_strata")
    def test_stability_check_true_no_c_col(self, mock_split):
        """tests binary prediction with stability check without class value."""
        mock_split.return_value = (pd.DataFrame({"b": ["i", None, None, "k"], "c": [None, 2, 8, 9]}),
                                   pd.Series(data=[0, 0, 0, 1]),
                                   pd.DataFrame({"b": ["r", "p"], "c": [3, 1]}),
                                   pd.Series(data=[0, 0]))
        self.mock_get_type.side_effect = self._get_mock_native_type_side_effect_method(
            {"b": ColumnNativeType.String, "c": ColumnNativeType.Numeric, "target": ColumnNativeType.Numeric}
        )
        fit_data = pd.DataFrame(
            {"b": ["r", "p", "i", None, None, "k"], "c": [3, 1, None, 2, 8, 9]})
        settings = CoarseClassSettings(0.3, 1, TargetType.BINARY, 20, True, 0.3, 42, None, None)
        cc_service = CoarseClassService(settings)
        cc_service.fit(fit_data, pd.Series(data=[0, 0, 0, 0, 0, 1]))
        self.assertEqual(0.25, cc_service.fit_output.target_mean)
        self.assertTrue(cc_service.fit_output.char_table.equals(pd.DataFrame(
            {"char_value": ["i", "k", "NULL", "NULL"], "num_value": [0.0, 1.0, 0.0, 0.0],
             "variable": ["b", "b", "b", "c"]}, index=[0, 1, 2, 3])))
        self.assertListEqual(cc_service.fit_output.rejected_list, [])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_min"].to_list(), [0.0, 0.0, 2.0, 0.0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_max"].to_list(),
                             [np.inf, 0, np.inf, 0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_target_mean"].to_list(),
                             [0.5, 0.0, 0.3333333333333333, 0.0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_count"].to_list(),
                             [2, 2, 3, 1])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_target_count"].to_list(),
                             [1, 0, 1, 0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_no"].to_list(),
                             ["1", "nulls", "1", "nulls"])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_percent"].to_list(),
                             [0.5, 0.5, 0.75, 0.25])
        out = cc_service.transform(fit_data, False)
        pd.testing.assert_frame_equal(out,
                                      pd.DataFrame({"Z_b": [0.0, 0.0, 0.25, -0.25, -0.25, 0.25],
                                                    "Z_c": [0.0833333, 0.0, -0.25, 0.08333333, 0.0833333, 0.0833333]}))

    @patch(coarse_class_service.__name__ + ".convert_str_series_to_binary")
    @patch(coarse_class_service.__name__ + ".get_train_test_split_from_strata")
    def test_target_change(self, mock_split, mock_convert_target):
        """tests target data does not change."""
        mock_split.return_value = (pd.DataFrame({"b": ["i", None, None, "k"], "c": [None, 2, 8, 9]}),
                                   pd.Series(data=[0, 0, 0, 1]), pd.DataFrame({"b": ["r", "p"], "c": [3, 1]}),
                                   pd.Series(data=[0, 0]))
        self.mock_get_type.side_effect = self._get_mock_native_type_side_effect_method(
            {"b": ColumnNativeType.String, "c": ColumnNativeType.Numeric, "target": ColumnNativeType.String}
        )
        mock_convert_target.return_value = pd.Series(data=[0, 0, 0, 0, 0, 1])
        fit_data = pd.DataFrame(
            {"b": ["r", "p", "i", None, None, "k"], "c": [3, 1, None, 2, 8, 9]})
        target_data = pd.Series(data=["neg", "neg", "neg", "neg", "neg", "pos"])
        initial_target_data = target_data.copy()
        settings = CoarseClassSettings(0.3, 1, TargetType.BINARY, 20, True, 0.3, 42, "neg", "pos")
        cc_service = CoarseClassService(settings)
        cc_service.fit(fit_data, target_data)
        pd.testing.assert_series_equal(initial_target_data, target_data)
        self.assertEqual(0.25, cc_service.fit_output.target_mean)
        self.assertTrue(cc_service.fit_output.char_table.equals(pd.DataFrame(
            {"char_value": ["i", "k", "NULL", "NULL"], "num_value": [0.0, 1.0, 0.0, 0.0],
             "variable": ["b", "b", "b", "c"]}, index=[0, 1, 2, 3])))
        self.assertListEqual(cc_service.fit_output.rejected_list, [])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_min"].to_list(), [0.0, 0.0, 2.0, 0.0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_max"].to_list(),
                             [np.inf, 0, np.inf, 0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_target_mean"].to_list(),
                             [0.5, 0.0, 0.3333333333333333, 0.0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_count"].to_list(),
                             [2, 2, 3, 1])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_target_count"].to_list(),
                             [1, 0, 1, 0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_no"].to_list(),
                             ["1", "nulls", "1", "nulls"])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_percent"].to_list(),
                             [0.5, 0.5, 0.75, 0.25])
        out = cc_service.transform(fit_data, True)
        pd.testing.assert_frame_equal(out,
                                      pd.DataFrame({"Z_b": [0.0, 0.0, 0.25, -0.25, -0.25, 0.25],
                                                    "C_b": ["Other (Unseen)", "Other (Unseen)", "i,k", "null", "null",
                                                            "i,k"],
                                                    "Z_c": [0.0833333, 0.0, -0.25, 0.0833333, 0.0833333, 0.0833333],
                                                    "C_c": ["9.0-inf,2.0-9.0", "outlier_<2.0", "null",
                                                            "9.0-inf,2.0-9.0", "9.0-inf,2.0-9.0", "9.0-inf,2.0-9.0"]}))

    def test_pos_neg_not_given(self):
        """tests raise error when positive and negative classes are not given."""
        self.mock_get_type.side_effect = self._get_mock_native_type_side_effect_method(
            {"b": ColumnNativeType.String, "c": ColumnNativeType.Numeric, "target": ColumnNativeType.String}
        )
        fit_data = pd.DataFrame(
            {"b": ["r", "p", "i", None, None, "k"], "c": [3, 1, None, 2, 8, 9]})
        settings = CoarseClassSettings(0.3, 1, TargetType.BINARY, 20, True, 0.3, 42, None, None)
        with self.assertRaisesRegex(ValueError, "Target includes non-numeric values but positive and "
                                                "negative classes are not given."):
            CoarseClassService(settings).fit(fit_data, pd.Series(data=["neg", "neg", "neg", "neg", "neg", "pos"]))

    @patch(coarse_class_service.__name__ + ".get_sample_indices_from_series")
    @patch(coarse_class_service.__name__ + ".get_train_test_split_from_strata")
    def test_stability_check_false(self, mock_split, mock_get_indices):
        """tests binary prediction without stability check"""
        self.mock_get_type.side_effect = self._get_mock_native_type_side_effect_method(
            {"b": ColumnNativeType.String, "c": ColumnNativeType.Numeric, "target": ColumnNativeType.Numeric}
        )
        mock_split.return_value = (pd.DataFrame({"b": ["p", "p", "k", None, None, "k"], "c": [3, 1, None, 2, 8, 9]}),
                                   pd.Series(data=[1, 0, 1, 0, 0, 1]), pd.DataFrame({"b": ["p", "p"], "c": [3, 1]}),
                                   pd.Series(data=[1, 0]))
        mock_get_indices.return_value = pd.DataFrame(index=[1, 5]).index
        fit_data = pd.DataFrame(
            {"b": ["p", "p", "k", None, None, "k"], "c": [3, 1, None, 2, 8, 9]})
        settings = CoarseClassSettings(0.3, 1, TargetType.BINARY, 20, False, 0.3, 42, None, None)
        cc_service = CoarseClassService(settings)
        cc_service.fit(fit_data, pd.Series(data=[1, 0, 1, 0, 0, 1]))
        self.assertEqual(0.5, cc_service.fit_output.target_mean)
        self.assertTrue(cc_service.fit_output.char_table.equals(pd.DataFrame(
            {"char_value": ["k", "p", "NULL", "NULL"], "num_value": [1.0, 0.5, 0.0, 1.0],
             "variable": ["b", "b", "b", "c"]}, index=[0, 1, 2, 3])))
        self.assertListEqual(cc_service.fit_output.rejected_list, [])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_min"].to_list(),
                             [0.5, 1.0, 0.0, 1.0, 3.0, 8.0, 9.0, 1.0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_max"].to_list(),
                             [1.0, np.inf, 0.0, 3.0, 8.0, 9.0, np.inf, 1.0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_target_mean"].to_list(),
                             [0.5, 1.0, 0.0, 0.0, 1.0, 0.0, 1.0, 1.0])
        out = cc_service.transform(fit_data[["b", "c"]], True)
        pd.testing.assert_frame_equal(out,
                                      pd.DataFrame({"Z_b": [0.0, 0.0, 0.5, -0.5, -0.5, 0.5],
                                                    "C_b": ["p", "p", "k", "null", "null", "k"],
                                                    "Z_c": [0.5, -0.5, 0.5, -0.5, -0.5, 0.5],
                                                    "C_c": ["3.0-8.0", "1.0-3.0", "null", "1.0-3.0", "8.0-9.0",
                                                            "9.0-inf"]}))

    def test_with_valid_data(self):
        """tests multiclass prediction with stability check """
        self.mock_get_type.side_effect = self._get_mock_native_type_side_effect_method(
            {"b": ColumnNativeType.String, "c": ColumnNativeType.Numeric, "target": ColumnNativeType.Numeric}
        )
        fit_data = pd.DataFrame(
            {"b": ["i", "i", None, "k"], "c": [None, 2, 8, 9]})
        settings = CoarseClassSettings(0.3, 1, TargetType.SCALAR, 20, True, 0.3, 42, None, None)
        cc_service = CoarseClassService(settings)
        cc_service.fit(fit_data, pd.Series(data=[3, 6, 5, 7]),
                       stability_check_data=pd.DataFrame({"b": ["p", "p"], "c": [3, 1]}),
                       stability_check_target_data=pd.Series(data=[1, 2]))
        self.assertEqual(5.25, cc_service.fit_output.target_mean)
        self.assertTrue(cc_service.fit_output.char_table.equals(pd.DataFrame(
            {"char_value": ["i", "k", "NULL", "NULL"], "num_value": [4.5, 7.0, 5.0, 3.0],
             "variable": ["b", "b", "b", "c"]}, index=[0, 1, 2, 3])))
        self.assertListEqual(cc_service.fit_output.rejected_list, [])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_min"].to_list(),
                             [4.5, 7.0, 5.0, 2.0, 3.0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_max"].to_list(),
                             [7.0, np.inf, 5.0, np.inf, 3.0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_target_mean"].to_list(),
                             [4.5, 7.0, 5.0, 6.0, 3.0])
        out = cc_service.transform(fit_data[["b", "c"]], True)
        pd.testing.assert_frame_equal(out,
                                      pd.DataFrame({"Z_b": [-0.75, -0.75, -0.25, 1.75],
                                                    "C_b": ["i", "i", "null", "k"],
                                                    "Z_c": [-2.25, 0.75, 0.75, 0.75],
                                                    "C_c": ["null", "2.0-8.0,8.0-9.0,9.0-inf",
                                                            "2.0-8.0,8.0-9.0,9.0-inf", "2.0-8.0,8.0-9.0,9.0-inf"]}))

    @patch(coarse_class_service.__name__ + ".convert_str_series_to_binary")
    def test_with_valid_data_str_target(self, mock_convert_target):
        """tests multiclass prediction with stability check"""
        self.mock_get_type.side_effect = self._get_mock_native_type_side_effect_method(
            {"b": ColumnNativeType.String, "c": ColumnNativeType.Numeric, "target": ColumnNativeType.String}
        )
        mock_convert_target.side_effect = [pd.Series(data=[1, 0]), pd.Series(data=[1, 0, 0, 1])]
        fit_data = pd.DataFrame(
            {"b": ["i", "i", None, "k"], "c": [None, 2, 8, 9]})
        target_data = pd.Series(data=["pos", "neg", "neg", "pos"])
        initial_target_data = target_data.copy()
        stability_target_data = pd.Series(data=["pos", "neg"])
        initial_stability_data = stability_target_data.copy()
        settings = CoarseClassSettings(0.3, 1, TargetType.BINARY, 20, True, 0.3, 42, "pos", "neg")
        cc_service = CoarseClassService(settings)
        cc_service.fit(fit_data, pd.Series(data=["pos", "neg", "neg", "pos"]),
                       stability_check_data=pd.DataFrame({"b": ["p", "p"], "c": [3, 1]}),
                       stability_check_target_data=pd.Series(data=["pos", "neg"]))
        pd.testing.assert_series_equal(initial_target_data, target_data)
        pd.testing.assert_series_equal(initial_stability_data, stability_target_data)
        self.assertEqual(cc_service.fit_output.target_mean, 0.5)

    def test_with_valid_data_last_class_unstable(self):
        """tests multiclass prediction with stability check"""
        self.mock_get_type.side_effect = self._get_mock_native_type_side_effect_method(
            {"b": ColumnNativeType.String, "c": ColumnNativeType.Numeric, "target": ColumnNativeType.Numeric}
        )
        fit_data = pd.DataFrame(
            {"b": ["i", "i", None, "k"], "c": [None, 2, 8, 9]})
        settings = CoarseClassSettings(0.3, 1, TargetType.BINARY, 20, True, 0.3, 42, None, None)
        cc_service = CoarseClassService(settings)
        cc_service.fit(fit_data, pd.Series(data=[1, 0, 0, 1]),
                       stability_check_data=pd.DataFrame({"b": ["p", "p"], "c": [3, 1]}),
                       stability_check_target_data=pd.Series(data=[1, 0]))
        self.assertEqual(0.5, cc_service.fit_output.target_mean)
        self.assertTrue(cc_service.fit_output.char_table.equals(pd.DataFrame(
            {"char_value": ["i", "k", "NULL", "NULL"], "num_value": [0.5, 1, 0, 1],
             "variable": ["b", "b", "b", "c"]}, index=[0, 1, 2, 3])))
        self.assertListEqual(cc_service.fit_output.rejected_list, [])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_min"].to_list(),
                             [0.5, 0.0, 2.0, 1.0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_max"].to_list(),
                             [np.inf, 0.0, np.inf, 1.0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_target_mean"].to_list(),
                             [0.6666666666666666, 0, 0.3333333333333333, 1])

    def test_object_type_multi_target(self):
        """tests raise error if multi class object type target is given"""
        settings = CoarseClassSettings(0.3, 1, TargetType.SCALAR, 20, True, 0.3, 42, None, None)
        with self.assertRaisesRegex(ValueError, "Target column should be numeric for categorical coarse classing."):
            CoarseClassService(settings).fit(pd.DataFrame({"b": [1, 0, 1]}), pd.Series(data=["x", "y", "z"]))

    def test_with_valid_data_mid_class_unstable(self):
        """tests multiclass prediction with stability check"""
        self.mock_get_type.side_effect = self._get_mock_native_type_side_effect_method(
            {"b": ColumnNativeType.String, "c": ColumnNativeType.Numeric, "target": ColumnNativeType.Numeric}
        )
        fit_data = pd.DataFrame(
            {"b": ["i", "r", None, "k", "l", "i", "k", "k"],
             "c": [None, 2, 2, 2, 3, 3, 5, 5]})
        settings = CoarseClassSettings(0.3, 1, TargetType.BINARY, 20, True, 0.3, 42, None, None)
        cc_service = CoarseClassService(settings)
        cc_service.fit(fit_data, pd.Series(data=[1, 1, 1, 1, 1, 0, 1, 1]),
                       stability_check_data=pd.DataFrame({"b": ["k", "l"], "c": [3, 1]}),
                       stability_check_target_data=pd.Series(data=[0, 0]), )
        self.assertEqual(0.875, cc_service.fit_output.target_mean)
        self.assertTrue(cc_service.fit_output.char_table.equals(pd.DataFrame(
            {"char_value": ["i", "k", "l", "r", "NULL", "NULL"], "num_value": [0.5, 1, 1, 1, 1, 1],
             "variable": ["b", "b", "b", "b", "b", "c"]}, index=[0, 1, 2, 3, 4, 5])))
        self.assertListEqual(cc_service.fit_output.rejected_list, [])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_min"].to_list(),
                             [0.5, 1.0, 2.0, 1.0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_max"].to_list(),
                             [np.inf, 1.0, np.inf, 1.0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_target_mean"].to_list(),
                             [0.8571428571428571, 1.0, 0.8571428571428571, 1.0])

    def test_unstable_object(self):
        """tests multi prediction with stability check"""
        self.mock_get_type.side_effect = self._get_mock_native_type_side_effect_method(
            {"b": ColumnNativeType.String, "c": ColumnNativeType.Numeric, "target": ColumnNativeType.Numeric}
        )
        fit_data = pd.DataFrame(
            {"b": ["i", "r", None, "k", "l", "i", "k", "k"]})
        settings = CoarseClassSettings(0.3, 1, TargetType.SCALAR, 20, True, 0.3, 42, None, None)
        cc_service = CoarseClassService(settings)
        cc_service.fit(fit_data, pd.Series(data=[5, 0, 0, 0, 0, 5, 0, 0]),
                       pd.DataFrame({"b": ["k", "l"], "c": [3, 1]}), pd.Series(data=[0, 0]))
        self.assertEqual(1.25, cc_service.fit_output.target_mean)
        self.assertTrue(cc_service.fit_output.char_table.equals(pd.DataFrame(
            {"char_value": ["i", "k", "l", "r", "NULL"], "num_value": [5.0, 0.0, 0, 0, 0],
             "variable": ["b", "b", "b", "b", "b"]}, index=[0, 1, 2, 3, 4])))
        self.assertListEqual(cc_service.fit_output.rejected_list, [])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_min"].to_list(),
                             [0, 0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_max"].to_list(),
                             [np.inf, 0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_target_mean"].to_list(),
                             [1.4285714285714286, 0])

    @patch(coarse_class_service.__name__ + ".get_train_test_split_from_strata")
    def test_multi_target(self, mock_split):
        """tests multiclass prediction with stability check"""
        self.mock_get_type.side_effect = self._get_mock_native_type_side_effect_method(
            {"b": ColumnNativeType.String, "c": ColumnNativeType.Numeric, "target": ColumnNativeType.Numeric}
        )
        mock_split.return_value = (pd.DataFrame({"b": ["i", "i", None, "k"], "c": [None, 2, 8, 9]}),
                                   pd.Series(data=[3, 6, 5, 7]),
                                   pd.DataFrame({"b": ["p", "p"], "c": [3, 1]}),
                                   pd.Series(data=[1, 2]))
        fit_data = pd.DataFrame({"b": ["p", "p", "i", "i", None, "k"], "c": [3, 1, None, 2, 8, 9]})
        settings = CoarseClassSettings(0.3, 1, TargetType.SCALAR, 20, True, 0.3, 42, None, None)
        cc_service = CoarseClassService(settings)
        cc_service.fit(fit_data, pd.Series(data=[1, 2, 3, 6, 5, 7]))
        self.assertEqual(5.25, cc_service.fit_output.target_mean)
        self.assertTrue(cc_service.fit_output.char_table.equals(pd.DataFrame(
            {"char_value": ["i", "k", "NULL", "NULL"], "num_value": [4.5, 7.0, 5.0, 3.0],
             "variable": ["b", "b", "b", "c"]}, index=[0, 1, 2, 3])))
        self.assertListEqual(cc_service.fit_output.rejected_list, [])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_min"].to_list(),
                             [4.5, 7.0, 5.0, 2.0, 3.0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_max"].to_list(),
                             [7.0, np.inf, 5.0, np.inf, 3.0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_target_mean"].to_list(),
                             [4.5, 7.0, 5.0, 6.0, 3.0])
        out = cc_service.transform(fit_data, True)
        pd.testing.assert_frame_equal(out,
                                      pd.DataFrame({"Z_b": [0.0, 0.0, -0.75, -0.75, -0.25, 1.75],
                                                    "C_b": ["Other (Unseen)", "Other (Unseen)", "i", "i", "null", "k"],
                                                    "Z_c": [0.75, 0.0, -2.25, 0.75, 0.75, 0.75],
                                                    "C_c": ["2.0-8.0,8.0-9.0,9.0-inf", "outlier_<2.0", "null",
                                                            "2.0-8.0,8.0-9.0,9.0-inf",
                                                            "2.0-8.0,8.0-9.0,9.0-inf", "2.0-8.0,8.0-9.0,9.0-inf"]}))

    @patch(coarse_class_service.__name__ + ".get_sample_indices_from_series")
    @patch(coarse_class_service.__name__ + ".get_train_test_split_from_strata")
    def test_multi_without_stability(self, mock_split, mock_get_indices):
        """tests multiclass prediction without stability check"""
        self.mock_get_type.side_effect = self._get_mock_native_type_side_effect_method(
            {"b": ColumnNativeType.String, "c": ColumnNativeType.Numeric, "target": ColumnNativeType.Numeric}
        )
        mock_split.return_value = (pd.DataFrame({"b": ["p", "p", "i", "i", None, "k"], "c": [3, 1, None, 2, 8, 9]}),
                                   pd.Series(data=[1, 2, 3, 6, 5, 7]),
                                   pd.DataFrame({"b": ["p", "p"], "c": [3, 1]}),
                                   pd.Series(data=[1, 2]))
        mock_get_indices.return_value = pd.DataFrame().index
        fit_data = pd.DataFrame({"b": ["p", "p", "i", "i", None, "k"], "c": [3, 1, None, 2, 8, 9]})
        settings = CoarseClassSettings(0.3, 1, TargetType.SCALAR, 20, False, 0.3, 42, None, None)
        cc_service = CoarseClassService(settings)
        cc_service.fit(fit_data, pd.Series(data=[1, 2, 3, 6, 5, 7]))
        self.assertEqual(4, cc_service.fit_output.target_mean)
        self.assertTrue(cc_service.fit_output.char_table.equals(pd.DataFrame(
            {"char_value": ["i", "k", "p", "NULL", "NULL"], "num_value": [4.5, 7.0, 1.5, 5.0, 3.0],
             "variable": ["b", "b", "b", "b", "c"]}, index=[0, 1, 2, 3, 4])))
        self.assertListEqual(cc_service.fit_output.rejected_list, [])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_min"].to_list(),
                             [1.5, 4.5, 7.0, 5.0, 1.0, 2.0, 3.0, 8.0, 9.0, 3.0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_max"].to_list(),
                             [4.5, 7.0, np.inf, 5.0, 2.0, 3.0, 8.0, 9.0, np.inf, 3.0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_target_mean"].to_list(),
                             [1.5, 4.5, 7.0, 5.0, 2.0, 6.0, 1.0, 5.0, 7.0, 3.0])
        out = cc_service.transform(fit_data, True)
        pd.testing.assert_frame_equal(out,
                                      pd.DataFrame({"Z_b": [-2.5, -2.5, 0.5, 0.5, 1.0, 3.0],
                                                    "C_b": ["p", "p", "i", "i", "null", "k"],
                                                    "Z_c": [-3.0, -2.0, -1.0, 2.0, 1.0, 3.0],
                                                    "C_c": ["3.0-8.0", "1.0-2.0", "null", "2.0-3.0", "8.0-9.0",
                                                            "9.0-inf"]}))

    @patch(coarse_class_service.__name__ + ".get_train_test_split_from_strata")
    def test_cannot_cc(self, mock_split):
        """tests when a column cannot be coarse classes."""
        self.mock_get_type.side_effect = self._get_mock_native_type_side_effect_method(
            {"b": ColumnNativeType.String, "c": ColumnNativeType.Numeric, "target": ColumnNativeType.Numeric}
        )
        mock_split.return_value = (pd.DataFrame({"b": ["p", "p", None, "p"], "c": [None, 2, 8, 9]}),
                                   pd.Series(data=[3, 6, 5, 7]), pd.DataFrame({"b": ["p", "p"], "c": [3, 1]}),
                                   pd.Series(data=[1, 2]))
        fit_data = pd.DataFrame({"b": ["p", "p", "p", "p", None, "p"], "c": [3, 1, None, 2, 8, 9]})
        settings = CoarseClassSettings(0.3, 1, TargetType.SCALAR, 20, True, 0.3, 42, None, None)
        cc_service = CoarseClassService(settings)
        cc_service.fit(fit_data, pd.Series([1, 2, 3, 6, 5, 7]))
        self.assertEqual(5.25, cc_service.fit_output.target_mean)
        self.assertTrue(cc_service.fit_output.char_table.equals(pd.DataFrame(
            {"char_value": ["NULL"], "num_value": [3.0],
             "variable": ["c"]}, index=[0])))
        self.assertListEqual(cc_service.fit_output.rejected_list, ["b"])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_min"].to_list(),
                             [2.0, 3.0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_max"].to_list(),
                             [np.inf, 3.0])
        self.assertListEqual(cc_service.fit_output.coarse_class_table["class_target_mean"].to_list(),
                             [6.0, 3.0])
        out = cc_service.transform(fit_data, True)
        pd.testing.assert_frame_equal(out,
                                      pd.DataFrame({"Z_c": [0.75, 0.0, -2.25, 0.75, 0.75, 0.75],
                                                    "C_c": ["2.0-8.0,8.0-9.0,9.0-inf", "outlier_<2.0", "null",
                                                            "2.0-8.0,8.0-9.0,9.0-inf",
                                                            "2.0-8.0,8.0-9.0,9.0-inf", "2.0-8.0,8.0-9.0,9.0-inf"]}))

    def _get_mock_native_type_side_effect_method(self, col_types_dict: Dict[str, ColumnNativeType]):
        def _se(frame: pd.DataFrame, col: str):
            # pylint:disable=unused-argument
            return col_types_dict[col]

        return _se
