"""This module includes OneHotEncodingServiceTestCase"""
import unittest

import numpy as np
import pandas as pd

from organon.fl.core.enums.column_native_type import ColumnNativeType
from organon.ml.preprocessing.domain.services import one_hot_encoding_service
from organon.ml.preprocessing.domain.services.one_hot_encoding_service import OneHotEncodingService
from organon.ml.preprocessing.settings.objects.one_hot_encoding_settings import OneHotEncodingSettings
from organon.tests import test_helper


class OneHotEncodingTestCase(unittest.TestCase):
    """Tests OneHotEncodingService class."""

    def setUp(self) -> None:
        module_name = one_hot_encoding_service.__name__
        self.settings = OneHotEncodingSettings(0.6, 50)
        self.service = OneHotEncodingService(self.settings)
        self.mock_get_type = test_helper.get_mock(self, module_name + ".get_column_native_type")

    def test_with_mixed_typed_columns(self):
        """tests one hot when data contains numeric and categorical columns."""
        self.mock_get_type.side_effect = [ColumnNativeType.Numeric, ColumnNativeType.String, ColumnNativeType.Numeric]
        train_data = pd.DataFrame({"t": [1, 2, 3], "a": ["a", "b", "b"], "w": [3, 5, 10]})
        trans_data = pd.DataFrame({"k": [1, 1, 1], "a": ["c", "b", "c"], "t": [1, 2, 2]})
        self.service.fit(train_data)
        self.mock_get_type.side_effect = [ColumnNativeType.Numeric, ColumnNativeType.String, ColumnNativeType.Numeric]
        transformed_data = self.service.transform(trans_data)
        expected_df = pd.DataFrame({"k": [1, 1, 1], "a___b": [0, 1, 0], "a__OTHER": [1, 0, 1], "t": [1, 2, 2]})
        expected_df["a___b"] = expected_df["a___b"].astype(np.uint8)
        expected_df["a__OTHER"] = expected_df["a__OTHER"].astype(np.uint8)
        self.assertTrue(self.service.features_dict == {'a': ['b']})
        self.assertEqual({"a": ["a___b", "a__OTHER"]}, self.service.columns_dict)
        self.assertEqual(len(transformed_data.columns), 4)
        pd.testing.assert_frame_equal(expected_df, transformed_data[expected_df.columns])

    def test_onehot_ratio_given(self):
        """tests one hot encoding when threshold less than 1"""
        self.mock_get_type.side_effect = [ColumnNativeType.String]
        train_data = pd.DataFrame({"a": ["a", "b", "b", "c", "c"]})
        trans_data = pd.DataFrame({"a": ["c", "b", "b", "b", "c"]})
        original_trans_data = trans_data.copy()
        self.settings.threshold = 0.8
        self.settings.max_bin = 50
        self.service.fit(train_data)
        self.mock_get_type.side_effect = [ColumnNativeType.String]
        transformed_data = self.service.transform(trans_data)
        expected_df = pd.DataFrame({"a___b": [0, 1, 1, 1, 0], "a___c": [1, 0, 0, 0, 1], "a__OTHER": [0, 0, 0, 0, 0]}) \
            .astype(np.uint8)
        self.assertTrue(self.service.features_dict == {'a': ['b', 'c']})
        self.assertEqual({"a": ["a___b", "a___c", "a__OTHER"]}, self.service.columns_dict)
        self.assertTrue(transformed_data.sum(axis=1).isin([1]).all())
        self.assertEqual(len(transformed_data.columns), 3)
        self.assertTrue(transformed_data.equals(expected_df))
        self.assertTrue(trans_data.equals(original_trans_data))

    def test_onehot_ratio_given_inplace(self):
        """tests one hot encoding when threshold less than 1"""
        self.mock_get_type.side_effect = [ColumnNativeType.String]
        train_data = pd.DataFrame({"a": ["a", "b", "b", "c", "c"]})
        trans_data = pd.DataFrame({"a": ["c", "b", "b", "b", "c"]})
        original_trans_data = trans_data.copy()
        self.settings.threshold = 0.8
        self.settings.max_bin = 50
        self.service.fit(train_data)
        self.mock_get_type.side_effect = [ColumnNativeType.String]
        transformed_data = self.service.transform(trans_data, inplace=True)
        expected_df = pd.DataFrame({"a___b": [0, 1, 1, 1, 0], "a___c": [1, 0, 0, 0, 1], "a__OTHER": [0, 0, 0, 0, 0]}) \
            .astype(np.uint8)
        self.assertTrue(self.service.features_dict == {'a': ['b', 'c']})
        self.assertEqual({"a": ["a___b", "a___c", "a__OTHER"]}, self.service.columns_dict)
        self.assertTrue(transformed_data.sum(axis=1).isin([1]).all())
        self.assertEqual(len(transformed_data.columns), 3)
        self.assertTrue(transformed_data.equals(expected_df))
        self.assertFalse(trans_data.equals(original_trans_data))
        self.assertTrue(trans_data.equals(transformed_data))

    def test_onehot_none_feature_in_transform(self):
        """tests one hot encoding when threshold less than 1"""
        self.mock_get_type.side_effect = [ColumnNativeType.String]
        train_data = pd.DataFrame({"a": ["a", None, None, "a"]})
        trans_data = pd.DataFrame({"a": ["a", None, None, "c"]})
        self.settings.threshold = 0.8
        self.settings.max_bin = 50
        self.service.fit(train_data)
        self.mock_get_type.side_effect = [ColumnNativeType.String]
        transformed_data = self.service.transform(trans_data)
        expected_df = pd.DataFrame({"a___a": [1, 0, 0, 0], "a__NULL": [0, 1, 1, 0], "a__OTHER": [0, 0, 0, 1]}).astype(
            np.uint8)
        self.assertTrue(self.service.features_dict == {'a': ['a', None]})
        self.assertEqual({"a": ["a___a", "a__NULL", "a__OTHER"]}, self.service.columns_dict)
        self.assertTrue(transformed_data.sum(axis=1).isin([1]).all())
        self.assertEqual(len(transformed_data.columns), 3)
        self.assertTrue(transformed_data.equals(expected_df))

    def test_onehot_pdna_feature_in_transform(self):
        """tests one hot encoding when threshold less than 1"""
        self.mock_get_type.side_effect = [ColumnNativeType.String]
        train_data = pd.DataFrame({"a": ["a", pd.NA, pd.NA, "a"]})
        trans_data = pd.DataFrame({"a": ["a", pd.NA, pd.NA, "c"]})
        self.settings.threshold = 0.8
        self.settings.max_bin = 50
        self.service.fit(train_data)
        self.mock_get_type.side_effect = [ColumnNativeType.String]
        transformed_data = self.service.transform(trans_data)
        expected_df = pd.DataFrame({"a___a": [1, 0, 0, 0], "a__NULL": [0, 1, 1, 0], "a__OTHER": [0, 0, 0, 1]}).astype(
            np.uint8)
        self.assertEqual({"a": ["a___a", "a__NULL", "a__OTHER"]}, self.service.columns_dict)
        self.assertTrue(self.service.features_dict == {'a': ['a', pd.NA]})
        self.assertTrue(transformed_data.sum(axis=1).isin([1]).all())
        self.assertEqual(len(transformed_data.columns), 3)
        self.assertTrue(transformed_data.equals(expected_df))

    def test_onehot_bin_count_given_and_col_name_conflict(self):
        """test one hot when max_bin restricts feature number and ohe column name conflict with a column in df"""
        self.mock_get_type.side_effect = [ColumnNativeType.String, ColumnNativeType.Numeric]
        train_data = pd.DataFrame({"a": ["a", "a", "b", "c"]})
        trans_data = pd.DataFrame({"a": ["a", "b", "b"], "a___b": [1, 1, 0]})
        self.settings.threshold = 1.0
        self.settings.max_bin = 2
        self.service.fit(train_data)
        self.mock_get_type.side_effect = [ColumnNativeType.String, ColumnNativeType.Numeric]
        transformed_data = self.service.transform(trans_data)
        expected_df = pd.DataFrame({"a___a": [1, 0, 0], "a___b2": [0, 1, 1], "a__OTHER": [0, 0, 0]}).astype(
            np.uint8)
        expected_df["a___b"] = trans_data["a___b"]
        self.assertEqual( {"a": ["a", "b"]}, self.service.features_dict)
        self.assertEqual({"a": ["a___a", "a___b2", "a__OTHER"]}, self.service.columns_dict)
        self.assertEqual(4, len(transformed_data.columns))
        pd.testing.assert_frame_equal(expected_df, transformed_data[expected_df.columns])
        self.assertIsNone(self.service.get_ohe_col_name_for_nulls("a"))
        self.assertEqual("a__OTHER", self.service.get_ohe_col_name_for_others("a"))
        self.assertEqual("a___a", self.service.get_ohe_col_name_for_categorical_value("a", "a"))
        self.assertEqual("a___b2", self.service.get_ohe_col_name_for_categorical_value("a", "b"))

    def test_onehot_threshold_one(self):
        """tests one hot when threshold is 1"""
        self.mock_get_type.side_effect = [ColumnNativeType.String]
        train_data = pd.DataFrame({"a": ["a", "b", "b", "c"]})
        trans_data = pd.DataFrame({"a": ["a", "b", "b", "d"]})
        self.settings.threshold = 1.0
        self.settings.max_bin = 50
        self.service.fit(train_data)
        self.mock_get_type.side_effect = [ColumnNativeType.String]
        transformed_data = self.service.transform(trans_data)
        expected_df = pd.DataFrame(
            {"a___b": [0, 1, 1, 0], "a___a": [1, 0, 0, 0], "a___c": [0, 0, 0, 0], "a__OTHER": [0, 0, 0, 1]}).astype(
            np.uint8)
        self.assertTrue(self.service.features_dict == {'a': ['b', 'a', 'c']})
        self.assertEqual({"a": ["a___b", "a___a", "a___c", "a__OTHER"]}, self.service.columns_dict)
        self.assertTrue(transformed_data.sum(axis=1).isin([1]).all())
        self.assertEqual(len(transformed_data.columns), 4)
        self.assertTrue(transformed_data.equals(expected_df))
        self.assertIsNone(self.service.get_ohe_col_name_for_nulls("a"))
        self.assertEqual("a__OTHER", self.service.get_ohe_col_name_for_others("a"))
        self.assertEqual("a___a", self.service.get_ohe_col_name_for_categorical_value("a", "a"))
        self.assertEqual("a___b", self.service.get_ohe_col_name_for_categorical_value("a", "b"))
        self.assertEqual("a___c", self.service.get_ohe_col_name_for_categorical_value("a", "c"))

    def test_onehot_threshold_one_other_inside(self):
        """tests one hot when threshold is 1"""
        self.mock_get_type.side_effect = [ColumnNativeType.String]
        train_data = pd.DataFrame({"a": ["a", "b", "b", "OTHER"]})
        trans_data = pd.DataFrame({"a": ["a", "b", "b", "d"]})
        self.settings.threshold = 1.0
        self.settings.max_bin = 50
        self.service.fit(train_data)
        self.mock_get_type.side_effect = [ColumnNativeType.String]
        transformed_data = self.service.transform(trans_data)
        expected_df = pd.DataFrame(
            {"a___b": [0, 1, 1, 0], "a___a": [1, 0, 0, 0], "a___OTHER": [0, 0, 0, 0], "a__OTHER": [0, 0, 0, 1], }) \
            .astype(np.uint8)
        self.assertEqual({'a': ['b', 'a', 'OTHER']}, self.service.features_dict)
        self.assertEqual({"a": ["a___b", "a___a", "a___OTHER", "a__OTHER"]}, self.service.columns_dict)
        self.assertTrue(transformed_data.sum(axis=1).isin([1]).all())
        self.assertEqual(len(transformed_data.columns), 4)
        pd.testing.assert_frame_equal(expected_df, transformed_data)

    def test_fit_negative_threshold_error(self):
        """tests error if threshold is given negative"""
        self.settings.threshold = -1
        with self.assertRaisesRegex(ValueError, "Threshold cannot be negative."):
            self.service.fit(pd.DataFrame({"a": ["a", "b", "b", "Other"]}))

    def test_fit_negative_max_bin_error(self):
        """tests error if max_bin is given negative"""
        self.settings.max_bin = -1
        with self.assertRaisesRegex(ValueError, "Maximum number of features cannot be negative."):
            self.service.fit(pd.DataFrame({"a": ["a", "b", "b", "Other"]}))

    def test_fit_train_data_empty(self):
        """tests error if train_data is empty"""
        with self.assertRaisesRegex(ValueError, "Train data cannot be null or empty."):
            self.service.fit(pd.DataFrame({"a": []}))

    def test_transform_before_fit_error(self):
        """tests error when transform called before fit"""
        with self.assertRaisesRegex(ValueError, "Encoder not fitted yet"):
            self.service.transform(pd.DataFrame({"a": ["a", "b", "b", "d"]}))

    def test_transform_data_empty(self):
        """tests error if data is empty on transform"""
        with self.assertRaisesRegex(ValueError, "Transformation data cannot be null or empty."):
            self.service.fit(pd.DataFrame({"a": ["a", "b", "b", "Other"]})).transform(pd.DataFrame({"a": []}))

    def test_invalid_columns_on_transformation_error(self):
        """tests error when new categorical columns are observed in transformation data"""
        self.mock_get_type.side_effect = [ColumnNativeType.String, ColumnNativeType.Numeric]
        self.service.fit(pd.DataFrame({"a": ["a", "b", "b", "d"], "b": [1, 2, 3, 4]}))
        trans_data = pd.DataFrame({"a": ["b", "b", "a", "d"], "b": ["a", "b", "c", "d"], "c": ["a", "a", "b", "b"],
                                   "d": [1, 2, 3, 4]})
        self.mock_get_type.side_effect = [ColumnNativeType.String, ColumnNativeType.String, ColumnNativeType.String,
                                          ColumnNativeType.Numeric]
        with self.assertRaisesRegex(ValueError, "These columns are not found in the train data or"
                                                " their types were not categorical: b, c"):
            self.service.transform(trans_data)
