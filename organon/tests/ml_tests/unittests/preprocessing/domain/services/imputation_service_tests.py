"""This module includes test cases for ImputationService."""
import unittest
from typing import Dict
from unittest.mock import patch

import numpy as np
import pandas as pd
# this import must stay: https://github.com/scikit-learn/scikit-learn/issues/16833
# noinspection PyUnresolvedReferences
from sklearn.experimental import enable_iterative_imputer  # pylint: disable= unused-import
from sklearn.impute import SimpleImputer, IterativeImputer

from organon.fl.core.enums.column_native_type import ColumnNativeType
from organon.ml.common.helpers.parameter_helper import USE_CLASS_DEFAULT_STR
from organon.ml.preprocessing.domain.objects.imputation_fit_output import ImputationFitOutput
from organon.ml.preprocessing.domain.services import imputation_service
from organon.ml.preprocessing.domain.services.imputation_service import ImputationService
from organon.ml.preprocessing.settings.enums.imputer_type import ImputerType
from organon.ml.preprocessing.settings.objects.imputation_settings import ImputationSettings
from organon.tests import test_helper


class ImputationServiceTestCase(unittest.TestCase):
    """Test class for imputation service."""

    def setUp(self) -> None:
        module_name = imputation_service.__name__
        self.train_data = pd.DataFrame(
            {"a": [1, 2, 3, 4], "b": ["k", "m", "m", "m"], "c": [1, "k", "k", "p"], "d": ["p", "p", "r", "t"]})
        self.trans_data = pd.DataFrame(
            {"a": [1, 3, np.nan], "b": ["k", None, "i"], "c": [1, "j", "o"], "d": [None, "o", "r"]})
        self.check_trans_data = self.trans_data.copy(deep=True)
        self.mock_get_type = test_helper.get_mock(self, module_name + ".get_column_native_type")
        self.col_native_types = {
            "a": ColumnNativeType.Numeric, "b": ColumnNativeType.String, "c": ColumnNativeType.String,
            "d": ColumnNativeType.String
        }
        self.mock_get_type.side_effect = self._get_mock_native_type_side_effect_method(self.col_native_types)

    def test_fit_simple_imputer(self):
        """tests fit method on simple imputer"""
        settings = ImputationSettings(ImputerType.SIMPLE, ImputerType.SIMPLE, USE_CLASS_DEFAULT_STR,
                                      USE_CLASS_DEFAULT_STR, USE_CLASS_DEFAULT_STR,
                                      "most_frequent", USE_CLASS_DEFAULT_STR, USE_CLASS_DEFAULT_STR)
        imp_service = ImputationService(settings)
        imp_service.fit(self.train_data)
        self.assertIsNotNone(imp_service.fit_output)
        self.assertIsInstance(imp_service.fit_output, ImputationFitOutput)
        self.assertIsInstance(imp_service.fit_output.numerical_imputer["a"], SimpleImputer)
        self.assertIsInstance(imp_service.fit_output.categorical_imputer["b"], SimpleImputer)
        self.assertListEqual(imp_service.numerical_col_list, ["a"])
        self.assertListEqual(imp_service.categorical_col_list, ["b", "c", "d"])

    def test_fit_data_empty_error(self):
        """tests if error is raised when categorical method is given as iterative"""
        settings = ImputationSettings(ImputerType.SIMPLE, ImputerType.SIMPLE, USE_CLASS_DEFAULT_STR,
                                      None, USE_CLASS_DEFAULT_STR,
                                      "most_frequent", USE_CLASS_DEFAULT_STR, USE_CLASS_DEFAULT_STR)
        imp_service = ImputationService(settings)
        with self.assertRaisesRegex(ValueError, "Imputer cannot be fit. Data is not provided."):
            imp_service.fit(pd.DataFrame({"a": [], "b": []}))

    def test_fit_categorical_with_iterative_imputer_error(self):
        """tests if error is raised when categorical method is given as iterative"""
        settings = ImputationSettings(ImputerType.SIMPLE, ImputerType.ITERATIVE, USE_CLASS_DEFAULT_STR,
                                      None, USE_CLASS_DEFAULT_STR,
                                      "most_frequent", USE_CLASS_DEFAULT_STR, USE_CLASS_DEFAULT_STR)
        imp_service = ImputationService(settings)
        with self.assertRaisesRegex(ValueError, "IterativeImputer cannot be used for categorical columns"):
            imp_service.fit(self.train_data)

    def test_transform_simple_imputer(self):
        """tests tranform method on simple imputer"""
        settings = ImputationSettings(ImputerType.SIMPLE, ImputerType.SIMPLE, USE_CLASS_DEFAULT_STR,
                                      None, USE_CLASS_DEFAULT_STR,
                                      "most_frequent", USE_CLASS_DEFAULT_STR, USE_CLASS_DEFAULT_STR)
        imp_service = ImputationService(settings)
        with self.assertRaisesRegex(ValueError, ".*Imputer not fitted yet. Run fit method first.*"):
            imp_service.transform(self.trans_data)
        imp_service.fit(self.train_data)
        imputed_data = imp_service.transform(self.trans_data)
        expected_df = pd.DataFrame(
            {"a": [1, 3, 2.5], "b": ["k", "m", "i"], "c": [1, "j", "o"], "d": ["p", "o", "r"]})
        self.assertTrue(imputed_data.equals(expected_df))
        self.assertTrue(self.trans_data.equals(self.check_trans_data))

    def test_iterative_double_numeric_col(self):
        """tests tranform method on iterative imputer"""
        self.mock_get_type.side_effect = self._get_mock_native_type_side_effect_method(
            {"a": ColumnNativeType.Numeric, "b": ColumnNativeType.Numeric})
        train_data = pd.DataFrame({"a": [1, 2, 3, 4, 5, 6, 7], "b": [41, 22, 73, 344, 85, 64, 7]})
        trans_data = pd.DataFrame({"a": [1, None, 2, 3], "b": [1, None, 2, 5]})
        settings = ImputationSettings(ImputerType.ITERATIVE, ImputerType.SIMPLE, USE_CLASS_DEFAULT_STR,
                                      None, USE_CLASS_DEFAULT_STR,
                                      "median", USE_CLASS_DEFAULT_STR, USE_CLASS_DEFAULT_STR)
        imp_service = ImputationService(settings)
        with self.assertRaisesRegex(ValueError, ".*Imputer not fitted yet. Run fit method first.*"):
            imp_service.transform(trans_data.copy())
        imp_service.fit(train_data)
        imputed_data = imp_service.transform(trans_data.copy())
        expected_df = pd.DataFrame(
            {"a": [1.0, 4, 2, 3], "b": [1, 90.857143, 2, 5]})
        pd.testing.assert_frame_equal(imputed_data, expected_df)

    def test_fit_iterative_imputer(self):
        """tests fit method on iterative imputer"""
        settings = ImputationSettings(ImputerType.ITERATIVE, ImputerType.SIMPLE, USE_CLASS_DEFAULT_STR,
                                      USE_CLASS_DEFAULT_STR, USE_CLASS_DEFAULT_STR,
                                      "most_frequent", USE_CLASS_DEFAULT_STR, USE_CLASS_DEFAULT_STR)
        imp_service = ImputationService(settings)
        imp_service.fit(self.train_data)
        self.assertIsNotNone(imp_service.fit_output)
        self.assertIsInstance(imp_service.fit_output, ImputationFitOutput)
        self.assertIsInstance(imp_service.fit_output.numerical_imputer, IterativeImputer)
        self.assertIsInstance(imp_service.fit_output.categorical_imputer["b"], SimpleImputer)
        self.assertListEqual(imp_service.numerical_col_list, ["a"])
        self.assertListEqual(imp_service.categorical_col_list, ["b", "c", "d"])

    def test_transform_iterative_imputer(self):
        """tests transform method on iterative imputer."""
        settings = ImputationSettings(ImputerType.ITERATIVE, ImputerType.SIMPLE, USE_CLASS_DEFAULT_STR,
                                      None, USE_CLASS_DEFAULT_STR,
                                      "most_frequent", USE_CLASS_DEFAULT_STR, USE_CLASS_DEFAULT_STR)
        imp_service = ImputationService(settings)
        with self.assertRaisesRegex(ValueError, ".*Imputer not fitted yet. Run fit method first.*"):
            imp_service.transform(self.trans_data)
        imp_service.fit(self.train_data)
        imputed_data = imp_service.transform(self.trans_data)
        self.assertListEqual(imp_service.categorical_col_list, ["b", "c", "d"])
        self.assertListEqual(imp_service.numerical_col_list, ["a"])
        self.assertIsInstance(imputed_data, pd.DataFrame)
        expected_df = pd.DataFrame(
            {"a": [1, 3, 2.5], "b": ["k", "m", "i"], "c": [1, "j", "o"], "d": ["p", "o", "r"]})
        self.assertTrue(imputed_data.equals(expected_df))
        self.assertTrue(self.trans_data.equals(self.check_trans_data))

    def test_transform_iterative_imputer_inplace(self):
        """tests transform method on iterative imputer"""
        settings = ImputationSettings(ImputerType.ITERATIVE, ImputerType.SIMPLE, USE_CLASS_DEFAULT_STR,
                                      None, USE_CLASS_DEFAULT_STR,
                                      "most_frequent", USE_CLASS_DEFAULT_STR, USE_CLASS_DEFAULT_STR)
        imp_service = ImputationService(settings)
        with self.assertRaisesRegex(ValueError, ".*Imputer not fitted yet. Run fit method first.*"):
            imp_service.transform(self.trans_data)
        imp_service.fit(self.train_data)
        imputed_data = imp_service.transform(self.trans_data, inplace=True)
        self.assertListEqual(imp_service.categorical_col_list, ["b", "c", "d"])
        self.assertListEqual(imp_service.numerical_col_list, ["a"])
        self.assertIsInstance(imputed_data, pd.DataFrame)
        expected_df = pd.DataFrame(
            {"a": [1, 3, 2.5], "b": ["k", "m", "i"], "c": [1, "j", "o"], "d": ["p", "o", "r"]})
        self.assertTrue(imputed_data.equals(expected_df))
        self.assertFalse(self.trans_data.equals(self.check_trans_data))
        self.assertTrue(self.trans_data.equals(imputed_data))

    def test_imputer_columns_different(self):
        """tests raise error when transformation data columns differ with train data columns"""
        settings = ImputationSettings(ImputerType.SIMPLE, ImputerType.SIMPLE, USE_CLASS_DEFAULT_STR,
                                      USE_CLASS_DEFAULT_STR, USE_CLASS_DEFAULT_STR,
                                      "most_frequent", USE_CLASS_DEFAULT_STR, USE_CLASS_DEFAULT_STR)
        imp_service = ImputationService(settings)
        imp_service.fit(self.train_data)
        with self.assertRaisesRegex(ValueError, "Imputer was not fitted for following columns: t.*"):
            imp_service.transform(pd.DataFrame(
                {"a": [1, 3, np.nan], "b": ["k", np.nan, "i"], "t": [1, "j", "o"], "d": [np.nan, "o", "r"]}))

    def test_imputer_column_types_different(self):
        """tests raise error when train and transformation data types do not match with each other."""
        settings = ImputationSettings(ImputerType.SIMPLE, ImputerType.SIMPLE, USE_CLASS_DEFAULT_STR,
                                      USE_CLASS_DEFAULT_STR, USE_CLASS_DEFAULT_STR,
                                      "most_frequent", USE_CLASS_DEFAULT_STR, USE_CLASS_DEFAULT_STR)
        imp_service = ImputationService(settings)
        imp_service.fit(self.train_data)
        self.mock_get_type.side_effect = self._get_mock_native_type_side_effect_method(
            {"a": ColumnNativeType.Numeric, "b": ColumnNativeType.String, "c": ColumnNativeType.Numeric,
             "d": ColumnNativeType.String}
        )
        with self.assertRaisesRegex(ValueError, r"Following columns are expected to be categorical: c"):
            imp_service.transform(pd.DataFrame(
                {"a": [1, 3, np.nan], "b": ["k", np.nan, "i"], "c": [1, 2, 3], "d": [np.nan, "o", "r"]}))

    def test_imputer_column_types_different_numerical(self):
        """tests raise error when train and transformation data types do not match with each other."""
        settings = ImputationSettings(ImputerType.SIMPLE, ImputerType.SIMPLE, USE_CLASS_DEFAULT_STR,
                                      USE_CLASS_DEFAULT_STR, USE_CLASS_DEFAULT_STR,
                                      "most_frequent", USE_CLASS_DEFAULT_STR, USE_CLASS_DEFAULT_STR)
        imp_service = ImputationService(settings)
        imp_service.fit(self.train_data)
        self.mock_get_type.side_effect = self._get_mock_native_type_side_effect_method(
            {"a": ColumnNativeType.String, "b": ColumnNativeType.String, "c": ColumnNativeType.String,
             "d": ColumnNativeType.String}
        )
        with self.assertRaisesRegex(ValueError, r"Following columns are expected to be numerical: a"):
            imp_service.transform(pd.DataFrame(
                {"a": ["a", "b", None], "b": ["k", np.nan, "i"], "c": [1, 2, 3], "d": [np.nan, "o", "r"]}))

    def test_transform_simple_imputer_with_included_column(self):
        """tests transform method on simple imputer"""
        settings = ImputationSettings(ImputerType.SIMPLE, ImputerType.SIMPLE, USE_CLASS_DEFAULT_STR,
                                      None, USE_CLASS_DEFAULT_STR,
                                      "most_frequent", USE_CLASS_DEFAULT_STR, USE_CLASS_DEFAULT_STR,
                                      included_columns=["a"])
        imp_service = ImputationService(settings)
        imp_service.fit(self.train_data)
        imputed_data = imp_service.transform(self.trans_data, ignore_extra_columns=True)
        expected_df = pd.DataFrame(
            {"a": [1, 3, 2.5], "b": ["k", None, "i"], "c": [1, "j", "o"], "d": [None, "o", "r"]})
        self.assertTrue(imputed_data.equals(expected_df))

    @patch(imputation_service.__name__ + ".LogHelper")
    def test_transform_simple_imputer_with_all_nan_column(self, mock_log_helper):
        """tests tranform method with simple imputer on numerical column with all nan values"""
        self.mock_get_type.side_effect = self._get_mock_native_type_side_effect_method(
            {"a": ColumnNativeType.Numeric, "b": ColumnNativeType.String, "c": ColumnNativeType.String,
             "d": ColumnNativeType.String, "all_nan": ColumnNativeType.Numeric})
        data = self.train_data.copy()
        trans_data = self.trans_data.copy()
        data["all_nan"] = float("nan")
        trans_data["all_nan"] = float("nan")
        settings = ImputationSettings(ImputerType.SIMPLE, ImputerType.SIMPLE, USE_CLASS_DEFAULT_STR,
                                      None, USE_CLASS_DEFAULT_STR,
                                      "most_frequent", USE_CLASS_DEFAULT_STR, USE_CLASS_DEFAULT_STR,
                                      included_columns=["all_nan"])
        imp_service = ImputationService(settings)
        imp_service.fit(data)
        imputed_data = imp_service.transform(trans_data, ignore_extra_columns=True)
        expected_df = pd.DataFrame(
            {"a": [1, 3, float("nan")], "b": ["k", None, "i"], "c": [1, "j", "o"], "d": [None, "o", "r"],
             "all_nan": [float("nan")] * 3})
        pd.testing.assert_frame_equal(expected_df, imputed_data)
        mock_log_helper.warning.assert_called_with(
            "Column all_nan could not be transformed by imputer since all values are NaN.")

    def test_transform_missing_numerical_columns_when_iterative_imputer_used(self):
        """missing numerical columns should raise Error on iterative imputer if they were used during fit"""
        settings = ImputationSettings(ImputerType.ITERATIVE, ImputerType.SIMPLE, USE_CLASS_DEFAULT_STR,
                                      None, USE_CLASS_DEFAULT_STR,
                                      "most_frequent", USE_CLASS_DEFAULT_STR, USE_CLASS_DEFAULT_STR)
        imp_service = ImputationService(settings)
        data = self.train_data.copy()
        data["second_num_col"] = [2.2, 1.4, 2.4, 3.2]
        col_native_types = self.col_native_types.copy()
        col_native_types["second_num_col"] = ColumnNativeType.Numeric
        self.mock_get_type.side_effect = self._get_mock_native_type_side_effect_method(col_native_types)
        imp_service.fit(data)
        with self.assertRaisesRegex(
                ValueError, "Following numerical columns should be supplied to transform data: second_num_col"):
            imp_service.transform(self.trans_data, ignore_extra_columns=True)

    def test_transform_missing_numerical_columns_with_included_columns_on_iterative_imputer(self):
        """missing numerical columns should not raise Error on iterative imputer if they were not included during fit"""
        settings = ImputationSettings(ImputerType.ITERATIVE, ImputerType.SIMPLE, USE_CLASS_DEFAULT_STR,
                                      None, USE_CLASS_DEFAULT_STR,
                                      "most_frequent", USE_CLASS_DEFAULT_STR, USE_CLASS_DEFAULT_STR,
                                      included_columns=["a"])
        imp_service = ImputationService(settings)
        data = self.train_data.copy()
        data["second_num_col"] = [2.2, 1.4, 2.4, 3.2]
        col_native_types = self.col_native_types.copy()
        col_native_types["second_num_col"] = ColumnNativeType.Numeric
        self.mock_get_type.side_effect = self._get_mock_native_type_side_effect_method(col_native_types)
        imp_service.fit(data)
        imputed_data = imp_service.transform(self.trans_data, ignore_extra_columns=True)
        expected_df = pd.DataFrame(
            {"a": [1, 3, 2.5], "b": ["k", None, "i"], "c": [1, "j", "o"], "d": [None, "o", "r"]})
        pd.testing.assert_frame_equal(expected_df, imputed_data)

    @staticmethod
    def _get_mock_native_type_side_effect_method(col_types_dict: Dict[str, ColumnNativeType]):
        def _se(frame: pd.DataFrame, col: str):
            # pylint:disable=unused-argument
            return col_types_dict[col]

        return _se

    @patch(imputation_service.__name__ + "." + SimpleImputer.__name__)
    def test_transform_convert_dtype_back(self, mock_simple_imputer):
        """tests transform method when dtype of a column is converted back to original after imputer transform."""
        settings = ImputationSettings(ImputerType.SIMPLE, ImputerType.SIMPLE, USE_CLASS_DEFAULT_STR,
                                      None, USE_CLASS_DEFAULT_STR, "most_frequent", USE_CLASS_DEFAULT_STR,
                                      USE_CLASS_DEFAULT_STR)
        imp_service = ImputationService(settings)
        imp_service.fit(self.train_data)
        self.trans_data["a"] = pd.Series([1, 2, 3], dtype=pd.Int64Dtype())

        def _mock_transform_se(frame: pd.DataFrame):  # pylint: disable=inconsistent-return-statements
            col = frame.columns[0]
            if col == "a":
                return np.array([[1], [2], [3]], dtype=np.dtype("O"))
            if col == "b":
                return np.array([["k"], ["m"], ["i"]])
            if col == "c":
                return np.array([[1], ["j"], ["o"]])
            if col == "d":
                return np.array([["p"], ["o"], ["r"]])
            self.fail("Transform called with unexpected column")

        mock_simple_imputer.return_value.transform.side_effect = _mock_transform_se
        imputed_data = imp_service.transform(self.trans_data)
        self.assertListEqual(imp_service.categorical_col_list, ["b", "c", "d"])
        self.assertListEqual(imp_service.numerical_col_list, ["a"])
        self.assertIsInstance(imputed_data, pd.DataFrame)
        expected_df = pd.DataFrame(
            {"a": [1, 2, 3], "b": ["k", "m", "i"], "c": ["1", "j", "o"], "d": ["p", "o", "r"]})
        pd.testing.assert_frame_equal(expected_df, imputed_data, check_dtype=False)
        self.assertEqual(pd.Int64Dtype(), imputed_data["a"].dtype)

    @patch(imputation_service.__name__ + "." + SimpleImputer.__name__)
    def test_transform_convert_dtype_new(self, mock_simple_imputer):
        """tests transform method when dtype of a column is converted to appropriate numeric dtype
         after imputer transform."""
        settings = ImputationSettings(ImputerType.SIMPLE, ImputerType.SIMPLE, USE_CLASS_DEFAULT_STR,
                                      None, USE_CLASS_DEFAULT_STR,
                                      "most_frequent", USE_CLASS_DEFAULT_STR, USE_CLASS_DEFAULT_STR)
        imp_service = ImputationService(settings)
        imp_service.fit(self.train_data)
        self.trans_data["a"] = pd.Series([1, 2, 3], dtype=pd.Int64Dtype())

        def _mock_transform_se(frame: pd.DataFrame):  # pylint: disable=inconsistent-return-statements
            col = frame.columns[0]
            if col == "a":
                return np.array([[1.2], [2.4], [3.2]], dtype=np.dtype("O"))
            if col == "b":
                return np.array([["k"], ["m"], ["i"]])
            if col == "c":
                return np.array([[1], ["j"], ["o"]])
            if col == "d":
                return np.array([["p"], ["o"], ["r"]])
            self.fail("Transform called with unexpected column")

        mock_simple_imputer.return_value.transform.side_effect = _mock_transform_se
        imputed_data = imp_service.transform(self.trans_data)
        self.assertListEqual(imp_service.categorical_col_list, ["b", "c", "d"])
        self.assertListEqual(imp_service.numerical_col_list, ["a"])
        self.assertIsInstance(imputed_data, pd.DataFrame)
        expected_df = pd.DataFrame(
            {"a": [1.2, 2.4, 3.2], "b": ["k", "m", "i"], "c": ["1", "j", "o"], "d": ["p", "o", "r"]})
        pd.testing.assert_frame_equal(expected_df, imputed_data, check_dtype=False)
        self.assertEqual(pd.Float64Dtype(), imputed_data["a"].dtype)
