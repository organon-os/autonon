"""This module includes ScalingService test cases"""
import unittest

import numpy as np
import pandas as pd

from organon.fl.core.enums.column_native_type import ColumnNativeType
from organon.ml.preprocessing.domain.services import scaling_service
from organon.ml.preprocessing.domain.services.scaling_service import ScalingService
from organon.ml.preprocessing.settings.enums.scaler_type import ScalerType
from organon.ml.preprocessing.settings.objects.scaling_settings import ScalingSettings
from organon.tests import test_helper


class ScalingServiceTestCase(unittest.TestCase):
    """test class for scaling service"""

    def setUp(self) -> None:
        self.train_data = pd.DataFrame({"a": [1, 2, 3], "b": [4, 5, 6], "c": ["o", "l", "k"], "d": [3, 5, 10]})
        self.trans_data = pd.DataFrame({"a": [5], "b": [12], "c": ["o"]})
        self.settings = ScalingSettings(ScalerType.MINMAX)
        self.mock_get_col_native_type = test_helper.get_mock(self, scaling_service.__name__ + ".get_column_native_type")

    def test_standard_scaling(self):
        """tests StandardScaler"""
        self.mock_get_col_native_type.side_effect = \
            lambda df, col: ColumnNativeType.Numeric if col in ["a", "b"] else ColumnNativeType.String
        original_trans_data = self.trans_data.copy()
        expected_df = pd.DataFrame(
            {"a": (self.trans_data["a"][0] - self.train_data["a"].mean()) / np.std(self.train_data["a"]),
             "b": (self.trans_data["b"][0] - self.train_data["b"].mean()) / np.std(self.train_data["b"]), "c": ["o"]})
        self.settings = ScalingSettings(ScalerType.STANDARD)
        scaled_df = ScalingService(self.settings).fit(self.train_data).transform(self.trans_data)
        self.assertTrue(original_trans_data.equals(self.trans_data))
        self.assertTrue(scaled_df.equals(expected_df))

    def test_minmax_scaling(self):
        """tests MinMaxScaler"""
        self.mock_get_col_native_type.side_effect = \
            lambda df, col: ColumnNativeType.Numeric if col in ["a", "b"] else ColumnNativeType.String
        original_trans_data = self.trans_data.copy()
        scaled_df = ScalingService(self.settings).fit(self.train_data).transform(self.trans_data)
        expected_df = pd.DataFrame(
            {"a": (self.trans_data["a"][0] - self.train_data["a"].min()) / (
                    self.train_data["a"].max() - self.train_data["a"].min()),
             "b": (self.trans_data["b"][0] - self.train_data["b"].min()) / (
                     self.train_data["b"].max() - self.train_data["b"].min()), "c": ["o"]})
        self.assertTrue(scaled_df.equals(expected_df))
        self.assertTrue(original_trans_data.equals(self.trans_data))

    def test_standard_scaling_inplace_true(self):
        """tests StandardScaler"""
        self.mock_get_col_native_type.side_effect = \
            lambda df, col: ColumnNativeType.Numeric if col in ["a", "b"] else ColumnNativeType.String
        original_trans_data = self.trans_data.copy()
        expected_df = pd.DataFrame(
            {"a": (self.trans_data["a"][0] - self.train_data["a"].mean()) / np.std(self.train_data["a"]),
             "b": (self.trans_data["b"][0] - self.train_data["b"].mean()) / np.std(self.train_data["b"]), "c": ["o"]})
        self.settings = ScalingSettings(ScalerType.STANDARD)
        scaled_df = ScalingService(self.settings).fit(self.train_data).transform(self.trans_data, inplace=True)
        self.assertFalse(original_trans_data.equals(self.trans_data))
        self.assertTrue(scaled_df.equals(expected_df))

    def test_minmax_scaling_inplace_true(self):
        """tests MinMaxScaler"""
        self.mock_get_col_native_type.side_effect = \
            lambda df, col: ColumnNativeType.Numeric if col in ["a", "b"] else ColumnNativeType.String
        original_trans_data = self.trans_data.copy()
        scaled_df = ScalingService(self.settings).fit(self.train_data).transform(self.trans_data, inplace=True)
        expected_df = pd.DataFrame(
            {"a": (original_trans_data["a"][0] - self.train_data["a"].min()) / (
                    self.train_data["a"].max() - self.train_data["a"].min()),
             "b": (original_trans_data["b"][0] - self.train_data["b"].min()) / (
                     self.train_data["b"].max() - self.train_data["b"].min()), "c": ["o"]})
        self.assertTrue(scaled_df.equals(expected_df))
        self.assertFalse(original_trans_data.equals(self.trans_data))
        self.assertTrue(self.trans_data.equals(scaled_df))

    def test_transform_before_fit_error(self):
        """tests error when transform called before fit"""
        with self.assertRaisesRegex(ValueError, "Scaler not fitted yet"):
            ScalingService(self.settings).transform(self.trans_data)

    def test_invalid_columns_error(self):
        """tests error when new columns are observed in transformation"""
        trans_data = pd.DataFrame({"a": [5], "b": [12], "c": ["o"], "g": [1]})
        self.mock_get_col_native_type.side_effect = \
            lambda df, col: ColumnNativeType.Numeric if col in ["b"] else ColumnNativeType.String
        service = ScalingService(self.settings).fit(self.train_data)
        self.mock_get_col_native_type.side_effect = \
            lambda df, col: ColumnNativeType.Numeric if col in ["a", "b", "g"] else ColumnNativeType.String
        with self.assertRaisesRegex(ValueError, "These numerical columns are not found in the train data or "
                                                "their types were not numerical: a, g"):
            service.transform(trans_data)
