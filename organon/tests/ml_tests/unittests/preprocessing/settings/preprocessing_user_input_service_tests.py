"""This module includes test cases for PreprocessingUserInputService"""
import unittest
from unittest.mock import patch

import pandas as pd

from organon.ml.common.enums.target_type import TargetType
from organon.ml.preprocessing.services.user_settings.coarse_input_dto import CoarseInputDto
from organon.ml.preprocessing.settings import preprocessing_user_input_service
from organon.ml.preprocessing.settings.enums.imputer_type import ImputerType
from organon.ml.preprocessing.settings.enums.scaler_type import ScalerType
from organon.ml.preprocessing.settings.objects.coarse_class_settings import CoarseClassSettings
from organon.ml.preprocessing.settings.objects.scaling_settings import ScalingSettings
from organon.ml.preprocessing.settings.preprocessing_user_input_service import PreprocessingUserInputService


def get_cc_setting_sample():
    """returns a cc setting instance"""
    cc_setting = CoarseClassSettings(0.3, 1, TargetType.BINARY, 20, True, 0.3, 42, None, None)
    return cc_setting


class PreprocessingUserInputServiceTestCase(unittest.TestCase):
    """tests PreprocessingUserInputService class."""

    def setUp(self) -> None:
        self.train_data = pd.DataFrame({"a": [1, 2, 3], "b": [4, 5, 6], "c": ["o", "l", "k"]})
        self.trans_data = pd.DataFrame({"a": [5], "b": [12], "c": ["o"]})
        self.scaling_settings = ScalingSettings(ScalerType.MINMAX)

    @patch(preprocessing_user_input_service.__name__ + ".get_enum")
    def test_imputer_none_numeric_imputer(self, mock_get_enum):
        """tests raise error when numeric imputer is None"""
        mock_get_enum.side_effect = [None, ImputerType.SIMPLE]
        with self.assertRaisesRegex(ValueError, "Numeric imputer type is None."):
            PreprocessingUserInputService().get_imputation_settings(None)

    @patch(preprocessing_user_input_service.__name__ + ".get_enum")
    def test_wrong_categorical_imputer_type(self, mock_get_enum):
        """tests raise error when categorical imputer is iterative"""
        mock_get_enum.side_effect = [ImputerType.SIMPLE, ImputerType.ITERATIVE]
        with self.assertRaisesRegex(ValueError, "The given categorical data imputer cannot be used. "
                                                "Please select SIMPLE as imputer."):
            PreprocessingUserInputService().get_imputation_settings("SIMPLE", "ITERATIVE")

    @patch(preprocessing_user_input_service.__name__ + ".get_enum")
    def test_wrong_categorical_strategy(self, mock_get_enum):
        """tests raise error when categorical strategy is not most_frequent or constant"""
        mock_get_enum.side_effect = [ImputerType.SIMPLE, ImputerType.SIMPLE]
        with self.assertRaisesRegex(ValueError, "The given categorical column imputation strategy cannot be used. "
                                                "You can select most_frequent or constant strategies."):
            PreprocessingUserInputService().get_imputation_settings("SIMPLE", "SIMPLE",
                                                                    c_strategy="mean")

    @patch(preprocessing_user_input_service.__name__ + ".get_enum")
    def test_get_scaling_settings(self, mock_get_enum):
        """tests get_scaling_settings"""
        mock_get_enum.return_value = ScalerType.MINMAX
        scaler_settings = PreprocessingUserInputService().get_scaling_settings("MINMAX")
        self.assertIsInstance(scaler_settings, ScalingSettings)

        self.assertEqual(scaler_settings.strategy, ScalerType.MINMAX)

    @patch(preprocessing_user_input_service.__name__ + ".get_enum")
    def test_get_cc_settings(self, mock_enum):
        """tests get_coarse_class_settings when parameters given properly"""
        mock_enum.return_value = TargetType.BINARY
        cc_input = CoarseInputDto(0.3, TargetType.BINARY.name, 1, 20, True, 0.3, 42, None, None)
        cc_settings = PreprocessingUserInputService.get_coarse_class_settings(cc_input)
        self.assertEqual(cc_settings.target_type, get_cc_setting_sample().target_type)
        self.assertEqual(cc_settings.min_class_size, get_cc_setting_sample().min_class_size)
        self.assertEqual(cc_settings.max_leaf_nodes, get_cc_setting_sample().max_leaf_nodes)
        self.assertEqual(cc_settings.stability_threshold, get_cc_setting_sample().stability_threshold)
        self.assertEqual(cc_settings.stability_check, get_cc_setting_sample().stability_check)
