"""This module includes tests for preprocessing_input_helper"""
import unittest

import numpy as np
import pandas as pd

from organon.ml.preprocessing.helpers.preprocessing_input_helper import convert_str_series_to_binary, \
    convert_binary_series_to_str


class PreprocessingInputHelperTestCase(unittest.TestCase):
    """tests preprocessing_input_helper"""

    def test_convert_binary_series_to_str(self):
        """tests string conversion on binary series data"""
        result_series = convert_binary_series_to_str(pd.Series(data=[1, 1, 0, 1]), "bir", "iki")
        self.assertTrue(result_series.equals(pd.Series(data=["bir", "bir", "iki", "bir"])))

    def test_convert_str_series_to_binary(self):
        """tests binary conversion on series data"""
        result_series = convert_str_series_to_binary(pd.Series(data=["bir", "bir", "iki", "bir"]), "bir", "iki")
        self.assertTrue(result_series.equals(pd.Series(data=[1, 1, 0, 1]).astype(
            np.uint8)))

    def test_series_binary_conversion_data_none(self):
        """tests raise error when data is none"""
        with self.assertRaisesRegex(ValueError, "Data is None or empty."):
            convert_str_series_to_binary(None, "bir", "iki")

    def test_series_binary_conversion_data_empty(self):
        """tests raise error when data is empty"""
        with self.assertRaisesRegex(ValueError, "Data is None or empty."):
            convert_str_series_to_binary(pd.Series(dtype=np.object_), "bir", "iki")

    def test_series_binary_conversion_more_than_two(self):
        """tests raise error if class count is not equal to 2"""
        with self.assertRaisesRegex(ValueError, "Target column class count is not equal to 2."):
            convert_str_series_to_binary(pd.Series(data=["bir", "three", "iki", "bir"]), "bir", "iki")

    def test_series_binary_conversion_classes_different(self):
        """tests raise error if positive or negative class is different"""
        with self.assertRaisesRegex(ValueError,
                                    "Target column classes do not match with the given positive and negative classes."):
            convert_str_series_to_binary(pd.Series(data=["bir", "bir", "iki", "bir"]), "bir", "three")
