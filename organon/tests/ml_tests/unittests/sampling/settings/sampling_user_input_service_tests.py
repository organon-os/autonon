"""Includes tests for SamplingUserInputService"""
import unittest
from unittest.mock import patch

import pandas as pd

from organon.ml.common.enums.target_type import TargetType
from organon.ml.sampling.services.user_settings.user_sampling_settings import UserSamplingSettings
from organon.ml.sampling.settings import sampling_user_input_service
from organon.ml.sampling.settings.enums.sampling_strategy import SamplingStrategy
from organon.ml.sampling.settings.sampling_user_input_service import SamplingUserInputService


def _get_enum_side_effect(val, enum_type):
    if val is None:
        return val
    return enum_type[val]


def _get_default_if_none_side_effect(val, def_val):
    return val if val is not None else def_val


class SamplingUserInputServiceTestCase(unittest.TestCase):
    """Unittest class for SamplingUserInputService"""

    def setUp(self) -> None:
        get_enum_patcher = patch(sampling_user_input_service.__name__ + ".get_enum", side_effect=_get_enum_side_effect)
        get_enum_patcher.start()
        self.addCleanup(get_enum_patcher.stop)
        get_def_patcher = patch(sampling_user_input_service.__name__ + ".get_default_if_none",
                                side_effect=_get_default_if_none_side_effect)
        get_def_patcher.start()
        self.addCleanup(get_def_patcher.stop)
        self._user_settings = UserSamplingSettings(pd.DataFrame(), "target_col", TargetType.BINARY.name, ["col1"],
                                                   False, 0.3, SamplingStrategy.RANDOM_SAMPLING.name, 0.5, 0.3)

    def test_get_sampling_settings_data_none(self):
        """test exception when data is none"""
        self._user_settings.data = None
        with self.assertRaisesRegex(ValueError, "Data to sample should be given"):
            SamplingUserInputService.get_sampling_settings(self._user_settings)

    def test_get_sampling_settings_target_type_not_given_error(self):
        """test exception when target_column_name is given but target_type not given"""
        self._user_settings.target_type = None
        with self.assertRaisesRegex(ValueError, "Type of target column should be given"):
            SamplingUserInputService.get_sampling_settings(self._user_settings)

    def test_get_sampling_settings_strata_cols_default(self):
        """test default value for strata_columns"""
        self._user_settings.strata_columns = None
        self._user_settings.target_column_name = None
        settings = SamplingUserInputService.get_sampling_settings(self._user_settings)
        self.assertEqual([], settings.strata_columns)

    def test_get_sampling_settings_strata_cols_default_when_target_column_given(self):
        """test default value for strata_columns when target_column_name is given"""
        self._user_settings.strata_columns = None
        self._user_settings.target_column_name = "target_col"
        settings = SamplingUserInputService.get_sampling_settings(self._user_settings)
        self.assertEqual(["target_col"], settings.strata_columns)

    def test_get_sampling_settings_oversampling_without_proper_target_type(self):
        """test exception when target type is scalar but sampling strategy is oversampling"""
        self._user_settings.target_type = TargetType.SCALAR.name
        self._user_settings.sampling_strategy = SamplingStrategy.OVERSAMPLING.name
        with self.assertRaisesRegex(ValueError, "Oversampling and undersampling can only be used when there is a "
                                                "binary or multiclass target column"):
            SamplingUserInputService.get_sampling_settings(self._user_settings)

    def test_get_sampling_settings_undersampling_without_proper_target_type(self):
        """test exception when target type is scalar but sampling strategy is undersampling"""
        self._user_settings.target_type = TargetType.SCALAR.name
        self._user_settings.sampling_strategy = SamplingStrategy.UNDERSAMPLING.name
        with self.assertRaisesRegex(ValueError, "Oversampling and undersampling can only be used when there is a "
                                                "binary or multiclass target column"):
            SamplingUserInputService.get_sampling_settings(self._user_settings)

    def test_get_sampling_settings_oversampling_without_target_col_name(self):
        """test exception when target_column_name is none but sampling strategy is oversampling"""
        self._user_settings.target_column_name = None
        self._user_settings.sampling_strategy = SamplingStrategy.OVERSAMPLING.name
        with self.assertRaisesRegex(ValueError, "Oversampling and undersampling can only be used when there is a "
                                                "binary or multiclass target column"):
            SamplingUserInputService.get_sampling_settings(self._user_settings)

    def test_get_sampling_settings_undersampling_without_target_col_name(self):
        """test exception when target_column_name is none but sampling strategy is undersampling"""
        self._user_settings.target_column_name = None
        self._user_settings.sampling_strategy = SamplingStrategy.UNDERSAMPLING.name
        with self.assertRaisesRegex(ValueError, "Oversampling and undersampling can only be used when there is a "
                                                "binary or multiclass target column"):
            SamplingUserInputService.get_sampling_settings(self._user_settings)

    def test_get_sampling_settings_undersampling_multiclass_sampling_ratio_error(self):
        """test exception when sampling ratio is given for undersampling with multiclass target"""
        self._user_settings.target_type = TargetType.MULTICLASS.name
        self._user_settings.sampling_strategy = SamplingStrategy.UNDERSAMPLING.name
        self._user_settings.sampling_ratio = 0.4
        with self.assertRaisesRegex(ValueError, "Sampling ratio cannot be used for undersampling or oversampling with"
                                                " multiclass target"):
            SamplingUserInputService.get_sampling_settings(self._user_settings)

    def test_get_sampling_settings_oversampling_multiclass_sampling_ratio_error(self):
        """test exception when sampling ratio is given for oversampling with multiclass target"""
        self._user_settings.target_type = TargetType.MULTICLASS.name
        self._user_settings.sampling_strategy = SamplingStrategy.OVERSAMPLING.name
        self._user_settings.sampling_ratio = 0.4
        with self.assertRaisesRegex(ValueError, "Sampling ratio cannot be used for undersampling or oversampling with"
                                                " multiclass target"):
            SamplingUserInputService.get_sampling_settings(self._user_settings)


if __name__ == '__main__':
    unittest.main()
