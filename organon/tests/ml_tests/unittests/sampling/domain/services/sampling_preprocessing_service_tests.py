"""Includes unittests for SamplingPreprocessingService"""
import unittest
from unittest.mock import patch

import pandas as pd

from organon.fl.core.enums.column_native_type import ColumnNativeType
from organon.ml.sampling.domain.services import sampling_preprocessing_service
from organon.ml.sampling.domain.services.sampling_preprocessing_service import SamplingPreprocessingService


class SamplingPreprocessingServiceTestCase(unittest.TestCase):
    """Unittest class for SamplingPreprocessingService"""

    @patch(sampling_preprocessing_service.__name__ + ".get_column_native_type")
    @patch(sampling_preprocessing_service.__name__ + ".get_bins")
    def test_discretize(self, mock_get_bins, mock_get_col_native_type):
        """tests discretize method"""
        mock_get_col_native_type.side_effect = \
            lambda df, col: ColumnNativeType.Numeric if col in ["col1", "col2", "col3"] else ColumnNativeType.String
        mock_get_bins.return_value = pd.Series([0.0, 0.0, 1.0, 1.0, 1.0, 2.0, 2.0, 2.0, 4.0, 4.0, 5.0, 5.0, 5.0, 0.0,
                                                0.0, 1.0, 3.0, 3.0, 4.0, 3.0])
        frame = pd.DataFrame({"col1": [1] * 10 + [2] * 10,
                              "col2": [1, 1, 2, 3, 3, 3, 3, 4, 5, 6, 7, 8, 9, 1, 1, 2, 4, 4, 5, 5],
                              "col3": list(range(20)),
                              "col4": [f"val_{i}" for i in range(20)]})
        new_frame = SamplingPreprocessingService.discretize(frame.copy(), ["col1", "col2", "col4"], num_bins=6)
        mock_get_bins.assert_called_once()
        pd.testing.assert_series_equal(frame["col2"], mock_get_bins.call_args.args[0])
        self.assertEqual(6, mock_get_bins.call_args.args[1])
        pd.testing.assert_series_equal(frame["col1"], new_frame["col1"])
        pd.testing.assert_series_equal(frame["col3"], new_frame["col3"])
        pd.testing.assert_series_equal(frame["col4"], new_frame["col4"])
        self.assertEqual(new_frame["col2"].to_list(), mock_get_bins.return_value.to_list())
        col2_val_counts = new_frame["col2"].value_counts()
        self.assertEqual(6, len(col2_val_counts))
        self.assertEqual(20, col2_val_counts.sum())


if __name__ == '__main__':
    unittest.main()
