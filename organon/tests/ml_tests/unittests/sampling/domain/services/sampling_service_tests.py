"""Includes unittests for SamplingService"""
import unittest
from unittest.mock import patch

import pandas as pd

from organon.fl.core.exceptionhandling.known_exception import KnownException
from organon.ml.common.enums.target_type import TargetType
from organon.ml.sampling.domain.services import sampling_service
from organon.ml.sampling.domain.services.sampling_preprocessing_service import SamplingPreprocessingService
from organon.ml.sampling.domain.services.sampling_service import SamplingService
from organon.ml.sampling.settings.enums.sampling_strategy import SamplingStrategy
from organon.ml.sampling.settings.objects.sampling_settings import SamplingSettings


class SamplingServiceTestCase(unittest.TestCase):
    """Unittest class for SamplingService"""

    def setUp(self) -> None:
        preprocess_patcher = patch(sampling_service.__name__ + "." + SamplingPreprocessingService.__name__)
        self.mock_preprocess_service = preprocess_patcher.start()
        self.mock_preprocess_service.discretize.side_effect = lambda data, *args: data
        self.addCleanup(preprocess_patcher.stop)

        df_ops_helper_patcher = patch(sampling_service.__name__ + ".df_ops_helper")
        self.mock_df_ops_helper = df_ops_helper_patcher.start()
        self.addCleanup(preprocess_patcher.stop)

    def test_undersampling_binary(self):
        """tests undersampling binary"""
        target_col = ["a"] * 18 + ["b"] * 12
        train_data = pd.DataFrame({"a": list(range(30)), "b": [1] * 6 + [2] * 24, "c": target_col})
        test_data = pd.DataFrame({"a": list(range(10)), "b": list(range(10)), "c": ["a"] * 10})
        self.mock_df_ops_helper.get_train_test_split.return_value = (train_data, test_data)
        self.mock_df_ops_helper.get_sample_indices.return_value = pd.Series(list(range(12)))
        all_data = pd.concat([train_data, test_data])
        settings = SamplingSettings(all_data, "c", TargetType.BINARY, ["b", "c"], True, 0.25,
                                    SamplingStrategy.UNDERSAMPLING, 0.5)
        output = SamplingService.sample(settings)
        self.assertTrue(test_data.equals(output.test_data))
        self.mock_df_ops_helper.get_train_test_split.assert_called_once_with(all_data, 0.25, strata_columns=["b", "c"])
        args = self.mock_df_ops_helper.get_sample_indices.call_args.args
        df_arg = args[0]
        self.assertEqual(["b", "c"], list(df_arg.columns))
        self.assertEqual(list(range(18)), df_arg.index.tolist())  # first 18 rows(rows with "a" as class)
        self.assertEqual(0, sum(df_arg["c"] == "b"))
        self.assertEqual(12 / 18, args[1])

        self.assertEqual(24, len(output.train_data))
        self.assertEqual(12, sum(output.train_data["c"] == "a"))
        self.assertEqual(12, sum(output.train_data["c"] == "b"))

    def test_undersampling_binary_with_data_sample_ratio(self):
        """tests undersampling binary when data sample ratio is given"""
        target_col = ["a"] * 27 + ["b"] * 18
        train_data = pd.DataFrame({"a": list(range(45)), "b": [1] * 9 + [2] * 36, "c": target_col})
        test_data = pd.DataFrame({"a": list(range(15)), "b": list(range(15)), "c": ["a"] * 15})
        self.mock_df_ops_helper.get_train_test_split.return_value = (train_data, test_data)
        indices_after_data_sample = list(range(18)) + list(range(27, 39))
        self.mock_df_ops_helper.get_sample_indices.side_effect = [pd.Series(indices_after_data_sample),
                                                                  pd.Series(list(range(12)))]
        all_data = pd.concat([train_data, test_data])
        settings = SamplingSettings(all_data, "c", TargetType.BINARY, ["b", "c"], True, 0.25,
                                    SamplingStrategy.UNDERSAMPLING, 0.5, data_sample_ratio=2/3)
        output = SamplingService.sample(settings)
        self.assertTrue(test_data.equals(output.test_data))
        self.mock_df_ops_helper.get_train_test_split.assert_called_once_with(all_data, 0.25, strata_columns=["b", "c"])
        self.assertEqual(2, self.mock_df_ops_helper.get_sample_indices.call_count)

        data_sample_args = self.mock_df_ops_helper.get_sample_indices.call_args_list[0].args
        data_sample_kwargs = self.mock_df_ops_helper.get_sample_indices.call_args_list[0].kwargs
        pd.testing.assert_frame_equal(train_data[["b", "c"]], data_sample_args[0])
        self.assertEqual(2 / 3, data_sample_args[1])
        self.assertEqual(["b", "c"], data_sample_kwargs["strata_columns"])

        args = self.mock_df_ops_helper.get_sample_indices.call_args_list[1].args
        df_arg = args[0]
        self.assertEqual(["b", "c"], list(df_arg.columns))
        self.assertEqual(list(range(18)), df_arg.index.tolist())  # first 18 rows(rows with "a" as class)
        self.assertEqual(0, sum(df_arg["c"] == "b"))
        self.assertEqual(12 / 18, args[1])

        self.assertEqual(24, len(output.train_data))
        self.assertEqual(12, sum(output.train_data["c"] == "a"))
        self.assertEqual(12, sum(output.train_data["c"] == "b"))

    def test_undersampling_multiclass(self):
        """tests undersampling with multiclass target"""
        target_col = ["a"] * 12 + ["b"] * 12 + ["c"] * 6
        train_data = pd.DataFrame(
            {"a": list(range(30)), "b": [1] * 18 + [2] * 12, "c": target_col})
        test_data = pd.DataFrame({"a": list(range(10)), "b": list(range(10)), "c": ["a"] * 10})
        self.mock_df_ops_helper.get_train_test_split.return_value = (train_data, test_data)
        self.mock_df_ops_helper.get_sample_indices.side_effect = [pd.Series(list(range(0, 6))),
                                                                  pd.Series(list(range(12, 18)))]
        all_data = pd.concat([train_data, test_data])
        settings = SamplingSettings(all_data, "c", TargetType.MULTICLASS, ["b", "c"], True, 0.25,
                                    SamplingStrategy.UNDERSAMPLING, None)
        output = SamplingService.sample(settings)
        self.assertTrue(output.test_data.equals(test_data))
        self.mock_df_ops_helper.get_train_test_split.assert_called_once_with(all_data, 0.25, strata_columns=["b", "c"])
        self.assertEqual(2, self.mock_df_ops_helper.get_sample_indices.call_count)
        args = self.mock_df_ops_helper.get_sample_indices.call_args_list[0].args
        df_arg = args[0]
        self.assertEqual(["b", "c"], list(df_arg.columns))
        self.assertEqual(list(range(0, 12)), df_arg.index.tolist())  # first 12 rows(rows with "a" as class)
        self.assertEqual(12, sum(df_arg["c"] == "a"))
        self.assertEqual(6 / 12, args[1])

        args2 = self.mock_df_ops_helper.get_sample_indices.call_args_list[1].args
        df_arg_2 = args2[0]
        self.assertEqual(["b", "c"], list(df_arg_2.columns))
        self.assertEqual(list(range(12, 24)), df_arg_2.index.tolist())  # rows with "b" as class
        self.assertEqual(12, sum(df_arg_2["c"] == "b"))
        self.assertEqual(6 / 12, args2[1])

        self.assertEqual(18, len(output.train_data))
        self.assertEqual(6, sum(output.train_data["c"] == "a"))
        self.assertEqual(6, sum(output.train_data["c"] == "b"))
        self.assertEqual(6, sum(output.train_data["c"] == "c"))

    def test_undersampling_multiclass_with_data_sample_ratio(self):
        """tests multiclass undersampling when data_sample_ratio is given"""
        target_col = ["a"] * 18 + ["b"] * 18 + ["c"] * 9
        train_data = pd.DataFrame(
            {"a": list(range(45)), "b": [1] * 27 + [2] * 18, "c": target_col})
        test_data = pd.DataFrame({"a": list(range(15)), "b": list(range(15)), "c": ["a"] * 15})
        self.mock_df_ops_helper.get_train_test_split.return_value = (train_data, test_data)
        indices_after_data_sample = list(range(12)) + list(range(18, 30)) + list(range(36, 42))
        self.mock_df_ops_helper.get_sample_indices.side_effect = [
            pd.Series(indices_after_data_sample),
            pd.Series(list(range(0, 6))),
            pd.Series(list(range(18, 24)))
        ]
        all_data = pd.concat([train_data, test_data])
        settings = SamplingSettings(all_data, "c", TargetType.MULTICLASS, ["b", "c"], True, 0.25,
                                    SamplingStrategy.UNDERSAMPLING, None, data_sample_ratio=2 / 3)
        output = SamplingService.sample(settings)
        self.assertTrue(output.test_data.equals(test_data))
        self.mock_df_ops_helper.get_train_test_split.assert_called_once_with(all_data, 0.25, strata_columns=["b", "c"])
        self.assertEqual(3, self.mock_df_ops_helper.get_sample_indices.call_count)  # first for data sample ratio

        data_sample_args = self.mock_df_ops_helper.get_sample_indices.call_args_list[0].args
        data_sample_kwargs = self.mock_df_ops_helper.get_sample_indices.call_args_list[0].kwargs
        pd.testing.assert_frame_equal(train_data[["b", "c"]], data_sample_args[0])
        self.assertEqual(2/3, data_sample_args[1])
        self.assertEqual(["b", "c"], data_sample_kwargs["strata_columns"])

        args = self.mock_df_ops_helper.get_sample_indices.call_args_list[1].args
        df_arg = args[0]
        self.assertEqual(["b", "c"], list(df_arg.columns))
        self.assertEqual(list(range(0, 12)), df_arg.index.tolist())  # first 12 rows(rows with "a" as class)
        self.assertEqual(12, sum(df_arg["c"] == "a"))
        self.assertEqual(6 / 12, args[1])

        args2 = self.mock_df_ops_helper.get_sample_indices.call_args_list[2].args
        df_arg_2 = args2[0]
        self.assertEqual(["b", "c"], list(df_arg_2.columns))
        self.assertEqual(list(range(18, 30)), df_arg_2.index.tolist())  # rows with "b" as class
        self.assertEqual(12, sum(df_arg_2["c"] == "b"))
        self.assertEqual(6 / 12, args2[1])

        self.assertEqual(18, len(output.train_data))
        self.assertEqual(6, sum(output.train_data["c"] == "a"))
        self.assertEqual(6, sum(output.train_data["c"] == "b"))
        self.assertEqual(6, sum(output.train_data["c"] == "c"))

    def test_undersampling_binary_when_ratio_already_as_expected(self):
        """tests undersampling when ratio already as expected"""
        target_col = ["a"] * 24 + ["b"] * 8
        train_data = pd.DataFrame({"a": list(range(32)), "b": [1] * 16 + [2] * 16, "c": target_col})
        test_data = pd.DataFrame({"a": list(range(8)), "b": list(range(8)), "c": ["a"] * 8})
        self.mock_df_ops_helper.get_train_test_split.return_value = (train_data, test_data)
        self.mock_df_ops_helper.get_sample_indices.side_effect = [pd.Series(list(range(0, 6))),
                                                                  pd.Series(list(range(12, 18)))]
        settings = SamplingSettings(pd.concat([train_data, test_data]), "c", TargetType.BINARY, ["b", "c"], True, 0.2,
                                    SamplingStrategy.UNDERSAMPLING, 0.25)
        output = SamplingService.sample(settings)
        self.mock_df_ops_helper.get_sample_indices.assert_not_called()
        self.assertTrue(output.test_data.equals(test_data))
        self.assertEqual(8, len(output.test_data))
        self.assertEqual(24, sum(output.train_data["c"] == "a"))
        self.assertEqual(8, sum(output.train_data["c"] == "b"))

    def test_undersampling_when_ratio_is_less_than_actual_ratio(self):
        """tests exception when undersampling when undersampling ratio is lower than ratio of minimum sized class"""
        target_col = ["a"] * 18 + ["b"] * 9
        train_data = pd.DataFrame(
            {"a": list(range(27)), "b": [1] * 9 + [2] * 18, "c": target_col})

        test_data = pd.DataFrame({"a": list(range(9)), "b": list(range(9)), "c": ["a"] * 9})
        self.mock_df_ops_helper.get_train_test_split.return_value = (train_data, test_data)

        settings = SamplingSettings(pd.concat([train_data, test_data]), "c", TargetType.BINARY, ["b", "c"], True, 0.25,
                                    SamplingStrategy.UNDERSAMPLING, 0.2)
        with self.assertRaisesRegex(KnownException, "Undersampling ratio cannot be less than the ratio of minimum "
                                                    "sized class"):
            SamplingService.sample(settings)

    def test_oversampling_binary(self):
        """tests oversampling with binary target"""
        target_col = ["a"] * 18 + ["b"] * 12
        train_data = pd.DataFrame({"a": list(range(30)), "b": [1] * 6 + [2] * 24, "c": target_col})
        test_data = pd.DataFrame({"a": list(range(10)), "b": list(range(10)), "c": ["a"] * 10})
        self.mock_df_ops_helper.get_train_test_split.return_value = (train_data, test_data)
        self.mock_df_ops_helper.get_sample_indices.return_value = pd.Series(list(range(18, 30)) + list(range(18, 24)))
        all_data = pd.concat([train_data, test_data])
        settings = SamplingSettings(all_data, "c", TargetType.MULTICLASS, ["b", "c"], True, 0.25,
                                    SamplingStrategy.OVERSAMPLING, 0.5)
        output = SamplingService.sample(settings)
        self.assertTrue(test_data.equals(output.test_data))
        self.mock_df_ops_helper.get_train_test_split.assert_called_once_with(all_data, 0.25, strata_columns=["b", "c"])
        args = self.mock_df_ops_helper.get_sample_indices.call_args.args
        df_arg = args[0]
        self.assertEqual(["b", "c"], list(df_arg.columns))
        self.assertEqual(list(range(18, 30)), df_arg.index.tolist())  # (rows with "b" as class)
        self.assertEqual(0, sum(df_arg["c"] == "a"))
        self.assertEqual(18 / 12, args[1])

        self.assertEqual(36, len(output.train_data))
        self.assertEqual(18, sum(output.train_data["c"] == "a"))
        self.assertEqual(18, sum(output.train_data["c"] == "b"))

    def test_oversampling_multiclass(self):
        """tests oversampling with multiclass target"""
        target_col = ["a"] * 12 + ["b"] * 12 + ["c"] * 6
        train_data = pd.DataFrame(
            {"a": list(range(30)), "b": [1] * 18 + [2] * 12, "c": target_col})
        test_data = pd.DataFrame({"a": list(range(10)), "b": list(range(10)), "c": ["a"] * 10})
        self.mock_df_ops_helper.get_train_test_split.return_value = (train_data, test_data)
        self.mock_df_ops_helper.get_sample_indices.side_effect = [pd.Series(list(range(12, 24))),
                                                                  pd.Series(list(range(24, 30)) + list(range(24, 30)))]
        all_data = pd.concat([train_data, test_data])
        settings = SamplingSettings(all_data, "c", TargetType.MULTICLASS, ["b", "c"], True, 0.25,
                                    SamplingStrategy.OVERSAMPLING, None)
        output = SamplingService.sample(settings)
        self.assertTrue(output.test_data.equals(test_data))
        self.mock_df_ops_helper.get_train_test_split.assert_called_once_with(all_data, 0.25, strata_columns=["b", "c"])
        self.assertEqual(2, self.mock_df_ops_helper.get_sample_indices.call_count)
        args = self.mock_df_ops_helper.get_sample_indices.call_args_list[0].args
        df_arg = args[0]
        self.assertEqual(["b", "c"], list(df_arg.columns))
        self.assertEqual(list(range(12, 24)), df_arg.index.tolist())  # first 12 rows(rows with "a" as class)
        self.assertEqual(12, sum(df_arg["c"] == "b"))
        self.assertEqual(12 / 12, args[1])

        args2 = self.mock_df_ops_helper.get_sample_indices.call_args_list[1].args
        df_arg_2 = args2[0]
        self.assertEqual(["b", "c"], list(df_arg_2.columns))
        self.assertEqual(list(range(24, 30)), df_arg_2.index.tolist())  # rows with "b" as class
        self.assertEqual(6, sum(df_arg_2["c"] == "c"))
        self.assertEqual(12 / 6, args2[1])

        self.assertEqual(36, len(output.train_data))
        self.assertEqual(12, sum(output.train_data["c"] == "a"))
        self.assertEqual(12, sum(output.train_data["c"] == "b"))
        self.assertEqual(12, sum(output.train_data["c"] == "c"))

    def test_oversampling_binary_when_ratio_already_as_expected(self):
        """tests oversampling with binary target when ratio is already as expected"""
        train_data = pd.DataFrame({"a": list(range(30)), "b": [1] * 15 + [2] * 15, "c": ["a"] * 18 + ["b"] * 12})
        test_data = pd.DataFrame({"a": list(range(10)), "b": list(range(10)), "c": ["a"] * 10})
        self.mock_df_ops_helper.get_train_test_split.return_value = (train_data, test_data)
        self.mock_df_ops_helper.get_sample_indices.side_effect = [pd.Series(list(range(12, 24))),
                                                                  pd.Series(list(range(24, 30)) + list(range(24, 30)))]
        all_data = pd.concat([train_data, test_data])
        settings = SamplingSettings(all_data, "c", TargetType.BINARY, ["b", "c"], True, 0.25,
                                    SamplingStrategy.OVERSAMPLING, 0.4)
        output = SamplingService.sample(settings)
        self.assertTrue(output.test_data.equals(test_data))
        self.mock_df_ops_helper.get_sample_indices.assert_not_called()
        self.assertEqual(10, len(output.test_data))
        self.assertEqual(18, sum(output.train_data["c"] == "a"))
        self.assertEqual(12, sum(output.train_data["c"] == "b"))

    def test_oversampling_binary_when_ratio_is_less_than_actual_ratio(self):
        """tests exception when oversampling with binary target when oversampling ratio is lower than ratio of
        minimum sized class"""
        target_col = ["a"] * 12 + ["b"] * 12
        train_data = pd.DataFrame({"a": list(range(24)), "b": [1] * 12 + [2] * 12, "c": target_col})
        test_data = pd.DataFrame({"a": list(range(8)), "b": list(range(8)), "c": ["a"] * 8})
        self.mock_df_ops_helper.get_train_test_split.return_value = (train_data, test_data)
        self.mock_df_ops_helper.get_sample_indices.side_effect = [pd.Series(list(range(12, 24))),
                                                                  pd.Series(list(range(24, 30)) + list(range(24, 30)))]
        all_data = pd.concat([train_data, test_data])
        settings = SamplingSettings(all_data, "c", TargetType.BINARY, ["b", "c"], True, 0.25,
                                    SamplingStrategy.OVERSAMPLING, 0.2)
        with self.assertRaisesRegex(KnownException, "Oversampling ratio cannot be less than the ratio of minimum "
                                                    "sized class"):
            SamplingService.sample(settings)

    def test_random_sampling(self):
        """tests random sampling"""
        train_data = pd.DataFrame(
            {"a": list(range(30)), "b": [1] * 12 + [2] * 12 + [3] * 6, "c": ["a"] * 18 + ["b"] * 12})
        test_data = pd.DataFrame({"a": list(range(10)), "b": list(range(10)), "c": ["a"] * 10})
        self.mock_df_ops_helper.get_train_test_split.return_value = (train_data, test_data)
        self.mock_df_ops_helper.get_sample_indices.return_value = pd.Series(list(range(10)))
        all_data = pd.concat([train_data, test_data])
        settings = SamplingSettings(all_data, "c", TargetType.SCALAR, ["b", "c"], True, 0.25,
                                    SamplingStrategy.RANDOM_SAMPLING, 1 / 3)
        output = SamplingService.sample(settings)
        self.assertTrue(output.test_data.equals(test_data))
        self.assertEqual(10, len(output.train_data))
        self.assertEqual(list(range(10)), output.train_data.index.tolist())

        args = self.mock_df_ops_helper.get_sample_indices.call_args_list[0].args
        df_arg = args[0]
        self.assertEqual(["b", "c"], list(df_arg.columns))
        self.assertEqual(1 / 3, args[1])

    def test_none_sampling(self):
        """tests none sampling"""
        train_data = pd.DataFrame({"a": list(range(30)), "b": [1] * 15 + [2] * 15, "c": list(range(30))})
        test_data = pd.DataFrame({"a": list(range(10)), "b": list(range(10)), "c": ["a"] * 10})
        self.mock_df_ops_helper.get_train_test_split.return_value = (train_data, test_data)
        settings = SamplingSettings(pd.concat([train_data, test_data]), "c", TargetType.SCALAR, ["b"], True, 0.25,
                                    None, 0.5)
        output = SamplingService.sample(settings)
        self.assertTrue(output.test_data.equals(test_data))
        self.assertTrue(output.train_data.equals(train_data))

if __name__ == '__main__':
    unittest.main()
