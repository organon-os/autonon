"""Includes unittests for PriorityQueue"""
import unittest

from organon.ml.feature_selection.domain.objects.priority_queue import PriorityQueue


class PriorityQueueTestCase(unittest.TestCase):
    """Unittest class for PriorityQueue"""

    def test_is_empty_initial(self):
        """tests if is_empty returns True initially"""
        self.assertTrue(PriorityQueue().is_empty())

    def test_push_pop(self):
        """tests push and pop"""
        queue = PriorityQueue()
        queue.push("a", 5)
        queue.push("b", 4)
        queue.push("b", 6)  # insert same value with different priority
        self.assertEqual(("b", 6), queue.pop())
        self.assertEqual(("a", 5), queue.pop())
        self.assertTrue(queue.is_empty())

    def test_pop_when_empty(self):
        """tests error when pop is called when queue is empty"""
        queue = PriorityQueue()
        queue.push("a", 5)
        queue.pop()
        with self.assertRaisesRegex(ValueError, "Queue is empty"):
            queue.pop()


if __name__ == '__main__':
    unittest.main()
