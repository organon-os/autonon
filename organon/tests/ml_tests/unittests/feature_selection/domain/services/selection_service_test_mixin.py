"""Includes SelectionServiceTestMixin class"""

from organon.ml.feature_selection.domain.services import base_feature_selection_service, \
    base_supervised_feature_selection_service
from organon.ml.preprocessing.services.preprocessor import Preprocessor
from organon.tests import test_helper


class SelectionServiceTestMixin:
    """Mixin class for common operations in selection service test classes"""

    def _set_up_rest(self):
        pass

    def setUp(self):  # pylint: disable=invalid-name
        """setUp"""
        self.mock_preprocessor = test_helper.get_mock(
            self, base_feature_selection_service.__name__ + "." + Preprocessor.__name__)
        self.mock_preprocessor.get_imputation_service.return_value.transform.side_effect = lambda x: x
        self.mock_preprocessor.get_one_hot_encoding_service.return_value.transform.side_effect = lambda data: data
        self.mock_lbl_encoder = test_helper.get_mock(
            self, base_supervised_feature_selection_service.__name__ + ".LabelEncoder").return_value
        self.mock_lbl_encoder.transform.side_effect = lambda ser: ser
        self.mock_get_numerical_column_names = test_helper.get_mock(
            self, base_feature_selection_service.__name__ + ".get_numerical_column_names"
        )
        self._set_up_rest()
