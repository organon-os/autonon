"""Includes unittests for LassoSelectionService"""
import unittest

import numpy as np
import pandas as pd

from organon.ml.common.enums.target_type import TargetType
from organon.ml.feature_selection.domain.objects.settings.lasso_selection_settings import LassoSelectionSettings
from organon.ml.feature_selection.domain.services import lasso_selection_service
from organon.ml.feature_selection.domain.services.lasso_selection_service import LassoSelectionService
from organon.tests import test_helper
from organon.tests.ml_tests.unittests.feature_selection.domain.services.selection_service_test_mixin import \
    SelectionServiceTestMixin


class LassoSelectionServiceTestCase(SelectionServiceTestMixin, unittest.TestCase):
    """Unittest class for LassoSelectionService"""

    def _set_up_rest(self) -> None:
        data = pd.DataFrame({"a": [1, 2, 3], "b": [4, 5, 6], "c": [7, 8, 9]})
        self.mock_get_numerical_column_names.return_value = ["a", "b", "c"]
        target = pd.DataFrame({"target": [0, 1, 1]})
        self.settings = LassoSelectionSettings(data, target, target_type=TargetType.BINARY, lasso_args={"alpha": 1.0})
        self.mock_lasso = test_helper.get_mock(
            self, lasso_selection_service.__name__ + ".Lasso")

    def test_run_selection_binary(self):
        """tests run_selection with binary target"""
        self.settings.target = pd.DataFrame({"target": [0, 1, 1]})
        self.settings.target_type = TargetType.BINARY
        self.mock_lasso.return_value.coef_ = np.array([0, 0.3, 0])
        output = LassoSelectionService().run_selection(self.settings)
        self.mock_lasso.assert_called_with(alpha=1.0)
        self.assertCountEqual(["b"], output.selected_features)

    def test_run_selection_scalar(self):
        """tests run_selection with scalar target"""
        self.settings.target = pd.DataFrame({"target": [0.5, 1.2, 1.8]})
        self.settings.target_type = TargetType.SCALAR
        self.mock_lasso.return_value.coef_ = np.array([0, 0.3, 0])
        output = LassoSelectionService().run_selection(self.settings)
        self.mock_lasso.assert_called_with(alpha=1.0)
        self.assertCountEqual(["b"], output.selected_features)

    def test_run_selection_no_lasso_args(self):
        """tests run_selection with binary target when lasso_args is None"""
        self.settings.lasso_args = None
        self.mock_lasso.return_value.coef_ = np.array([0, 0.8, 0.5])
        output = LassoSelectionService().run_selection(self.settings)
        self.mock_lasso.assert_called_with()
        self.assertCountEqual(["b", "c"], output.selected_features)

    def test_run_selection_multiclass_error(self):
        """tests error on run_selection when multiclass target is given"""
        self.settings.target = pd.DataFrame({"target": [0, 1, 2]})
        self.settings.target_type = TargetType.MULTICLASS
        self.mock_lasso.return_value.coef_ = np.array([0, 0.8, 0.5])
        with self.assertRaisesRegex(ValueError, "Feature selection with Lasso works only for BINARY or SCALAR targets"):
            LassoSelectionService().run_selection(self.settings)

    def test_run_selection_one_hot_encoded_feature_selected(self):
        """tests run_selection - one hot encoded column is selected"""
        self.settings.data = pd.DataFrame({"int_col": [1, 2, 3], "str_col": ["a", "b", "c"]})
        self.mock_get_numerical_column_names.return_value = ["int_col"]
        self.mock_preprocessor.get_one_hot_encoding_service.return_value.transform.side_effect = None
        self.mock_preprocessor.get_one_hot_encoding_service.return_value.transform.return_value = \
            pd.DataFrame({"int_col": [1, 2, 3], "str_col_a": [1, 0, 0], "str_col_b": [0, 1, 0], "str_col_c": [0, 0, 1]})
        self.mock_preprocessor.get_one_hot_encoding_service.return_value.columns_dict = {
            "str_col": ["str_col_a", "str_col_b", "str_col_c"]}
        self.mock_lasso.return_value.coef_ = np.array([0.1, 0.8, 0, 0.3])
        output = LassoSelectionService().run_selection(self.settings)
        self.assertCountEqual(["int_col", "str_col"], output.selected_features)


if __name__ == '__main__':
    unittest.main()
