"""Includes unittests for SweepSelectionService"""
import unittest

import numpy as np
import pandas as pd

from organon.fl.logging.helpers.log_helper import LogHelper
from organon.ml.feature_selection.domain.objects.settings.sweep_selection_settings import SweepSelectionSettings
from organon.fl.modelling.unsupervised_feature_extractor import UnsupervisedFeatureExtractor
from organon.ml.feature_selection.domain.services import base_feature_selection_service
from organon.ml.feature_selection.domain.services.sweep_selection_service import SweepSelectionService
from organon.tests import test_helper
from organon.tests.ml_tests.unittests.feature_selection.domain.services.selection_service_test_mixin import \
    SelectionServiceTestMixin


class SweepSelectionServiceTestCase(SelectionServiceTestMixin, unittest.TestCase):
    """Unittest class for SweepSelectionService"""

    def _set_up_rest(self) -> None:
        data = pd.DataFrame({"a": [4, 9, 5], "b": [1, 2, 3], "c": [5, 1, 8]})
        self.mock_get_numerical_column_names.return_value = ["a", "b", "c"]
        data["c"] = data["c"].astype(np.float32)
        self.settings = SweepSelectionSettings(data, bin_count=10, r_factor=0.5, max_col_count=10, n_threads=4)
        self.mock_unsup_feature_extractor = test_helper.get_mock(
            self, base_feature_selection_service.__name__ + "." + UnsupervisedFeatureExtractor.__name__
        )
        self.mock_log_helper = test_helper.get_mock(
            self, base_feature_selection_service.__name__ + "." + LogHelper.__name__
        )

    def test_run_selection(self):
        """test run_selection"""
        self.mock_unsup_feature_extractor.return_value.execute.return_value = ["a", "b"]
        output = SweepSelectionService().run_selection(self.settings)
        self.assertCountEqual(["a", "b"], output.selected_features)
        self.assertEqual({"bin_count": 10, "r_factor": 0.5, "max_iter": 10},
                         self.mock_unsup_feature_extractor.call_args.kwargs)
        dict_df = self.mock_unsup_feature_extractor.call_args.args[0]
        self.assertEqual(self.settings.data.columns.to_list(), list(dict_df.keys()))
        for col, ser in dict_df.items():
            pd.testing.assert_series_equal(self.settings.data[col].astype(np.float32), ser)
        self.mock_unsup_feature_extractor.return_value.execute.assert_called_with(num_threads=4)
        self.mock_log_helper.warning.assert_called_with(
            "dtypes of these numeric columns will be converted to np.float32 before sweep: a,b")

    def test_run_selection_one_hot_encoded_feature_selected(self):
        """tests run_selection - one hot encoded column is selected"""
        self.settings.data = pd.DataFrame({"int_col": [1, 2, 3], "str_col": ["a", "b", "c"]})
        self.mock_get_numerical_column_names.return_value = ["int_col"]
        self.mock_preprocessor.get_one_hot_encoding_service.return_value.transform.side_effect = None
        self.mock_preprocessor.get_one_hot_encoding_service.return_value.transform.return_value = \
            pd.DataFrame({"int_col": [1, 2, 3], "str_col_a": [1, 0, 0], "str_col_b": [0, 1, 0], "str_col_c": [0, 0, 1]})
        self.mock_preprocessor.get_one_hot_encoding_service.return_value.columns_dict = {
            "str_col": ["str_col_a", "str_col_b", "str_col_c"]}
        self.mock_unsup_feature_extractor.return_value.execute.return_value = ["int_col", "str_col_b"]
        output = SweepSelectionService().run_selection(self.settings)
        self.assertCountEqual(["int_col", "str_col"], output.selected_features)
