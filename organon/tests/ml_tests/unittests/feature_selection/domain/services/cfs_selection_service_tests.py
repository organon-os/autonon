"""Includes unittests for CFSSelectionService"""
import unittest
from collections import namedtuple

import numpy as np
import pandas as pd

from organon.ml.common.enums.target_type import TargetType
from organon.ml.feature_selection.domain.objects.priority_queue import PriorityQueue
from organon.ml.feature_selection.domain.objects.settings.cfs_selection_settings import CFSSelectionSettings
from organon.ml.feature_selection.domain.services import cfs_selection_service
from organon.ml.feature_selection.domain.services.cfs_selection_service import CFSSelectionService
from organon.tests import test_helper
from organon.tests.ml_tests.unittests.feature_selection.domain.services.selection_service_test_mixin import \
    SelectionServiceTestMixin


def _get_res(corr, pval=0):
    res = namedtuple('PointbiserialrResult',
                     ('correlation', 'pvalue'))
    return res(corr, pval)


def _get_correlation_matrix_side_effect(dataframe, *args, **kwargs):
    # pylint: disable=unused-argument
    corr_df = dataframe.corr()
    corr_df.values[np.tril_indices_from(corr_df.values)] = np.nan
    return corr_df


class CFSSelectionServiceTestCase(SelectionServiceTestMixin, unittest.TestCase):
    """Unittest class for CFSSelectionService"""

    def _set_up_rest(self) -> None:
        data = pd.DataFrame({"a": [1, 2, 3], "b": [4, 5, 6], "c": [7, 8, 9]})
        self.mock_get_numerical_column_names.return_value = ["a", "b", "c"]
        target = pd.DataFrame({"target": [0, 1, 1]})
        self.settings = CFSSelectionSettings(data, target, TargetType.BINARY)
        self.mock_queue = test_helper.get_mock(
            self, cfs_selection_service.__name__ + "." + PriorityQueue.__name__).return_value
        self.mock_pointbiserial = test_helper.get_mock(self,
                                                       cfs_selection_service.__name__ + ".pointbiserialr")
        self.mock_get_corr_matrix = test_helper.get_mock(
            self, cfs_selection_service.__name__ + ".get_correlation_matrix_memory_efficient")
        self.mock_get_corr_matrix.side_effect = _get_correlation_matrix_side_effect

    def test_run_selection_target_type_error(self):
        """test run_selection error when target is multiclass"""
        self.settings.target = pd.DataFrame({"target": [0, 1, 2]})
        self.settings.target_type = TargetType.MULTICLASS
        service = CFSSelectionService()
        with self.assertRaisesRegex(ValueError,
                                    "Correlation based feature selection works only for BINARY or SCALAR targets"):
            service.run_selection(self.settings)

    def test_run_selection(self):
        """tests run_selection"""
        self.mock_pointbiserial.side_effect = [_get_res(0.8), _get_res(0.7), _get_res(0.95)]
        self.mock_queue.is_empty.side_effect = [False, False, False, False, True]
        self.mock_queue.pop.side_effect = [({"c"}, 0.95), ({"c", "b"}, 0.97), ({"c", "b", "a"}, 0.98),
                                           ({"c", "a"}, 0.875)]
        output = CFSSelectionService().run_selection(self.settings)

        self.assertEqual(4, self.mock_queue.push.call_count)
        self.assertEqual(4, self.mock_queue.pop.call_count)
        self.assertEqual(({"c"}), self.mock_queue.push.call_args_list[0].args[0])
        self.assertEqual(({"c", "a"}), self.mock_queue.push.call_args_list[1].args[0])
        self.assertEqual(({"c", "b"}), self.mock_queue.push.call_args_list[2].args[0])
        self.assertEqual({"c", "a", "b"}, self.mock_queue.push.call_args_list[3].args[0])
        self.assertCountEqual(["a", "b", "c"], output.selected_features)

    def test_run_selection_max_backtracks_reached(self):
        """tests if iteration stops when max_backtracks reached"""
        self.settings.max_backtracks = 2
        self.mock_pointbiserial.side_effect = [_get_res(0.8), _get_res(0.9), _get_res(0.95)]
        self.mock_queue.is_empty.side_effect = [False, False, False, False, True]
        self.mock_queue.pop.side_effect = [({"c"}, 0.95), ({"c", "b"}, 0.925), ({"c", "b", "a"}, 0.883),
                                           ({"c", "a"}, 0.875)]
        output = CFSSelectionService().run_selection(self.settings)

        self.assertEqual(4, self.mock_queue.push.call_count)
        self.assertEqual(3, self.mock_queue.pop.call_count)  # since max_backtracks reached early
        self.assertEqual(({"c"}, 0.95), self.mock_queue.push.call_args_list[0].args)
        self.assertEqual(({"c", "a"}, 0.875), self.mock_queue.push.call_args_list[1].args)
        self.assertEqual(({"c", "b"}, 0.925), self.mock_queue.push.call_args_list[2].args)
        self.assertEqual({"c", "a", "b"}, self.mock_queue.push.call_args_list[3].args[0])
        np.testing.assert_almost_equal(2.65 / 3, self.mock_queue.push.call_args_list[3].args[1])
        self.assertEqual(["c"], output.selected_features)

    def test_run_selection_one_hot_encoded_feature_selected(self):
        """tests run_selection - one hot encoded column is selected"""
        self.settings.data = pd.DataFrame({"int_col": [1, 2, 3], "str_col": ["a", "b", "a"]})
        self.mock_get_numerical_column_names.return_value = ["int_col"]
        self.mock_pointbiserial.side_effect = [_get_res(0.8), _get_res(0.9), _get_res(0.95)]
        self.mock_queue.is_empty.side_effect = [False, False, False, False, True]
        self.mock_queue.pop.side_effect = [({"str_col_b"}, 0.95), ({"str_col_b", "str_col_a"}, 0.925),
                                           ({"str_col_b", "str_col_a", "int_col"}, 0.98),
                                           ({"str_col_b", "int_col"}, 0.875)]
        self.mock_preprocessor.get_one_hot_encoding_service.return_value.transform.side_effect = None
        self.mock_preprocessor.get_one_hot_encoding_service.return_value.transform.return_value = \
            pd.DataFrame({"int_col": [1, 2, 3], "str_col_a": [1, 0, 0], "str_col_b": [0, 1, 0]})
        self.mock_preprocessor.get_one_hot_encoding_service.return_value.columns_dict = {
            "str_col": ["str_col_a", "str_col_b"]}
        output = CFSSelectionService().run_selection(self.settings)
        self.assertCountEqual(["int_col", "str_col"], output.selected_features)

    def test_run_selection_with_prioritize_performance(self):
        """tests run_selection with prioritize_performance=False"""
        self.settings.prioritize_performance = True
        self.mock_pointbiserial.side_effect = [_get_res(0.8), _get_res(0.7), _get_res(0.95)]
        self.mock_queue.is_empty.side_effect = [False, False, False, False, True]
        self.mock_queue.pop.side_effect = [({"c"}, 0.95), ({"c", "b"}, 0.97), ({"c", "b", "a"}, 0.98),
                                           ({"c", "a"}, 0.875)]
        output = CFSSelectionService().run_selection(self.settings)
        self.mock_get_corr_matrix.assert_not_called()
        self.assertEqual(4, self.mock_queue.push.call_count)
        self.assertEqual(4, self.mock_queue.pop.call_count)
        self.assertEqual(({"c"}), self.mock_queue.push.call_args_list[0].args[0])
        self.assertEqual(({"c", "a"}), self.mock_queue.push.call_args_list[1].args[0])
        self.assertEqual(({"c", "b"}), self.mock_queue.push.call_args_list[2].args[0])
        self.assertEqual({"c", "a", "b"}, self.mock_queue.push.call_args_list[3].args[0])
        self.assertCountEqual(["a", "b", "c"], output.selected_features)


if __name__ == '__main__':
    unittest.main()
