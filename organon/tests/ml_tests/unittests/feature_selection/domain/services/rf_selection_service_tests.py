"""Includes unittests for RFSelectionService"""
import unittest

import numpy as np
import pandas as pd

from organon.fl.logging.helpers.log_helper import LogHelper
from organon.ml.common.enums.target_type import TargetType
from organon.ml.feature_selection.domain.objects.settings.rf_selection_settings import RFSelectionSettings
from organon.fl.modelling.unsupervised_feature_extractor import UnsupervisedFeatureExtractor
from organon.ml.feature_selection.domain.services import rf_selection_service, base_feature_selection_service
from organon.ml.feature_selection.domain.services.rf_selection_service import RFSelectionService
from organon.tests import test_helper
from organon.tests.ml_tests.unittests.feature_selection.domain.services.selection_service_test_mixin import \
    SelectionServiceTestMixin


class RFSelectionServiceTestCase(SelectionServiceTestMixin, unittest.TestCase):
    """Unittest class for RFSelectionService"""

    def _set_up_rest(self) -> None:
        data = pd.DataFrame({"a": [1, 2, 3], "b": [4, 5, 6], "c": [7, 8, 9]})
        self.mock_get_numerical_column_names.return_value = ["a", "b", "c"]
        data["c"] = data["c"].astype(np.float32)
        target = pd.DataFrame({"target": [0, 1, 1]})
        self.settings = RFSelectionSettings(data, target, target_type=TargetType.BINARY, num_features=2,
                                            rf_args={"n_estimators": 100})
        self.mock_rf_clf = test_helper.get_mock(
            self, rf_selection_service.__name__ + ".RandomForestClassifier")
        self.mock_rf_reg = test_helper.get_mock(
            self, rf_selection_service.__name__ + ".RandomForestRegressor")
        self.mock_unsup_feature_extractor = test_helper.get_mock(
            self, base_feature_selection_service.__name__ + "." + UnsupervisedFeatureExtractor.__name__
        )
        self.mock_log_helper = test_helper.get_mock(
            self, base_feature_selection_service.__name__ + "." + LogHelper.__name__
        )

    def test_run_selection_binary(self):
        """tests run_selection with binary target"""
        self.settings.target = pd.DataFrame({"target": [0, 1, 1]})
        self.settings.target_type = TargetType.BINARY
        self.mock_rf_clf.return_value.feature_importances_ = np.array([0.3, 0.8, 0.5])
        output = RFSelectionService().run_selection(self.settings)
        self.mock_rf_clf.assert_called_with(n_estimators=100)
        self.assertCountEqual(["b", "c"], output.selected_features)

    def test_run_selection_no_rf_args(self):
        """tests run_selection with binary target when rf_args is None"""
        self.settings.rf_args = None
        self.mock_rf_clf.return_value.feature_importances_ = np.array([0.3, 0.8, 0.5])
        output = RFSelectionService().run_selection(self.settings)
        self.mock_rf_clf.assert_called_with()
        self.assertCountEqual(["b", "c"], output.selected_features)

    def test_run_selection_binary_num_features_not_given(self):
        """tests run_selection with binary target when num_features not given"""
        self.settings.num_features = None
        self.settings.target = pd.DataFrame({"target": [0, 1, 1]})
        self.settings.target_type = TargetType.BINARY
        self.mock_rf_clf.return_value.feature_importances_ = np.array([0.3, 0.8, 0.5])
        self.mock_unsup_feature_extractor.return_value.execute.return_value = ["a"]
        output = RFSelectionService().run_selection(self.settings)
        self.mock_unsup_feature_extractor.assert_called()
        dict_df = self.mock_unsup_feature_extractor.call_args.args[0]
        self.assertEqual(self.settings.data.columns.to_list(), list(dict_df.keys()))
        for col, ser in dict_df.items():
            pd.testing.assert_series_equal(self.settings.data[col].astype(np.float32), ser)
        self.mock_rf_clf.assert_called_with(n_estimators=100)
        self.assertCountEqual(["b"], output.selected_features)
        self.mock_log_helper.warning.assert_called_with(
            "dtypes of these numeric columns will be converted to np.float32 before sweep: a,b")

    def test_run_selection_multiclass(self):
        """tests run_selection with multiclass target"""
        self.settings.target = pd.DataFrame({"target": [0, 1, 2]})
        self.settings.target_type = TargetType.MULTICLASS
        self.mock_rf_clf.return_value.feature_importances_ = np.array([0.3, 0.8, 0.5])
        output = RFSelectionService().run_selection(self.settings)
        self.mock_rf_clf.assert_called_with(n_estimators=100)
        self.assertCountEqual(["b", "c"], output.selected_features)

    def test_run_selection_scalar_target(self):
        """tests run_selection with scalar target"""
        self.settings.target = pd.DataFrame({"target": [0.5, 0.4, 0.7]})
        self.settings.target_type = TargetType.SCALAR
        self.mock_rf_reg.return_value.feature_importances_ = np.array([0.3, 0.8, 0.5])
        output = RFSelectionService().run_selection(self.settings)
        self.mock_rf_reg.assert_called_with(n_estimators=100)
        self.assertCountEqual(["b", "c"], output.selected_features)

    def test_run_selection_one_hot_encoded_feature_selected(self):
        """tests run_selection - one hot encoded column is selected"""
        self.settings.data = pd.DataFrame({"int_col": [1, 2, 3], "str_col": ["a", "b", "c"]})
        self.mock_get_numerical_column_names.return_value = ["int_col"]
        self.mock_preprocessor.get_one_hot_encoding_service.return_value.transform.side_effect = None
        self.mock_preprocessor.get_one_hot_encoding_service.return_value.transform.return_value = \
            pd.DataFrame({"int_col": [1, 2, 3], "str_col_a": [1, 0, 0], "str_col_b": [0, 1, 0], "str_col_c": [0, 0, 1]})
        self.mock_preprocessor.get_one_hot_encoding_service.return_value.columns_dict = {
            "str_col": ["str_col_a", "str_col_b", "str_col_c"]}
        self.mock_rf_clf.return_value.feature_importances_ = np.array([0.7, 0.8, 0.5, 0.2])
        output = RFSelectionService().run_selection(self.settings)
        self.assertCountEqual(["int_col", "str_col"], output.selected_features)


if __name__ == '__main__':
    unittest.main()
