"""Includes unittests for FeatureSimilaritySelectionService"""
import unittest

import numpy as np
import pandas as pd

from organon.ml.feature_selection.domain.objects.settings.feature_similarity_selection_settings import \
    FeatureSimilaritySelectionSettings
from organon.ml.feature_selection.domain.services import feature_similarity_selection_service
from organon.ml.feature_selection.domain.services.feature_similarity_selection_service import \
    FeatureSimilaritySelectionService
from organon.tests import test_helper
from organon.tests.ml_tests.unittests.feature_selection.domain.services.selection_service_test_mixin import \
    SelectionServiceTestMixin


def _get_correlation_matrix_side_effect(dataframe, columns=None, n_threads=None):
    # pylint: disable=unused-argument
    if columns is not None:
        return dataframe[columns].corr()
    return dataframe.corr()


class FeatureSimilaritySelectionServiceTestCase(SelectionServiceTestMixin, unittest.TestCase):
    """Unittest class for FeatureSimilaritySelectionService"""

    def _set_up_rest(self) -> None:
        data = pd.DataFrame({"a": [4, 9, 5], "b": [1, 2, 3], "c": [5, 1, 8], "d": [7, 8, 9],
                             "e": [5, 3, 8],
                             "zero_var_col": [10, 10, 10], "all_nan_col": [np.nan, np.nan, np.nan]})
        self.settings = FeatureSimilaritySelectionSettings(data, selection_percent=0)
        self.mock_get_corr_matrix = test_helper.get_mock(
            self, feature_similarity_selection_service.__name__ + ".get_correlation_matrix_memory_efficient")
        self.mock_get_corr_matrix.side_effect = _get_correlation_matrix_side_effect

    def test_run_selection_k_val_equals_1_after_first_iteration(self):
        """test run_selection when k_val equals 1 after first iteration of algorithm"""
        self.settings.selection_percent = 0.5
        output = FeatureSimilaritySelectionService().run_selection(self.settings)
        self.assertCountEqual(["a", "e"], output.selected_features)

    def test_run_selection_initial_k_val_equal_to_max_index_in_mci(self):
        """test run_selection when first k_val value between"""
        self.settings.selection_percent = 0.8
        output = FeatureSimilaritySelectionService().run_selection(self.settings)
        self.assertCountEqual(["b"], output.selected_features)

    def test_run_selection_initially_k_val_higher_than_max_index_in_mci(self):
        """test run_selection"""
        self.settings.selection_percent = 0.9
        output = FeatureSimilaritySelectionService().run_selection(self.settings)
        self.assertCountEqual(["b"], output.selected_features)

    def test_run_selection_initially_k_val_is_not_higher_than_1(self):
        """test run_selection"""
        self.settings.selection_percent = 0.25
        output = FeatureSimilaritySelectionService().run_selection(self.settings)
        self.assertCountEqual(["a", "b", "c", "e"], output.selected_features)

    def test_run_selection(self):
        """test run_selection"""
        self.settings.data = pd.DataFrame(
            {"a": [4, 9, 5], "b": [1, 2, 3], "c": [5, 1, 8], "d": [7, 8, 9],
             "e": [5, 3, 8], "f": [10, 23, 42], "g": [9, 3, 11], "h": [11, 23, 8],
             "zero_var_col": [10, 10, 10], "all_nan_col": [np.nan, np.nan, np.nan]})
        self.settings.selection_percent = 0.25
        output = FeatureSimilaritySelectionService().run_selection(self.settings)
        self.assertCountEqual(["a", "b", "c", "h"], output.selected_features)

    def test_run_selection_no_cols_after_initial_elimination(self):
        """test run_selection when all columns are eliminated before the iterations"""
        self.settings.data = pd.DataFrame(
            {"a": [1, 1, 1], "b": [np.nan, np.nan, np.nan]})
        output = FeatureSimilaritySelectionService().run_selection(self.settings)
        self.assertCountEqual([], output.selected_features)

    def test_run_selection_single_col_after_initial_elimination(self):
        """test run_selection when all but one column is eliminated before the iterations"""
        self.settings.data = pd.DataFrame(
            {"a": [1, 1, 1], "b": [np.nan, np.nan, np.nan], "c": [1, 2, 3]})
        output = FeatureSimilaritySelectionService().run_selection(self.settings)
        self.assertCountEqual(["c"], output.selected_features)

    def test_run_selection_one_hot_encoded_feature_selected(self):
        """tests run_selection - one hot encoded column is selected"""
        self.settings.data = pd.DataFrame({"int_col": [1, 2, 3], "str_col": ["a", "b", "c"]})
        self.mock_preprocessor.get_one_hot_encoding_service.return_value.transform.side_effect = None
        self.mock_preprocessor.get_one_hot_encoding_service.return_value.transform.return_value = \
            pd.DataFrame({"int_col": [1, 2, 3], "str_col_a": [1, 0, 0], "str_col_b": [0, 1, 0], "str_col_c": [0, 0, 1]})
        self.mock_preprocessor.get_one_hot_encoding_service.return_value.columns_dict = {
            "str_col": ["str_col_a", "str_col_b", "str_col_c"]}
        output = FeatureSimilaritySelectionService().run_selection(self.settings)
        self.assertCountEqual(["int_col", "str_col"], output.selected_features)

    def test_run_selection_with_prioritize_performance(self):
        """test run_selection with prioritize_performance=True"""
        self.settings.prioritize_performance = True
        self.settings.data = pd.DataFrame(
            {"a": [4, 9, 5], "b": [1, 2, 3], "c": [5, 1, 8], "d": [7, 8, 9],
             "e": [5, 3, 8], "f": [10, 23, 42], "g": [9, 3, 11], "h": [11, 23, 8],
             "zero_var_col": [10, 10, 10], "all_nan_col": [np.nan, np.nan, np.nan]})
        self.settings.selection_percent = 0.25
        output = FeatureSimilaritySelectionService().run_selection(self.settings)
        self.assertCountEqual(["a", "b", "c", "h"], output.selected_features)
        self.mock_get_corr_matrix.assert_not_called()
