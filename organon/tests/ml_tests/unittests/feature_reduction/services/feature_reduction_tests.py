"""Includes tests for FeatureReduction"""
import unittest
from unittest.mock import patch
import pandas as pd

from organon.ml.common.enums.target_type import TargetType
from organon.ml.feature_reduction.domain.enums.feature_reduction_types import FeatureReductionType
from organon.ml.feature_reduction.domain.services.feature_reduction_service import FeatureReductionService
from organon.ml.feature_reduction.services import feature_reduction
from organon.ml.feature_reduction.services.feature_reduction import FeatureReduction
from organon.ml.feature_reduction.settings.feature_reduction_user_input_service import FeatureReductionUserInputService


class FeatureReductionTestCase(unittest.TestCase):
    """Unittest class for FeatureReduction"""

    def setUp(self) -> None:
        feature_reduction_user_input_service_patcher = patch(
            feature_reduction.__name__ + "." + FeatureReductionUserInputService.__name__)
        self.mock_feature_reduction_user_input_service = feature_reduction_user_input_service_patcher.start()
        self.addCleanup(feature_reduction_user_input_service_patcher.stop)
        feature_reduction_service_patcher = patch(
            feature_reduction.__name__ + "." + FeatureReductionService.__name__)
        self.mock_feature_reduction_service = feature_reduction_service_patcher.start()
        self.addCleanup(feature_reduction_service_patcher.stop)
        self.feature_reduction_user_input_service_instance = self.mock_feature_reduction_user_input_service.return_value
        self.feature_reduction = FeatureReduction()

    def test_execute(self):
        """Test for execute method """
        data = pd.DataFrame([1, 2, 3])
        drop_cols = True
        included_feature_reduction_type = [FeatureReductionType.UNIVARIATE_PERFORMANCE.name,
                                           FeatureReductionType.NULL.name,
                                           FeatureReductionType.SIMILAR_DISTRIBUTION.name,
                                           FeatureReductionType.STABILITY.name,
                                           FeatureReductionType.HIGH_CORRELATION.name]
        self.feature_reduction.execute(data, drop_cols,
                                       included_feature_reduction_type,
                                       TargetType.BINARY.name, "target", "accuracy_score"
                                       )
        self.feature_reduction_user_input_service_instance.generate_feature_reduction_settings.assert_called_with(
            self.feature_reduction.settings)
        self.mock_feature_reduction_service.execute.assert_called()

    def test_set_null_feature_reduction_settings(self):
        """Test for set_null_feature_reduction_settings method """
        self.feature_reduction.set_null_feature_reduction_settings(0.2)
        self.assertEqual(0.2, self.feature_reduction.settings.null_ratio_threshold)

    def test_set_high_correlated_feature_reduction_settings(self):
        """Test for set_high_correlated_feature_reduction_settings method """

        self.feature_reduction.set_high_correlated_feature_reduction_settings(0.6)
        self.assertEqual(0.6, self.feature_reduction.settings.correlation_threshold)
        self.assertIsNone(self.feature_reduction.settings.random_state)

    def test_set_univariate_performance_feature_reduction_settings(self):
        """Test for set_similar_distribution_feature_reduction_settings method """

        self.feature_reduction.set_univariate_performance_feature_reduction_settings(0.5)
        self.assertEqual(0.5, self.feature_reduction.settings.univariate_performance_threshold)
        self.assertIsNone(self.feature_reduction.settings.random_state)

    def test_set_similar_distribution_feature_reduction_settings(self):
        """Test for set_similar_distribution_feature_reduction_settings method """

        self.feature_reduction.set_similar_distribution_feature_reduction_settings(10, 20)
        self.assertEqual(10, self.feature_reduction.settings.nunique_count)
        self.assertEqual(20, self.feature_reduction.settings.random_state)
