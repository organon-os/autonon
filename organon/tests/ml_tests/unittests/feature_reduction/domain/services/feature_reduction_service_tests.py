"""Includes tests for FeatureReductionService"""
import unittest
from unittest.mock import MagicMock

import pandas as pd

from organon.ml.common.enums.target_type import TargetType
from organon.ml.feature_reduction.domain.enums.feature_reduction_types import FeatureReductionType
from organon.ml.feature_reduction.domain.objects.univariate_performance_reduction_output import \
    UnivariatePerformanceFeatureReductionOutput
from organon.ml.feature_reduction.domain.reductions.high_correlated_feature_reduction import \
    HighCorrelatedFeatureReduction
from organon.ml.feature_reduction.domain.reductions.null_feature_reduction import NullFeatureReduction
from organon.ml.feature_reduction.domain.reductions.similar_distribution_feature_reduction import \
    SimilarDistributionFeatureReduction
from organon.ml.feature_reduction.domain.reductions.stability_feature_reduction import StabilityFeatureReduction
from organon.ml.feature_reduction.domain.reductions.univariate_performance_feature_reduction import \
    UnivariatePerformanceFeatureReduction
from organon.ml.feature_reduction.domain.services import feature_reduction_service
from organon.ml.feature_reduction.domain.services.feature_reduction_service import FeatureReductionService
from organon.ml.feature_reduction.settings.objects.high_correlated_feature_reduction_settings import \
    HighCorrelatedFeatureReductionSettings
from organon.ml.feature_reduction.settings.objects.null_feature_reduction_settings import NullFeatureReductionSettings
from organon.ml.feature_reduction.settings.objects.similar_distribution_feature_reduction_settings import \
    SimilarDistributionFeatureReductionSettings
from organon.ml.feature_reduction.settings.objects.stability_feature_reduction_settings import \
    StabilityFeatureReductionSettings
from organon.ml.feature_reduction.settings.objects.univariate_performance_feature_reduction_settings import \
    UnivariatePerformanceFeatureReductionSettings
from organon.tests import test_helper


class FeatureReductionServiceTestCase(unittest.TestCase):
    """Unittest class for FeatureReductionService"""

    def setUp(self) -> None:
        module_name = feature_reduction_service.__name__
        self.mock_null_feature_reduction = test_helper.get_mock(self, module_name + "." + NullFeatureReduction.__name__)
        self.mock_stability_feature_reduction = test_helper.get_mock(
            self, module_name + "." + StabilityFeatureReduction.__name__)
        self.mock_high_correlated_feature_reduction = test_helper.get_mock(
            self, module_name + "." + HighCorrelatedFeatureReduction.__name__)
        self.mock_similar_distribution_feature_reduction = test_helper.get_mock(
            self, module_name + "." + SimilarDistributionFeatureReduction.__name__)
        self.mock_univariate_performance_feature_reduction = test_helper.get_mock(
            self, module_name + "." + UnivariatePerformanceFeatureReduction.__name__)

        self.feature_reduction_service = FeatureReductionService()
        self.data = pd.DataFrame(
            {"col1": [1, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
             "col2": ["organon", None, None, None, None, None, None, None, None, None, None, None, None,
                      None, None],
             "col3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
             "co4": [1, None, None, 3, 4, 5, None, None, 8, 9, None, None, None, None, None],
             "col5": [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
             "col6": [24, 25, 26, 27, 28, 29, 30, 16, 17, 18, 19, 20, 21, 22, 23],
             "col7": [9, 10, 11, 12, 13, 14, 15, 1, 2, 3, 4, 5, 6, 7, 8],
             "target": [4, 5, 1, 0, 3, 0, 1, 1, 4, 0, 1.5, 0, 1, 3, 0]
             })

    def test_execute(self):
        """Test for execute method """
        data = self.data
        perf = {"col3": 0.6, "col4": 0.2, "col5": 0.8, "col6": 0.1, "col7": 0.6, "col8": 0.3, "col9": 0.2}
        reduced_cols_null = ['col1', 'col2', 'col5']
        reduced_cols_similar_dist = ['col7']
        mock_univariate_result = MagicMock(spec=UnivariatePerformanceFeatureReductionOutput())
        self.mock_univariate_performance_feature_reduction.return_value.execute.return_value = mock_univariate_result
        mock_univariate_result.univariate_performance_result = perf
        self.mock_null_feature_reduction.return_value.execute.return_value.reduced_column_list = reduced_cols_null
        self.mock_similar_distribution_feature_reduction.return_value.execute.return_value. \
            reduced_column_list = reduced_cols_similar_dist
        null_settings = NullFeatureReductionSettings(data, 0.5)
        stability_settings = StabilityFeatureReductionSettings(data)
        univariate_settings = UnivariatePerformanceFeatureReductionSettings(data, TargetType.BINARY, "target",
                                                                            "roc_auc", 0.6)
        high_correlated_settings = HighCorrelatedFeatureReductionSettings(data, TargetType.BINARY, "target",
                                                                          "r2", 0.7, perf)
        similar_dist_settings = SimilarDistributionFeatureReductionSettings(data, TargetType.BINARY, "target",
                                                                            "r2", 5, perf)
        feature_reduction_settings = [univariate_settings,
                                      stability_settings,
                                      null_settings,
                                      high_correlated_settings,
                                      similar_dist_settings
                                      ]
        results = self.feature_reduction_service.execute(feature_reduction_settings, data, drop_cols=True)
        self.mock_null_feature_reduction.assert_called_with(null_settings)
        self.mock_stability_feature_reduction.assert_called_with(stability_settings)
        self.mock_high_correlated_feature_reduction.assert_called_with(
            high_correlated_settings)
        self.mock_similar_distribution_feature_reduction.assert_called_with(similar_dist_settings)
        self.mock_univariate_performance_feature_reduction.assert_called_with(univariate_settings)

        for col in reduced_cols_null:
            self.assertNotIn(col, data.columns)
        for col in reduced_cols_similar_dist:
            self.assertNotIn(col, data.columns)

        for res in results:
            if res.feature_reduction_type == FeatureReductionType.NULL:
                self.assertCountEqual(reduced_cols_null, res.reduced_column_list)
            if res.feature_reduction_type == FeatureReductionType.SIMILAR_DISTRIBUTION:
                self.assertCountEqual(reduced_cols_similar_dist, res.reduced_column_list)

    def test_execute_no_drop_cols(self):
        """Test for execute method """
        data = self.data
        perf = {"col3": 0.6, "col4": 0.2, "col5": 0.8, "col6": 0.1, "col7": 0.6, "col8": 0.3, "col9": 0.2}
        reduced_cols_null = ['col1', 'col2', 'col5']
        reduced_cols_similar_dist = ["col1", 'col7']
        reduced_cols_high_corr = ["col2", 'col5', "col8"]
        mock_univariate_result = MagicMock(spec=UnivariatePerformanceFeatureReductionOutput())
        self.mock_univariate_performance_feature_reduction.return_value.execute.return_value = mock_univariate_result
        mock_univariate_result.univariate_performance_result = perf
        self.mock_null_feature_reduction.return_value.execute.return_value.reduced_column_list = reduced_cols_null
        self.mock_similar_distribution_feature_reduction.return_value.execute.return_value. \
            reduced_column_list = reduced_cols_similar_dist
        self.mock_high_correlated_feature_reduction.return_value.execute.return_value. \
            reduced_column_list = reduced_cols_high_corr

        null_settings = NullFeatureReductionSettings(data, 0.5)
        stability_settings = StabilityFeatureReductionSettings(data)
        univariate_settings = UnivariatePerformanceFeatureReductionSettings(data, TargetType.BINARY, "target",
                                                                            "roc_auc", 0.6)
        high_correlated_settings = HighCorrelatedFeatureReductionSettings(data, TargetType.BINARY, "target",
                                                                          "r2", 0.7, perf)
        similar_dist_settings = SimilarDistributionFeatureReductionSettings(data, TargetType.BINARY, "target",
                                                                            "r2", 5, perf)
        feature_reduction_settings = [univariate_settings,
                                      stability_settings,
                                      null_settings,
                                      high_correlated_settings,
                                      similar_dist_settings
                                      ]
        results = self.feature_reduction_service.execute(feature_reduction_settings, data, drop_cols=False)
        self.mock_null_feature_reduction.assert_called_with(null_settings)
        self.mock_stability_feature_reduction.assert_called_with(stability_settings)
        self.mock_high_correlated_feature_reduction.assert_called_with(
            high_correlated_settings)
        self.mock_similar_distribution_feature_reduction.assert_called_with(similar_dist_settings)
        self.mock_univariate_performance_feature_reduction.assert_called_with(univariate_settings)

        for res in results:
            if res.feature_reduction_type == FeatureReductionType.NULL:
                self.assertCountEqual(reduced_cols_null, res.reduced_column_list)
            if res.feature_reduction_type == FeatureReductionType.SIMILAR_DISTRIBUTION:
                self.assertCountEqual(reduced_cols_similar_dist, res.reduced_column_list)
            if res.feature_reduction_type == FeatureReductionType.HIGH_CORRELATION:
                self.assertCountEqual(reduced_cols_high_corr, res.reduced_column_list)
