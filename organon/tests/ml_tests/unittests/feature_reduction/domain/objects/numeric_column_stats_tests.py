"""Includes tests for NumericColumnStats"""
import unittest

from organon.ml.feature_reduction.domain.objects.numeric_column_stats import NumericColumnStats


class NumericColumnStatsTestCase(unittest.TestCase):
    """Unittest class for NumericColumnStats"""

    def test_equality(self):
        """Test for Numeric column stats equality Numeric column stats"""
        column1 = NumericColumnStats("col1", 1, 2, 3, 4, 5)
        column2 = NumericColumnStats("col2", 1, 2, 3, 4, 5)
        self.assertTrue(column1 == column2)

    def test_not_equality(self):
        """Test for Numeric column stats not equality Numeric column stats"""
        column1 = NumericColumnStats("col1", 5, 6, 7, 8, 9)
        column2 = NumericColumnStats("col2", 1, 2, 3, 4, 5)
        self.assertFalse(column1 == column2)
