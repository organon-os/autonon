"""Includes unittests for UnivariatePerformanceFeatureReduction"""
import unittest
from datetime import datetime

import pandas as pd

from organon.ml.common.enums.target_type import TargetType
from organon.ml.feature_reduction.domain.enums.feature_reduction_types import FeatureReductionType
from organon.ml.feature_reduction.domain.reductions import univariate_performance_feature_reduction
from organon.ml.feature_reduction.domain.reductions.univariate_performance_feature_reduction import \
    UnivariatePerformanceFeatureReduction
from organon.ml.feature_reduction.settings.objects.univariate_performance_feature_reduction_settings import \
    UnivariatePerformanceFeatureReductionSettings
from organon.tests import test_helper


class UnivariatePerformanceFeatureReductionTestCase(unittest.TestCase):
    """Unittest class for UnivariatePerformanceFeatureReduction"""

    def setUp(self) -> None:
        module_name = univariate_performance_feature_reduction.__name__
        self.mock_get_univariate_performances_for_columns = test_helper.get_mock(
            self, module_name + ".get_univariate_performances_for_columns"
        )

    def test_execute_reduction(self):
        """tests execute_reduction"""
        self.mock_get_univariate_performances_for_columns.return_value = {
            "col1": 0.5, "col2": 0.3, "col3": 0.8, "col4": None, "col5": None
        }
        data = pd.DataFrame(
            {"col1": [1, None, 3, 3, 5, 9, 16, 78, 56, 13, 65, 2, 5, 6, 7],
             "col2": ["organon", None, None, None, None, None, None, None, None, None, None, None, None,
                      None, None],
             "col3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
             "col4": [datetime(2010, 1, 1)] * 15,
             "col5": [datetime(2010, 1, 1) - datetime(2005, 1, 1)] * 15,
             "target": [4, 5, 1, 0, 3, 0, 1, 1, 4, 0, 1.5, 0, 1, 3, 0]
             }
        )
        feature_reduction_settings = \
            UnivariatePerformanceFeatureReductionSettings(data=data,
                                                          target_column_name="target",
                                                          target_type=TargetType.BINARY,
                                                          performance_metric="roc_auc",
                                                          univariate_performance_threshold=0.6)
        service = UnivariatePerformanceFeatureReduction(feature_reduction_settings)
        output = service.execute()
        self.assertEqual(FeatureReductionType.UNIVARIATE_PERFORMANCE, output.feature_reduction_type)
        self.assertEqual(['col1', 'col2'], output.reduced_column_list)

    def test_execute_reduction_with_excluded_columns(self):
        """tests execute_reduction with excluded columns"""
        self.mock_get_univariate_performances_for_columns.return_value = {
           "col2": 0.3, "col3": 0.8, "col4": None, "col5": None
        }
        data = pd.DataFrame(
            {"col1": [1, None, 3, 3, 5, 9, 16, 78, 56, 13, 65, 2, 5, 6, 7],
             "col2": ["organon", None, None, None, None, None, None, None, None, None, None, None, None,
                      None, None],
             "col3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
             "col4": [datetime(2010, 1, 1)] * 15,
             "col5": [datetime(2010, 1, 1) - datetime(2005, 1, 1)] * 15,
             "target": [4, 5, 1, 0, 3, 0, 1, 1, 4, 0, 1.5, 0, 1, 3, 0]
             }
        )
        feature_reduction_settings = \
            UnivariatePerformanceFeatureReductionSettings(data=data,
                                                          target_column_name="target",
                                                          target_type=TargetType.BINARY,
                                                          performance_metric="roc_auc",
                                                          univariate_performance_threshold=0.6,
                                                          excluded_columns=["col1"])
        service = UnivariatePerformanceFeatureReduction(feature_reduction_settings)
        output = service.execute()
        self.assertEqual(FeatureReductionType.UNIVARIATE_PERFORMANCE, output.feature_reduction_type)
        self.assertEqual(['col2'], output.reduced_column_list)

    def test_if_included_columns_empty(self):
        """Test if there's no columns that can be processed"""
        data = pd.DataFrame(
            {
                "target": [4, 5, 1, 0, 3, 0, 1, 1, 4, 0, 1.5, 0, 1, 3, 0]
            })

        univariate_perf_feature_reduction_settings = \
            UnivariatePerformanceFeatureReductionSettings(data=data,
                                                          target_column_name="target",
                                                          target_type=TargetType.SCALAR,
                                                          performance_metric="r2",
                                                          univariate_performance_threshold=0.1)
        service = UnivariatePerformanceFeatureReduction(univariate_perf_feature_reduction_settings)
        output = service.execute()
        self.assertEqual(FeatureReductionType.UNIVARIATE_PERFORMANCE, output.feature_reduction_type)
        self.assertIsNone(output.reduced_column_list)
