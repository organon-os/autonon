"""Includes unittests for NullFeatureReduction"""
import unittest
import pandas as pd

from organon.ml.feature_reduction.domain.enums.feature_reduction_types import FeatureReductionType
from organon.ml.feature_reduction.domain.reductions.null_feature_reduction import NullFeatureReduction
from organon.ml.feature_reduction.settings.objects.null_feature_reduction_settings import NullFeatureReductionSettings


class NullFeatureReductionTestCase(unittest.TestCase):
    """Unittest class for NullFeatureReduction"""

    def test_execute_reduction(self):
        """Test for execute_reduction"""
        data = pd.DataFrame(
            {"col1": [1, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
             "col2": ["organon", None, None, None, None, None, None, None, None, None, None, None, None,
                      None, None],
             "col3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
             "co4": [1, None, None, 3, 4, 5, None, None, 8, 9, None, None, None, None, None]
             })

        null_feature_reduction_settings = NullFeatureReductionSettings(data=data, null_ratio_threshold=0.90)

        service = NullFeatureReduction(null_feature_reduction_settings)
        output = service.execute()
        self.assertEqual(2, len(output.reduced_column_list))
        self.assertEqual(["col1", "col2"], output.reduced_column_list)
        self.assertEqual(FeatureReductionType.NULL, output.feature_reduction_type)

    def test_execute_reduction_with_excluded_columns(self):
        """Test for execute_reduction with excluded columns"""
        data = pd.DataFrame(
            {"col1": [1, None, None, None, None, None, None, None, None, None, None, None, None, None, None],
             "col2": ["organon", None, None, None, None, None, None, None, None, None, None, None, None,
                      None, None],
             "col3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
             "co4": [1, None, None, 3, 4, 5, None, None, 8, 9, None, None, None, None, None]
             })

        null_feature_reduction_settings = NullFeatureReductionSettings(data=data, null_ratio_threshold=0.90,
                                                                       excluded_columns=["col2"])

        service = NullFeatureReduction(null_feature_reduction_settings)
        output = service.execute()
        self.assertEqual(1, len(output.reduced_column_list))
        self.assertEqual(["col1"], output.reduced_column_list)
        self.assertEqual(FeatureReductionType.NULL, output.feature_reduction_type)

    def test_if_reduced_columns_empty(self):
        """test if there's no columns to reduce"""
        data = pd.DataFrame({
            "col3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
            "co4": [1, None, None, 3, 4, 5, None, None, 8, 9, None, None, None, None, None]
        })

        null_feature_reduction_settings = NullFeatureReductionSettings(data=data, null_ratio_threshold=0.90)

        service = NullFeatureReduction(null_feature_reduction_settings)
        output = service.execute()
        self.assertEqual([], output.reduced_column_list)
        self.assertEqual(FeatureReductionType.NULL, output.feature_reduction_type)

    def test_if_included_columns_empty(self):
        """test if there's no included columns"""
        data = pd.DataFrame()

        null_feature_reduction_settings = NullFeatureReductionSettings(data=data, null_ratio_threshold=0.90)

        service = NullFeatureReduction(null_feature_reduction_settings)
        output = service.execute()
        self.assertIsNone(output.reduced_column_list)
        self.assertEqual(FeatureReductionType.NULL, output.feature_reduction_type)
