"""Includes unittests for SimilarDistributionFeatureReduction"""
import unittest

import pandas as pd

from organon.fl.core.enums.column_native_type import ColumnNativeType
from organon.ml.common.enums.target_type import TargetType
from organon.ml.feature_reduction.domain.enums.feature_reduction_types import FeatureReductionType
from organon.ml.feature_reduction.domain.reductions import similar_distribution_feature_reduction
from organon.ml.feature_reduction.domain.reductions.similar_distribution_feature_reduction import \
    SimilarDistributionFeatureReduction
from organon.ml.feature_reduction.settings.objects.similar_distribution_feature_reduction_settings import \
    SimilarDistributionFeatureReductionSettings
from organon.tests import test_helper


def _get_mock_data():
    return pd.DataFrame(
        {"col1": [1, None, 3, 3, 5, 9, 16, 78, 56, 13, 65, 2, 5, 6, 7],
         "col2": ["organon", "organon", "organon", "organon", "organon", "organon", "organon", "organon",
                  "organon", "organon", None, None, "organon",
                  None, None],
         "col3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
         "col4": [9, 10, 11, 12, 13, 14, 15, 1, 2, 3, 4, 5, 6, 7, 8],
         "col5": [10, 11, 12, 13, 1, 2, 3, 4, 5, 6, 7, 8, 9, 14, 15],
         "col6": [16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30],
         "col7": [24, 25, 26, 27, 28, 29, 30, 16, 17, 18, 19, 20, 21, 22, 23],
         "col8": [0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0],
         "col9": [0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1],
         "target": [0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0]
         })


def _get_col_types_dict():
    col_types_dict = {f"col{i + 1}": ColumnNativeType.Numeric for i in range(9) if i + 1 != 2}
    col_types_dict["col2"] = ColumnNativeType.String
    col_types_dict["target"] = ColumnNativeType.Numeric
    return col_types_dict


class SimilarDistributionFeatureReductionTestCase(unittest.TestCase):
    """Unittest class for SimilarDistributionFeatureReduction"""

    def setUp(self) -> None:
        module_name = similar_distribution_feature_reduction.__name__
        self.data = _get_mock_data()
        col_types = _get_col_types_dict()
        self.mock_get_column_native_type = test_helper.get_mock(self, module_name + ".get_column_native_type")
        self.mock_get_column_native_type.side_effect = lambda frame, col: col_types[col]
        self.mock_get_reduced_columns_via_performance = test_helper.get_mock(
            self, module_name + ".get_reduced_columns_via_performance")

    def test_execute_reduction(self):
        """Test for execute_reduction without target column"""
        similar_distribution_feature_reduction_settings = SimilarDistributionFeatureReductionSettings(data=self.data,
                                                                                                      nunique_count=5)

        service = SimilarDistributionFeatureReduction(similar_distribution_feature_reduction_settings)
        output = service.execute()
        self.assertEqual(FeatureReductionType.SIMILAR_DISTRIBUTION, output.feature_reduction_type)
        self.assertEqual(["col4", "col5", "col7"], output.reduced_column_list)

    def test_execute_reduction_with_excluded_columns(self):
        """Test for execute_reduction with excluded columns"""
        similar_distribution_feature_reduction_settings = SimilarDistributionFeatureReductionSettings(data=self.data,
                                                                                                      nunique_count=5,
                                                                                                      excluded_columns=[
                                                                                                          "col4",
                                                                                                          "col5"])

        service = SimilarDistributionFeatureReduction(similar_distribution_feature_reduction_settings)
        output = service.execute()
        self.assertEqual(FeatureReductionType.SIMILAR_DISTRIBUTION, output.feature_reduction_type)
        self.assertEqual(["col7"], output.reduced_column_list)

    def test_execute_reduction_with_binary_target(self):
        """Test for execute_reduction with binary target column """
        similar_distribution_feature_reduction_settings = \
            SimilarDistributionFeatureReductionSettings(data=self.data,
                                                        target_column_name="target",
                                                        target_type=TargetType.BINARY,
                                                        performance_metric="roc_auc",
                                                        nunique_count=5)
        self.mock_get_reduced_columns_via_performance.return_value = (["col3", "col4", "col6"],
                                                                      {"col3": 0.5, "col4": 0.2, "col5": 0.8,
                                                                       "col6": 0.6, "col7": 0.9})
        service = SimilarDistributionFeatureReduction(similar_distribution_feature_reduction_settings)
        output = service.execute()
        self.assertCountEqual([["col3", "col4", "col5"], ["col6", "col7"]],
                              self.mock_get_reduced_columns_via_performance.call_args.args[4])
        self.assertEqual(FeatureReductionType.SIMILAR_DISTRIBUTION, output.feature_reduction_type)
        self.assertCountEqual(["col3", "col4", "col6"], output.reduced_column_list)

    def test_execute_reduction_with_univariate_perf(self):
        """Test for execute_reduction with univariate performance """
        similar_distribution_feature_reduction_settings = \
            SimilarDistributionFeatureReductionSettings(data=self.data,
                                                        target_column_name="target",
                                                        target_type=TargetType.BINARY,
                                                        performance_metric="roc_auc",
                                                        nunique_count=5,
                                                        univariate_performance_result={"col3": 0.5, "col4": 0.2})
        self.mock_get_reduced_columns_via_performance.return_value = (["col3", "col4", "col6"],
                                                                      {"col5": 0.8, "col6": 0.6, "col7": 0.9})
        service = SimilarDistributionFeatureReduction(similar_distribution_feature_reduction_settings)
        output = service.execute()
        self.assertCountEqual([["col3", "col4", "col5"], ["col6", "col7"]],
                              self.mock_get_reduced_columns_via_performance.call_args.args[4])
        self.assertEqual(FeatureReductionType.SIMILAR_DISTRIBUTION, output.feature_reduction_type)
        self.assertEqual(["col3", "col4", "col6"], output.reduced_column_list)

    def test_if_included_columns_empty(self):
        """Test if there's no columns that can be processed"""
        data = pd.DataFrame(
            {
                "target": [0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0]
            })

        similar_distribution_feature_reduction_settings = \
            SimilarDistributionFeatureReductionSettings(data=data,
                                                        target_column_name="target",
                                                        target_type=TargetType.BINARY,
                                                        performance_metric="roc_auc",
                                                        nunique_count=5)
        service = SimilarDistributionFeatureReduction(similar_distribution_feature_reduction_settings)
        output = service.execute()
        self.assertEqual(FeatureReductionType.SIMILAR_DISTRIBUTION, output.feature_reduction_type)
        self.assertIsNone(output.reduced_column_list)
