"""Includes unittests for StabilityFeatureReduction"""
import unittest
import pandas as pd

from organon.ml.feature_reduction.domain.enums.feature_reduction_types import FeatureReductionType
from organon.ml.feature_reduction.domain.reductions.stability_feature_reduction import StabilityFeatureReduction
from organon.ml.feature_reduction.settings.objects.stability_feature_reduction_settings import \
    StabilityFeatureReductionSettings


class StabilityFeatureReductionTestCase(unittest.TestCase):
    """Unittest class for StabilityFeatureReduction"""

    def test_execute_reduction(self):
        """Test for execute_reduction"""
        data = pd.DataFrame(
            {"col1": [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
             "col2": ["organon", "organon", "organon", "organon", "organon", "organon",
                      "organon", "organon", "organon", "organon", "organon", "organon", "organon", "organon",
                      "organon"],
             "col3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
             "co4": [1, None, None, 3, 4, 5, None, None, 8, 9, None, None, None, None, None]
             })

        stability_feature_reduction_settings = StabilityFeatureReductionSettings(data=data)

        service = StabilityFeatureReduction(stability_feature_reduction_settings)
        output = service.execute()
        self.assertEqual(2, len(output.reduced_column_list))
        self.assertEqual(["col1", "col2"], output.reduced_column_list)
        self.assertEqual(FeatureReductionType.STABILITY, output.feature_reduction_type)

    def test_execute_reduction_with_excluded_columns(self):
        """Test for execute_reduction with exluded columns"""
        data = pd.DataFrame(
            {"col1": [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
             "col2": ["organon", "organon", "organon", "organon", "organon", "organon",
                      "organon", "organon", "organon", "organon", "organon", "organon", "organon", "organon",
                      "organon"],
             "col3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
             "co4": [1, None, None, 3, 4, 5, None, None, 8, 9, None, None, None, None, None]
             })

        stability_feature_reduction_settings = StabilityFeatureReductionSettings(data=data,
                                                                                 excluded_columns=["col1", "col2"])

        service = StabilityFeatureReduction(stability_feature_reduction_settings)
        output = service.execute()
        self.assertEqual(0, len(output.reduced_column_list))
        self.assertEqual([], output.reduced_column_list)
        self.assertEqual(FeatureReductionType.STABILITY, output.feature_reduction_type)

    def test_if_reduced_columns_empty(self):
        """test if there's no columns to reduce"""
        data = pd.DataFrame(
            {
                "col3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
                "co4": [1, None, None, 3, 4, 5, None, None, 8, 9, None, None, None, None, None]
            })
        stability_feature_reduction_settings = StabilityFeatureReductionSettings(data=data)

        service = StabilityFeatureReduction(stability_feature_reduction_settings)
        output = service.execute()
        self.assertEqual([], output.reduced_column_list)
        self.assertEqual(FeatureReductionType.STABILITY, output.feature_reduction_type)

    def test_if_included_columns_empty(self):
        """test if there's no included columns"""
        data = pd.DataFrame()
        stability_feature_reduction_settings = StabilityFeatureReductionSettings(data=data)

        service = StabilityFeatureReduction(stability_feature_reduction_settings)
        output = service.execute()
        self.assertIsNone(output.reduced_column_list)
        self.assertEqual(FeatureReductionType.STABILITY, output.feature_reduction_type)
