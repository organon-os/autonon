"""Includes unittests for HighCorrelatedFeatureReduction"""
import unittest

import pandas as pd

from organon.fl.core.enums.column_native_type import ColumnNativeType
from organon.ml.common.enums.target_type import TargetType
from organon.ml.feature_reduction.domain.enums.feature_reduction_types import FeatureReductionType
from organon.ml.feature_reduction.domain.reductions import high_correlated_feature_reduction
from organon.ml.feature_reduction.domain.reductions.high_correlated_feature_reduction import \
    HighCorrelatedFeatureReduction
from organon.ml.feature_reduction.settings.objects.high_correlated_feature_reduction_settings import \
    HighCorrelatedFeatureReductionSettings
from organon.tests import test_helper


def _get_mock_data():
    return pd.DataFrame(
        {"col1": [1, None, 3, 3, 5, 9, 16, 78, 56, 13, 65, 2, 5, 6, 7],
         "col2": ["organon", None, None, None, None, None, None, None, None, None, None, None, None,
                  None, None],
         "col3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
         "col4": [2, 4, 6, 8, 18, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30],
         "col5": [3, 5, 7, 9, 11, 16, 15, 17, 19, 21, 23, 25, 27, 29, 31],
         "col6": [16, 17, 18, 19, 20, 32, 22, 23, 24, 25, 26, 27, 28, 29, 30],
         "col7": [16, 17, 18, 19, 20, 21, 22, 42, 24, 25, 26, 27, 28, 29, 30],
         "col8": [0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0],
         "col9": [0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1],
         })


def _get_col_types_dict():
    col_types_dict = {f"col{i + 1}": ColumnNativeType.Numeric for i in range(9) if i + 1 != 2}
    col_types_dict["col2"] = ColumnNativeType.String
    col_types_dict["target"] = ColumnNativeType.Numeric
    return col_types_dict


class HighCorrelatedFeatureReductionTestCase(unittest.TestCase):
    """Unittest class for HighCorrelatedFeatureReduction"""

    def setUp(self) -> None:
        module_name = high_correlated_feature_reduction.__name__
        self.data = _get_mock_data()
        col_types = _get_col_types_dict()
        self.mock_get_column_native_type = test_helper.get_mock(self, module_name + ".get_column_native_type")
        self.mock_get_column_native_type.side_effect = lambda frame, col: col_types[col]
        self.mock_get_univariate_performances = test_helper.get_mock(
            self, module_name + ".get_univariate_performances_for_columns")
        self.mock_random = test_helper.get_mock(self, module_name + ".random")

    def test_execute_reduction_without_target(self):
        """Test for execute_reduction without target column"""
        high_corr_feature_reduction_settings = HighCorrelatedFeatureReductionSettings(data=self.data,
                                                                                      correlation_threshold=0.9)
        self.mock_random.sample.side_effect = [
            ["col3", "col5"], ["col3", "col4"], ["col4", "col5"]
        ]
        service = HighCorrelatedFeatureReduction(high_corr_feature_reduction_settings)
        output = service.execute()
        all_high_corr_pairs = [call_args.args[0] for call_args in self.mock_random.sample.call_args_list]
        self.assertCountEqual([["col3", "col4"], ["col3", "col5"], ["col4", "col5"]], all_high_corr_pairs)
        self.assertEqual(FeatureReductionType.HIGH_CORRELATION, output.feature_reduction_type)
        self.assertEqual(["col3", "col4"], output.reduced_column_list)

    def test_execute_reduction_with_perf(self):
        """Test for execute_reduction with binary target column with univariate perf"""
        data = pd.DataFrame(
            {"col1": [1, None, 3, 3, 5, 9, 16, 78, 56, 13, 65, 2, 5, 6, 7],
             "col2": ["organon", None, None, None, None, None, None, None, None, None, None, None, None,
                      None, None],
             "col3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
             "col4": [2, 4, 6, 8, 18, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30],
             "col5": [3, 5, 7, 9, 11, 16, 15, 17, 19, 21, 23, 25, 27, 29, 31],
             "col6": [16, 17, 18, 19, 20, 32, 22, 23, 24, 25, 26, 27, 28, 29, 30],
             "col7": [16, 17, 18, 19, 20, 21, 22, 42, 24, 25, 26, 27, 28, 29, 30],
             "col8": [0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0],
             "col9": [0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1],
             "target": [4, 5, 1, 0, 3, 0, 1, 1, 4, 0, 1.5, 0, 1, 3, 0]
             })

        perf = {"col1": 0.4, "col3": 0.6, "col4": 0.2, "col5": 0.8, "col6": 0.1, "col7": 0.6, "col8": 0.3, "col9": 0.2}
        self.mock_get_univariate_performances.side_effect = \
            lambda frame, cols, *args, **kwargs: {cols[0]: perf[cols[0]]}
        high_corr_feature_reduction_settings = \
            HighCorrelatedFeatureReductionSettings(data,
                                                   target_column_name="target",
                                                   target_type=TargetType.SCALAR,
                                                   performance_metric="r2",
                                                   correlation_threshold=0.7)

        service = HighCorrelatedFeatureReduction(high_corr_feature_reduction_settings)
        output = service.execute()
        self.assertEqual(FeatureReductionType.HIGH_CORRELATION, output.feature_reduction_type)
        self.assertEqual(['col3', 'col4', 'col7', 'col6', 'col9'], output.reduced_column_list)

    def test_execute_with_exluded_columns(self):
        """Test for execute_reduction with excluded columns"""
        data = pd.DataFrame(
            {"col1": [1, None, 3, 3, 5, 9, 16, 78, 56, 13, 65, 2, 5, 6, 7],
             "col2": ["organon", None, None, None, None, None, None, None, None, None, None, None, None,
                      None, None],
             "col3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
             "col4": [2, 4, 6, 8, 18, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30],
             "col5": [3, 5, 7, 9, 11, 16, 15, 17, 19, 21, 23, 25, 27, 29, 31],
             "col6": [16, 17, 18, 19, 20, 32, 22, 23, 24, 25, 26, 27, 28, 29, 30],
             "col7": [16, 17, 18, 19, 20, 21, 22, 42, 24, 25, 26, 27, 28, 29, 30],
             "col8": [0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0],
             "col9": [0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1],
             "target": [4, 5, 1, 0, 3, 0, 1, 1, 4, 0, 1.5, 0, 1, 3, 0]
             })

        perf = {"col1": 0.4, "col3": 0.6, "col4": 0.2, "col5": 0.8, "col6": 0.1, "col7": 0.6, "col8": 0.3, "col9": 0.2}
        self.mock_get_univariate_performances.side_effect = \
            lambda frame, cols, *args, **kwargs: {cols[0]: perf[cols[0]]}
        high_corr_feature_reduction_settings = \
            HighCorrelatedFeatureReductionSettings(data,
                                                   target_column_name="target",
                                                   excluded_columns=["col4", "col6"],
                                                   target_type=TargetType.SCALAR,
                                                   performance_metric="r2",
                                                   correlation_threshold=0.7)

        service = HighCorrelatedFeatureReduction(high_corr_feature_reduction_settings)
        output = service.execute()
        self.assertEqual(FeatureReductionType.HIGH_CORRELATION, output.feature_reduction_type)
        self.assertEqual(['col3', 'col7', 'col9'], output.reduced_column_list)

    def test_execute_reduction_with_perf_existing_perf_results(self):
        """Test for execute_reduction with binary target column with univariate performance and
        already existing performances"""
        data = pd.DataFrame(
            {"col1": [1, None, 3, 3, 5, 9, 16, 78, 56, 13, 65, 2, 5, 6, 7],
             "col2": ["organon", None, None, None, None, None, None, None, None, None, None, None, None,
                      None, None],
             "col3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
             "col4": [2, 4, 6, 8, 18, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30],
             "col5": [3, 5, 7, 9, 11, 16, 15, 17, 19, 21, 23, 25, 27, 29, 31],
             "col6": [16, 17, 18, 19, 20, 32, 22, 23, 24, 25, 26, 27, 28, 29, 30],
             "col7": [16, 17, 18, 19, 20, 21, 22, 42, 24, 25, 26, 27, 28, 29, 30],
             "col8": [0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0],
             "col9": [0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1],
             "target": [4, 5, 1, 0, 3, 0, 1, 1, 4, 0, 1.5, 0, 1, 3, 0]
             })

        perf = {"col1": 0.4, "col3": 0.6, "col4": 0.2, "col5": 0.8}
        new_perf = {"col6": 0.1, "col7": 0.6, "col8": 0.3, "col9": 0.2}
        self.mock_get_univariate_performances.side_effect = \
            lambda frame, cols, *args, **kwargs: {cols[0]: new_perf[cols[0]]}
        high_corr_feature_reduction_settings = \
            HighCorrelatedFeatureReductionSettings(data=data,
                                                   target_column_name="target",
                                                   target_type=TargetType.SCALAR,
                                                   performance_metric="r2",
                                                   correlation_threshold=0.7,
                                                   univariate_performance_result=perf)

        service = HighCorrelatedFeatureReduction(high_corr_feature_reduction_settings)
        output = service.execute()
        self.assertEqual(FeatureReductionType.HIGH_CORRELATION, output.feature_reduction_type)
        self.assertEqual(['col3', 'col4', 'col7', 'col6', 'col9'], output.reduced_column_list)

    def test_protected_columns_random_selection(self):
        """Test if protected columns logic is working with random selection"""

        # random function will always return these column values
        self.mock_random.sample.side_effect = [['col4', 'col3'], ['col3', 'col1'], ['col1', 'col4']]
        data = pd.DataFrame(
            {
                "col1": [2, 4, 6, 8, 18, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30],
                "col3": [16, 17, 18, 19, 20, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
                "col4": [16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]
            })

        high_corr_feature_reduction_settings = HighCorrelatedFeatureReductionSettings(data=data,
                                                                                      correlation_threshold=0.9)

        service = HighCorrelatedFeatureReduction(high_corr_feature_reduction_settings)
        output = service.execute()

        self.assertEqual(FeatureReductionType.HIGH_CORRELATION, output.feature_reduction_type)
        self.assertEqual(['col4'], output.reduced_column_list)
        all_high_corr_pairs = [call_args.args[0] for call_args in self.mock_random.sample.call_args_list]
        self.assertCountEqual([["col3", "col4"], ["col1", "col3"], ["col1", "col4"]], all_high_corr_pairs)

    def test_protected_columns_with_perf(self):
        """Test if protected columns logic works with perf"""
        data = pd.DataFrame(
            {
                "col1": [2, 4, 6, 8, 18, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30],
                "col3": [16, 17, 18, 19, 20, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
                "col4": [16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30],
                "target": [4, 5, 1, 0, 3, 0, 1, 1, 4, 0, 1.5, 0, 1, 3, 0]
            })
        perf = {"col2": 0.2, "col3": 0.6, "col4": 0.2}
        high_corr_feature_reduction_settings = \
            HighCorrelatedFeatureReductionSettings(data=data,
                                                   target_column_name="target",
                                                   target_type=TargetType.BINARY,
                                                   performance_metric="roc_auc",
                                                   correlation_threshold=0.9,
                                                   univariate_performance_result=perf)
        protected_columns = ['col3']

        service = HighCorrelatedFeatureReduction(high_corr_feature_reduction_settings)
        new_perf = {"col1": 0.8}
        self.mock_get_univariate_performances.side_effect = \
            lambda frame, cols, *args, **kwargs: {cols[0]: new_perf[cols[0]]}
        output = service.execute()
        self.assertEqual(FeatureReductionType.HIGH_CORRELATION, output.feature_reduction_type)
        self.assertEqual(set(output.reduced_column_list).issuperset(protected_columns), False)
        self.assertEqual(['col4'], output.reduced_column_list)

    def test_included_if_columns_empty(self):
        """Test if there's no columns that can be processed"""
        data = pd.DataFrame(
            {
             "target": [4, 5, 1, 0, 3, 0, 1, 1, 4, 0, 1.5, 0, 1, 3, 0]
             })

        high_corr_feature_reduction_settings = \
            HighCorrelatedFeatureReductionSettings(data=data,
                                                   target_column_name="target",
                                                   target_type=TargetType.SCALAR,
                                                   performance_metric="r2",
                                                   correlation_threshold=0.7)
        service = HighCorrelatedFeatureReduction(high_corr_feature_reduction_settings)
        output = service.execute()
        self.assertIsNone(output.reduced_column_list)
        self.assertEqual(FeatureReductionType.HIGH_CORRELATION, output.feature_reduction_type)
