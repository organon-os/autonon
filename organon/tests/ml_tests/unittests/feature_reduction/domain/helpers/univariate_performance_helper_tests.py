"""Includes unit test class for univariate_performance_helper"""
import unittest
from datetime import datetime
from typing import Dict
from unittest.mock import patch, MagicMock

import lightgbm
import numpy as np
import pandas as pd

from organon.fl.core.enums.column_native_type import ColumnNativeType
from organon.ml.common.enums.target_type import TargetType
from organon.ml.feature_reduction.domain.helpers import univariate_performance_helper
from organon.ml.feature_reduction.domain.helpers.univariate_performance_helper import \
    get_univariate_performances_for_columns, get_reduced_columns_via_performance
from organon.tests import test_helper


def _get_side_effect_for_cross_val_score(scores: Dict[str, np.array]):
    def _se(model, frame: pd.DataFrame, *args, **kwargs):
        # pylint: disable=unused-argument
        col = frame.columns[0]
        if col in scores:
            return scores[frame.columns[0]]
        raise AssertionError("cross_val_score called with unexpected value")

    return _se


def _get_mock_data():
    return pd.DataFrame(
        {"col1": [1, None, 3, 3, 5, 9, 16, 78, 56, 13, 65, 2, 5, 6, 7],
         "col2": ["organon", None, None, None, None, None, None, None, None, None, None, None, None,
                  None, None],
         "col3": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
         "col4": [datetime(2010, 1, 1)] * 15,
         "col5": [datetime(2010, 1, 1) - datetime(2005, 1, 1)] * 15,
         "target": [4, 5, 1, 0, 3, 0, 1, 1, 4, 0, 1.5, 0, 1, 3, 0]
         }
    )


def _get_col_types_dict():
    col_types_dict = {
        "col1": ColumnNativeType.Numeric,
        "col2": ColumnNativeType.String,
        "col3": ColumnNativeType.Numeric,
        "col4": ColumnNativeType.Date,
        "col5": ColumnNativeType.Other
    }
    return col_types_dict


class UnivariatePerformanceHelperTestCase(unittest.TestCase):
    """Unit test class for univariate_performance_helper"""

    def setUp(self) -> None:
        self.module_name = univariate_performance_helper.__name__

        mock_lgbm = MagicMock(wraps=lightgbm)
        self.mock_lgbm_clf = MagicMock(name="LGBMClassifier")
        self.mock_lgbm_reg = MagicMock(name="LGBMRegressor")
        mock_lgbm.LGBMClassifier = self.mock_lgbm_clf
        mock_lgbm.LGBMRegressor = self.mock_lgbm_reg

        # required since LGBM classes are imported inside method
        self.lgbm_patcher = patch.dict("sys.modules", {"lightgbm": mock_lgbm})
        self.lgbm_patcher.start()

        # self.mock_lgbm_clf = test_helper.get_mock(self, self.module_name + "." + LGBMClassifier.__name__)
        # self.mock_lgbm_reg = test_helper.get_mock(self, self.module_name + "." + LGBMRegressor.__name__)

        self.mock_cross_val = test_helper.get_mock(self, self.module_name + ".cross_val_score")
        col_types = _get_col_types_dict()
        self.mock_get_column_native_type = test_helper.get_mock(self, self.module_name + ".get_column_native_type")
        self.mock_get_column_native_type.side_effect = lambda frame, col: col_types[col]

        self.data = _get_mock_data()

    def tearDown(self) -> None:
        self.lgbm_patcher.stop()

    def test_get_reduced_columns_via_performance_initial_perf_none(self):
        """tests get_reduced_columns_via_performance when no performance_result exists"""
        mock_get_univariate_performances_for_columns = test_helper.get_mock(
            self, self.module_name + ".get_univariate_performances_for_columns")
        mock_get_univariate_performances_for_columns.return_value = {"col1": 0.5, "col2": 0.6, "col3": 0.8,
                                                                     "col4": None}
        reduced_cols, new_col_perfs = get_reduced_columns_via_performance(self.data, "target", TargetType.BINARY,
                                                                          "r2", [["col1", "col2"], ["col2", "col3"],
                                                                                 ["col2", "col4"]],
                                                                          univariate_performance_results=None)
        self.assertCountEqual(["col1", "col2", "col3", "col4"],
                              mock_get_univariate_performances_for_columns.call_args.args[1])
        self.assertCountEqual(["col1", "col2"], reduced_cols)
        self.assertEqual({"col1": 0.5, "col2": 0.6, "col3": 0.8, "col4": None}, new_col_perfs)

    def test_get_reduced_columns_via_performance_initial_perf_not_none(self):
        """tests get_reduced_columns_via_performance when performance_result not empty"""
        mock_get_univariate_performances_for_columns = test_helper.get_mock(
            self, self.module_name + ".get_univariate_performances_for_columns")
        mock_get_univariate_performances_for_columns.return_value = {"col1": 0.5, "col3": 0.8,
                                                                     "col4": None}
        reduced_cols, new_col_perfs = get_reduced_columns_via_performance(self.data, "target", TargetType.BINARY,
                                                                          "r2", [["col1", "col2"], ["col2", "col3"],
                                                                                 ["col2", "col4"]],
                                                                          univariate_performance_results={"col2": 0.6})
        self.assertCountEqual(["col1", "col3", "col4"],
                              mock_get_univariate_performances_for_columns.call_args.args[1])
        self.assertCountEqual(["col1", "col2"], reduced_cols)
        self.assertEqual({"col1": 0.5, "col3": 0.8, "col4": None}, new_col_perfs)

    def test_get_reduced_columns_via_performance_no_new_cols_to_calculates(self):
        """tests get_reduced_columns_via_performance when performance_results exist for all columns"""
        mock_get_univariate_performances_for_columns = test_helper.get_mock(
            self, self.module_name + ".get_univariate_performances_for_columns")
        reduced_cols, new_col_perfs = get_reduced_columns_via_performance(self.data, "target", TargetType.BINARY,
                                                                          "r2", [["col1", "col3"], ["col1", "col4"]],
                                                                          univariate_performance_results={
                                                                              "col1": 0.6, "col3": 0.4, "col4": 0.9
                                                                          })
        mock_get_univariate_performances_for_columns.assert_not_called()
        self.assertCountEqual(["col1", "col3"], reduced_cols)
        self.assertIsNone(new_col_perfs)

    def test_get_reduced_columns_via_performance_all_scores_none_for_group(self):
        """tests get_reduced_columns_via_performance when performance_results are None for all columns in a group"""
        mock_get_univariate_performances_for_columns = test_helper.get_mock(
            self, self.module_name + ".get_univariate_performances_for_columns")
        mock_get_univariate_performances_for_columns.return_value = {"col2": None}
        reduced_cols, new_col_perfs = get_reduced_columns_via_performance(self.data, "target", TargetType.BINARY,
                                                                          "r2", [["col1", "col3"], ["col1", "col2"],
                                                                                 ["col1", "col4", "col5"]],
                                                                          univariate_performance_results={
                                                                              "col1": None, "col3": None, "col4": 0.9,
                                                                              "col5": 0.3
                                                                          })
        self.assertCountEqual(["col2"], mock_get_univariate_performances_for_columns.call_args.args[1])
        self.assertCountEqual(["col5"], reduced_cols)
        self.assertEqual({"col2": None}, new_col_perfs)

    def test_get_univariate_performances_for_columns_with_binary_target(self):
        """Tests get_univariate_performances_for_columns with binary target column."""
        data = self.data
        scores_dict = {
            "col1": np.array([0.4, 0.5, 0.6]),
            "col2": np.array([0.2, 0.2, 0.5]),
            "col3": np.array([0.1, 0.1, 0.1]),
        }
        self.mock_cross_val.side_effect = _get_side_effect_for_cross_val_score(scores_dict)
        cols_to_calc = [col for col in data.columns if col != "target"]
        perf_dict = get_univariate_performances_for_columns(
            data, cols_to_calc, "target", TargetType.BINARY, "r2")
        self._assert_perf_dict({
            "col1": 0.5, "col2": 0.3, "col3": 0.1, "col4": None, "col5": None
        }, perf_dict)
        for call_args in self.mock_cross_val.call_args_list:
            self.assertEqual(self.mock_lgbm_clf.return_value, call_args.args[0])

        cvs_call_df = next(args_list.args[1] for args_list in self.mock_cross_val.call_args_list
                           if args_list.args[1].columns[0] == "col2")
        self.assertEqual("category", cvs_call_df["col2"].dtype.name)

    def test_get_univariate_performances_for_columns_with_multiclass_target(self):
        """Tests get_univariate_performances_for_columns with multiclass target column."""
        data = self.data
        scores_dict = {
            "col1": np.array([0.4, 0.5, 0.6]),
            "col2": np.array([0.2, 0.2, 0.5]),
            "col3": np.array([0.1, 0.1, 0.1]),
        }
        self.mock_cross_val.side_effect = _get_side_effect_for_cross_val_score(scores_dict)
        cols_to_calc = [col for col in data.columns if col != "target"]
        perf_dict = get_univariate_performances_for_columns(
            data, cols_to_calc, "target", TargetType.MULTICLASS, "r2")
        self._assert_perf_dict({
            "col1": 0.5, "col2": 0.3, "col3": 0.1, "col4": None, "col5": None
        }, perf_dict)
        for call_args in self.mock_cross_val.call_args_list:
            self.assertEqual(self.mock_lgbm_clf.return_value, call_args.args[0])

        cvs_call_df = next(args_list.args[1] for args_list in self.mock_cross_val.call_args_list
                           if args_list.args[1].columns[0] == "col2")
        self.assertEqual("category", cvs_call_df["col2"].dtype.name)

    def test_get_univariate_performances_for_columns_with_scalar_target(self):
        """Tests get_univariate_performances_for_columns with scalar target column."""
        data = self.data
        scores_dict = {
            "col1": np.array([0.4, 0.5, 0.6]),
            "col2": np.array([0.2, 0.2, 0.5]),
            "col3": np.array([0.1, 0.1, 0.1]),
        }
        self.mock_cross_val.side_effect = _get_side_effect_for_cross_val_score(scores_dict)

        cols_to_calc = [col for col in data.columns if col != "target"]
        perf_dict = get_univariate_performances_for_columns(
            data, cols_to_calc, "target", TargetType.SCALAR, "r2")

        self._assert_perf_dict({
            "col1": 0.5, "col2": 0.3, "col3": 0.1, "col4": None, "col5": None
        }, perf_dict)
        for call_args in self.mock_cross_val.call_args_list:
            self.assertEqual(self.mock_lgbm_reg.return_value, call_args.args[0])

        cvs_call_df = next(args_list.args[1] for args_list in self.mock_cross_val.call_args_list
                           if args_list.args[1].columns[0] == "col2")
        self.assertEqual("category", cvs_call_df["col2"].dtype.name)

    def _assert_perf_dict(self, expected: Dict[str, float], actual: Dict[str, float]):
        self.assertEqual(len(expected), len(actual))
        for col, score in expected.items():
            if score is not None:
                np.testing.assert_almost_equal(score, actual[col])
            else:
                self.assertIsNone(actual[col])


if __name__ == '__main__':
    unittest.main()
