"""Includes tests for FeatureReductionUserInputService"""
import unittest
from unittest.mock import patch

import pandas as pd

from organon.ml.common.enums.target_type import TargetType
from organon.ml.feature_reduction.domain.enums.feature_reduction_types import FeatureReductionType
from organon.ml.feature_reduction.settings import feature_reduction_user_input_service
from organon.ml.feature_reduction.settings.feature_reduction_user_input_service import FeatureReductionUserInputService
from organon.ml.feature_reduction.settings.objects.high_correlated_feature_reduction_settings import \
    HighCorrelatedFeatureReductionSettings
from organon.ml.feature_reduction.settings.objects.null_feature_reduction_settings import NullFeatureReductionSettings
from organon.ml.feature_reduction.settings.objects.similar_distribution_feature_reduction_settings import \
    SimilarDistributionFeatureReductionSettings
from organon.ml.feature_reduction.settings.objects.stability_feature_reduction_settings import \
    StabilityFeatureReductionSettings
from organon.ml.feature_reduction.settings.objects.univariate_performance_feature_reduction_settings import \
    UnivariatePerformanceFeatureReductionSettings
from organon.ml.feature_reduction.settings.objects.user_feature_reduction_settings import UserFeatureReductionSettings


def _get_enum_side_effect(val, enum_type):
    if val is None:
        return val
    return enum_type[val]


class FeatureReductionUserInputServiceTestCase(unittest.TestCase):
    """Unittest class for FeatureReductionUserInputService"""

    def setUp(self) -> None:
        get_enum_patcher = patch(feature_reduction_user_input_service.__name__ + ".get_enum",
                                 side_effect=_get_enum_side_effect)
        get_enum_patcher.start()
        self.addCleanup(get_enum_patcher.stop)
        self.input_service = FeatureReductionUserInputService()

    def test_get_stability_feature_reduction_settings_data_is_none(self):
        """Test for get_stability_feature_reduction_settings which data is  None"""
        with self.assertRaisesRegex(ValueError, "Data should be given"):
            self.input_service.get_stability_feature_reduction_settings(None)

    def test_get_stability_feature_reduction_settings(self):
        """Test for get_stability_feature_reduction_settings"""
        data = pd.DataFrame([1, 2, 3])
        setting = self.input_service.get_stability_feature_reduction_settings(data)
        self.assertTrue(data.equals(setting.data))

    def test_get_null_feature_reduction_settings_data_is_none(self):
        """Test for get_null_feature_reduction_settings which data is  None"""
        with self.assertRaisesRegex(ValueError, "Data should be given"):
            self.input_service.get_null_feature_reduction_settings(None)

    def test_get_null_feature_reduction_settings_default_threshold(self):
        """Test for get_null_feature_reduction_settings with default null_ratio_threshold"""
        data = pd.DataFrame([1, 2, 3])
        setting = self.input_service.get_null_feature_reduction_settings(data)
        self.assertEqual(0.99, setting.null_ratio_threshold)

    def test_get_null_feature_reduction_settings_given_threshold(self):
        """Test for get_null_feature_reduction_settings which with given null_ratio_threshold"""
        data = pd.DataFrame([1, 2, 3])
        setting = self.input_service.get_null_feature_reduction_settings(data, 0.5)
        self.assertEqual(0.5, setting.null_ratio_threshold)

    def test_get_high_correlated_feature_reduction_settings_data_is_none(self):
        """Test for get_high_correlated_feature_reduction_settings which data is  None"""
        with self.assertRaisesRegex(ValueError, "Data should be given"):
            self.input_service.get_high_correlated_feature_reduction_settings(None)

    def test_get_high_correlated_feature_reduction_settings_with_target_and_no_target_type(self):
        """Test for get_high_correlated_feature_reduction_settings with no target_type"""
        data = pd.DataFrame([1, 2, 3])
        with self.assertRaisesRegex(ValueError, "Type of target column should be given."):
            self.input_service.get_high_correlated_feature_reduction_settings(data, target_column_name="target")

    def test_get_high_correlated_feature_reduction_settings_with_default_threshold(self):
        """Test for get_high_correlated_feature_reduction_settings with default correlation_threshold"""
        data = pd.DataFrame([1, 2, 3])
        setting = self.input_service.get_high_correlated_feature_reduction_settings(data)
        self.assertEqual(0.99, setting.correlation_threshold)

    def test_get_high_correlated_feature_reduction_settings_with_default_metric_for_regression(self):
        """Test for get_high_correlated_feature_reduction_settings with default metric for regression"""
        data = pd.DataFrame([1, 2, 3])
        setting = self.input_service.get_high_correlated_feature_reduction_settings(data, target_column_name="target",
                                                                                    target_type="SCALAR")
        self.assertEqual("r2", setting.performance_metric)

    def test_get_high_correlated_feature_reduction_settings_with_default_metric_for_binary_classification(self):
        """Test for get_high_correlated_feature_reduction_settings with default metric for binary classification"""
        data = pd.DataFrame([1, 2, 3])
        setting = self.input_service.get_high_correlated_feature_reduction_settings(data, target_column_name="target",
                                                                                    target_type="BINARY")
        self.assertEqual("roc_auc", setting.performance_metric)

    def test_get_high_correlated_feature_reduction_settings_with_default_metric_for_multi_class_classification(self):
        """Test for get_high_correlated_feature_reduction_settings with default metric for multi class classification"""
        data = pd.DataFrame([1, 2, 3])
        setting = self.input_service.get_high_correlated_feature_reduction_settings(data, target_column_name="target",
                                                                                    target_type="MULTICLASS")
        self.assertEqual("roc_auc_ovr_weighted", setting.performance_metric)

    def test_get_similar_distribution_feature_reduction_settings_data_is_none(self):
        """Test for get_similar_distribution_feature_reduction_settings which data is  None"""
        with self.assertRaisesRegex(ValueError, "Data should be given"):
            self.input_service.get_similar_distribution_feature_reduction_settings(None)

    def test_get_similar_distribution_feature_reduction_settings_with_target_and_no_target_type(self):
        """Test for get_similar_distribution_feature_reduction_settings with no target_type"""
        data = pd.DataFrame([1, 2, 3])
        with self.assertRaisesRegex(ValueError, "Type of target column should be given."):
            self.input_service.get_similar_distribution_feature_reduction_settings(data, target_column_name="target")

    def test_get_similar_distribution_feature_reduction_settings_with_default_metric_for_regression(self):
        """Test for get_similar_distribution_feature_reduction_settings with default metric for regression"""
        data = pd.DataFrame([1, 2, 3])
        setting = self.input_service.get_similar_distribution_feature_reduction_settings(data,
                                                                                         target_column_name="target",
                                                                                         target_type=
                                                                                         TargetType.SCALAR.name)
        self.assertEqual("r2", setting.performance_metric)

    def test_get_similar_distribution_feature_reduction_settings_with_default_nunique_count(self):
        """Test for get_similar_distribution_feature_reduction_settings with default metric for regression"""
        data = pd.DataFrame([1, 2, 3])
        setting = self.input_service.get_similar_distribution_feature_reduction_settings(data,
                                                                                         target_column_name="target",
                                                                                         target_type=
                                                                                         TargetType.SCALAR.name)
        self.assertEqual(20, setting.nunique_count)

    def test_get_similar_distribution_feature_reduction_settings_with_nunique_count(self):
        """Test for get_similar_distribution_feature_reduction_settings with default metric for regression"""
        data = pd.DataFrame([1, 2, 3])
        setting = self.input_service.get_similar_distribution_feature_reduction_settings(data,
                                                                                         target_column_name="target",
                                                                                         target_type=
                                                                                         TargetType.SCALAR.name,
                                                                                         nunique_count=5)
        self.assertEqual(5, setting.nunique_count)

    def test_get_similar_distribution_feature_reduction_settings_with_default_metric_for_binary_classification(self):
        """Test for get_similar_distribution_feature_reduction_settings with default metric for binary classification"""
        data = pd.DataFrame([1, 2, 3])
        setting = self.input_service.get_similar_distribution_feature_reduction_settings(data,
                                                                                         target_column_name="target",
                                                                                         target_type=
                                                                                         TargetType.BINARY.name)
        self.assertEqual("roc_auc", setting.performance_metric)

    def test_get_similar_distribution_feature_reduction_settings_with_default_metric_for_multi_class_classification(
            self):
        """Test for get_similar_distribution_feature_reduction_settings with default
        metric for multi class classification"""
        data = pd.DataFrame([1, 2, 3])
        setting = self.input_service.get_similar_distribution_feature_reduction_settings(data,
                                                                                         target_column_name="target",
                                                                                         target_type=
                                                                                         TargetType.MULTICLASS.name)
        self.assertEqual("roc_auc_ovr_weighted", setting.performance_metric)

    def test_get_univariate_performance_feature_reduction_settings_settings_data_is_none(self):
        """Test for get_univariate_performance_feature_reduction_settings which data is  None"""
        with self.assertRaisesRegex(ValueError, "Data should be given"):
            self.input_service.get_univariate_performance_feature_reduction_settings(None, target_column_name="target",
                                                                                     target_type=
                                                                                     TargetType.SCALAR.name)

    def test_get_univariate_performance_feature_reduction_settings_settings_target_is_none(self):
        """Test for get_univariate_performance_feature_reduction_settings which data is  None"""
        data = pd.DataFrame([1, 2, 3])
        with self.assertRaisesRegex(ValueError, "Target column name and type of target column should be given."):
            self.input_service.get_univariate_performance_feature_reduction_settings(data, target_column_name=None,
                                                                                     target_type=None)

    def test_get_univariate_performance_feature_reduction_settings_with_default_metric_for_regression(self):
        """Test for get_univariate_performance_feature_reduction_settings with default metric for regression"""
        data = pd.DataFrame([1, 2, 3])
        setting = self.input_service.get_univariate_performance_feature_reduction_settings(data,
                                                                                           target_column_name="target",
                                                                                           target_type=
                                                                                           TargetType.SCALAR.name)
        self.assertEqual("r2", setting.performance_metric)

    def test_get_univariate_performance_feature_reduction_settings_with_default_metric_for_binary_classification(self):
        """Test for get_univariate_performance_feature_reduction_settings with
        default metric for binary classification"""
        data = pd.DataFrame([1, 2, 3])
        setting = self.input_service.get_univariate_performance_feature_reduction_settings(data,
                                                                                           target_column_name="target",
                                                                                           target_type=
                                                                                           TargetType.BINARY.name)
        self.assertEqual("roc_auc", setting.performance_metric)

    def test_get_univariate_performance_feature_reduction_settings_with_default_metric_for_multi_class_classification(
            self):
        """Test for get_univariate_performance_feature_reduction_settings with
         default metric for multi class classification"""
        data = pd.DataFrame([1, 2, 3])
        setting = self.input_service.get_univariate_performance_feature_reduction_settings(data,
                                                                                           target_column_name="target",
                                                                                           target_type=
                                                                                           TargetType.MULTICLASS.name)
        self.assertEqual("roc_auc_ovr_weighted", setting.performance_metric)

    def test_get_univariate_performance_feature_reduction_settings_with_default_threshold_for_regression(self):
        """Test for get_univariate_performance_feature_reduction_settings with default
         univariate_performance_threshold for regression"""
        data = pd.DataFrame([1, 2, 3])
        setting = self.input_service.get_univariate_performance_feature_reduction_settings(data,
                                                                                           target_column_name="target",
                                                                                           target_type=
                                                                                           TargetType.SCALAR.name)
        self.assertEqual(0.05, setting.univariate_performance_threshold)

    def test_get_univariate_performance_feature_reduction_settings_with_default_thresholdfor_binary_classification(
            self):
        """Test for get_univariate_performance_feature_reduction_settings with default
         univariate_performance_threshold for binary classification"""
        data = pd.DataFrame([1, 2, 3])
        setting = self.input_service.get_univariate_performance_feature_reduction_settings(data,
                                                                                           target_column_name="target",
                                                                                           target_type=
                                                                                           TargetType.BINARY.name)
        self.assertEqual(0.55, setting.univariate_performance_threshold)

    def test_get_univariate_performance_feature_reduction_settings_with_default_threshold_for_multi_class(self):
        """Test for get_univariate_performance_feature_reduction_settings with default
         univariate_performance_thresholdfor multi class classification"""
        data = pd.DataFrame([1, 2, 3])
        setting = self.input_service.get_univariate_performance_feature_reduction_settings(data,
                                                                                           target_column_name="target",
                                                                                           target_type=
                                                                                           TargetType.MULTICLASS.name)
        self.assertEqual(0.55, setting.univariate_performance_threshold)

    def test_set_performance_metric(self):
        """Test for set performance metric """

        data = pd.DataFrame([1, 2, 3])
        setting = self.input_service.get_univariate_performance_feature_reduction_settings(data,
                                                                                           target_column_name="target",
                                                                                           target_type=
                                                                                           TargetType.MULTICLASS.name,
                                                                                           performance_metric="ABC")
        self.assertEqual("ABC", setting.performance_metric)

    def tests_generate_feature_reduction_settings(self):
        """Test for generate_feature_reduction_settings"""
        settings = UserFeatureReductionSettings()
        data = pd.DataFrame([1, 2, 3])
        included_feature_reduction_type = [FeatureReductionType.UNIVARIATE_PERFORMANCE.name,
                                           FeatureReductionType.NULL.name]
        settings.data = data
        settings.target_column_name = "target"
        settings.target_type = TargetType.BINARY.name
        settings.performance_metric = "roc_auc"
        settings.included_reduction_types = included_feature_reduction_type
        settings = self.input_service.generate_feature_reduction_settings(settings)
        null_settings = NullFeatureReductionSettings(data, 0.99, excluded_columns=["target"])
        univariate_settings = UnivariatePerformanceFeatureReductionSettings(data, target_type=TargetType.BINARY,
                                                                            target_column_name="target",
                                                                            performance_metric="roc_auc",
                                                                            univariate_performance_threshold=0.55,
                                                                            random_state=42,
                                                                            excluded_columns=["target"])
        expected_settings = [null_settings, univariate_settings]
        self.assertEqual(2, len(settings))
        self.assertDictEqual(expected_settings[0].__dict__, settings[0].__dict__)
        self.assertDictEqual(expected_settings[1].__dict__, settings[1].__dict__)

    def tests_generate_feature_reduction_settings_included_is_none(self):
        """Test for generate_feature_reduction_settings"""
        settings = UserFeatureReductionSettings()
        data = pd.DataFrame([1, 2, 3])
        settings.data = data
        settings.target_column_name = "target"
        settings.target_type = TargetType.BINARY.name
        settings.performance_metric = "roc_auc"
        settings.random_state = 30
        excluded_column_names = [settings.target_column_name]
        self.input_service.generate_feature_reduction_settings(settings)
        settings = self.input_service.generate_feature_reduction_settings(settings)
        expected_settings = [NullFeatureReductionSettings(data, 0.99, excluded_columns=excluded_column_names),
                             StabilityFeatureReductionSettings(data, excluded_columns=excluded_column_names),
                             UnivariatePerformanceFeatureReductionSettings(data, target_type=TargetType.BINARY,
                                                                           target_column_name="target",
                                                                           performance_metric="roc_auc",
                                                                           univariate_performance_threshold=0.55,
                                                                           random_state=30,
                                                                           excluded_columns=excluded_column_names),
                             SimilarDistributionFeatureReductionSettings(data, target_type=TargetType.BINARY,
                                                                         target_column_name="target",
                                                                         performance_metric="roc_auc",
                                                                         nunique_count=20,
                                                                         random_state=30,
                                                                         excluded_columns=excluded_column_names),
                             HighCorrelatedFeatureReductionSettings(data, target_type=TargetType.BINARY,
                                                                    target_column_name="target",
                                                                    performance_metric="roc_auc",
                                                                    correlation_threshold=0.99,
                                                                    random_state=30,
                                                                    excluded_columns=excluded_column_names)

                             ]
        self.assertEqual(5, len(settings))
        for index, setting in enumerate(expected_settings):
            self.assertDictEqual(setting.__dict__, settings[index].__dict__)

    def tests_generate_feature_reduction_settings_included_is_none_and_no_target(self):
        """Test for generate_feature_reduction_settings"""
        settings = UserFeatureReductionSettings()
        data = pd.DataFrame([1, 2, 3])
        settings.data = data
        settings.performance_metric = "roc_auc"
        self.input_service.generate_feature_reduction_settings(settings)
        settings = self.input_service.generate_feature_reduction_settings(settings)
        expected_settings = [NullFeatureReductionSettings(data, 0.99),
                             StabilityFeatureReductionSettings(data),
                             SimilarDistributionFeatureReductionSettings(data, target_type=None,
                                                                         target_column_name=None,
                                                                         performance_metric="roc_auc",
                                                                         nunique_count=20,
                                                                         random_state=None),
                             HighCorrelatedFeatureReductionSettings(data, target_type=None,
                                                                    target_column_name=None,
                                                                    performance_metric="roc_auc",
                                                                    correlation_threshold=0.99,
                                                                    random_state=None)

                             ]
        self.assertEqual(4, len(settings))
        for index, setting in enumerate(expected_settings):
            self.assertDictEqual(setting.__dict__, settings[index].__dict__)


if __name__ == '__main__':
    unittest.main()
