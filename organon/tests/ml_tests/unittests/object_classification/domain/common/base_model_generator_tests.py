"""Includes BaseModelGeneratorTests"""
import unittest
from unittest.mock import MagicMock, patch
from organon.ml.common.enums.color_type import ColorType
from organon.ml.common.enums.pretrained_model_type import PretrainedModelType
from organon.ml.object_classification.domain.common.base_model_generator import BaseModelGenerator
from organon.ml.object_classification.domain.objects.data_augmentation_settings import DataAugmentationSettings
from organon.ml.object_classification.domain.objects.pretrained_model_settings import PretrainedModelSettings


def augmentation_se(layer_list):
    """imitates tf.keras.Sequential"""
    augmentation_mock = MagicMock()
    augmentation_mock.side_effect = lambda inputs: inputs + str(layer_list[0]) + str(layer_list[1]) + str(layer_list[2])
    return augmentation_mock


class BaseModelGeneratorTests(unittest.TestCase):
    """BaseModelGeneratorTests"""

    def setUp(self) -> None:
        self.base_model_mock = MagicMock()
        self.base_model_mock.trainable = True
        self.base_model_mock.layers = "layers"
        self.base_model_mock.return_value = self.base_model_mock

        self.tf_mock = MagicMock()
        self.sys_mod_patcher = patch.dict("sys.modules", {"tensorflow": self.tf_mock})
        self.sys_mod_patcher.start()

        self.tf_mock.keras.Input.return_value = "inputs"
        self.tf_mock.keras.applications.Xception.return_value = self.base_model_mock
        self.tf_mock.keras.applications.xception.preprocess_input.side_effect = lambda \
                inputs: inputs + "-processed"
        self.tf_mock.keras.Sequential.side_effect = augmentation_se
        self.tf_mock.keras.layers.RandomFlip.side_effect = lambda random_flip, seed: [
            random_flip, seed]
        self.tf_mock.keras.layers.RandomRotation.side_effect = lambda \
                random_rotation, seed: [random_rotation, seed]
        self.tf_mock.keras.layers.RandomZoom.side_effect = lambda random_zoom, seed: [
            random_zoom, seed]

        self.data_augmentation_settings = DataAugmentationSettings()
        self.pretrained_model_settings = PretrainedModelSettings(model=PretrainedModelType.XCEPTION, weights="imagenet")

    def test_get_base_model(self):
        """testing get_base_model function with data augmentation"""
        base_model, inputs = BaseModelGenerator. \
            get_base_model(image_size=(150, 150), color_mode=ColorType.RGB,
                           data_augmentation_settings=self.data_augmentation_settings,
                           pretrained_model_settings=self.pretrained_model_settings)

        self.assertEqual("layers", base_model.layers)
        self.assertEqual("inputs['horizontal_and_vertical', 42][0.1, 42][0.1, 42]", inputs)
        self.assertEqual("inputs['horizontal_and_vertical', 42][0.1, 42][0.1, 42]-processed",
                         self.base_model_mock.call_args[0][0])
        self.tf_mock.keras.Input.assert_called_with(shape=(150, 150, 3))

    def test_get_base_model_no_augmentation_gray_resnet(self):
        """testing get_base_model function without data augmentation and different settings"""
        self.tf_mock.keras.applications.resnet_v2.ResNet50V2 = self.base_model_mock
        self.tf_mock.keras.applications.resnet_v2.preprocess_input.side_effect = lambda \
                inputs: inputs + "-processed-resnet"
        self.pretrained_model_settings.model = PretrainedModelType.RES_NET_50_V2
        base_model, inputs = BaseModelGenerator. \
            get_base_model(image_size=(100, 100), color_mode=ColorType.GRAY,
                           pretrained_model_settings=self.pretrained_model_settings)

        self.assertEqual("layers", base_model.layers)
        self.assertEqual("inputs", inputs)
        self.assertEqual("inputs-processed-resnet",
                         self.base_model_mock.call_args[0][0])
        self.tf_mock.keras.Input.assert_called_with(shape=(100, 100, 1))

    def test_fit_model_type_error(self):
        """Testing fit with ambiguous model type"""
        self.pretrained_model_settings.model = "model"
        with self.assertRaisesRegex(ValueError, "Model type is not defined!"):
            BaseModelGenerator.get_base_model(image_size=(150, 150), color_mode=ColorType.RGB,
                                              data_augmentation_settings=self.data_augmentation_settings,
                                              pretrained_model_settings=self.pretrained_model_settings)


if __name__ == '__main__':
    unittest.main()
