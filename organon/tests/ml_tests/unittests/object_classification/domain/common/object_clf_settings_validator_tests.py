"""Includes ObjectClfSettingsValidatorTests"""
import unittest

from organon.ml.common.enums.pretrained_model_type import PretrainedModelType
from organon.ml.object_classification.domain.common import object_clf_settings_validator
from organon.ml.object_classification.domain.common.object_clf_settings_validator import ObjectClfSettingsValidator
from organon.ml.object_classification.domain.objects.object_clf_settings import ObjectClfSettings
from organon.tests import test_helper


class ObjectClfSettingsValidatorTests(unittest.TestCase):
    """ObjectClfSettingsValidatorTests"""

    def setUp(self) -> None:
        module = object_clf_settings_validator.__name__
        self.check_directory_mock = test_helper.get_mock(self, module + "." + "check_directory")
        self.settings = ObjectClfSettings("train_data_dir", batch_size=1)

    def test_fit_image_size_error_xception(self):
        """Testing fit with small image size for xception model"""
        self.settings.image_size = (50, 150)
        with self.assertRaisesRegex(ValueError,
                                    r"Using XCEPTION model, image_size \(width, height\) should be bigger than 71"):
            ObjectClfSettingsValidator.check_settings(self.settings)

    def test_fit_image_size_error_resnet(self):
        """Testing fit with small image size for res_net_50_v2 model"""
        self.settings.image_size = (30, 50)
        self.settings.pretrained_model_settings.model = PretrainedModelType.RES_NET_50_V2
        with self.assertRaisesRegex(ValueError,
                                    r"Using RES_NET_50_V2 model, image_size \(width, height\) "
                                    r"should be bigger than 32"):
            ObjectClfSettingsValidator.check_settings(self.settings)

    def test_fit_image_size_error_inception_res_net(self):
        """Testing fit with small image size for inception_res_net_50_v2 model"""
        self.settings.image_size = (70, 150)
        self.settings.pretrained_model_settings.model = PretrainedModelType.INCEPTION_RES_NET_V2
        with self.assertRaisesRegex(ValueError,
                                    r"Using INCEPTION_RES_NET_V2 model, image_size \(width, height\) "
                                    r"should be bigger than 75"):
            ObjectClfSettingsValidator.check_settings(self.settings)


if __name__ == '__main__':
    unittest.main()
