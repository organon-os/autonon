"""Includes ObjectClassificationServiceFactoryTests"""
import unittest

from organon.ml.object_classification.domain.objects.object_clf_settings import ObjectClfSettings
from organon.ml.object_classification.domain.services import object_classification_service_factory
from organon.ml.object_classification.domain.services.binary_object_classificion_service import \
    BinaryObjectClassificationService
from organon.ml.object_classification.domain.services.multiclass_object_classification_service import \
    MulticlassObjectClassificationService
from organon.ml.object_classification.domain.services.object_classification_service_factory import \
    ObjectClassificationServiceFactory
from organon.tests import test_helper


class ObjectClassificationServiceFactoryTests(unittest.TestCase):
    """ObjectClassificationServiceFactory Tests"""

    def setUp(self) -> None:
        self.settings = ObjectClfSettings(train_data_dir="train_dir")
        self.service = ObjectClassificationServiceFactory()
        module_name = object_classification_service_factory.__name__
        self.directory_helper_mock = test_helper.get_mock(self, module_name + "." + "directory_helper.exists")
        self.listdir_mock = test_helper.get_mock(self, module_name + "." + "os.listdir")

    def test_get_binary_classification_service(self) -> None:
        """Tests factory when service should be Binary"""
        self.listdir_mock.return_value = ["folder1", "folder2"]
        self.directory_helper_mock.return_value = True
        returned_service = self.service.get_classification_service(self.settings)
        self.assertIsInstance(returned_service, BinaryObjectClassificationService)
        self._assert_mock_calls()

    def test_get_multiclass_classification_service(self):
        """Tests factory when service should be Multiclass"""
        self.listdir_mock.return_value = ["folder1 ", "folder2", "folder3", "folder4"]
        self.directory_helper_mock.return_value = True
        returned_service = self.service.get_classification_service(self.settings)
        self.assertIsInstance(returned_service, MulticlassObjectClassificationService)
        self._assert_mock_calls()

    def test_train_dir_error(self):
        """Tests factory when directory does not exist"""
        self.directory_helper_mock.return_value = False
        with self.assertRaisesRegex(ValueError, "Directory: train_dir not found!"):
            _ = self.service.get_classification_service(self.settings)

    def test_one_class_error(self):
        """Tests factory when num ber of classes are below 2"""
        self.directory_helper_mock.return_value = True
        self.listdir_mock.return_value = ["folder1"]
        with self.assertRaisesRegex(ValueError, "At least 2 classes are required for classification!"):
            _ = self.service.get_classification_service(self.settings)

    def _assert_mock_calls(self):
        self.listdir_mock.assert_called_with("train_dir")
        self.directory_helper_mock.assert_called_with("train_dir")


if __name__ == '__main__':
    unittest.main()
