"""Binary Object Classification Tests"""
import itertools
import unittest
from unittest.mock import MagicMock

import numpy as np
import pandas as pd

from organon.ml.object_classification.common.object_classification_constants import ObjectClassificationConstants
from organon.ml.object_classification.domain.services import binary_object_classificion_service
from organon.ml.object_classification.domain.services.base_object_classification_service import \
    BaseObjectClassificationService
from organon.ml.object_classification.domain.services.binary_object_classificion_service import \
    BinaryObjectClassificationService
from organon.tests.ml_tests.unittests.object_classification.domain.services.object_classification_base_test_case \
    import ObjectClassificationBaseTestCase, dense_se, add_process_data_se


def data_handler_mock_se(data, settings):
    """imitates TfDatasetDataSource Object"""
    data_mock = MagicMock()
    data_mock.get_labels.return_value = np.array([[[0], [1]]])
    data_mock.data = data
    data_mock.settings = settings
    data_mock.initialize_probs_df.side_effect = lambda: pd.DataFrame(
        {ObjectClassificationConstants.LABELS_COL_NAME: list(itertools.chain.from_iterable(data_mock.get_labels()[0]))})
    add_process_data_se(data_mock)
    return data_mock


def model_se(inputs, outputs):
    """imitates tf.Model Object"""
    model_mock = MagicMock()
    model_mock.inputs = inputs
    model_mock.outputs = outputs
    model_mock.predict.side_effect = lambda dataset: np.arange(len(dataset))
    return model_mock


class BinaryObjectClassificationServiceTests(ObjectClassificationBaseTestCase, unittest.TestCase):
    """Binary Object Classification Tests"""

    def setUp(self) -> None:
        super().setUp()
        self.module_name = binary_object_classificion_service.__name__
        self.os_mock.listdir.return_value = ["cats", "dogs"]
        self.tf_mock.keras.layers.Dense.return_value.side_effect = dense_se
        self.tf_mock.keras.Model.side_effect = model_se
        self.tf_mock.keras.losses.BinaryCrossentropy.return_value = "binary_crossentropy"
        self.tf_mock.keras.metrics.BinaryAccuracy.return_value = "binary_accuracy"
        self.tf_data_handler_mock.side_effect = data_handler_mock_se
        self.get_data_source_handler_mock.side_effect = data_handler_mock_se

    def test_fit_default(self):
        """Testing binary fit with default values"""
        # pylint: disable=no-member
        self._add_image_data_from_directory_return_value()
        self.service.fit()
        self.tf_mock.keras.utils.image_dataset_from_directory.assert_called_with(directory="train_data_dir",
                                                                                 batch_size=1,
                                                                                 image_size=(150, 150),
                                                                                 shuffle=True, seed=42,
                                                                                 validation_split=0.2,
                                                                                 subset="both", color_mode="rgb",
                                                                                 label_mode="binary")
        self.service.model.compile.assert_called_with(optimizer="opt0.001",
                                                      loss="binary_crossentropy",
                                                      metrics="binary_accuracy")
        self._fit_default_assert_common()

    def test_fit_gray_with_validation_augmentation_fine_tuning(self):
        """Testing binary fit with augmentation and fine tuning settings"""
        # pylint: disable=no-member
        self._prepare_test_fit_gray_with_validation_augmentation_fine_tuning()
        self.assertEqual([({"directory": "train_data_dir",
                            "batch_size": 1,
                            "image_size": (150, 150),
                            "shuffle": True, "seed": 42,
                            "color_mode": "grayscale",
                            "label_mode": "binary"},),
                          ({"directory": "validation_dir",
                            "batch_size": 1,
                            "image_size": (150, 150),
                            "shuffle": False, "seed": 42,
                            "color_mode": "grayscale",
                            "label_mode": "binary"},)],
                         self.tf_mock.keras.utils.image_dataset_from_directory.call_args_list)

        self.assertEqual([({"optimizer": "opt0.001",
                            "loss": "binary_crossentropy",
                            "metrics": "binary_accuracy"},),
                          ({"optimizer": 'opt1e-05',
                            "loss": "binary_crossentropy",
                            "metrics": "binary_accuracy"},)],
                         self.service.model.compile.call_args_list)

        self._fit_gray_with_validation_augmentation_fine_tuning_assert_common()

    def test_fit_big_batch_size(self):
        """Testing fit when batch size is big"""
        self._add_image_data_from_directory_return_value()
        self.settings.batch_size = 50
        with self.assertRaisesRegex(ValueError, r"Batch size \(50\) can't be bigger than total sample number \(4\)"):
            self.service.fit()

    def test_predict_proba(self):
        """Testing predict_proba for function"""
        self._add_image_data_from_directory_return_value()
        self.service.fit()
        probs_df = self.service.predict_proba([("img0", 0), ("img1", 1)])
        pd.testing.assert_frame_equal(pd.DataFrame({
            ObjectClassificationConstants.LABELS_COL_NAME: np.array([0, 1]),
            ObjectClassificationConstants.PROBA_COL_NAME: np.array([0, 1])
        }), probs_df)

    def test_predict(self):
        """Testing predict function"""
        self._add_image_data_from_directory_return_value()
        self.service.fit()
        pred_df = self.service.predict([("img0", 0), ("img1", 1)])
        pd.testing.assert_frame_equal(pd.DataFrame({
            ObjectClassificationConstants.LABELS_COL_NAME: np.array([0, 1]),
            ObjectClassificationConstants.PREDICTON_COL_NAME: np.array([0, 1])
        }), pred_df)

    def test_evaluate(self):
        """Testing evaluate function"""
        self._add_image_data_from_directory_return_value()
        self._mock_metrics()
        self.service.fit()
        evaluation_metrics, _ = self.service.evaluate([("img0", 0), ("img1", 1)], return_df=False)
        self.assertDictEqual({'accuracy': 2, 'recall': 1, 'f1': 0, 'precision': 2}, evaluation_metrics)

    def test_evaluate_return_df(self):
        """Testing evaluate function with result df"""
        self._add_image_data_from_directory_return_value()
        self._mock_metrics()
        self.service.fit()
        evaluation_metrics, result_df = self.service.evaluate([("img0", 0), ("img1", 1)], return_df=True)
        self.assertDictEqual({'accuracy': 2, 'recall': 1, 'f1': 0, 'precision': 2}, evaluation_metrics)
        pd.testing.assert_frame_equal(pd.DataFrame({
            ObjectClassificationConstants.LABELS_COL_NAME: np.array([0, 1]),
            ObjectClassificationConstants.PREDICTON_COL_NAME: np.array([0, 1])
        }), result_df)

    def _get_service(self) -> BaseObjectClassificationService:
        return BinaryObjectClassificationService(self.settings)


if __name__ == '__main__':
    unittest.main()
