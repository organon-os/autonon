"""Common tests for Binary and Multiclass Object Classifications"""
import unittest
from unittest.mock import MagicMock, patch

from organon.ml.common.enums.color_type import ColorType
from organon.ml.common.enums.optimizer_type import OptimizerType
from organon.ml.object_classification.domain.data_sources.tf_dataset_data_source_handler import \
    TfDatasetDataSourceHandler
from organon.ml.object_classification.domain.data_sources.unlabeled_dir_data_source_handler import \
    UnlabeledDirDataSourceHandler
from organon.ml.object_classification.domain.objects.data_augmentation_settings import DataAugmentationSettings
from organon.ml.object_classification.domain.objects.fine_tuning_settings import FineTuningSettings
from organon.ml.object_classification.domain.objects.object_clf_settings import ObjectClfSettings
from organon.ml.object_classification.domain.services import base_object_classification_service
from organon.ml.object_classification.domain.services.base_object_classification_service import \
    BaseObjectClassificationService
from organon.tests import test_helper


def add_process_data_se(data_mock: MagicMock):
    """imitates DataSourcehnadler.process_data"""

    def process_data_se():
        new_dataset = []
        for data in data_mock.data:
            new_dataset.append((data[0] + "p", data[1]))
        data_mock.dataset = new_dataset

    data_mock.process_data.side_effect = process_data_se


def global_average_pooling_se(base_model: MagicMock):
    """imitates tf.keras.layers.GlobalAveragePooling2D"""
    base_model.layers = base_model.layers + "-avgpool"
    return base_model


def dropout_se(base_model: MagicMock):
    """imitates tf.keras.layers.Dropout"""
    base_model.layers = base_model.layers + "-dropout"
    return base_model


def dense_se(base_model: MagicMock):
    """imitates tf.keras.layers.Dense"""
    base_model.outputs = "outputs"
    return base_model


# pylint: disable=invalid-name,no-member
class ObjectClassificationBaseTestCase:
    """Class for handling common tests in Binary and Multiclass Object Classification"""

    def setUp(self):
        """Mocking BaseObjectClassificationService"""
        self.base_module = base_object_classification_service.__name__
        self.os_mock = test_helper.get_mock(self, self.base_module + "." + "os")
        self.os_mock.path.join.return_value = "joined"

        self.tf_mock = MagicMock()
        self.sys_mod_patcher = patch.dict("sys.modules", {"tensorflow": self.tf_mock})
        self.sys_mod_patcher.start()
        self.tf_mock.keras.layers.GlobalAveragePooling2D.return_value.side_effect = global_average_pooling_se
        self.tf_mock.keras.layers.Dropout.return_value.side_effect = dropout_se
        self.tf_mock.keras.callbacks.EarlyStopping.side_effect = lambda patience: patience
        self.tf_mock.keras.callbacks.TensorBoard.return_value = "tensorboard"
        self.tf_mock.keras.optimizers.Adam.side_effect = lambda learning_rate: "opt" + str(learning_rate)

        self.tf_data_handler_mock = test_helper.get_mock(self,
                                                         self.base_module + "." + TfDatasetDataSourceHandler.__name__)

        self._get_stepts_parameter_mock = test_helper.get_mock(self,
                                                               self.base_module + "." +
                                                               "BaseObjectClassificationService._get_steps_parameter")
        self.check_settings_mock = test_helper.get_mock(self,
                                                        self.base_module + "." +
                                                        "ObjectClfSettingsValidator.check_settings")
        self.get_data_source_handler_mock = test_helper.get_mock(self,
                                                                 self.base_module + "." +
                                                                 "DataSourceHandlerFactory.get_data_source_handler")
        self.get_base_model_mock = test_helper.get_mock(self,
                                                        self.base_module + "." +
                                                        "BaseModelGenerator.get_base_model")
        self.base_model_mock = MagicMock()
        self.base_model_mock.layers = "layers"
        self.base_model_mock.trainable = False
        self.get_base_model_mock.return_value = (self.base_model_mock, "inputs")
        self._get_stepts_parameter_mock.side_effect = lambda dataset: len(dataset)  # pylint: disable=unnecessary-lambda

        self.settings = ObjectClfSettings("train_data_dir", batch_size=1)

        self.service = self._get_service()

    def test_fit_color_parameter_error(self):
        """Testing fit with ambiguous color mode"""
        self._add_image_data_from_directory_return_value()
        self.settings.color_mode = "color"
        with self.assertRaisesRegex(ValueError, "Color type is ambiguous!"):
            self.service.fit()

    def test_fit_optimizer_error(self):
        """Testing fit with ambiguous optimizer"""
        self._add_image_data_from_directory_return_value()
        self.settings.transfer_learning_settings.optimizer = "optimizer"
        with self.assertRaisesRegex(ValueError, "Optimizer is ambiguous!"):
            self.service.fit()

    def test_evaluate_unlabeled_dataset(self):
        """Testing evaluate for UnlabeledDirectoryDataSource"""
        self._add_image_data_from_directory_return_value()
        unlabeled_data_handler_mock = test_helper.get_mock(self,
                                                           self.base_module + "." +
                                                           UnlabeledDirDataSourceHandler.__name__)
        isinstance_mock = test_helper.get_mock(self, self.base_module + "." + "isinstance")

        def is_instance_se(arg1, arg2):
            return True if arg1.data == "unlabeled_dir" and arg2 == unlabeled_data_handler_mock \
                else isinstance(arg1, arg2)

        isinstance_mock.side_effect = is_instance_se
        self.service.fit()
        with self.assertRaisesRegex(TypeError, r"Unlabeled sources can't be evaluated!"):
            _, _ = self.service.evaluate("unlabeled_dir", return_df=False)

    def test_get_processed_data(self):
        """Testing get processed data function"""
        processed_data = self.service.get_processed_data([("img0", 0), ("img1", 1), ("img2", 2), ("img3", 3)])
        self.assertEqual([("img0p", 0), ("img1p", 1), ("img2p", 2), ("img3p", 3)], processed_data)

    def _add_image_data_from_directory_return_value(self):
        self.tf_mock.keras.utils.image_dataset_from_directory.return_value = (
            [("img0", 0), ("img1", 1)], [("img2", 2), ("img3", 3)])

    def _mock_metrics(self):
        accuracy_score_mock = test_helper.get_mock(self, self.base_module + "." + "accuracy_score")
        accuracy_score_mock.side_effect = lambda true, pred: sum(true + pred)
        f1_score_mock = test_helper.get_mock(self, self.base_module + "." + "f1_score")
        f1_score_mock.side_effect = lambda true, pred, average="weighted": sum(true - pred)
        recall_score_mock = test_helper.get_mock(self, self.base_module + "." + "recall_score")
        recall_score_mock.side_effect = lambda true, pred, average="weighted": sum(true * pred)
        precision_score_mock = test_helper.get_mock(self, self.base_module + "." + "precision_score")
        precision_score_mock.side_effect = lambda true, pred, average="weighted": sum(true + pred)

    def _prepare_test_fit_gray_with_validation_augmentation_fine_tuning(self):
        self.tf_mock.keras.utils.image_dataset_from_directory.side_effect = [[("img0", 0), ("img1", 1)],
                                                                             [("img2", 2), ("img3", 3)]]

        self.tf_mock.keras.optimizers.SGD.side_effect = lambda learning_rate: "opt" + str(learning_rate)

        self.settings.fine_tuning_settings = FineTuningSettings(optimizer=OptimizerType.SGD, early_stopping_patience=3,
                                                                epoch=10)
        self.settings.data_augmentation_settings = DataAugmentationSettings()
        self.settings.color_mode = ColorType.GRAY
        self.settings.validation_data_dir = "validation_dir"

        self.service.fit()

    def _fit_default_assert_common(self):
        self.assertEqual(False, self.base_model_mock.trainable)
        self.assertEqual("layers-avgpool-dropout", self.service.model.outputs.layers)
        self.assertEqual("inputs", self.service.model.inputs)
        self.service.model.fit.assert_called_with(x=[('img0', 0), ('img1', 1)],
                                                  epochs=20,
                                                  steps_per_epoch=2,
                                                  validation_data=[('img2', 2), ('img3', 3)],
                                                  validation_steps=2,
                                                  class_weight=None,
                                                  callbacks=[2, "tensorboard"])

    def _fit_gray_with_validation_augmentation_fine_tuning_assert_common(self):
        self.assertEqual(True, self.base_model_mock.trainable)
        self.assertEqual("layers-avgpool-dropout", self.service.model.outputs.layers)
        self.assertEqual("inputs", self.service.model.inputs)
        self.assertEqual([({"x": [('img0', 0), ('img1', 1)],
                            "epochs": 20,
                            "steps_per_epoch": 2,
                            "validation_data": [('img2', 2), ('img3', 3)],
                            "validation_steps": 2,
                            "class_weight": None,
                            "callbacks": [2, "tensorboard"]},),
                          ({"x": [('img0', 0), ('img1', 1)],
                            "epochs": 10,
                            "steps_per_epoch": 2,
                            "validation_data": [('img2', 2), ('img3', 3)],
                            "validation_steps": 2,
                            "class_weight": None,
                            "callbacks": [3, "tensorboard"]},)], self.service.model.fit.call_args_list)

    def _get_service(self) -> BaseObjectClassificationService:
        raise NotImplementedError


if __name__ == '__main__':
    unittest.main()
