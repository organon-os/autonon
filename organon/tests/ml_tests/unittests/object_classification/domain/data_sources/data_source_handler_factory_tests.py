"""Includes DataSourceHandlerFactoryTests"""
import unittest
from unittest.mock import MagicMock

from organon.ml.object_classification.domain.data_sources.data_source_handler_factory import DataSourceHandlerFactory
from organon.ml.object_classification.domain.data_sources import data_source_handler_factory
from organon.ml.object_classification.domain.data_sources.labeled_dir_data_source_handler import \
    LabeledDirDataSourceHandler
from organon.ml.object_classification.domain.data_sources.tf_dataset_data_source_handler import \
    TfDatasetDataSourceHandler
from organon.ml.object_classification.domain.data_sources.unlabeled_dir_data_source_handler import \
    UnlabeledDirDataSourceHandler
from organon.ml.object_classification.domain.objects.object_clf_settings import ObjectClfSettings
from organon.tests import test_helper


class DataSourceHandlerFactoryTests(unittest.TestCase):
    """DataSourceHnadlerFactory tests"""
    def setUp(self) -> None:
        self.module = data_source_handler_factory.__name__
        self.os_mock = test_helper.get_mock(self, self.module + "." + "os")
        self.os_mock.listdir.side_effect = ["dir1", "dir2"]
        self.check_directory_mock = test_helper.get_mock(self, self.module + "." + "check_directory")
        self.settings = ObjectClfSettings("train_data_dir")

    def test_get_data_source_unlabeled(self):
        """test get_data_source for unlabeled directory"""
        self.os_mock.path.isfile.return_value = True

        data_source = DataSourceHandlerFactory.get_data_source_handler("data", self.settings)
        self.check_directory_mock.assert_called_with("data")
        self.assertIsInstance(data_source, UnlabeledDirDataSourceHandler)

    def test_get_data_source_labeled(self):
        """test get_data_source for labeled directory"""
        self.os_mock.path.isfile.return_value = False
        data_source = DataSourceHandlerFactory.get_data_source_handler("data", self.settings)
        self.check_directory_mock.assert_called_with("data")
        self.assertIsInstance(data_source, LabeledDirDataSourceHandler)

    def test_get_data_source_tf_dataset(self):
        """Testing get_data_source for tf.dataset"""
        data = MagicMock()
        data.type = "tf_dataset"
        isinstance_mock = test_helper.get_mock(self, self.module + "." + "isinstance")
        isinstance_mock.side_effect = lambda arg1, arg2: False if arg1.type == "tf_dataset" and arg2 == str else \
            isinstance(arg1, arg2)
        data_source = DataSourceHandlerFactory.get_data_source_handler(data, self.settings)
        self.assertIsInstance(data_source, TfDatasetDataSourceHandler)

    def test_get_data_source_error(self):
        """Testing get_data_source with mixed directory"""
        self.os_mock.path.isfile.side_effect = [True, True, False, True]
        with self.assertRaisesRegex(ValueError,
                                    r"Prediction path should contain only unlabeled "
                                    r"images \(files\) or labeled images \(folders\)"):
            _ = DataSourceHandlerFactory.get_data_source_handler("data", self.settings)
        self.check_directory_mock.assert_called_with("data")


if __name__ == '__main__':
    unittest.main()
