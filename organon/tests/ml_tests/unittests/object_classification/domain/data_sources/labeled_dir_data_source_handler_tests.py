"""Includes LabeledDataSourceHandlerTests"""
import unittest
from unittest.mock import MagicMock, patch

import numpy as np
import pandas as pd

from organon.ml.common.enums.classification_type import ClassificationType
from organon.ml.object_classification.common.object_classification_constants import ObjectClassificationConstants
from organon.ml.object_classification.domain.data_sources import labeled_dir_data_source_handler
from organon.ml.object_classification.domain.data_sources.labeled_dir_data_source_handler import \
    LabeledDirDataSourceHandler
from organon.ml.object_classification.domain.objects.data_source_settings import DataSourceSettings
from organon.tests import test_helper


class LabeledDirDataSourceHandlerTests(unittest.TestCase):
    """LabeledDirDataSourceHandlerTests"""

    def setUp(self) -> None:
        module = labeled_dir_data_source_handler.__name__
        self.os_walk_mock = test_helper.get_mock(self, module + "." + "os.walk")
        self.os_walk_mock.return_value = [("root_dir", ["dir1", "di2"], ["file1", "file2", "file3"])]
        self.tf_mock = MagicMock()
        self.sys_mod_patcher = patch.dict("sys.modules", {"tensorflow": self.tf_mock})
        self.sys_mod_patcher.start()
        self.settings = DataSourceSettings(clf_type=ClassificationType.BINARY)
        self.handler = LabeledDirDataSourceHandler("directory", self.settings)

    def test_process_data(self):
        """test_process_data for labeled directory data sources"""
        self.tf_mock.keras.utils.image_dataset_from_directory.return_value = "dataset"
        self.handler.process_data()
        self.os_walk_mock.assert_called_with("directory")
        self.tf_mock.keras.utils.image_dataset_from_directory.assert_called_with(directory="directory",
                                                                                 batch_size=50,
                                                                                 image_size=(150, 150),
                                                                                 shuffle=False, seed=42,
                                                                                 color_mode="rgb",
                                                                                 label_mode="binary")
        self.assertEqual(self.handler.dataset, "dataset")

    def test_initialize_probs_df(self):
        """test_initialize_probs_df for labeled directory data sources"""
        self.tf_mock.keras.utils.image_dataset_from_directory.return_value = [("img0", 0), ("img1", 1), ("img2", 2)]
        self.handler.process_data()
        result_df = self.handler.initialize_probs_df()
        pd.testing.assert_frame_equal(pd.DataFrame({
            ObjectClassificationConstants.FILENAME_COL_NAME: ["file1", "file2", "file3"],
            ObjectClassificationConstants.LABELS_COL_NAME: np.array([0, 1, 2])
        }), result_df)

    def test_get_binary_labels(self):
        """test_get_binary_labels for labeled directory data sources"""
        self.tf_mock.keras.utils.image_dataset_from_directory.return_value = [("img0", 0), ("img1", 1)]
        self.handler.process_data()
        labels = self.handler.get_labels()
        np.testing.assert_array_equal(np.array([0, 1]), labels)

    def test_get_multiclass_labels(self):
        """test_get_multiclass_labels for labeled directory data sources"""
        self.settings.clf_type = ClassificationType.MULTICLASS
        self.tf_mock.keras.utils.image_dataset_from_directory.return_value = [("img0", [[1, 0]]), ("img1", [[0, 1]])]
        self.handler.process_data()
        labels = self.handler.get_labels()
        np.testing.assert_array_equal(np.array([0, 1]), labels)


if __name__ == '__main__':
    unittest.main()
