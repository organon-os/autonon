"""Includes UnlabeledDirDataSourceHandlerTests"""
import unittest
from unittest.mock import MagicMock, patch

import numpy as np
import pandas as pd

from organon.ml.common.enums.classification_type import ClassificationType
from organon.ml.object_classification.common.object_classification_constants import ObjectClassificationConstants
from organon.ml.object_classification.domain.data_sources import unlabeled_dir_data_source_handler, \
    base_data_source_handler
from organon.ml.object_classification.domain.data_sources.unlabeled_dir_data_source_handler import \
    UnlabeledDirDataSourceHandler
from organon.ml.object_classification.domain.objects.data_source_settings import DataSourceSettings
from organon.tests import test_helper
from organon.tests.ml_tests.unittests.object_classification.domain.data_sources.data_source_handler_tests_helper \
    import mock_tf_in_transform, add_map_side_effect_for_tuple_dataset


def add_map_side_effect(mock):
    """add map side effect for process data and transform dataset"""

    def map_side_effect(func):
        unlabeled_dataset = MagicMock()
        images = []
        labels = []
        for file in mock.files_dataset_list:
            image, label = func(file)
            images.append(image)
            labels.append(label)

        unlabeled_dataset.files_dataset_list = list(zip(images, labels))
        unlabeled_dataset.__iter__.return_value = unlabeled_dataset.files_dataset_list
        add_map_side_effect_for_tuple_dataset(unlabeled_dataset)
        return unlabeled_dataset

    mock.map.side_effect = map_side_effect


class UnlabeledDirDataSourceHandlerTests(unittest.TestCase):
    """UnlabeledDirDataSourceHandlerTests"""

    def setUp(self) -> None:
        self.settings = DataSourceSettings(clf_type=ClassificationType.BINARY)
        self.handler = UnlabeledDirDataSourceHandler("directory", self.settings)
        self.base_module = base_data_source_handler.__name__
        self.module_name = unlabeled_dir_data_source_handler.__name__
        self.tf_mock = MagicMock()
        self.sys_mod_patcher = patch.dict("sys.modules", {"tensorflow": self.tf_mock})
        self.sys_mod_patcher.start()

    def test_process_data(self):
        """test_process_data for unlabeled directory data sources"""

        self.tf_mock.strings.split.side_effect = lambda filename, sep: filename[-1]
        self._process_data()
        self.assertEqual([('img0-conv-resize150150-dims', '0'), ('img1-conv-resize150150-dims', '1')],
                         self.handler.dataset)

    def test_initialize_probs_df(self):
        """test_initialize_probs_df for unlabeled directory data sources"""
        self._labels_test_prepare()
        result_df = self.handler.initialize_probs_df()
        pd.testing.assert_frame_equal(pd.DataFrame({
            ObjectClassificationConstants.FILENAME_COL_NAME: np.array([0, 1])
        }), result_df)

    def test_get_binary_labels(self):
        """test_get_binary_labels for unlabeled directory data sources"""
        self._labels_test_prepare()
        labels = self.handler.get_labels()
        np.testing.assert_array_equal(np.array([0, 1]), labels)

    def test_get_multiclass_labels(self):
        """test_get_multiclass_labels for unlabeled directory data sources"""
        self.settings.clf_type = ClassificationType.MULTICLASS
        self._labels_test_prepare()
        labels = self.handler.get_labels()
        np.testing.assert_array_equal(np.array([0, 1]), labels)

    def _labels_test_prepare(self):
        np_array_mock = test_helper.get_mock(self, self.base_module + "." + "np.array")
        np_array_mock.side_effect = lambda arr: arr
        label_mock0 = MagicMock()
        label_mock1 = MagicMock()
        label_mock0.numpy.return_value.decode.return_value = 0
        label_mock1.numpy.return_value.decode.return_value = 1
        self.tf_mock.strings.split.side_effect = lambda filename, sep: [label_mock0] if filename[-1] == "0" else [
            label_mock1]
        self._process_data()

    def _process_data(self):
        os_join_mock = test_helper.get_mock(self, self.module_name + "." + "os.path.join")
        os_join_mock.return_value = "files"
        files_dataset = MagicMock()
        files_dataset.files_dataset_list = ["file0", "file1"]
        add_map_side_effect(files_dataset)
        self.tf_mock.data.Dataset.list_files.return_value = files_dataset

        self.tf_mock.io.read_file.side_effect = lambda filename: filename.replace("file", "img")
        self.tf_mock.io.decode_jpeg.side_effect = lambda image: image

        mock_tf_in_transform(self.tf_mock, self.settings)

        self.handler.process_data()


if __name__ == '__main__':
    unittest.main()
