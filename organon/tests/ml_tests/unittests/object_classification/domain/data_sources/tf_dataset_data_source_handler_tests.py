"""Includes TfDatasetDataSourceHandlerTests"""
import unittest
from unittest.mock import MagicMock, patch

import numpy as np
import pandas as pd

from organon.ml.common.enums.classification_type import ClassificationType
from organon.ml.object_classification.common.object_classification_constants import ObjectClassificationConstants
from organon.ml.object_classification.domain.data_sources.tf_dataset_data_source_handler import \
    TfDatasetDataSourceHandler
from organon.ml.object_classification.domain.objects.data_source_settings import DataSourceSettings
from organon.tests.ml_tests.unittests.object_classification.domain.data_sources.data_source_handler_tests_helper \
    import add_map_side_effect_for_tuple_dataset, mock_tf_in_transform


class TfDatasetDataSourceHandlerTests(unittest.TestCase):
    """TfDatasetDataSourceHandlerTests"""
    def setUp(self) -> None:
        dataset = MagicMock()
        dataset.files_dataset_list = [("img0", 0), ("img1", 1)]
        add_map_side_effect_for_tuple_dataset(dataset)
        self.settings = DataSourceSettings(clf_type=ClassificationType.BINARY)
        self.handler = TfDatasetDataSourceHandler(dataset, self.settings)
        self.base_tf_mock = MagicMock()
        self.sys_mod_patcher = patch.dict("sys.modules", {"tensorflow": self.base_tf_mock})
        self.sys_mod_patcher.start()

    def test_process_data(self):
        """test_process_data for tf dataset data sources"""
        mock_tf_in_transform(self.base_tf_mock, self.settings)
        self.handler.process_data()
        self.assertEqual([('img0-conv-resize150150-dims', 0), ('img1-conv-resize150150-dims', 1)],
                         self.handler.dataset)

    def test_initialize_probs_df(self):
        """test_initialize_probs_df for tf dataset data sources"""
        self.handler.process_data()
        result_df = self.handler.initialize_probs_df()
        pd.testing.assert_frame_equal(pd.DataFrame({
            ObjectClassificationConstants.LABELS_COL_NAME: np.array([0, 1])
        }), result_df)

    def test_get_binary_labels(self):
        """test_get_binary_labels for tf dataset data sources"""
        self.handler.process_data()
        labels = self.handler.get_labels()
        np.testing.assert_array_equal(np.array([0, 1]), labels)

    def test_get_multiclass_labels(self):
        """test_get_multiclass_labels for tf dataset data sources"""
        self.settings.clf_type = ClassificationType.MULTICLASS
        self.handler.process_data()
        labels = self.handler.get_labels()
        np.testing.assert_array_equal(np.array([0, 1]), labels)


if __name__ == '__main__':
    unittest.main()
