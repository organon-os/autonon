"""Data Source Handler Test Helpers"""


def add_map_side_effect_for_tuple_dataset(mock):
    """imitate map for transform dataset method"""

    def map_side_effect(func):
        images = []
        labels = []
        for img, label in mock.files_dataset_list:
            image, lbl = func(img, label)
            images.append(image)
            labels.append(lbl)
        return list(zip(images, labels))

    mock.map.side_effect = map_side_effect


def mock_tf_in_transform(tf_mock, settings):
    """imitate transform dataset method"""
    tf_mock.image.convert_image_dtype.side_effect = lambda image, tf_float: image + "-conv"
    tf_mock.image.resize_with_pad.side_effect = \
        lambda image, target_height=settings.image_size[1], target_width=settings.image_size[0]: \
            image + "-resize" + str(target_height) + str(target_width)
    tf_mock.expand_dims.side_effect = lambda image, axis=0: image + "-dims"
