"""Includes Object Classifier Tests"""
import unittest

from organon.ml.common.enums.color_type import ColorType
from organon.ml.common.enums.optimizer_type import OptimizerType
from organon.ml.common.enums.pretrained_model_type import PretrainedModelType
from organon.ml.common.enums.random_flip_type import RandomFlipType
from organon.ml.object_classification.domain.services.object_classification_service_factory import \
    ObjectClassificationServiceFactory
from organon.ml.object_classification.services import object_classifier
from organon.ml.object_classification.services.object_classifier import ObjectClassifier
from organon.tests import test_helper


class ObjectClassifierTests(unittest.TestCase):
    """Object Classificer Tests"""
    def setUp(self) -> None:
        module = object_classifier.__name__
        self.get_classification_service_mock = test_helper.get_mock(self, module + "." +
                                                                    ObjectClassificationServiceFactory.__name__ +
                                                                    "." + "get_classification_service")
        self.service = ObjectClassifier(train_data_dir="train_data_dir")
        self.service.set_transfer_learning_settings(epoch=50, learning_rate=0.8)
        self.service.set_pretrained_model_settings(model="RES_NET_50_V2")

    def test_fit_parameters(self) -> None:
        """Tests fit parameters"""
        self.service.fit()
        self.assertEqual(None, self.get_classification_service_mock.call_args.args[0].fine_tuning_settings)
        self.assertEqual(None, self.get_classification_service_mock.call_args.args[0].data_augmentation_settings)
        self._assert_rest()

    def test_fit_parameters_fine_tuning_augmentation(self):
        """Tests fit parameters including augmentation and fine tuning"""
        self.service.set_fine_tuning_settings(epoch=7)
        self.service.set_data_augmentation_settings(random_flip="VERTICAL")
        self.service.fit()
        self.assertEqual(7, self.get_classification_service_mock.call_args.args[0].fine_tuning_settings.epoch)
        self.assertEqual(RandomFlipType.VERTICAL,
                         self.get_classification_service_mock.call_args.args[0].data_augmentation_settings.random_flip)
        self._assert_rest()

    def _assert_rest(self):
        self.assertEqual("train_data_dir", self.get_classification_service_mock.call_args.args[0].train_data_dir)
        self.assertEqual(None, self.get_classification_service_mock.call_args.args[0].validation_data_dir)
        self.assertEqual(0.2, self.get_classification_service_mock.call_args.args[0].validation_data_ratio)
        self.assertEqual((150, 150), self.get_classification_service_mock.call_args.args[0].image_size)
        self.assertEqual(50, self.get_classification_service_mock.call_args.args[0].batch_size)
        self.assertEqual(ColorType.RGB, self.get_classification_service_mock.call_args.args[0].color_mode)
        self.assertEqual(42, self.get_classification_service_mock.call_args.args[0].random_seed)
        self.assertEqual(50, self.get_classification_service_mock.call_args.args[0].transfer_learning_settings.epoch)
        self.assertEqual(2, self.get_classification_service_mock.call_args.args[
            0].transfer_learning_settings.early_stopping)
        self.assertEqual(OptimizerType.ADAM,
                         self.get_classification_service_mock.call_args.args[0].transfer_learning_settings.optimizer)
        self.assertEqual(0.2, self.get_classification_service_mock.call_args.args[0].transfer_learning_settings.dropout)
        self.assertEqual(0.8, self.get_classification_service_mock.call_args.args[
            0].transfer_learning_settings.learning_rate)
        self.assertEqual("imagenet",
                         self.get_classification_service_mock.call_args.args[0].pretrained_model_settings.weights)
        self.assertEqual(PretrainedModelType.RES_NET_50_V2,
                         self.get_classification_service_mock.call_args.args[0].pretrained_model_settings.model)


if __name__ == '__main__':
    unittest.main()
