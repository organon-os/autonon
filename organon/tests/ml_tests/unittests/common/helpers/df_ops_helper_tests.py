"""Includes tests for df_ops_helper module"""
import unittest

import pandas as pd

from organon.ml.common.helpers.df_ops_helper import get_sample_indices, get_sample_data, get_train_test_split, \
    get_bins, get_sample_indices_from_series, get_train_test_split_from_strata


class DfOpsHelperTestCase(unittest.TestCase):
    """Test class for df_ops_helper class."""

    def test_get_train_test_split_from_strata(self):
        """tests get_train_test_split function"""
        data = pd.DataFrame({"a": [1, 2, 2, 3, 10, 6]})
        train_x, train_y, test_x, test_y = get_train_test_split_from_strata(data, pd.Series(
            data=[2, 2, 1, 7, 9, 7]), 1 / 3, random_state=42)
        self.assertListEqual(list(train_x.index), list(train_y.index))
        self.assertListEqual(list(test_x.index), list(test_y.index))
        total_index = list(test_x.index) + list(train_x.index)
        total_index.sort()
        self.assertListEqual(total_index, list(data.index))

    def test_stratified_get_train_test_split_from_strata(self):
        """tests get_train_test_split function"""
        data = pd.DataFrame({"a": list(range(30))})
        series_data = pd.Series(data=[1] * 12 + [2] * 12 + [3] * 6)
        _, train_y, _, test_y = get_train_test_split_from_strata(data, series_data, 1 / 3, random_state=42)
        self.assertEqual(len(test_y[test_y == 1]), 4)
        self.assertEqual(len(train_y[train_y == 1]), 8)
        self.assertEqual(len(test_y[test_y == 2]), 4)
        self.assertEqual(len(train_y[train_y == 2]), 8)
        self.assertEqual(len(test_y[test_y == 3]), 2)
        self.assertEqual(len(train_y[train_y == 3]), 4)

    def test_get_sample_indices_from_series(self):
        """tests get_sampled_indices function"""
        series_data = pd.Series(data=[2, 2, 1, 7, 9, 7])
        sample_indices = get_sample_indices_from_series(series_data, 1 / 3, random_state=42)
        self.assertListEqual(list(sample_indices), [1, 3])
        self.assertListEqual(list(series_data.index.difference(sample_indices)), [0, 2, 4, 5])

    def test_stratified_get_sample_indices_from_series(self):
        """tests get_sampled_indices function"""
        series_data = pd.Series(data=[1] * 12 + [2] * 12 + [3] * 6)
        sample_indices = get_sample_indices_from_series(series_data, 1 / 3, random_state=42)
        self.assertEqual(len([ind for ind in series_data[sample_indices] if ind == 1]), 4)
        self.assertEqual(len([ind for ind in series_data[series_data.index.difference(sample_indices)] if ind == 1]), 8)
        self.assertEqual(len([ind for ind in series_data[sample_indices] if ind == 2]), 4)
        self.assertEqual(len([ind for ind in series_data[series_data.index.difference(sample_indices)] if ind == 2]),
                         8)
        self.assertEqual(len([ind for ind in series_data[sample_indices] if ind == 3]), 2)
        self.assertEqual(len([ind for ind in series_data[series_data.index.difference(sample_indices)] if ind == 3]),
                         4)

    def test_get_train_test_split(self):
        """tests get_train_test_split function"""
        data = pd.DataFrame({"a": list(range(30)), "b": [1] * 12 + [2] * 12 + [3] * 6, "c": ["a"] * 18 + ["b"] * 12})
        train_data, test_data = get_train_test_split(data, 1 / 3, strata_columns=["b", "c"])
        self.assertEqual(10, len(test_data))
        self.assertEqual(4, sum((test_data["b"] == 1) & (test_data["c"] == "a")))
        self.assertEqual(2, sum((test_data["b"] == 2) & (test_data["c"] == "a")))
        self.assertEqual(2, sum((test_data["b"] == 2) & (test_data["c"] == "b")))
        self.assertEqual(2, sum((test_data["b"] == 3) & (test_data["c"] == "b")))

        self.assertEqual(20, len(train_data))
        self.assertEqual(8, sum((train_data["b"] == 1) & (train_data["c"] == "a")))
        self.assertEqual(4, sum((train_data["b"] == 2) & (train_data["c"] == "a")))
        self.assertEqual(4, sum((train_data["b"] == 2) & (train_data["c"] == "b")))
        self.assertEqual(4, sum((train_data["b"] == 3) & (train_data["c"] == "b")))

    def test_get_train_test_split_ratio_error(self):
        """tests get_train_test_split function"""
        data = pd.DataFrame({"a": list(range(30)), "b": [1] * 12 + [2] * 12 + [3] * 6, "c": ["a"] * 18 + ["b"] * 12})
        with self.assertRaisesRegex(ValueError, "test_ratio cannot be higher than 1.0"):
            get_train_test_split(data, 1.5)

    def test_get_sample_indices(self):
        """tests get_sampled_indices function"""
        data = pd.DataFrame(
            {"a": list(range(30)), "b": [1] * 12 + [2] * 12 + [3] * 6, "c": ["a"] * 18 + ["b"] * 12})
        sample_indices = get_sample_indices(data, 1 / 3, strata_columns=["b", "c"])
        self.assertEqual(10, len(sample_indices))
        selected_data = data.loc[sample_indices]
        self.assertEqual(4, sum((selected_data["b"] == 1) & (selected_data["c"] == "a")))
        self.assertEqual(2, sum((selected_data["b"] == 2) & (selected_data["c"] == "a")))
        self.assertEqual(2, sum((selected_data["b"] == 2) & (selected_data["c"] == "b")))
        self.assertEqual(2, sum((selected_data["b"] == 3) & (selected_data["c"] == "b")))

    def test_get_sample_indices_frac_higher_than_1(self):
        """tests get_sample_indices function with frac>1.0"""
        data = pd.DataFrame(
            {"a": list(range(30)), "b": [1] * 12 + [2] * 12 + [3] * 6, "c": ["a"] * 18 + ["b"] * 12})
        sample_indices = get_sample_indices(data, 4 / 3, strata_columns=["b", "c"], replace=True)
        self.assertEqual(40, len(sample_indices))
        selected_data = data.loc[sample_indices]
        self.assertEqual(16, sum((selected_data["b"] == 1) & (selected_data["c"] == "a")))
        self.assertEqual(8, sum((selected_data["b"] == 2) & (selected_data["c"] == "a")))
        self.assertEqual(8, sum((selected_data["b"] == 2) & (selected_data["c"] == "b")))
        self.assertEqual(8, sum((selected_data["b"] == 3) & (selected_data["c"] == "b")))

    def test_get_sample_indices_no_strata(self):
        """tests get_sample_indices function when no strata columns given"""
        data = pd.DataFrame(
            {"a": list(range(30)), "b": [1] * 12 + [2] * 12 + [3] * 6, "c": ["a"] * 18 + ["b"] * 12})
        sample_indices = get_sample_indices(data, 1 / 3)
        self.assertEqual(10, len(sample_indices))

    def test_get_sample_data(self):
        """tests get_sample_data function"""
        data = pd.DataFrame(
            {"a": list(range(30)), "b": [1] * 12 + [2] * 12 + [3] * 6, "c": ["a"] * 18 + ["b"] * 12})
        selected_data = get_sample_data(data, 1 / 3, strata_columns=["b", "c"])
        self.assertEqual(10, len(selected_data))
        self.assertEqual(4, sum((selected_data["b"] == 1) & (selected_data["c"] == "a")))
        self.assertEqual(2, sum((selected_data["b"] == 2) & (selected_data["c"] == "a")))
        self.assertEqual(2, sum((selected_data["b"] == 2) & (selected_data["c"] == "b")))
        self.assertEqual(2, sum((selected_data["b"] == 3) & (selected_data["c"] == "b")))

    def test_get_sample_data_no_strata(self):
        """tests get_sample_data function when no strata columns given"""
        data = pd.DataFrame({"a": list(range(30)), "b": [1] * 12 + [2] * 12 + [3] * 6, "c": ["a"] * 18 + ["b"] * 12})
        sample_data = get_sample_indices(data, 1 / 3)
        self.assertEqual(10, len(sample_data))

    def test_get_sample_data_frac_higher_than_1(self):
        """tests get_sample_data function with frac>1.0"""
        data = pd.DataFrame(
            {"a": list(range(30)), "b": [1] * 12 + [2] * 12 + [3] * 6, "c": ["a"] * 18 + ["b"] * 12})
        selected_data = get_sample_data(data, 4 / 3, strata_columns=["b", "c"], replace=True)
        self.assertEqual(40, len(selected_data))
        self.assertEqual(16, sum((selected_data["b"] == 1) & (selected_data["c"] == "a")))
        self.assertEqual(8, sum((selected_data["b"] == 2) & (selected_data["c"] == "a")))
        self.assertEqual(8, sum((selected_data["b"] == 2) & (selected_data["c"] == "b")))
        self.assertEqual(8, sum((selected_data["b"] == 3) & (selected_data["c"] == "b")))

    def test_get_bins(self):
        """tests get_bins method"""
        data = pd.Series([0.5, 0.3, 0.7, 0.1])
        bin_data = get_bins(data, 2)
        self.assertEqual([1, 0, 1, 0], bin_data.to_list())

    def test_get_bins_with_unequal_bins(self):
        """tests get_bins method where bin sizes are unequal"""
        data = pd.Series([0.5, 0.3, 0.7, 0.1, 0.2])
        bin_data = get_bins(data, 2)
        self.assertEqual([1, 0, 1, 0, 0], bin_data.to_list())


if __name__ == '__main__':
    unittest.main()
