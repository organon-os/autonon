"""Includes unittests for ObjectDetectionTrainer"""
import os.path
import unittest
from typing import List, Dict
from unittest.mock import patch

from ultralytics import YOLO

from organon.ml.object_detection.domain import object_detection_training_service
from organon.ml.object_detection.domain.object_detection_training_service import ObjectDetectionTrainingService
from organon.ml.object_detection.services.enums.map_metric import MapMetrics
from organon.tests import test_helper


class ObjectDetectionTrainingServiceTestCase(unittest.TestCase):
    """Unittest class for ObjectDetectionTrainingService"""

    def setUp(self) -> None:
        module_name = object_detection_training_service.__name__
        self.mock_model_trainer_cls = test_helper.get_mock(self, module_name + "." + YOLO.__name__)
        self._default_non_existing_files = [os.path.join("data_dir", "models"),
                                            os.path.join("data_dir", "dataset.yaml")]
        self.mock_dir_helper = test_helper.get_mock(self, module_name + ".directory_helper")
        self.mock_os = test_helper.get_mock(self, module_name + ".os", wraps=os)
        self.mock_os.path.exists.side_effect = self._get_dir_helper_se(self._default_non_existing_files)
        self.mock_os.remove.return_value = True
        self.mock_os.listdir.side_effect = self._get_listdir_se({
            "data_dir/images/train": ["img1.txt", "img2.txt"],
            "data_dir/images/validation": ["img3.jpg", "img4.jpg"],
            "data_dir/labels/validation": ["img3.txt", "img4.txt"],
            "data_dir/labels/train": ["img1.jpg", "img2.jpg"]
        })

    @staticmethod
    def _get_dir_helper_se(non_existing_files: List[str] = None):
        def _se(directory: str):
            if non_existing_files and directory in non_existing_files:
                return False
            return True

        return _se

    def test_train(self):
        """tests train method"""
        trainer = self._get_trainer()
        self.mock_os.path.normpath.return_value = "data_yaml"
        self.assertFalse(trainer.is_trained)
        yaml_data_1 = {
            "train": "data_dir/images/train",
            "val": "data_dir/images/validation"
        }

        with patch('yaml.safe_load') as mock_load:
            mock_load.return_value = yaml_data_1
            with patch('builtins.open', return_value=mock_load):
                trainer.train(epochs=100, batch_size=16, output_folder="some folder", random_seed=42,
                              override_data_files=False)
        self.assertTrue(trainer.is_trained)

    def test_evaluate(self):
        """tests error when evaluate is called before train"""
        trainer = self._get_trainer()
        yaml_data_1 = {
            "train": "data_dir/images/train",
            "val": "data_dir/images/validation"
        }

        with patch('yaml.safe_load') as mock_load:
            mock_load.return_value = yaml_data_1
            with patch('builtins.open', return_value=mock_load):
                trainer.train(epochs=100, batch_size=16, output_folder="some folder", random_seed=42,
                              override_data_files=False)
                metrics = trainer.evaluate(MapMetrics.MAP50)

        self.assertEqual(self.mock_model_trainer_cls.return_value.metrics.box.map50, metrics)

    def test_evaluate_before_train_error(self):
        """tests error when evaluate is called before train"""
        trainer = self._get_trainer()
        with self.assertRaisesRegex(Exception, "Trainer is not trained yet"):
            trainer.evaluate(MapMetrics.MAP50)

    def test_train_error_when_data_dir_not_exists(self):
        """tests error when given data directory does not exist"""
        trainer = self._get_trainer()
        self.mock_os.path.exists.side_effect = [False, False]
        with self.assertRaisesRegex(ValueError, "Directory of yaml file does not exist"):
            trainer.train(epochs=100, batch_size=16, output_folder="some folder", random_seed=42,
                          override_data_files=False)

    def test_train_error_when_val_image_dir_not_exists(self):
        """tests error when given data directory does not include a train directory"""
        trainer = self._get_trainer()

        def _se(path: str):
            if path.startswith(os.path.join("data_dir/images/validation")) or path in self._default_non_existing_files:
                return False
            return True

        self.mock_dir_helper.exists.side_effect = _se

        with self.assertRaisesRegex(ValueError, "Images Validation dir does not exists."):
            yaml_data_1 = {
                "train": "data_dir/images/train",
                "val": "data_dir/images/validation"
            }

            with patch('yaml.safe_load') as mock_load:
                mock_load.return_value = yaml_data_1
                with patch('builtins.open', return_value=mock_load):
                    trainer.train(epochs=100, batch_size=16, output_folder="some folder", random_seed=42,
                                  override_data_files=False)

    def test_train_error_when_train_dir_not_exists_in_images_dir(self):
        """tests error when train directory does not include an images directory"""

        def _se(path: str):
            if path.startswith(os.path.join("data_dir/images/train")) or path in self._default_non_existing_files:
                return False
            return True

        self.mock_dir_helper.exists.side_effect = _se
        exp_error = "Images Train dir does not exists."
        with self.assertRaisesRegex(ValueError, exp_error):
            yaml_data_1 = {
                "train": "data_dir/images/train",
                "val": "data_dir/images/validation"
            }

            with patch('yaml.safe_load') as mock_load:
                mock_load.return_value = yaml_data_1
                with patch('builtins.open', return_value=mock_load):
                    self._get_trainer().train(epochs=100, batch_size=16, output_folder="some folder", random_seed=42,
                                              override_data_files=False)

    def test_train_error_when_annotations_dir_not_exists_in_train_dir(self):
        """tests error when labels directory does not include a train directory"""

        def _se(path: str):
            if path.startswith(os.path.join("data_dir/labels/train")) or path in self._default_non_existing_files:
                return False
            return True

        self.mock_dir_helper.exists.side_effect = _se
        exp_error = "Labels Train dir does not exists."
        with self.assertRaisesRegex(ValueError, exp_error):
            yaml_data_1 = {
                "train": "data_dir/images/train",
                "val": "data_dir/images/validation"
            }

            with patch('yaml.safe_load') as mock_load:
                mock_load.return_value = yaml_data_1
                with patch('builtins.open', return_value=mock_load):
                    self._get_trainer().train(epochs=100, batch_size=16, output_folder="some folder", random_seed=42,
                                              override_data_files=False)

    def test_data_folder_error_when_images_dir_not_exists(self):
        """tests error when validation directory does not include an images directory"""

        def _se(path: str):
            if path.startswith(os.path.join("data_dir", "images")) \
                    or path in self._default_non_existing_files:
                return False
            return True

        self.mock_dir_helper.exists.side_effect = _se
        exp_error = f"No 'images' directory found in {os.path.join('data_dir')}".\
            replace("\\", "\\\\").replace("/", "\\\\")
        with self.assertRaisesRegex(ValueError, exp_error):
            yaml_data_1 = {
                "train": "data_dir/images/train",
                "val": "data_dir/images/validation"
            }

            with patch('yaml.safe_load') as mock_load:
                mock_load.return_value = yaml_data_1
                with patch('builtins.open', return_value=mock_load):
                    self._get_trainer().train(epochs=100, batch_size=16, output_folder="some folder", random_seed=42,
                                              override_data_files=False)

    def test_data_folder_error_when_labels_dir_not_exists(self):
        """tests error when validation directory does not include an annotations directory"""

        def _se(path: str):
            if os.path.normpath(path).startswith(
                    os.path.normpath(os.path.join("data_dir", "labels"))) \
                    or path in self._default_non_existing_files:
                return False
            return True

        self.mock_dir_helper.exists.side_effect = _se
        exp_error = f"No 'labels' directory found in {os.path.join('data_dir')}". \
            replace("\\", "\\\\").replace("/", "\\\\")
        with self.assertRaisesRegex(ValueError, exp_error):
            yaml_data_1 = {
                "train": "data_dir/images/train",
                "val": "data_dir/images/validation"
            }

            with patch('yaml.safe_load') as mock_load:
                mock_load.return_value = yaml_data_1
                with patch('builtins.open', return_value=mock_load):
                    self._get_trainer().train(epochs=100, batch_size=16, output_folder="some folder", random_seed=42,
                                              override_data_files=False)

    def test_train_error_when_label_for_an_image_is_missing(self):
        """tests error when annotation of an image is missing"""
        files_in_dirs = {
            "data_dir/labels/train": ["img1.xml", "img2.xml"],
            "data_dir/images/train": ["img1.jpg", "img2.jpg", "img3.jpg", "img4.jpg"]
        }
        self.mock_os.listdir.side_effect = self._get_listdir_se(files_in_dirs)
        path_str = os.path.join("data_dir/images/train")
        exp_error = f"Following images in '{path_str}' do not have corresponding labels: img3,img4"
        exp_error = exp_error.replace("\\", "\\\\")
        with self.assertRaisesRegex(ValueError, exp_error):
            yaml_data_1 = {
                "train": "data_dir/images/train",
                "val": "data_dir/images/validation"
            }

            with patch('yaml.safe_load') as mock_load:
                mock_load.return_value = yaml_data_1
                with patch('builtins.open', return_value=mock_load):
                    self._get_trainer().train(epochs=100, batch_size=16, output_folder="some folder", random_seed=42,
                                              override_data_files=False)

    def test_train_error_when_image_for_a_label_is_missing(self):
        """tests error when annotation of an image is missing"""
        files_in_dirs = {
            "data_dir/labels/train": ["img1.txt", "img2.txt"],
            "data_dir/images/train": ["img1.jpg"],
            "data_dir/labels/validation": ["img3.txt", "img4.txt"],
            "data_dir/images/validation": ["img3.jpg", "img4.jpg"]
        }
        self.mock_os.listdir.side_effect = self._get_listdir_se(files_in_dirs)
        path_str = os.path.join("data_dir/labels/train")
        exp_error = f"Following labels in '{path_str}' do not have corresponding images: img2"
        exp_error = exp_error.replace("\\", "\\\\")
        with self.assertRaisesRegex(ValueError, exp_error):
            yaml_data_1 = {
                "train": "data_dir/images/train",
                "val": "data_dir/images/validation"
            }

            with patch('yaml.safe_load') as mock_load:
                mock_load.return_value = yaml_data_1
                with patch('builtins.open', return_value=mock_load):
                    self._get_trainer().train(epochs=100, batch_size=16, output_folder="some folder", random_seed=42,
                                              override_data_files=False)

    def _get_listdir_se(self, files_dict: Dict[str, List[str]]):
        new_files_dict = {os.path.normpath(key): value for key, value in files_dict.items()}

        def _se(path: str):
            if os.path.normpath(path) in new_files_dict:
                return new_files_dict[os.path.normpath(path)]
            return self.fail("Unittest is not properly configured")

        return _se

    @staticmethod
    def _get_trainer(yaml_data_dir: str = "data_dir/dataset.yaml", pretr_model_path: str = "pretrained_model_file"):
        return ObjectDetectionTrainingService(yaml_data_dir, pretr_model_path)


if __name__ == '__main__':
    unittest.main()
