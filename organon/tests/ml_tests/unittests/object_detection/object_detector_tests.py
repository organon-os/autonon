"""Includes unittests for ObjectDetector"""
import unittest
from unittest.mock import MagicMock

from ultralytics import YOLO

from organon.ml.object_detection.services import object_detector
from organon.ml.object_detection.services.object_detector import ObjectDetector
from organon.tests import test_helper


class ObjectDetectorTestCase(unittest.TestCase):
    """Unittest class for ObjectDetector"""

    def setUp(self) -> None:
        module_name = object_detector.__name__
        self.mock_obj_det = test_helper.get_mock(self, module_name + "." + YOLO.__name__)
        self.mock_os = test_helper.get_mock(self, module_name + ".os")

    def test_detection_with_pretrained_model(self):
        """tests detect method with a pretrained model and custom objects to detect"""
        mock_obj_detection = self.mock_obj_det.return_value
        mock_detection1 = MagicMock()
        mock_detection1.boxes.cls = "obj1"
        mock_detection1.boxes.conf = 0.6
        mock_detection1.boxes.xyxy = [10, 10, 40, 40]
        mock_detection2 = MagicMock()
        mock_detection2.boxes.cls = "obj2"
        mock_detection2.boxes.conf = 0.7
        mock_detection2.boxes.xyxy = [50, 60, 70, 90]
        mock_obj_detection.return_value = [mock_detection1, mock_detection2]
        # ObjectDetector.load_pretrained_model_from_web("small")
        detector = ObjectDetector("yolov8s.pt")
        detections = detector.detect("some image file", "output file")

        mock_obj_detection.assert_called_once_with('some image file', save=True, save_txt=True, project='output file')

        first_detection = detections[0]
        self.assertEqual(2, len(detections))
        self.assertEqual("obj1", first_detection.object_name)
        self.assertEqual(0.6, first_detection.confidence)
        self.assertEqual([10, 10, 40, 40], first_detection.box_points)

        second_detection = detections[1]
        self.assertEqual("obj2", second_detection.object_name)
        self.assertEqual(0.7, second_detection.confidence)
        self.assertEqual([50, 60, 70, 90], second_detection.box_points)

    def test_detection_with_pretrained_model_invalid_custom_objects(self):
        """tests detect method with a pretrained model - test error when invalid objects to detect given"""
        mock_obj_detection = self.mock_obj_det.return_value
        mock_obj_detection.names = {1: "obj1", 2: "obj2", 3: "obj3"}
        detector = ObjectDetector("some file")
        with self.assertRaisesRegex(Exception, "Following objects cannot be detected by model: obj4."
                                               " Call get_all_object_names to see valid object names"):
            detector.detect("some image file", objects_to_detect=["obj4"])

    def test_detection_with_custom_model(self):
        """tests detect method with a custom model and detected object images are extracted"""
        mock_obj_detection = self.mock_obj_det.return_value
        mock_detection1 = MagicMock()
        mock_detection1.boxes.cls = "obj1"
        mock_detection1.boxes.conf = 0.6
        mock_detection1.boxes.xyxy = [10, 10, 40, 40]
        mock_detection2 = MagicMock()
        mock_detection2.boxes.cls = "obj2"
        mock_detection2.boxes.conf = 0.7
        mock_detection2.boxes.xyxy = [50, 60, 70, 90]
        mock_obj_detection.return_value = [mock_detection1, mock_detection2]
        detector = ObjectDetector.from_custom_model("some file")
        detections = detector.detect("some image file", "output file")

        mock_obj_detection.assert_called_once_with('some image file', save=True, save_txt=True, project='output file')

        first_detection = detections[0]
        self.assertEqual(2, len(detections))
        self.assertEqual("obj1", first_detection.object_name)
        self.assertEqual(0.6, first_detection.confidence)
        self.assertEqual([10, 10, 40, 40], first_detection.box_points)

        second_detection = detections[1]
        self.assertEqual("obj2", second_detection.object_name)
        self.assertEqual(0.7, second_detection.confidence)
        self.assertEqual([50, 60, 70, 90], second_detection.box_points)

    def test_detection_with_custom_model_with_save_and_save_txt_false(self):
        """tests detect method with a custom model and detected object images are extracted"""
        mock_obj_detection = self.mock_obj_det.return_value
        mock_detection1 = MagicMock()
        mock_detection1.boxes.cls = "obj1"
        mock_detection1.boxes.conf = 0.6
        mock_detection1.boxes.xyxy = [10, 10, 40, 40]
        mock_detection2 = MagicMock()
        mock_detection2.boxes.cls = "obj2"
        mock_detection2.boxes.conf = 0.7
        mock_detection2.boxes.xyxy = [50, 60, 70, 90]
        mock_obj_detection.return_value = [mock_detection1, mock_detection2]
        detector = ObjectDetector.from_custom_model("some file")
        detections = detector.detect("some image file", "output file", save=False, save_txt=False)

        mock_obj_detection.assert_called_once_with('some image file', save=False, save_txt=False, project='output file')

        first_detection = detections[0]
        self.assertEqual(2, len(detections))
        self.assertEqual("obj1", first_detection.object_name)
        self.assertEqual(0.6, first_detection.confidence)
        self.assertEqual([10, 10, 40, 40], first_detection.box_points)

        second_detection = detections[1]
        self.assertEqual("obj2", second_detection.object_name)
        self.assertEqual(0.7, second_detection.confidence)
        self.assertEqual([50, 60, 70, 90], second_detection.box_points)

    def test_get_all_object_names_for_pretrained_detection(self):
        """tests get_all_object_names for detection with pretrained model"""
        self.mock_obj_det.return_value.names = {1: "obj1", 2: "obj2"}
        detector = ObjectDetector("some file")
        self.assertEqual({1: "obj1", 2: "obj2"}, detector.get_all_object_names())


if __name__ == '__main__':
    unittest.main()
