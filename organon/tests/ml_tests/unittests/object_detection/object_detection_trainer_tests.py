"""Includes unittests for ObjectDetectionTrainer"""

import os
import unittest

from typing import Dict, List
from unittest.mock import MagicMock, patch

import numpy
from organon.fl.core.exceptionhandling.known_exception import KnownException

from organon.fl.logging.helpers.log_helper import LogHelper

from organon.ml.object_detection.services import object_detection_trainer
from organon.ml.object_detection.services.enums.map_metric import MapMetrics
from organon.ml.object_detection.services.object_detection_trainer import ObjectDetectionTrainer
from organon.tests import test_helper


class ObjectDetectionTrainerTestCase(unittest.TestCase):
    """Unittest class for ObjectDetectionTrainer"""

    def setUp(self) -> None:
        module_name = object_detection_trainer.__name__
        self.mock_dir_helper = test_helper.get_mock(self, module_name + ".directory_helper")
        mock_guid_helper = test_helper.get_mock(self, module_name + ".guid_helper")
        mock_guid_helper.new_guid.return_value = "abcde123"
        self.mock_os = test_helper.get_mock(self, module_name + ".os", wraps=os)
        self.mock_os.listdir.side_effect = self._get_listdir_se({
            "a/b/data_dir/labels/train": ["img1.txt", "img2.txt", "img3.txt"],
            "a/b/data_dir/images/train": ["img1.jpg", "img2.jpg", "img3.jpg"],
            "a/b/data_dir/labels/validation": ["img4.txt", "img5.txt"],
            "a/b/data_dir/images/validation": ["img4.jpg", "img5.jpg"]
        })
        mock_training_service_cls = test_helper.get_mock(self, module_name + ".ObjectDetectionTrainingService")
        mock_training_service_cls.get_train_images_dir_path.side_effect = \
            lambda dir_name: os.path.join(dir_name, "images", "train")
        mock_training_service_cls.get_train_label_dir_path.side_effect = \
            lambda dir_name: os.path.join(dir_name, "labels", "train")
        mock_training_service_cls.get_validation_images_dir_path.side_effect = \
            lambda dir_name: os.path.join(dir_name, "images", "validation")
        mock_training_service_cls.get_validation_label_dir_path.side_effect = \
            lambda dir_name: os.path.join(dir_name, "labels", "validation")
        self.mock_training_service_cls = mock_training_service_cls
        self.mock_training_service_cls.get_train_images_dir_path_yaml.return_value = 'a/b/data_dir/images/train'
        self.mock_training_service_cls.get_validation_images_dir_path_yaml.return_value = \
            'a/b/data_dir/images/validation'
        self.mock_training_service_cls.get_train_label_dir_path_yaml.return_value = 'a/b/data_dir/labels/train'
        self.mock_training_service_cls.get_validation_label_dir_path_yaml.return_value = \
            'a/b/data_dir/labels/validation'
        mock_np = test_helper.get_mock(self, module_name + ".np", wraps=numpy)
        mock_np.random.choice.side_effect = lambda _list, num, replace=False: _list[:num]
        self.mock_log_helper = test_helper.get_mock(self, module_name + "." + LogHelper.__name__)
        self.mock_urllib = test_helper.get_mock(self, module_name + ".urllib")

    def test_train_without_pre_train(self):
        """tests training without executing pre-training"""
        trainer = self._get_trainer()
        mock_training_service = MagicMock(name="training_service")
        self.mock_training_service_cls.return_value = mock_training_service
        trainer.train(False)
        self.mock_training_service_cls.assert_called_once()
        expected_new_data_dir = r"a/b/data_dir/dataset.yaml"
        self.assertEqual(expected_new_data_dir, self.mock_training_service_cls.call_args.args[0])
        self.assertTrue(trainer.is_trained_on_full_data)

    def test_train_pre_train_success(self):
        """tests train - when pre-training results are satisfying"""
        trainer = self._get_trainer()
        mock_training_service = MagicMock(name="training_service")
        self.mock_training_service_cls.return_value = mock_training_service
        mock_training_service.evaluate.return_value = 0.9
        self.mock_os.open.return_value = True

        yaml_data_1 = {
            "train:": "a/b/data_dir/images/train",
            "val": "a/b/data_dir/images/validation"
        }
        # Define the expected data to be returned for the second YAML file
        yaml_data_2 = {
            "train:": "a/b/data_dir/sample_abcde123/images/train",
            "val": "a/b/data_dir/sample_abcde123/images/validation"
        }

        with patch('yaml.safe_load') as mock_load:
            mock_load.side_effect = [yaml_data_1, yaml_data_2]
            with patch('builtins.open', side_effect=[mock_load, mock_load]):
                trainer.train(True, sampling_ratio=0.7)

        self.assertEqual(1, self.mock_training_service_cls.call_count)
        expected_new_yaml_dir = os.path.join(r"a/b/data_dir", "sample_abcde123", "dataset_updated.yaml")
        self.assertEqual(expected_new_yaml_dir,
                         self.mock_training_service_cls.call_args_list[0].args[0])

        mock_training_service.evaluate.assert_called_once_with(MapMetrics.MAP50)

        self.mock_dir_helper.create.assert_called_once_with(os.path.dirname(expected_new_yaml_dir),
                                                            exception_if_exists=True)
        self.mock_dir_helper.copy_files.assert_any_call(
            [os.path.join("a/b/data_dir/images/train", "img1.jpg"),
             os.path.join("a/b/data_dir/images/train", "img2.jpg")],
            os.path.join(os.path.dirname(expected_new_yaml_dir), "images", "train"))
        self.mock_dir_helper.copy_files.assert_any_call(
            [os.path.join("a/b/data_dir/labels/train", "img1.txt"),
             os.path.join("a/b/data_dir/labels/train", "img2.txt")],
            os.path.join(os.path.dirname(expected_new_yaml_dir), "labels", "train"))
        self.mock_dir_helper.copy_files.assert_any_call(
            [os.path.join("a/b/data_dir/images/validation", "img4.jpg")],
            os.path.join(os.path.dirname(expected_new_yaml_dir), "images", "validation"))
        self.mock_dir_helper.copy_files.assert_any_call(
            [os.path.join("a/b/data_dir/labels/validation", "img4.txt")],
            os.path.join(os.path.dirname(expected_new_yaml_dir), "labels", "validation"))

        self.assertFalse(trainer.is_trained_on_full_data)
        self.mock_dir_helper.delete_directory_with_contents.assert_not_called()

    def test_train_pre_train_not_successful(self):
        """tests train - when pre-training results are not satisfying and training would run on full data"""
        trainer = self._get_trainer()
        mock_pretrain_service = MagicMock(name="pretrain_service")
        mock_full_data_train_service = MagicMock(name="full_data_train_service")
        self.mock_training_service_cls.side_effect = [mock_pretrain_service, mock_full_data_train_service]
        mock_pretrain_service.evaluate.return_value = 0.2

        yaml_data_1 = {
            "train:": "a/b/data_dir/images/train",
            "val": "a/b/data_dir/images/validation"
        }
        # Define the expected data to be returned for the second YAML file
        yaml_data_2 = {
            "train:": "a/b/data_dir/sample_abcde123/images/train",
            "val": "a/b/data_dir/sample_abcde123/images/validation"
        }

        with patch('yaml.safe_load') as mock_load:
            mock_load.side_effect = [yaml_data_1, yaml_data_2]
            with patch('builtins.open', side_effect=[mock_load, mock_load]):
                trainer.train(True, sampling_ratio=0.7)

        self.assertEqual(2, self.mock_training_service_cls.call_count)
        expected_new_yaml_dir = os.path.join(r"a/b/data_dir", "sample_abcde123", "dataset_updated.yaml")
        self.assertEqual(expected_new_yaml_dir,
                         self.mock_training_service_cls.call_args_list[0].args[0])

        mock_pretrain_service.evaluate.assert_called_once_with(MapMetrics.MAP50)

        self.mock_dir_helper.delete_directory_with_contents.assert_called_once_with(
            os.path.dirname(expected_new_yaml_dir))

        self.mock_dir_helper.create.assert_called_once_with(os.path.dirname(expected_new_yaml_dir),
                                                            exception_if_exists=True)
        self.mock_dir_helper.copy_files.assert_any_call(
            [os.path.join("a/b/data_dir/images/train", "img1.jpg"),
             os.path.join("a/b/data_dir/images/train", "img2.jpg")],
            os.path.join(os.path.dirname(expected_new_yaml_dir), "images", "train"))
        self.mock_dir_helper.copy_files.assert_any_call(
            [os.path.join("a/b/data_dir/labels/train", "img1.txt"),
             os.path.join("a/b/data_dir/labels/train", "img2.txt")],
            os.path.join(os.path.dirname(expected_new_yaml_dir), "labels", "train"))
        self.mock_dir_helper.copy_files.assert_any_call(
            [os.path.join("a/b/data_dir/images/validation", "img4.jpg")],
            os.path.join(os.path.dirname(expected_new_yaml_dir), "images", "validation"))
        self.mock_dir_helper.copy_files.assert_any_call(
            [os.path.join("a/b/data_dir/labels/validation", "img4.txt")],
            os.path.join(os.path.dirname(expected_new_yaml_dir), "labels", "validation"))

        self.assertTrue(trainer.is_trained_on_full_data)

    def test_train_data_not_enough_for_pretrain(self):
        """tests error when data is not enough for pretrain sampling ratio"""
        trainer = self._get_trainer()
        with self.assertRaisesRegex(KnownException, "Not enough files to run pre-evaluation, "):
            trainer.train(True, sampling_ratio=0.2)

    def test_load_pretrained_model_from_web(self):
        """tests load_pretrained_model_from_web"""
        self.mock_os.path.isfile.return_value = False
        ObjectDetectionTrainer.load_pretrained_model_from_web("some file", "nano")
        self.mock_urllib.request.urlretrieve.assert_called_once_with(
            "https://github.com/ultralytics/assets/releases/download/v0.0.0/yolov8n.pt", "some file")

    def test_load_pretrained_model_from_web_file_exists_error(self):
        """tests file exists error on load_pretrained_model_from_web"""
        self.mock_os.path.isfile.return_value = True
        with self.assertRaisesRegex(FileExistsError, "File 'some file' already exists"):
            ObjectDetectionTrainer.load_pretrained_model_from_web("some file", "nano")

    def test_load_pretrained_model_from_web_file_error_on_load(self):
        """tests file exists error on load_pretrained_model_from_web"""
        self.mock_os.path.isfile.return_value = False

        def _raise_exc():
            raise Exception("some exc msg")

        self.mock_urllib.request.urlretrieve.side_effect = _raise_exc
        with self.assertRaisesRegex(Exception, "Error occurred while loading a pretrained model from internet"):
            ObjectDetectionTrainer.load_pretrained_model_from_web("some file", "nano")

    @classmethod
    def _get_trainer(cls):
        return ObjectDetectionTrainer(os.path.join("pr", "pretrained_model.pt"),
                                      r"a/b/data_dir/dataset.yaml")

    def _get_listdir_se(self, files_dict: Dict[str, List[str]]):
        new_files_dict = {os.path.normpath(key): value for key, value in files_dict.items()}

        def _se(path: str):
            if os.path.normpath(path) in new_files_dict:
                return new_files_dict[os.path.normpath(path)]
            return self.fail("Unittest is not properly configured")

        return _se


if __name__ == '__main__':
    unittest.main()
