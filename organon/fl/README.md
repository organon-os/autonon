# Organon Framework Library Project

This package contains common code of Organon Analytics' projects. Some of common codes are database access, File utilities etc.

To use Framework Library project in other Organon projects, you should install this project to target projects' virtual environment.
In pycharm, open Terminal, navigate to Organon.FrameworkLibrary project folder. Run following command:
`pip install .`
 
 ## Required packages
 | package | version |
 | ------- | ------- |
 | numpy   | 1.19.1  |
 | pandas  | 1.1.1   |
 | psutil  | 5.7.2   |
 | JayDeBeApi  | 1.1.1   |
NOT: TODO Intel-MKL de kullanilan package' lar buraya eklenmeli 