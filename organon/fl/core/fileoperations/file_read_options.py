"""
This module keeps the FileReadOptions class.
"""


class FileReadOptions:
    """
    Holds the features (such as path) of the file.
    """
    file_full_path: str = None
