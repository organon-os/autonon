"""Includes math constants"""

import numpy as np

DOUBLE_MIN = np.finfo(np.float64).min
DOUBLE_MAX = np.finfo(np.float64).max
INT_MAX = np.iinfo(np.int64).max
INT_MIN = np.iinfo(np.int64).min
NEGATIVE_INFINITY = np.NINF
POSITIVE_INFINITY = np.PINF
