"""Includes UserDateValueDefinition class."""


class UserDateValueDefinition:
    """Date value definition class."""

    def __init__(self):
        self.year: int = None
        self.month: int = None
        self.day: int = None
        self.hour: int = None
