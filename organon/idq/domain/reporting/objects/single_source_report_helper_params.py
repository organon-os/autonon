"""Includes SingleSourceReportHelperParams class."""

from organon.idq.domain.reporting.objects.base_dq_report_helper_params import BaseDqReportHelperParams


class SingleSourceReportHelperParams(BaseDqReportHelperParams):
    """Params for DQ report helper."""
