env.deploymentPath = null
env.scaSuccessful = 'true'
DEVELOPMENT_BRANCH = 'develop-os'
PREMIUM_DEVELOPMENT_BRANCH = 'develop-pr'
STAGING_BRANCH = 'staging-os'
PREMIUM_STAGING_BRANCH = 'staging-pr'

def decideDeploymentPath(){
    if (isPushTo(DEVELOPMENT_BRANCH)){
        env.deploymentPath = env.AUTONON_DEV_DEPLOY_PATH  // jenkins global environment variable
    }
    else if (isPushTo(STAGING_BRANCH)){
         env.deploymentPath = env.AUTONON_PROD_DEPLOY_PATH
    }
    else if (isPushTo(PREMIUM_DEVELOPMENT_BRANCH)){
        env.deploymentPath = env.AUTONON_PR_DEV_DEPLOY_PATH
    }
    else if (isPushTo(PREMIUM_STAGING_BRANCH)){
         env.deploymentPath = env.AUTONON_PR_PROD_DEPLOY_PATH
    }
    else {
         env.deploymentPath = env.AUTONON_JEN_TEST_DEPLOY_PATH
    }
}

def isPushToDev(){
    return isPushTo(DEVELOPMENT_BRANCH) || isPushTo(PREMIUM_DEVELOPMENT_BRANCH)
}

def isPushToStaging(){
    return isPushTo(STAGING_BRANCH) || isPushTo(PREMIUM_STAGING_BRANCH)
}

def isMergeToDev(){
    return isMergeTo(DEVELOPMENT_BRANCH) || isMergeTo(PREMIUM_DEVELOPMENT_BRANCH)
}

def isMergeToStaging(){
    return isMergeTo(STAGING_BRANCH) || isMergeTo(PREMIUM_STAGING_BRANCH)
}

def isMergeTo(branchName){
    return env.CHANGE_TARGET == branchName
}

def isPushTo(branchName){
    return env.BRANCH_NAME == branchName && env.CHANGE_TARGET == null
}

pipeline {
    agent any
    options {
      gitLabConnection('gitadmin gitlab connection')
    }
    stages {
        stage('Decide Deployment Path') {
            steps {
                gitlabCommitStatus("Decide Deployment Path"){
                   decideDeploymentPath()
                }
            }
        }
        stage('Setup Python Environment'){
            steps {
                gitlabCommitStatus("Setup Env."){
                    bat "setup.bat"
                }
            }
        }
        stage('Static Analysis And Tests') {
            parallel{
                stage('Static Code Analysis') {
                    steps {
                        gitlabCommitStatus("static analysis"){
                            script {
                                try{
                                    sh './pre-commit'
                                } catch(Exception e){
                                    env.scaSuccessful = 'false'
                                }
                            }
                        }
                    }
                }
                stage('Test And Coverage') {
                    steps {
                        gitlabCommitStatus("tests"){
                            script {
                                bat "runTestAndCoverage.bat"
                            }
                        }
                    }
                }
            }
        }

        stage("Sonarqube Analysis And Report") {
            steps {
                gitlabCommitStatus("Sonarqube"){
                    script {
                        env.scannerHome = tool 'python sonar-scanner'  // Manage Jenkins>Global Tool Config.>Sonarqube Scanner
                        env.sonarProjectKey = env.BRANCH_NAME.replaceAll("/", "_")
                    }
                    withSonarQubeEnv('orgdevops-sonarserver') {
                      bat "${env.scannerHome}/bin/sonar-scanner.bat -Dsonar.projectKey=Autonon_PR_${env.sonarProjectKey}"
                    }
                    /*
                    timeout(time: 20, unit: 'MINUTES') {
                        waitForQualityGate abortPipeline: true
                    }
                    */
                    script {
                        if (env.scaSuccessful=='false') {
                            error("SCA failed in previous steps. Check reports on Sonarqube.")
                        }
                    }
                }
            }
        }

        stage("Generate SDist"){
            when{
                expression{
                    isPushToDev() || isPushToStaging()
                }
            }
            steps{
                gitlabCommitStatus("generate sdist"){
                    bat "generateSDist.bat"
                    echo "Generating sdist done"
                }
            }
        }

        stage("Deploy"){
            when{
                expression{
                    isPushToDev() || isPushToStaging()
                }
            }
            steps{
                gitlabCommitStatus("deploy sdist"){
                    sshPublisher(publishers: [sshPublisherDesc(configName: 'dev-server', transfers: [sshTransfer(cleanRemote: false, excludes: '', execCommand: 'xcopy /s AfepJenkinsTest "'+env.deploymentPath+'" /y /i', execTimeout: 600000, flatten: false, makeEmptyDirs: false, noDefaultExcludes: false, patternSeparator: '[, ]+', remoteDirectorySDF: false, remoteDirectory: 'AfepJenkinsTest', , sourceFiles: 'dist/*.tar.gz')], usePromotionTimestamp: false, useWorkspaceInPromotion: false, verbose: false)])
                }
            }
        }
    }
    post {
        always {
             script {
                    junit skipPublishingChecks: true, testResults:'reports/testResults.xml'
                    cobertura coberturaReportFile: 'reports/coverage.xml', enableNewApi: true

                    if (isPushToDev() || isPushToStaging()) {
                        emailext body: '$DEFAULT_CONTENT',to: '$DEFAULT_RECIPIENTS', recipientProviders: [developers(), requestor()], subject: 'Push To branch: ' + env.BRANCH_NAME + '- $DEFAULT_SUBJECT'
                    } else if(isMergeToDev() || isMergeToStaging())  {
                        emailext body: '$DEFAULT_CONTENT', recipientProviders: [developers(), requestor()], subject: 'Merge Request Control('+env.CHANGE_BRANCH+'->'+env.CHANGE_TARGET+'):' + env.CHANGE_TITLE + '- $BUILD_STATUS - $PROJECT_NAME'
                    }
                }

        }
    }
}
