echo "starting to create virtual environment and install packages"
python -m venv orgenv || goto :ERROR

call "orgenv/Scripts/activate.bat"  || goto :ERROR

python -m pip install --upgrade pip

python -m pip install -r requirements.txt || goto :ERROR
echo "finished to create virtual environment and install packages"

echo "compiling cython sources"
python cython_setup.py build_ext --inplace || goto :ERROR
echo "cython sources are compiled"

echo "Generating development environment finished"
exit 0

:ERROR
exit /b %errorlevel%