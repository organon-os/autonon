FROM python:3.8
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
RUN apt-get update && apt-get install -y locales locales-all
RUN apt-get install -y libomp-dev
RUN dpkg-reconfigure locales
COPY . /code/
RUN python cython_setup.py build_ext --inplace
