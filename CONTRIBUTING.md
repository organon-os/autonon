# How To Contribute Autonon?
We are really glad you're reading this, because we need volunteer developers to help this project.Your help is very valuable to make it better for everyone.

If you need development guideline, you can check the Development Guide(link verilcek) or directly ask us in Issues/Pull Requests.

`There are many ways to contribute to autonon, with the most common ones being contribution of code or documentation to the project. Other ways to contribute:`<br>

* Contribute to the tests and improving test coverage.<br>
* Contribute to the documentation.<br>
* Contribute to reply questions other user.
* Contribute to review PR.
* Contribute to open an issue to report problems or recommend new features.


### Contributing Code

Firstly, to avoid duplicating work, it is highly advised that you search through the issue tracker and the PR list.
1. Fork the (project repository link ): click on the ‘Fork’ button near the top of the page.
2. Clone your fork of the autonon  repo from your GitLab account to your local disk
3. Run` ./setup.sh`. This script will prepare a virtual environment('orgenv') for you.
4. Add the upstream remote. 
   `git remote add upstream https://gitlab.com/organon-os/autonon`
5. Synchronize your main branch with the upstream/main branch.
    * `git checkout main`
    * `git fetch upstream`
    * `git merge upstream/main`
6. Create a feature branch. `git checkout -b my_feature`
7. `git add modified_files`
8. `git commit`
9. `git push -u origin my_feature`
10. Follow these instructions to create a pull request from your fork.
   https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/proposing-changes-to-your-work-with-pull-requests/creating-a-pull-request-from-a-fork

### Contributing Tests

To run unittests and measure code coverage:

* Activate 'orgenv' virtual environment
* Run command to run coverage: <br>
`coverage run --omit='*orgenv*' -m unittest organon/all_tests.py` <br>

Run command to get coverage report: <br>
* `coverage report`

You can contribute to unit tests to improve the Coverage report. (test klasörünün yolu verilcek.)

### Contributing Documentation

Improving the documentation is important. If you find a typo in the documentation, or have made improvements submit pull request.


