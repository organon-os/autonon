call "orgenv/Scripts/activate.bat" || goto :ERROR
pip install wheel setuptools || goto :ERROR
python setup.py sdist || goto :ERROR
rmdir build /s /q || goto :ERROR

exit 0

:ERROR
exit /b %errorlevel%